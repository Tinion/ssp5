﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.IO;

[CustomEditor (typeof(PrefabCollector))]
public class PrefabCollectorEditor : Editor {

	GUISkin standartSkin = null;
	GUIStyle labelStyle;
	GUIStyle labelStyleOld;
	Color guiColor;
	Color greenColor = new Color (0.34f, 0.89f, 0.33f);
	Color grayColor = new Color (0.33f, 0.33f, 0.33f);
	Vector2 scrollPosition = Vector2.zero;
	public int selectedBiome = 0;
	float width;

	public int selectedBiomInput = 0;

	bool isTabChange = false;

	private void Updateline(int numberCurrent, int number, ref GUIStyle _labelStyle, Color color)
	{
		if (numberCurrent == number) 
		{
			PrefabCollector myTarget = (PrefabCollector)target;
			_labelStyle = myTarget.guiSkin.GetStyle ("Label");
			GUI.color = color;
			_labelStyle.normal.textColor = Color.white;
		}
	}



	public override void OnInspectorGUI ()
	{
		PrefabCollector myTarget = (PrefabCollector)target;
		GUIStyle _labelStyle;
		GUISkin oldSkin = GUI.skin;
		GUI.skin = oldSkin;


		labelStyle = myTarget.guiSkin.GetStyle ("Button");
		labelStyle.alignment = TextAnchor.MiddleCenter;
		labelStyle.clipping = TextClipping.Overflow;
		labelStyle.fontSize = 14;
		labelStyle.fontStyle = FontStyle.Bold;
		labelStyle.normal.textColor = Color.white;
		//_labelStyle.normal.textColor = Color.white;


		guiColor = GUI.color;

		GUILayout.BeginHorizontal (myTarget.guiSkin.GetStyle ("Box"));

		width = EditorGUIUtility.currentViewWidth;

		int next = (int)myTarget.creatType;
		GUI.color = Color.white;
		GUI.contentColor = greenColor;
		//GUI.Box(GUILayoutUtility.GetRect(width, 20.0f), "Density");
		GUIStyle _labelStyle2 = myTarget.guiSkin.GetStyle ("horizontalslider");
		GUIStyle _labelStyle3 = myTarget.guiSkin.GetStyle ("Label");
		_labelStyle3.alignment = TextAnchor.MiddleCenter;
		_labelStyle3.clipping = TextClipping.Overflow;
		_labelStyle3.fontSize = 14;
		_labelStyle3.fontStyle = FontStyle.Bold;
		_labelStyle3.normal.textColor = Color.black;
		Rect rect = EditorGUILayout.GetControlRect (true, 40.0f, _labelStyle2, GUILayout.Height (40.0f));

		EditorGUI.ProgressBar (rect, myTarget.Progress, "");
		GUI.Label (rect, myTarget.ProgressName, _labelStyle3);
		GUILayout.EndHorizontal ();

		GUI.contentColor = Color.white;
		//GUI.skin = guiSkin;
		//GUILayout.Box ("Input", labelStyle, GUILayout.Width (width/6-8), GUILayout.Height (40));
		//Updateline (next, 0, ref labelStyle, grayColor);
		//GUILayout.Box ("Biom", labelStyle, GUILayout.Width (width/6-8), GUILayout.Height (40));
		//Updateline (next, 1, ref labelStyle, grayColor); 
		//GUILayout.Box ("Paint", labelStyle, GUILayout.Width (width/6-8), GUILayout.Height (40));
		//Updateline (next, 2, ref labelStyle, grayColor);
		//GUILayout.Box ("Road", labelStyle, GUILayout.Width (width/6-8), GUILayout.Height (40));
		//Updateline (next, 3, ref labelStyle, grayColor);
		//GUILayout.Box ("Bake", labelStyle, GUILayout.Width (width/6-8), GUILayout.Height (40));
		//Updateline (next, 4, ref labelStyle, grayColor);
		//GUILayout.Box ("Create", labelStyle, GUILayout.Width (width/6-8), GUILayout.Height (40));
		//GUI.color = guiColor; 
		//GUILayout.EndHorizontal ();



		//GUI.skin = oldSkin;
		labelStyleOld = oldSkin.GetStyle ("Label");
		GUI.skin = null;
		switch (myTarget.creatType) {
		case CreationType.Input:


			myTarget.tabSwitch = GUILayout.Toolbar (myTarget.tabSwitch, new string[] { "Open Existing", "Create Manual" });

			switch (myTarget.tabSwitch) {
			case 0:
				{
					InputFromBase ();
				}
				break;
			case 1:
				{
					InputManual ();
				}
				break;


			}

			myTarget.rotationAngle = EditorGUILayout.FloatField ("Rotation Angle", myTarget.rotationAngle);
			myTarget.rotatationMesh = (Mesh)EditorGUILayout.ObjectField ("Mesh to rotation", myTarget.rotatationMesh, typeof(Mesh));

			myTarget.colliderOne = (Mesh)EditorGUILayout.ObjectField ("colliderOne", myTarget.colliderOne, typeof(Mesh));
			myTarget.colliderTwo = (Mesh)EditorGUILayout.ObjectField ("colliderTwo", myTarget.colliderTwo, typeof(Mesh));
			break;
		
		case CreationType.Biom:
			{
				GUILayout.Space (3);
				GUILayout.BeginHorizontal ();
				GUILayout.Space (4);
				GUILayout.Label ("Mesh", EditorStyles.boldLabel);
				GUILayout.EndHorizontal ();

				EditorGUILayout.BeginVertical ("Box");
				GUILayout.Space (4);

				width = EditorGUIUtility.currentViewWidth;

				myTarget.tabSwitch = GUILayout.Toolbar (myTarget.tabSwitch, new string[] {"4 Layers", "8 Layers", "Transition"});
				if (myTarget.tabSwitch != myTarget.tabSwitchSelect)
				{
					myTarget.tabSwitchSelect = myTarget.tabSwitch;
				
					switch (myTarget.tabSwitch) {
					case 0:
						{
							myTarget.current.ln = myTarget.RT.ln = LayersNumber._4Layers;
							myTarget.RT.GetComponent<Renderer> ().sharedMaterial = new Material (Shader.Find ("Relief Pack/Terrain2Geometry4Layers"));
							if (myTarget.RT.currentPresetNumber01 != -1)
								ReliefTerrainEditor.LoadAsset (myTarget.RT.presets [myTarget.RT.currentPresetNumber01].path, myTarget.RT.globalSettingsHolder, myTarget.RT, null);
							myTarget.RT.RefreshAll (myTarget.RT);
						}
						break;
					case 1:
						{
							myTarget.current.ln = myTarget.RT.ln = LayersNumber._8Layers;
							myTarget.RT.GetComponent<Renderer> ().sharedMaterial = new Material (Shader.Find ("Relief Pack/Terrain2Geometry8Layers"));
							if (myTarget.RT.currentPresetNumber01 != -1 && myTarget.RT.currentPresetNumber02 != -1)
								LoadPreset (myTarget.RT.currentPresetNumber01, myTarget.RT.currentPresetNumber02);
							myTarget.RT.RefreshAll (myTarget.RT);
						}
						break;
					case 2:
						{
							myTarget.current.ln = myTarget.RT.ln = LayersNumber._Transition;
							myTarget.RT.GetComponent<Renderer> ().sharedMaterial = new Material (Shader.Find ("Relief Pack/Terrain2GeometryTransition"));
							if (myTarget.RT.currentPresetNumber01 != -1 && myTarget.RT.currentPresetNumber02 != -1)
								LoadPreset (myTarget.RT.currentPresetNumber01, myTarget.RT.currentPresetNumber02);
							myTarget.RT.RefreshAll (myTarget.RT);
						}
						break;
					}
				}
			
				GUI.color = guiColor;
				//GUILayout.FlexibleSpace ();
				GUILayout.Space (3);

				if (myTarget.RT == null) {
					GameObject meshTR = GameObject.Find ("Mesh");
					if (meshTR == null) {
						myTarget.creatType = CreationType.Input;
						return;
					} else
						myTarget.RT = meshTR.transform.GetComponent<ReliefTerrain> ();
				}

				int ch = 0;
				scrollPosition = GUILayout.BeginScrollView (scrollPosition, false, false);
				GUILayout.BeginHorizontal ();
				for (int i = 0; i < myTarget.RT.presets.Length; i++) {
					if (ch == 3) {
						GUILayout.EndHorizontal ();
						GUILayout.BeginHorizontal ();
						ch = 0;
					}
				
					GUILayout.BeginVertical ();
					GUILayout.Space (3);
					GUILayout.Label (myTarget.RT.presets [i].name, EditorStyles.boldLabel, GUILayout.Width (70));
				
					if (GUILayout.Button (myTarget.RT.presets [i].ico, GUILayout.Width (70), GUILayout.Height (70))) {
						switch (myTarget.current.ln) {
						case LayersNumber._4Layers:
							{
								myTarget.RT.ln = myTarget.current.ln;
								ReliefTerrainEditor.LoadAsset (myTarget.RT.presets [i].path, myTarget.RT.globalSettingsHolder, myTarget.RT, null);
								myTarget.RT.RefreshAll (myTarget.RT);
								myTarget.RT.currentPresetNumber01 = i;
								myTarget.current.type = (landscapeType)System.Enum.Parse (typeof(landscapeType), myTarget.RT.presets [i].name);
							} 
							break;
						case LayersNumber._8Layers:

							{
								myTarget.RT.ln = myTarget.current.ln;
								if (((selectedBiome == 0) ? myTarget.RT.currentPresetNumber02 : myTarget.RT.currentPresetNumber01) == -1) {
									ReliefTerrainEditor.LoadAsset (myTarget.RT.presets [i].path, myTarget.RT.globalSettingsHolder, myTarget.RT, null);
									myTarget.RT.RefreshAll (myTarget.RT);
									if (selectedBiome == 0) {
										myTarget.current.type = (landscapeType)System.Enum.Parse (typeof(landscapeType), myTarget.RT.presets [i].name);
										myTarget.RT.currentPresetNumber01 = i;
									} else {
										myTarget.RT.currentPresetNumber02 = i;
									}
								
								} else {
									if (selectedBiome == 0) {
										myTarget.current.type = (landscapeType)System.Enum.Parse (typeof(landscapeType), myTarget.RT.presets [i].name);
										myTarget.RT.currentPresetNumber01 = i;
									} else {
										myTarget.RT.currentPresetNumber02 = i;
									}	

									LoadPreset (myTarget.RT.currentPresetNumber01, myTarget.RT.currentPresetNumber02);
								}
							}
							break;
						case LayersNumber._Transition:
							
							{
								myTarget.RT.ln = myTarget.current.ln;
								if (((selectedBiome == 0) ? myTarget.RT.currentPresetNumber02 : myTarget.RT.currentPresetNumber01) == -1) {
									ReliefTerrainEditor.LoadAsset (myTarget.RT.presets [i].path, myTarget.RT.globalSettingsHolder, myTarget.RT, null);
									myTarget.RT.RefreshAll (myTarget.RT);
									if (selectedBiome == 0) {
										myTarget.current.type = (landscapeType)System.Enum.Parse (typeof(landscapeType), myTarget.RT.presets [i].name);
										myTarget.RT.currentPresetNumber01 = i;
									} else
										myTarget.RT.currentPresetNumber02 = i;

								} else {
									if (selectedBiome == 0) {
										myTarget.current.type = (landscapeType)System.Enum.Parse (typeof(landscapeType), myTarget.RT.presets [i].name);
										myTarget.RT.currentPresetNumber01 = i;
									} else
										myTarget.RT.currentPresetNumber02 = i;

									LoadPreset (myTarget.RT.currentPresetNumber01, myTarget.RT.currentPresetNumber02);
								}	
							}
							break;





						}
					
					}
				
				
					GUILayout.EndVertical ();
					GUILayout.Space (4);

					ch++;
				}

				GUILayout.EndHorizontal ();
				GUILayout.EndScrollView ();

			
				EditorGUILayout.BeginVertical ("Box");

				switch (myTarget.current.ln) {
				case LayersNumber._4Layers:
					{
						GUILayout.Space (10);
						GUILayout.BeginHorizontal ();
						GUILayout.FlexibleSpace ();
						GUILayout.BeginVertical ();
						GUILayout.Label ("Current Biome: ");
						if (myTarget.RT.currentPresetNumber01 == -1) {
							GUILayout.Label (" ", EditorStyles.boldLabel); 
							GUI.color = Color.gray;
							GUILayout.Button ("", GUI.skin.GetStyle ("Button"), GUILayout.Width (100), GUILayout.Height (100));
						} else {
							GUILayout.Label (myTarget.RT.presets [myTarget.RT.currentPresetNumber01].name, EditorStyles.boldLabel); 
							GUI.color = Color.gray;
							GUILayout.Button (myTarget.RT.presets [myTarget.RT.currentPresetNumber01].atlas, GUI.skin.GetStyle ("Button"), GUILayout.Width (100), GUILayout.Height (100));
						}

						GUI.color = Color.white;
						GUILayout.EndVertical ();
						GUILayout.FlexibleSpace ();
						GUILayout.EndHorizontal ();
						GUILayout.Space (10);
					}
					break;

				case LayersNumber._8Layers:
					{
						GUILayout.Space (10);
						GUILayout.BeginHorizontal ();
						GUILayout.FlexibleSpace ();
						GUILayout.BeginVertical ();
						GUILayout.Label ("Main Biome:");
						if (myTarget.RT.currentPresetNumber01 == -1) {
							GUILayout.Label (" ", EditorStyles.boldLabel); 
							GUI.color = (selectedBiome == 0) ? Color.gray : Color.white;
							if (GUILayout.Button ("", GUI.skin.GetStyle ("Button"), GUILayout.Width (100), GUILayout.Height (100)))
								selectedBiome = 1;
						} else {
							GUILayout.Label (myTarget.RT.presets [myTarget.RT.currentPresetNumber01].name, EditorStyles.boldLabel);
							GUI.color = (selectedBiome == 0) ? Color.gray : Color.white;
							if (GUILayout.Button (myTarget.RT.presets [myTarget.RT.currentPresetNumber01].atlas, GUI.skin.GetStyle ("Button"), GUILayout.Width (100), GUILayout.Height (100)))
								selectedBiome = 0;
						}
						GUI.color = Color.white;
						GUILayout.EndVertical ();
						GUILayout.FlexibleSpace ();
						GUILayout.BeginVertical ();
						GUILayout.Label ("Additional Biome:");
						if (myTarget.RT.currentPresetNumber02 == -1) {
							GUILayout.Label (" ", EditorStyles.boldLabel); 
							GUI.color = (selectedBiome == 1) ? Color.gray : Color.white;
							if (GUILayout.Button ("", GUI.skin.GetStyle ("Button"), GUILayout.Width (100), GUILayout.Height (100)))
								selectedBiome = 1;
						} else {
							GUILayout.Label (myTarget.RT.presets [myTarget.RT.currentPresetNumber02].name, EditorStyles.boldLabel); 
							GUI.color = (selectedBiome == 1) ? Color.gray : Color.white;
							if (GUILayout.Button (myTarget.RT.presets [myTarget.RT.currentPresetNumber02].atlas, GUI.skin.GetStyle ("Button"), GUILayout.Width (100), GUILayout.Height (100)))
								selectedBiome = 1;
						}
						GUI.color = Color.white;
						GUILayout.EndVertical ();
						GUILayout.FlexibleSpace ();
						GUILayout.EndHorizontal ();
						GUILayout.Space (10);
					}
					break;
				

				case LayersNumber._Transition:
					{
						GUILayout.Space (10);
						GUILayout.BeginHorizontal ();
						GUILayout.FlexibleSpace ();
						GUILayout.BeginVertical ();
						GUILayout.Label ("Start Biome:");
						if (myTarget.RT.currentPresetNumber01 == -1) {
							GUILayout.Label (" ", EditorStyles.boldLabel); 
							GUI.color = (selectedBiome == 0) ? Color.gray : Color.white;
							if (GUILayout.Button ("", GUI.skin.GetStyle ("Button"), GUILayout.Width (100), GUILayout.Height (100)))
								selectedBiome = 0;
						} else {
							GUILayout.Label (myTarget.RT.presets [myTarget.RT.currentPresetNumber01].name, EditorStyles.boldLabel); 
							GUI.color = (selectedBiome == 0) ? Color.gray : Color.white;
							if (GUILayout.Button (myTarget.RT.presets [myTarget.RT.currentPresetNumber01].atlas, GUI.skin.GetStyle ("Button"), GUILayout.Width (100), GUILayout.Height (100)))
								selectedBiome = 0;
						}
						GUI.color = Color.white;
						GUILayout.EndVertical ();
						GUILayout.FlexibleSpace ();
						GUILayout.BeginVertical ();
						GUILayout.Label ("End Biome:");
						if (myTarget.RT.currentPresetNumber02 == -1) {
							GUILayout.Label (" ", EditorStyles.boldLabel); 
							GUI.color = (selectedBiome == 1) ? Color.gray : Color.white;
							if (GUILayout.Button ("", GUI.skin.GetStyle ("Button"), GUILayout.Width (100), GUILayout.Height (100)))
								selectedBiome = 1;
						} else {
							GUILayout.Label (myTarget.RT.presets [myTarget.RT.currentPresetNumber02].name, EditorStyles.boldLabel); 
							GUI.color = (selectedBiome == 1) ? Color.gray : Color.white;
							if (GUILayout.Button (myTarget.RT.presets [myTarget.RT.currentPresetNumber02].atlas, GUI.skin.GetStyle ("Button"), GUILayout.Width (100), GUILayout.Height (100)))
								selectedBiome = 1;
						}
						GUI.color = Color.white;
						GUILayout.EndVertical ();
						GUILayout.FlexibleSpace ();
						GUILayout.EndHorizontal ();
						GUILayout.Space (10);
					}
					break;

				}
			
				EditorGUILayout.EndVertical ();

				GUI.color = guiColor;

				GUILayout.Space (7);
			
				if (myTarget.RT.globalSettingsHolder.currentPreset != "") {
					//GUI.color = greenColor;
					GUILayout.BeginHorizontal (myTarget.guiSkin.GetStyle ("Box"));
					if (GUILayout.Button ("Next: Paint & Merging", labelStyle, GUILayout.ExpandWidth (true), GUILayout.Height (35)))
						myTarget.Prepare ();
					GUILayout.EndHorizontal ();

					GUI.color = guiColor;
				}


			}
			break;		
		
		case CreationType.Paint:
			{
				if (myTarget.RT == null) 
				{
					GameObject meshTR = GameObject.Find ("Mesh");
					if (meshTR == null) {
						myTarget.creatType = CreationType.Input;
						return;
					} else
						myTarget.RT = meshTR.transform.GetComponent<ReliefTerrain> ();
				}

				PaintInspector ();

				GUILayout.Space (7);
				GUILayout.BeginHorizontal (myTarget.guiSkin.GetStyle ("Box"));
				//GUI.color = greenColor;
				if (GUILayout.Button ("Next: Bake", labelStyle, GUILayout.ExpandWidth (true), GUILayout.Height (35))) 
				{
					if (myTarget.mergingButton)
						myTarget.Bake ();
					else
						myTarget.Merging ();
				}
				GUILayout.EndHorizontal ();
				GUI.color = guiColor;


			}
			break;
		case CreationType.Bake:

			if (myTarget.RT == null) {
				GameObject meshTR = GameObject.Find ("Mesh");
				if (meshTR == null) {
					myTarget.creatType = CreationType.Input;
					return;
				} else
					myTarget.RT = meshTR.transform.GetComponent<ReliefTerrain> ();
			}
			GUILayout.Space (7);
			GUILayout.Label ("Thanks for using Prefab Collector");

			break;

		}

	}

	public void InputFromBase()
	{
		PrefabCollector myTarget = (PrefabCollector)target;

		GUI.color = Color.white;
		GUILayout.BeginHorizontal ();
		for (int i = 0; i < myTarget.biomeSources.biomesSource.Count; i++) 
		{
			if (selectedBiomInput == i)
				GUI.color = Color.yellow;
			else
				GUI.color = Color.white;
			if (GUILayout.Button (myTarget.biomeSources.biomesSource [i].image, GUILayout.Width (69), GUILayout.Height (69))) 
			{
				selectedBiomInput = i;
			}
		}
		GUI.color = Color.white;
		GUILayout.EndHorizontal ();

			GUILayout.BeginHorizontal ();
			GUILayout.Space (4);
			GUILayout.Label (myTarget.biomeSources.biomesSource[selectedBiomInput].name, EditorStyles.boldLabel);
			GUILayout.EndHorizontal ();

			EditorGUILayout.BeginVertical ("Box", GUILayout.Width(width-40));
			GUILayout.Space (4);

			for (int j = 0; j < myTarget.blocks.Count; j++)
			{
			
			if ((int)myTarget.blocks[j].type == selectedBiomInput)
				//if (myTarget.blocks[j].ln == LayersNumber._4Layers)
				{
					GUILayout.BeginHorizontal ();
						
						if (GUILayout.Button (myTarget.blocks[j].blockName, GUILayout.Height (20),  GUILayout.Width (150))) 
						{
							myTarget.current = myTarget.blocks [j];
							myTarget.tabSwitch = 1;
						}
						
					GUILayout.Space (20);
					GUILayout.Label(myTarget.blocks[j].typeBlockPlane.ToString (), GUILayout.Height (20),  GUILayout.Width (150));
					GUILayout.Label(myTarget.blocks[j].ln.ToString (), GUILayout.Height (20),  GUILayout.Width (150));
					GUILayout.EndHorizontal ();
				}
			}

			
			GUILayout.Space (3);
			EditorGUILayout.EndVertical ();


	}

	public void InputManual()
	{
		PrefabCollector myTarget = (PrefabCollector)target;

		bool isFull = true;
		//base.OnInspectorGUI ();
		//		GUILayout.Space (3);
		GUI.color = Color.white;

		GUILayout.BeginHorizontal ();
		GUILayout.Space (4);
		GUILayout.Label ("Type", EditorStyles.boldLabel);
		GUILayout.EndHorizontal ();

		EditorGUILayout.BeginVertical ("Box");
		GUILayout.Space (4);

		GUILayout.BeginHorizontal ();
		GUILayout.Label (" Name", labelStyleOld, GUILayout.Width (200));
		myTarget.current.blockName = EditorGUILayout.TextField (myTarget.current.blockName);
		if (myTarget.current.blockName != "")
			GUI.color = Color.green;
		else {
			isFull = false;
			GUI.color = Color.red;
		}
		GUILayout.Box ("", GUILayout.Width (15), GUILayout.Height (15));
		GUI.color = Color.white;
		GUILayout.EndHorizontal ();

		GUILayout.BeginHorizontal ();
		GUILayout.Label (" Type", labelStyleOld, GUILayout.Width (200));
		myTarget.current.typeBlockPlane = (BlockPlaneType)EditorGUILayout.EnumPopup (myTarget.current.typeBlockPlane);
		GUILayout.EndHorizontal ();


		//if (GUILayout.Button ((myTarget.isBezierData) ? " Load Bezier (Switch to Create)" : " Create Bezier (Switch to Load)", GUI.skin.GetStyle ("Button"), GUILayout.Width (200))) {
		//	myTarget.isBezierData = !myTarget.isBezierData;
		//}
		bool isForkless = false;
		if (myTarget.current.typeBlockPlane == BlockPlaneType.Fork || myTarget.current.typeBlockPlane == BlockPlaneType.Connection)
			isForkless = true;

		GUILayout.BeginHorizontal ();

		if (myTarget.current.typeBlockPlane != BlockPlaneType.Outpost) {
			myTarget.current.bezierData = (Object)EditorGUILayout.ObjectField (myTarget.current.bezierData, typeof(Object), false);

			if (myTarget.current.bezierData != null)
				GUI.color = Color.green;
			else {
				isFull = false;
				GUI.color = Color.red;
			}
			GUILayout.Box ("", GUILayout.Width (15), GUILayout.Height (15));
			GUI.color = Color.white;
		}
		GUILayout.EndHorizontal ();


		GUILayout.BeginHorizontal ();

		if (isForkless) {
			myTarget.current.bezierData02 = (Object)EditorGUILayout.ObjectField (myTarget.current.bezierData02, typeof(Object), false);

			if (myTarget.current.bezierData02 != null)
				GUI.color = Color.green;
			else {
				isFull = false;
				GUI.color = Color.red;
			}
			GUILayout.Box ("", GUILayout.Width (15), GUILayout.Height (15));
			GUI.color = Color.white;
		}
		GUILayout.EndHorizontal ();

		GUILayout.Space (3);
		EditorGUILayout.EndVertical ();

		GUILayout.Space (3);
		GUILayout.BeginHorizontal ();
		GUILayout.Space (4);
		GUILayout.Label ("Mesh", EditorStyles.boldLabel);
		GUILayout.EndHorizontal ();

		EditorGUILayout.BeginVertical ("Box");
		GUILayout.Space (4);

		GUILayout.BeginHorizontal ();
		GUILayout.Label (" Terrain Mesh", GUILayout.Width (200));
		myTarget.current.terrain = (Mesh)EditorGUILayout.ObjectField (myTarget.current.terrain, typeof(Mesh), false);

		if (myTarget.current.terrain != null)
			GUI.color = Color.green;
		else {
			isFull = false;
			GUI.color = Color.red;
		}
		GUILayout.Box ("", GUILayout.Width (15), GUILayout.Height (15));
		GUI.color = Color.white;

		GUILayout.EndHorizontal ();

		GUILayout.BeginHorizontal ();
		GUILayout.Label (" Collider Mesh", GUILayout.Width (200));
		myTarget.current.collider = (Mesh)EditorGUILayout.ObjectField (myTarget.current.collider, typeof(Mesh), false);

		if (myTarget.current.collider != null)
			GUI.color = Color.green;
		else {
			isFull = false;
			GUI.color = Color.red;
		}
		GUILayout.Box ("", GUILayout.Width (15), GUILayout.Height (15));
		GUI.color = Color.white;
		GUILayout.EndHorizontal ();


		GUILayout.BeginHorizontal ();

		if (GUILayout.Button ("Add Road"))
			myTarget.current.road.Add (null);
		GUILayout.EndHorizontal ();



		for (int i = 0; i < myTarget.current.road.Count; i++) 
		{
			

				GUILayout.BeginHorizontal ();
				if (myTarget.current.road[i] != null) 
					GUILayout.Label (myTarget.current.road [i].name, GUILayout.Width (200));
				else
					GUILayout.Label ("Road", GUILayout.Width (200));

				myTarget.current.road [i] = (Mesh)EditorGUILayout.ObjectField (myTarget.current.road [i], typeof(Mesh), false);


				GUI.color = Color.red;
				if (GUILayout.Button ("X"))
					myTarget.current.road.Remove (myTarget.current.road [i]);

				if (myTarget.current.road [i] != null)
					GUI.color = Color.green;
				else {
					isFull = false;
					GUI.color = Color.red;
				}
				GUILayout.Box ("", GUILayout.Width (15), GUILayout.Height (15));
				GUI.color = Color.white;
				GUILayout.EndHorizontal ();

		}



		GUILayout.Space (3);
		EditorGUILayout.EndVertical ();

		GUILayout.BeginHorizontal ();
		GUILayout.Space (4);
		GUILayout.Label ("Splat Maps", EditorStyles.boldLabel);
		GUILayout.EndHorizontal ();

		EditorGUILayout.BeginVertical ("Box");
		GUILayout.Space (4);

		GUILayout.BeginHorizontal ();
		GUILayout.Label (" Type", GUILayout.Width (200));
		if (GUILayout.Button ((myTarget.current.ln == LayersNumber._4Layers) ? " 4 Layers (Switch to 8 Layers)" : " 8 Layers (Switch to 4 Layers)", GUI.skin.GetStyle ("Button"))) {
			if (myTarget.current.ln == LayersNumber._4Layers) {
				myTarget.current.ln = LayersNumber._8Layers;
				//myTarget.splatMapSources = new Texture2D[2];
			} else {
				myTarget.current.ln = LayersNumber._4Layers;
				//myTarget.splatMapSources = new Texture2D[1];
			}

		} 
		GUILayout.EndHorizontal ();

		if (myTarget.current.ln == LayersNumber._4Layers) {
			GUILayout.BeginHorizontal ();
			GUILayout.Label (" Splat Map", GUILayout.Width (200));
			myTarget.current.splatMapSources [0] = (Texture2D)EditorGUILayout.ObjectField (myTarget.current.splatMapSources [0], typeof(Texture2D), false);

			if (myTarget.current.splatMapSources [0] != null)
				GUI.color = Color.green;
			else {
				isFull = false;
				GUI.color = Color.red;
			}
			GUILayout.Box ("", GUILayout.Width (15), GUILayout.Height (15));
			GUI.color = Color.white;

			GUILayout.EndHorizontal ();
		} else {
			GUILayout.BeginHorizontal ();
			GUILayout.Label (" Splat Map A", GUILayout.Width (200));
			myTarget.current.splatMapSources [0] = (Texture2D)EditorGUILayout.ObjectField (myTarget.current.splatMapSources [0], typeof(Texture2D), false);

			if (myTarget.current.splatMapSources [0] != null)
				GUI.color = Color.green;
			else {
				isFull = false;
				GUI.color = Color.red;
			}
			GUILayout.Box ("", GUILayout.Width (15), GUILayout.Height (15));
			GUI.color = Color.white;
			GUILayout.EndHorizontal ();

			GUILayout.BeginHorizontal ();
			GUILayout.Label (" Splat Map B", GUILayout.Width (200));
			myTarget.current.splatMapSources [1] = (Texture2D)EditorGUILayout.ObjectField (myTarget.current.splatMapSources [1], typeof(Texture2D), false);

			if (myTarget.current.splatMapSources [1] != null)
				GUI.color = Color.green;
			else {
				isFull = false;
				GUI.color = Color.red;
			}
			GUILayout.Box ("", GUILayout.Width (15), GUILayout.Height (15));
			GUI.color = Color.white;
			GUILayout.EndHorizontal ();
		}

		GUILayout.Space (3);
		if (isFull)
		{
			GUILayout.BeginHorizontal ();
			myTarget.current.type = (landscapeType)EditorGUILayout.EnumPopup (myTarget.current.type);
			if (myTarget.isSavePresets)
				GUI.color = Color.green;
			else
				GUI.color = Color.red;
			if (GUILayout.Button ((myTarget.isSavePresets) ? "Save preset" : "Not save preset")) 
			{
				myTarget.isSavePresets = !myTarget.isSavePresets;
			}
			GUILayout.EndHorizontal ();
			GUI.color = Color.white;
		}
		EditorGUILayout.EndVertical ();



		GUILayout.Space (7);

		if (isFull) {
			GUILayout.BeginHorizontal (myTarget.guiSkin.GetStyle ("Box"));

			//GUI.color = greenColor;
			labelStyle = myTarget.guiSkin.GetStyle ("Button");
			if (GUILayout.Button ("Next: Select Biom", labelStyle, GUILayout.ExpandWidth (true), GUILayout.Height (35))) {
				myTarget.Create ();
			}
			GUI.color = guiColor;
			GUILayout.EndHorizontal ();

		}
	}

	public void LoadPreset(int SourceA, int SourceB)
	{
		PrefabCollector myTarget = (PrefabCollector)target;
		ReliefTerrainPresetHolder preset01 = AssetDatabase.LoadAssetAtPath (myTarget.RT.presets [SourceA].path, typeof(ReliefTerrainPresetHolder)) as ReliefTerrainPresetHolder;
		ReliefTerrainPresetHolder preset02 = AssetDatabase.LoadAssetAtPath (myTarget.RT.presets [SourceB].path, typeof(ReliefTerrainPresetHolder)) as ReliefTerrainPresetHolder;

		ReliefTerrainPresetHolder preset01Copy = UnityEngine.Object.Instantiate (preset01) as ReliefTerrainPresetHolder;
		ReliefTerrainPresetHolder preset02Copy = UnityEngine.Object.Instantiate (preset02) as ReliefTerrainPresetHolder;

		ReliefTerrainPresetHolder mergePreset = ScriptableObject.CreateInstance (typeof(ReliefTerrainPresetHolder)) as ReliefTerrainPresetHolder;
		mergePreset = mergePreset.Merge (preset01Copy.PresetName + preset02Copy.PresetName, preset01Copy, preset02Copy);
		mergePreset.type = "MESH";
		string path = "";
		//ReliefTerrainPresetHolder savedPreset=UnityEngine.Object.Instantiate(mergePreset) as ReliefTerrainPresetHolder;
		if (mergePreset != null) {
			path = "Assets/Editor/" +  preset01Copy.PresetName + preset02Copy.PresetName + ".asset";

			int idx = path.IndexOf ("/Assets/") + 1;
			if (idx > 0)
				path = path.Substring (idx);
			Debug.Log (path);
			if (AssetDatabase.LoadAssetAtPath (path, typeof(ReliefTerrainPresetHolder)) != null)
				AssetDatabase.DeleteAsset (path);
			AssetDatabase.CreateAsset (mergePreset, path);
		} else
			Debug.Log ("Merge preset is null");
		AssetDatabase.Refresh ();
		AssetDatabase.SaveAssets ();

		ReliefTerrainEditor.LoadAsset(path, myTarget.RT.globalSettingsHolder, myTarget.RT, null);
		myTarget.RT.RefreshAll (myTarget.RT);

	}

	public void PaintInspector()
	{
		
		
		PrefabCollector myTarget = (PrefabCollector)target;
	int thumb_size = 32;

		ReliefTerrain _targetRT = myTarget.RT;
		ReliefTerrainGlobalSettingsHolder _target = _targetRT.globalSettingsHolder;


		GUILayout.Space (3);
		EditorGUILayout.LabelField ("Paint & Merging", EditorStyles.boldLabel);
		GUILayout.Space (3);
		EditorGUILayout.BeginVertical ("Box");		

		if (GUILayout.Button ("Select Mesh", GUILayout.Height(25)))
			Selection.activeGameObject = _targetRT.gameObject;
		
		//if (AssetDatabase."Assets/Resources/blocks/" + RT.presets[RT.currentPresetNumber01].name.ToString () + "/" + RT.presets[RT.currentPresetNumber01].name.ToString () + blockName.ToString () + ".prefab"
		if (File.Exists ("Assets/Resources/blocks/" + myTarget.RT.presets [myTarget.RT.currentPresetNumber01].name.ToString () + "/" + myTarget.RT.presets [myTarget.RT.currentPresetNumber01].name.ToString () + myTarget.current.blockName.ToString () + ".prefab"))
		{
			if (!myTarget.mergingButton)
			{
				GUI.color = Color.green;
				if (GUILayout.Button ("Block is exist. Merging: active", GUILayout.Height (25))) {
					myTarget.mergingButton = !myTarget.mergingButton;
				}
			} 
			else
			{
				GUI.color = Color.red;
				if (GUILayout.Button ("Block is exist. Merging: overlay", GUILayout.Height (25))) 
				{
					myTarget.mergingButton = !myTarget.mergingButton;
				}
			}

		} else {
			GUI.color = Color.yellow;
			if (GUILayout.Button ("Block is not exist. Merging: deactive", GUILayout.Height (25))) {
			}
			myTarget.mergingButton = true;
		}

		if (GUILayout.Button ("TextureControlTo256", labelStyle, GUILayout.ExpandWidth (true), GUILayout.Height (35)))
			myTarget.TextureControlTo256 ();
		
		GUI.color = Color.white;
		EditorGUILayout.EndVertical ();	
	}

	public void CustomOnSceneGUI (SceneView sceneview)
	{
		
		PrefabCollector targetCol = (PrefabCollector)target;
		if (targetCol.RT == null)
			return;
			ReliefTerrain _targetRT = targetCol.RT;
			ReliefTerrainGlobalSettingsHolder _target = _targetRT.globalSettingsHolder;
		
		bool control_down_flag = _targetRT.control_down_flag;

		EditorWindow currentWindow = EditorWindow.mouseOverWindow;
		if (!currentWindow)
			return;

		Event current = Event.current;

		if (current.alt) {
			return;
		}		
		if (Event.current.button == 1) {
			HandleUtility.AddDefaultControl (GUIUtility.GetControlID (FocusType.Passive));
			return;
		}

		bool paintOff_flag = (UnityEditor.Tools.current != Tool.View);
		if (_target.paint_wetmask) {
			paintOff_flag = paintOff_flag;
		} else {
			paintOff_flag = paintOff_flag;
		}
		if (paintOff_flag) {
			_target.paint_flag = false;
			SceneView.onSceneGUIDelegate -= ReliefTerrain._SceneGUI;
			EditorUtility.SetDirty (target);
			return;
		}		

		if (current.type == EventType.Layout) {
			HandleUtility.AddDefaultControl (GUIUtility.GetControlID (FocusType.Passive));
			return;
		}

		switch (current.type) {
		case EventType.KeyDown:
			if (current.keyCode == KeyCode.Escape) {
				_target.paint_flag = false;
				UnityEditor.Tools.current = _targetRT.prev_tool;
				SceneView.onSceneGUIDelegate -= ReliefTerrain._SceneGUI;
				EditorUtility.SetDirty (target);
			}
			break;
		}

		if (current.control) {
			if (current.type == EventType.MouseMove) {
				if (control_down_flag) {
					control_down_flag = false;
					EditorUtility.SetDirty (target);
				}
			}
			return;
		}
		control_down_flag = true;

		switch (current.type) {
		case EventType.MouseDown:
			_targetRT.get_paint_coverage ();
			// Debug.Log(""+cover_verts_num + "  "+ paintHitInfo_flag + _target.prepare_tmpColorMap());
			if (_targetRT.paintHitInfo_flag) {
				if (_targetRT.prepare_tmpTexture (!_target.paint_wetmask)) {
					#if UNITY_3_5 || UNITY_4_0 || UNITY_4_1 || UNITY_4_2
					Undo.RegisterUndo(_targetRT.tmp_globalColorMap, "Geometry Blend Edit");
					#else
					Undo.RegisterCompleteObjectUndo (_targetRT.tmp_globalColorMap, "Geometry Blend Edit");
					#endif
					_targetRT.modify_blend (!current.shift);

				}
			} else {
				_target.undo_flag = true;
			}
			current.Use ();
			break;
		case EventType.MouseDrag:
			_targetRT.get_paint_coverage ();
			if (_targetRT.paintHitInfo_flag) {
				if (_target.undo_flag) {
					if (_targetRT.prepare_tmpTexture (!_target.paint_wetmask)) {
						#if UNITY_3_5 || UNITY_4_0 || UNITY_4_1 || UNITY_4_2
						Undo.RegisterUndo(_targetRT.tmp_globalColorMap, "Geometry Blend Edit");
						#else
						Undo.RegisterCompleteObjectUndo (_targetRT.tmp_globalColorMap, "Geometry Blend Edit");
						#endif
					}
					_target.undo_flag = false;
				}
			}
			if (_targetRT.paintHitInfo_flag) {
				if (_targetRT.prepare_tmpTexture (!_target.paint_wetmask)) {
					_targetRT.modify_blend (!current.shift);
					//current.Use();
				}
			}
			break;
		case EventType.MouseMove:
			_targetRT.get_paint_coverage ();
			break;
		}



		if (_targetRT.paintHitInfo_flag) {
			bool upflag = _target.paint_wetmask ? !current.shift : current.shift;

			if (upflag) {

				Handles.color = new Color (1, 0, 0, Mathf.Max (0.1f, _target.paint_opacity * 0.5f));
				Handles.DrawWireDisc (_targetRT.paintHitInfo.point, _targetRT.paintHitInfo.normal, _target.paint_size);
				Handles.color = new Color (1, 0, 0, Mathf.Max (0.6f, _target.paint_opacity));
				Handles.DrawWireDisc (_targetRT.paintHitInfo.point, _targetRT.paintHitInfo.normal, _target.paint_size * Mathf.Max (0.3f, 1 - _target.paint_smoothness));
			} else {
				Handles.color = new Color (0, 1, 0, Mathf.Max (0.1f, _target.paint_opacity * 0.5f));
				Handles.DrawWireDisc (_targetRT.paintHitInfo.point, _targetRT.paintHitInfo.normal, _target.paint_size);
				Handles.color = new Color (0, 1, 0, Mathf.Max (0.6f, _target.paint_opacity));
				Handles.DrawWireDisc (_targetRT.paintHitInfo.point, _targetRT.paintHitInfo.normal, _target.paint_size * Mathf.Max (0.3f, 1 - _target.paint_smoothness));
			}			
		}		
		if (current.shift)
			current.Use ();
			
	}
}
