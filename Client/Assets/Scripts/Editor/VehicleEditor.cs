﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor (typeof(Vehicle))]
public class VehicleEditor : UnitEditor {

	bool isVehicleSpecifications = false;

	public override void OnInspectorGUI ()
	{
		Vehicle myTarget = (Vehicle)target;

		base.OnInspectorGUI ();

		Color GUIColor = GUI.color;
		if (isVehicleSpecifications)
			GUI.color = Color.gray;

		if (GUILayout.Button ("Vehicle Specifications", GUILayout.Height (20.0f)))
			isVehicleSpecifications = !isVehicleSpecifications;


		GUI.color = GUIColor;

		if (isVehicleSpecifications) 
		{
			EditorGUILayout.BeginVertical ("Box");

			myTarget.curveBoost = (AnimationCurve)EditorGUILayout.CurveField ("Boost Curve", myTarget.curveBoost);

			GUILayout.Label ("Current steer", EditorStyles.boldLabel);

			myTarget.steerCurrent = EditorGUILayout.FloatField ("Steer", myTarget.steerCurrent);
			myTarget.currentSteerClamp = EditorGUILayout.FloatField ("Steer Clamp", myTarget.currentSteerClamp);

			GUILayout.Label ("Current speed", EditorStyles.boldLabel);

			myTarget.currentBrake = EditorGUILayout.FloatField ("Brake", myTarget.currentBrake);
			myTarget.currentBoost = EditorGUILayout.FloatField ("Boost", myTarget.currentBoost);
			myTarget.currentAccel = EditorGUILayout.FloatField ("Accel", myTarget.currentAccel);

			EditorGUILayout.Space();

			myTarget.averageRpm = EditorGUILayout.FloatField ("Average RPM", myTarget.averageRpm);

			myTarget.killTimer.max = EditorGUILayout.FloatField ("KillTimer", myTarget.killTimer.max);

			GUILayout.Label ("Wheel Colliders", EditorStyles.boldLabel);

			for (int i = 0; i < myTarget.wheelsColliderForward.Length; i++) 
				myTarget.wheelsColliderForward [i] = (WheelCollider)EditorGUILayout.ObjectField ("Forward " + ((i%2==0) ? "Left" : "Right").ToString() + " Wheel Collider", myTarget.wheelsColliderForward [i], typeof(WheelCollider), true);
			
			for (int i = 0; i < myTarget.wheelsColliderBack.Length; i++) 
				myTarget.wheelsColliderBack [i] = (WheelCollider)EditorGUILayout.ObjectField ("Rear " + ((i%2==0) ? "Left" : "Right") + " Wheel Collider", myTarget.wheelsColliderBack [i], typeof(WheelCollider), true);

			GUILayout.Label ("Wheel Transforms", EditorStyles.boldLabel);

			for (int i = 0; i < myTarget.wheelsTransformForward.Length; i++) 
				myTarget.wheelsTransformForward [i] = (Transform)EditorGUILayout.ObjectField ("Forward " + ((i%2==0) ? "Left" : "Right") + " Wheel Transform", myTarget.wheelsTransformForward [i], typeof(Transform), true);

			for (int i = 0; i < myTarget.wheelsTransformBack.Length; i++) 
				myTarget.wheelsTransformBack [i] = (Transform)EditorGUILayout.ObjectField ("Rear " + ((i%2==0) ? "Left" : "Right") + " Wheel Transform", myTarget.wheelsTransformBack [i], typeof(Transform), true);

			GUILayout.Label ("Suspension Data", EditorStyles.boldLabel);
	
			myTarget.naturalFrequency = EditorGUILayout.Slider ("Natural Frequency", myTarget.naturalFrequency, 0, 20.0f);
			myTarget.dampingRatio = EditorGUILayout.Slider ("Damping Ratio", myTarget.dampingRatio, 0, 3.0f);
			myTarget.forceShift = EditorGUILayout.Slider ("Force Shift", myTarget.forceShift, -1f, 1f);

			EditorGUILayout.EndVertical ();
			EditorGUILayout.Separator ();
		}
	}
	/*
	GUISkin standartSkin = null;
	GUIStyle labelStyle;
	Color greenColor = new Color (0.34f, 0.89f, 0.33f);
	Color grayColor = new Color (0.33f, 0.33f, 0.33f);
	Vector2 scrollPosition = Vector2.zero;
	public int selectedBiome = 0;

	private void Updateline(int numberCurrent, int number, ref GUIStyle _labelStyle, Color color)
	{
		if (numberCurrent == number) 
		{
			PrefabCollector myTarget = (PrefabCollector)target;
			_labelStyle = myTarget.guiSkin.GetStyle ("Label");
			GUI.color = color;
			_labelStyle.normal.textColor = Color.white;
		}
	}



	
	{
		PrefabCollector myTarget = (PrefabCollector)target;
		GUIStyle _labelStyle;
		GUISkin oldSkin = GUI.skin;
		GUI.skin = oldSkin;


		labelStyle = myTarget.guiSkin.GetStyle ("Button");
		labelStyle.alignment = TextAnchor.MiddleCenter;
		labelStyle.clipping = TextClipping.Overflow;
		labelStyle.fontSize = 14;
		labelStyle.fontStyle = FontStyle.Bold;
		labelStyle.normal.textColor = Color.white;
		//_labelStyle.normal.textColor = Color.white;


		Color guiColor = GUI.color;

		GUILayout.BeginHorizontal (myTarget.guiSkin.GetStyle ("Box"));

		float width = EditorGUIUtility.currentViewWidth;

		int next = (int)myTarget.creatType;
		GUI.color = Color.white;
		GUI.contentColor = greenColor;
		//GUI.Box(GUILayoutUtility.GetRect(width, 20.0f), "Density");
		GUIStyle _labelStyle2 = myTarget.guiSkin.GetStyle ("horizontalslider");
		GUIStyle _labelStyle3 = myTarget.guiSkin.GetStyle ("Label");
		_labelStyle3.alignment = TextAnchor.MiddleCenter;
		_labelStyle3.clipping = TextClipping.Overflow;
		_labelStyle3.fontSize = 14;
		_labelStyle3.fontStyle = FontStyle.Bold;
		_labelStyle3.normal.textColor = Color.black;
		Rect rect = EditorGUILayout.GetControlRect (true, 40.0f, _labelStyle2, GUILayout.Height (40.0f));

		EditorGUI.ProgressBar (rect, myTarget.Progress, "");
		GUI.Label (rect, myTarget.ProgressName, _labelStyle3);
		GUILayout.EndHorizontal ();

		GUI.contentColor = Color.white;
		//GUI.skin = guiSkin;
		//GUILayout.Box ("Input", labelStyle, GUILayout.Width (width/6-8), GUILayout.Height (40));
		//Updateline (next, 0, ref labelStyle, grayColor);
		//GUILayout.Box ("Biom", labelStyle, GUILayout.Width (width/6-8), GUILayout.Height (40));
		//Updateline (next, 1, ref labelStyle, grayColor); 
		//GUILayout.Box ("Paint", labelStyle, GUILayout.Width (width/6-8), GUILayout.Height (40));
		//Updateline (next, 2, ref labelStyle, grayColor);
		//GUILayout.Box ("Road", labelStyle, GUILayout.Width (width/6-8), GUILayout.Height (40));
		//Updateline (next, 3, ref labelStyle, grayColor);
		//GUILayout.Box ("Bake", labelStyle, GUILayout.Width (width/6-8), GUILayout.Height (40));
		//Updateline (next, 4, ref labelStyle, grayColor);
		//GUILayout.Box ("Create", labelStyle, GUILayout.Width (width/6-8), GUILayout.Height (40));
		//GUI.color = guiColor; 
		//GUILayout.EndHorizontal ();



		//GUI.skin = oldSkin;
		GUIStyle labelStyleOld = oldSkin.GetStyle ("Label");
		GUI.skin = null;
		switch (myTarget.creatType) {
		case CreationType.Input:

			bool isFull = true;
			//base.OnInspectorGUI ();
//		GUILayout.Space (3);
			GUI.color = Color.white;

			GUILayout.BeginHorizontal ();
			GUILayout.Space (4);
			GUILayout.Label ("Type", EditorStyles.boldLabel);
			GUILayout.EndHorizontal ();

			EditorGUILayout.BeginVertical ("Box");
			GUILayout.Space (4);

			GUILayout.BeginHorizontal ();
			GUILayout.Label (" Name", labelStyleOld, GUILayout.Width (200));
			myTarget.blockName = EditorGUILayout.TextField (myTarget.blockName);
			if (myTarget.blockName != "")
				GUI.color = Color.green;
			else {
				isFull = false;
				GUI.color = Color.red;
			}
			GUILayout.Box ("", GUILayout.Width (15), GUILayout.Height (15));
			GUI.color = Color.white;
			GUILayout.EndHorizontal ();


			GUILayout.BeginHorizontal ();
			if (GUILayout.Button ((myTarget.isBezierData) ? " Load Bezier (Switch to Create)" : " Create Bezier (Switch to Load)", GUI.skin.GetStyle ("Button"), GUILayout.Width (200))) {
				myTarget.isBezierData = !myTarget.isBezierData;
			}
			if (!myTarget.isBezierData)
				myTarget.bezierData = (Object)EditorGUILayout.ObjectField (myTarget.bezierData, typeof(Object), false);
			else
				GUILayout.Label ("New Forward Bezier", labelStyleOld);

			if (myTarget.isBezierData != null || myTarget.isBezierData)
				GUI.color = Color.green;
			else {
				isFull = false;
				GUI.color = Color.red;
			}
			GUILayout.Box ("", GUILayout.Width (15), GUILayout.Height (15));
			GUI.color = Color.white;

			GUILayout.EndHorizontal ();

			GUILayout.Space (3);
			EditorGUILayout.EndVertical ();

			GUILayout.Space (3);
			GUILayout.BeginHorizontal ();
			GUILayout.Space (4);
			GUILayout.Label ("Mesh", EditorStyles.boldLabel);
			GUILayout.EndHorizontal ();

			EditorGUILayout.BeginVertical ("Box");
			GUILayout.Space (4);

			GUILayout.BeginHorizontal ();
			GUILayout.Label (" Terrain Mesh", GUILayout.Width (200));
			myTarget.terrain = (Mesh)EditorGUILayout.ObjectField (myTarget.terrain, typeof(Mesh), false);

			if (myTarget.terrain != null)
				GUI.color = Color.green;
			else {
				isFull = false;
				GUI.color = Color.red;
			}
			GUILayout.Box ("", GUILayout.Width (15), GUILayout.Height (15));
			GUI.color = Color.white;

			GUILayout.EndHorizontal ();

			GUILayout.BeginHorizontal ();
			GUILayout.Label (" Road Mesh", GUILayout.Width (200));
			myTarget.road = (Mesh)EditorGUILayout.ObjectField (myTarget.road, typeof(Mesh), false);

			if (myTarget.road != null)
				GUI.color = Color.green;
			else {
				isFull = false;
				GUI.color = Color.red;
			}
			GUILayout.Box ("", GUILayout.Width (15), GUILayout.Height (15));
			GUI.color = Color.white;
			GUILayout.EndHorizontal ();

			GUILayout.BeginHorizontal ();
			GUILayout.Label (" Collider Mesh", GUILayout.Width (200));
			myTarget.collider = (Mesh)EditorGUILayout.ObjectField (myTarget.collider, typeof(Mesh), false);

			if (myTarget.collider != null)
				GUI.color = Color.green;
			else {
				isFull = false;
				GUI.color = Color.red;
			}
			GUILayout.Box ("", GUILayout.Width (15), GUILayout.Height (15));
			GUI.color = Color.white;
			GUILayout.EndHorizontal ();

			GUILayout.Space (3);
			EditorGUILayout.EndVertical ();

			GUILayout.BeginHorizontal ();
			GUILayout.Space (4);
			GUILayout.Label ("Splat Maps", EditorStyles.boldLabel);
			GUILayout.EndHorizontal ();

			EditorGUILayout.BeginVertical ("Box");
			GUILayout.Space (4);

			GUILayout.BeginHorizontal ();
			GUILayout.Label (" Type", GUILayout.Width (200));
			if (GUILayout.Button ((myTarget.ln == LayersNumber._4Layers) ? " 4 Layers (Switch to 8 Layers)" : " 8 Layers (Switch to 4 Layers)", GUI.skin.GetStyle ("Button"))) {
				if (myTarget.ln == LayersNumber._4Layers) {
					myTarget.ln = LayersNumber._8Layers;
					//myTarget.splatMapSources = new Texture2D[2];
				} else {
					myTarget.ln = LayersNumber._4Layers;
					//myTarget.splatMapSources = new Texture2D[1];
				}

			} 
			GUILayout.EndHorizontal ();

			if (myTarget.ln == LayersNumber._4Layers) {
				GUILayout.BeginHorizontal ();
				GUILayout.Label (" Splat Map", GUILayout.Width (200));
				myTarget.splatMapSources [0] = (Texture2D)EditorGUILayout.ObjectField (myTarget.splatMapSources [0], typeof(Texture2D), false);

				if (myTarget.splatMapSources [0] != null)
					GUI.color = Color.green;
				else {
					isFull = false;
					GUI.color = Color.red;
				}
				GUILayout.Box ("", GUILayout.Width (15), GUILayout.Height (15));
				GUI.color = Color.white;

				GUILayout.EndHorizontal ();
			} else {
				GUILayout.BeginHorizontal ();
				GUILayout.Label (" Splat Map A", GUILayout.Width (200));
				myTarget.splatMapSources [0] = (Texture2D)EditorGUILayout.ObjectField (myTarget.splatMapSources [0], typeof(Texture2D), false);

				if (myTarget.splatMapSources [0] != null)
					GUI.color = Color.green;
				else {
					isFull = false;
					GUI.color = Color.red;
				}
				GUILayout.Box ("", GUILayout.Width (15), GUILayout.Height (15));
				GUI.color = Color.white;
				GUILayout.EndHorizontal ();

				GUILayout.BeginHorizontal ();
				GUILayout.Label (" Splat Map B", GUILayout.Width (200));
				myTarget.splatMapSources [1] = (Texture2D)EditorGUILayout.ObjectField (myTarget.splatMapSources [1], typeof(Texture2D), false);

				if (myTarget.splatMapSources [1] != null)
					GUI.color = Color.green;
				else {
					isFull = false;
					GUI.color = Color.red;
				}
				GUILayout.Box ("", GUILayout.Width (15), GUILayout.Height (15));
				GUI.color = Color.white;
				GUILayout.EndHorizontal ();
			}

			GUILayout.Space (3);
			EditorGUILayout.EndVertical ();



			GUILayout.Space (7);

			if (isFull) {
				GUILayout.BeginHorizontal (myTarget.guiSkin.GetStyle ("Box"));

				GUI.color = greenColor;
				labelStyle = myTarget.guiSkin.GetStyle ("Button");
				if (GUILayout.Button ("Next: Select Biom", labelStyle, GUILayout.ExpandWidth (true), GUILayout.Height (35))) {
					myTarget.Create ();
				}
				GUI.color = guiColor;
				GUILayout.EndHorizontal ();

			}
			break;
		
		case CreationType.Biom:
			{
				GUILayout.Space (3);
				GUILayout.BeginHorizontal ();
				GUILayout.Space (4);
				GUILayout.Label ("Mesh", EditorStyles.boldLabel);
				GUILayout.EndHorizontal ();

				EditorGUILayout.BeginVertical ("Box");
				GUILayout.Space (4);

				width = EditorGUIUtility.currentViewWidth;

				GUILayout.BeginHorizontal ();

				GUI.color = (myTarget.ln == LayersNumber._4Layers) ? Color.gray : Color.white;
				if (GUILayout.Button ("4 Layers", GUILayout.Width (width / 3 - 12), GUILayout.Height (50))) {
					myTarget.ln = myTarget.RT.ln = LayersNumber._4Layers;
					Debug.Log ("4_3");
					myTarget.RT.GetComponent<Renderer> ().sharedMaterial = new Material (Shader.Find ("Relief Pack/Terrain2Geometry4Layers"));
					if (myTarget.RT.currentPresetNumber01 != -1)
						ReliefTerrainEditor.LoadAsset (myTarget.RT.presets [myTarget.RT.currentPresetNumber01].path, myTarget.RT.globalSettingsHolder, myTarget.RT, null);
					myTarget.RT.RefreshAll (myTarget.RT);


				}
			
				GUI.color = (myTarget.ln == LayersNumber._8Layers) ? Color.gray : Color.white;
				if (GUILayout.Button ("8 Layers", GUILayout.Width (width / 3 - 12), GUILayout.Height (50))) {
					Debug.Log ("8_3");
					myTarget.ln = myTarget.RT.ln = LayersNumber._8Layers;
					myTarget.RT.GetComponent<Renderer> ().sharedMaterial = new Material (Shader.Find ("Relief Pack/Terrain2Geometry8Layers"));
					if (myTarget.RT.currentPresetNumber01 != -1 && myTarget.RT.currentPresetNumber02 != -1)
						LoadPreset (myTarget.RT.currentPresetNumber01, myTarget.RT.currentPresetNumber02);
					myTarget.RT.RefreshAll (myTarget.RT);
				}
			
				GUI.color = (myTarget.ln == LayersNumber._Transition) ? Color.gray : Color.white;
				if (GUILayout.Button ("Transition", GUILayout.Width (width / 3 - 12), GUILayout.Height (50))) {
					Debug.Log ("T_3");
					myTarget.ln = myTarget.RT.ln = LayersNumber._Transition;
					myTarget.RT.GetComponent<Renderer> ().sharedMaterial = new Material (Shader.Find ("Relief Pack/Terrain2GeometryTransition"));
					if (myTarget.RT.currentPresetNumber01 != -1 && myTarget.RT.currentPresetNumber02 != -1)
						LoadPreset (myTarget.RT.currentPresetNumber01, myTarget.RT.currentPresetNumber02);
					myTarget.RT.RefreshAll (myTarget.RT);
				}
			
				GUI.color = guiColor;
				GUILayout.FlexibleSpace ();
				GUILayout.EndHorizontal ();
				GUILayout.Space (3);


				if (myTarget.RT == null) {
					GameObject meshTR = GameObject.Find ("Mesh");
					if (meshTR == null) {
						myTarget.creatType = CreationType.Input;
						return;
					} else
						myTarget.RT = meshTR.transform.GetComponent<ReliefTerrain> ();
				}

				int ch = 0;
				scrollPosition = GUILayout.BeginScrollView (scrollPosition, false, false);
				GUILayout.BeginHorizontal ();
				for (int i = 0; i < myTarget.RT.presets.Length; i++) {
					if (ch == 3) {
						GUILayout.EndHorizontal ();
						GUILayout.BeginHorizontal ();
						ch = 0;
					}
				
					GUILayout.BeginVertical ();
					GUILayout.Space (3);
					GUILayout.Label (myTarget.RT.presets [i].name, EditorStyles.boldLabel, GUILayout.Width (70));
				
					if (GUILayout.Button (myTarget.RT.presets [i].ico, GUILayout.Width (70), GUILayout.Height (70))) {
						switch (myTarget.ln) {
						case LayersNumber._4Layers:
							{
								myTarget.RT.ln = myTarget.ln;
								ReliefTerrainEditor.LoadAsset (myTarget.RT.presets [i].path, myTarget.RT.globalSettingsHolder, myTarget.RT, null);
								myTarget.RT.RefreshAll (myTarget.RT);
								myTarget.RT.currentPresetNumber01 = i;
								myTarget.type = (landscapeType)System.Enum.Parse (typeof(landscapeType), myTarget.RT.presets [i].name);
							} 
							break;
						case LayersNumber._8Layers:

							{
								myTarget.RT.ln = myTarget.ln;
								if (((selectedBiome == 0) ? myTarget.RT.currentPresetNumber02 : myTarget.RT.currentPresetNumber01) == -1) {
									ReliefTerrainEditor.LoadAsset (myTarget.RT.presets [i].path, myTarget.RT.globalSettingsHolder, myTarget.RT, null);
									myTarget.RT.RefreshAll (myTarget.RT);
									if (selectedBiome == 0) {
										myTarget.type = (landscapeType)System.Enum.Parse (typeof(landscapeType), myTarget.RT.presets [i].name);
										myTarget.RT.currentPresetNumber01 = i;
									} else {
										myTarget.RT.currentPresetNumber02 = i;
									}
								
								} else {
									if (selectedBiome == 0) {
										myTarget.type = (landscapeType)System.Enum.Parse (typeof(landscapeType), myTarget.RT.presets [i].name);
										myTarget.RT.currentPresetNumber01 = i;
									} else {
										myTarget.RT.currentPresetNumber02 = i;
									}	

									LoadPreset (myTarget.RT.currentPresetNumber01, myTarget.RT.currentPresetNumber02);
								}
							}
							break;
						case LayersNumber._Transition:
							
							{
								myTarget.RT.ln = myTarget.ln;
								if (((selectedBiome == 0) ? myTarget.RT.currentPresetNumber02 : myTarget.RT.currentPresetNumber01) == -1) {
									ReliefTerrainEditor.LoadAsset (myTarget.RT.presets [i].path, myTarget.RT.globalSettingsHolder, myTarget.RT, null);
									myTarget.RT.RefreshAll (myTarget.RT);
									if (selectedBiome == 0) {
										myTarget.type = (landscapeType)System.Enum.Parse (typeof(landscapeType), myTarget.RT.presets [i].name);
										myTarget.RT.currentPresetNumber01 = i;
									} else
										myTarget.RT.currentPresetNumber02 = i;

								} else {
									if (selectedBiome == 0) {
										myTarget.type = (landscapeType)System.Enum.Parse (typeof(landscapeType), myTarget.RT.presets [i].name);
										myTarget.RT.currentPresetNumber01 = i;
									} else
										myTarget.RT.currentPresetNumber02 = i;

									LoadPreset (myTarget.RT.currentPresetNumber01, myTarget.RT.currentPresetNumber02);
								}	
							}
							break;





						}
					
					}
				
				
					GUILayout.EndVertical ();
					GUILayout.Space (4);

					ch++;
				}

				GUILayout.EndHorizontal ();
				GUILayout.EndScrollView ();

			
				EditorGUILayout.BeginVertical ("Box");

				switch (myTarget.ln) {
				case LayersNumber._4Layers:
					{
						GUILayout.Space (10);
						GUILayout.BeginHorizontal ();
						GUILayout.FlexibleSpace ();
						GUILayout.BeginVertical ();
						GUILayout.Label ("Current Biome: ");
						if (myTarget.RT.currentPresetNumber01 == -1) {
							GUILayout.Label (" ", EditorStyles.boldLabel); 
							GUI.color = Color.gray;
							GUILayout.Button ("", GUI.skin.GetStyle ("Button"), GUILayout.Width (100), GUILayout.Height (100));
						} else {
							GUILayout.Label (myTarget.RT.presets [myTarget.RT.currentPresetNumber01].name, EditorStyles.boldLabel); 
							GUI.color = Color.gray;
							GUILayout.Button (myTarget.RT.presets [myTarget.RT.currentPresetNumber01].atlas, GUI.skin.GetStyle ("Button"), GUILayout.Width (100), GUILayout.Height (100));
						}

						GUI.color = Color.white;
						GUILayout.EndVertical ();
						GUILayout.FlexibleSpace ();
						GUILayout.EndHorizontal ();
						GUILayout.Space (10);
					}
					break;

				case LayersNumber._8Layers:
					{
						GUILayout.Space (10);
						GUILayout.BeginHorizontal ();
						GUILayout.FlexibleSpace ();
						GUILayout.BeginVertical ();
						GUILayout.Label ("Main Biome:");
						if (myTarget.RT.currentPresetNumber01 == -1) {
							GUILayout.Label (" ", EditorStyles.boldLabel); 
							GUI.color = (selectedBiome == 0) ? Color.gray : Color.white;
							if (GUILayout.Button ("", GUI.skin.GetStyle ("Button"), GUILayout.Width (100), GUILayout.Height (100)))
								selectedBiome = 1;
						} else {
							GUILayout.Label (myTarget.RT.presets [myTarget.RT.currentPresetNumber01].name, EditorStyles.boldLabel);
							GUI.color = (selectedBiome == 0) ? Color.gray : Color.white;
							if (GUILayout.Button (myTarget.RT.presets [myTarget.RT.currentPresetNumber01].atlas, GUI.skin.GetStyle ("Button"), GUILayout.Width (100), GUILayout.Height (100)))
								selectedBiome = 0;
						}
						GUI.color = Color.white;
						GUILayout.EndVertical ();
						GUILayout.FlexibleSpace ();
						GUILayout.BeginVertical ();
						GUILayout.Label ("Additional Biome:");
						if (myTarget.RT.currentPresetNumber02 == -1) {
							GUILayout.Label (" ", EditorStyles.boldLabel); 
							GUI.color = (selectedBiome == 1) ? Color.gray : Color.white;
							if (GUILayout.Button ("", GUI.skin.GetStyle ("Button"), GUILayout.Width (100), GUILayout.Height (100)))
								selectedBiome = 1;
						} else {
							GUILayout.Label (myTarget.RT.presets [myTarget.RT.currentPresetNumber02].name, EditorStyles.boldLabel); 
							GUI.color = (selectedBiome == 1) ? Color.gray : Color.white;
							if (GUILayout.Button (myTarget.RT.presets [myTarget.RT.currentPresetNumber02].atlas, GUI.skin.GetStyle ("Button"), GUILayout.Width (100), GUILayout.Height (100)))
								selectedBiome = 1;
						}
						GUI.color = Color.white;
						GUILayout.EndVertical ();
						GUILayout.FlexibleSpace ();
						GUILayout.EndHorizontal ();
						GUILayout.Space (10);
					}
					break;
				

				case LayersNumber._Transition:
					{
						GUILayout.Space (10);
						GUILayout.BeginHorizontal ();
						GUILayout.FlexibleSpace ();
						GUILayout.BeginVertical ();
						GUILayout.Label ("Start Biome:");
						if (myTarget.RT.currentPresetNumber01 == -1) {
							GUILayout.Label (" ", EditorStyles.boldLabel); 
							GUI.color = (selectedBiome == 0) ? Color.gray : Color.white;
							if (GUILayout.Button ("", GUI.skin.GetStyle ("Button"), GUILayout.Width (100), GUILayout.Height (100)))
								selectedBiome = 0;
						} else {
							GUILayout.Label (myTarget.RT.presets [myTarget.RT.currentPresetNumber01].name, EditorStyles.boldLabel); 
							GUI.color = (selectedBiome == 0) ? Color.gray : Color.white;
							if (GUILayout.Button (myTarget.RT.presets [myTarget.RT.currentPresetNumber01].atlas, GUI.skin.GetStyle ("Button"), GUILayout.Width (100), GUILayout.Height (100)))
								selectedBiome = 0;
						}
						GUI.color = Color.white;
						GUILayout.EndVertical ();
						GUILayout.FlexibleSpace ();
						GUILayout.BeginVertical ();
						GUILayout.Label ("End Biome:");
						if (myTarget.RT.currentPresetNumber02 == -1) {
							GUILayout.Label (" ", EditorStyles.boldLabel); 
							GUI.color = (selectedBiome == 1) ? Color.gray : Color.white;
							if (GUILayout.Button ("", GUI.skin.GetStyle ("Button"), GUILayout.Width (100), GUILayout.Height (100)))
								selectedBiome = 1;
						} else {
							GUILayout.Label (myTarget.RT.presets [myTarget.RT.currentPresetNumber02].name, EditorStyles.boldLabel); 
							GUI.color = (selectedBiome == 1) ? Color.gray : Color.white;
							if (GUILayout.Button (myTarget.RT.presets [myTarget.RT.currentPresetNumber02].atlas, GUI.skin.GetStyle ("Button"), GUILayout.Width (100), GUILayout.Height (100)))
								selectedBiome = 1;
						}
						GUI.color = Color.white;
						GUILayout.EndVertical ();
						GUILayout.FlexibleSpace ();
						GUILayout.EndHorizontal ();
						GUILayout.Space (10);
					}
					break;

				}
			
				EditorGUILayout.EndVertical ();

				GUI.color = guiColor;

				GUILayout.Space (7);
			
				if (myTarget.RT.globalSettingsHolder.currentPreset != "") {
					GUI.color = greenColor;
					GUILayout.BeginHorizontal (myTarget.guiSkin.GetStyle ("Box"));
					if (GUILayout.Button ("Next: Paint", labelStyle, GUILayout.ExpandWidth (true), GUILayout.Height (35)))
						myTarget.Prepare ();
					GUILayout.EndHorizontal ();
					GUI.color = guiColor;
				}


			}
			break;		
		
		case CreationType.Paint:
			{
				if (myTarget.RT == null) 
				{
					GameObject meshTR = GameObject.Find ("Mesh");
					if (meshTR == null) {
						myTarget.creatType = CreationType.Input;
						return;
					} else
						myTarget.RT = meshTR.transform.GetComponent<ReliefTerrain> ();
				}

				PaintInspector ();

				GUILayout.Space (3);
				GUILayout.BeginHorizontal ();
				GUILayout.Space (4);
				GUILayout.Label ("Snap Road", EditorStyles.boldLabel);
				GUILayout.EndHorizontal ();

				EditorGUILayout.BeginVertical ("Box");
				GUILayout.Space (4);
				labelStyle = myTarget.guiSkin.GetStyle ("Button");

				if (GUILayout.Button ("Autoblend", GUI.skin.GetStyle ("Button"), GUILayout.Height (20))) 
				{
					myTarget.gblend.Reset ();
					myTarget.gblend.Autoblend ();
					myTarget.gblend.Next ();

				}

				if (GUILayout.Button ("Flatten", GUI.skin.GetStyle ("Button"), GUILayout.Height (20))) 
				{
					myTarget.gblend.Reset ();
					myTarget.gblend.Flatten ();
					myTarget.gblend.Next ();
				}

				if (GUILayout.Button ("Tesselate", GUI.skin.GetStyle ("Button"), GUILayout.Height (20))) 
				{
					myTarget.gblend.Reset ();
					myTarget.gblend.Tesselate ();
					myTarget.gblend.Next ();
				}
				GUILayout.Space (4);

				EditorGUILayout.EndVertical ();

				GUILayout.Space (7);
				GUILayout.BeginHorizontal (myTarget.guiSkin.GetStyle ("Box"));
				GUI.color = greenColor;
				if (GUILayout.Button ("Next: Bake", labelStyle, GUILayout.ExpandWidth (true), GUILayout.Height (35))) 
				{
					myTarget.Bake ();
				}
				GUILayout.EndHorizontal ();
				GUI.color = guiColor;


			}
			break;
		case CreationType.Bake:

			if (myTarget.RT == null) {
				GameObject meshTR = GameObject.Find ("Mesh");
				if (meshTR == null) {
					myTarget.creatType = CreationType.Input;
					return;
				} else
					myTarget.RT = meshTR.transform.GetComponent<ReliefTerrain> ();
			}
			GUILayout.Space (7);
			GUILayout.Label ("Thanks for using Prefab Collector");

			break;

		}

	}


	public void LoadPreset(int SourceA, int SourceB)
	{
		PrefabCollector myTarget = (PrefabCollector)target;
		ReliefTerrainPresetHolder preset01 = AssetDatabase.LoadAssetAtPath (myTarget.RT.presets [SourceA].path, typeof(ReliefTerrainPresetHolder)) as ReliefTerrainPresetHolder;
		ReliefTerrainPresetHolder preset02 = AssetDatabase.LoadAssetAtPath (myTarget.RT.presets [SourceB].path, typeof(ReliefTerrainPresetHolder)) as ReliefTerrainPresetHolder;

		ReliefTerrainPresetHolder preset01Copy = UnityEngine.Object.Instantiate (preset01) as ReliefTerrainPresetHolder;
		ReliefTerrainPresetHolder preset02Copy = UnityEngine.Object.Instantiate (preset02) as ReliefTerrainPresetHolder;

		ReliefTerrainPresetHolder mergePreset = ScriptableObject.CreateInstance (typeof(ReliefTerrainPresetHolder)) as ReliefTerrainPresetHolder;
		mergePreset = mergePreset.Merge (preset01Copy.PresetName + preset02Copy.PresetName, preset01Copy, preset02Copy);
		mergePreset.type = "MESH";
		string path = "";
		//ReliefTerrainPresetHolder savedPreset=UnityEngine.Object.Instantiate(mergePreset) as ReliefTerrainPresetHolder;
		if (mergePreset != null) {
			path = "Assets/Editor/" +  preset01Copy.PresetName + preset02Copy.PresetName + ".asset";

			int idx = path.IndexOf ("/Assets/") + 1;
			if (idx > 0)
				path = path.Substring (idx);
			Debug.Log (path);
			if (AssetDatabase.LoadAssetAtPath (path, typeof(ReliefTerrainPresetHolder)) != null)
				AssetDatabase.DeleteAsset (path);
			AssetDatabase.CreateAsset (mergePreset, path);
		} else
			Debug.Log ("Merge preset is null");
		AssetDatabase.Refresh ();
		AssetDatabase.SaveAssets ();

		ReliefTerrainEditor.LoadAsset(path, myTarget.RT.globalSettingsHolder, myTarget.RT, null);
		myTarget.RT.RefreshAll (myTarget.RT);

	}

	public void PaintInspector()
	{
		
		
		PrefabCollector myTarget = (PrefabCollector)target;
	int thumb_size = 32;

		ReliefTerrain _targetRT = myTarget.RT;
		ReliefTerrainGlobalSettingsHolder _target = _targetRT.globalSettingsHolder;


		_target.paint_wetmask = false;
		GUILayout.Space (6);

		Texture2D prev_globalColor = _targetRT.ColorGlobal;

		if (_targetRT.ColorGlobal == null)
			_targetRT.ColorGlobal = _targetRT.controlA;
	//
		PaintMode paintMode = _target.paintMode;
		Tool prev_tool = _targetRT.prev_tool;
	// paint tools
	//
	GUILayout.Space (8);
	EditorGUILayout.LabelField ("Paint tools", EditorStyles.boldLabel);
	EditorGUILayout.BeginVertical ("Box");		
//	//skin_color = GUI.color;
		//GUI.color = greenColor;
	
//	//GUI.color = skin_color;

	bool prev_paint_flag = _target.paint_flag;

		if (!_target.paint_flag) {
			Color c = GUI.color;
			GUI.color = Color.white;
			labelStyle = myTarget.guiSkin.GetStyle ("Button");
			if (GUILayout.Button ("Begin painting", labelStyle, GUILayout.Height(35))) {

				_target.paint_flag = true;
				_target.paint_wetmask = false;
			}
			if (!_targetRT.GetComponent<Collider> () || !_targetRT.GetComponent<Collider> ().enabled)
			{
				_target.paint_flag = false;
		}
			GUI.color = Color.white;
	} 
		else
			if (_target.paint_flag)
			{
		Color c = GUI.color;
				GUI.color = Color.white;
				labelStyle = myTarget.guiSkin.GetStyle ("Button");
				if (GUILayout.Button ("End painting", labelStyle, GUILayout.Height(35))) {
			_target.paint_flag = false;

		}
				GUI.color = Color.white;
	}
	if (!prev_paint_flag && _target.paint_flag) 
	{
		UnityEditor.Tools.current = Tool.View;
		ReliefTerrain._SceneGUI = new SceneView.OnSceneFunc (CustomOnSceneGUI);
		SceneView.onSceneGUIDelegate += ReliefTerrain._SceneGUI;
	} 
		else 
	if (prev_paint_flag && !_target.paint_flag) {
		UnityEditor.Tools.current = prev_tool;
		SceneView.onSceneGUIDelegate -= ReliefTerrain._SceneGUI;
	}
	if (prev_paint_flag != _target.paint_flag)
		EditorUtility.SetDirty (target);
	if (_target.paint_flag) {
		if (!_targetRT.GetComponent<Collider> () || !_targetRT.GetComponent<Collider> ().enabled)
			EditorGUILayout.HelpBox ("Object doesn't have collider (necessary for painting).", MessageType.Error, true);

		string[] modes;
		modes = new string[3] { "Layer", "Color", "Extrude Height" };
		_target.cut_holes = false;

		_target.paintMode = (PaintMode)GUILayout.SelectionGrid ((int)_target.paintMode, modes, modes.Length);
		EditorUtility.SetDirty (target);

		switch (_target.paintMode) {
		case PaintMode.extrude:
			EditorGUILayout.HelpBox ("Hold SHIFT while painting to apply eraser.", MessageType.Info, true);
			EditorGUILayout.BeginHorizontal ();
			if (GUILayout.Button ("First")) {
				_target.isFirst = true;
			}
			if (GUILayout.Button ("Second")) {
				_target.isFirst = false;
			}
			EditorGUILayout.EndHorizontal ();	
			break;
		case PaintMode.color:
			{
				EditorGUILayout.BeginVertical ("Box");
//				skin_color = GUI.color;
				GUI.color = Color.white;
				if (_target.paintColorSwatches != null && _target.paintColorSwatches.Length > 0)
					EditorGUILayout.LabelField ("Color swatches", EditorStyles.boldLabel);
//				GUI.color = skin_color;
				EditorGUILayout.BeginHorizontal ();
				thumb_size = 16;
				for (int n = 0; _target.paintColorSwatches != null && n < _target.paintColorSwatches.Length; n++) {
					Color ccol = GUI.contentColor;
					GUI.contentColor = _target.paintColorSwatches [n];
					if (GUILayout.Button (_target.get_dumb_tex (), "Label", GUILayout.Width (thumb_size), GUILayout.Height (thumb_size))) {
						_target.paintColor = _target.paintColorSwatches [n];
					}
					GUI.contentColor = ccol;
				}
				EditorGUILayout.EndHorizontal ();	


				EditorGUILayout.BeginHorizontal ();
				EditorGUI.BeginDisabledGroup (_target.paintColorSwatches == null || _target.paintColorSwatches.Length == 12);
				if (GUILayout.Button ("Add swatch")) {
					Color[] ns = new Color[_target.paintColorSwatches.Length + 1];
					for (int k = 0; k < _target.paintColorSwatches.Length; k++)
						ns [k] = _target.paintColorSwatches [k];
					ns [ns.Length - 1] = new Color (_target.paintColor.r, _target.paintColor.g, _target.paintColor.b, 1);
					_target.paintColorSwatches = ns;
				}
				EditorGUI.EndDisabledGroup ();
				EditorGUI.BeginDisabledGroup (_target.paintColorSwatches == null || _target.paintColorSwatches.Length == 0);
				if (GUILayout.Button ("Remove swatch")) {
					Color[] ns = new Color[_target.paintColorSwatches.Length - 1];
					for (int k = 0; k < ns.Length; k++)
						ns [k] = _target.paintColorSwatches [k];
					_target.paintColorSwatches = ns;
				}
				EditorGUI.EndDisabledGroup ();
				EditorGUILayout.EndHorizontal ();								

				_target.paintColor = EditorGUILayout.ColorField ("Paint color", _target.paintColor);
				_target.paintColor2 = EditorGUILayout.ColorField ("Paint color", _target.paintColor2);
				_target.preserveBrightness = EditorGUILayout.Toggle ("Preserve luminosity", _target.preserveBrightness);
				EditorGUILayout.EndVertical ();
			}
			break;
		case PaintMode.layer:
			{
				EditorGUILayout.BeginVertical ("Box");
//				skin_color = GUI.color;
					GUI.color = Color.white;
				//if (_target.paintColorSwatches!=null && _target.paintColorSwatches.Length>0) EditorGUILayout.LabelField("Color swatches", EditorStyles.boldLabel);
//				GUI.color = skin_color;
				GUISkin gs = EditorGUIUtility.GetBuiltinSkin (EditorSkin.Inspector);
				RectOffset ro1 = gs.label.padding;
				RectOffset ro2 = gs.label.margin;
				gs.label.padding = new  RectOffset (0, 0, 0, 0);
				gs.label.margin = new  RectOffset (3, 3, 3, 3);


				int per_row = Mathf.Max (8, (Screen.width) / thumb_size - 1);
				thumb_size = 50;//(Screen.width-50-2*per_row)/per_row;
				Color ccol = GUI.contentColor;
				for (int n = 0; n < _target.numLayers; n++) 
					{
					if ((n % per_row) == 0)
						EditorGUILayout.BeginHorizontal ();
						Color bcol = Color.white;//GUI.backgroundColor;
					if (n == _target.selectedLayer) 
						{
							GUI.contentColor = Color.white;
							GUI.backgroundColor = Color.gray;
						EditorGUILayout.BeginHorizontal ("Box");

						if (_target.splats [n]) 
						{
							#if !UNITY_3_5
							GUILayout.Label ((Texture2D)AssetPreview.GetAssetPreview (_target.splats [n]), GUILayout.Width (thumb_size - 8), GUILayout.Height (thumb_size - 8));
							#else
							GUILayout.Label((Texture2D)EditorUtility.GetAssetPreview(_target.splats[n]), GUILayout.Width(thumb_size-8), GUILayout.Height(thumb_size-8));
							#endif
						} 
						else 
								
							{
							GUILayout.Label (" ", GUILayout.Width (thumb_size - 8), GUILayout.Height (thumb_size - 8));
						}
					} 
						else
					{
							GUI.contentColor = Color.white;
						if (_target.splats [n]) {
							#if !UNITY_3_5
							if (GUILayout.Button ((Texture2D)AssetPreview.GetAssetPreview (_target.splats [n]), "Label", GUILayout.Width (thumb_size), GUILayout.Height (thumb_size))) {
							#else
							if (GUILayout.Button((Texture2D)EditorUtility.GetAssetPreview(_target.splats[n]), "Label", GUILayout.Width(thumb_size), GUILayout.Height(thumb_size))) {
							#endif
								_target.selectedLayer = n;
							}
						} else {
							if (GUILayout.Button (" ", "Label", GUILayout.Width (thumb_size), GUILayout.Height (thumb_size))) {
								_target.selectedLayer = n;
							}
						}
					}
					if (n == _target.selectedLayer) {
						EditorGUILayout.EndHorizontal ();
							GUI.backgroundColor = Color.gray;
					}
					if ((n % per_row) == (per_row - 1) || n == _target.numLayers - 1)
						EditorGUILayout.EndHorizontal ();
				}


				EditorGUILayout.EndVertical ();
				
					GUI.color = Color.white;
					GUI.backgroundColor = Color.white;
					bool isDoubleString = true;
				GUILayout.Label (" Opacity", EditorStyles.boldLabel);
				GUILayout.BeginHorizontal();
					for (int i=0; i<11; i++)
					{
						if (i==myTarget.paintOpacity)
							GUI.contentColor = Color.gray;
						else
							GUI.contentColor = Color.white;
						float result = (float)i/10f;

						float width = EditorGUIUtility.currentViewWidth;

						if ((width < (i * 50f + 10f))  && isDoubleString)
						{
							isDoubleString= false;
							GUILayout.EndHorizontal();
							GUILayout.BeginHorizontal();
						}

						Color32[] pix = myTarget.smearIco.GetPixels32 ();
						for (int j=0; j<pix.Length; j++)
							pix[j].a = (byte)(result * 255);
						Texture2D ico = new Texture2D (myTarget.smearIco.width, myTarget.smearIco.height);
						ico.SetPixels32 (pix);
						ico.Apply ();

						if (GUILayout.Button(ico, GUILayout.Width(40), GUILayout.Height(40)))
						{
							myTarget.paintOpacity = i;
							_target.colorOpacity = result; 
						}
					}
				GUILayout.EndHorizontal();
				_target.colorOpacity = EditorGUILayout.Slider (_target.colorOpacity, 0.0f, 1.0f);
				float other = 1.0f - _target.colorOpacity;

				switch (_target.selectedLayer) {
				case 0:
					_target.paintColor = new Color (_target.colorOpacity, other, other, other);
					_target.isFirst = true;
					break;
				case 1:
					_target.paintColor = new Color (other, _target.colorOpacity, other, other);
					_target.isFirst = true;
					break;
				case 2:
					_target.paintColor = new Color (other, other, _target.colorOpacity, other);
					_target.isFirst = true;
					break;
				case 3:
					_target.paintColor = new Color (other, other, other, _target.colorOpacity);
					_target.isFirst = true;
					break;
				case 4:
					_target.paintColor = new Color (_target.colorOpacity, other, other, other);
					_target.isFirst = false;
					break;
				case 5:
					_target.paintColor = new Color (other, _target.colorOpacity, other, other);
					_target.isFirst = false;
					break;
				case 6:
					_target.paintColor = new Color (other, other, _target.colorOpacity, other);
					_target.isFirst = false;
					break;
				case 7:
					_target.isFirst = false;
					_target.paintColor = new Color (other, other, other, _target.colorOpacity);
					break;
				}
			}
			break;
		}
//		skin_color = GUI.color;
			GUILayout.Space(8);
			GUI.color = Color.white;
		EditorGUILayout.LabelField (" Layer properties", EditorStyles.boldLabel);
//		GUI.color = skin_color;

		Texture2D tmp_tex = _targetRT.tmp_globalColorMap;
		if (tmp_tex && (tmp_tex.format != TextureFormat.Alpha8 && tmp_tex.format != TextureFormat.ARGB32)) {
			EditorGUILayout.HelpBox ("Global colormap need to be readable and uncompressed for painting.", MessageType.Error, true);
		}
		GUILayout.BeginHorizontal ();
		GUILayout.Label ("Area size", EditorStyles.label);
		_target.paint_size = EditorGUILayout.Slider (_target.paint_size, 0.1f, 20);
		GUILayout.EndHorizontal ();	
		GUILayout.BeginHorizontal ();
		GUILayout.Label ("Area smoothness", EditorStyles.label);
		_target.paint_smoothness = EditorGUILayout.Slider (_target.paint_smoothness, 0.001f, 1);
		GUILayout.EndHorizontal ();
		GUILayout.BeginHorizontal ();
		GUILayout.Label ("Opacity", EditorStyles.label);
		_target.paint_opacity = EditorGUILayout.Slider (_target.paint_opacity, 0, 1);
		GUILayout.EndHorizontal ();	
		GUILayout.Space (10);

			GUILayout.BeginHorizontal();
			if (GUILayout.Button("Clean"))
			{
				Color[] cols = _targetRT.controlA.GetPixels(0);
				for (int i=0; i<cols.Length; i++)
					cols[i] = new Color32(90,0,0,0);
				_targetRT.controlA.SetPixels(cols);
				_targetRT.controlA.Apply(false,false);

				if (_targetRT.ln != LayersNumber._4Layers)
				{
					Color[] cols2 = _targetRT.controlB.GetPixels(0);
					for (int i=0; i<cols2.Length; i++)
						cols2[i] = new Color32(0,0,0,0);
					_targetRT.controlB.SetPixels(cols2);
					_targetRT.controlB.Apply(false,false);
				}


				
				




			}
	
	}
	EditorGUILayout.EndVertical ();	
	}

	public void CustomOnSceneGUI (SceneView sceneview)
	{
		
		PrefabCollector targetCol = (PrefabCollector)target;
		if (targetCol.RT == null)
			return;
			ReliefTerrain _targetRT = targetCol.RT;
			ReliefTerrainGlobalSettingsHolder _target = _targetRT.globalSettingsHolder;
		
		bool control_down_flag = _targetRT.control_down_flag;

		EditorWindow currentWindow = EditorWindow.mouseOverWindow;
		if (!currentWindow)
			return;

		Event current = Event.current;

		if (current.alt) {
			return;
		}		
		if (Event.current.button == 1) {
			HandleUtility.AddDefaultControl (GUIUtility.GetControlID (FocusType.Passive));
			return;
		}

		bool paintOff_flag = (UnityEditor.Tools.current != Tool.View);
		if (_target.paint_wetmask) {
			paintOff_flag = paintOff_flag;
		} else {
			paintOff_flag = paintOff_flag;
		}
		if (paintOff_flag) {
			_target.paint_flag = false;
			SceneView.onSceneGUIDelegate -= ReliefTerrain._SceneGUI;
			EditorUtility.SetDirty (target);
			return;
		}		

		if (current.type == EventType.layout) {
			HandleUtility.AddDefaultControl (GUIUtility.GetControlID (FocusType.Passive));
			return;
		}

		switch (current.type) {
		case EventType.keyDown:
			if (current.keyCode == KeyCode.Escape) {
				_target.paint_flag = false;
				UnityEditor.Tools.current = _targetRT.prev_tool;
				SceneView.onSceneGUIDelegate -= ReliefTerrain._SceneGUI;
				EditorUtility.SetDirty (target);
			}
			break;
		}

		if (current.control) {
			if (current.type == EventType.mouseMove) {
				if (control_down_flag) {
					control_down_flag = false;
					EditorUtility.SetDirty (target);
				}
			}
			return;
		}
		control_down_flag = true;

		switch (current.type) {
		case EventType.mouseDown:
			_targetRT.get_paint_coverage ();
			// Debug.Log(""+cover_verts_num + "  "+ paintHitInfo_flag + _target.prepare_tmpColorMap());
			if (_targetRT.paintHitInfo_flag) {
				if (_targetRT.prepare_tmpTexture (!_target.paint_wetmask)) {
					#if UNITY_3_5 || UNITY_4_0 || UNITY_4_1 || UNITY_4_2
					Undo.RegisterUndo(_targetRT.tmp_globalColorMap, "Geometry Blend Edit");
					#else
					Undo.RegisterCompleteObjectUndo (_targetRT.tmp_globalColorMap, "Geometry Blend Edit");
					#endif
					_targetRT.modify_blend (!current.shift);

				}
			} else {
				_target.undo_flag = true;
			}
			current.Use ();
			break;
		case EventType.mouseDrag:
			_targetRT.get_paint_coverage ();
			if (_targetRT.paintHitInfo_flag) {
				if (_target.undo_flag) {
					if (_targetRT.prepare_tmpTexture (!_target.paint_wetmask)) {
						#if UNITY_3_5 || UNITY_4_0 || UNITY_4_1 || UNITY_4_2
						Undo.RegisterUndo(_targetRT.tmp_globalColorMap, "Geometry Blend Edit");
						#else
						Undo.RegisterCompleteObjectUndo (_targetRT.tmp_globalColorMap, "Geometry Blend Edit");
						#endif
					}
					_target.undo_flag = false;
				}
			}
			if (_targetRT.paintHitInfo_flag) {
				if (_targetRT.prepare_tmpTexture (!_target.paint_wetmask)) {
					_targetRT.modify_blend (!current.shift);
					//current.Use();
				}
			}
			break;
		case EventType.mouseMove:
			_targetRT.get_paint_coverage ();
			break;
		}



		if (_targetRT.paintHitInfo_flag) {
			bool upflag = _target.paint_wetmask ? !current.shift : current.shift;

			if (upflag) {

				Handles.color = new Color (1, 0, 0, Mathf.Max (0.1f, _target.paint_opacity * 0.5f));
				Handles.DrawWireDisc (_targetRT.paintHitInfo.point, _targetRT.paintHitInfo.normal, _target.paint_size);
				Handles.color = new Color (1, 0, 0, Mathf.Max (0.6f, _target.paint_opacity));
				Handles.DrawWireDisc (_targetRT.paintHitInfo.point, _targetRT.paintHitInfo.normal, _target.paint_size * Mathf.Max (0.3f, 1 - _target.paint_smoothness));
			} else {
				Handles.color = new Color (0, 1, 0, Mathf.Max (0.1f, _target.paint_opacity * 0.5f));
				Handles.DrawWireDisc (_targetRT.paintHitInfo.point, _targetRT.paintHitInfo.normal, _target.paint_size);
				Handles.color = new Color (0, 1, 0, Mathf.Max (0.6f, _target.paint_opacity));
				Handles.DrawWireDisc (_targetRT.paintHitInfo.point, _targetRT.paintHitInfo.normal, _target.paint_size * Mathf.Max (0.3f, 1 - _target.paint_smoothness));
			}			
		}		
		if (current.shift)
			current.Use ();
			
	}
	*/
}
