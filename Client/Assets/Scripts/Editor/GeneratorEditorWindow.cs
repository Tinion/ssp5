﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Dest.Math;

#if UNITY_EDITOR
using UnityEditor;
#endif

[System.Flags]
public enum MapEditorMode
{
	None = 0,
	ActiveSettings = 1,
	ActiveRoadSettings = 2,
	selectOne = 3,
	selectTwo = 4,
	selectThree = 5,
}





public class GeneratorEditorWindow : EditorWindow
{
	public Vector2 scrollPosition = Vector2.zero;
	public Vector2 scrollPosition2 = Vector2.zero;
	MapDataSource current;
	BiomesSources currentBiomesSources;
	UnitsSources currentUnitsSources;
	BiomesSource currentBiomesSource;
	MiscSources currentMiscSource;
	Generator generator;
	MapEditorMode currentMode = MapEditorMode.None;
	string newBiomName = "New Biome";
	bool isNewBiomPreferences = false;
	bool isLeft = false;
	bool isRight = false;
	int newBiomeIcon = 0;
	bool isMapOpen = false;
	public Vector2 delta = Vector2.zero;
	bool isDragAll = true;
	int currentWay = 0;
	int currentNumber = 0;
	int currentRoadNumber = 0;
	Vector2 currentPos = Vector2.zero;
	int roadTypeEditor = 0;
	GUISkin skin;
	int type = 0;
	int currentElement = 0;
	bool isConnectionMode = false;
	string[] maps = { "City01", "Desert01", "Grass01", "Standart" };

	WayBlock[] wayBlocks;

	[MenuItem ("Window/Map Editor")]
	static void CreateWindow ()
	{
		GeneratorEditorWindow window = (GeneratorEditorWindow)EditorWindow.GetWindow (typeof(GeneratorEditorWindow));
	}

	void OnGUI ()
	{
		if (Event.current.button != 0 || Event.current.type == EventType.MouseUp) {
//			Debug.Log ("FF");
			isDragAll = true;
		}
		GUI.skin = skin;
		Side ();
        
        
		switch (type) {
		case 0:
			Units ();
			break;

		case 1:
			{
				if (!isMapOpen)
					Maps ();
				else
					Roads ();
				switch (currentMode) {
				case MapEditorMode.None:
					break;
				case MapEditorMode.ActiveSettings:
					ShowSettings (currentNumber);
					break;
				case MapEditorMode.ActiveRoadSettings:
					ShowRoadSettings (currentNumber, currentRoadNumber);
					break;
				}
			}
			break;

		case 2:
			Miscs ();
			break;

		case 3:
			Biomes ();
			break;
		}
        

		switch (Event.current.type) {
		case EventType.MouseDrag:
			delta = Event.current.delta;
			if (isDragAll)
				moveAllWays ();
			break;
		}
        
		if (GUI.changed) {
			Repaint ();
		}
        
        

	}

	void moveAllWays ()
	{
		if (current.ways != null) {
			if (current.ways.Count > 0) {
				for (int i = 0; i < current.ways.Count; i++) {
					current.ways [i].pos = new Vector2 (current.ways [i].pos.x + delta.x, current.ways [i].pos.y + delta.y);
					GUI.changed = true;
				}
			}
		}

		//GUI.skin.GetStyle ("Field").contentOffset = new Vector2 (GUI.skin.GetStyle ("Field").contentOffset.x + delta.x, GUI.skin.GetStyle ("Field").contentOffset.y + delta.y);
		//currentPos = new Vector2 (currentPos.x + delta.x, currentPos.y + delta.y);
	}


	void Side ()
	{
		GUI.Box (new Rect (position.width - 300.0f, 5.0f, 295, position.height / 2 - 10.0f), "");
		GUILayout.BeginArea (new Rect (position.width - 295.0f, 10.0f, 295, position.height / 2 - 15.0f));

		if (skin == null)
			LoadData ();

		GUILayout.BeginHorizontal ();
		if (GUILayout.Button ("", GUILayout.Width (140), GUILayout.Height (60)))
			type = 3;
		if (generator != null)
		if (generator.buttonIcons != null)
			GUI.DrawTexture (new Rect (5, 5, 133, 52), generator.buttonIcons [0], ScaleMode.ScaleAndCrop, true);
		GUI.Label (new Rect (0, 0, 140, 60), "Biomes", skin.GetStyle ("toggle"));
		if (GUILayout.Button ("", GUILayout.Width (140), GUILayout.Height (60)))
			type = 1;
		GUILayout.EndHorizontal ();
		if (generator != null)
		if (generator.buttonIcons != null)
			GUI.DrawTexture (new Rect (145, 5, 133, 52), generator.buttonIcons [1], ScaleMode.ScaleAndCrop, true);
		GUI.Label (new Rect (135, 0, 140, 60), "Maps", skin.GetStyle ("toggle"));
		GUILayout.BeginHorizontal ();
		if (GUILayout.Button ("Units", GUILayout.Width (140), GUILayout.Height (60)))
			type = 0;
		if (GUILayout.Button ("Barrage", GUILayout.Width (140), GUILayout.Height (60)))
			type = 2;
		GUILayout.EndHorizontal ();

		GUILayout.BeginArea (new Rect (0.0f, 130.0f, 285, position.height / 2 - 180.0f));
		GUI.Box (new Rect (0.0f, 0.0f, 285, position.height / 2 - 180.0f), "");


		switch (type) {
		case 0:
			if (GUILayout.Button ("Diagram", GUILayout.Width (140), GUILayout.Height (60)))
				type = 3;
			break;
		case 1:
			{
				//if (currentBiomesSources == null)
				//	LoadData ();
				//currentBiomesSource = currentBiomesSources.biomesSource[0];
			}
			break;
		case 2:
			{
			}
			break;
		case 3:
			{
			}
			break;
		}

		GUILayout.EndArea ();
		GUILayout.EndArea ();

	}

	[System.Serializable]
	public class WayBlock
	{
		public List<WayElement> activeSeason;
		public int forkSeason;
		public int forkRoad;
		public float size;
	}

	[System.Serializable]
	public class WayElement
	{
		public int activeSeason;
		public Color color;
		public List<WayRoad> activeRoad;
	}

	[System.Serializable]
	public class WayRoad
	{
		public int roadNumber;
		public float size;

		public WayRoad(int _roadNumber, float _size)
		{
			roadNumber = _roadNumber;
			size = _size;
		}
	}

	void CleanData ()
	{
		
		if (current != null) {
			current.activeSeasons.Clear ();
			current.ways.Clear ();
			current.ways.Add (new Ways (new Vector2 (50, 200), 0, 0, 0, 0));
		}



		currentMode = MapEditorMode.None;
		isConnectionMode = false;
	}


	void LoadData ()
	{
		//Debug.Log ("LoadData");
		if (generator == null)
			generator = FindObjectOfType<Generator> ();
		if (currentBiomesSources == null)
			currentBiomesSources = AssetDatabase.LoadAssetAtPath ("Assets/Resources/biomes.asset", typeof(BiomesSources)) as BiomesSources;
		generator.mapData = AssetDatabase.LoadAssetAtPath ("Assets/Resources/mapsData.asset", typeof(MapsData)) as MapsData;
		generator.unitsSources = AssetDatabase.LoadAssetAtPath ("Assets/Resources/unitData.asset", typeof(UnitsSources)) as UnitsSources;
		generator.miscSources = AssetDatabase.LoadAssetAtPath ("Assets/Resources/miscData.asset", typeof(MiscSources)) as MiscSources;
		if (generator != null)
		if (generator.mapData != null)
		if (generator.mapData.maps != null)
		if (current == null)
			current = generator.mapData.maps [0];

		if (skin == null)
			skin = AssetDatabase.LoadAssetAtPath ("Assets/Resources/MapEditorGuiSkin.guiskin", typeof(GUISkin)) as GUISkin;

		currentMode = MapEditorMode.None;
	}

	void ShowSettings (int number)
	{
		GUI.Box (new Rect (position.width - 300.0f, position.height / 2, 295, position.height / 2 - 5.0f), "");
		GUILayout.BeginArea (new Rect (position.width - 295.0f, Mathf.CeilToInt (position.height / 2) + 5.0f, 270, position.height / 2 - 50.0f));

		if (current.activeSeasons != null)
		if (current.activeSeasons.Count > number) {
			current.activeSeasons [number].name = GUILayout.TextField (current.activeSeasons [number].name);
			GUILayout.Space (5);
			GUILayout.BeginHorizontal ();
			if (GUILayout.Button ("-", GUILayout.Width (20), GUILayout.Height (20)))
			if (current.activeSeasons [number].roadElements.Count > 0)
				current.activeSeasons [number].roadElements.RemoveAt (current.activeSeasons [number].roadElements.Count - 1);         
			GUILayout.Label (current.activeSeasons [number].roadElements.Count.ToString ());
			if (GUILayout.Button ("+", GUILayout.Width (20), GUILayout.Height (20)))
				current.activeSeasons [number].roadElements.Add (new RoadElement (RoadElementType.Default));
			GUILayout.EndHorizontal ();
			GUILayout.Space (5);
			ShowRoadMagazine (ref current.activeSeasons [number].roadMagazine);

			GUILayout.EndArea ();
		}
	}


	public void ShowRoadMagazine(ref List<RoadSourceData> roadSourceData)
	{
		scrollPosition2 = GUILayout.BeginScrollView (scrollPosition2);
		GUIStyle myStyle = new GUIStyle ();
		myStyle.fontSize = 11;
		myStyle.alignment = TextAnchor.UpperLeft;

		myStyle.normal.textColor = Color.black;

		for (int i = 0; i < roadSourceData.Count; i++) 
		{
			RoadSourceData roadSource = roadSourceData [i];

			GUILayout.BeginHorizontal ();
			roadSource.isActive = EditorGUILayout.Toggle (roadSource.isActive, GUILayout.Width (15), GUILayout.Height (20));
			GUILayout.Space (5);

			if (roadSource.isActive)
				roadSource.coefficient = EditorGUILayout.Slider (roadSource.coefficient, 0, 5.0f);

			GUILayout.Label (roadSource.name, myStyle, GUILayout.Width (120), GUILayout.Height (20));
			GUILayout.EndHorizontal ();
		}

		GUILayout.EndScrollView ();
	}

	void SwitchRoadToType (int number, int roadNumber, RoadElementType type, int other)
	{
		RoadElement re = current.activeSeasons [number].roadElements [roadNumber];
		if (re == null)
			return;
		
		RoadElementType rstl = re.roadSourceType;
		re.roadSourceType = type;

		if (rstl != re.roadSourceType)
		{
			
			switch (re.roadSourceType) {
			case RoadElementType.Default:
				break;
			case RoadElementType.Custom:
				if (re.magazine == null || re.magazine.Count == 0) 
				{
					re.magazine = new List<RoadSourceData> ();
					for (int i = 0; i < current.activeSeasons [number].roadMagazine.Count; i++) 
					{
						RoadSourceData rs = new RoadSourceData ();
						RoadSourceData old = current.activeSeasons [number].roadMagazine [i];
						rs.name = old.name;
						rs.roadSourceType = old.roadSourceType;
						rs.coefficient = old.coefficient;
						rs.isActive = old.isActive;
						re.magazine.Add (rs);
					}
					re.roadSourceType = RoadElementType.Custom;
				}
				else
					re.roadSourceType = RoadElementType.Custom;
				break;
			case RoadElementType.Special:
				break;
			case RoadElementType.Fork:
				if (other == 0) {
					float curX = CalculateX (number) + 70 + 4 + 24 * roadNumber;
					float curY = CalculateY (number) + 23;
					current.ways.Add (new Ways (new Vector2 (curX + 11 - 35, curY - 131), current.ways.Count, number, roadNumber, 1));
					re.fork = RoadSourceType.ForkLeft;
					re.leftWay = current.ways.Count - 1;
					CalculateYApply (current.ways.Count - 1, true);
				}

				if (other == 1) {
					float curX = CalculateX (number) + 70 + 4 + 24 * roadNumber;
					float curY = CalculateY (number) + 23;
					current.ways.Add (new Ways (new Vector2 (curX + 11 - 35, curY + 89), current.ways.Count, number, roadNumber, 2));                     
					re.fork = RoadSourceType.ForkRight;
					re.rightWay = current.ways.Count - 1;
					CalculateYApply (current.ways.Count - 1, false);
				}
				break;
			case RoadElementType.Connection:
				break;
			}
		}

		if (re.roadSourceType == RoadElementType.Connection && rstl == RoadElementType.Connection) {
			switch (re.connect) {
			case RoadSourceType.ForkLeftReverse:
				{
					int numb = GetLastActiveNumber (re.leftWay);
					current.activeSeasons [numb].endType = ActiveEndType.final;

					break;
				}
			case RoadSourceType.ForkRightReverse:
				{
					int numb = GetLastActiveNumber (re.rightWay);
					current.activeSeasons [numb].endType = ActiveEndType.final;

					break;
				}
			case RoadSourceType.ForkBothReverse:
				{
					int numb = GetLastActiveNumber (re.leftWay);
					current.activeSeasons [numb].endType = ActiveEndType.final;
					numb = GetLastActiveNumber (re.rightWay);
					current.activeSeasons [numb].endType = ActiveEndType.final;
					break;
				}
			}
			re.connect = RoadSourceType.None;
			re.roadSourceType = RoadElementType.Default;

		}

		if (re.roadSourceType != RoadElementType.Fork && rstl == RoadElementType.Fork) {
			int deleteWay = -1;
			switch (re.fork) {
			case RoadSourceType.ForkLeft:
				deleteWay = re.leftWay;
				break;
			case RoadSourceType.ForkRight:
				deleteWay = re.rightWay;
				break;
			case RoadSourceType.ForkBoth:
				deleteWay = re.leftWay;
				break;
			}
			Debug.Log (deleteWay);
			int numb = -1;
			numb = GetLastActiveNumber (deleteWay);
			Debug.Log (numb);
			if (numb < 0)
				current.ways.Remove (current.ways [deleteWay]);
			else {
				List<int> variants = new List<int> ();

				for (int i = 0; i < current.activeSeasons [current.ways [deleteWay].seasonMaster].roadElements.Count; i++) {
					if (current.activeSeasons [current.ways [deleteWay].seasonMaster].roadElements [i] != re && current.activeSeasons [current.ways [deleteWay].seasonMaster].roadElements [i].roadSourceType == RoadElementType.Default)
						variants.Add (i);	
				}

				int min = 100;
				int selectNear = -1;
				for (int i = 0; i < variants.Count; i++) {
					int calc = Mathf.Abs (variants [i] - current.ways [deleteWay].roadMaster);

					if (calc < min) {
						min = calc;
						selectNear = i;
					}
				}
				int selectFinal;
				if (selectNear >= 0)
					selectFinal = selectNear;
				else
					selectFinal = Random.Range (0, current.activeSeasons [current.ways [deleteWay].seasonMaster].roadElements.Count);

				current.ways [deleteWay].roadMaster = selectFinal;
				current.activeSeasons [current.ways [deleteWay].seasonMaster].roadElements [selectFinal].fork = re.fork;
				current.activeSeasons [current.ways [deleteWay].seasonMaster].roadElements [selectFinal].roadSourceType = RoadElementType.Fork;
				switch (re.fork) {
				case RoadSourceType.ForkLeft:
					current.activeSeasons [current.ways [deleteWay].seasonMaster].roadElements [selectFinal].leftWay = deleteWay;
					break;
				case RoadSourceType.ForkRight:
					current.activeSeasons [current.ways [deleteWay].seasonMaster].roadElements [selectFinal].rightWay = deleteWay;
					break;
				}

			}
			re.fork = RoadSourceType.None;

		}
	}

	void ShowRoadSettings (int number, int roadNumber)
	{
		

		if (current == null) {
			LoadData ();
			return;
		}
			
		
		if (current.activeSeasons == null) {
			LoadData ();
			return;
		}

		if (current.activeSeasons [number].roadElements == null) {
			LoadData ();
			return;
		}

		RoadElement re = current.activeSeasons [number].roadElements [roadNumber];
		if (re == null)
			return;

		GUI.Box (new Rect (position.width - 300.0f, position.height / 2, 290, position.height / 2 - 10.0f), "");
		float x = position.width - 295.0f;
		float y = Mathf.CeilToInt (position.height / 2) + 5.0f;
		GUILayout.BeginArea (new Rect (x, y, 270, position.height / 2 - 10.0f));

		//GUI.Box (new Rect (80, 0, 110, 30.0f), "");
		RoadElementType rstl = re.roadSourceType;
		GUIStyle myStyle = new GUIStyle ();
		myStyle.fontSize = 14;
		myStyle.normal.textColor = Color.white;
		myStyle.alignment = TextAnchor.MiddleCenter;

		GUILayout.Label (re.roadSourceType.ToString () + " Road", GUILayout.Height (40.0f));
			

		switch (re.roadSourceType)
		{
		case RoadElementType.Default:
			break;
		case RoadElementType.Custom:
			{
				GUILayout.BeginArea (new Rect (0, 40, 270, position.height / 2 - 50.0f));
				ShowRoadMagazine (ref re.magazine);
				GUILayout.EndArea ();
			}
			break;
		case RoadElementType.Special:
			{
				GUILayout.BeginArea (new Rect (0, 40, 270, position.height / 2 - 50.0f));
				int count = currentBiomesSources.biomesSource [current.activeSeasons [number].biomeNumber].roadSource.Count;
				string[] roadDataString = new string[count];
				for (int i = 0; i < count; i++)
					roadDataString [i] = currentBiomesSources.biomesSource [current.activeSeasons [number].biomeNumber].roadSource [i].name;
				re.special = EditorGUILayout.Popup (re.special, roadDataString);
				GUILayout.EndArea ();
			}
			break;
		case RoadElementType.Fork:
			{
				isLeft = false;
				isRight = false;

				for (int i = 0; i < currentBiomesSources.biomesSource [current.activeSeasons [number].biomeNumber].crossingPower.Length; i++) {
					if (currentBiomesSources.biomesSource [current.activeSeasons [number].biomeNumber].crossingPower [i].toLeft)
						isLeft = true;
					if (currentBiomesSources.biomesSource [current.activeSeasons [number].biomeNumber].crossingPower [i].toRight)
						isRight = true;
				}
				ShowFork (number, roadNumber);
			}
			break;
		case RoadElementType.Connection:
			break;
		}

		GUILayout.EndArea ();
        
	}



    
	void OnEnable ()
	{
		Debug.Log ("2");
		LoadData ();
	}

	void ShowEnemySeason (EnemySeason currentEnemySeason, int i, int height, List<EnemySeason> seasons)
	{
        
		GUILayout.BeginHorizontal ();
		GUILayout.Label (i.ToString (), GUILayout.Width (height), GUILayout.Height (height));

		if (GUILayout.Button ("-", GUILayout.Width (height), GUILayout.Height (height))) {
			seasons.Remove (currentEnemySeason);
			return;
		}
		currentEnemySeason.name = GUILayout.TextField (currentEnemySeason.name, GUILayout.Width (120), GUILayout.Height (height));
		currentEnemySeason.enemyType = (EnemyType)EditorGUILayout.EnumPopup (currentEnemySeason.enemyType, GUILayout.Width (100), GUILayout.Height (height));
		float Dx, Dy;
		Dx = currentEnemySeason.Difficult.x;
		Dy = currentEnemySeason.Difficult.y;
		Dx = EditorGUILayout.FloatField (Dx, GUILayout.Height (height), GUILayout.Width (70));
		Dy = EditorGUILayout.FloatField (Dy, GUILayout.Height (height), GUILayout.Width (70));
		currentEnemySeason.Difficult = new Vector2 (Dx, Dy);
		currentEnemySeason.TimeDuration = EditorGUILayout.FloatField (currentEnemySeason.TimeDuration, GUILayout.Height (height), GUILayout.Width (70));
		currentEnemySeason.Coefficient = EditorGUILayout.FloatField (currentEnemySeason.Coefficient, GUILayout.Height (height), GUILayout.Width (70));
		currentEnemySeason.isDisposable = (Disposable)EditorGUILayout.EnumPopup (currentEnemySeason.isDisposable, GUILayout.Height (height), GUILayout.Width (100));
		currentEnemySeason.timerMax = EditorGUILayout.FloatField (currentEnemySeason.timerMax, GUILayout.Height (height), GUILayout.Width (70));
		currentEnemySeason.timerOffset = EditorGUILayout.FloatField (currentEnemySeason.timerOffset, GUILayout.Height (height), GUILayout.Width (70));
		currentEnemySeason.max = EditorGUILayout.IntField (currentEnemySeason.max, GUILayout.Height (height), GUILayout.Width (70));
		currentEnemySeason.enemyDirection = (EnemyDirection)EditorGUILayout.EnumPopup (currentEnemySeason.enemyDirection, GUILayout.Height (height), GUILayout.Width (100));
		GUILayout.EndHorizontal ();
	}

	void ShowBiomeSource (int select, int height)
	{

		GUILayout.BeginHorizontal ();
		GUILayout.Label (select.ToString (), GUILayout.Width (height), GUILayout.Height (height));

		if (GUILayout.Button ("-", GUILayout.Width (height), GUILayout.Height (height))) {
			currentBiomesSource.roadSource.Remove (currentBiomesSource.roadSource [select]);
			return;
		}
		currentBiomesSource.roadSource [select].name = GUILayout.TextField (currentBiomesSource.roadSource [select].name, GUILayout.Width (180), GUILayout.Height (height));
		GUILayout.Box (generator.editorIcons [Mathf.Clamp ((int)currentBiomesSource.roadSource [select].roadSourceStyle, 0, generator.editorIcons.Length)], GUILayout.Width (height), GUILayout.Height (height));
		currentBiomesSource.roadSource [select].roadSourceType = (RoadSourceType)EditorGUILayout.EnumPopup (currentBiomesSource.roadSource [select].roadSourceType, GUILayout.Width (100), GUILayout.Height (height));
		currentBiomesSource.roadSource [select].roadSourceStyle = (RoadSourceStyle)EditorGUILayout.EnumPopup (currentBiomesSource.roadSource [select].roadSourceStyle, GUILayout.Width (100), GUILayout.Height (height));
		string[] biomes = new string[currentBiomesSources.biomesSource.Count + 1];
		for (int j = 0; j < currentBiomesSources.biomesSource.Count; j++) {
			biomes [j] = currentBiomesSources.biomesSource [j].name;
		}
		biomes [currentBiomesSources.biomesSource.Count] = "None";


		if (!currentBiomesSource.roadSource [select].isTransition) {
			GUILayout.Label ("", GUILayout.Width (36));
			currentBiomesSource.roadSource [select].isTransition = EditorGUILayout.Toggle (currentBiomesSource.roadSource [select].isTransition, GUILayout.Height (height), GUILayout.Width (60));
		} else {
			currentBiomesSource.roadSource [select].transitionToBiom = EditorGUILayout.Popup ("", currentBiomesSource.roadSource [select].transitionToBiom, biomes, GUILayout.Width (100), GUILayout.Height (height));
			if (currentBiomesSource.roadSource [select].transitionToBiom >= currentBiomesSources.biomesSource.Count) {
				currentBiomesSource.roadSource [select].isTransition = false;
				currentBiomesSource.roadSource [select].transitionToBiom = 0;
			} 
		}

		GUIStyle field = skin.GetStyle ("Field");
		currentBiomesSource.roadSource [select].coefficient = float.Parse (GUILayout.TextField (currentBiomesSource.roadSource [select].coefficient.ToString (), GUILayout.Height (height), GUILayout.Width (70)));
		currentBiomesSource.roadSource [select].points = float.Parse (GUILayout.TextField (currentBiomesSource.roadSource [select].points.ToString (), GUILayout.Height (height), GUILayout.Width (70)));


		if (GUILayout.Button ((currentBiomesSource.roadSource [select].isActive) ? "Active" : "Deactive", GUILayout.Width (100), GUILayout.Height (height))) 
		{
			currentBiomesSource.roadSource [select].isActive = !currentBiomesSource.roadSource [select].isActive;
		}

		if (currentBiomesSource.roadSource [select].isCheck) {
			GUILayout.Label (generator.green, GUILayout.Width (height), GUILayout.Height (height));
		} else {
			GUILayout.Label (generator.red, GUILayout.Width (height), GUILayout.Height (height));
		}
		GUILayout.EndHorizontal ();


	}

	void ShowUnitSource (int select, int height)
	{
		float iconSize = 60;

		GUILayout.BeginHorizontal ();
		GUILayout.Label (select.ToString (), GUILayout.Width (height), GUILayout.Height (height));

		if (GUILayout.Button ("-", GUILayout.Width (height), GUILayout.Height (height))) {
			generator.unitsSources.unit.Remove (generator.unitsSources.unit [select]);
			return;
		}

		generator.unitsSources.unit [select].name = GUILayout.TextField (generator.unitsSources.unit [select].name, GUILayout.Width (180), GUILayout.Height (height));

		if (GUILayout.Button (generator.enemyIcons [generator.unitsSources.unit [select].image], skin.GetStyle ("Label"), GUILayout.Width (iconSize), GUILayout.Height (iconSize))) {
			currentMode = MapEditorMode.selectOne;
			currentElement = select;
			return;
		}
		Texture classTex = generator.redBack;
		if (generator.unitsSources.unit [select].type == EnemyClass.Attacker)
			classTex = generator.blueBack;
		if (generator.unitsSources.unit [select].type == EnemyClass.Neutral)
			classTex = generator.greenBack;
		
		GUI.DrawTexture (new Rect (20 + height * 2 + 180 + iconSize, height + 6 + (select + (currentMode != MapEditorMode.None ? ((select > currentElement) ? 1 : 0) : 0)) * (iconSize + 4), 120, 60), classTex, ScaleMode.ScaleAndCrop, true);

		if (GUILayout.Button (generator.unitsSources.unit [select].type.ToString (), GUILayout.Width (120), GUILayout.Height (iconSize))) 
		{
			currentMode = MapEditorMode.selectTwo;
			currentElement = select;
			return;
		}
		if (GUILayout.Button (generator.directionIcons [(int)generator.unitsSources.unit [select].direction], skin.GetStyle ("Label"), GUILayout.Width (iconSize), GUILayout.Height (iconSize))) {
			currentMode = MapEditorMode.selectThree;
			currentElement = select;
			return;
		}
		GUIStyle field = skin.GetStyle ("Field");
		generator.unitsSources.unit [select].cost = int.Parse (GUILayout.TextField (generator.unitsSources.unit [select].cost.ToString (), GUILayout.Width (100.0f), GUILayout.Height (iconSize)));
		generator.unitsSources.unit [select].tier = int.Parse (GUILayout.TextField (generator.unitsSources.unit [select].tier.ToString (), GUILayout.Width (iconSize), GUILayout.Height (iconSize)));
		GUILayout.EndHorizontal ();

		if (currentElement == select)
			switch (currentMode) {
			case MapEditorMode.selectOne:
				GUILayout.BeginHorizontal ();
				for (int i = 0; i < generator.enemyIcons.Length; i++)
					if (GUILayout.Button (generator.enemyIcons [i], skin.GetStyle ("Label"), GUILayout.Width (iconSize), GUILayout.Height (iconSize))) {
						generator.unitsSources.unit [select].image = i;
						currentElement = -1;
						currentMode = MapEditorMode.None;
					}
				GUILayout.EndHorizontal ();
				break;
			case MapEditorMode.selectTwo:
				GUILayout.BeginHorizontal ();
				if (GUILayout.Button (generator.redBack, skin.GetStyle ("Label"), GUILayout.Width (120), GUILayout.Height (iconSize))) {
					generator.unitsSources.unit [select].type = EnemyClass.Fighter;
					currentElement = -1;
					currentMode = MapEditorMode.None;
				}
				if (GUILayout.Button (generator.blueBack, skin.GetStyle ("Label"), GUILayout.Width (120), GUILayout.Height (iconSize))) {
					generator.unitsSources.unit [select].type = EnemyClass.Attacker;
					currentElement = -1;
					currentMode = MapEditorMode.None;
				}
				if (GUILayout.Button (generator.greenBack, skin.GetStyle ("Label"), GUILayout.Width (120), GUILayout.Height (iconSize))) {
					generator.unitsSources.unit [select].type = EnemyClass.Neutral;
					currentElement = -1;
					currentMode = MapEditorMode.None;
				}
				GUILayout.EndHorizontal ();
				break;
			case MapEditorMode.selectThree:
				GUILayout.BeginHorizontal ();
				for (int i = 0; i < generator.directionIcons.Length; i++)
					if (GUILayout.Button (generator.directionIcons [i], skin.GetStyle ("Label"), GUILayout.Width (iconSize), GUILayout.Height (iconSize))) {
						generator.unitsSources.unit [select].direction = (EnemyDirection)i;
						currentElement = -1;
						currentMode = MapEditorMode.None;
					}
				GUILayout.EndHorizontal ();
				break;
			}


	}

	void ShowMiscSource (int select, int height)
	{
		float iconSize = 60;

		GUILayout.BeginHorizontal ();
		GUILayout.Label (select.ToString (), GUILayout.Width (height), GUILayout.Height (height));

		if (GUILayout.Button ("-", GUILayout.Width (height), GUILayout.Height (height))) {
			generator.miscSources.unit.Remove (generator.miscSources.unit [select]);
			return;
		}

		generator.miscSources.unit [select].name = GUILayout.TextField (generator.miscSources.unit [select].name, GUILayout.Width (180), GUILayout.Height (height));

		if (GUILayout.Button (generator.miscIcons [generator.miscSources.unit [select].image], skin.GetStyle ("Label"), GUILayout.Width (iconSize), GUILayout.Height (iconSize))) {
			currentMode = MapEditorMode.selectOne;
			currentElement = select;
			return;
		}

		GUIStyle field = skin.GetStyle ("Field");
		generator.miscSources.unit [select].cost = int.Parse (GUILayout.TextField (generator.miscSources.unit [select].cost.ToString (), GUILayout.Width (100.0f), GUILayout.Height (iconSize)));
		GUILayout.EndHorizontal ();

		if (currentElement == select)
			switch (currentMode) {
		case MapEditorMode.selectOne:
			GUILayout.BeginHorizontal ();
			for (int i = 0; i < generator.miscIcons.Length; i++)
				if (GUILayout.Button (generator.miscIcons [i], skin.GetStyle ("Label"), GUILayout.Width (iconSize), GUILayout.Height (iconSize))) {
					generator.miscSources.unit [select].image = i;
					currentElement = -1;
					currentMode = MapEditorMode.None;
				}
			GUILayout.EndHorizontal ();
			break;
		
		}


	}

	void ShowMapSource (int select, int height)
	{

		GUILayout.BeginHorizontal ();
		GUILayout.Label (select.ToString (), GUILayout.Width (height), GUILayout.Height (height));

		if (GUILayout.Button ("-", GUILayout.Width (height), GUILayout.Height (height))) {
			generator.mapData.maps.Remove (generator.mapData.maps [select]);
			return;
		}
		if (GUILayout.Button ("Open", GUILayout.Width (height*2), GUILayout.Height (height))) {
			isMapOpen = true;
			current = generator.mapData.maps [select];
			return;
		}

		generator.mapData.maps [select].id = GUILayout.TextField (generator.mapData.maps [select].id, GUILayout.Width (180), GUILayout.Height (height));
		generator.mapData.maps [select].tier = int.Parse(GUILayout.TextField (generator.mapData.maps [select].tier.ToString(), GUILayout.Width (height), GUILayout.Height (height)));
		GUILayout.Label (generator.mapData.maps [select].blockCount.ToString(), GUILayout.Width (height), GUILayout.Height (height)); 
		GUILayout.EndHorizontal ();


	}

	void SaveBiomeSource ()
	{
		//currentBiomesSource.crossingPower = new CrossingPower[currentBiomesSources.biomesSource.Count];
		for (int z = 0; z < currentBiomesSources.biomesSource.Count; z++) {
			currentBiomesSources.biomesSource [z].crossingPower = new CrossingPower[currentBiomesSources.biomesSource.Count];
			for (int j = 0; j < currentBiomesSources.biomesSource.Count; j++) {
				currentBiomesSources.biomesSource [z].crossingPower [j] = new CrossingPower ();
			}
		}

		ResourcesDatabase resourcesDB = Resources.Load (ResourcesDatabase.assetName, typeof(Object)) as ResourcesDatabase;
		for (int i = 0; i < currentBiomesSource.roadSource.Count; i++) {
			currentBiomesSource.roadSource [i].isCheck = false;
			for (int j = 0; j < resourcesDB.blocks.Count; j++) {
				if (currentBiomesSource.roadSource [i].name.ToLower () == resourcesDB.blocks [j].name.ToLower ()) {
					
					currentBiomesSource.roadSource [i].isCheck = true;
					break;
				}


			}
		}

		for (int z = 0; z < currentBiomesSources.biomesSource.Count; z++) {
			for (int j = 0; j < currentBiomesSources.biomesSource.Count; j++) {
				for (int i = 0; i < currentBiomesSources.biomesSource [j].roadSource.Count; i++) {
					switch (currentBiomesSources.biomesSource [j].roadSource [i].roadSourceType) {
					case RoadSourceType.None:
						if (currentBiomesSources.biomesSource [j].roadSource [i].transitionToBiom == z)
							currentBiomesSources.biomesSource [j].crossingPower [z].toForward = true;
						break;
					case RoadSourceType.ForkLeft:
						if (currentBiomesSources.biomesSource [j].roadSource [i].transitionToBiom == z)
							currentBiomesSources.biomesSource [j].crossingPower [z].toLeft = true;
						break;
					case RoadSourceType.ForkRight:
						if (currentBiomesSources.biomesSource [j].roadSource [i].transitionToBiom == z)
							currentBiomesSources.biomesSource [j].crossingPower [z].toRight = true;
						break;
					case RoadSourceType.ForkLeftReverse:
						if (currentBiomesSources.biomesSource [j].roadSource [i].transitionToBiom == z)
							currentBiomesSources.biomesSource [j].crossingPower [z].toLeftReverse = true;
						break;
					case RoadSourceType.ForkRightReverse:
						if (currentBiomesSources.biomesSource [j].roadSource [i].transitionToBiom == z)
							currentBiomesSources.biomesSource [j].crossingPower [z].toRightReverse = true;
						break;
					}
				}
			}
		}

			
		if (AssetDatabase.LoadAssetAtPath ("Assets/Resources/biomes.asset", typeof(Object)) == null)
			AssetDatabase.CreateAsset (currentBiomesSources, "Assets/Resources/biomes.asset");
		AssetDatabase.Refresh ();
		EditorUtility.SetDirty (currentBiomesSources);
		AssetDatabase.SaveAssets ();
	}

	void SaveActiveSeasons ()
	{
		
		//current = AssetDatabase.LoadAssetAtPath ("Assets/Resources/maps/" + maps [currentMap].ToString () + ".asset", typeof(MapsData)) as MapDataSource;

		if (AssetDatabase.LoadAssetAtPath ("Assets/Resources/mapsData.asset", typeof(MapsData)) == null)
			AssetDatabase.CreateAsset (generator.mapData, "Assets/Resources/mapsData.asset");
		AssetDatabase.Refresh ();
		EditorUtility.SetDirty (generator.mapData);
		AssetDatabase.SaveAssets ();
	}

	void SaveUnits ()
	{
		//current = AssetDatabase.LoadAssetAtPath ("Assets/Resources/maps/" + maps [currentMap].ToString () + ".asset", typeof(MapsData)) as MapDataSource;

		if (AssetDatabase.LoadAssetAtPath ("Assets/Resources/unitData.asset", typeof(UnitsSources)) == null)
			AssetDatabase.CreateAsset (generator.unitsSources, "Assets/Resources/unitData.asset");
		AssetDatabase.Refresh ();
		EditorUtility.SetDirty (generator.unitsSources);
		AssetDatabase.SaveAssets ();
	}

	void SaveMiscs ()
	{
		//current = AssetDatabase.LoadAssetAtPath ("Assets/Resources/maps/" + maps [currentMap].ToString () + ".asset", typeof(MapsData)) as MapDataSource;

		if (AssetDatabase.LoadAssetAtPath ("Assets/Resources/miscData.asset", typeof(MiscSources)) == null)
			AssetDatabase.CreateAsset (generator.miscSources, "Assets/Resources/miscData.asset");
		AssetDatabase.Refresh ();
		EditorUtility.SetDirty (generator.miscSources);
		AssetDatabase.SaveAssets ();
	}

	int GetPrevRoadCount(int season)
	{
		int result = 0;
		if (current.activeSeasons [season].seasonPrev >= 0)
		{
			if (current.activeSeasons [season].seasonPrevRoad == 0)
			{
				return current.activeSeasons [current.activeSeasons [season].seasonPrev].roadElements.Count + GetPrevRoadCount(current.activeSeasons [season].seasonPrev);
			} else {
				return current.activeSeasons [season].seasonPrevRoad + GetPrevRoadCount(current.activeSeasons [season].seasonPrev) + 1;
			}

		} else
			return 0;
	}

	void ShowMapsSourceHead (int height)
	{

		GUILayout.BeginHorizontal ();
		GUILayout.Label ("#", GUILayout.Width (height), GUILayout.Height (height));
		GUILayout.Label ("", GUILayout.Width (height * 3), GUILayout.Height (height));
		GUILayout.Label ("Name", GUILayout.Width (180), GUILayout.Height (height));
		GUILayout.Label ("Tier", GUILayout.Width (height), GUILayout.Height (height));
		GUILayout.Label ("Num", GUILayout.Width (height), GUILayout.Height (height));
		GUILayout.EndHorizontal ();
	}

	void ShowUnitSourceHead (int height)
	{

		GUILayout.BeginHorizontal ();
		GUILayout.Label ("#", GUILayout.Width (height), GUILayout.Height (height));
		GUILayout.Label ("", GUILayout.Width (height), GUILayout.Height (height));
		GUILayout.Label ("Name", GUILayout.Width (180), GUILayout.Height (height));
		GUILayout.Label ("", GUILayout.Width (height), GUILayout.Height (height));
		GUILayout.Label ("Class", GUILayout.Width (120), GUILayout.Height (height));
		GUILayout.Label ("Dir", GUILayout.Width (height), GUILayout.Height (height));
		GUILayout.Label ("Cost", GUILayout.Width (120), GUILayout.Height (height));
		GUILayout.Label ("Tier", GUILayout.Width (60), GUILayout.Height (height));
		GUILayout.EndHorizontal ();
	}

	void ShowMiscSourceHead (int height)
	{

		GUILayout.BeginHorizontal ();
		GUILayout.Label ("#", GUILayout.Width (height), GUILayout.Height (height));
		GUILayout.Label ("", GUILayout.Width (height), GUILayout.Height (height));
		GUILayout.Label ("Name", GUILayout.Width (180), GUILayout.Height (height));
		GUILayout.Label ("", GUILayout.Width (height), GUILayout.Height (height));
		GUILayout.Label ("Class", GUILayout.Width (120), GUILayout.Height (height));
		GUILayout.Label ("Dir", GUILayout.Width (height), GUILayout.Height (height));
		GUILayout.Label ("Cost", GUILayout.Width (120), GUILayout.Height (height));
		GUILayout.Label ("Tier", GUILayout.Width (60), GUILayout.Height (height));
		GUILayout.EndHorizontal ();
	}

	void ShowBiomeSourceHead (int height)
	{
		Color c =	GUI.color;
		GUI.color = Color.white;
		GUILayout.BeginHorizontal ();
		GUILayout.Label ("#", GUILayout.Width (height), GUILayout.Height (height));
		GUILayout.Label ("", GUILayout.Width (height), GUILayout.Height (height));
		GUILayout.Label ("Name", GUILayout.Width (180), GUILayout.Height (height));
		GUILayout.Label ("", GUILayout.Width (height), GUILayout.Height (height));
		GUILayout.Label ("Type", GUILayout.Width (100), GUILayout.Height (height));
		GUILayout.Label ("Style", GUILayout.Width (100), GUILayout.Height (height));
		GUILayout.Label ("Transition", GUILayout.Width (100), GUILayout.Height (height));
		GUILayout.Label ("Coefficient", GUILayout.Width (70), GUILayout.Height (height));
		GUILayout.Label ("Points", GUILayout.Width (70), GUILayout.Height (height));
		GUILayout.Label ("Active", GUILayout.Width (100), GUILayout.Height (height));
		GUILayout.EndHorizontal ();
	}

	void ShowEnemySeasonHead (int height)
	{

		GUILayout.BeginHorizontal ();
		GUILayout.Label ("#", GUILayout.Width (30), GUILayout.Height (height));
		GUILayout.Label ("Name", GUILayout.Width (120), GUILayout.Height (height));
		GUILayout.Label ("Type", GUILayout.Width (100), GUILayout.Height (height));
		GUILayout.Label ("DiffMin", GUILayout.Width (70), GUILayout.Height (height));
		GUILayout.Label ("DiffMax", GUILayout.Width (70), GUILayout.Height (height));
		GUILayout.Label ("Duration", GUILayout.Width (70), GUILayout.Height (height));
		GUILayout.Label ("Coefficient", GUILayout.Width (70), GUILayout.Height (height));
		GUILayout.Label ("Disposable", GUILayout.Width (100), GUILayout.Height (height));
		GUILayout.Label ("TimerMax", GUILayout.Width (70), GUILayout.Height (height));
		GUILayout.Label ("TimerOffset", GUILayout.Width (70), GUILayout.Height (height));
		GUILayout.Label ("MaxCount", GUILayout.Width (70), GUILayout.Height (height));
		GUILayout.Label ("Direction", GUILayout.Width (100), GUILayout.Height (height));
		GUILayout.EndHorizontal ();
	}

	void BiomesSelectBiome()
	{
		for (int i = 0; i < currentBiomesSources.biomesSource.Count; i++) 
		{
			if (GUI.Button (new Rect (10.0f + 90.0f * i, 10.0f, 70.0f, 70.0f), "")) 
				currentBiomesSource = currentBiomesSources.biomesSource [i];

			GUI.DrawTexture (new Rect (13.0f + 90.0f * i, 13.0f, 64.0f, 64.0f), currentBiomesSources.biomesSource [i].image);
			GUI.Label (new Rect (10.0f + 90.0f * i, 70.0f, 70.0f, 40.0f), currentBiomesSources.biomesSource [i].name);
		}

		if (!isNewBiomPreferences) 
		{
			if (GUI.Button (new Rect (5.0f + 90.0f * currentBiomesSources.biomesSource.Count, 5.0f, 40.0f, 40.0f), "+"))
				isNewBiomPreferences = true;
			if (GUI.Button (new Rect (5.0f + 90.0f * currentBiomesSources.biomesSource.Count, 50.0f, 40.0f, 40.0f), "-"))
				currentBiomesSources.biomesSource.Remove (currentBiomesSources.biomesSource [currentBiomesSources.biomesSource.Count - 1]);

		} else 
		{
			newBiomName = GUI.TextField (new Rect (5.0f + 90.0f * currentBiomesSources.biomesSource.Count, 5.0f, 160.0f, 40.0f), newBiomName);
			for (int i = 0; i < generator.biomeIcons.Length; i++) 
			{
				if (GUI.Button (new Rect (5.0f + 90.0f * currentBiomesSources.biomesSource.Count + 60 * i, 50.0f, 40.0f, 40.0f), generator.biomeIcons [i])) 
					newBiomeIcon = i;
			}
			if (GUI.Button (new Rect (5.0f + 90.0f * currentBiomesSources.biomesSource.Count + 165.0f, 2.0f, 80.0f, 40.0f), "Create")) 
			{
				BiomesSource newBiomeSource = new BiomesSource ();
				newBiomeSource.image = generator.biomeIcons [newBiomeIcon];
				newBiomeSource.name = newBiomName;
				currentBiomesSources.biomesSource.Add (newBiomeSource);
				newBiomeSource.roadSource = new List<RoadSourceData> ();
				isNewBiomPreferences = false;
			}



		}

	}
	void Biomes ()
	{
		
		if (currentBiomesSources == null) 
			LoadData ();


		int height = 40;
		StartEditorHead ();
			
		BiomesSelectBiome ();
			
		StartEditorSpace (100);


		scrollPosition = GUILayout.BeginScrollView (scrollPosition);

		if (currentBiomesSource == null)
			currentBiomesSource = currentBiomesSources.biomesSource [0];
			
		if (currentBiomesSource.roadSource == null)
			LoadData ();
		
		ShowBiomeSourceHead (height);
		for (int i = 0; i < currentBiomesSource.roadSource.Count; i++) 
			ShowBiomeSource (i, height);
		
		GUILayout.BeginHorizontal ();
		GUILayout.Space (height + 7);
		if (GUILayout.Button ("+", GUILayout.Width (height), GUILayout.Height (height))) 
		{
			RoadSourceData roadSource = new RoadSourceData ();
			roadSource.name = "";
			roadSource.roadSourceType = RoadSourceType.None;
			roadSource.isActive = true;
			roadSource.isTransition = false;
			roadSource.coefficient = 1.0f;
			roadSource.points = 1.0f;
			currentBiomesSource.roadSource.Add (roadSource);
		}
		GUILayout.EndHorizontal ();
		         
		GUILayout.EndScrollView ();

		GUILayout.EndArea ();
		GUILayout.EndArea ();

		if (GUI.Button (new Rect (5.0f, position.height - 45.0f, 100, 40), "Save")) 
			SaveBiomeSource ();

		GUI.Box (new Rect (105.0f, position.height - 45.0f, position.width - 105.0f - 305.0f, 40), "", skin.GetStyle ("Button"));

	}

	void DisplayActiveSeason (int x, int y, int number)
	{
		//current.activeSeasons[number].pos = new Vector2(x, y);
		Rect button = new Rect (x - 40, y - 15, 35.0f, 35.0f);
		GUI.Box (button, number.ToString ());
		Vector2 mouse = Event.current.mousePosition;
		if (button.Contains (mouse) && Event.current.button == 0 && Event.current.type == EventType.MouseDown) {
			isDragAll = false;
		}


		if (isDragAll == false && button.Contains (mouse)) {
			Vector2 pos = current.ways [current.activeSeasons [number].wayNumber].pos;
			pos.x = mouse.x + 20;
			pos.y = mouse.y + 7;
			current.ways [current.activeSeasons [number].wayNumber].pos = pos;
			GUI.changed = true;


		}


		if (GUI.Button (new Rect (x, y, 70.0f, 70.0f), "")) {
			currentNumber = number;
			Menu02 (number);
		}
		GUI.DrawTexture (new Rect (x + 3.0f, y + 3.0f, 64.0f, 64.0f), currentBiomesSources.biomesSource [current.activeSeasons [number].biomeNumber].image);
		GUI.Label (new Rect (x, y + 60.0f, 70.0f, 40.0f), current.activeSeasons [number].name);

		for (int i = 0; i < current.activeSeasons [number].roadElements.Count; i++) {
			DisplayActiveRoadElement (x, y, number, i);       
		}

		DisplayEndRoad (x, y, number);
        
	}

	float CalculatePreviuisHeight(int number)
	{
		float height2 = 0;
		for (int i = 0; i < number; i++) 
		{
			height2 += 90.0f;
			for (int j = 0; j < current.activeSeasons [number].fighterSeasons.Count; j++) 
			{
				height2 += 25.0f;
			}
		}
		return height2;
	}

	void CreateWays()
	{
		

		int ways = 1;
		for (int i = 0; i < current.activeSeasons.Count; i++)
			for (int j = 0; j < current.activeSeasons [i].roadElements.Count; j++)
				if (current.activeSeasons [i].roadElements [j].roadSourceType == RoadElementType.Fork)
					ways++;

		wayBlocks = new WayBlock[ways];
		for (int i = 0; i < wayBlocks.Length; i++)
		{
			wayBlocks [i] = new WayBlock ();
			wayBlocks [i].forkRoad = -1;
			wayBlocks [i].forkSeason = -1;
			wayBlocks [i].activeSeason = new List<WayElement> ();
		}

		for (int i = 0; i < wayBlocks.Length; i++)
		{
			CalculateRoad (i);
		}


	}

	void DisplayWays()
	{
		GUILayout.Space (100);
		Color color = GUI.color;

		for (int i = 0; i < wayBlocks.Length; i++) 
		{
			GUILayout.BeginHorizontal ("Box");
			GUILayout.Label ("Way " + i, GUILayout.Width (75));
			for (int j = 0; j < wayBlocks[i].activeSeason.Count; j++) 
			{
				GUI.color = wayBlocks [i].activeSeason [j].color;
				for (int z = 0; z < wayBlocks[i].activeSeason[j].activeRoad.Count; z++) 
				{
					GUILayout.Label (wayBlocks[i].activeSeason[j].activeSeason.ToString(), skin.GetStyle ("Button"), GUILayout.Width (22 * wayBlocks[i].activeSeason[j].activeRoad[z].size));
				}

			}

			GUI.color = color;
			GUILayout.Label (" Size: " + wayBlocks[i].size, skin.GetStyle ("Label"), GUILayout.Width (142));
			GUILayout.EndHorizontal ();
			GUILayout.Space (4);
		}
	}

	void CalculateRoad(int select)
	{
		int way = 0;
		int road = 0;
		wayBlocks [select].size = 0;
		while (isContinue (ref way, select, ref road)) 
		{
			Debug.Log ("Continue " + way + " " + road);
		}

	}

	public float Calc(int way, int road)
	{
		switch (current.activeSeasons [way].roadElements [road].roadSourceType)
		{
		case  RoadElementType.Default:
			{
				float coeffSum = 0;
				int count = 0;
				for (int i = 0; i < current.activeSeasons [way].roadMagazine.Count; i++)
					if (current.activeSeasons [way].roadMagazine [i].isActive) 
					{
						coeffSum += current.activeSeasons [way].roadMagazine [i].coefficient;
						count++;
					}

				//coeffSum = coeffSum / count;

				float sum = 0;
				for (int i = 0; i < current.activeSeasons [way].roadMagazine.Count; i++)
					if (current.activeSeasons [way].roadMagazine [i].isActive) 
					{
						float points = currentBiomesSources.biomesSource [current.activeSeasons [way].biomeNumber].Get (current.activeSeasons [way].roadMagazine [i].name).points * current.activeSeasons [way].roadMagazine[i].coefficient / coeffSum;
						sum += points;// * current.activeSeasons [way].roadMagazine [i].coefficient * coeffSum;
					}
				//sum /= count;
				return sum;
			}
			break;
		case RoadElementType.Custom:
			{
				float coeffSum = 0;
				int count = 0;
				for (int i = 0; i < current.activeSeasons [way].roadElements [road].magazine.Count; i++)
					if (current.activeSeasons [way].roadElements [road].magazine [i].isActive)
					{
						coeffSum += current.activeSeasons [way].roadElements [road].magazine [i].coefficient;
						count++;
					}
				
				//coeffSum = coeffSum / count;


				float sum = 0;
				for (int i = 0; i < current.activeSeasons [way].roadElements[road].magazine.Count; i++)
					if (current.activeSeasons [way].roadElements [road].magazine [i].isActive)
							sum += currentBiomesSources.biomesSource[current.activeSeasons [way].biomeNumber].Get(current.activeSeasons [way].roadElements[road].magazine[i].name).points * current.activeSeasons [way].roadElements[road].magazine[i].coefficient / coeffSum;
				//sum /= count;
				return sum;
			}
			break;
		case RoadElementType.Special:
			return currentBiomesSources.biomesSource [current.activeSeasons [way].biomeNumber].roadSource [current.activeSeasons [way].roadElements [road].special].points;
			break;
		case RoadElementType.Fork:
			return 1.5f;
			break;
		case RoadElementType.Connection:
			return 1.5f;
			break;
		}
		return 0;
	}
	bool isContinue(ref int way, int select, ref int road)
	{
		
		wayBlocks [select].activeSeason.Add (new WayElement ());
		int number = wayBlocks [select].activeSeason.Count - 1;
		wayBlocks [select].activeSeason [number].color = new Color (Random.Range (0.0f, 1.0f), Random.Range (0.0f, 1.0f), Random.Range (0.0f, 1.0f), 1.0f);
		wayBlocks [select].activeSeason [number].activeSeason = way;
		wayBlocks [select].activeSeason [number].activeRoad = new List<WayRoad> ();

		for (int j = road; j < current.activeSeasons [way].roadElements.Count; j++) 
		{
			float size = Calc (way, j);
			wayBlocks [select].activeSeason [number].activeRoad.Add (new WayRoad(j, size));
			wayBlocks [select].size += size;

			if (current.activeSeasons [way].roadElements [j].roadSourceType == RoadElementType.Fork) 
			{
				bool isCheck = false;
				for (int m = 0; m < wayBlocks.Length; m++) 
				{
					if (way == wayBlocks [m].forkSeason && j == wayBlocks [m].forkRoad) 
					{
						Debug.Log ("Select " + select.ToString () + " WayBlock " + m);
						isCheck = true;


					} 


				}

			

					
					if (current.activeSeasons [way].roadElements [j].fork == RoadSourceType.ForkLeft) 
					{
						if (!isCheck) 
						{
							wayBlocks [select].forkRoad = j;
							wayBlocks [select].forkSeason = way;
							
							way = current.activeSeasons [way].roadElements [j].leftWay;
							road = 0;
							return true;
						}
					}
					if (current.activeSeasons [way].roadElements [j].fork == RoadSourceType.ForkRight) 
					{
						if (!isCheck) 
						{
							wayBlocks [select].forkRoad = j;
							wayBlocks [select].forkSeason = way;

							way = current.activeSeasons [way].roadElements [j].rightWay;
							road = 0;
							return true;
						}
					}


			}

			

		}

		if (current.activeSeasons [way].endType == ActiveEndType.final)
			return false;
		if (current.activeSeasons [way].endType == ActiveEndType.connect) 
		{
			
			road = current.activeSeasons [way].roadConnect;
			way = current.activeSeasons [way].seasonConnect;
			return true;
		}
		if (current.activeSeasons [way].endType == ActiveEndType.next) 
			{
				way = current.activeSeasons [way].seasonConnect;
				road = 0;
				return true;
			}
		return false;
	}

	void DisplayUnitsSeason (int number)
	{
		//GUI.color = Color.white;
		float heightAll = 70;
		for (int j = 0; j < number; j++) 
		{
			heightAll += current.activeSeasons [j].height;
		}

		current.activeSeasons [number].height = 90;
		for (int j = 0; j < current.activeSeasons[number].fighterSeasons.Count; j++) 
		{
			current.activeSeasons [number].height += 25;
		}
		for (int j = 0; j < current.activeSeasons[number].attackerSeasons.Count; j++) 
		{
			current.activeSeasons [number].height += 25;
		}
		for (int j = 0; j < current.activeSeasons[number].neutralSeasons.Count; j++) 
		{
			current.activeSeasons [number].height += 25;
		}
		for (int j = 0; j < current.activeSeasons[number].miscSeasons.Count; j++) 
		{
			current.activeSeasons [number].height += 25;
		}

		int elements = GetPrevRoadCount (number);
		int length1 = elements * 25;
		int length2 = current.activeSeasons [number].roadElements.Count * 25;

		GUILayout.BeginArea (new Rect (15, heightAll + 10 * number, 380 + length1 + length2, current.activeSeasons [number].height), skin.GetStyle("Box"));
		//GUILayout.Label (currentBiomesSources.biomesSource [current.activeSeasons [number].biomeNumber].image);

		GUILayout.BeginHorizontal ();
		GUILayout.Label (currentBiomesSources.biomesSource [current.activeSeasons [number].biomeNumber].image, GUILayout.Width(40), GUILayout.Height(40));
		GUILayout.Label (number.ToString (), GUILayout.Width(10.0f), GUILayout.Height(22.0f));
		GUILayout.Label (current.activeSeasons [number].name, GUILayout.Width(100.0f), GUILayout.Height(22.0f));



		for (int i = 0; i < current.activeSeasons [number].fighterSeasons.Count; i++) 
		{

			GUILayout.BeginArea (new Rect (5, 45 + 25 * i, position.width + 20, 25 * current.activeSeasons [number].fighterSeasons.Count));
			if (GUI.Button(new Rect ((elements) * 25, 0, 145.0f, 25.0f), ""))
				Menu02 (number);
			GUI.DrawTexture (new Rect (5 + length1, 0, 25.0f, 25.0f), generator.enemyIcons [generator.unitsSources.GetImage(current.activeSeasons [number].fighterSeasons[i].enemyType)]);
			GUI.Label (new Rect (30 + length1, 0, 110.0f, 22), current.activeSeasons [number].fighterSeasons[i].name, skin.GetStyle("Label"));
			MyGUI.MinMaxSlider(new Rect (165.0f + length1, 0, length2, 25.0f), ref current.activeSeasons [number].fighterSeasons [i].Difficult.x, ref current.activeSeasons [number].fighterSeasons [i].Difficult.y, 0.0f, 1.0f, Color.red);

			minMaxInterface (length1 + length2, current.activeSeasons [number].fighterSeasons [i]);
			timerInterface(length1 + length2 + 75, current.activeSeasons [number].fighterSeasons [i]); 
			GUILayout.EndArea ();

		}

		for (int i = 0; i < current.activeSeasons [number].attackerSeasons.Count; i++) 
		{
			
			GUILayout.BeginArea (new Rect (5, 50 + 25 * i + 25 * current.activeSeasons [number].fighterSeasons.Count, position.width + 20, 25 * current.activeSeasons [number].attackerSeasons.Count));
					if (GUI.Button(new Rect (length1, 0, 145.0f, 25.0f), ""))
				Menu02 (number);
			GUI.DrawTexture (new Rect (5 + length1, 0, 25.0f, 25.0f), generator.enemyIcons [generator.unitsSources.GetImage(current.activeSeasons [number].attackerSeasons [i].enemyType)]);
					GUI.Label (new Rect (30 + length1, 0, 110.0f, 22), current.activeSeasons [number].attackerSeasons[i].name, skin.GetStyle("Label"));
					MyGUI.MinMaxSlider(new Rect (165.0f + length1, 0, length2, 25.0f), ref current.activeSeasons [number].attackerSeasons [i].Difficult.x, ref current.activeSeasons [number].attackerSeasons [i].Difficult.y, 0.0f, 1.0f, Color.blue);
			minMaxInterface (length1 + length2, current.activeSeasons [number].attackerSeasons [i]);
			timerInterface(length1 + length2 + 75, current.activeSeasons [number].attackerSeasons [i]); 
			GUILayout.EndArea ();
		}

		for (int i = 0; i < current.activeSeasons [number].neutralSeasons.Count; i++) 
		{

			GUILayout.BeginArea (new Rect (5, 55 + 25 * i + 25 * current.activeSeasons [number].fighterSeasons.Count + 25 * current.activeSeasons [number].attackerSeasons.Count, position.width + 20, 25.0f * current.activeSeasons [number].neutralSeasons.Count));
					if (GUI.Button(new Rect (length1, 0, 145.0f, 25.0f), ""))
				Menu02 (number);
			GUI.DrawTexture (new Rect (5 + length1, 0, 25.0f, 25.0f), generator.enemyIcons [generator.unitsSources.GetImage(current.activeSeasons [number].neutralSeasons [i].enemyType)]);
					GUI.Label (new Rect (30 + length1, 0, 110.0f, 22), current.activeSeasons [number].neutralSeasons[i].name, skin.GetStyle("Label"));
					MyGUI.MinMaxSlider(new Rect (165.0f + length1, 0, length2, 25.0f), ref current.activeSeasons [number].neutralSeasons [i].Difficult.x, ref current.activeSeasons [number].neutralSeasons [i].Difficult.y, 0.0f, 1.0f, Color.green);

			minMaxInterface (length1 + length2, current.activeSeasons [number].neutralSeasons [i]);
			timerInterface(length1 + length2 + 75, current.activeSeasons [number].neutralSeasons [i]); 
			GUILayout.EndArea ();
		}

		for (int i = 0; i < current.activeSeasons [number].miscSeasons.Count; i++) 
		{

			GUILayout.BeginArea (new Rect (5, 60 + 25 * i + 25 * current.activeSeasons [number].fighterSeasons.Count + 25 * current.activeSeasons [number].attackerSeasons.Count + 25 * current.activeSeasons [number].neutralSeasons.Count, position.width + 20, 25.0f * current.activeSeasons [number].miscSeasons.Count));
			if (GUI.Button(new Rect (length1, 0, 145.0f, 25.0f), ""))
				Menu02 (number);
			GUI.DrawTexture (new Rect (5 + length1, 0, 25.0f, 25.0f), generator.miscIcons [generator.miscSources.GetImage(current.activeSeasons [number].miscSeasons [i].miscType)]);
			GUI.Label (new Rect (30 + length1, 0, 110.0f, 22), current.activeSeasons [number].miscSeasons[i].name, skin.GetStyle("Label"));
			MyGUI.MinMaxSlider(new Rect (165.0f + length1, 0, length2, 25.0f), ref current.activeSeasons [number].miscSeasons [i].Difficult.x, ref current.activeSeasons [number].miscSeasons [i].Difficult.y, 0.0f, 1.0f, Color.yellow);

			minMaxInterface (length1 + length2, current.activeSeasons [number].miscSeasons [i]);
			timerInterface(length1 + length2 + 75, current.activeSeasons [number].miscSeasons [i]); 
			GUILayout.EndArea ();
		}


		if (GUI.Button (new Rect (25.0f + (elements) * 25, 65 + 25 * current.activeSeasons [number].fighterSeasons.Count + 25 * current.activeSeasons [number].attackerSeasons.Count + 25 * current.activeSeasons [number].neutralSeasons.Count + 25 * current.activeSeasons [number].miscSeasons.Count, 95.0f, 22), "Add", skin.GetStyle("Button")))
			Menu10 (number);
		
		for (int i = 0; i < elements; i++) 
		{
			if (GUI.Button (new Rect (i * 25 + 170, 5, 22, 22), "", skin.GetStyle("Box")))
				Menu02 (number);
		}

		for (int i = 0; i < current.activeSeasons [number].roadElements.Count; i++) 
		{
			RoadElement re = current.activeSeasons [number].roadElements[i];
			if (GUI.Button (new Rect ((i + elements) * 25 + 170, 5, 22, 22), ""))
				Menu02 (number);
			switch (re.roadSourceType) 
			{
			case RoadElementType.Custom:
				GUI.DrawTexture (new Rect ((i + elements) * 25 + 170, 5, 22, 22), generator.roadCustom);
				break;
			case RoadElementType.Special:
				GUI.DrawTexture (new Rect ((i + elements) * 25 + 170, 5, 22, 22), generator.roadSpecial);
				break;
			case RoadElementType.Fork:
				GUI.DrawTexture (new Rect ((i + elements) * 25 + 170, 5, 22, 22), generator.roadFork);
				break;
			case RoadElementType.Connection:
				GUI.DrawTexture (new Rect ((i + elements) * 25 + 170, 5, 22, 22), generator.roadConnect);
				break;
			}
		}

		GUILayout.EndHorizontal ();
		GUILayout.EndArea ();
	}



	void minMaxInterface(float lenght, Season season)
	{
		if (GUI.Button(new Rect (175.0f + lenght, 2, 22, 22), '\u25B2'.ToString()))
			season.max += 1;
		GUI.Label (new Rect (175.0f + lenght + 24, 2, 22, 22), season.max.ToString(), skin.GetStyle ("Label"));
		if (GUI.Button(new Rect (175.0f + lenght + 48, 2, 22, 22), '\u25BC'.ToString()))
			season.max -= 1;
	}

	void timerInterface(float lenght, Season season)
	{
		season.timerMax = EditorGUI.FloatField (new Rect (175.0f + lenght, 2, 50, 22), season.timerMax);
		season.timerOffset = EditorGUI.FloatField (new Rect (175.0f + lenght + 55, 2, 50, 22), season.timerOffset);
	}

	void DisplayBigNumber (int x, int y, string bigNumber)
	{
		GUIContent content = new GUIContent ();
		GUIStyle myStyle = new GUIStyle ();
		myStyle.font = generator.polaris;
		myStyle.fontSize = 52;
		myStyle.alignment = TextAnchor.MiddleCenter;
		myStyle.normal.textColor = Color.white;
		GUI.Label (new Rect (x, y, 70.0f, 70.0f), bigNumber, myStyle);
	}

	void DisplayWay (int x, int y, int wayNumber, bool useCheck)
	{
		//int curX = x + 70 + 4 + 24 * roadNumber;
		//int curY = y + 23;
		if (current.ways.Count <= wayNumber) {
			Debug.Log (current.ways.Count + " " + wayNumber);
			return;
		}


		if (!current.ways [wayNumber].isUsed || useCheck) {
			if (GUI.Button (new Rect (x, y, 70.0f, 70.0f), "")) {
				currentWay = wayNumber;
				currentPos = new Vector2 (x, y);
				Menu01 ();
			}

			DisplayBigNumber (x, y, wayNumber.ToString ());
		}

		if (!current.ways [wayNumber].isUsed && !useCheck || current.ways [wayNumber].isUsed && !useCheck) {

			if (wayNumber > 0 && current.ways.Count > wayNumber) {
				int _number = current.ways [wayNumber].seasonMaster;
				SeasonActive sa = current.activeSeasons [_number];
				RoadElement re = sa.roadElements [current.ways [wayNumber].roadMaster];
				switch (re.fork) {
				case RoadSourceType.ForkLeft:
					Drawing.DrawLine (new Vector2 (x + 35, y + 70), new Vector2 (CalculateX (_number) + 70 + 15 + 24 * current.ways [wayNumber].roadMaster, CalculateY (_number) + 23), Color.red, 2, true);
                        //Drawing.DrawLine(new Vector2(curX + 11, curY), new Vector2(curX - 23 + 35, y + 23 - 135 - 35 + 110), Color.red, 2, true);
                        //Drawing.DrawLine(new Vector2(curX + 11, curY), current.activeSeasons[curRoadElem.leftWay].pos, Color.red, 2, true);

					break;
				case RoadSourceType.ForkRight:
					Drawing.DrawLine (new Vector2 (x + 35, y), new Vector2 (CalculateX (_number) + 70 + 15 + 24 * current.ways [wayNumber].roadMaster, CalculateY (_number) + 45), Color.red, 2, true);
                        //Drawing.DrawLine(new Vector2(curX + 11, curY + 22), new Vector2(curX - 23 + 35, y + 23 + 135 - 35 + 110), Color.red, 2, true);
                        //Drawing.DrawLine(new Vector2(curX + 11, curY + 22), current.activeSeasons[curRoadElem.rightWay].pos, Color.red, 2, true);

					break;
				case RoadSourceType.ForkBoth:
					Drawing.DrawLine (new Vector2 (x + 35, y + 70), new Vector2 (CalculateX (_number) + 70 + 15 + 24 * current.ways [wayNumber].roadMaster, CalculateY (_number) + 23), Color.yellow, 2, true);
					Drawing.DrawLine (new Vector2 (x + 35, y), new Vector2 (CalculateX (_number) + 70 + 15 + 24 * current.ways [wayNumber].roadMaster, CalculateY (_number) + 45), Color.red, 2, true);
                        //Drawing.DrawLine(new Vector2(curX + 11, curY), new Vector2(curX - 23 + 35, y + 23 - 135 - 35 + 110), Color.red, 2, true);
                        //Drawing.DrawLine(new Vector2(curX + 11, curY + 22), new Vector2(curX - 23 + 35, y + 23 + 135 - 35 + 110), Color.red, 2, true);
                        //Drawing.DrawLine(new Vector2(curX + 11, curY), current.activeSeasons[curRoadElem.leftWay].pos, Color.red, 2, true);
                        //Drawing.DrawLine(new Vector2(curX + 11, curY + 22), current.activeSeasons[curRoadElem.rightWay].pos, Color.red, 2, true);

					break;
				}
			}
		}

	}

	void DisplayEndRoad (int x, int y, int number)
	{
		int curX = x + 70 + 4 + 24 * current.activeSeasons [number].roadElements.Count;
		int curY = y;

		switch (current.activeSeasons [number].endType) {
		case ActiveEndType.final:
			{
				DisplayWay (curX, curY, current.activeSeasons [number].wayNumber, true);
			}
			break;
		case ActiveEndType.next:
                
			break;
		case ActiveEndType.connect:
			{
				if (GUI.Button (new Rect (curX, curY, 70.0f, 70.0f), "")) {
					currentNumber = number;
					;
					Menu05 ();
				}

				int sC = current.activeSeasons [number].seasonConnect;
				int rC = current.activeSeasons [number].roadConnect;

                    
				if (CalculateY (sC) < curY) {
					Vector2 CurConnect = new Vector2 (CalculateX (sC) + 70 + 4 + 24 * rC + 11, CalculateY (sC) + 46);
					Drawing.DrawLine (new Vector2 (curX + 35, curY), CurConnect, Color.red, 2, true);
				} else {
					Vector2 CurConnect = new Vector2 (CalculateX (sC) + 70 + 4 + 24 * rC + 11, CalculateY (sC) + 23);
					Drawing.DrawLine (new Vector2 (curX + 35, curY + 70), CurConnect, Color.red, 2, true);
				}

				GUI.DrawTexture (new Rect (curX + 3.0f, curY + 3.0f, 64.0f, 64.0f), generator.bigConnect);
			}
			break;
		}
	}

	void DisplayActiveRoadElement (int x, int y, int number, int roadNumber)
	{
		int curX = x + 70 + 4 + 24 * roadNumber;
		int curY = y + 23;
        

		RoadElement curRoadElem = current.activeSeasons [number].roadElements [roadNumber];
		switch (curRoadElem.roadSourceType) {
		case RoadElementType.Default:
			if (GUI.Button (new Rect (curX, curY, 22.0f, 22.0f), "")) {
				currentNumber = number;
				currentRoadNumber = roadNumber;
				if (!isConnectionMode)
					Menu03 (roadNumber);
				else
					Menu04 (roadNumber);
			}
			break;
		case RoadElementType.Special:
			if (GUI.Button (new Rect (curX, curY, 22.0f, 22.0f), new GUIContent ("", currentBiomesSources.biomesSource [current.activeSeasons [number].biomeNumber].roadSource [curRoadElem.special].name))) {
				currentNumber = number;
				currentRoadNumber = roadNumber;
				if (!isConnectionMode)
					Menu03 (roadNumber);
				else
					Menu04 (roadNumber);
			}


			GUIContent content = new GUIContent ();
			GUIStyle myStyle = new GUIStyle ();
			myStyle.fontSize = 11;
			myStyle.alignment = TextAnchor.MiddleLeft;
			myStyle.normal.textColor = Color.white;
                //GUI.Label(new Rect(curX, curY + 15.0f, 100.0f, 22.0f), GUI.tooltip);
			GUI.DrawTexture (new Rect (curX, curY, 22.0f, 22.0f), generator.roadSpecial);
			break;
		case RoadElementType.Custom:
			if (GUI.Button (new Rect (curX, curY, 22.0f, 22.0f), "")) {
				currentNumber = number;
				currentRoadNumber = roadNumber;
				if (!isConnectionMode)
					Menu03 (roadNumber);
				else
					Menu04 (roadNumber);
			}
			GUI.DrawTexture (new Rect (curX, curY, 22.0f, 22.0f), generator.roadCustom);
			break;
		case RoadElementType.Fork:
			{
				switch (curRoadElem.fork) {
				case RoadSourceType.None:
					{
						if (GUI.Button (new Rect (curX, curY, 22.0f, 22.0f), "")) {
							currentNumber = number;
							currentRoadNumber = roadNumber;
							if (!isConnectionMode)
								Menu03 (roadNumber);
							else
								Menu04 (roadNumber);
						}
					}
					break;
				case RoadSourceType.ForkLeft:
                            //Drawing.DrawLine(new Vector2(curX + 11, curY), new Vector2(curX - 23 + 35, y + 23 - 135 - 35 + 110), Color.red, 2, true);
                            //Drawing.DrawLine(new Vector2(curX + 11, curY), current.activeSeasons[curRoadElem.leftWay].pos, Color.red, 2, true);
					if (GUI.Button (new Rect (curX, curY, 22.0f, 22.0f), "")) {
						currentNumber = number;
						currentRoadNumber = roadNumber;
						if (!isConnectionMode)
							Menu03 (roadNumber);
						else
							Menu04 (roadNumber);
					}
					GUI.DrawTexture (new Rect (curX, curY, 22.0f, 22.0f), generator.roadFork); 
					break;
				case RoadSourceType.ForkRight:
                            //Drawing.DrawLine(new Vector2(curX + 11, curY + 22), new Vector2(curX - 23 + 35, y + 23 + 135 - 35 + 110), Color.red, 2, true);
                            //Drawing.DrawLine(new Vector2(curX + 11, curY + 22), current.activeSeasons[curRoadElem.rightWay].pos, Color.red, 2, true);
					if (GUI.Button (new Rect (curX, curY, 22.0f, 22.0f), "")) {
						currentNumber = number;
						currentRoadNumber = roadNumber;
						if (!isConnectionMode)
							Menu03 (roadNumber);
						else
							Menu04 (roadNumber);
					}
					GUI.DrawTexture (new Rect (curX, curY, 22.0f, 22.0f), generator.roadFork);
					break;
				case RoadSourceType.ForkBoth:
                            //Drawing.DrawLine(new Vector2(curX + 11, curY), new Vector2(curX - 23 + 35, y + 23 - 135 - 35 + 110), Color.red, 2, true);
                            //Drawing.DrawLine(new Vector2(curX + 11, curY + 22), new Vector2(curX - 23 + 35, y + 23 + 135 - 35 + 110), Color.red, 2, true);
                            //Drawing.DrawLine(new Vector2(curX + 11, curY), current.activeSeasons[curRoadElem.leftWay].pos, Color.red, 2, true);
                            //Drawing.DrawLine(new Vector2(curX + 11, curY + 22), current.activeSeasons[curRoadElem.rightWay].pos, Color.red, 2, true);
					if (GUI.Button (new Rect (curX, curY, 22.0f, 22.0f), "")) {
						currentNumber = number;
						currentRoadNumber = roadNumber;
						if (!isConnectionMode)
							Menu03 (roadNumber);
						else
							Menu04 (roadNumber);
					}
					GUI.DrawTexture (new Rect (curX, curY, 22.0f, 22.0f), generator.roadFork);
					break;
				}
                    
			}

			break;
		case RoadElementType.Connection:
			if (GUI.Button (new Rect (curX, curY, 22.0f, 22.0f), "")) {
				currentNumber = number;
				currentRoadNumber = roadNumber;
				Menu03 (roadNumber);
			}
			GUI.DrawTexture (new Rect (curX, curY, 22.0f, 22.0f), generator.roadConnect);
			break;
		}

        
	}

	void ShowFork (int number, int roadNumber)
	{
		RoadElement re = current.activeSeasons [number].roadElements [roadNumber];
		GUILayout.BeginArea (new Rect (0, 40, 270, position.height / 2 - 50.0f));
		float curX = CalculateX (number) + 70 + 4 + 24 * roadNumber;
		float curY = CalculateY (number) + 23;
		Rect leftRect = new Rect (50.0f, 30.0f, 150.0f, position.height / 8 - 50.0f);
		Rect rigthRect = new Rect (50.0f, 30.0f + position.height / 8 - 40.0f, 150.0f, position.height / 8 - 50.0f);

		switch (re.fork) {
		case RoadSourceType.None:
			{
				if (isLeft) {
					if (GUI.Button (leftRect, "Add Fork Left")) {
						current.ways.Add (new Ways (new Vector2 (curX + 11 - 35, curY - 131), current.ways.Count, number, roadNumber, 1));
						re.fork = RoadSourceType.ForkLeft;
						re.leftWay = current.ways.Count - 1;
						CalculateYApply (current.ways.Count - 1, true);
						//Debug.Log (current.ways.Count + " " + number + " " + roadNumber);
					}
				} else
					GUI.Label (leftRect, "Not find Left Fork Block");

				if (isRight) {
					if (GUI.Button (rigthRect, "Add Fork Right")) {
						current.ways.Add (new Ways (new Vector2 (curX + 11 - 35, curY + 89), current.ways.Count, number, roadNumber, 2));                     
						re.fork = RoadSourceType.ForkRight;
						re.rightWay = current.ways.Count - 1;
						CalculateYApply (current.ways.Count - 1, false);
						//Debug.Log (current.ways.Count + " " + number + " " + roadNumber);
					}
				} else
					GUI.Label (rigthRect, "Not find Right Fork Block");
			}
			break;
		case RoadSourceType.ForkLeft:
			if (GUI.Button (leftRect, "Delete Fork Left")) {
				re.fork = RoadSourceType.None;
			}
			if (GUI.Button (rigthRect, "Add Fork Right")) {
				current.ways.Add (new Ways (new Vector2 (curX + 11 - 35, curY + 89), current.ways.Count, number, roadNumber, 2)); 
				re.fork = RoadSourceType.ForkBoth;
				re.rightWay = current.ways.Count - 1;
				CalculateYApply (current.ways.Count - 1, false);
				Debug.Log (current.ways.Count + " " + number + " " + roadNumber);
			}
			break;
		case RoadSourceType.ForkRight:
			if (GUI.Button (leftRect, "Add Fork Left")) {
				current.ways.Add (new Ways (new Vector2 (curX + 11 - 35, curY - 131), current.ways.Count, number, roadNumber, 1));                       
				re.fork = RoadSourceType.ForkBoth;
				re.leftWay = current.ways.Count - 1;
				CalculateYApply (current.ways.Count - 1, true);
				Debug.Log (current.ways.Count + " " + number + " " + roadNumber);
			}
			if (GUI.Button (rigthRect, "Delete Fork Right")) {
				re.fork = RoadSourceType.None;
			}
			break;
		case RoadSourceType.ForkBoth:
			if (GUI.Button (leftRect, "Delete Fork Left")) {
				re.fork = RoadSourceType.ForkRight;
			}
			if (GUI.Button (rigthRect, "Delete Fork Right")) {
				re.fork = RoadSourceType.ForkLeft;
			}
			break;
		}

        

		GUILayout.EndArea ();
	}


	void Maps ()
	{
		int height = 40;
		StartEditorHead ();

		GUI.Label (new Rect (15, 10, 200, 40), "Maps Data", skin.GetStyle ("Header"));  

		StartEditorSpace (30);
		scrollPosition = GUILayout.BeginScrollView (scrollPosition);

		if (generator.mapData == null) {
			LoadData ();
			return;
		}

		ShowMapsSourceHead (40);
		for (int i = 0; i < generator.mapData.maps.Count; i++)
			ShowMapSource (i, 40);

		GUILayout.BeginHorizontal ();
		GUILayout.Space (40 + 7);
		if (GUILayout.Button ("+", GUILayout.Width (40), GUILayout.Height (40))) {
			MapDataSource newMapData = new MapDataSource (generator.mapData.maps.Count, "Untitled Map ");
			generator.mapData.maps.Add (newMapData);
		}

		GUILayout.EndHorizontal ();
		GUILayout.EndScrollView ();
		GUILayout.EndArea ();
		GUILayout.EndArea ();


		if (GUI.Button (new Rect (5.0f, position.height - 45.0f, 100, 40), "Save")) {
			SaveActiveSeasons ();

		}
	

		GUI.Box (new Rect (105.0f, position.height - 45.0f, position.width - 105.0f - 305.0f, 40), "", skin.GetStyle ("Button"));


			
			

	}

	void Roads ()
	{
		StartEditorHead();

		GUI.Label (new Rect (15, 10, 200, 40), current.id, skin.GetStyle ("Header"));  
		
		StartEditorSpace (30);

		if (GUI.Button (new Rect (5.0f, 5.0f, 80, 40), "Builder"))
			roadTypeEditor = 0;
		if (GUI.Button (new Rect (90.0f, 5.0f, 80, 40), "Units"))
			roadTypeEditor = 1;
		if (GUI.Button (new Rect (175.0f, 5.0f, 80, 40), "Ways"))
		{
			CreateWays ();
			roadTypeEditor = 2;
		}


		switch (roadTypeEditor)
		{
		case 0:
			{
				if (current.activeSeasons != null) {
					if (current.activeSeasons.Count > 0) {
						for (int i = 0; i < current.activeSeasons.Count; i++) {
							DisplayActiveSeason ((int)CalculateX (i), (int)CalculateY (i), i);
						}
					}
				}

				if (current.ways != null) {
					if (current.ways.Count > 0) {
						for (int i = 0; i < current.ways.Count; i++) {
							DisplayWay (Mathf.CeilToInt (current.ways [i].pos.x), Mathf.CeilToInt (current.ways [i].pos.y), i, false);
						}
					}
				}
			}
			break;
		case 1:
			if (current.activeSeasons != null) 
			{
				scrollPosition = GUI.BeginScrollView ( new Rect(5.0f, 40.0f,  position.width - 325.0f, position.height - 100.0f), scrollPosition, new Rect(0, 0, 1000, 1000));

				if (current.activeSeasons.Count > 0) {
					for (int i = 0; i < current.activeSeasons.Count; i++) {
						DisplayUnitsSeason (i);
					}
				}
				GUI.EndScrollView ();
			}
			break;
		case 2:
			if (current.activeSeasons != null) 
			{
				scrollPosition = GUI.BeginScrollView ( new Rect(5.0f, 40.0f,  position.width - 325.0f, position.height - 100.0f), scrollPosition, new Rect(0, 0, 1000, 1000));

				DisplayWays ();
				GUI.EndScrollView ();
			}
			break;
		}

				
		GUILayout.EndArea ();
		GUILayout.EndArea ();
           		
		if (GUI.Button (new Rect (5.0f, position.height - 45.0f, 100, 40), "Save")) {
			SaveActiveSeasons ();

		}
		if (GUI.Button (new Rect (105.0f, position.height - 45.0f, 100, 40), "Back")) {
			isMapOpen = false;

		}
		if (GUI.Button (new Rect (205.0f, position.height - 45.0f, 100, 40), "Clean")) {
			CleanData ();

		}
		GUI.Box (new Rect (305.0f, position.height - 45.0f, position.width - 300.0f - 310.0f, 40), "", skin.GetStyle ("Button"));


		if (isConnectionMode) 
		{
			int numb = GetLastActiveNumber (currentWay);
			Drawing.DrawLine (Event.current.mousePosition, new Vector2 ((CalculateX (numb) + 75 + 4 + 24 * current.activeSeasons [numb].roadElements.Count + 40), CalculateY (numb) + 70 + 45), Color.red, 2, true);
			GUI.changed = true;
		}
	}

	int GetLastActiveNumber (int _way)
	{
		int lastNumber = -1;
		for (int i = 0; i < current.activeSeasons.Count; i++) {
			if (current.activeSeasons [i].wayNumber == currentWay)
				lastNumber = i;
		}
		return lastNumber;
	}

	int GetFirstActiveNumber (int _way)
	{
		int lastNumber = -1;
		for (int i = 0; i < current.activeSeasons.Count; i++) {
			if (current.activeSeasons [i].wayNumber == currentWay)
				return lastNumber;
		}
		return lastNumber;
	}

	float CalculateX (int number)
	{
		float result = 0;
		if (current.ways == null)
			current.ways = new List<Ways> ();

		if (current.ways.Count > current.activeSeasons [number].wayNumber)
			result = current.ways [current.activeSeasons [number].wayNumber].pos.x;
		for (int i = 0; i < current.activeSeasons.Count; i++) {
			if (i == number) {
				return result;
			} else if (current.activeSeasons [i].wayNumber == current.activeSeasons [number].wayNumber) {
				result += 70 + 4 + 24 * current.activeSeasons [i].roadElements.Count;
			}
		}
		return result;
	}

	void CalculateYApply (int _wayNumber, bool isUp)
	{
		for (int i = 0; i < current.ways.Count; i++) {
			if (i != _wayNumber) {
				if (isUp) {
					Debug.Log (current.ways [i].pos.y + " " + current.ways [_wayNumber].pos.y);
					if (current.ways [i].pos.y <= current.ways [_wayNumber].pos.y) {
						current.ways [i].pos = new Vector2 (current.ways [i].pos.x, current.ways [i].pos.y - 131);
					}
				} else {
					Debug.Log (current.ways [i].pos.y + " " + current.ways [_wayNumber].pos.y);
					if (current.ways [i].pos.y >= current.ways [_wayNumber].pos.y) {
						current.ways [i].pos = new Vector2 (current.ways [i].pos.x, current.ways [i].pos.y + 89);
					}
				}
			}
		}
	}

  
	float CalculateY (int number)
	{
		float result = 0;
		if (current.ways == null)
			current.ways = new List<Ways> ();
		int currentWayNumber = current.activeSeasons [number].wayNumber;
		if (current.ways.Count > current.activeSeasons [number].wayNumber)
			result = current.ways [currentWayNumber].pos.y;
		return result;
	}

	void Menu01 ()
	{
		if (isConnectionMode) {
			isConnectionMode = false;
			return;
		}

		GenericMenu menu = new GenericMenu ();
		//GenericMenu menu = new GenericMenu ();


		for (int i = 0; i < currentBiomesSources.biomesSource.Count; i++)
			if (isBlockExist (i, current.ways [currentWay].side))
				menu.AddItem (new GUIContent ("LoadBiome/" + currentBiomesSources.biomesSource [i].name), true, CallbackMenu01, i.ToString ());
				//menu.AddItem (new GUIContent ("LoadBiome/" + currentBiomesSources.biomesSource [i].name), true, CallbackMenu01, i.ToString ());
			else
				menu.AddDisabledItem (new GUIContent ("LoadBiome/" + currentBiomesSources.biomesSource [i].name));	
		if (current.ways [currentWay].isUsed)
			menu.AddItem (new GUIContent ("ConnectTo..."), false, CallbackMenu05, "0");

		menu.ShowAsContext ();
		Event.current.Use ();
	}

	bool isBlockExist (int i, int side)
	{
		if (current.activeSeasons.Count == 0 && currentWay == 0)
			return true;

		int numb = GetLastActiveNumber (currentWay);
		if (numb < 0)
			numb = current.ways [currentWay].seasonMaster;

		Debug.Log (numb);

		if (side == 0 && currentBiomesSources.biomesSource [current.activeSeasons [numb].biomeNumber].crossingPower [i].toForward ||
			side == 1 && currentBiomesSources.biomesSource [current.activeSeasons [numb].biomeNumber].crossingPower [i].toLeft ||
			side == 2 && currentBiomesSources.biomesSource [current.activeSeasons [numb].biomeNumber].crossingPower [i].toRight)
			return true;
		else
			return false;
	}

	void CallbackMenu01 (object obj)
	{
		int count = currentBiomesSources.biomesSource.Count;
		for (int i = 0; i < count; i++) {
			if (obj.ToString () == i.ToString ()) 
			{
				Debug.Log (currentBiomesSources.biomesSource [i].name);
				AddActiveSeason (i, currentWay, currentPos);
				currentNumber = current.activeSeasons.Count - 1;
				currentMode = MapEditorMode.ActiveSettings;
				return;
			}
		}
	}

	void Menu02 (int number)
	{
		if (isConnectionMode) {
			isConnectionMode = false;
			return;
		}
		
		GenericMenu menu = new GenericMenu ();

		//for (int i = 0; i < currentBiomesSources.biomesSource.Count; i++)
		//menu.AddItem (new GUIContent ("ReplaceBiome/" + currentBiomesSources.biomesSource [i].name), false, CallbackMenu02, i.ToString ());
		menu.AddItem (new GUIContent ("Settings"), false, CallbackMenu03, number.ToString ());
		menu.AddItem (new GUIContent ("DeleteBiome"), false, CallbackMenu09, number.ToString ());
		menu.ShowAsContext ();
		Event.current.Use ();
	}

	void Menu10 (int number)
	{
		GenericMenu menu = new GenericMenu ();

		for (int i = 0; i < generator.unitsSources.unit.Count; i++) 
		{
			
			switch (generator.unitsSources.unit [i].type) {
			case EnemyClass.Fighter:
				{
					currentNumber = i;
					menu.AddItem (new GUIContent ("Add unit/Fighter/" + generator.unitsSources.unit [i].name), false, CallbackMenu10, i.ToString() + ";" + number.ToString());
				}
				break;
			case EnemyClass.Attacker:
					{
						currentNumber = i;
					menu.AddItem (new GUIContent ("Add unit/Attacker/" + generator.unitsSources.unit [i].name), false, CallbackMenu11, i.ToString() + ";" + number.ToString());
					}
				break;
			case EnemyClass.Neutral:
						{
							currentNumber = i;
					menu.AddItem (new GUIContent ("Add unit/Neutral/" + generator.unitsSources.unit [i].name), false, CallbackMenu12, i.ToString() + ";" + number.ToString());
						}
			
				break;
			}
		}

		for (int i = 0; i < generator.miscSources.unit.Count; i++) {
			currentNumber = i;
			menu.AddItem (new GUIContent ("Add misc/" + generator.miscSources.unit [i].name), false, CallbackMenu13, i.ToString () + ";" + number.ToString ());
		}
		menu.ShowAsContext ();
		Event.current.Use ();
	}

	void CallbackMenu10 (object obj)
	{
		int count = current.activeSeasons.Count;
		string objData = obj.ToString ();
		int currEnemy = int.Parse(objData.Substring (0, objData.IndexOf (";")));
		int currSeason = int.Parse(objData.Substring (objData.IndexOf (";") + 1, objData.Length - (objData.IndexOf (";") + 1)));
		for (int i = 0; i < count; i++)
		{
			if (currSeason == i)
			{
				string name1 = generator.unitsSources.unit [currEnemy].name;
				string name2 = name1.Substring (0, 1).ToUpper () + name1.Substring (1, name1.Length - 1);
				current.activeSeasons [i].fighterSeasons.Add (new EnemySeason (generator.unitsSources.unit[currEnemy].name + " " + current.activeSeasons [i].fighterSeasons.Count.ToString(), (EnemyType)System.Enum.Parse (typeof(EnemyType), name2), generator.unitsSources.unit[currEnemy].direction, 1.0f, Disposable.Repeat));
				current.activeSeasons [i].fighterSeasons [current.activeSeasons [i].fighterSeasons.Count - 1].Difficult = new Vector2 (0.0f, 1.0f);
			}
		}
	}

	void CallbackMenu11 (object obj)
	{

		int count = current.activeSeasons.Count;
		string objData = obj.ToString ();
		int currEnemy = int.Parse(objData.Substring (0, objData.IndexOf (";")));
		int currSeason = int.Parse(objData.Substring (objData.IndexOf (";") + 1, objData.Length - (objData.IndexOf (";") + 1)));
		for (int i = 0; i < count; i++) {
			if (currSeason == i)
			{
				string name1 = generator.unitsSources.unit [currEnemy].name;
				string name2 = name1.Substring (0, 1).ToUpper () + name1.Substring (1, name1.Length - 1);
				current.activeSeasons [i].attackerSeasons.Add (new EnemySeason (generator.unitsSources.unit[currEnemy].name + " " + current.activeSeasons [i].attackerSeasons.Count.ToString(), (EnemyType)System.Enum.Parse (typeof(EnemyType), name2), generator.unitsSources.unit[currEnemy].direction, 1.0f, Disposable.Repeat));
				current.activeSeasons [i].attackerSeasons [current.activeSeasons [i].attackerSeasons.Count - 1].Difficult = new Vector2 (0.0f, 1.0f);
			}
		}
	}

	void CallbackMenu12 (object obj)
	{
		int count = current.activeSeasons.Count;
		string objData = obj.ToString ();
		int currEnemy = int.Parse(objData.Substring (0, objData.IndexOf (";")));
		int currSeason = int.Parse(objData.Substring (objData.IndexOf (";") + 1, objData.Length - (objData.IndexOf (";") + 1)));
		for (int i = 0; i < count; i++) {
			if (currSeason == i)
			{
				string name1 = generator.unitsSources.unit [currEnemy].name;
				string name2 = name1.Substring (0, 1).ToUpper () + name1.Substring (1, name1.Length - 1);
				current.activeSeasons [i].neutralSeasons.Add (new EnemySeason (generator.unitsSources.unit[currEnemy].name + " " + current.activeSeasons [i].neutralSeasons.Count.ToString(), (EnemyType)System.Enum.Parse (typeof(EnemyType), name2), generator.unitsSources.unit[currEnemy].direction, 1.0f, Disposable.Repeat));
				current.activeSeasons [i].neutralSeasons [current.activeSeasons [i].neutralSeasons.Count - 1].Difficult = new Vector2 (0.0f, 1.0f);
			}
		}
	}

	void CallbackMenu13 (object obj)
	{
		int count = current.activeSeasons.Count;
		string objData = obj.ToString ();
		int currMisc = int.Parse(objData.Substring (0, objData.IndexOf (";")));
		int currSeason = int.Parse(objData.Substring (objData.IndexOf (";") + 1, objData.Length - (objData.IndexOf (";") + 1)));
		for (int i = 0; i < count; i++) {
			if (currSeason == i)
			{
				string name1 = generator.miscSources.unit [currMisc].name;
				string name2 = name1.Substring (0, 1).ToUpper () + name1.Substring (1, name1.Length - 1);
				current.activeSeasons [i].miscSeasons.Add (new MiscSeason (generator.miscSources.unit[currMisc].name + " " + current.activeSeasons [i].miscSeasons.Count.ToString(), (MiscType)System.Enum.Parse (typeof(MiscType), name2), 1.0f, Disposable.Repeat));
				current.activeSeasons [i].miscSeasons [current.activeSeasons [i].miscSeasons.Count - 1].Difficult = new Vector2 (0.0f, 1.0f);
			}
		}
	}

	void CallbackMenu02 (object obj)
	{
		int count = currentBiomesSources.biomesSource.Count;
		for (int i = 0; i < count; i++) {
			if (obj.ToString () == i.ToString ()) {
				Debug.Log (currentBiomesSources.biomesSource [i].name);
				AddActiveSeason (i, currentWay, currentPos);
				currentMode = MapEditorMode.ActiveSettings;
				return;
			}
		}


	}

	void CallbackMenu03 (object obj)
	{
		currentMode = MapEditorMode.ActiveSettings;
	}

	void Menu03 (int roadNumber)
	{
		
		GenericMenu menu = new GenericMenu ();
		menu.AddItem (new GUIContent ("Edit/Default"), false, CallbackMenu04Default, roadNumber.ToString ());
		menu.AddItem (new GUIContent ("Edit/Custom"), false, CallbackMenu04Custom, roadNumber.ToString ());
		menu.AddItem (new GUIContent ("Edit/Special"), false, CallbackMenu04Special, roadNumber.ToString ());
		RoadSourceType type = current.activeSeasons [currentNumber].roadElements [roadNumber].fork;
		switch (type) {
		case RoadSourceType.None:
			menu.AddItem (new GUIContent ("Edit/Fork/AddLeft"), false, CallbackMenu04ForkLeft, roadNumber.ToString ());
			menu.AddItem (new GUIContent ("Edit/Fork/AddRight"), false, CallbackMenu04ForkRight, roadNumber.ToString ());
			break;
		case RoadSourceType.ForkLeft:
			menu.AddDisabledItem (new GUIContent ("Edit/Fork/AddLeft"));
			menu.AddItem (new GUIContent ("Edit/Fork/AddRight"), false, CallbackMenu04ForkRight, roadNumber.ToString ());
			break;
		case RoadSourceType.ForkRight:
			menu.AddItem (new GUIContent ("Edit/Fork/AddLeft"), false, CallbackMenu04ForkLeft, roadNumber.ToString ());
			menu.AddDisabledItem (new GUIContent ("Edit/Fork/AddRight"));
			break;
		case RoadSourceType.ForkBoth:
			menu.AddDisabledItem (new GUIContent ("Edit/Fork/AddLeft"));
			menu.AddDisabledItem (new GUIContent ("Edit/Fork/AddRight"));
			break;
		}
		menu.ShowAsContext ();
		Event.current.Use ();
	}

	void Menu04 (int roadNumber)
	{
			
		GenericMenu menu = new GenericMenu ();
		menu.AddItem (new GUIContent ("Connect"), false, CallbackMenu06, roadNumber.ToString ());
		menu.AddItem (new GUIContent ("Cancel"), false, CallbackMenu07, roadNumber.ToString ());
		menu.ShowAsContext ();
		Event.current.Use ();
	}

	void Menu05 ()
	{
		if (isConnectionMode) {
			isConnectionMode = false;
			return;
		}
		
		GenericMenu menu = new GenericMenu ();
		menu.AddItem (new GUIContent ("Cancel"), false, CallbackMenu08, "0");
		menu.ShowAsContext ();
		Event.current.Use ();
	}

	void CallbackMenu04Default (object obj)
	{
		currentMode = MapEditorMode.ActiveRoadSettings;
		SwitchRoadToType (currentNumber, currentRoadNumber, RoadElementType.Default, 0);
	}

	void CallbackMenu04Custom (object obj)
	{
		currentMode = MapEditorMode.ActiveRoadSettings;
		SwitchRoadToType (currentNumber, currentRoadNumber, RoadElementType.Custom, 0);
	}

	void CallbackMenu04Special (object obj)
	{
		currentMode = MapEditorMode.ActiveRoadSettings;
		SwitchRoadToType (currentNumber, currentRoadNumber, RoadElementType.Special, 0);
	}

	void CallbackMenu04ForkLeft (object obj)
	{
		currentMode = MapEditorMode.ActiveRoadSettings;
		SwitchRoadToType (currentNumber, currentRoadNumber, RoadElementType.Fork, 0);
	}

	void CallbackMenu04ForkRight (object obj)
	{
		currentMode = MapEditorMode.ActiveRoadSettings;
		SwitchRoadToType (currentNumber, currentRoadNumber, RoadElementType.Fork, 1);
	}

	void CallbackMenu05 (object obj)
	{
		isConnectionMode = true;
	}

	void CallbackMenu06 (object obj)
	{
		isConnectionMode = false;
		int numb = GetLastActiveNumber (currentWay);

		switch (current.activeSeasons [currentNumber].roadElements [currentRoadNumber].roadSourceType) {
		case RoadElementType.Default:
			{
				if (current.activeSeasons [currentNumber].wayNumber == currentWay)
					return;
				current.activeSeasons [currentNumber].roadElements [currentRoadNumber].roadSourceType = RoadElementType.Connection;

				current.activeSeasons [numb].endType = ActiveEndType.connect;
				current.activeSeasons [numb].seasonConnect = currentNumber;
				current.activeSeasons [numb].roadConnect = currentRoadNumber;

				if (CalculateY (numb) < CalculateY (currentNumber))
					current.activeSeasons [currentNumber].roadElements [currentRoadNumber].connect = RoadSourceType.ForkLeftReverse;
				else
					current.activeSeasons [currentNumber].roadElements [currentRoadNumber].connect = RoadSourceType.ForkRightReverse;
			}
			break;
		default:
			break;
		}

        
	}

	void CallbackMenu07 (object obj)
	{
		isConnectionMode = false;
	}

	void CallbackMenu08 (object obj)
	{
		int numb = currentNumber;
		if (current.activeSeasons [numb].endType == ActiveEndType.connect) {
			int season = current.activeSeasons [numb].seasonConnect;
			int road = current.activeSeasons [numb].roadConnect;

			current.activeSeasons [season].roadElements [road].roadSourceType = RoadElementType.Default;
			switch (current.activeSeasons [season].roadElements [road].connect) {
			case RoadSourceType.ForkLeftReverse:
				current.activeSeasons [season].roadElements [road].connect = RoadSourceType.None;
				current.activeSeasons [season].roadElements [road].roadSourceType = RoadElementType.Default;
				break;
			case RoadSourceType.ForkRightReverse:
				current.activeSeasons [season].roadElements [road].connect = RoadSourceType.None;
				current.activeSeasons [season].roadElements [road].roadSourceType = RoadElementType.Default;
				break;
			case RoadSourceType.ForkBothReverse:
				if (current.ways [current.activeSeasons [season].wayNumber].pos.y > current.ways [current.activeSeasons [numb].wayNumber].pos.y) {
					current.activeSeasons [season].roadElements [road].connect = RoadSourceType.ForkRight;
					current.activeSeasons [season].roadElements [road].roadSourceType = RoadElementType.Connection;
				} else {
					current.activeSeasons [season].roadElements [road].connect = RoadSourceType.ForkLeft;
					current.activeSeasons [season].roadElements [road].roadSourceType = RoadElementType.Connection;
				}
				break;
			}
				
			current.activeSeasons [numb].endType = ActiveEndType.final;
			current.activeSeasons [numb].roadConnect = 0;
			current.activeSeasons [numb].seasonConnect = 0;
		}
	}



	void CallbackMenu09 (object obj)
	{
		int count = current.activeSeasons.Count;
		for (int i = 0; i < count; i++) {
			if (obj.ToString () == i.ToString ()) {
				DeleteSeason (i);
			}
		}
	}

	void DeleteSeason (int numb)
	{
		int count = current.activeSeasons.Count;
		int previousSeason = -1;
		for (int j = 0; j < count; j++) {
			if (current.activeSeasons [j].wayNumber == current.activeSeasons [numb].wayNumber) {
				if (current.activeSeasons [j] != current.activeSeasons [numb])
					previousSeason = j;	
			}
		}
		if (previousSeason < 0)
			current.ways [current.activeSeasons [numb].wayNumber].isUsed = false;
		else
			current.activeSeasons [previousSeason].endType = ActiveEndType.final;

		//switch (current.activeSeasons [i].endType == ActiveEndType.connect) 

		current.activeSeasons.Remove (current.activeSeasons [numb]);							
	}

	void AddActiveSeason (int number, int way, Vector2 pos)
	{
		for (int i = 0; i < current.ways.Count; i++)
			if (current.ways [i].wayNumber == way) {          
				current.ways [i].isUsed = true;
				current.ways [i].side = 0;
				break;
			}
		int prevSeason = -1;
		int prevSeasonRoad = 0;
		for (int i = 0; i < current.activeSeasons.Count; i++) {
			if (current.activeSeasons [i].wayNumber == way && current.activeSeasons [i].endType == ActiveEndType.final) 
			{
				prevSeason = i;
				current.activeSeasons [i].endType = ActiveEndType.next;
				current.activeSeasons [i].seasonConnect = current.activeSeasons.Count;
				break;
			}
		}            

		if (prevSeason < 0 && current.activeSeasons.Count > 0)
		{
			prevSeason = current.ways [way].seasonMaster;
			prevSeasonRoad = current.ways [way].roadMaster;
		}

		current.activeSeasons.Add (new SeasonActive (currentBiomesSources.biomesSource [number].name + "_" + (current.activeSeasons.Count + 1).ToString (), way, number, prevSeason, prevSeasonRoad));
		int last = current.activeSeasons.Count - 1;
		current.activeSeasons [last].roadMagazine = new List<RoadSourceData> ();
		for (int i = 0; i < currentBiomesSources.biomesSource [current.activeSeasons [last].biomeNumber].roadSource.Count; i++)
		{
			
			RoadSourceData rd = new RoadSourceData ();
			RoadSourceData old = currentBiomesSources.biomesSource [current.activeSeasons [last].biomeNumber].roadSource [i];
			rd.name = old.name;
			rd.roadSourceType = old.roadSourceType;
			rd.coefficient = old.coefficient;
			rd.isActive = old.isActive;
			current.activeSeasons [last].roadMagazine.Add (rd);
		}

	}



	void StartEditorHead ()
	{
		GUI.Box (new Rect (5.0f, 5.0f, position.width - 310.0f, position.height - 10.0f), "", skin.GetStyle ("Back"));
		GUILayout.BeginArea (new Rect (5.0f, 5.0f, position.width - 310.0f, position.height - 50.0f));
	}

	void StartEditorSpace (int _height)
	{
		GUILayout.BeginArea (new Rect (5, 10 + _height, position.width - 210.0f, position.height - _height - 15.0f));
		GUI.Box (new Rect (0, 0, position.width - 320.0f, position.height - _height - 30.0f), "", skin.GetStyle ("Field"));
	}

	void EndEditorSpace (int type)
	{
		GUILayout.EndArea ();
		GUILayout.EndArea ();


		if (GUI.Button (new Rect (5.0f, position.height - 45.0f, 100, 40), "Save")) 
		{
			switch (type) {
			case 0:
				SaveUnits ();
				break;
			case 1:
				SaveMiscs ();
				break;
			}
		}
		GUI.Box (new Rect (105.0f, position.height - 45.0f, position.width - 105.0f - 305.0f, 40), "", skin.GetStyle ("Button"));
	}

	void Units ()
	{
		int height = 40;

		StartEditorHead ();
		GUI.Label (new Rect (15, 10, 300, height), "Units Source", skin.GetStyle ("Header"));
		StartEditorSpace (30);
		scrollPosition = GUILayout.BeginScrollView (scrollPosition);

		if (generator.unitsSources == null) 
		{
			LoadData ();
			return;
		}

		ShowUnitSourceHead (height);
		for (int i = 0; i < generator.unitsSources.unit.Count; i++)
			ShowUnitSource (i, height);

		GUILayout.BeginHorizontal ();
		GUILayout.Space (height + 7);

		if (GUILayout.Button ("+", GUILayout.Width (40), GUILayout.Height (40)))
		{
			UnitSource newUnitSource = new UnitSource ();
			generator.unitsSources.unit.Add (newUnitSource);
		}

		GUILayout.EndHorizontal ();
		GUILayout.EndScrollView ();
		EndEditorSpace (0);

	}

	void Miscs ()
	{
		int height = 40;

		StartEditorHead ();
		GUI.Label (new Rect (15, 10, 300, height), "Misc Source", skin.GetStyle ("Header"));
		StartEditorSpace (30);
		scrollPosition = GUILayout.BeginScrollView (scrollPosition);

		if (generator.miscSources == null) 
		{
			LoadData ();
			return;
		}

		ShowMiscSourceHead (height);
		for (int i = 0; i < generator.miscSources.unit.Count; i++)
			ShowMiscSource (i, height);

		GUILayout.BeginHorizontal ();
		GUILayout.Space (height + 7);

		if (GUILayout.Button ("+", GUILayout.Width (40), GUILayout.Height (40)))
		{
			MiscSource newMiscSource = new MiscSource ();
			generator.miscSources.unit.Add (newMiscSource);
		}

		GUILayout.EndHorizontal ();
		GUILayout.EndScrollView ();
		EndEditorSpace (0);

	}
	/*
	void Enemies ()
	{
		if (current != null) {
			int fCount = current.fighterSeasons.Count;
			int aCount = current.attackerSeasons.Count;
			int nCount = current.neutralSeasons.Count;
			int height = 30;
			float heightAdd = 2.0f;

			GUILayout.BeginArea (new Rect (5.0f, 5.0f, position.width - 320.0f, position.height - 10.0f));
			scrollPosition = GUILayout.BeginScrollView (scrollPosition);
			//

			GUI.Box (new Rect (2.0f, 10.0f, position.width - 300.0f, height + (fCount + 2) * (height + heightAdd) + 5.0f), "");
			GUILayout.Label ("Fighters", GUILayout.Height (height));
			ShowEnemySeasonHead (height);
			for (int i = 0; i < fCount; i++) {
				EnemySeason currentEnemySeason = current.fighterSeasons [i];
				ShowEnemySeason (currentEnemySeason, i, height, current.fighterSeasons);
			}
			GUILayout.BeginHorizontal ();
			GUILayout.Space (37.5f);
			if (GUILayout.Button ("+", GUILayout.Width (30), GUILayout.Height (height)))
				current.fighterSeasons.Add (new EnemySeason ("Fighters" + fCount.ToString (), EnemyType.Roadhog, EnemyDirection.Back, 1.0f, Disposable.Repeat));
			
			GUILayout.EndHorizontal ();
			//GUILayout.Space(100.0f);
			// GUILayout.EndArea();

			GUI.Box (new Rect (2.0f, height + (fCount + 2) * (height + heightAdd) + 10.0f * 2, position.width - 300.0f, height + (aCount + 2) * (height + heightAdd) + 5.0f), "");
            
			GUILayout.Label ("Attackers", GUILayout.Height (height));
			ShowEnemySeasonHead (height);
			for (int i = 0; i < aCount; i++) {
				EnemySeason currentEnemySeason = current.attackerSeasons [i];
				ShowEnemySeason (currentEnemySeason, i, height, current.attackerSeasons);
			}
			GUILayout.BeginHorizontal ();
			GUILayout.Space (37.5f);
			if (GUILayout.Button ("+", GUILayout.Width (30), GUILayout.Height (height))) {
				current.attackerSeasons.Add (new EnemySeason ("Attackers" + aCount.ToString (), EnemyType.Scorcher, EnemyDirection.Spline, 1.0f, Disposable.Repeat));
			}
			GUILayout.EndHorizontal ();
			//GUILayout.EndArea();

			GUI.Box (new Rect (2.0f, height * 2 + (fCount + 2) * (height + heightAdd) + (aCount + 2) * (height + heightAdd) + 10.0f * 3, position.width - 300.0f, height + (nCount + 2) * (height + heightAdd) + 5.0f), "");
			GUILayout.Label ("Neutrals", GUILayout.Height (height));
			ShowEnemySeasonHead (height);
			for (int i = 0; i < nCount; i++) {
				EnemySeason currentEnemySeason = current.neutralSeasons [i];
				ShowEnemySeason (currentEnemySeason, i, height, current.neutralSeasons);
			}
			GUILayout.BeginHorizontal ();
			GUILayout.Space (37.5f);
			if (GUILayout.Button ("+", GUILayout.Width (30), GUILayout.Height (height))) 
				current.neutralSeasons.Add (new EnemySeason ("Neutrals" + nCount.ToString (), EnemyType.Civilian, EnemyDirection.Traffic, 1.0f, Disposable.Repeat));
			
			GUILayout.EndHorizontal ();

			GUILayout.EndScrollView ();
			GUILayout.EndArea ();
		} else {
			Debug.Log ("9");
			LoadData ();
		}
	}
*/


    
}


public static class MyGUI {

	#region Implementation

	private static GUIStyle backgroundStyle = "Box";
	private static GUIStyle thumbStyle = "Box";
	private static GUIStyle minThumbStyle = "Button";
	private static GUIStyle maxThumbStyle = "Button";

	private static void DoMinMaxSlider(Rect position, ref float minValue, ref float maxValue, float minLimit, float maxLimit, Color color) {
		int controlID = GUIUtility.GetControlID(FocusType.Passive);
		int minThumbControlID = GUIUtility.GetControlID(FocusType.Passive);
		int maxThumbControlID = GUIUtility.GetControlID(FocusType.Passive);

		//backgroundStyle = GUI.skin.GetStyle ("DoubleSlider");
		//thumbStyle = GUI.skin.GetStyle ("DoubleSlider");
		//minThumbStyle = "Button";
		//maxThumbStyle = "Button";

		// Do not proceed for layout event.
		if (Event.current.type == EventType.Layout)
			return;

		// Clamp current state of values.
		minValue = Mathf.Clamp(minValue, minLimit, maxLimit);
		maxValue = Mathf.Max(minValue, Mathf.Clamp(maxValue, minLimit, maxLimit));

		// Calculate normalized version of values.
		float range = Mathf.Abs(maxLimit - minLimit);
		float normalizedMinValue = (minValue - minLimit) / range;
		float normalizedMaxValue = (maxValue - minLimit) / range;

		// Calculate visual version of values.
		float minValueX = position.x + normalizedMinValue * position.width;
		float maxValueX = position.x + normalizedMaxValue * position.width;

		Rect minThumbPosition = new Rect(minValueX, position.y+2, 15, position.height-2);
		Rect maxThumbPosition = new Rect(maxValueX - 15, position.y+2, 15, position.height-2);
		Rect both = new Rect(minValueX, position.y+2, maxValueX-minValueX, position.height-2);

		float normalizedMousePosition;


		switch (Event.current.GetTypeForControl(controlID))
		{
		case EventType.MouseDown:

			if (Event.current.button == 0) 
			{
				if (new Rect (position.x-4, position.y, position.width+4, position.height).Contains (Event.current.mousePosition)) 
				{
					
					normalizedMousePosition = (Event.current.mousePosition.x - position.x) / position.width;
					//Debug.Log (normalizedMousePosition.ToString("F2"));
					if (normalizedMousePosition < 0)
						minValue = 0;
					else 
						if 
					(normalizedMousePosition > 0.98f)
						maxValue = 1.0f;
					else {
							if (Mathf.Abs (normalizedMousePosition - minValue) < Mathf.Abs (normalizedMousePosition - maxValue))
								minValue = normalizedMousePosition;
						else
								maxValue = normalizedMousePosition;
					}

					Event.current.Use();
				}
			}
			// Mouse pressed down on minimum thumb position?
			if (minThumbPosition.Contains(Event.current.mousePosition)) 
			{
				GUIUtility.hotControl = minThumbControlID;
				Event.current.Use();


			}
			// Mouse pressed down on maximum thumb position?
			else if (maxThumbPosition.Contains(Event.current.mousePosition)) {
				GUIUtility.hotControl = maxThumbControlID;
				Event.current.Use();
			}
			break;

		case EventType.MouseUp:
			// Extremely important!!
			if (GUIUtility.hotControl == minThumbControlID || GUIUtility.hotControl == maxThumbControlID || GUIUtility.hotControl == controlID)
				GUIUtility.hotControl = 0;
			break;

		case EventType.MouseDrag:
			normalizedMousePosition = (Event.current.mousePosition.x - position.x) / position.width;

			// Process mouse movement?
			if (GUIUtility.hotControl == minThumbControlID) {
				float newMinValue = Mathf.Clamp(normalizedMousePosition * range + minLimit, minLimit, maxValue);
				if (newMinValue != minValue) {
					minValue = newMinValue;
					GUI.changed = true;
				}
				Event.current.Use();
			}
			else if (GUIUtility.hotControl == maxThumbControlID) {
				float newMaxValue = Mathf.Clamp(normalizedMousePosition * range + minLimit, minValue, maxLimit);
				if (newMaxValue != maxValue) {
					maxValue = newMaxValue;
					GUI.changed = true;
				}
				Event.current.Use();
			}
			break;

		case EventType.Repaint:
			// Draw background of slider control.
			Color mm = GUI.backgroundColor;
			GUI.backgroundColor = Color.white;
			backgroundStyle.Draw (new Rect (position.x, position.y + 5, position.width-5, position.height - 10), GUIContent.none, controlID);
			// Draw background of thumb range.
			//thumbStyle.Draw(new Rect(minValueX, position.y + 9, maxValueX - minValueX, 5), GUIContent.none, false, false, false, false);
			// Draw minimum thumb button.

			GUI.backgroundColor = color;
			minThumbStyle.Draw (both, GUIContent.none, minThumbControlID);

			//GUI.backgroundColor = Color.white;
			//minThumbStyle.Draw (minThumbPosition, GUIContent.none, minThumbControlID);
			// Draw maximum thumb button.
			//maxThumbStyle.Draw (maxThumbPosition, GUIContent.none, maxThumbControlID);

			GUI.backgroundColor = mm;
			GUI.Label (new Rect (position.x, position.y+5, 50.0f, 15), minValue.ToString ("F2"));
			GUI.Label (new Rect (position.x + position.width - 50.0f, position.y+5, 50.0f, 15), maxValue.ToString ("F2"));
			break;
		}
	}

	#endregion

	#region Absolute Version

	public static void MinMaxSlider(Rect position, ref float minValue, ref float maxValue, float minLimit, float maxLimit, Color color) {
		DoMinMaxSlider(position, ref minValue, ref maxValue, minLimit, maxLimit, color);
	}

	#endregion

	#region Layout-enabled Version

	public static void MinMaxSlider(ref float minValue, ref float maxValue, float minLimit, float maxLimit, Color color) {
		Rect position = GUILayoutUtility.GetRect(GUIContent.none, backgroundStyle);
		DoMinMaxSlider(position, ref minValue, ref maxValue, minLimit, maxLimit, color);
	}

	#endregion

}