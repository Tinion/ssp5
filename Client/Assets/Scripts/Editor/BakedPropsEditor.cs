﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor (typeof(BakedProps))]
[CanEditMultipleObjects]
public class BakedPropsEditor : MultiEditor {



	public override void OnInspectorGUI ()
	{

		BakedProps myTarget = (BakedProps)target;

		myTarget.corpse = (Corpse)EditorGUILayout.ObjectField("Corpse", myTarget.corpse, typeof(Corpse));

		if (myTarget.parentBlockPlane != null) {
			string[] customBordersNames = new string[myTarget.parentBlockPlane.customBorders.Count + 1];
			customBordersNames [0] = myTarget.parentBlockPlane.mainBorder.name;
			for (int i = 1; i < myTarget.parentBlockPlane.customBorders.Count + 1; i++) {
				customBordersNames [i] = myTarget.parentBlockPlane.customBorders [i - 1].name;
			}
		
			GUILayout.BeginHorizontal ();


			myTarget.selectedType = EditorGUILayout.Popup (myTarget.selectedType, customBordersNames, GUILayout.Height(25));
			if (GUILayout.Button ("Change", GUILayout.Height(25)))
			{
				myTarget.parentBlockPlane.bordersHub.ChangeBakedProps (myTarget, myTarget.selectedType);

			}

			GUILayout.EndHorizontal ();
		}
		if (GUILayout.Button ("Remove", GUILayout.Height(25))) 
		{
			
			BordersHub.RemoveBorders ();

		}
	}
}