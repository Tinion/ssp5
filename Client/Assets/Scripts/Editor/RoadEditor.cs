﻿using UnityEngine;
using System.Collections;
using UnityEditor;


[CustomEditor (typeof(Road))]
public class RoadEditor : Editor {

	public override void OnInspectorGUI()
	{
		Road myTarget = (Road)target;

		GUILayout.Space (3);
		EditorGUILayout.BeginVertical ("Box");

		GUILayout.Space (4);

		if (!myTarget.isEdit) {
			if (myTarget.collider == null)
			myTarget.collider = myTarget.transform.GetComponent<MeshCollider> ();
			GUI.color = Color.green;
			if (GUILayout.Button ("Start Edit"))
				myTarget.isEdit = !myTarget.isEdit;
			GUI.color = Color.white;
		} else
		{
			GUI.color = Color.red;
			if (GUILayout.Button ("Stop Edit")) 
			{
				myTarget.isEdit = !myTarget.isEdit;
				Collider col = myTarget.collider;
				DestroyImmediate (myTarget);
				DestroyImmediate (col);
			}
			GUILayout.Space (3);
			GUILayout.Label ("Road", EditorStyles.boldLabel);
			GUI.color = Color.white;

			myTarget.tabSwitch = GUILayout.Toolbar (myTarget.tabSwitch, new string[] {"Decal", "Cracks", "Border Size", "Border Type"});
			myTarget.size = EditorGUILayout.Slider (myTarget.size, myTarget.sizeMinMax.x, myTarget.sizeMinMax.y);
			myTarget.opacity = EditorGUILayout.Slider (myTarget.opacity, 0.1f, 1.0f);

			GUILayout.Space (3);
			GUILayout.Label ("Translation", EditorStyles.boldLabel);
			if (GUILayout.Button ("Translation Apply")) 
			{
				TranslationApply ();
			}
			if (GUILayout.Button ("Force Save Mesh")) 
				SaveRoad();

		}


		GUILayout.EndVertical ();


	}

	void OnSceneGUI()
	{
		
		bool isShift = false;
		Road myTarget = (Road)target;

		if (!myTarget.isEdit)
			return;
		
		if (myTarget.collider == null)
			myTarget.GetComponent<MeshCollider> ();
		
		Ray ray = HandleUtility.GUIPointToWorldRay (Event.current.mousePosition);	
		RaycastHit hit;
		bool flag = myTarget.collider.Raycast (ray, out hit, Mathf.Infinity);

		Event current = Event.current;

		int controlID = GUIUtility.GetControlID (FocusType.Passive);

		if (Event.current.button == 1) {
			HandleUtility.AddDefaultControl (controlID);
			return;
		}

		if (Event.current.alt) {
			HandleUtility.AddDefaultControl (controlID);
			return;
		}

		if (Event.current.shift) {
			isShift = true;
		}

		switch (current.GetTypeForControl (controlID)) 
		{
		case EventType.MouseDown:
			GUIUtility.hotControl = controlID;
			//Overall (hit.point, isShift);
			current.Use ();
			break;
		case EventType.MouseUp:
			GUIUtility.hotControl = 0;
			//Overall (hit.point, isShift);
			current.Use ();
			break;
		case EventType.MouseDrag:
			GUIUtility.hotControl = controlID;
			//Check (hit.point);
			Overall (hit.point, isShift);
			current.Use ();
			break;
		case EventType.ScrollWheel:
			if (isShift) 
			{
				GUIUtility.hotControl = 0;
				ChangeSize (current.delta.y);
				current.Use ();

			} 
			break;
		}
		if (flag)
		{
			Handles.color = new Color (1, 0, 0, Mathf.Max (0.1f, myTarget.opacity * 0.5f));
			Handles.DrawWireDisc (hit.point, hit.normal, myTarget.size);
			Handles.color = new Color (1, 0, 0, Mathf.Max (0.6f, myTarget.opacity));
			Handles.DrawWireDisc (hit.point, hit.normal, myTarget.size * Mathf.Max (0.3f, 1));

			//Check (hit.point);

		}
	}

	void ChangeSize(float delta)
	{
		Road myTarget = (Road)target;
		if (delta > 0) {
			if (myTarget.size + delta * 0.1f >= myTarget.sizeMinMax.y)
				myTarget.size = myTarget.sizeMinMax.y;
			else
				myTarget.size += delta * 0.1f;
		} 
		else
		{
			if (myTarget.size + delta * 0.1f <= myTarget.sizeMinMax.x)
				myTarget.size = myTarget.sizeMinMax.x;
			else
				myTarget.size += delta * 0.1f;
		}
			
	}

	public void Check(Vector3 point)
	{
		Road myTarget = (Road)target;

		Mesh mesh = myTarget.collider.sharedMesh;
		for (int i = 0; i < mesh.vertices.Length; i++) 
		{
			Vector3 pos = mesh.vertices[i];
			if (Vector3.Distance(point, pos) < myTarget.size)
				{
					Handles.color = mesh.colors [i];
					Handles.DrawLine (pos, pos + Vector3.up);
				}
		}
	}

	public void Overall(Vector3 point, bool isShift)
	{
		
		Road myTarget = (Road)target;
		Transform transformRoad = myTarget.transform;
		MeshRenderer MR = transformRoad.GetComponent<MeshRenderer> ();
		Mesh mesh = myTarget.collider.sharedMesh;
		Vector2[] uv3;
		uv3 = new Vector2[mesh.vertices.Length];
		Color[] color;
		if (mesh.colors != null) {
			Debug.Log ("A");
			color = mesh.colors;
		} else {
			Debug.Log ("B");
			color = new Color[mesh.vertices.Length];
		}
		
		for (int i = 0; i < mesh.vertices.Length; i++) 
		{
			//if (i < 100)
			//	Debug.Log (mesh.vertices[i]);
			float a = Mathf.Abs(mesh.vertices[i].x/280);
			float b = (a >= 0.5f) ? 1 : 0;
			uv3[i] = new Vector2 (a, b);
			Vector3 pos = transformRoad.TransformPoint(mesh.vertices[i]);
			if (Vector3.Distance (point, pos) < myTarget.size) 
			{
				//Color temp;
				//if (color [i] != null)
				Color 	temp = color [i];
				//else
				//	temp = Color.black;
				switch (myTarget.tabSwitch) 
				{
				case 0:
					temp.a = (isShift) ? Mathf.Min (1.0f, temp.a + myTarget.opacity) : Mathf.Max (0.0f, temp.a - myTarget.opacity);
					break;
				case 1:
					temp.r = (isShift) ? Mathf.Min (1.0f, temp.r + myTarget.opacity) : Mathf.Max (0.0f, temp.r - myTarget.opacity);
					break;
				case 2:
					temp.b = (isShift) ? Mathf.Min (1.0f, temp.b + myTarget.opacity) : Mathf.Max (0.0f, temp.b - myTarget.opacity);
					break;
				case 3:
					temp.g = (isShift) ? Mathf.Min (1.0f, temp.g + myTarget.opacity) : Mathf.Max (0.0f, temp.g - myTarget.opacity);
					break;

				}
				color [i] = temp;
			}

		}

		mesh.name = "roadMesh";
		mesh.colors = color;
	    mesh.uv3 = uv3;
		myTarget.collider.sharedMesh = mesh;
	}

	public void TranslationApply()
	{
		Road myTarget = (Road)target;
		Transform transformRoad = myTarget.transform;
		Mesh mesh = myTarget.collider.sharedMesh;
		MeshRenderer MR = transformRoad.GetComponent<MeshRenderer> ();

		Vector2[] uv3;
		uv3 = new Vector2[mesh.vertices.Length];

		for (int i = 0; i < mesh.vertices.Length; i++) 
		{
			float a = Mathf.Abs (mesh.vertices [i].z / (MR.bounds.extents.z * 2));
			float b = (a >= 0.5f) ? 1 : 0;
			uv3[i] = new Vector2 (a, b);
		}

		mesh.name = "roadMesh";
		mesh.uv3 = uv3;
		myTarget.collider.sharedMesh = mesh;
	}

	public void SaveRoad()
	{
		Road myTarget = (Road)target;
		Transform transformRoad = myTarget.transform;
		MeshFilter MF = transformRoad.GetComponent<MeshFilter> ();

		BlockPlane blockPlane = transformRoad.parent.parent.GetComponent<BlockPlane> ();
		string newName = blockPlane.blockName + "_road";
		Selection.gameObjects [0].name = newName;

		PrefabCollector.CreateOrReplaceAssetMesh(MF.sharedMesh, "Assets/Art/roads/" + blockPlane.landType.ToString().ToLower() + "/" + blockPlane.blockName.ToLower() + "/road/" + newName + ".asset");
	}
			
}