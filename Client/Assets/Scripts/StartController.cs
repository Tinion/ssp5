﻿using UnityEngine;
using System.Collections;

public enum StartControllerType
{
	Inactive = 0,
	FreeActivator = 1,
	BattleActivator = 2,
	EndLevelActivator = 3,
	ReturnToBaseActivator = 4,
}

public class StartController : MonoBehaviour {


    public Quad2dMarker marker;
	public StartControllerType sctype;
	public bool isCheck = false;

	void Update () {

        if (marker.Check(Main.instance.hero.transform.position, true) && !isCheck)
        {
			switch (sctype) 
			{
			case StartControllerType.FreeActivator:
				//Main.instance.mainCamera.SwitchBaseToFree ();
				break;
			case StartControllerType.BattleActivator:
				Main.instance.PrepareBattle ();
				break;
			case StartControllerType.EndLevelActivator:
				Main.instance.EndMission ();
				break;
			case StartControllerType.ReturnToBaseActivator:
				Main.instance.EnterToHanger ();
				break;
			}
			isCheck = true;
        }
	}
}
