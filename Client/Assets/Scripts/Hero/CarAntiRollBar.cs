using UnityEngine;
using System.Collections;

public class CarAntiRollBar : MonoBehaviour
{

    public WheelCollider WheelL;
    public WheelCollider WheelR;
    public float AntiRoll;
    public float AntiRollFactor;
    public float AntiRollBias;
    public int StrictMode = 0;

    // Datos para telemetrнa

    private float extensionLeft = 0.0f;
    private float extensionRight = 0.0f;
    private float antiRollForce = 0.0f;
    private float antiRollRatio = 0.0f;

    float getExtensionL() { return extensionLeft; }
    float getExtensionR() { return extensionRight; }
    float getAntiRollForce() { return antiRollForce; }
    float getAntiRollRatio() { return antiRollRatio; }
    float getAntiRollTravel() { return extensionLeft - extensionRight; }

    // Implementaciуn de la barra estabilizadora

    void FixedUpdate()
    {
        WheelHit hitL;
        WheelHit hitR;

        bool groundedL = WheelL.GetGroundHit(out hitL);
        if (groundedL)
            extensionLeft = (-WheelL.transform.InverseTransformPoint(hitL.point).y - WheelL.radius) / WheelL.suspensionDistance;
        else
            extensionLeft = 1.0f;

        bool groundedR = WheelR.GetGroundHit(out hitR);
        if (groundedR)
            extensionRight = (-WheelR.transform.InverseTransformPoint(hitR.point).y - WheelR.radius) / WheelR.suspensionDistance;
        else
            extensionRight = 1.0f;

        antiRollRatio = Bias(extensionLeft - extensionRight, AntiRollBias);
        antiRollForce = antiRollRatio * AntiRoll * AntiRollFactor;

        if (groundedL || StrictMode == 2)
            GetComponent<Rigidbody>().AddForceAtPosition(WheelL.transform.up * -antiRollForce, WheelL.transform.position);
        else if (StrictMode == 1)
            GetComponent<Rigidbody>().AddForce(WheelL.transform.up * -antiRollForce);

        if (groundedR || StrictMode == 2)
            GetComponent<Rigidbody>().AddForceAtPosition(WheelR.transform.up * antiRollForce, WheelR.transform.position);
        else if (StrictMode == 1)
            GetComponent<Rigidbody>().AddForce(WheelL.transform.up * antiRollForce);
    }


    private float lastExponent = 0.0f;
    private float lastBias = -1.0f;

    private float BiasRaw(float x, float fBias)
    {
        if (x <= 0.0f) return 0.0f;
        if (x >= 1.0f) return 1.0f;

        if (fBias != lastBias)
        {
            if (fBias <= 0.0f) return x >= 1.0f ? 1.0f : 0.0f;
            else if (fBias >= 1.0f) return x > 0.0f ? 1.0f : 0.0f;
            else if (fBias == 0.5f) return x;

            lastExponent = Mathf.Log(fBias) * -1.4427f;
            lastBias = fBias;
        }

        return Mathf.Pow(x, lastExponent);
    }

    private float Bias(float x, float fBias)
    {
        float fResult;

        fResult = fBias <= 0.5f ? BiasRaw(Mathf.Abs(x), fBias) : 1.0f - BiasRaw(1.0f - Mathf.Abs(x), 1.0f - fBias);

        return x < 0.0f ? -fResult : fResult;
    }
}












