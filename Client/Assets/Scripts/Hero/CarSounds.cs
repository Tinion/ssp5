﻿using UnityEngine;
using System.Collections;

public class CarSounds : MonoBehaviour 
{
    private Hero hero;
    public AudioSource EngineAudio;
    //public Engine engine;

    public float engineRPM = 0.0f;
    float engineIdleRPM = 200.0f;
    float engineGearDownRPM = 400.0f;			// GearDown - GearUp define the range for each gear. 
    float engineGearUpRPM = 1030.0f;
    float engineMaxRPM = 8000.0f;

    public int currentGear;
    int lastGear;

    public float transmissionRPM;

    float engineGearUpSpeed = 5.0f;			// How fast are the gear shift transitions. Small values can be used to emulate true automated transmissions.
    float engineGearDownSpeed = 20.0f;

    int gearCount = 5;
    float transmissionRatio = 13.6f;

    float engineAudioIdlePitch = 0.6f;			// Pitch settings for the engine audio source
    float engineAudioMaxPitch = 3.5f;
    float engineAudioIdleVolume = 0.8f;		// Volume settings for the engine audio source
    float engineAudioMaxVolume = 1.0f;

    float engineDamp;

    void Start()
    {
        hero = transform.GetComponent<Hero>();
        //engine = hero.engine;
    }

	void Update ()
    {
		float averageWheelRate = hero.vehicle.wheelsColliderForward[0].rpm/100;

        transmissionRPM = averageWheelRate * Mathf.Rad2Deg / 6.0f;		
        transmissionRPM *= transmissionRatio;

        //engineRPM = engine.engineRPM;
        
        
        float updatedEngineRPM = 0;

        
        if (averageWheelRate < 1.0f)
        {
            currentGear = 0;
            updatedEngineRPM = engineIdleRPM + Mathf.Abs(transmissionRPM);
        }
        else
        {
                float firstGear = engineGearUpRPM - engineIdleRPM;

                if (transmissionRPM < firstGear)
                {
                    currentGear = 1;
                    updatedEngineRPM = transmissionRPM + engineIdleRPM;
                }
                else
                {
                  

                    float  gearWidth = engineGearUpRPM - engineGearDownRPM;

                    currentGear = 2 + (int)((transmissionRPM - firstGear) / gearWidth);

                    if (currentGear > gearCount)
                    {
                        currentGear = gearCount;
                        updatedEngineRPM = transmissionRPM - firstGear - (gearCount - 2) * gearWidth + engineGearDownRPM;
                    }
                    else
                        updatedEngineRPM = Mathf.Repeat(transmissionRPM - firstGear, gearWidth) + engineGearDownRPM;
                }
            }

        if (currentGear != lastGear)
        {
            engineDamp = currentGear > lastGear ? engineGearUpSpeed : engineGearDownSpeed;
            lastGear = currentGear;
        }
        
        engineRPM = Mathf.Lerp(engineRPM, updatedEngineRPM, engineDamp * Time.deltaTime);
        
        
       // if (engineRPM > 1500.0f)
        //    engineRPM = 1500.0f;
        
        if (EngineAudio)
            ProcessContinuousAudio(EngineAudio, engineRPM, engineIdleRPM, engineMaxRPM, engineAudioIdlePitch, engineAudioMaxPitch, engineAudioIdleVolume, engineAudioMaxVolume);

	}

    void ProcessContinuousAudio(AudioSource Audio, float audioValue, float audioMin, float audioMax, float minPitch, float maxPitch, float minVolume, float maxVolume)
	{
	float audioRatio = Mathf.InverseLerp(audioMin, audioMax, audioValue);
	
	Audio.pitch = Mathf.Lerp(minPitch, maxPitch, audioRatio);
	Audio.volume = Mathf.Lerp(minVolume, maxVolume, audioRatio);
	
	if (!Audio.isPlaying) Audio.Play();
	Audio.loop = true;
	}

    void onGUI()
    {
        //GUIStyle style = new GUIStyle();
        //style.fontSize = 36;
        //style.alignment = TextAnchor.MiddleCenter;
        //GUI.Box(new Rect(270, 10, 50, 50), currentGear.ToString());
    }

    
    
}
