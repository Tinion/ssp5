﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(Hero))]
public class HeroEditor : Editor
{

    

    public override void OnInspectorGUI()
    {
        Hero myTarget = (Hero)target;

		//base.OnInspectorGUI ();
		myTarget.vehicle = EditorGUILayout.ObjectField ("Vehicle", myTarget.vehicle, typeof(Vehicle)) as Vehicle;
		GUIStyle _labelStyle;
		_labelStyle = new GUIStyle ();
		_labelStyle.alignment = TextAnchor.MiddleCenter;
		_labelStyle.clipping = TextClipping.Overflow;
		_labelStyle.fontSize = 12;

		float width = EditorGUIUtility.currentViewWidth;
		GUI.Box (GUILayoutUtility.GetRect (width, 20.0f), "Cam Percent");
		EditorGUI.ProgressBar (EditorGUILayout.GetControlRect (true, 20.0f, _labelStyle, GUILayout.Height (20.0f)), (myTarget.vehicle.forwardSpeed-myTarget.minSpeed)/(myTarget.maxSpeed-myTarget.minSpeed), "[" + myTarget.minSpeed + "] " + myTarget.vehicle.forwardSpeed + " [" + myTarget.maxSpeed + "]");
		EditorGUILayout.Separator ();
		EditorGUI.ProgressBar (EditorGUILayout.GetControlRect (true, 20.0f, _labelStyle, GUILayout.Height (20.0f)), (myTarget.accelTimer.current)/(myTarget.accelTimer.max), "Accel Timer: [" + myTarget.accelTimer.current/myTarget.accelTimer.max + "]");
		EditorGUILayout.Separator ();
		EditorGUI.ProgressBar (EditorGUILayout.GetControlRect (true, 20.0f, _labelStyle, GUILayout.Height (20.0f)), myTarget.currentSteerSpeed/100f, "Steer Speed: " + myTarget.currentSteerSpeed);
		EditorGUILayout.Separator ();

		myTarget.controlStatus = (ControlStatus)EditorGUILayout.EnumPopup("Control Status", myTarget.controlStatus);
		myTarget.accelStatus = (AccelStatus)EditorGUILayout.EnumPopup("Accel Status", myTarget.accelStatus);

        myTarget.maxSpeedMin = EditorGUILayout.FloatField("Max Speed Min", myTarget.maxSpeedMin);
        myTarget.averageSpeedMin = EditorGUILayout.FloatField("Average Speed Min", myTarget.averageSpeedMin);
        myTarget.minSpeedMin = EditorGUILayout.FloatField("Min Speed Min", myTarget.minSpeedMin);
        myTarget.minSpeedBreakMin = EditorGUILayout.FloatField("Break Speed Min", myTarget.minSpeedBreakMin);
        myTarget.steerMin = EditorGUILayout.FloatField("Steer Min", myTarget.steerMin);
        //myTarget.stiffnesMin = EditorGUILayout.FloatField("Stiffnes Min", myTarget.stiffnesMin);
        EditorGUILayout.Separator();
        myTarget.maxSpeedMax = EditorGUILayout.FloatField("Max Speed Max", myTarget.maxSpeedMax);
        myTarget.averageSpeedMax = EditorGUILayout.FloatField("Average Speed Max", myTarget.averageSpeedMax);
        myTarget.minSpeedMax = EditorGUILayout.FloatField("Min Speed Max", myTarget.minSpeedMax);
        myTarget.minSpeedBreakMax = EditorGUILayout.FloatField("Break Speed Max", myTarget.minSpeedBreakMax);
        myTarget.steerMax = EditorGUILayout.FloatField("Steer Max", myTarget.steerMax);
       // myTarget.stiffnesMax = EditorGUILayout.FloatField("Stiffnes Max", myTarget.stiffnesMax);
        EditorGUILayout.Separator();
        //myTarget.minAntiRollBars = EditorGUILayout.FloatField("Min AntiRoll Bars", myTarget.minAntiRollBars);
        //myTarget.maxAntiRollBars = EditorGUILayout.FloatField("Max AntiRoll Bars", myTarget.maxAntiRollBars);
        myTarget.steerClampMin = EditorGUILayout.FloatField("Min Steer Clamp", myTarget.steerClampMin);
        myTarget.steerClampMax = EditorGUILayout.FloatField("Max Steer Clamp", myTarget.steerClampMax);
        
        //EditorGUILayout.Separator();
        //myTarget.currentBoost = EditorGUILayout.FloatField("Boost", myTarget.currentBoost);
        //myTarget.currentAccel = EditorGUILayout.FloatField("Default", myTarget.currentAccel);
        //myTarget.currentBrake = EditorGUILayout.FloatField("Brake", myTarget.currentBrake);

		EditorGUILayout.Separator();
		myTarget.isAutopilot = EditorGUILayout.Toggle ("Autopilot", myTarget.isAutopilot);
		myTarget.isCalcVertical = EditorGUILayout.Toggle ("CalcVertical", myTarget.isCalcVertical);
        /*
        if (!myTarget.isBuilding)
        {
            if (GUILayout.Button("Build Level"))
            {
                myTarget.BuildLevel();
            }
            else
                if (GUILayout.Button("Save Level"))
                {
                    myTarget.SaveLevel();
                }
        }
         */

        //if (GUILayout.Button("Check"))
        //{
        //    myTarget._topLineCollider.Check();
        //}
    }
}
