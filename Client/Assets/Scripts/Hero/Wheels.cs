﻿using UnityEngine;
using System.Collections;

public class Wheels : Module
{
    public Mesh sourceWheel;

    public float addSpeed;
    public float addMaxMass;

    public string additionalName;
    public string additionalType;
    public float factorSlow;
    public float resistanceSlip;

    public override void Install()
    {
        transform.parent = Main.instance.hero.transform;
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;
        gameObject.SetActive(true);

		for (int i=0; i<Main.instance.hero.vehicle.wheels.Length; i++)
        {
            MeshFilter mf;
			mf = Main.instance.hero.vehicle.wheels[i].wheelTransform.GetComponent<MeshFilter>();
        mf.mesh = sourceWheel;
        }
        
        
        
    }

    void OnCollisionEnter(Collision collision)
    {
        Debug.Log("FF");

    }
    
}
