﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Dest.Math;
using Dest.Math.Tests;
//using UnityEditor;



[System.Flags]
public enum ControlStatus
{
    Default = 0,
    Boost = 1,
    Brake = 2,
    ImpactControl = 3,
}

[System.Flags]
public enum AccelStatus
{
    Default = 0,
    ForwardBoost = 1,
    ForwardAverage = 2,
    ForwardBrake = 3,
    ForwardEnd = 4,
    BackBoost = 5,
    BackAverage = 6,
    BackBrake = 7,
    BackEnd = 8,
}

public class Hero : MonoBehaviour
{
	public static Hero instance;
	public Vehicle vehicle;

    public ControlStatus controlStatus;
    public ModuleSystem moduleSystem;
    public AccelStatus accelStatus;
    
    private float engineTurbo = 0;

    public bool isAutopilot;
    bool isCheats = false;
    private bool reverseBreaking = false;
    private bool partialVertial = false;
    public bool isMoving = false;
	public bool isCalcVertical = true;

    public Transform centerUIpos;

    public bool isArmorActive = true;
    public CounterTimerF ArmorFront;
    public CounterTimerF ArmorTimer;

    public float speedFactor;
    public float currentVertical;
    public float partialCounts;
    public float verticalSlice;
    public CounterTimerF speedFactorUpCoeff;
    public CounterTimerF speedFactorDownCoeff;

    public CounterTimerF accelTimer;
    public CounterTimerF forwardSpeedTactTimer;
    public CounterTimerF shieldPoints;
    public CounterTimerF expiriencePoints;
    public int goldCoins;

    public float boostAccelSpeed = 3;
    public float boostCamSpeed = 0.03f;
    public float boostAccelBrake = 3;
    public float boostCamBrake = 0.03f;

    public Light leftStopSignal;
    public Light rightStopSignal;

	private Vector3 _hangarDot = Vector3.zero;

	float motorTorque = 0;
	float brakeTorque = 0;

	public Transform hood;
    //public Object punchProcess;
    // public Object punchImpact;
    //public bool isPunch = false;
    //public Unit punchTarget;
    //GameObject punchProceed;
    //public CounterTimerF punchPowerTimerOn;
    //public CounterTimerF punchPowerTimerOff;
    // public float[] SideFactors;

    private float _inputAccel = 0.0f;
    private float _inputSteer = 0.0f;

    public float forwardSpeedTact = 0;
    public float currentArmor;
    public float[] sideArmor;

    public float maxSpeedMin;
    public float averageSpeedMin;
    public float minSpeedMin;
    public float minSpeedBreakMin;

    public float maxSpeedMax;
    public float averageSpeedMax;
    public float minSpeedMax;
    public float minSpeedBreakMax;

    public float steerMin;
    public float steerMax;
    public float steerClampMin;
    public float steerClampMax;

    public float maxSpeed;
    public float averageSpeed;
    public float minSpeed;
    public float minSpeedBreak;


    public Vector3 angularVerticalVelocity;

    private bool isVertialCalc = false;
    private float dist;
    private float distMax;
    private Quaternion rotationUp;
    public bool isCollisionBlock;

    public Object crushSource;

    public CounterTimerF energyLimit;

    public Transform crushCenter;

    public CounterTimerF startBoostTimerFree;

    //Minimap
    public Vector2i currentResolution;

    public Slider speedSlider;
    
    public Image sliderBack;
    public Image ArmorFrontUI;
    public Image ArmorDoll;
    public Text speedText;


	public OneSpline currentSpline;
	public OneCubicBezierCurve currentCurve;
	public float currentStep;
	public Vector3 currentSplinePos;

	public Matrix4x4 currentSplineSpace;
	public Vector3 localSplinePos;
	public Vector3 currentHighwayDirection;
	public float currentSplineOffset;

	public float verticleSliceNormal;
	public float boostSlice;
	public float currentSteerSpeed = 0;
    public void Initialize(bool activeObject)
    {
		if (vehicle == null)
			vehicle = GetComponent<Vehicle> ();
		
		vehicle.Initialize(activeObject);
		instance = this;
        //currentRigidbody.velocity = transform.forward * 10.0f;
		forwardSpeedTact = vehicle.forwardSpeed;


		vehicle.currentRigidbody.centerOfMass = vehicle.centerOfMass.localPosition;

        controlStatus = ControlStatus.Default;
		vehicle.SetupWheels();

		vehicle.ApplyWheelCollector();

        currentResolution.x = Screen.width;
        currentResolution.y = Screen.height;

        StopSignals(false);

    }

	void Start () 
	{
		Initialize(true);
	}

    void ChangeSpeedFactorParameters()
    {
        float currentFactor = speedFactor;
        maxSpeed = Mathf.Lerp(maxSpeedMin, maxSpeedMax, currentFactor);
        averageSpeed = Mathf.Lerp(averageSpeedMin, averageSpeedMax, currentFactor);
        minSpeed = Mathf.Lerp(minSpeedMin, minSpeedMax, currentFactor);
        minSpeedBreak = Mathf.Lerp(minSpeedBreakMin, minSpeedBreakMax, currentFactor);
		vehicle.steerCurrent = Mathf.Lerp(steerMin, steerMax, currentFactor);
		vehicle.currentSteerClamp = Mathf.Lerp(steerClampMin, steerClampMax, currentFactor);
    }

    public void AddCoins(int number)
    {
        goldCoins += number;
        if (goldCoins < 0)
            goldCoins = 0;
    }

    public void GetDamage(float damage)
    {
		speedFactor -= (damage / vehicle.healthPoints.max) * 100;
        if (speedFactor < 0)
            speedFactor = 0;

        float currentDamage = damage;

        if (shieldPoints.current > 0)
        {
            shieldPoints.current -= damage;

            if (shieldPoints.current < 0)
            {
                currentDamage -= shieldPoints.current;
            }
            else
                return;
        }

		if (vehicle.healthPoints.current - damage > 0)
        {
			vehicle.healthPoints.current -= damage;
        }
        else
        {
            Main.instance.Lose();
			vehicle.healthPoints.current = 0;
        }
    }

    
    public void GetDamage(Vector3 point, Vector3 force)
    {
		SideTriangle sideTriangle = vehicle.GetSideTriangle(point, transform);
        float fullArmor = GetFullArmor(sideTriangle);
        if (force.magnitude > fullArmor)
        {
            GetHeroDamage(force.magnitude - fullArmor, sideTriangle);
        }
    }

    public void GetDamageUp(Vector3 point, Vector3 force)
    {
		SideTriangle sideTriangle = vehicle.GetSideTriangle(point, transform);
        float fullArmor = GetFullArmor(sideTriangle);
        if (force.magnitude > fullArmor)
        {
            float damage = force.magnitude - fullArmor;
            if (moduleSystem.module[0] != null)
                damage -= moduleSystem.module[0].GetDamage(damage * Mathf.Lerp(0, 1.0f, Random.Range(0.15f, 0.6f)));

            GetHeroDamage(damage, sideTriangle);
        }
    }

    public void GetHeroDamage(float damage, SideTriangle sideTriangle)
    {
        

        switch (sideTriangle)
        {
            case SideTriangle.Top:
                if (moduleSystem.module[1] != null)
                    damage -= moduleSystem.module[1].GetDamage(damage * Mathf.Lerp(0, 1.0f, Random.Range(0.25f, 0.75f)));

            break;
                case SideTriangle.Left:
                if (moduleSystem.module[2] != null)
                    damage -= moduleSystem.module[2].GetDamage(damage * Mathf.Lerp(0, 1.0f, Random.Range(0.05f, 0.2f)));
                if (moduleSystem.module[4] != null)
                    damage -= moduleSystem.module[4].GetDamage(damage * Mathf.Lerp(0, 1.0f, Random.Range(0.2f, 0.55f)));

            break;
                case SideTriangle.Right:
                if (moduleSystem.module[2] != null)
                    damage -= moduleSystem.module[2].GetDamage(damage * Mathf.Lerp(0, 1.0f, Random.Range(0.05f, 0.2f)));
                if (moduleSystem.module[4] != null)
                    damage -= moduleSystem.module[4].GetDamage(damage * Mathf.Lerp(0, 1.0f, Random.Range(0.2f, 0.55f)));

           break;
                case SideTriangle.Back:
                if (moduleSystem.module[2] != null)
                    damage -= moduleSystem.module[2].GetDamage(damage * Mathf.Lerp(0, 1.0f, Random.Range(0.1f, 0.2f)));
                if (moduleSystem.module[3] != null)
                    damage -= moduleSystem.module[3].GetDamage(damage * Mathf.Lerp(0, 1.0f, Random.Range(0.2f, 0.55f)));

           break;

        }

        if (moduleSystem.module[0] != null)
            damage -= moduleSystem.module[0].GetDamage(damage * Mathf.Lerp(0, 1.0f, Random.Range(0.1f, 0.15f)));


        GetDamage(damage);
        Main.instance.battleRect.durabilityInfo.UpdateDurabilityInfo();
    }

    public void GetDamageSplash(float damage)
    {

        if (moduleSystem.module[1] != null)
            damage -= moduleSystem.module[1].GetDamage(damage * Mathf.Lerp(0, 1.0f, Random.Range(0.05f, 0.15f)));
        if (moduleSystem.module[2] != null)
            damage -= moduleSystem.module[2].GetDamage(damage * Mathf.Lerp(0, 1.0f, Random.Range(0.05f, 0.15f)));
        if (moduleSystem.module[3] != null)
            damage -= moduleSystem.module[3].GetDamage(damage * Mathf.Lerp(0, 1.0f, Random.Range(0.05f, 0.15f)));
        if (moduleSystem.module[4] != null)
            damage -= moduleSystem.module[4].GetDamage(damage * Mathf.Lerp(0, 1.0f, Random.Range(0.05f, 0.15f)));
        if (moduleSystem.module[0] != null)
            damage -= moduleSystem.module[0].GetDamage(damage * Mathf.Lerp(0, 1.0f, Random.Range(0.05f, 0.15f)));


        GetDamage(damage);
        Main.instance.battleRect.durabilityInfo.UpdateDurabilityInfo();
    }

    public void AddExpierence(float expierence)
    {
        expiriencePoints.current += expierence;
    }

    public float GetFullArmor(SideTriangle currentSide)
    {
        float fullArmor = currentArmor;
        switch (currentSide)
        {
            case SideTriangle.Top:
                fullArmor += sideArmor[0];
                break;
            case SideTriangle.Left:
                fullArmor += sideArmor[1];
                break;
            case SideTriangle.Right:
                fullArmor += sideArmor[1];
                break;
            case SideTriangle.Back:
                fullArmor += sideArmor[2];
                break;
        }
        return fullArmor;
    }
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Unit"))
        {

            Unit other = collision.transform.GetComponent<Unit>();


            if (other.unitType == UnitType.Enemy)
            {
                if (collision.relativeVelocity.magnitude > 5.0f)
                    speedFactor -= collision.relativeVelocity.magnitude / 50.0f;
                if (speedFactor < 0)
                    speedFactor = 0;

                /*
                if (other == punchTarget)
                {
                    //isPunch = false;
                    //coin.texture = coinDefault;
                    punchPowerTimerOn.current = punchPowerTimerOn.max;
                    GameObject punchImpactGO = (GameObject)GameObject.Instantiate(punchImpact, collision.contacts[0].point, Quaternion.identity) as GameObject;
                    punchImpactGO.transform.parent = transform;
                    other.currentRigidbody.AddForce(collision.relativeVelocity * -1500, ForceMode.Impulse);
                    other.GetDamage(collision.contacts[0].point, collision.relativeVelocity * -1500);
                }
                else
                */
                {
					float mass = collision.relativeVelocity.magnitude * Mathf.Clamp(collision.rigidbody.mass / vehicle.currentRigidbody.mass, 0, 4.0f);
					vehicle.DropText(mass.ToString("F2"));
					SideTriangle currentSide = vehicle.GetSideTriangle(collision.contacts[0].point, transform);
                    float fullArmor = GetFullArmor(currentSide);

                    if (mass > fullArmor)
                    {
                        mass -= fullArmor;

                        SideQuad heroQuad = EnemyVehicle.GetSideQuad(collision.contacts[0].point, transform);

                        /*
                        if (isArmorActive && (heroQuad == SideQuad.TopLeft || heroQuad == SideQuad.TopRight))
                        {
                            if (ArmorFront.current > mass)
                            {
                                ArmorFront.current -= mass;
                            }
                            else
                            {

                                float diff = mass - ArmorFront.current;
                                GetHeroDamage(diff, EnemyVehicle.GetSideTriangle(collision.contacts[0].point, transform));
                                isArmorActive = false;
                                ArmorDoll.color = new Color(0.5f, 0.5f, 0.5f, 0.5f);
                            }
                        }
                        else
                        {
                         */
                            GetHeroDamage(mass, currentSide);
                        //}
                    }
                    // else
                    //   onArmorShine = true;
                }

            }


        }
        if (collision.gameObject.CompareTag("Solid"))
        {
            if (collision.relativeVelocity.magnitude > 5.0f)
                speedFactor -= 0.1f;// (collision.relativeVelocity.magnitude - 5.0f) / 5.0f;
            if (speedFactor < 0)
                speedFactor = 0;
            //mass = currentRigidbody.mass;
            //Debug.Log(collision.impactForceSum.magnitude + " " + collision.relativeVelocity.magnitude);
            if (collision.relativeVelocity.magnitude > currentArmor)
				GetHeroDamage(collision.relativeVelocity.magnitude, vehicle.GetSideTriangle(collision.contacts[0].point, transform));
            // else
            //    onArmorShine = true;

        }

        if (collision.gameObject.CompareTag("Block"))
        {
            isCollisionBlock = true;
        }
        
            
        /*
        if (collision.gameObject.CompareTag("Unit") || collision.gameObject.CompareTag("Solid"))
        {
            if (collision.gameObject.CompareTag("Unit"))
            mass = collision.rigidbody.mass;
            else
            mass = currentRigidbody.mass;
			
            if (collision.relativeVelocity.magnitude > currentArmor)
               GetHeroDamage(collision.relativeVelocity.magnitude - currentArmor, Enemy.GetSideTriangle(collision.contacts[0].point, transform), mass);
            else
               onArmorShine = true;
			
        }
        */
        // Debug.Log(collision.collider.transform.parent.name);
        /*
         foreach (ContactPoint contact in collision.contacts)
            {
				
                GameObject sparks = Instantiate(Sparks, contact.point, Quaternion.Euler(contact.normal)) as GameObject;
                sparks.transform.parent = transform;
                //if (collision.gameObject.CompareTag("Enemy"))
                //_forwardstate = false;
            }
         */
    }
    /*
    [ContextMenu("Do")]
    public void CalcSide()
    {
        GameObject current = Selection.activeGameObject;
        SideTriangle thisSide = GetSideTriangle(current.transform.position, transform);
        Debug.Log(thisSide.ToString());
    }
     */
    public void Heal()
    {
        vehicle.healthPoints.Restore();
    }

    public void ShieldUp()
    {
        shieldPoints.Restore();
    }

	public void OnDrawGizmos()
	{
		Gizmos.color = Color.green;
		Gizmos.DrawSphere (currentSplinePos, 0.5f);
		Gizmos.DrawSphere (currentSplinePos, 0.5f);
	}


    void Update()
    {

		currentSpline = Highway.instance.GetSpline (transform.position);
		currentStep = currentSpline.GetStep (transform.position);
		float percent = 0;
		currentCurve = currentSpline.GetLocalCurve (currentStep, out percent);
		currentSplinePos = currentSpline.GetPointOnCurve(transform.position);
		vehicle.currentSplineDirection = currentSpline.GetTangent (currentStep) * Controller.highwayDirection;

		vehicle.worldAngle = Math3d.GetAngle(vehicle.currentSplineDirection.z, vehicle.currentSplineDirection.x, transform.forward.z, transform.forward.x);

		currentSplineSpace = Matrix4x4.TRS (currentSplinePos, Quaternion.LookRotation (vehicle.currentSplineDirection), Vector3.one);
		Matrix4x4 currentSplineSpaceInv = currentSplineSpace.inverse;
		localSplinePos = currentSplineSpaceInv.MultiplyPoint(transform.position);

		Vector2 currentSplineSegmentScale = currentSpline.GetSegmentScale (currentStep);
		if (localSplinePos.x > 0)
			currentSplineOffset = localSplinePos.x / currentSplineSegmentScale.y;
		else
			currentSplineOffset = localSplinePos.x / currentSplineSegmentScale.x;

		verticleSliceNormal = (verticalSlice + 1) / 2;
		boostSlice = Mathf.Clamp01(vehicle.forwardSpeed / averageSpeed);
		
		vehicle.DropText("FS: " + vehicle.forwardSpeed + " AS: " + averageSpeed + " MT: " + motorTorque + " BT: " + brakeTorque + " CR: " + vehicle.currentRpm);
		//DropText (currentSpline.name + " [" + currentSpline.curves.IndexOf(currentCurve) + "/" + currentSpline.curves.Count + "]: " + percent + " Offset: " + currentSplineOffset + " Controller " + verticalSlice);

		/*
        if (punchPowerTimerOff.current > 0)
        {
            punchPowerTimerOff.current -= Time.deltaTime;

            if (punchPowerTimerOff.current <= 0)
            {
                //isPunch = false;
                Destroy(punchProceed);
                punchTarget = null;
                //punchPowerTimerOn.current = punchPowerTimerOn.max;
                //coin.texture = coinDefault;
            }


        }

        if (punchPowerTimerOn.isEnd(Time.deltaTime))
        {
            //coin.texture = coinAura;
            //isPunch = true;
        }


        //if (currentResolution.y != Screen.height || currentResolution.x != Screen.width)
        //    ReSize();
/*
		if (Controller.instance.gameMode == Mode.Battle)
        {
            UpdateTireMarks(wheels[2], fxData[0]);
            UpdateTireMarks(wheels[3], fxData[1]);
        }
*/
		//DropText (offset.ToString());
    }

	public float GetSplineDirection()
	{
		return Vector2.Dot (transform.forward.ToVector2XZ (), currentSpline.GetTangent (currentStep).ToVector2XZ ());
	}
    void ReSize()
    {

        currentResolution.x = Screen.width;
        currentResolution.y = Screen.height;
        if (Main.instance.miniMapCam != null)
            Main.instance.miniMapCam.camera.rect = new Rect(15.0f / currentResolution.x, 1.0f - 345.0f / currentResolution.y, 330.0f / currentResolution.x, 330.0f / currentResolution.y);
    }

	void SpeedFactorUpdate()
	{
		//if (Main.instance.speedFactorActive && Mathf.Abs (currentSplineOffset) < Main.instance.mainCamera.CheckRoadWidth(currentStep)) {
		if (Mathf.Abs (currentSplineOffset) < 0.9f) 
		{
			if (speedFactor < 1.0f)
				speedFactor += speedFactorUpCoeff.current;
			else
				speedFactor = 1.0f;
		} else {
			speedFactor = 0.0f;
		}

		//speedText.text = "Speed " + forwardSpeed.ToString("F2") + " " + speedFactor.ToString("F2");

		if (speedSlider != null)
			speedSlider.value = speedFactor;
		if (sliderBack != null)
			sliderBack.color = Color.Lerp(Color.white, Color.red, speedFactor);

		ChangeSpeedFactorParameters();
	}



    void FixedUpdate()
    {
        
		switch (Controller.instance.gameMode)
        {
            case Mode.Battle:
                {

				//SpeedFactorUpdate ();

				/*
                    if (!isArmorActive)
                    {
                        if (ArmorTimer.isEnd(0.05f))
                        {
                            ArmorDoll.color = new Color(1.0f, 1.0f, 1.0f, 0.5f);
                            ArmorFront.Restore();
                            isArmorActive = true;
                            ArmorTimer.Restore();
                            ArmorFrontUI.color = new Color(0, 1, 0, 0.372f);
                        }
                        else
                            ArmorFrontUI.color = Color.Lerp(new Color(0, 1, 0, 0.372f), new Color(1, 0, 0, 0.372f), ArmorTimer.current / ArmorTimer.max);
                    }
                    else
                        ArmorFrontUI.color = Color.Lerp(new Color(1, 0, 0, 0.372f), new Color(0, 1, 0, 0.372f), ArmorFront.current / ArmorFront.max);
				*/

				/*
					float _step = 0;
					BlockPlane _plane = Main.instance.GetBlockPlane (transform.position, out _step);

					if (_plane == null ||_plane.type == BlockPlaneType.Outpost)
					roadDirection = transform.forward;
					else
					spline = _plane.GetSpline(transform.position);
					roadDirection = spline.GetTangent(_step);
				*/

					
					//currentSpline = Highway.instance.GetSpline (transform.position);
					//currentStep = currentSpline.GetStep (transform.position);
					//currentSplineDirection = currentSpline.GetTangent (currentStep) * Controller.instance.highwayDirection;

					
                    
                }
                break;
			/*
            case Mode.Base:
                {
				if (Main.instance.mainCamera.isContrastStetch) 
				{
					float _time = Main.instance.mainCamera.ExitFromDock ();
					currentSplineDirection = transform.forward;
					worldAngle = 0;
					ApplyColliders (0.0f, curveBoost.Evaluate(1 - _time) * 500.0f, 0.0f);
				}
                    UpdateWheels();

                }
                break;

            case Mode.Score:
                {
					Vector3 point = Main.instance.supportPoint.spline.GetPointOnCurve (transform.position);
					Main.instance.supportStep = Main.instance.supportPoint.spline.GetStep (point);
					
					Vector3 roadDirection = Main.instance.supportPoint.spline.GetTangent (Main.instance.supportStep);
				//if (Main.instance.supportPoint.type == SplineType.Left)
				//	roadDirection = Vector3.Normalize(Quaternion.Euler(0, -60, 0) * roadDirection);
				//if (Main.instance.supportPoint.type == SplineType.Right)
				//	roadDirection = Vector3.Normalize(Quaternion.Euler(0, -210, 0) * roadDirection);
					worldAngle = Math3d.GetAngle(roadDirection.z, roadDirection.x, transform.forward.z, transform.forward.x);

					Debug.DrawLine (transform.position, transform.position + roadDirection * 2, Color.red);
					Debug.DrawLine (transform.position, transform.position + transform.forward * 2, Color.blue);
					//Debug.Log (yAngle);
					//CarMove(0.0f, Mathf.Clamp(-yAngle,-1,1));
					CarMove(_inputAccel, _inputSteer);
                    UpdateWheels();
                }
                break;

		case Mode.EnterToHangar:
			{
				
				_hangarDot = Main.instance.hangar.transform.TransformPoint(Main.instance.hangar.heroPosition);
				Vector3 roadDirection = Main.instance.hangar.transform.TransformDirection(Main.instance.hangar.transform.right);

				//Main.instance.supportStep = Main.instance.supportPoint.spline.GetStep (point);
				Debug.DrawLine (_hangarDot, _hangarDot + Vector3.up * 10, Color.blue);
				Debug.DrawLine (_hangarDot, _hangarDot + roadDirection * 10, Color.blue);
				//Vector3 roadDirection = Main.instance.supportPoint.spline.GetAngle (Main.instance.supportStep);
				//if (Main.instance.supportPoint.type == SplineType.Left)
				//	roadDirection = Vector3.Normalize(Quaternion.Euler(0, -60, 0) * roadDirection);
				//if (Main.instance.supportPoint.type == SplineType.Right)
				//	roadDirection = Vector3.Normalize(Quaternion.Euler(0, -210, 0) * roadDirection);
				worldAngle = Math3d.GetAngle(roadDirection.z, roadDirection.x, transform.forward.z, transform.forward.x);


				//Debug.DrawLine (transform.position, transform.position + transform.forward * 2, Color.blue);
				//Debug.Log (yAngle);
				CarMove(_inputAccel, _inputSteer);
				UpdateWheels();

				if (Vector3.Distance (_hangarDot, transform.position) < 0.1f)
					Main.instance.ReturnToBase ();
			}
			break;
*/
		case Mode.Free:
			{
				
			}
			break;
        }
        
		CarMove(_inputAccel, _inputSteer);
		vehicle.UpdateWheels();
        //AntiRollBars();
    }

    public void InputSteer(float steer)
    {
        _inputSteer = steer;

        /*
        Unit target = getPunch(steer);
        if (target != null && isPunch)
        {
            if (punchProceed == null)
            {
                punchProceed = (GameObject)GameObject.Instantiate(punchProcess, transform.TransformPoint(Vector3.forward), Quaternion.identity) as GameObject;
                punchProceed.transform.parent = transform;
            }
            punchTarget = target;
            _inputSteer = steer * 1.5f;

        }
        else
        {
            //SpeedFactor -= 0.01f;
            if (speedFactor < 0)
                speedFactor = 0;
            _inputSteer = steer;
            punchPowerTimerOff.current = punchPowerTimerOff.max;


        }
        */
    }

    /*
    Unit getPunch(float steer)
    {
        if (steer < 0)
        {
            return PrepareForPunch(transform.TransformPoint(sideDetectors[0]), transform.right * -6);

        }
        else
            if (steer > 0)
        {
            return PrepareForPunch(transform.TransformPoint(sideDetectors[1]), transform.right * 6);
        }
        return null;
    }

    Unit PrepareForPunch(Vector3 position, Vector3 direction)
    {
        RaycastHit hit;
        BlockPlane result = null;
        int layerMask = 1 << 0;

        //////Debug.DrawRay(position, direction, Color.white);
        Vector2 aimTriangleStartPos = position.ToVector2XZ();
        Vector2 direction2D = direction.ToVector2XZ();
        Vector2 right = transform.forward.ToVector2XZ();
        Vector2 aimTriangleRangePos = direction2D + aimTriangleStartPos;
        Vector2 aimTriangleLeftPos = aimTriangleRangePos + right * 2;
        Vector2 aimTriangleRightPos = aimTriangleRangePos - right * 2;

        ////Debug.DrawLine(new Vector3(aimTriangleStartPos.x, transform.position.y, aimTriangleStartPos.y), new Vector3(aimTriangleLeftPos.x, transform.position.y, aimTriangleLeftPos.y), Color.red);
        ////Debug.DrawLine(new Vector3(aimTriangleStartPos.x, transform.position.y, aimTriangleStartPos.y), new Vector3(aimTriangleRightPos.x, transform.position.y, aimTriangleRightPos.y), Color.red);
        ////Debug.DrawLine(new Vector3(aimTriangleLeftPos.x, transform.position.y, aimTriangleLeftPos.y), new Vector3(aimTriangleRightPos.x, transform.position.y, aimTriangleRightPos.y), Color.red);


        List<Unit> targets = new List<Unit>();

        float minimum = 6 * 2;
        int select = -1;
        int count = Main.instance.unitObjects.Count;
        for (int i = 0; i < count; i++)
        {
            if (Main.instance.unitObjects[i].currentGameObject.activeInHierarchy && Main.instance.unitObjects[i].isActive)
            {
                Box2 box = TrafficController.CreateBox2(Main.instance.unitObjects[i].transform, Main.instance.unitObjects[i].currentCollider);
                Vector2[] points = box.CalcVertices();
                if (Math3d.isTriRectIntersect(aimTriangleLeftPos, aimTriangleRightPos, aimTriangleStartPos, points[0], points[1], points[2], points[3]))
                {
                    ////Debug.DrawLine(Main.instance.unitObjects[i].transform.position, Main.instance.unitObjects[i].transform.position + Vector3.up * 8.0f, Color.magenta, 5.0f);
                    targets.Add(Main.instance.unitObjects[i]);
                    float min = Mathf.Abs(transform.InverseTransformPoint(Main.instance.unitObjects[i].transform.position).x);
                    if (min < minimum)
                    {
                        select = i;
                        minimum = min;
                    }

                }

            }

        }

        if (select > -1)
        {
            return Main.instance.unitObjects[select];
        }
        else
            return null;

    }
    */

    public void InputAccel(float accel)
    {
        _inputAccel = accel;
        if (controlStatus == ControlStatus.Boost)
        {
            if (accelStatus != AccelStatus.ForwardBrake)
            {
                accelStatus = AccelStatus.ForwardBrake;
                // if (!partialVertial)
                ///     accelTimer.current = accelTimer.max;
            }
        }
        else
            if (controlStatus == ControlStatus.Brake)
            {
                if (accelStatus != AccelStatus.BackBrake)
                {
                    accelStatus = AccelStatus.BackBrake;
                    // if (!partialVertial)
                    //     accelTimer.current = accelTimer.max;
                }
            }
            else
            {
                accelStatus = AccelStatus.Default;
                controlStatus = ControlStatus.Default;
            }
    }

    public void Boost(float input)
    {
		if (Controller.instance.gameMode == Mode.Battle)
        {
            if (controlStatus == ControlStatus.Brake)
            {
                if (accelStatus != AccelStatus.BackBrake)
                {
                    accelStatus = AccelStatus.BackBrake;
                    // if (!partialVertial)
                    //    accelTimer.current = accelTimer.max;
                }
            }
            else
                controlStatus = ControlStatus.Boost;


        }
        else
            InputAccel(input);


    }

    public void Brake(float input)
    {
		if (Controller.instance.gameMode == Mode.Battle)
        {
            if (controlStatus == ControlStatus.ImpactControl)
            {
                //_backMovement = true;
            }
            else
                if (controlStatus == ControlStatus.Boost)
                {
                    if (accelStatus != AccelStatus.ForwardBrake)
                    {
                        accelStatus = AccelStatus.ForwardBrake;
                        //if (!partialVertial)
                        //   accelTimer.current = accelTimer.max;
                    }
                }
                else
                    controlStatus = ControlStatus.Brake;
        }
        else
            InputAccel(-input);
    }

    public void DebugGui()
    {
        //base.DebugGui();

        /*
        if (!isCheats)
        {
            if (GUI.Button(new Rect(30, 130, 60, 40), "Cheats"))
            {
                isCheats = true;
            }
        }
        else
        {

            GUI.BeginGroup(new Rect(25, 125, 80, 220));
            GUI.Box(new Rect(0, 0, 80, 220), "Steer");
            if (GUI.Button(new Rect(10, 25, 60, 40), "Poor"))
            {
                steerMin = 12;
                steerMax = 8;
            }

            if (GUI.Button(new Rect(10, 70, 60, 40), "Average"))
            {
                steerMin = 13.5f;
                steerMax = 9.5f;
            }

            if (GUI.Button(new Rect(10, 115, 60, 40), "Good"))
            {
                steerMin = 15;
                steerMax = 11;
            }

            if (GUI.Button(new Rect(10, 160, 60, 40), "Best"))
            {
                steerMin = 16.5f;
                steerMax = 12.5f;
            }
            GUI.EndGroup();

            GUI.BeginGroup(new Rect(25, 360, 80, 220));

            GUI.Box(new Rect(0, 0, 80, 220), "Transmission");
            if (GUI.Button(new Rect(10, 25, 60, 40), "Poor"))
            {
                boostAccelSpeed = 2f;
                boostCamSpeed = 0.02f;
                boostAccelBrake = 2f;
                boostCamBrake = 0.03f;
            }

            if (GUI.Button(new Rect(10, 70, 60, 40), "Average"))
            {
                boostAccelSpeed = 3f;
                boostCamSpeed = 0.03f;
                boostAccelBrake = 2.777f;
                boostCamBrake = 0.037f;
            }

            if (GUI.Button(new Rect(10, 115, 60, 40), "Good"))
            {
                boostAccelSpeed = 4f;
                boostCamSpeed = 0.04f;
                boostAccelBrake = 3.333f;
                boostCamBrake = 0.043f;
            }

            if (GUI.Button(new Rect(10, 160, 60, 40), "Best"))
            {
                boostAccelSpeed = 5f;
                boostCamSpeed = 0.05f;
                boostAccelBrake = 4f;
                boostCamBrake = 0.05f;
            }
            GUI.EndGroup();

            GUI.BeginGroup(new Rect(25, 590, 80, 220));

            GUI.Box(new Rect(0, 0, 80, 220), "Engine");
            if (GUI.Button(new Rect(10, 25, 60, 40), "Poor"))
            {
                boostAccel = 700;
                defaultAccel = 1000;
                brake = 500;
                stiffnesMin = 5;
                stiffnesMax = 10;
            }

            if (GUI.Button(new Rect(10, 70, 60, 40), "Average"))
            {
                boostAccel = 930;
                defaultAccel = 1330;
                brake = 660;
                stiffnesMin = 6.6f;
                stiffnesMax = 11.6f;
            }

            if (GUI.Button(new Rect(10, 115, 60, 40), "Good"))
            {
                boostAccel = 1170;
                defaultAccel = 1670;
                brake = 830;
                stiffnesMin = 8.2f;
                stiffnesMax = 13.2f;
            }

            if (GUI.Button(new Rect(10, 160, 60, 40), "Best"))
            {
                boostAccel = 1400;
                defaultAccel = 2000;
                brake = 1000;
                stiffnesMin = 7.5f;
                stiffnesMax = 15;
            }
            GUI.EndGroup();

            GUI.BeginGroup(new Rect(Screen.width - 125, 125, 80, 220));
            GUI.Box(new Rect(0, 0, 80, 220), "Mass");
            if (GUI.Button(new Rect(10, 25, 60, 40), "3t"))
            {
                currentRigidbody.mass = 3000;
            }

            if (GUI.Button(new Rect(10, 70, 60, 40), "5t"))
            {
                currentRigidbody.mass = 5000;
            }

            if (GUI.Button(new Rect(10, 115, 60, 40), "7t"))
            {
                currentRigidbody.mass = 7000;
            }

            if (GUI.Button(new Rect(10, 160, 60, 40), "9t"))
            {
                currentRigidbody.mass = 9000;
            }
            GUI.EndGroup();

            GUI.BeginGroup(new Rect(Screen.width - 125, 360, 80, 220));
            GUI.Box(new Rect(0, 0, 80, 220), "Turbo");
            if (GUI.Button(new Rect(10, 25, 60, 40), "Poor"))
            {
                TurboSpeed = 0.0011f;
            }

            if (GUI.Button(new Rect(10, 70, 60, 40), "Average"))
            {
                TurboSpeed = 0.0014f;
            }

            if (GUI.Button(new Rect(10, 115, 60, 40), "Good"))
            {
                TurboSpeed = 0.0017f;
            }

            if (GUI.Button(new Rect(10, 160, 60, 40), "Best"))
            {
                TurboSpeed = 0.002f;
            }
            GUI.EndGroup();

            GUI.BeginGroup(new Rect(Screen.width - 125, 590, 80, 70));

            GUI.Box(new Rect(0, 0, 80, 70), "Modules");
            if (GUI.Button(new Rect(10, 25, 60, 40), "Gattling"))
            {
                moduleSystem.EquipModule(0);
            }


            GUI.EndGroup();
        }
         */
    }



    float SteerManual(float steer, bool clamp)
    {
        
        currentSteerSpeed = steer * vehicle.steerCurrent;

		if (Vector3.Dot(transform.forward, vehicle.currentSplineDirection)>0.5f)
        {
            if (clamp)
			if ((vehicle.worldAngle > vehicle.currentSteerClamp && currentSteerSpeed > 0) || (vehicle.worldAngle < -vehicle.currentSteerClamp && currentSteerSpeed < 0))
                    currentSteerSpeed = 0;
        }

        

        return currentSteerSpeed;
    }

    public void StopSignals(bool on)
    {
        if (on)
        {
			vehicle.currentRenderer.material.SetColor("_EmissionColor", new Color32(255, 255, 255, 255));
            leftStopSignal.enabled = true;
            rightStopSignal.enabled = true;
        }
        else
        {
			vehicle.currentRenderer.material.SetColor("_EmissionColor", new Color32(1, 1, 1, 255));
            leftStopSignal.enabled = false;
            rightStopSignal.enabled = false;
        }
    }


    float AutopilotLate(float steer)
    {

        Vector3 RelativeWaypointPosition = Main.instance.GetHeroWaypoint();

		Vector3 gl = vehicle.transform.TransformPoint(RelativeWaypointPosition);
        Debug.DrawLine(gl, gl + Vector3.up * 5, Color.black);

		return RelativeWaypointPosition.x / RelativeWaypointPosition.magnitude * vehicle.steerCurrent;


    }

	float AutopilotLateToDot(float steer, Vector3 dot)
	{

		Vector3 RelativeWaypointPosition = vehicle.transform.InverseTransformPoint (dot);


		Debug.DrawLine(dot, dot + Vector3.up * 5, Color.black);

		return RelativeWaypointPosition.x / RelativeWaypointPosition.magnitude * vehicle.steerCurrent;


	}


    float Autopilot(float steer)
    {

		float param = vehicle.worldAngle;

		float aver = vehicle.steerCurrent / 3;
		float min = vehicle.steerCurrent / 6;
		float micro = vehicle.steerCurrent / 15;

		float averFrontier = vehicle.steerCurrent / 2;
		float minFrontier = vehicle.steerCurrent / 6;
		float microFrontier = vehicle.steerCurrent / 10;

        if (Mathf.Abs(param) <= averFrontier)
        {
            if (Mathf.Abs(param) <= minFrontier)
            {
                if (Mathf.Abs(param) <= microFrontier)
                {
                    param = Mathf.Clamp(param, -micro, micro);
                }
                else
                {
					float cmin = ((vehicle.worldAngle > 0) ? min : -min) / 3;
                    float mmin = Mathf.Clamp(param, -min, min);
                    param = mmin + cmin;
                }
            }
            else
            {
				float caver = ((vehicle.worldAngle > 0) ? aver : -aver) / 3;
                float maver = Mathf.Clamp(param, -aver, aver);
                param = maver + caver;
            }
        }
        else
        {

			param = Mathf.Clamp(param, -1.0f, 1.0f) * vehicle.steerCurrent;
        }
        return param * -1;
    }

    float steerControl(float steer)
    {
		if (isAutopilot && steer == 0 && !Vector3.Equals(vehicle.currentSplineDirection, Vector3.zero))
            return Autopilot(steer);
        else
            return SteerManual(steer, true);
    }

    private void CarMove(float accel, float steer)
    {
        if (Mathf.Abs(steer) > 0.001f)
            speedFactor -= speedFactorDownCoeff.current;
        if (speedFactor < 0)
            speedFactor = 0;

		vehicle.forwardSpeed = Mathf.Abs(vehicle.transform.InverseTransformDirection(vehicle.currentRigidbody.velocity).z);
        if (forwardSpeedTactTimer.isEnd(Time.deltaTime))
        {
			forwardSpeedTact = vehicle.forwardSpeed;
            forwardSpeedTactTimer.current = forwardSpeedTactTimer.max;
        }
        //if (isControl == false)
        //    return;

        //float motorTorque = 0;
        //float brakeTorque = 0;
        float currentSteerSpeed = 0;
		switch (Controller.instance.gameMode)
        {
            case Mode.Free:
                CarMoveFree(accel, steer, out motorTorque, out brakeTorque, out currentSteerSpeed);
                break;

            case Mode.Battle:
                CarMoveBattle(accel, steer, out motorTorque, out brakeTorque, out currentSteerSpeed);
                break;
			/*
		case Mode.Score:
				CarMoveScore(accel, steer, out motorTorque, out brakeTorque, out currentSteerSpeed);
			break;

		case Mode.EnterToHangar:
				CarMoveHangar(accel, steer, out motorTorque, out brakeTorque, out currentSteerSpeed);
			break;
			*/
        }


		vehicle.ApplyColliders(currentSteerSpeed, motorTorque, brakeTorque);
		vehicle.currentRpm = vehicle.wheels[0].collider.rpm;

        //DropText(wheels[0].collider.rpm.ToString("F2"));
    }


    private void CarMoveFree(float accel, float steer, out float motorTorque, out float brakeTorque, out float currentSteerSpeed)
    {
        motorTorque = 0;
        brakeTorque = 0;
        currentSteerSpeed = 0;

        currentSteerSpeed = SteerManual(steer, false);
        SimpleAuto(out motorTorque, out brakeTorque, steer, accel);

		if (isCalcVertical) 
		{
			CalculateVertical ();	
		}
    }


    void ImpactMode()
    {
		if (vehicle.impactTimer.isEnd(Time.deltaTime))
        {
			vehicle.impactTimer.Restore();
            //Main.instance.heroReturn.Activate();
        }
    }

    public void CalcVertical()
    {       
        
        RaycastHit hit;
		if (Math3d.RayCastGroundDown(transform.position, out hit))
        {
			dist = Vector3.Distance(vehicle.currentCollider.ClosestPointOnBounds(hit.point), hit.point);

            if (!isVertialCalc)
            {
                distMax = dist;
                isVertialCalc = true;
				rotationUp = vehicle.transform.rotation;

            }
            
            //Debug.Log(dist + "!!");
            //Debug.Log(dist);
            return;
        }
        dist = 0;
        return;
    }

	private void CalculateVertical()
	{
		if (vehicle.wheelsColliderBack [0].isGrounded || vehicle.wheelsColliderBack [1].isGrounded || vehicle.wheelsColliderForward [0].isGrounded || vehicle.wheelsColliderForward [1].isGrounded)
			return;
		
		RaycastHit hit;
		float lenght = 1.25f;


		Debug.DrawRay(transform.position, Quaternion.AngleAxis(90.0f, transform.up) * transform.forward * lenght, Color.white);

		if (Physics.Raycast(transform.position, Quaternion.AngleAxis(90.0f, transform.up) * transform.forward, out hit, lenght))
		{
			Debug.DrawRay(transform.position, Quaternion.AngleAxis(90.0f, transform.up) * transform.forward * lenght, Color.red);
			float left = hit.distance / lenght;

			vehicle.transform.Rotate (vehicle.transform.forward, left);
		}

		Debug.DrawRay(transform.position, Quaternion.AngleAxis(-90.0f, transform.up) * transform.forward * lenght, Color.white);

		if (Physics.Raycast(transform.position, Quaternion.AngleAxis(-90.0f, transform.up) * transform.forward, out hit, lenght))
		{
			Debug.DrawRay(transform.position, Quaternion.AngleAxis(-90.0f, transform.up) * transform.forward * lenght, Color.red);
			float right = hit.distance / lenght;

			transform.Rotate (transform.forward, -right);
		}

		Debug.DrawRay(transform.position, transform.forward * lenght, Color.white);

		if (Physics.Raycast(transform.position, transform.forward, out hit, lenght))
		{
			Debug.DrawRay(transform.position, transform.forward * lenght, Color.red);
			float forward = hit.distance / lenght;

			transform.Rotate (transform.right, forward);
		}

		Debug.DrawRay(transform.position, transform.forward * -lenght, Color.white);

		if (Physics.Raycast(transform.position, transform.forward * -1, out hit, lenght))
		{
			Debug.DrawRay(transform.position, transform.forward * -lenght, Color.red);
			float back = hit.distance / lenght;

			transform.Rotate (transform.right, -back);
		}
		//oldCalcVertical

		//if (!wheelsColliderBack [0].isGrounded && !wheelsColliderBack [1].isGrounded && !wheelsColliderForward [0].isGrounded && !wheelsColliderForward [1].isGrounded && !isCollisionBlock) {
		//	if (currentRigidbody.velocity.y < 0) {
		//
		//		CalcVertical ();
		//		if (dist > 0 && distMax > 0.3f) {
		//			transform.rotation = Quaternion.Slerp (Quaternion.LookRotation (roadDirection), rotationUp, dist / distMax);
		//		}
		//	}
		//} else {
		////	isVertialCalc = false;
		//	dist = 0;
		//}
		//isCollisionBlock = false;
		

	}

    private void CarMoveBattle(float accel, float steer, out float motorTorque, out float brakeTorque, out float currentSteerSpeed)
    {

        if (Mathf.Abs(vehicle.currentRigidbody.angularVelocity.y) > 5.0f)
        {
			Vector3 temp = vehicle.currentRigidbody.angularVelocity;
            temp.y = 4;
			if (vehicle.currentRigidbody.angularVelocity.y > 0)
                temp.y *= -1;
			vehicle.currentRigidbody.angularVelocity = temp;
            
        }

        motorTorque = 0;
        brakeTorque = 0;
        currentSteerSpeed = 0;

        if (currentSpline != null)
		if (vehicle.currentRigidbody.velocity.magnitude < 5.0f || currentSplineOffset > currentSpline.GetCurrentVerge(MainCamera.instance._currentHeroPercent))
            {
               ImpactMode();
            }
            else
			vehicle.impactTimer.Restore();

        currentSteerSpeed = steerControl(steer);

		if (isCalcVertical) 
		{
			CalculateVertical ();	
		}
        switch (controlStatus)
        {

            case ControlStatus.Default:
                {
                    AverageAuto(50.0f, out motorTorque, out brakeTorque, steer);
                    accelStatus = AccelStatus.Default;
                    accelTimer.current = 0;
                    StopSignals(false);
                }

                break;
            case ControlStatus.Brake:
                {
                    if (Mathf.Abs(steer) > 0.001f)
                        speedFactor -= speedFactorDownCoeff.current;
                    if (speedFactor < 0)
                        speedFactor = 0;

                    if (verticalSlice > -1.0f)
                    {
                        switch (accelStatus)
                        {
                            case AccelStatus.Default:
                                {
                                    StopSignals(true);
                                    accelTimer.current = 0;
                                    accelStatus = AccelStatus.BackBoost;
                                    currentVertical = verticalSlice;
                                    if (currentVertical <= -0.5f)
                                    {
                                        partialVertial = true;
                                        partialCounts = -1 - (currentVertical / 2 + 0.02f);
                                    }
                                    else
                                    {
                                        partialVertial = false;
                                        partialCounts = -0.65f;
                                    }
                                }
                                break;
                            case AccelStatus.BackBoost:
                                if (accelTimer.current < accelTimer.max)
                                {
                                    StopSignals(true);
                                    accelTimer.current += Time.deltaTime * boostAccelBrake;
                                }
                                else
                                {
                                    accelStatus = AccelStatus.BackAverage;
                                    accelTimer.current = accelTimer.max;
                                }
                                break;
                            case AccelStatus.BackAverage:
                                if (verticalSlice < partialCounts)
                                {
                                    StopSignals(true);
                                    accelStatus = AccelStatus.BackBrake;
                                    if (!partialVertial)
                                        accelTimer.current = accelTimer.max;
                                }
                                break;
                            case AccelStatus.BackBrake:
                                if (accelTimer.current > 0)
                                {
                                    StopSignals(false);
                                    accelTimer.current -= Time.deltaTime * boostAccelBrake;
                                }
                                else
                                {
                                    accelTimer.current = 0;
                                    accelStatus = AccelStatus.Default;
                                    controlStatus = ControlStatus.Default;
                                }
                                break;
                        }
					float _accel=vehicle.currentAccel;
					if (vehicle.forwardSpeed > minSpeed)
                            accel = 0;
                        else


						if (vehicle.forwardSpeed > minSpeedBreak)
                            {
                                motorTorque = accel;
							brakeTorque = vehicle.currentBrake * vehicle.curveBoost.Evaluate(accelTimer.current);
                            }
                            else
                            {
                                motorTorque = accel;
                                brakeTorque = 0;
                            }
					verticalSlice -= boostCamSpeed * vehicle.curveBoost.Evaluate(accelTimer.current);

                       // 
                    }
                    else
                    {
					if (vehicle.forwardSpeed < minSpeed)
                        {
						motorTorque = vehicle.currentAccel;
                            brakeTorque = 0;
                            accelStatus = AccelStatus.Default;
                            controlStatus = ControlStatus.Default;
                        }
                        else
                        {
                            motorTorque = 0;
						brakeTorque = vehicle.currentBrake;
                            accelStatus = AccelStatus.Default;
                            controlStatus = ControlStatus.Default;
                            AverageAuto(50.0f, out motorTorque, out brakeTorque, steer);

                        }
                        verticalSlice = -1.0f;
                    }
                }
                break;
            case ControlStatus.Boost:
                {
                    if (verticalSlice < 1.0f)
                    {
                        switch (accelStatus)
                        {
                            case AccelStatus.Default:
                                {
                                    accelTimer.current = 0;
                                    accelStatus = AccelStatus.ForwardBoost;
                                    currentVertical = verticalSlice;
                                    if (currentVertical >= 0.5f)
                                    {

                                        partialVertial = true;
                                        partialCounts = 1 - (currentVertical / 2 + 0.02f);
                                    }
                                    else
                                    {
                                        partialVertial = false;
                                        partialCounts = 0.77f;
                                    }

                                }
                                break;
                            case AccelStatus.ForwardBoost:

                                if (accelTimer.current < accelTimer.max)
                                {
                                    StopSignals(false);
                                    accelTimer.current += Time.deltaTime * boostAccelSpeed;
                                }
                                else
                                {

                                    accelStatus = AccelStatus.ForwardAverage;
                                    accelTimer.current = accelTimer.max;
                                }
                                break;
                            case AccelStatus.ForwardAverage:
                                if (verticalSlice > partialCounts)
                                {
                                    accelStatus = AccelStatus.ForwardBrake;
                                    if (!partialVertial)
                                        accelTimer.current = accelTimer.max;
                                }
                                break;
                            case AccelStatus.ForwardBrake:
                                
                                if (accelTimer.current > 0)
                                {
                                    StopSignals(true);
                                    accelTimer.current -= Time.deltaTime * boostAccelSpeed;
                                }
                                else
                                {
                                    
                                    accelTimer.current = 0;
                                    accelStatus = AccelStatus.Default;
                                    controlStatus = ControlStatus.Default;
                                }
                                break;
                        }

						motorTorque = vehicle.currentAccel + vehicle.currentBoost * vehicle.curveBoost.Evaluate(accelTimer.current);
                        brakeTorque = 0;
						verticalSlice += boostCamSpeed * vehicle.curveBoost.Evaluate(accelTimer.current);

					if (vehicle.forwardSpeed >= maxSpeed)
                        {
                            motorTorque = 0;
							brakeTorque = vehicle.currentBrake;
                        }
                    }
                    else
                    {
						if (vehicle.forwardSpeed > averageSpeed)
                        {
                            motorTorque = 0;
							brakeTorque = vehicle.currentBrake;
                            accelStatus = AccelStatus.Default;
                            controlStatus = ControlStatus.Default;
                        }
                        else
                        {
                            accelStatus = AccelStatus.Default;
                            controlStatus = ControlStatus.Default;
                            AverageAuto(50.0f, out motorTorque, out brakeTorque, steer);
                        }
                        verticalSlice = 1.0f;
                    }
                }
                break;
            case ControlStatus.ImpactControl:
                {
					if (vehicle.currentRigidbody.velocity.magnitude < 5.0f)
                    {
						currentSteerSpeed = vehicle.worldAngle;
						motorTorque = -vehicle.currentAccel;
                        brakeTorque = 0;
                    }
                    else
                        controlStatus = ControlStatus.Default;
                }
                break;
        }

    }

	private void CarMoveScore(float accel, float steer, out float motorTorque, out float brakeTorque, out float currentSteerSpeed)
	{

		if (Mathf.Abs(vehicle.currentRigidbody.angularVelocity.y) > 5.0f)
		{
			Vector3 temp = vehicle.currentRigidbody.angularVelocity;
			temp.y = 4;
			if (vehicle.currentRigidbody.angularVelocity.y > 0)
				temp.y *= -1;
			vehicle.currentRigidbody.angularVelocity = temp;

		}

		motorTorque = 0;
		brakeTorque = 0;
		currentSteerSpeed = 0;
		vehicle.steerCurrent = 20.0f;

		if (Main.instance.supportStep < 0.9f)
			currentSteerSpeed = AutopilotLate (Autopilot (steer));
		else
			currentSteerSpeed = Autopilot (steer);

		averageSpeed = Mathf.Clamp (1 - Main.instance.supportStep, 1.0f, 0.1f) * 10.0f;
		AverageAuto(50.0f, out motorTorque, out brakeTorque, steer);
		accelStatus = AccelStatus.Default;
		accelTimer.current = 0;
		StopSignals(false);

		if (isCalcVertical) 
		{
			CalculateVertical ();	
		}
	}

	private void CarMoveHangar(float accel, float steer, out float motorTorque, out float brakeTorque, out float currentSteerSpeed)
	{

		if (Mathf.Abs(vehicle.currentRigidbody.angularVelocity.y) > 5.0f)
		{
			Vector3 temp = vehicle.currentRigidbody.angularVelocity;
			temp.y = 4;
			if (vehicle.currentRigidbody.angularVelocity.y > 0)
				temp.y *= -1;
			vehicle.currentRigidbody.angularVelocity = temp;

		}

		motorTorque = 0;
		brakeTorque = 0;
		currentSteerSpeed = 0;
		vehicle.steerCurrent = 20.0f;


		currentSteerSpeed = AutopilotLateToDot((steer), _hangarDot);


		averageSpeed = 1.0f;
		AverageAuto(50.0f, out motorTorque, out brakeTorque, steer);
		accelStatus = AccelStatus.Default;
		accelTimer.current = 0;
		StopSignals(false);

		if (isCalcVertical) 
		{
			CalculateVertical ();	
		}
	}

    public void AverageAuto(float clamp, out float motorTorque, out float brakeTorque, float inputSteer)
    {
        motorTorque = 0;
        brakeTorque = 0;

		float diff = averageSpeed - vehicle.forwardSpeed;
        float absDiff = Mathf.Abs(diff);
        float factor = absDiff / 4.0f;

        if (absDiff < 1.5f)
        {
            if (diff >= 0)
				motorTorque = vehicle.currentAccel * factor;//(1 - Mathf.Abs(inputSteer/4))
            else
				brakeTorque = vehicle.currentBrake * factor;

        }
        else
        {
            if (diff < 0)
            {
                motorTorque = 0;
				brakeTorque = vehicle.currentBrake;
            }
            else
            {
				motorTorque = vehicle.currentAccel;//k * (1 - Mathf.Abs(inputSteer/4));
                brakeTorque = 0;
            }
        }
    }


    public void SimpleAuto(out float motorTorque, out float brakeTorque, float inputSteer, float inputAccel)
    {
        motorTorque = 0;
        brakeTorque = 0;


        if (inputAccel > 0)
			engineTurbo += 0.001f * engineTurbo;

        if (engineTurbo > 1.0f || inputAccel <= 0)
            engineTurbo = 0;


		if (vehicle.currentRpm >= vehicle.averageRpm || vehicle.currentRpm < (-vehicle.averageRpm / 3))
        {
            motorTorque = 0;
			brakeTorque = vehicle.currentBrake;
            return;
        }


		if (inputAccel < 0 && vehicle.forwardSpeed < 0.01 && vehicle.currentRpm < 0.01)
            reverseBreaking = true;

		if (inputAccel > 0 && vehicle.forwardSpeed < 0.01 && vehicle.currentRpm < 0.01)
            reverseBreaking = false;

        if (!reverseBreaking)
            if (inputAccel > 0)
            {
			motorTorque = vehicle.currentAccel * 1.5f;
                brakeTorque = 0;
            }
            else
            {
                motorTorque = 0;
			brakeTorque = vehicle.currentBrake;
            }

        if (reverseBreaking)
            if (inputAccel < 0)
            {
				motorTorque = vehicle.currentAccel * -1;
                brakeTorque = 0;
            }
            else
            {
                motorTorque = 0;
				brakeTorque = vehicle.currentBrake;
            }
    }



    
    

}