﻿using UnityEngine;
using System.Collections;

public class OilThrower : Module
{
    public Vector3 oilPos;
    TireFxData fxData;
    public CounterTimerI nozzleCount;
    public CounterTimerF recharge;
    public CounterTimerF magazine;
    public bool isActive = false;
    public ParticleSystem oilFall;
    public OilDot sourceOilDot;

    public float oilSize;
    public float width;
    public override void Install()
    {
        base.Install();
        fxData = new TireFxData();
       
        
    }

    void Update()
    {
        if (Main.instance == null)
            Main.instance = FindObjectOfType<Main>();

        if (Main.instance.gameMode != Mode.Battle)
            return;

        if (recharge.current < 0)
        {
            if (Input.GetButtonDown("Special02") && !isActive)
            {
                magazine.Restore();
				oilFall.Play();
                isActive = true;
            }

            ammoSource.ico.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
            ammoSource.back.fillAmount = (float)magazine.current / (float)magazine.max;
            ammoSource.data.text = magazine.current.ToString("F0") + "/" + magazine.max.ToString("F0");

        }
        else
        {
            ammoSource.ico.color = new Color(1.0f, 1.0f, 1.0f, 0.2f);
            ammoSource.back.fillAmount = 1 - (recharge.current / recharge.max);
            ammoSource.data.text = "Перезарядка";
        }

        recharge.current-=Time.deltaTime;

        if (isActive)
        {
            if (magazine.isEnd(Time.deltaTime))
            {
                isActive = false;
				oilFall.Stop();
                recharge.Restore();
            }
            else
                UpdateOil(fxData);
        }
    }

    public override void Reset()
    {
        magazine.Restore();
        recharge.current = 0;
		oilFall.Stop();
        isActive = false;
    }



    protected void UpdateOil(TireFxData fxData)
    {
        // If we are already drawing marks to this wheel, wait before updating.

        if (fxData.lastMarksIndex != -1 && fxData.marksDelta < 0.02f)
        {
            fxData.marksDelta += Time.deltaTime;
            return;
        }

        // deltaT = time since last mark for this wheel

        float deltaT = fxData.marksDelta;
        if (deltaT == 0.0f)
            deltaT = Time.deltaTime;
        fxData.marksDelta = 0.0f;


        TireMarksRenderer marksRenderer = Main.instance.oilRenderer;

        if (marksRenderer != fxData.lastRenderer)
        {
            fxData.lastRenderer = marksRenderer;
            fxData.lastMarksIndex = -1;
        }

        RaycastHit hit;
        if (marksRenderer != null && Math3d.RayCastGroundDown(transform.position + Vector3.up, out hit))
        {
            float pressureRatio = 1.0f;

            fxData.lastMarksIndex = marksRenderer.AddMark
                (
                    hit.point,
                    hit.normal,
                    pressureRatio,
                    0.0f,
                    width,
                    fxData.lastMarksIndex
                );
        }
    }
}
