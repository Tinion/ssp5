﻿using UnityEngine;
using System.Collections;


[System.Flags]
public enum ModuleSlot
{
    Top = 0,
    Front = 1,
    Side = 2,
    Back = 3,
    Down = 4,
    Wheels = 5,
    Engine = 6,
    Generator = 7,
}

public enum ModuleType
{
    Weapon = 0,
    Active = 1,
    Passive = 2,
}




public class Module : MonoBehaviour
{
    public ModuleSlot slotType;
    public ModuleType moduleType;
    public Transform transform;
    public GameObject currentGameObject;
    public Renderer currentRenderer;
    public int number;
    public WeaponType weaponType;
    public Weapon weapon;
    public AmmoSource ammoSource;
    public CounterTimerF durability;
    public bool defaultModule;
    
    
    public bool isDamaged = false;

    public SideTriangle additionalArmorType;
    public int additionalArmor;
    public int armor;
    public int hp;
    

    public virtual void Reset()
    {

    }

    void Start()
    {

    }

    public virtual void Install()
    {
        transform.parent = Main.instance.hero.transform;
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;
        gameObject.SetActive(true);

        int current = (int)slotType;
        for (int i = 0; i < Main.instance.inventory.inventorySlots[current].renderer.Length; i++)
            Main.instance.inventory.inventorySlots[current].renderer[i].enabled = false;
    }

    public virtual void Remove()
    {
        int current = (int)slotType;

        for (int i = 0; i < Main.instance.inventory.inventorySlots[current].renderer.Length; i++)
            Main.instance.inventory.inventorySlots[current].renderer[i].enabled = true;

        currentGameObject.SetActive(false);
    }

    public float GetDamage(float damage)
    {
        float returnDmg;

        if (isDamaged)
            return 0;

        if (durability.current - damage > 0)
        {
            durability.current -= damage;
            returnDmg = damage;
        }
        else
        {
            if (Math3d.GetChanceOf(Main.instance.inventory.items[number].destructionChance))
                Main.instance.hero.moduleSystem.DamageModule(slotType, false);
            else
                Main.instance.hero.moduleSystem.DamageModule(slotType, true);
            returnDmg = durability.current;
            durability.current = 0;
             
        }
        return returnDmg; 
    }
    public virtual void Shoot()
    {
    }

    public virtual void ShootStop()
    {
    }




}
