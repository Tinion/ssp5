﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ModuleSystem : MonoBehaviour
{

    public AmmoSource weaponSource;
    public AmmoSource activeSource;
    public RectTransform battleUI;
    public Module[] module;
    public List<ModuleWeapon> moduleWeapon;
    //public int otherModulesCount;
    public List<Module> otherModules;
    public Module[] defaultModules;

    public float bodyCost;

    void Start()
    {
        otherModules = new List<Module>();
        moduleWeapon = new List<ModuleWeapon>();
    }


    public void EquipModuleRoot(int select)
    {
        int current = (int)otherModules[select].slotType;

        if (module[current] != null)
        {
            Module temp = module[current];

            CalculateModuleBonuses(temp, false);

            if (module[current].moduleType == ModuleType.Weapon)
            {
                ModuleWeapon mw = module[current].GetComponent<ModuleWeapon>();
                moduleWeapon.Remove(module[current].GetComponent<ModuleWeapon>());
            }
        }


        if (otherModules[select].moduleType == ModuleType.Weapon)
        {
            moduleWeapon.Add(otherModules[select].GetComponent<ModuleWeapon>());
        }
            if (module[current] != null)
            {
                Module temp = module[current];

                module[current].gameObject.SetActive(false);
                module[current] = otherModules[select];
                otherModules.RemoveAt(select);

                if (!temp.defaultModule)
                    otherModules.Insert(select, temp);



            }
            else
            {
                module[current] = otherModules[select];
                otherModules.RemoveAt(select);
            }


            CalculateModuleBonuses(module[current], true);
            module[current].Install();
            

            UpdateIcons();
            Main.instance.baseRect.durabilityInfo.CreateDurabilityInfo();
            Main.instance.inventory.paramSliders.UpdateSliders();
			Main.instance.hero.vehicle.ApplyWheelCollector();
        
    }
    public void EquipModule(int select)
    {
        
        int current = (int)otherModules[select].slotType;

        Vector2 oldParams = Vector2.zero;
        if (module[current] != null)
        {
            oldParams.x = Main.instance.inventory.items[module[current].number].mass;
            oldParams.y = Main.instance.inventory.items[module[current].number].energy;
        }
        else
        {
            oldParams.x = 0;
            oldParams.y = 0;
        }

		if (Main.instance.inventory.items[otherModules[select].number].mass > Main.instance.inventory.paramSliders.maxMass - Main.instance.hero.vehicle.currentRigidbody.mass + oldParams.x)
        {
			Debug.Log("Превышена масса " + Main.instance.inventory.items[otherModules[select].number].mass + ">" + (Main.instance.inventory.paramSliders.massSlider.maxValue - Main.instance.hero.vehicle.currentRigidbody.mass + oldParams.x));
            return;
        }


        if (Main.instance.inventory.items[otherModules[select].number].energy - oldParams.y > Main.instance.hero.energyLimit.max - Main.instance.hero.energyLimit.current + oldParams.y)
        {
            Debug.Log("Превышено энергопотребление " + Main.instance.inventory.items[otherModules[select].number].energy + ">" + (Main.instance.hero.energyLimit.max - Main.instance.hero.energyLimit.current + oldParams.y));
            return;
        }
        
            Main.instance.inventory.inventorySlots[current].ClickThis();
            EquipModuleRoot(select);
        
    }

    public void CalculateCooldownIcons()
    {

        AmmoSource[] oldSources = FindObjectsOfType<AmmoSource>();
        int lenght = oldSources.Length;
        for (int i = 0; i < lenght; i++)
        {
            Destroy(oldSources[i].gameObject);
        }
        int weapons = 0;
        int actives = 0;
        for (int i=0; i<module.Length; i++)
        {
            if (module[i]!=null)
            {
                if (!module[i].isDamaged)
                {
                    switch (module[i].moduleType)
                    {
                        case ModuleType.Weapon:
                            {
                                module[i].ammoSource = Instantiate<AmmoSource>(weaponSource);
                                module[i].ammoSource.transform.SetParent(battleUI);
								module[i].ammoSource.transform.anchoredPosition = new Vector2(100, -140 - 105 * weapons);
                                module[i].ammoSource.ico.sprite = Main.instance.inventory.items[module[i].number].ammoSprite;
                                weapons++;
                            }
                            break;
                        case ModuleType.Active:
                            {
                                module[i].ammoSource = Instantiate<AmmoSource>(activeSource);
                                module[i].ammoSource.transform.SetParent(battleUI);

                                module[i].ammoSource.ico.sprite = Main.instance.inventory.items[module[i].number].ammoSprite;
								module[i].ammoSource.transform.anchoredPosition = new Vector2(170 + 70 * actives, -140);
                                switch (Main.instance.inventory.items[module[i].number].ammoType)
                                {
                                    case AmmoType.Missiles:
                                        module[i].ammoSource.key.text = "Q";
                                    break;
                                    case AmmoType.Toxic:
                                        module[i].ammoSource.key.text = "E";
                                    break;
                                    case AmmoType.Steel:
                                        module[i].ammoSource.key.text = "R";
                                    break;
                                }
                                 

                                actives++;
                            }
                            break;
                    }
                }
            }
        }
    }
    void CalculateModuleBonuses(Module _module, bool plus)
    {
        if (Main.instance == null)
            Main.instance = FindObjectOfType<Main>();

        float dec = (plus) ? -1.0f : 1.0f;

		Main.instance.hero.vehicle.currentRigidbody.mass -= Main.instance.inventory.items[_module.number].mass * dec;

        if (_module.slotType == ModuleSlot.Wheels)
        {
            Wheels ws = _module.GetComponent<Wheels>();
            Main.instance.inventory.paramSliders.minMass -= ws.addMaxMass * dec;
            Main.instance.inventory.paramSliders.averMass -= ws.addMaxMass * dec;
            Main.instance.inventory.paramSliders.maxMass -= ws.addMaxMass * dec;
			Main.instance.hero.vehicle.currentAccel -= ws.addMaxMass * dec;
			Main.instance.hero.vehicle.currentBrake -= ws.addMaxMass * dec * 0.85f;
			Main.instance.hero.vehicle.currentBoost -= ws.addMaxMass * dec * 0.85f;
            Main.instance.hero.steerMin -= ws.addSpeed * dec;
            Main.instance.hero.steerMax -= ws.addSpeed * dec;
        }
        if (_module.slotType == ModuleSlot.Engine)
        {
            Engine ws = _module.GetComponent<Engine>();
            Main.instance.inventory.paramSliders.minMass -= ws.addMaxMass * dec;
            Main.instance.inventory.paramSliders.averMass -= ws.addMaxMass * dec;
            Main.instance.inventory.paramSliders.maxMass -= ws.addMaxMass * dec;
			Main.instance.hero.vehicle.currentAccel -= ws.addMaxMass * dec;
			Main.instance.hero.vehicle.currentBrake -= ws.addMaxMass * dec * 0.85f;
			Main.instance.hero.vehicle.currentBoost -= ws.addMaxMass * dec * 0.85f;
			Main.instance.hero.vehicle.currentAccel -= ws.power * dec;
			Main.instance.hero.vehicle.currentBrake -= ws.power * dec * 0.85f;
			Main.instance.hero.vehicle.currentBoost -= ws.power * dec * 0.85f;
            Main.instance.hero.boostAccelSpeed -= 2.0f * ws.accelBonus * dec;
            Main.instance.hero.boostAccelBrake -= 2.0f * ws.accelBonus * dec;
            Main.instance.hero.boostCamSpeed -= 0.02f * ws.accelBonus * dec;
            Main.instance.hero.boostCamBrake -= 0.03f * ws.accelBonus * dec;

        }

        Main.instance.hero.currentArmor -= _module.armor * dec;
        if (_module.additionalArmor > 0)
        {
            switch (_module.additionalArmorType)
            {
                case SideTriangle.Top:
                    Main.instance.hero.sideArmor[0] -= _module.additionalArmor * dec;
                    break;
                case SideTriangle.Left:
                    Main.instance.hero.sideArmor[1] -= _module.additionalArmor * dec;
                    break;
                case SideTriangle.Right:
                    Main.instance.hero.sideArmor[1] -= _module.additionalArmor * dec;
                    break;
                case SideTriangle.Back:
                    Main.instance.hero.sideArmor[2] -= _module.additionalArmor * dec;
                    break;
            }
        }
		Main.instance.hero.vehicle.healthPoints.max -= _module.hp * dec;
		Main.instance.hero.vehicle.healthPoints.current -= _module.hp * dec;

        if (_module.slotType != ModuleSlot.Generator)
            Main.instance.hero.energyLimit.current -= Main.instance.inventory.items[_module.number].energy * dec;
        else
            Main.instance.hero.energyLimit.max -= Main.instance.inventory.items[_module.number].energy * dec;

		float speedCoef = Mathf.Clamp01((Main.instance.hero.vehicle.currentRigidbody.mass - Main.instance.inventory.paramSliders.minMass) / (Main.instance.inventory.paramSliders.maxMass - Main.instance.inventory.paramSliders.minMass));
        Main.instance.hero.speedFactorDownCoeff.current = Main.instance.hero.speedFactorDownCoeff.max * speedCoef * 0.5f;
        Main.instance.hero.speedFactorUpCoeff.current = Main.instance.hero.speedFactorDownCoeff.max * (1.0f - speedCoef) * 0.5f;

        /*
        for (int i = 0; i < Main.instance.hero.wheels.Length; i++)
        {
            JointSpring spring = Main.instance.hero.wheels[i].collider.suspensionSpring;
            spring.targetPosition = Mathf.Lerp(0.85f, 0.95f, speedCoef);
            Main.instance.hero.wheels[i].collider.suspensionSpring = spring;
        }
        */
    }

    public void UnEquipModule(ModuleSlot _slotType)
    {

        int current = (int)_slotType;

        if (module[current].defaultModule)
            return;


        CalculateModuleBonuses(module[current], false);
        otherModules.Add(module[current]);

        
        if (module[current].slotType == ModuleSlot.Generator || module[current].slotType == ModuleSlot.Engine || module[current].slotType == ModuleSlot.Wheels)
        {
            ModuleSlot _slotTemp = module[current].slotType;
            module[current].gameObject.SetActive(false);

            switch (_slotTemp)
            {
                case ModuleSlot.Engine:
                    module[current] = defaultModules[0];
                    break;
                case ModuleSlot.Generator:
                    module[current] = defaultModules[1];             
                    break;
                case ModuleSlot.Wheels:
                    {                     
                        module[current] = defaultModules[2];
                        Wheels mw = module[current].GetComponent<Wheels>();
						for (int i = 0; i < Main.instance.hero.vehicle.wheels.Length; i++)
						Main.instance.hero.vehicle.wheels[i].wheelTransform.GetComponent<MeshFilter>().mesh = mw.sourceWheel;
                    }
                    break;
            }
            CalculateModuleBonuses(module[current], true);

            module[current].Install();
            UpdateIcons();
            Main.instance.inventory.paramSliders.UpdateSliders();
			Main.instance.hero.vehicle.ApplyWheelCollector();

            return;
        }

        module[current].Remove();
        module[current] = null;

        UpdateIcons();
        Main.instance.baseRect.durabilityInfo.CreateDurabilityInfo();
        Main.instance.inventory.paramSliders.UpdateSliders();
		Main.instance.hero.vehicle.ApplyWheelCollector();
    }

    public int GetRepairCost()
    {

		float repairCost = bodyCost * (1.0f - Main.instance.hero.vehicle.healthPoints.current / Main.instance.hero.vehicle.healthPoints.max);
            for (int i=0; i<5; i++)
            {
                if (module[i] != null)
                {
                    float koeff = (1.0f - module[i].durability.current / module[i].durability.max);
                    repairCost += koeff * Main.instance.inventory.items[module[i].number].cost;
                }
            }
            return Mathf.CeilToInt(repairCost);
    }

    public void Repair()
    {

        if (Main.instance.moneyCount >= Main.instance.repairCount)
        {
			Main.instance.hero.vehicle.healthPoints.Restore();

            Main.instance.moneyCount -= Main.instance.repairCount;

            for (int i = 0; i < 5; i++)
            {
                if (module[i] != null)
                {
                    module[i].durability.Restore();
                    if (module[i].isDamaged)
                        RestoreModule(module[i].slotType);
                }
            }

            Main.instance.repairCount = 0;
        }

        

        Main.instance.UpdateMoney();
        Main.instance.baseRect.durabilityInfo.CreateDurabilityInfo();
        UpdateIcons();
    }
    public void DamageModule(ModuleSlot _slotType, bool isDamage)
    {

        int current = (int)_slotType;

        if (module[current].defaultModule)
            return;

        CalculateModuleBonuses(module[current], false);
        module[current].Remove();

        
        if (!isDamage)
        {
            Main.instance.scoreUI.destroyedModules.Add(Main.instance.inventory.items[module[current].number].name);
            module[current] = null;
        }
        else
        {
            module[current].isDamaged = true;
        }

        Main.instance.hero.moduleSystem.CalculateCooldownIcons();
        Main.instance.battleRect.durabilityInfo.CreateDurabilityInfo();
		Main.instance.hero.vehicle.ApplyWheelCollector();
    }

    public void RestoreModule(ModuleSlot _slotType)
    {

        int current = (int)_slotType;

        if (module[current].defaultModule)
            return;

        for (int i = 0; i < Main.instance.inventory.inventorySlots[current].renderer.Length; i++)
            Main.instance.inventory.inventorySlots[current].renderer[i].enabled = false;


        CalculateModuleBonuses(module[current], true);


        if (module[current].moduleType == ModuleType.Weapon)
        {
            ModuleWeapon mw = module[current].GetComponent<ModuleWeapon>();
            moduleWeapon.Add(module[current].GetComponent<ModuleWeapon>());
        }


        if (module[current].slotType == ModuleSlot.Front)
        {
            FrontModule front = module[current].GetComponent<FrontModule>();
			Main.instance.hero.hood.GetComponent<MeshFilter>().mesh = front.sourceHood;// Main.instance.inventory.hoodDefaultSource;
        }

        module[current].gameObject.SetActive(true);
        module[current].isDamaged = false;


        Main.instance.hero.moduleSystem.CalculateCooldownIcons();
        Main.instance.baseRect.durabilityInfo.CreateDurabilityInfo();
		Main.instance.hero.vehicle.ApplyWheelCollector();
    } 
    

    public void UpdateIcons()
    {
        for (int i = 0; i < Main.instance.inventory.inventorySlots.Length; i++)
        {
            Main.instance.inventory.inventorySlots[i].currentIcon.color = new Color(1, 1, 1, 0);
            Main.instance.inventory.inventorySlots[i].isActive = false;

            if (Main.instance.inventory.inventorySlots[i].durSlider!=null)
                Destroy(Main.instance.inventory.inventorySlots[i].durSlider.gameObject);
        }
        for (int i = 0; i < Main.instance.inventory.inventorySlots.Length; i++)
        {
            if (module[i]!=null)
            {
                Main.instance.inventory.inventorySlots[i].currentIcon.sprite = Main.instance.inventory.items[module[i].number].sprite;
                Main.instance.inventory.inventorySlots[i].currentIcon.color = new Color(1, 1, 1, 1);
                Main.instance.inventory.inventorySlots[i].isActive = true;

                if (module[i].durability.current < module[i].durability.max)
                {
                    Main.instance.inventory.inventorySlots[i].durSlider = Instantiate<durabilitySlider>(Main.instance.inventory.durabilitySliderSource);
                    Main.instance.inventory.inventorySlots[i].durSlider.currentRectTransform.SetParent(Main.instance.inventory.inventorySlots[i].current);
                    Main.instance.inventory.inventorySlots[i].durSlider.currentRectTransform.anchoredPosition = new Vector2(0.0f, -25.9f);
                    Main.instance.inventory.inventorySlots[i].durSlider.SetValue(module[i].durability.current / module[i].durability.max);
                }
            }
        }

        for (int i = 0; i < Main.instance.inventory.inventoryObjects.Length; i++)
        {
            Main.instance.inventory.inventoryObjects[i].slot.currentImageIcon.color = new Color(1, 1, 1, 0);
            Main.instance.inventory.inventoryObjects[i].slot.isActive = false;

            if (Main.instance.inventory.inventoryObjects[i].slot.durSlider != null)
                Destroy(Main.instance.inventory.inventoryObjects[i].slot.durSlider.gameObject);
        }
        for (int i = 0; i < otherModules.Count; i++)
        {
            Main.instance.inventory.inventoryObjects[i].number = otherModules[i].number;
            Main.instance.inventory.inventoryObjects[i].slot.isActive = true;
            Main.instance.inventory.inventoryObjects[i].slot.currentImageIcon.color = new Color(1, 1, 1, 1);
            Main.instance.inventory.inventoryObjects[i].slot.currentImageIcon.sprite = Main.instance.inventory.items[Main.instance.inventory.inventoryObjects[i].number].sprite;

            if (otherModules[i].durability.current < otherModules[i].durability.max)
            {
               // if (Main.instance.inventory.inventoryObjects[i].slot.durSlider == null)
               // {
                    Main.instance.inventory.inventoryObjects[i].slot.durSlider = Instantiate<durabilitySlider>(Main.instance.inventory.durabilitySliderSource);
                    Main.instance.inventory.inventoryObjects[i].slot.durSlider.currentRectTransform.parent = Main.instance.inventory.inventoryObjects[i].slot.current;
                    Main.instance.inventory.inventoryObjects[i].slot.durSlider.currentRectTransform.anchoredPosition = new Vector2(0.0f, -25.9f); 
                //}
                Main.instance.inventory.inventoryObjects[i].slot.durSlider.SetValue(otherModules[i].durability.current / otherModules[i].durability.max);
            }
        }

        Main.instance.inventory.shop.button01.SetAsLastSibling();
        Main.instance.inventory.shop.button02.SetAsLastSibling();
    }
}
