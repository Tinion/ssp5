﻿using UnityEngine;
using System.Collections;

public class MarkFree : MonoBehaviour {

    public Quad2d quad2d;
    public BlockPlane currentCenter;
    private bool isUse = false;
    CounterTimerF UseTimer;
    public bool isStart;

    public int line;
    public bool reverse;
    
	// Use this for initialization
	void Start () 
    {
    UseTimer.max = 1.0f;
		Main.instance = FindObjectOfType<Main> ();
    currentCenter = Main.Placed(transform.position);
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (isUse)
        {
            if (UseTimer.isEnd(Time.deltaTime))
            {
                isUse = false;
            }
            return;
        }
        if (GetChoise(Main.instance.hero.transform.position) && !isUse)
        {
            isUse = true;
            if (currentCenter == null)
                currentCenter = Main.Placed(transform.position);
            Main.instance.currentPlane = currentCenter;
            
            UseTimer.current = UseTimer.max;
     

            if (!Main.instance.freeIn)
            {
                //if (Vector3.Dot(Main.instance.hero.transform.forward, transform.forward) <= 0)
                 //   Main.instance.isReverse = !Main.instance.isReverse;

                 Main.instance.planeRun = currentCenter.localForkNumber + 1;
                 Main.instance.freeIn = true;
                Main.instance.forkRun = line;
                Main.instance.CreateNew();
            }
            else
                if (Main.instance.freeIn)
                {
                    Main.instance.freeIn = false;
                    Main.instance.forkRun = line;
                    Main.instance.DeleteAllLine(line, currentCenter);

                    //if (Vector3.Dot(Main.instance.hero.transform.forward, transform.forward) <= 0)
                    //    Main.instance.isReverse = !Main.instance.isReverse;

                }

            

            
            
        }
	}

    bool GetChoise(Vector3 _Pos)
    {
        Vector3 Pos = transform.InverseTransformPoint(_Pos);

        if (Math3d.dotInTriangle(Pos, quad2d.a, quad2d.b, quad2d.c) ||
            Math3d.dotInTriangle(Pos, quad2d.b, quad2d.c, quad2d.d))
             return true;
        return false;

    }
}
