﻿using UnityEngine;
using System.Collections;

public class Misc : Unit
{
    public MiscType miscType;
    
    public override void Initialize(bool activeObject)
    {
        base.Initialize(activeObject);

    }

    void Update ()
    {
		if (!isInitialize )
            Initialize(true);
		
		if (killTimer.isEnd(Time.deltaTime))
		{
			killTimer.Restore ();
			if (isTooFar(killDistance, 5.0f))    
				Kill();
		}
    }

 
	public override void Kill()
	{
		
		RevertObject();

		Main.instance.activeMiscObjects.Remove(this);
		Main.instance.MiscPoolSystem.DestroyObject(this, nominal);

		return;
	}


}
