﻿using UnityEngine;
using System.Collections;

public enum BonusType
{
    HealthUp,
    ShieldUp,
    Module,
    Coin
}

public class Bonus : MonoBehaviour {

    public Transform transform;
    public BonusType bonusType;
    public float killDistance;
    private float killTimerMax = 1.0f;
    private float killTimer = 0.0f;
    private float timerTake;
    private float speed = 0.1f;

    public bool isTakeIt;
    public string moduleName;

    void Start()
    {
        Main.instance = GameObject.FindObjectOfType<Main>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Unit"))
        {
            Unit unit = other.transform.parent.GetComponent<Unit>();
            if (unit.unitType == UnitType.Hero)
                isTakeIt = true;
        }

    }

    void Update()
    {

        if (isTakeIt)
        {
            float Dist = Vector3.Distance(transform.position, Main.instance.hero.transform.position);
            if (Dist > 0.5f)
            {
                speed += Time.deltaTime * 15.0f;
                transform.position = Vector3.MoveTowards(transform.position, Main.instance.hero.transform.position, speed);
            }
            else
            {
                TakeTheBonus();
            }
            timerTake -= Time.deltaTime;
            

        }

        if (killTimer > 0)
        {
            killTimer -= Time.deltaTime;
        }
        else
        {
            killTimer = killTimerMax;
            if (isTooFar(killDistance))
            {
                //Debug.Log("HUUU");
                Destroy(this.gameObject);

            }
        }
    }


    public virtual void TakeTheBonus()
    {
        if (bonusType == BonusType.HealthUp)
            Main.instance.hero.Heal();
        if (bonusType == BonusType.ShieldUp)
            Main.instance.hero.ShieldUp();
        if (bonusType == BonusType.Coin)
            Main.instance.bonusElements.SetCoins(1);
        if (bonusType == BonusType.Module)
        {
            Main.instance.LoadModule(moduleName);
            //if (Main.instance.hero.moduleSystem.otherModules.Count>0)
            Main.instance.hero.moduleSystem.EquipModule(Main.instance.hero.moduleSystem.otherModules.Count-1);
        }
        Destroy(this.gameObject);
    }

    public virtual bool isTooFar(float _Dist)
    {
            float Dist = Vector3.Distance(transform.position, Main.instance.mainCamera.transform.position);

            if (Dist > _Dist)
                return true;



            return false;

    }
}
