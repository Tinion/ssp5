﻿using UnityEngine;
using System.Collections;

public class Puddle : Misc {

    public float friction;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Unit"))
        {
            Unit unit = other.transform.parent.GetComponent<Unit>();
            unit.SetFriction(unit.frictionResistance);
        }
        
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Unit"))
        {
            Unit unit = other.transform.parent.GetComponent<Unit>();
            unit.SetFriction(1.0f);
            


        }

    }
}
