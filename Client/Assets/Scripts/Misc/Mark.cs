﻿using UnityEngine;
using System.Collections;

public class Mark : MonoBehaviour {

    public Quad2d quad2d;
    public Mode mode;
    public BlockPlane currentCenter;
    public int id;

    public int line;
    public bool reverse;
    

	void Start () 
    {
    id = gameObject.GetInstanceID();
		Main.instance = FindObjectOfType<Main> ();
    currentCenter = Main.Placed(transform.position);
	}

	void Update ()
    {

        if (GetChoise(Main.instance.hero.transform.position) && Main.instance.usedMarkGate!=id)
        {
            Main.instance.usedMarkGate = id;
            if (currentCenter == null)
                currentCenter = Main.Placed(transform.position);
            Main.instance.currentPlane = currentCenter;
            //Debug.Log("Проехал + " + id);         

            if (Main.instance.gameMode == Mode.Free)
            {
                
                //if (Vector3.Dot(Main.instance.hero.transform.forward, transform.forward) <= 0)
                 //   Main.instance.isReverse = !Main.instance.isReverse;
				Debug.Log("FF");
                Main.instance.gameMode = Mode.Battle;
                Main.instance.planeRun = 2;
                Main.instance.forkRun = line;
                //Main.instance.hero.ActivateBattle(true);
                Main.instance.CreateNew();

                Main.instance.mainCamera.OnBattleCam(CamMode.Battle);
            }
            else
                if (Main.instance.gameMode == Mode.Battle)
                {
                    Main.instance.gameMode = Mode.Free;
                    Main.instance.forkRun = line;
                    Main.instance.DeleteAllLine(line, currentCenter);
                    //Main.instance.hero.ActivateBattle(false);

                    //if (Vector3.Dot(Main.instance.hero.transform.forward, transform.forward) <= 0)
                   //     Main.instance.isReverse = !Main.instance.isReverse;

                    Main.instance.mainCamera.OnBattleCam(CamMode.Free);
                }

            

            
            
        }
	}




    bool GetChoise(Vector3 _Pos)
    {
        Vector3 Pos = transform.InverseTransformPoint(_Pos);

        if (Math3d.dotInTriangle(Pos, quad2d.a, quad2d.b, quad2d.c) ||
            Math3d.dotInTriangle(Pos, quad2d.b, quad2d.c, quad2d.d))
             return true;
        return false;

    }
}
