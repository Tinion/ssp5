﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class GarbageElement
{
    private Unit unit;
    private Vector3 basicPosition;
    private Quaternion basicRotation;

    public GarbageElement(Unit _unit, Vector3 _basicPosition, Quaternion _basicRotation)
    {
        unit = _unit;
        basicPosition = _basicPosition;
        basicRotation = _basicRotation;
    }

    public void Restore()
    {
        unit.currentRigidbody.velocity = Vector3.zero;
        unit.currentRigidbody.angularVelocity = Vector3.zero;

        unit.transform.localPosition = basicPosition;
        unit.transform.localRotation = basicRotation;
        
    }
 
}

public class Garbage : Misc {

    public bool hasRestored = false;
    public GarbageElement[] elements;

    void Start()
    {
        SaveElementsPositions();
    }

    public override void Initialize(bool activeObject)
    {
        base.Initialize(activeObject);

        if (hasRestored)
        Restore();
    }

    public void SaveElementsPositions()
    {
        elements = new GarbageElement[transform.childCount];
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform current = transform.GetChild(i);
            elements[i] = new GarbageElement(current.GetComponent<Unit>(), current.localPosition, current.localRotation);
        }

        hasRestored = true;
    }

    public void Restore()
    {
        for (int i = 0; i < elements.Length; i++)
        {
            elements[i].Restore();
        }
    }


}
