﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class durabilitySlider : MonoBehaviour 
{
    public Color max;
    public Color average;
    public Color min;
    public RectTransform currentRectTransform;
    public Slider slider;
    public Image fill;

    public void SetValue(float value)
    {
        slider.value = value;

        if (value > 0.5f)
            fill.color = Color.Lerp(average, max, (value - 0.5f) * 2.0f);
        else
            fill.color = Color.Lerp(min, average, value * 2.0f);
    }
}
