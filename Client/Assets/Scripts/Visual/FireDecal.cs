﻿using UnityEngine;
using System.Collections;

public class FireDecal : MonoBehaviour {

    public Renderer flame;
    private float offset = 0;
    public float speed = 1;


    void Update()
    {
        offset += Time.deltaTime * speed;
        flame.material.SetFloat("_Offset", offset);
    }
}
