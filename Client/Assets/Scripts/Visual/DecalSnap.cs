﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Dest.Math;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class PosRot
{
    public Vector3 pos;
    public Vector3 rot;
    

    public PosRot(Vector3 _pos, Vector3 _rot)
    {
        pos = _pos;
        rot = _rot;
    }
}
public class DecalSnap : MonoBehaviour
{
    public MeshFilter currentMeshFilter;
    public Transform transform;
    public Material material;
    float wayLenght = 3.0f;
    public bool direction;
    public Scorcher master;
	public OneSpline spline;
    public CatmullRomSpline3 splineNew;

    public List<Vector3> segments; 

    public int ch2 = 0;
    public int ch3 = 0;
    public int ch4 = 0;

    public float dist2;
    public CounterTimerF timer;
    public bool isStart;
    public bool isEnd;
    public Vector2[] bezier;
    public bool isSplineCreated = false;

    public Vector3 target;
    public Vector3 start;
    public float targetOffset;
    public float startOffset;

    public float localStartDist;
    public float localTargetDist;

    public float Length = 0;

    void FixedUpdate()
    {
        
        if (isStart)
        {
            
            
            if (currentMeshFilter.sharedMesh != null)
            {
                
                   Color[] color = currentMeshFilter.sharedMesh.colors;
              

                    color[ch3] = Color.white;
                    color[ch3 + 1] = Color.white;
                    ch3 += 2;


                    if (ch3 + 1 > currentMeshFilter.sharedMesh.colors.Length)
                    {
                        isStart = false;
                        End();
                    }
                    currentMeshFilter.mesh.colors = color;
            }
            
        }

        if (isEnd)
        {
            
            if (currentMeshFilter.sharedMesh != null)
            {
                Color[] color = currentMeshFilter.sharedMesh.colors;


                color[ch3] = new Color(0,0,0,0);
                color[ch3 + 1] = new Color(0, 0, 0, 0);
                ch3 += 2;

                    currentMeshFilter.mesh.colors = color;
                if (ch3 + 1 > currentMeshFilter.sharedMesh.colors.Length)
                {
                    isEnd = false;
                    isSplineCreated = false;
                    Length = 0;
                    Destroy(transform.gameObject);
                }    

                
            }
            
        }
             
    }

    public void RevertDecal()
    {
        currentMeshFilter.sharedMesh.Clear();
        transform.parent = this.transform;
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;
        ch2 = 0;
        ch3 = 0;
        ch4 = 0;
        Length = 0;
        isSplineCreated = false;

    }
   
    #if UNITY_EDITOR
    [ContextMenu("Apply WheelCollector")]
    public void SnapHuyap()
    {
		Main Tempain = FindObjectOfType<Main> ();
        Snap(Tempain.hero.transform.position, Tempain.hero.currentSplineOffset, Tempain.generator.one.point, Tempain.generator.one.offset);
    }
#endif

    public void Snap(Vector3 _target, float _targetOffset, Vector3 _start, float _startOffset)
    {
        //RevertDecal();
        //Debug.Log("FFF");

        if (Main.instance == null)
			Main.instance = FindObjectOfType<Main> ();

        isStart = true;
        isEnd = false;
        timer.max = 1.0f;
        timer.current = timer.max/2;
        ch2 = 0;
        ch3 = 0;
        
        Mesh Deform = new Mesh();
        Deform.name = "DeformationMesh";
        Vector3 currentPos = Vector3.zero;
        float addPercent = 0;
        float _dist = 0;
        if (spline = Math3d.GetEnemySpline(master.transform.position))
        {
            master.currentStep = spline.GetStep(master.transform.position);

        }
        else
        {
            Debug.LogError("FFSFS");
            return;
        }
        spline.UniformGlobalPosAdd(master.currentStep, 23.0f, 0, out currentPos, out addPercent, out _dist, true);
        transform.position = currentPos;

         target = _target;
         start = _start;
         targetOffset = _targetOffset;
         startOffset = _startOffset;

        List<Vector3> vertices = new List<Vector3>();
        List<Vector3> normals = new List<Vector3>();
        List<Vector2> uv = new List<Vector2>();
        List<int> indices = new List<int>();
        List<Color> colors = new List<Color>();

		OneSpline currentSpline = spline;

        int ch = 0;

       // Debug.Log("Start");
        PosRot one = Main.instance.GetNextDot(Vector3.zero, direction, 3.0f, transform, ref currentSpline);
        //one.pos = Math3d.RotateThis(master.offset, 180.0f, one.pos);
        //one.pos = one.pos + (Quaternion.Euler(0, 90, 0) * one.rot) * master.offset;
        one.pos = one.pos + (Quaternion.Euler(0, 90, 0) * one.rot) * master.currentSplineOffset;
        Vector3 globalThis = transform.TransformPoint(one.pos);
        splineNew.AddVertexLast(SnapToTerrain(one.pos));
        segments.Add(globalThis);
        //Locators.CreateLocator(one.pos, "Start");
        //
        //Debug.Log("Segments");
        for (int i = 0; i < 45; i++)
        {
            float xUv = ((i == 0) ? 0.5f : 0);
            Vector3 localDot = Quaternion.AngleAxis(-90, Vector3.up) * one.rot * (wayLenght / 2 ) + one.pos;/*+ -master.offset*/
            vertices.Add(SnapToTerrain(localDot));
            //vertices.Add(localDot);
            uv.Add(new Vector2(0+xUv, 1));
            normals.Add(Vector3.up);
            colors.Add(Color.black);

            localDot = Quaternion.AngleAxis(90, Vector3.up) * one.rot * (wayLenght / 2) + one.pos;/*master.offset*/
            vertices.Add(SnapToTerrain(localDot));
            //vertices.Add(localDot);
            uv.Add(new Vector2(0.5f + xUv, 1));
            normals.Add(Vector3.up);
            colors.Add(Color.black);

            one = CreateSegment(one.pos, one.rot, direction, wayLenght, ref currentSpline);

            globalThis = transform.TransformPoint(one.pos);
            splineNew.AddVertexLast(SnapToTerrain(one.pos));
            segments.Add(globalThis);

            localDot = Quaternion.AngleAxis(90, Vector3.up) * one.rot * (wayLenght / 2) + one.pos;/*master.offset*/
            vertices.Add(SnapToTerrain(localDot));
            //vertices.Add(localDot);
            uv.Add(new Vector2(0.5f + xUv, 0));
            normals.Add(Vector3.up);
            colors.Add(Color.black);

            localDot = Quaternion.AngleAxis(-90, Vector3.up) * one.rot * (wayLenght / 2) + one.pos;/*-master.offset*/
            vertices.Add(SnapToTerrain(localDot));
            //vertices.Add(localDot);
            uv.Add(new Vector2(0 + xUv, 0));
            normals.Add(Vector3.up);
            colors.Add(Color.black);
            
            indices.Add(0 + ch * 4);
            indices.Add(1 + ch * 4);
            indices.Add(2 + ch * 4);
            indices.Add(0 + ch * 4);
            indices.Add(2 + ch * 4);
            indices.Add(3 + ch * 4);
            ch++;
        }

        Deform.SetVertices(vertices);
        Deform.uv = uv.ToArray();
        Deform.SetNormals(normals);
        Deform.SetColors(colors);
        Deform.SetTriangles(indices.ToArray(),0);
        Deform.RecalculateBounds();
        currentMeshFilter.mesh = Deform;
        transform.GetComponent<Renderer>().sharedMaterial = material;

        master.lifeTime.max = splineNew.CalcTotalLength() / master.speed;
        master.lifeTime.Restore();
        
    }

    public void End()
    {
        isEnd = true;
        ch3 = 0;
    }

    void CreateSpline(Vector3 startPos)
    {


		OneSpline current = Main.instance.mainCamera.GetSpline(startPos + Vector3.up * 100.0f);
        Vector3 startDir = Vector3.Normalize(current.GetTangent(current.GetStep(startPos)));

		current = Main.instance.mainCamera.GetSpline(target + Vector3.up * 100.0f);
		Vector3 endDir = Vector3.Normalize(current.GetTangent(current.GetStep(target)));

        Debug.DrawLine(startPos, startPos + Vector3.up * 15.0f, Color.blue, 10.0f);
        Debug.DrawLine(target, target + Vector3.up * 15.0f, Color.red, 10.0f);
        //Debug.Log(startDir.ToVector2XZ().normalized.Perp() * startOffset + " " + startOffset + " " + targetOffset);

        bezier = new Vector2[4];
        bezier[0] = startPos.ToVector2XZ() + startDir.ToVector2XZ().normalized.Perp() * startOffset;
        bezier[3] = target.ToVector2XZ();
       // bezier[0] = Math3d.RotateThis2d(startOffset, )
        Vector2 closestPoint = Vector2.zero;
        Ray2 ray = new Ray2(bezier[0], startDir.ToVector2XZ().normalized);
        Vector2 point = bezier[3];
        float dist = Distance.Point2Ray2(ref point, ref ray, out closestPoint);

        bezier[1] = bezier[0] - startDir.ToVector2XZ().normalized * dist * 0.375f;
        bezier[2] = bezier[3] + endDir.ToVector2XZ().normalized * dist * 0.375f;

       // Debug.DrawRay(bezier[0].ToVector3XZ(), endDir.ToVector2XZ().normalized.Perp().ToVector3XZ(), Color.yellow, 10.0f);

        ray = new Ray2(bezier[0], endDir.ToVector2XZ().normalized.Perp());
            dist2 = Distance.Point2Ray2(ref point, ref ray, out closestPoint);
            dist2 = Vector2.Distance(closestPoint, bezier[0]);

        if (dist2==0)
        {
            ray = new Ray2(bezier[0], endDir.ToVector2XZ().normalized.Perp() * -1);
            dist2 = Distance.Point2Ray2(ref point, ref ray, out closestPoint);
            dist2 = Vector2.Distance(closestPoint, bezier[0]);
        }
        //dist2 = Vector2.Distance(closestPoint, point);
        //float dist3 = Mathf.Sqrt(dist2 * dist2 - dist * dist);
        
        
        //Locators.CreateLocator(bezier[0].ToVector3XZ(), "bezier[0]");
        //Locators.CreateLocator(bezier[1].ToVector3XZ(), "bezier[1]");
        //Locators.CreateLocator(bezier[2].ToVector3XZ(), "bezier[2]");
        //Locators.CreateLocator(bezier[3].ToVector3XZ(), "bezier[3]");
        //Locators.CreateLocator(point.ToVector3XZ(), "point");
        //Locators.CreateLocator(start, "start");
        //Locators.CreateLocator(closestPoint.ToVector3XZ(), "closestPoint");
        
       // Debug.Log("P: " + startPos + " R: " + startDir + " 0: " + bezier[0] + " 1: " + bezier[1] + " 2: " + bezier[2] + " 3: " + bezier[3] + " " + dist + " " + dist2);
        isSplineCreated = true;

        //localStartDist = Vector2.Distance(bezier[0], bezier[0]);
        localTargetDist = Vector2.Distance(bezier[0], bezier[3]);
        //Debug.Log(localTargetDist);
        
        for (int i = 0; i < 20; i++)
            Length += Vector3.Distance(Math3d.CubicBezierLerp(bezier[0], bezier[1], bezier[2], bezier[3], (float)i / 20.0f), Math3d.CubicBezierLerp(bezier[0], bezier[1], bezier[2], bezier[3], (float)(i + 1) / 20.0f));

    }
	PosRot CreateSegment(Vector3 pos, Vector3 dir, bool _direction, float wayLenght, ref OneSpline currentSpline)
    {
        ch2++;
        //Vector3 nextPos = Vector3.zero;
        //Vector3 nextDir = Vector3.zero;

        PosRot posRot = Main.instance.GetNextDot(pos, _direction, 3.0f, transform, ref currentSpline);

        //posRot.pos = Math3d.RotateThis(master.offset, 180.0f, posRot.pos);
        //Debug.Log(posRot.pos + " " +  gaga);

        Vector3 globalPos = transform.TransformPoint(posRot.pos);
        float aiming = Vector3.Distance(globalPos, target);
        float aimingFactor = Mathf.Lerp(0, 1, (Mathf.Clamp(aiming, 10.0f, 50.0f)-10.0f) / 40.0f);
        Vector3 globalRot = Vector3.zero;

        if (!isSplineCreated)
                  CreateSpline(globalPos);


        float currentDist = Vector2.Distance(globalPos.ToVector2XZ(), bezier[0]);
       // Debug.Log(currentDist + " " + localTargetDist);
        
        
        if (currentDist < localTargetDist)
        {
            //float part = currentDist - localStartDist;
            float t = 3.0f / Length;
            Vector3 _pos = Math3d.CubicBezierLerp(bezier[0].ToVector3XZ(), bezier[1].ToVector3XZ(), bezier[2].ToVector3XZ(), bezier[3].ToVector3XZ(), t * ch4);
            //Debug.Log(t * ch4 + " " + ch2);
            //Debug.Log(currentDist + " " + localStartDist + " " + localTargetDist + " " + t);
            posRot.pos = transform.InverseTransformPoint(_pos);
            ch4++;
        }
        else
            {
                posRot.pos = posRot.pos + (Quaternion.Euler(0, 90, 0) * posRot.rot) * targetOffset;
                //posRot.pos = posRot.pos + (Quaternion.Euler(0, 270, 0) * posRot.rot) * ( targetOffset + master.offset/2);
                //posRot.pos = Math3d.RotateThis(-master.offset + ((targetOffset < 0) ? dist2 : -dist2), 180.0f, posRot.pos);
                //posRot.pos = Math3d.RotateThis(master.offset + ((targetOffset < 0) ? dist2 : -dist2), 180.0f, posRot.pos);

                //posRot.pos = Math3d.RotateThis(((targetOffset < 0) ? dist2 : -dist2), 180.0f, posRot.pos);
                //posRot.pos = posRot.pos + (Quaternion.Euler(0, 90, 0) * posRot.rot) * (master.offset + ((targetOffset < 0) ? -dist2 : dist2));
                //Debug.Log(ch2);
            }
            //else
            //    if (currentDist <= localStartDist)
         
            //{
                
        //
                    //posRot.pos = Math3d.RotateThis(master.offset, 180.0f, posRot.pos);
                    //Debug.Log(ch2);
            //}
        

        //posRot.pos = Math3d.RotateThis(Mathf.Lerp(0.0f,4.0f,aimingFactor), 180.0f, posRot.pos);
        //posRot.rot = Vector3.Lerp(posRot.rot, transform.InverseTransformDirection(Vector3.Normalize(target - globalPos)), aimingFactor);

        globalPos = transform.TransformPoint(posRot.pos);
        globalRot = transform.TransformDirection(posRot.rot);

        
        //Debug.Log(aimingFactor);
        //posRot.pos = Math3d.RotateThis(5.0f, Mathf.Lerp(0,90,aimingFactor), posRot.pos);
        //posRot.rot = transform.InverseTransformDireaction(Vector3.Normalize(target - globalPos));
        //posRot.rot = Vector3.Lerp(posRot.rot, transform.InverseTransformDirection(Vector3.Normalize(target - globalPos)), 1.0f);
       // Debug.Log(aiming + " " + aimingFactor + " " + globalPos);
        //if (ch2 > 2)
        //{
            //Debug.Log(posRot.rot + " ");
           // globalRot = Vector3.Lerp(transform.TransformDirection(posRot.rot), Vector3.Normalize(target - globalPos), 1.0f);
            
            //globalRot = Vector3.Normalize(target - globalPos);

            //posRot.rot += new Vector3(0.1f,0,0) * ch2;// transform.InverseTransformDirection(globalRot);

           // Debug.Log(posRot.rot + " ");
       //}
       // else
            
            //globalRot = Vector3.Normalize(globalPos - Main.instance.transform.position);
            //posRot.rot = transform.InverseTransformDirection(globalRot);

        Debug.DrawLine(globalPos, globalPos + Vector3.up * 5, new Color(0,ch2,0,1) , 10.0f);
        Debug.DrawLine(globalPos + Vector3.up * 2, globalPos + Vector3.up * 2 + globalRot * 2, Color.Lerp(Color.yellow, Color.red, aimingFactor), 10.0f);

        return posRot;
    }

    Vector3 SnapToTerrain(Vector3 source)
    {
        
        source.y += 100.0f;
        int layerMask = 1 << 0 | 1 << 9 | 1 << 12;
        layerMask = ~layerMask;
        RaycastHit Hit;

        if (Physics.Raycast(transform.TransformPoint(source), Vector3.down, out Hit, Mathf.Infinity, layerMask))
        {
            source.y = transform.InverseTransformPoint(Hit.point).y;
        }
        
        return source;
    }

        /*
        Vector3[] vertices = currentMeshFilter.mesh.vertices;
        for (int i = 0; i < currentMeshFilter.mesh.vertexCount; i++)
        {
            Vector3 global = transform.TransformPoint(vertices[i]);
            global.y = 500.0f;

            int layerMask = 1 << 0 | 1 << 9 | 1 << 12;
            layerMask = ~layerMask;
            RaycastHit Hit;

            if (Physics.Raycast(global, Vector3.down, out Hit, Mathf.Infinity, layerMask))
            {

                global.y = Hit.point.y;
                //Debug.Log(i + " " + global.y);
                vertices[i].y = transform.InverseTransformPoint(global).y;
            }
            else
                Debug.Log(i + " RayCast Decal Snap Error");


        }

        Deform.vertices = vertices;
        Deform.SetIndices(currentMeshFilter.mesh.GetIndices(0), currentMeshFilter.mesh.GetTopology(0), 0);
        Deform.SetTriangles(currentMeshFilter.mesh.triangles, 0);
        Deform.uv = currentMeshFilter.mesh.uv;
        Deform.uv2 = currentMeshFilter.mesh.uv2;
        Deform.normals = currentMeshFilter.mesh.normals;
        Deform.tangents = currentMeshFilter.mesh.tangents;

        
        Deform.RecalculateBounds();
        currentMeshFilter.sharedMesh = Deform;
         */
    //}

}
