﻿using UnityEngine;
using System.Collections;

public class Civilian : EnemyVehicle 
{
    public Transform dir;

    public virtual void CollisionWithUnit(UnityEngine.Collision collision)
    {

        if (collision.gameObject.CompareTag("Unit"))
        {
            Unit unit = collision.gameObject.GetComponent<Unit>();
            bool withHero = (unit.unitType == UnitType.Hero) ? true : false;

            if (collision.relativeVelocity.magnitude > armor)
            {
                if (garbageEnable)
                    DamageHull(collision.contacts[0].point);
                GetDamage(collision.contacts[0].point, collision.relativeVelocity, collision.rigidbody.mass, withHero);

                if (withHero && heroAbilityEffect && isCollisionActive)
                {
                    SideQuad heroQuad = GetSideQuad(collision.contacts[0].point, Main.instance.hero.transform);

                    isAttackedByHero = true;
                    isCollisionActive = false;

                    if (heroQuad == SideQuad.TopLeft || heroQuad == SideQuad.TopRight)
                    {
                        
                        
                        Main.instance.SelectTarget(this);
                        heroAttackTimer.Restore();

                        float magn = collision.relativeVelocity.magnitude;

                        Vector3 dir = -1 * Vector3.Normalize(collision.relativeVelocity);
                        Vector3 point = transform.position + dir;
                        point.y -= 1f;

                        if (magn > 30.0f && isBonusHas && Math3d.GetChanceOf(bonusChance))
                        {
                            TakeBonus();
                            durabilityStatus = DurabilityStatus.Ruined;
                        }

                        float dist = Vector3.Distance(transform.position, point);
						float diff = Mathf.Clamp(Main.instance.hero.vehicle.currentRigidbody.mass / currentRigidbody.mass, 1.0f, 2.0f);
                        currentRigidbody.AddExplosionForce((magn + 10) * diff * (0.75f + currentRigidbody.mass / 1000) * currentRigidbody.mass, point, 25.0f, magn * diff * 200.0f, ForceMode.Force);

                        //Debug.Log(magn);
                    }
                }
            }

        }
        else
            if (collision.gameObject.CompareTag("Solid"))
            {
                if (collision.relativeVelocity.magnitude > armor)
                    GetDamage(collision.contacts[0].point, collision.relativeVelocity, currentRigidbody.mass, false);
            }
    }
     
}
