﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum Roles
{
    Dominate,
    Deceive,
    Melee,
    Leave,  
}

public enum Counts
{
    One,
    Two,
}

public class GrenadeActive
{
    public bool isLaunched = false;
    public OneTarget target;
    public CounterTimerF timer;
}

public class Baggy : EnemyVehicle
{

    public EnemyVisualMachineGun MachineGun;
    
    public CounterTimerF defaultTimer;
    public CounterTimerF rotateTimer;
    public CounterTimerF roleTimer;
    bool rightSide = true;
    public FollowMoving moving;
    public Roles roles = Roles.Dominate;

    public CounterTimerF machineGunTimer;
    public CounterTimerF grenadeTimer;

    public GrenadeActive[] grenades;

    public OneArcMachine machineTarget;
    
    public Transform gunTurret;
    public Counts counts;

    private Vector3 RelativeHeroPosition;
    public Object GrenadeTarget;
    public Object MachineGunTarget;
    public Object grenade;
    public float grenadeLife;

    public float MaxDistance = 30.0f;
    public float AverageDistance = 25.0f;
    public float MinDistance = 20.0f;

    public float MaxDistanceDeceive = 20.0f;
    public float AverageDistanceDeceive = 15.0f;
    public float MinDistanceDeceive = 10.0f;

    public float MaxDistanceFlame = 10.0f;
    public float AverageDistanceFlame = 5.0f;
    public float MinDistanceFlame = 0.0f;

    public float AccelerationBonus = 100.0f;
    public float BrakeBonus = 200.0f;
    public float SpeedDifference = 5.0f;

    public bool isTarget;
    public int grenadeCount;
    public int grenadeCh = 0;

    public bool isMachinGunAttackLeft;

    public override void RevertObject()
    {
        base.RevertObject();
        roles = Roles.Dominate;
        roleTimer.current = roleTimer.max;
        moving = FollowMoving.Move;
        MachineGun.StopShoot();

    }
  
    
    public override void PrepareObject()
    {

        base.PrepareObject();
        //defaultTimer = new CounterTimerF(6.0f, 6.0f);
        //roleTimer.max = Random.Range(8, 16);
        roleTimer.current = roleTimer.max;
        //defaultTimer.current = deathTimer
        isTarget = false;
    }


    public override void SwitchSkillStatus()
    {
        /*
        int countB = 0;
        for (int i = 0; i<Main.instance.activeUnitObjects.Count; i++)
        {
            if (Main.instance.activeUnitObjects[i].type == EnemyType.Baggy)
                countB++;
        }

        if (countB == 2)
        {
            Main.instance.coop.Activate();
        }
        else
            Main.instance.coop.Deactivate();
        */
        Vector3 directionToTarget = Vector3.Normalize(Main.instance.hero.transform.position - transform.position);

        float yAngle = 0;

        RelativeHeroPosition = transform.InverseTransformPoint(Main.instance.hero.transform.position);
        if (RelativeHeroPosition.x > 0)
            yAngle = Vector3.Angle(transform.right, directionToTarget);
        else
            yAngle = Vector3.Angle(transform.right * -1, directionToTarget) * -1;

        float factor = RelativeHeroPosition.z;


            MovementFunctions(Waypoint, true, true, false);


            StateController(factor);

            //DropText(skillStatus.ToString() + " " + roles.ToString());
    }

    private void StateController(float factor)
    {
        switch (skillStatus)
        {

            case SkillStatus.Default:
                {

                    if (defaultTimer.isEnd(Time.deltaTime) && RelativeHeroPosition.magnitude < AverageDistance)
                    {
                        if (roles == Roles.Dominate)
                        {
                            if (Math3d.GetChanceOf(50))

                                skillStatus = SkillStatus.RangedAttack;
                            else
                            {

                                skillStatus = SkillStatus.ExplosiveAttack;
                            }
                        }
                    }

                }
                break;

            case SkillStatus.RangedAttack:
                {
                    if (!isTarget)
                    {
                        machineTarget = Main.instance.mainCamera.targetController.CreateTargetArc(true, machineGunTimer.max, MachineGunTarget, transform);

                        MachineGun.spline = machineTarget.spline;
                        MachineGun.StartShoot();
                        machineGunTimer.current = machineGunTimer.max;
                        isMachinGunAttackLeft = Math3d.GetChanceOf(50);
                        isTarget = true;
                        
                    }
                    if (machineGunTimer.isEnd(Time.deltaTime) || CheckGround()<2)
                    {

                        skillStatus = SkillStatus.Default;
                        gunTurret.localEulerAngles = Vector3.zero;
                        Destroy(machineTarget.gameObject);
                        MachineGun.StopShoot();
                        MachineGun.gun.magazine.current = MachineGun.gun.magazine.max;
                        isTarget = false;
                    }
                    else
                    {
                        float factorSide = machineGunTimer.current / machineGunTimer.max;
                        Vector3 inverse = machineTarget.spline.GlobalPos((isMachinGunAttackLeft) ? factorSide : 1 - factorSide);
                        Debug.DrawLine(inverse, inverse + Vector3.up * 3, Color.green);
                        inverse.x += Random.Range(0, 0.30f)-0.15f;
                        inverse.z += Random.Range(0, 0.30f) - 0.15f;
                        MachineGun.GetInversePoint(inverse, machineTarget);
                        gunTurret.LookAt(inverse);
                    }
                }

                break;
            
            case SkillStatus.ExplosiveAttack:
                {
                    grenadeTimer.current -= Time.deltaTime;
                    
                    if (grenadeTimer.current < grenadeTimer.max && !isTarget && RelativeHeroPosition.z < -5.0f && RelativeHeroPosition.z > -20.0f)
                    {
                        
                        isTarget = true;
                        grenades = new GrenadeActive[grenadeCount];

                        
                        OneTarget[] targets = Main.instance.mainCamera.targetController.CreateTargets(grenadeCount, 2.0f, GrenadeTarget, transform);
                        for (int i = 0; i < grenadeCount; i++ )
                        {
                            grenades[i] = new GrenadeActive();
                            grenades[i].target = targets[i];
                            float dist = Vector3.Distance(targets[i].transform.position, transform.position);
                            
                            grenades[i].timer.max = grenades[i].target.timer.max - dist / 12;
                            grenades[i].timer.current = grenades[i].timer.max;
                            //Debug.Log(dist + " " + grenades[i].target.timer.max.ToString() + " " + grenades[i].timer.max);
                            
                        }

                    }
                    
                    if (isTarget)
                    for (int i = 0; i < grenadeCount; i++)
                    {
                        if (grenades[i].timer.isEnd(Time.deltaTime))
                        {
                            GrenadeLaunch(i); 
                        }


                    }
                    
                    if (grenadeCh==grenadeCount)
                    {
                        skillStatus = SkillStatus.Default;
                        grenadeTimer.current = grenadeTimer.max;
                        grenadeCh = 0;
                        isTarget = false;
                    }
                    

                }

                break;
             
        }
    }

    void GrenadeLaunch(int current)
    {
        if (!grenades[current].isLaunched)
        {
            grenades[current].isLaunched = true;


            GameObject newBomb = GameObject.Instantiate(grenade, transform.position, Quaternion.identity) as GameObject;
            Shell bombComponent = newBomb.GetComponent<Shell>();
            bombComponent.transform.parent = Main.instance.mainCamera.targetController.transform;
            bombComponent.lifeTime.max = grenades[current].target.timer.current;
            bombComponent.lifeTime.current = bombComponent.lifeTime.max;
            bombComponent.master = transform;
            bombComponent.startPos = Main.instance.mainCamera.targetController.transform.InverseTransformPoint(transform.position);
            bombComponent.oneTarget = grenades[current].target;
            bombComponent.cameraController = Main.instance.mainCamera.targetController.transform;

            grenadeCh++;

        }
        
    }

    public override void CreateNewWaypoint(float wayLength, bool needNewSpline)
    {
        currentSplineOffset = CalculateOffsetFromHero(Random.Range(0.8f, 1.2f));

        base.CreateNewWaypoint(wayLength, needNewSpline);
    }

    public override void Boost()
    {
        currentRigidbody.velocity = transform.TransformDirection(Vector3.forward * Math3d.GetSpeed(currentRpm, wheelsColliderBack[0].radius));
        boost = false;
    }
    
    /*
    public override bool isTooFar(float _Dist, float _trafficDist)
    {

        float Dist = Vector3.Distance(transform.position, Main.instance.mainCamera.transform.position);
        if (Dist > _Dist)
            return true;

        return false;
    }
    */
    
    public override float Avoidance(bool isLong, float multi)
    {
        return base.Avoidance(false, 1.5f);

    }
    
    
    public override void AverageAuto(float clamp, out float motorTorque, out float brakeTorque)
    {
        motorTorque = 0;
        brakeTorque = 0;
        
        float difference = Main.instance.hero.forwardSpeedTact - forwardSpeed;
        float distance = -RelativeHeroPosition.z;

        if (distance < AverageDistance)
        {
            if (roleTimer.isEnd(Time.deltaTime))
            {
                
                        roles = SwitchRole();                                      

                        roleTimer.current = roleTimer.max;

                        if (skillStatus == SkillStatus.RangedAttack && roles == Roles.Melee)

                        {
                            skillStatus = SkillStatus.Default;
                            gunTurret.localEulerAngles = Vector3.zero;
                            MachineGun.StopShoot();

                        }
             }
            
            }
        

        switch (roles)
        {
            case Roles.Dominate:
                Domination(ref motorTorque, ref brakeTorque, ref distance, ref difference);
            break;
            case Roles.Melee:
                Domination(ref motorTorque, ref brakeTorque, ref distance, ref difference);
            break;
            case Roles.Deceive:
                Deceive(ref motorTorque, ref brakeTorque, ref distance, ref difference);
            break;
        }

        
    }

    

    Roles SwitchRole()
    {
        /*
        if (roles != Roles.Deceive)
        {
            float r = Random.Range(0.0f, 100.0f);
            if (r < 20.0f)
                return Roles.Deceive;
        }

        if (roles == Roles.Dominate)
            return Roles.Melee;
        else
            if (roles == Roles.Melee)
                return Roles.Dominate;

        return Roles.Dominate;
        */
        if (Math3d.GetChanceOf(0))
            return Roles.Deceive;
            else
            return Roles.Dominate;


    }

    void Domination(ref float motorTorque, ref float brakeTorque, ref float distance, ref float difference)
    {
        if ((distance < MinDistance || difference > SpeedDifference))
            moving = FollowMoving.Boost;
        else
            if (distance > AverageDistance)
                moving = FollowMoving.Brake;
            else
                moving = FollowMoving.Move;

        if (forwardSpeed > Main.instance.hero.maxSpeed)
            moving = FollowMoving.Brake;

        switch (moving)
        {
            case FollowMoving.Boost:

                motorTorque = currentAccel * inputTorque;
                if (distance < MinDistance)
                    motorTorque += Mathf.Lerp(AccelerationBonus, 0, Mathf.Clamp(distance, 0, MinDistance));
                if (difference > 0)
                    motorTorque += AccelerationBonus * (difference / SpeedDifference);
                if (difference < 0)
                    brakeTorque += Mathf.Lerp(BrakeBonus, 0, Mathf.Clamp(difference, 0, SpeedDifference) / SpeedDifference);
                brakeTorque = 0;// Mathf.Abs(inputSteer * currentBrake * 0.5f);

                break;
            case FollowMoving.Move:



                motorTorque = 0;
                brakeTorque = 0;// Mathf.Abs(inputSteer * currentBrake * 0.5f);

                if (difference > 0)
                    motorTorque += inputTorque * AccelerationBonus * (difference / SpeedDifference);
                if (difference < 0)
                    brakeTorque += BrakeBonus * (-difference / SpeedDifference);


                break;
            case FollowMoving.Brake:


                motorTorque = 0;
                brakeTorque = currentBrake + Mathf.Lerp(0, BrakeBonus, Mathf.Clamp(distance, AverageDistance, MaxDistance));
                if (difference < 0)
                    brakeTorque += BrakeBonus * (-difference / SpeedDifference);
                break;
        }      
            //string text = moving.ToString() + " " + motorTorque.ToString("F1") + " " + brakeTorque.ToString("F1") + " " + RelativeTargetPosition.z.ToString("F1") + " " + (forwardSpeed - Main.instance.hero.forwardSpeed).ToString("F1");
        
    }

    

    public void Deceive(ref float motorTorque, ref float brakeTorque, ref float distance, ref float difference)
    {
        //difference -= 7.5f;

        if (distance < MinDistanceDeceive)
            moving = FollowMoving.Boost;
        else
            if (distance > AverageDistanceDeceive)
                moving = FollowMoving.Brake;
            else
                moving = FollowMoving.Move;

        if (forwardSpeed > 40.0f)
            moving = FollowMoving.Brake;

        switch (moving)
        {
            case FollowMoving.Boost:

                motorTorque = currentAccel * inputTorque + Mathf.Lerp(AccelerationBonus, 0, Mathf.Clamp(distance, 0, MinDistanceDeceive));            
                brakeTorque = 0;

                break;
            case FollowMoving.Move:

                if (difference < 0)
                    motorTorque = inputTorque * Mathf.Lerp(AccelerationBonus, 0, Mathf.Clamp(distance, 0, MinDistanceDeceive));
                if (difference > 0)
                    brakeTorque = Mathf.Lerp(0, BrakeBonus, Mathf.Clamp(distance, AverageDistanceDeceive, MaxDistanceDeceive));
                break;

            case FollowMoving.Brake:

                motorTorque = 0;
                brakeTorque = currentBrake + Mathf.Lerp(0, BrakeBonus, Mathf.Clamp(distance, AverageDistanceDeceive, MaxDistanceDeceive));
                
                break;
        }

        if (difference > 0)
            motorTorque += inputTorque * currentAccel / 1.5f * Mathf.Clamp01(Mathf.Abs(difference));
        if (difference < 0)
            brakeTorque += currentBrake / 1.5f * Mathf.Clamp01(Mathf.Abs(difference));

        //if (difference > 0)
        //    motorTorque += AccelerationBonus * (difference / SpeedDifference);
        //if (difference < 0)
        //    brakeTorque += BrakeBonus * (-difference / SpeedDifference);

    }

    public void Leave(ref float motorTorque, ref float brakeTorque, ref float distance, ref float difference)
    {
        if (forwardSpeed > 35.0f)
            brakeTorque = currentBrake;
        else
            motorTorque = currentAccel * inputTorque;

       //DropText(Vector3.Distance(transform.position, Main.instance.mainCamera.transform.position).ToString());
    }


    
    
}
