﻿using UnityEngine;
using System.Collections;

public class Trailer : EnemyVehicle
{
    public Wagon wagon;

    public override void RevertObject()
    {
        base.RevertObject();

        wagon.transform.localPosition = wagon.startPose;
        wagon.transform.localRotation = Quaternion.identity;

        wagon.currentRigidbody.velocity = Vector3.zero;
        wagon.currentRigidbody.angularVelocity = Vector3.zero;

    }

    public override void Moving()
    {
        //wagon.intersectRect.UpdateRect(wagon.transform);
        base.Moving();
    }

    public override void Boost()
    {
        base.Boost();
        wagon.currentRigidbody.velocity = transform.TransformDirection(Vector3.forward * startVelocityZ);
    }
    /*
    public override void Active(bool active)
    {

        if (active == activeInCamera)
            return;

        activeInCamera = active;

        //currentRenderer.enabled = active;
        //wagon.currentRenderer.enabled = active;
        //if (active)
        //currentRigidbody.WakeUp();
        //else
        //currentRigidbody.Sleep();
        Arb[0].enabled = active;
        Arb[1].enabled = active;

        wagon.Arb[0].enabled = active;
        wagon.Arb[1].enabled = active;

    }
   */

}
