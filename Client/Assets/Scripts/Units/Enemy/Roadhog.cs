﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Dest.Math;

public class Roadhog : EnemyVehicle
{

    private bool isImpact = false;
    private Vector3 RelativeTargetPosition;
    private float factor;
    private SkillStatus nextSkillStatus = SkillStatus.Default;
    bool isDiversionNegative;
    bool isPrepare;

    public Object decalRangeAttack;

    public CounterTimerF meleeAttackTimer;
    public CounterTimerF rangeAttackTimer;
    public CounterTimerF cooldownTimer;
    public CounterTimerF prepareTimer;
    public CounterTimerF defaultTimer;
    public CounterTimerF diversionTimer;
    
    public GameObject decaRangeAttackActive;


    private bool isAttackTypeChoise;

    public EnemyGun[] Pistols;

    public override void PrepareObject()
    {
        base.PrepareObject();

        prepareTimer.Restore();
        meleeAttackTimer.Restore();
        rangeAttackTimer.Restore();
        cooldownTimer.Restore();
        defaultTimer.Restore();
        diversionTimer.Restore();

        isImpact = false;
        force = false;
        isPrepare = false;
    }

    public override void CreateObject()
    {
        base.CreateObject();
        Pistols = GetComponentsInChildren<EnemyGun>();

    }

    public override void CollisionWithUnit(Collision collision)
    {
        if (skillStatus == SkillStatus.MeleeAttack)
        {
            if (collision.gameObject.CompareTag("Unit"))
            {
                Unit unit = collision.gameObject.GetComponent<Unit>();
                bool withHero = (unit.unitType == UnitType.Hero) ? true : false;
                if (!isImpact)
                {
                    if (withHero)
                        unit.currentRigidbody.AddForceAtPosition(collision.impulse * -1.0f, collision.contacts[0].point, ForceMode.Impulse);
                    isImpact = true;
                    meleeAttackTimer.Restore();
                }
            }
        }
        else
        {
            base.CollisionWithUnit(collision);
        }
        
        
       
    }

    public void CreateRoadHogTarget(float forward, float back)
    {
        follow = true;
        Vector3 localPos = Main.instance.mainCamera.targetController.transform.InverseTransformPoint(Main.instance.hero.transform.position);
		OneSpline spline = Math3d.GetEnemySpline(Main.instance.hero.transform.position);
        if (spline == null)
            Kill();
        float width = spline.GetCurrentWidth(Main.instance.hero.currentStep) - 0.5f;
        localPos.z += Random.Range(0, forward+back)-back;

        float leftBorder = Main.instance.hero.currentSplineOffset - 4.0f;
        float rightBorder = Main.instance.hero.currentSplineOffset + 4.0f;

        if (-width > leftBorder)
        {
            localPos.x = Random.Range(rightBorder, width);
            //Debug.Log("LEFT " + -width + " " + leftBorder + " " + rightBorder + " " + localPos.x);
        }
        else
            if (width < rightBorder)
            {
                localPos.x = Random.Range(0.0f, leftBorder + width) - width;
                //Debug.Log("RIGHT " + width + " " + rightBorder + " " + leftBorder + " " + localPos.x);
            }
            else
            {
                if (Mathf.Abs(leftBorder - currentSplineOffset) > Mathf.Abs(rightBorder - currentSplineOffset))
                    localPos.x = Random.Range(rightBorder, width);
                else
                    localPos.x = Random.Range(0.0f, leftBorder + width)-width;
                //Debug.Log("TRAITOR " + Mathf.Abs(leftBorder - offset) + " " + Mathf.Abs(rightBorder - offset));
            }
        CreateTarget(localPos);
        
        
        
        
    }
    public override void Boost()
    {

        CreateRoadHogTarget(0.0f, 0.0f);
     
        currentRigidbody.velocity = transform.TransformDirection(Vector3.forward * Math3d.GetSpeed(currentRpm, wheelsColliderBack[0].radius));
        boost = false;

    }

    public override void SwitchSkillStatus()
    {
        Vector3 directionToTarget = Vector3.Normalize(Main.instance.hero.transform.position - transform.position);
        RelativeTargetPosition = transform.InverseTransformPoint(Main.instance.hero.transform.position);
        float distance = RelativeTargetPosition.z;
        factor = Mathf.Clamp(distance, -3.0f, 3.0f)/3;

        float yAngle = 0;


        switch (skillStatus)
        {
            case SkillStatus.Default:
                {
                    if (!isAttackTypeChoise)
                    {
                        if (Math3d.GetChanceOf(60))
                        {
                     
                            nextSkillStatus = SkillStatus.MeleeAttack;
                        }
                        else
                        {
                            
                            
                            nextSkillStatus = SkillStatus.RangedAttack;
                        }
                        
                        isAttackTypeChoise = true;
                    }

                    if (defaultTimer.isEnd(Time.deltaTime))
                    {
                        DoDefault();
                    }

                    if (cooldownTimer.isEnd(Time.deltaTime))
                    {
                        isPrepare = true;
                    }

                    if (isPrepare)
                    {
                        if (factor > -1.0f && factor < 1.0f && RelativeTargetPosition.magnitude < 8.0f)
                        {

                            //necessarySpeed = Main.instance.hero.forwardSpeedTact;
                            RaycastHit hit;
                            if (Physics.Raycast(transform.position, directionToTarget, out hit, 30.0f))
                            {
                                if (hit.transform.CompareTag("Unit"))
                                {
                                    Unit unit = hit.transform.GetComponent<Unit>();
                                    if (unit.unitType == UnitType.Hero)
                                    {
                                        if (decaRangeAttackActive == null && nextSkillStatus == SkillStatus.RangedAttack)
                                        {
                                            decaRangeAttackActive = GameObject.Instantiate(decalRangeAttack, transform.position, transform.rotation) as GameObject;
                                            decaRangeAttackActive.transform.parent = transform;

                                            if (RelativeTargetPosition.x > 0)
                                                decaRangeAttackActive.transform.localRotation = Quaternion.Euler(new Vector3(0, 90, 0));
                                            else
                                                decaRangeAttackActive.transform.localRotation = Quaternion.Euler(new Vector3(0, -90, 0));

                                        }

                                        if (prepareTimer.isEnd(Time.deltaTime))
                                        {
                                            if (decaRangeAttackActive != null)
                                                Destroy(decaRangeAttackActive);

                                            skillStatus = nextSkillStatus;
                                            isPrepare = false;
                                            isAttackTypeChoise = false;
                                            meleeAttackTimer.Restore();
                                            rangeAttackTimer.Restore();

                                            if (skillStatus == SkillStatus.RangedAttack)
                                            {
                                                CreateRoadHogTarget(0.0f, 0.0f);
                                            }
                                        }
                                    }

                                    else
                                    {
                                        if (decaRangeAttackActive != null)
                                            Destroy(decaRangeAttackActive);

                                        if (unit.unitType == UnitType.Enemy)
                                        {
                                            if (Math3d.GetChanceOf(10))
                                                DoDiversion();
                                        }
                                    }
                                }

                            }
                        }
                        else
                        {
                            if (decaRangeAttackActive != null)
                                Destroy(decaRangeAttackActive);

                            prepareTimer.Restore();
                        }
                    }
                    

                   //DropText("-1 " + factor + " 1 " + currentRigidbody.angularVelocity.magnitude.ToString("F2") + " " + forwardSpeed.ToString("F2") + " " + currentRigidbody.velocity.magnitude.ToString("F2"));
                    MovementFunctions(Waypoint, true, true, true);
                    
                }
                break;

            //ATTACK
            case SkillStatus.MeleeAttack:
                {
                    if (!force)
                    {
                        followTarget.localPosition = Main.instance.mainCamera.targetController.transform.InverseTransformPoint(Main.instance.hero.transform.position);
                        //offset = target.localPosition.x;
                        force = true;
                    }

                    if (meleeAttackTimer.isEnd(Time.deltaTime) || isImpact)
                    {
                        if (Math3d.GetChanceOf(10))
                            DoDiversion();
                        else
                            DoDefault();
                    }

                    /*
                    if (Mathf.Abs(distance) > 3.5f || RelativeTargetPosition.x > 0 && inputSteer < 0 || RelativeTargetPosition.x < 0 && inputSteer > 0)
                    {
                        if (Math3d.GetChanceOf(10))
                            DoDiversion();
                        else
                            DoDefault();
                    }
                    */
                     //Debug.Log("HYU");
                    MovementFunctions(Waypoint, false, true, false);
                }

                break;

            case SkillStatus.RangedAttack:
                {

                    

                    if (RelativeTargetPosition.x > 0)
                        yAngle = Vector3.Angle(transform.right, directionToTarget);
                    else
                        yAngle = Vector3.Angle(transform.right * -1, directionToTarget);

                    for (int i = 0; i < Pistols.Length; i++)
                    {
						Pistols[i].target = Main.instance.hero.vehicle;
                        Pistols[i].Attack();
                    }

                    MovementFunctions(Waypoint, true, true, true);

                    if (rangeAttackTimer.isEnd(Time.deltaTime) || Mathf.Abs(yAngle) > 40.0f)
                    {
                        for (int i = 0; i < Pistols.Length; i++)
                        {
                            Pistols[i].gun.magazine.Restore();
                            Pistols[i].Stop();
                        }

                        if (Math3d.GetChanceOf(30))
                            DoDiversion();
                        else
                            DoDefault();
                    }               
                }

                break;

            case SkillStatus.Diversion:
                {
                    if (diversionTimer.isEnd(Time.deltaTime))
                        DoDefault();

                    MovementFunctions(Waypoint, true, true, true);
                }

                break;    
        }

        //DropText(skillStatus.ToString());
    }

    void DoDefault()
    {
        defaultTimer.Restore();
        CreateRoadHogTarget(0.0f, 0.0f);
        force = false;
        skillStatus = SkillStatus.Default;
        isImpact = false;
        meleeAttackTimer.Restore();
        rangeAttackTimer.Restore();
        cooldownTimer.Restore();
    }

    void DoDiversion()
    {
        diversionTimer.Restore();
        
        skillStatus = SkillStatus.Diversion;
        isImpact = false;
        force = false;
        isDiversionNegative = Math3d.GetChanceOf(50);
        meleeAttackTimer.Restore();
        rangeAttackTimer.Restore();
        cooldownTimer.Restore();

        if (isDiversionNegative)
            CreateRoadHogTarget(-2.0f, 6.0f);
        else
            CreateRoadHogTarget(8.0f, -3.0f);
    }

    /*
       public override void CreateNewWaypoint(float wayLength, bool needNewSpline)
    {
        //Debug.Log("CreateDefaultWay");
        if (skillStatus == SkillStatus.MeleeAttack)
            offset = CalculateOffsetFromOffset(1.1f);
        if (skillStatus == SkillStatus.RangedAttack)
            offset = CalculateOffsetFromHero(1.65f);

        base.CreateNewWaypoint(wayLength, needNewSpline);
    }

    public void CreateNewWaypointStable(float wayLength, bool needNewSpline)
    {
        //Debug.Log("CreateNewWay");
        offset = CalculateOffsetFromOffset(1.25f);
        base.CreateNewWaypoint(wayLength, needNewSpline);
    }
    */



    public override float Avoidance(bool isLong, float multi)
    {

        float _multi = 1.2f;
        if (skillStatus == SkillStatus.RangedAttack || skillStatus == SkillStatus.MeleeAttack)
            _multi = 1.0f;
        return base.Avoidance(false, _multi);
    }


}
