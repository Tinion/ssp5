﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FlameTruck : EnemyVehicle
{
    public FlameThrower[] FlameThrower;

    
    public CounterTimerF roleTimer;

    public CounterTimerF defaultTimer;

    public Roles roles = Roles.Melee;

    private Vector3 RelativeHeroPosition;

    private bool dominate = false;
    
    /*
    private void StateController()
    {
        switch (skillStatus)
        {

            case SkillStatus.Default:
                {

                    if (defaultTimer.isEnd(Time.deltaTime))
                    {
                        bool left;
                        Vector3 target = CollectUnits(out left);
                        if (!Vector3.Equals(target,Vector3.zero))
                        {

                      //      skillStatus = SkillStatus.BattlePrepare;
                            //currentFlameThrower = left ? FlameThrowerLeft : FlameThrowerRight;


                            //currentFlameThrower.transform.LookAt(target);

                           // Vector3 loc = currentFlameThrower.transform.localEulerAngles;
                            //loc.x = 0; loc.z = 0;
                           // currentFlameThrower.transform.localEulerAngles = loc;


                            //prepareFlameThrowerTimer.current = prepareFlameThrowerTimer.max;
                        }
                    }

                }
                break;

            case SkillStatus.BattlePrepare:
                {

                  //  if (prepareFlameThrowerTimer.isEnd(Time.deltaTime))
                  //  {
                  //      currentFlameThrower.StartShoot();
                  //      flameThrowerTimer.current = flameThrowerTimer.max;
                  //      skillStatus = SkillStatus.ExplosiveAttack;
                  //  }
                }

                break;

            case SkillStatus.ExplosiveAttack:
                {
                   // if (flameThrowerTimer.isEnd(Time.deltaTime))
                   // {

                   //     skillStatus = SkillStatus.Default;
                        //currentFlameThrower.localEulerAngles = Vector3.zero;
                   //     currentFlameThrower.StopShoot();
                   // }
                }

                break;
        }
    }
    */
    public override void RevertObject()
    {
        base.RevertObject();
        defaultTimer.current = defaultTimer.max;
        roleTimer.max = 6.0f;
        roleTimer.current = roleTimer.max;
        roles = Roles.Melee;
        roleTimer.current = roleTimer.max;
        //flameThrowerTimer.current = flameThrowerTimer.max;
        //prepareFlameThrowerTimer.current = prepareFlameThrowerTimer.max;
        dominate = false;

    }

    public override void PrepareObject()
    {

        base.PrepareObject();
        defaultTimer.current = defaultTimer.max;
        roleTimer.max = 6.0f;
        roleTimer.current = roleTimer.max;
        roles = Roles.Melee;
        roleTimer.current = roleTimer.max;
        //flameThrowerTimer.current = flameThrowerTimer.max;
        //prepareFlameThrowerTimer.current = prepareFlameThrowerTimer.max;
        dominate = false;
    }

    public override void SwitchSkillStatus()
    {

        Vector3 directionToTarget = Vector3.Normalize(Main.instance.hero.transform.position - transform.position);

        float yAngle = 0;

        Vector3 RelativeTargetPosition = transform.InverseTransformPoint(Main.instance.hero.transform.position);
        if (RelativeTargetPosition.x > 0)
            yAngle = Vector3.Angle(transform.right, directionToTarget);
        else
            yAngle = Vector3.Angle(transform.right * -1, directionToTarget) * -1;

        float factor = RelativeTargetPosition.z;


        MovementFunctions(Waypoint, true, true, true);

        switch (roles)
        {
            case Roles.Melee:
                {
                    if (roleTimer.isEnd(Time.deltaTime))
                    {
                        if (Vector2.Distance(transform.position, Main.instance.hero.transform.position) < 10.0f)
                        {
                            currentRpm = 6000.0f;
                            roles = Roles.Dominate;
                            roleTimer.max = roleTimer.max * 2;
                            roleTimer.current = roleTimer.max;
                        }
                    }
                }
                break;
            case Roles.Dominate:
                {
                    if (roleTimer.isEnd(Time.deltaTime))
                    {
                        roleTimer.current = roleTimer.max;
                        currentRpm = 650.0f;
                        roles = Roles.Deceive;
                    }
                }
                break;
            case Roles.Deceive:
                {
                    if (roleTimer.isEnd(Time.deltaTime))
                    {
                        if (dominate)
                        {
                            currentRpm = 350.0f;
                            roles = Roles.Leave;
                        }
                        else
                        {
                            dominate = true;
                            currentRpm = 6000.0f;
                            roles = Roles.Dominate;
                            roleTimer.current = roleTimer.max;
                        }
                    }
                }
                break;
        }


        if (defaultTimer.isEnd(Time.deltaTime))
        {
            //  StateController();
            Collider[] victims = Physics.OverlapSphere(transform.position, 20.0f);
            for (int i = 0; i < victims.Length; i++)
            {
                
                if (victims[i].gameObject.CompareTag("Unit"))
                {
                   
                    Unit unit = victims[i].transform.parent.GetComponent<Unit>();
                    if (unit != null)
                    {
                        if (unit != this)
                        { 
                        Vector3 inverseUnitPos = transform.InverseTransformPoint(unit.transform.position);
                        if (inverseUnitPos.z > 0)
                        {
                            if (inverseUnitPos.x > 0)
                                FlameThrower[1].SetTarget(unit);
                            else
                                FlameThrower[0].SetTarget(unit);
                        }
                        else
                        {
                            if (inverseUnitPos.x > 0)
                                FlameThrower[3].SetTarget(unit);
                            else
                                FlameThrower[2].SetTarget(unit);
                        }
                        }
                    }

                }
            }

        }

    }

    Vector3 CollectUnits(out bool left)
    {
        left = true;
        List<Unit> unitsInLeftArea = new List<Unit>();
        List<Unit> unitsInRightArea = new List<Unit>(); 
        int count = Main.instance.activeUnitObjects.Count;
        Vector3 localPos = Vector3.zero;
        for (int i = 0; i < count; i++)
        {
            if (Main.instance.activeUnitObjects[i] != this)
            {
                
                localPos = transform.InverseTransformPoint(Main.instance.activeUnitObjects[i].transform.position);
                Debug.Log(localPos + " " + Main.instance.activeUnitObjects[i].name);
                if (Mathf.Abs(localPos.x) > 1.0f && Mathf.Abs(localPos.x) < 15.0f && Mathf.Abs(localPos.z) < 17.5f)
                {
                    if (localPos.x < 0)
                        unitsInLeftArea.Add(Main.instance.activeUnitObjects[i]);
                    else
                        unitsInRightArea.Add(Main.instance.activeUnitObjects[i]);
                }
            }
        }
        localPos = transform.InverseTransformPoint(Main.instance.hero.transform.position);
        Debug.Log(localPos + " " + Main.instance.hero.name);
        if (Mathf.Abs(localPos.x) > 1.0f && Mathf.Abs(localPos.x) < 15.0f && Mathf.Abs(localPos.z) < 12.5f)
        {
            if (localPos.x < 0)
				unitsInLeftArea.Add(Main.instance.hero.vehicle);
            else
				unitsInRightArea.Add(Main.instance.hero.vehicle);
        }

        Debug.Log(unitsInLeftArea.Count + " " + unitsInRightArea.Count);

        if (unitsInLeftArea.Count == 0 && unitsInRightArea.Count == 0)
            return Vector3.zero;

        Vector3 result = Vector3.zero;
        if (unitsInLeftArea.Count > unitsInRightArea.Count)
        {
            for (int i = 0; i < unitsInLeftArea.Count; i++)
            {
                result += unitsInLeftArea[0].transform.position;
            }
            result /= unitsInLeftArea.Count;
            left = true;
        }


        if (unitsInRightArea.Count > unitsInLeftArea.Count)
        {
            for (int i = 0; i < unitsInRightArea.Count; i++)
            {
                result += unitsInRightArea[0].transform.position;
            }

            result /= unitsInRightArea.Count;
            left = false;
        }

        return result;

    }

    /*
    Unit GetRandom(Unit[] massive)
    {
        int count = massive.Length;
        int random = Random.Range(0, count-1);
        return massive[random];
    }
    */
}
