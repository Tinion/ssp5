﻿using UnityEngine;
using System.Collections;
using Dest.Math;


public class Scorcher : Enemy
{
   // private float lockTargetRadius;
    //public float lockTargetRadiusMin;
    //public float lockTargetRadiusMax;

    //private float rotateAngle;
    //private bool isImpactHero;
    //public float rotateAngleMin;
    //public float rotateAngleMax;
    public CounterTimerF lifeTime;
    public Object decalScorcher;
    public DecalSnap decalScorcherActive;
    public SkillStatus skillStatus;
    public float speed;

    //Box2 attackZone;
    private bool isAttack = false;
    public bool isFollowDecal = true;
    Vector3 target;
    Vector3 start;
    
    public override void PrepareObject()
    {
        
        base.PrepareObject();
        //isImpactHero = false;
        isAttack = false;
        isActive = true;
        isFollowDecal = true;
        //lockTargetRadius = Random.Range(lockTargetRadiusMin, lockTargetRadiusMax);
        //rotateAngle = Random.Range(rotateAngleMin, rotateAngleMax);

		target = (Main.instance.hero.transform.position + Main.instance.hero.vehicle.currentRigidbody.velocity / 2);
        float distanceToTarget = Vector3.Distance(transform.position, target);
        Vector3 directionToTarget = Vector3.Normalize((target) - transform.position);
    }

    public override void RevertObject()
    {
        base.RevertObject();
        isAttack = false;
        lifeTime.max = 0;
        isFollowDecal = true;
        if (decalScorcherActive != null)
            Destroy(decalScorcherActive);
    }

    /*
	// Use this for initialization
    public override void Moving()
    {
        target = (Main.instance.hero.transform.position + Main.instance.hero.currentRigidbody.velocity/2);
        start = transform.position - currentRigidbody.velocity / 2;
        float distanceToTarget = Vector3.Distance(start, target);
        Vector3 directionToTarget = Vector3.Normalize((target) - start);

        if (!isAttack)
        {
            StartAttack(directionToTarget);
            
        }

        base.Moving();
        
            
        
        
    }
     * 
     *
   */
    public void GetPhysic()
    {
        isFollowDecal = false;
        currentRigidbody.velocity = transform.forward * 30.0f;
    }
    void OnCollisionEnter(Collision collision)
    {
        CollisionWithUnit(collision);
    }
    public void CollisionWithUnit(Collision collision)
    {

        if (collision.gameObject.CompareTag("Unit"))
        {
            
            Debug.Log("FALSE");
            Unit unit = collision.gameObject.GetComponent<Unit>();
            bool withHero = (unit.unitType == UnitType.Hero) ? true : false;

            if (withHero)
            {
                GetPhysic();
            }
            /*
                //if (garbageEnable)
                //    DamageHull(collision.contacts[0].point);
                 //   GetDamage(collision.contacts[0].point, collision.relativeVelocity, collision.rigidbody.mass, withHero);

                //if (withHero && heroAbilityEffect && isCollisionActive)
                //{
                    SideQuad scorcherQuad = EnemyVehicle.GetSideQuad(collision.contacts[0].point, transform);

                    if (scorcherQuad == SideQuad.TopLeft || scorcherQuad == SideQuad.TopRight)
                    {
                        //isAttackedByHero = true;
                        //isCollisionActive = false;
                        //Main.instance.SelectTarget(this);
                        //heroAttackTimer.Restore();
                        
                        float magn = collision.relativeVelocity.magnitude;

                        Vector3 dir = Vector3.Normalize(collision.relativeVelocity);
                        Vector3 point = unit.transform.position + dir;
                        point.y -= 1f;

                        float dist = Vector3.Distance(unit.transform.position, point);
                        float diff = Mathf.Clamp(currentRigidbody.mass / unit.currentRigidbody.mass, 1.0f, 2.0f);
                        unit.currentRigidbody.AddExplosionForce((magn + 10) * diff * (1.0f + unit.currentRigidbody.mass / 1000) * unit.currentRigidbody.mass, point, 25.0f, magn * diff * 200.0f, ForceMode.Force);

                        //Debug.Log(magn);
                    }
                //}
                */

        }
        else
            if (collision.gameObject.CompareTag("Solid"))
            {
                    GetDamage(collision.contacts[0].point, collision.relativeVelocity, currentRigidbody.mass, false);
            }

    }


    
    void StartAttack(Vector3 dir)
    {
        //Debug.Log(spline.Length);
        //Main.instance.gameSpeed = 0;
        //Debug.Log("STARTATTACK");
        isAttack = true;

        Debug.DrawLine(target, target + Vector3.up * 5, Color.black, 10.0f);
        Debug.DrawLine(Main.instance.hero.transform.position, Main.instance.hero.transform.position + Vector3.up * 5, Color.black, 10.0f);
        Debug.DrawLine(transform.position, transform.position + Vector3.up * 5, Color.black, 10.0f);
        Debug.DrawLine(transform.position + Vector3.up * 5, target + Vector3.up * 5, Color.black, 10.0f);

        if (decalScorcherActive != null)
            Destroy(decalScorcherActive.gameObject);


        //attackZone = new Box2(transform.position.ToVector2XZ(), (Quaternion.AngleAxis(90, Vector3.up) * dir).ToVector2XZ(), dir.ToVector2XZ(), new Vector2(2.0f, 60.0f));
        skillStatus = SkillStatus.MeleeAttack;
        GameObject decalScorcherGameObject = GameObject.Instantiate(decalScorcher) as GameObject;
        decalScorcherGameObject.transform.rotation = Quaternion.identity;
        decalScorcherGameObject.transform.parent = transform;
        decalScorcherGameObject.transform.localPosition = Vector3.zero;
        
        decalScorcherActive = decalScorcherGameObject.GetComponent<DecalSnap>();
        decalScorcherActive.master = this;
		OneSpline splineTarget = Main.instance.mainCamera.GetSpline(target + Vector3.up * 100.0f);
        Vector3 point = splineTarget.GetPointOnCurve(target);
        float targetOffset = Vector2.Distance(target.ToVector2XZ(),point.ToVector2XZ());
        if (Main.instance.mainCamera.targetController.transform.InverseTransformPoint(target).x<0)
            targetOffset*=-1;

        decalScorcherActive.Snap(target, targetOffset, start, currentSplineOffset);
        decalScorcherGameObject.transform.parent = null;


    }

    void Update()
    {

		target = (Main.instance.hero.transform.position + Main.instance.hero.vehicle.currentRigidbody.velocity/2);
        start = transform.position - currentRigidbody.velocity / 2;
        float distanceToTarget = Vector3.Distance(start, target);
        Vector3 directionToTarget = Vector3.Normalize((target) - start);

        if (!isAttack)
            StartAttack(directionToTarget);

        if (decalScorcherActive == null && isFollowDecal)
        {
            isFollowDecal = false;
            currentRigidbody.velocity = transform.forward * 30.0f;
        }


        if (lifeTime.max>0)
        {
            if (isFollowDecal)
            {
                if (lifeTime.isEnd(Time.deltaTime) )
                    Kill();

                if (decalScorcherActive != null)
                {
                    PositionTangent posTan = decalScorcherActive.splineNew.EvalPositionTangent(1 - (lifeTime.current / lifeTime.max));
                    transform.position = decalScorcherActive.transform.TransformPoint(posTan.Position);
                    transform.rotation = Quaternion.LookRotation(decalScorcherActive.transform.TransformDirection(posTan.Tangent));
                }
            }
        }

		if (killTimer.isEnd(Time.deltaTime))
		{
			killTimer.Restore ();
			if (isTooFar(killDistance, 5.0f))    
				Kill();
		}
    }

    public override void Kill()
    {
        if (!isActive)
            return;

        RevertObject();

        Main.instance.activeUnitObjects.Remove(this);
        Main.instance.EnemyPoolSystem.DestroyObject(this, nominal);

        return;
    }
    
    /*
    public void ForwardAttack()
    {

     float forwardSpeed = Mathf.Abs(transform.InverseTransformDirection(currentRigidbody.velocity).z);
     RelativeWaypointPosition = transform.InverseTransformPoint(Waypoint);

     if (RelativeWaypointPosition.ToVector2XZ().magnitude < WayPointDistanceLook)
     {
          CreateNewWaypoint(WayPointDistance, false);
     }

     Debug.DrawLine(Waypoint + Vector3.up * 2.0f, transform.position + Vector3.up * 2.0f, Color.black, 0.2f);
     Debug.DrawLine(transform.position, transform.position + Vector3.up * 2.0f, Color.magenta);
     Debug.DrawLine(Waypoint, Waypoint + Vector3.up * 2.0f, Color.magenta);

     inputSteer = RelativeWaypointPosition.x / RelativeWaypointPosition.magnitude * steerCurrent;
                                       
     if (Mathf.Abs(inputSteer) < 0.5f)
         inputTorque = Mathf.Abs(RelativeWaypointPosition.z / RelativeWaypointPosition.magnitude - Mathf.Abs(inputSteer));
     else
         inputTorque = 0.0f;

       float motorTorque = 0;
       float brakeTorque = 0;
       if (forwardSpeed < 40.0f)
           motorTorque = currentAccel * inputTorque;
       else
           brakeTorque = brake;

       ApplyColliders(inputSteer * steerCurrent, motorTorque, brakeTorque);
      
    }
    */
  


    
}
