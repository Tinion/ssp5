﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Dest.Math;

#if UNITY_EDITOR
using UnityEditor;
#endif

[System.Flags]
public enum SideTriangle
{
    Top = 0,
    Left = 1,
    Right = 2,
    Back = 3,
}

[System.Flags]
public enum SideQuad
{
    TopLeft = 0,
    TopRight = 1,
    BottomLeft = 2,
    BottomRight = 3,
}

[System.Serializable]
public class GarbagePart
{
    public bool isUsed;
    public SideTriangle side;
    private int _partsCount;

    public Vector3 lockPosition;
    public Quaternion lockRotation;

    public Transform GarbagePanel;
    public Renderer GarbageRenderer;

    public Rigidbody[] GarbageParts;
    public Vector3[] partsPositions;
    public Quaternion[] partsRotations;

    public void Create()
    {
        _partsCount = GarbageParts.Length;
        lockPosition = GarbagePanel.localPosition;
        lockRotation = GarbagePanel.localRotation;

        partsPositions = new Vector3[_partsCount];
        partsRotations = new Quaternion[_partsCount];

        for (int i = 0; i < _partsCount; i++)
        {
            partsPositions[i] = GarbageParts[i].transform.localPosition;
            partsRotations[i] = GarbageParts[i].transform.localRotation;
        }
    }

    public void Prepare(Material material)
    {
        GarbageRenderer.sharedMaterial = material;
    }

    public void Revert(Unit unit)
    {
        isUsed = false;
        GarbagePanel.parent = unit.transform;
        GarbagePanel.localPosition = lockPosition;
        GarbagePanel.localRotation = lockRotation;

        for (int i = 0; i < _partsCount; i++)
        {
            GarbageParts[i].velocity = Vector3.zero;
            GarbageParts[i].angularVelocity = Vector3.zero;
            GarbageParts[i].transform.localPosition = partsPositions[i];
            GarbageParts[i].transform.localRotation = partsRotations[i];
        }

        GarbagePanel.gameObject.SetActive(false);

    }
}





[System.Serializable]
public enum SkillStatus
{
    Default,
    MeleeAttack,
    RangedAttack,
    ExplosiveAttack,
    Diversion,
    BattlePrepare,
    Restore,
}

[System.Serializable]
public enum FollowMoving
{
    Boost,
    Brake,
    SmoothBoost,
    SmoothBrake,
    DistBoost,
    DistBrake,
    Move,
    SpeedBoost,
    SpeedBrake,
    None,
}


public class EnemyVehicle : Vehicle
{
    public SkillStatus skillStatus;
    private FollowMoving followStatus;

    public bool boost;
    public bool deformEnable;
    public bool garbageEnable;
    public bool wheelsEnable;
    public bool heroAbilityEffect;
    public bool avoidance;
    public bool stableZ;
    public bool follow;
    public bool force;



    public bool isAttackedByHero = false;
    public bool isAttackedOnce = false;
    public bool isCollisionActive = true;
    public Transform followTarget;

    public float armor;
    public int expBounty;

    public float maxRpm;
    public float rpmOffset;

    public float brake;
    public float brakeOffset;

    private float wheelRadius;
    private float wheelMass;

    public float wideRayDistance;
    public float tightRayDistance;
    public float longRayDistance;
    public float sideRayDistance;
    public Object decalAI;

    public int bonusChance;
    protected bool isBonusHas;

    protected float startVelocityZ;


    public float maxSpeed;
    public float minSpeed;
    protected float speedOffset;


    //public float antiRollBarForce;

    public float WayPointDistance;
    public float WayPointDistanceLook;

    public Object wheel;
    

    //private bool _isCheckImpact = false;

    protected float inputSteer = 0.0f;
    protected float inputTorque = 1.0f;

    private Mesh _DeformationMesh;
    private float deformRadius = 0.3f; //0.25
    private float deformNoise = 0.03f; //0.02
    private float maxDeform = 0.14f;
    private Vector3[] oldVertices;

    protected float offsetRange = 0;

    


    public bool direction;
    private bool isUnitInSphere;

    private Crush currentCrush;

    private GameObject solidCastMisc = null;

    protected Vector3 RelativeWaypointPosition;

    protected float longRayBraking;

    public Vector3 _frontPos;

    private Vector3 BaseTransform;
    protected Vector3 Waypoint;

    public CounterTimerF heroAttackTimer;


    public float[] SideFactors;

    public Mesh _DeformMesh;
    public Object crush;

    bool isOiled = false;
    float oiledSide = 1.0f;

    public MeshFilter meshFilter;
    public GarbagePart[] garbagePart;
    public Material[] randomMaterial;

    public override void Initialize(bool activeObject)
    {
        base.Initialize(activeObject);

        CreateObject();

        if (activeObject)
            PrepareObject();
    }

    public override void CreateObject()
    {
        isActive = true;

        currentRigidbody.centerOfMass = centerOfMass.localPosition;
        currentRigidbody.maxAngularVelocity = 20.0f;
        

        SetupWheels();

        wheelMass = wheelsColliderBack[0].mass;
        wheelRadius = wheelsColliderBack[0].radius;

        if (deformEnable)
            DeformationMeshActivate();     

        if (garbageEnable)
            for (int i = 0; i < 4; i++)
                garbagePart[i].Create();

        if (crush != null && currentCrush == null)
        {
            GameObject newCrush = GameObject.Instantiate(crush) as GameObject;
            currentCrush = newCrush.GetComponent<Crush>();
            currentCrush.master = this;
            currentCrush.CreateObject();
        }

        ApplyWheelCollector();
    }

    public override void PrepareObject()
    {
        Hide(false);
        healthPoints.Restore();
        heroAttackTimer.Restore();
        isBonusHas = true;
        durabilityStatus = DurabilityStatus.Normal;
        skillStatus = SkillStatus.Default;

        if (randomMaterial.Length > 0)
        {
            int randomMaterialIndex = Random.Range(0, randomMaterial.Length);
            currentRenderer.sharedMaterial = randomMaterial[randomMaterialIndex];
            if (garbageEnable)
                for (int i = 0; i < 4; i++)
                    garbagePart[i].Prepare(randomMaterial[randomMaterialIndex]);
        }

        if (wheelsEnable)
        {
            for (int i = 0; i < wheels.Length; i++)
            {
                wheels[i].collider.mass = wheelMass;
                wheels[i].collider.radius = wheelRadius;
                wheels[i].wheelTransform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
            }
        }

        


        currentRpm = maxRpm + Random.Range(-rpmOffset, rpmOffset);
        currentBrake = brake + Random.Range(-brakeOffset, brakeOffset);
        longRayBraking = 0;

        if (currentSpline = Math3d.GetEnemySpline(transform.position))
        {
            currentStep = currentSpline.GetStep(transform.position);
			currentSplineDirection = currentSpline.GetTangent(currentStep);
            if (Vector3.Angle(transform.forward, currentSplineDirection) < 90.0f)
                direction = true;
        }
        else
        {
            Debug.LogError("Enemy: Prepare Object can't find Spline " + currentGameObject.GetInstanceID() + currentGameObject.name + transform.position);
            Kill();
            return;
        }

        Vector3 startPos = currentSpline.GlobalPos(currentStep);
        currentSplineOffset = Vector3.Distance(startPos, transform.position);
        if ((Vector3.Angle(Vector3.Cross(currentSplineDirection, Vector3.up), Vector3.Normalize(startPos - transform.position))) > 90.0f)
            currentSplineOffset *= -1;

        BaseTransform = transform.position;
        CreateNewWaypoint(10.0f, false);

        if (currentCrush != null)
        {
            currentCrush.RevertObject();
            currentCrush.transform.parent = transform;
            currentCrush.transform.localPosition = Vector3.zero;
            currentCrush.transform.localRotation = Quaternion.identity;
            currentCrush.currentGameObject.SetActive(false);
        }
        

       //currentRigidbody.WakeUp();
        currentRigidbody.centerOfMass = centerOfMass.localPosition;
        isUnitInSphere = false;
    }

    public override void RevertObject()
    {
        currentRigidbody.velocity = Vector3.zero;
        currentRigidbody.angularVelocity = Vector3.zero;
       //currentRigidbody.Sleep();
       isCollisionActive = true;
       isAttackedByHero = false;
       isAttackedOnce = false;
       Main.instance.DeselectTarget(this);
       isActive = true;
       isBonusHas = true;
       isOiled = false;
       isBurned = false;
       healthPoints.Restore();

        boost = true;

        if (garbageEnable)
            RevertGarbage();

        if (deformEnable)
            DeformationRevert();

        if (wheelsEnable)
        {
            wheelsColliderBack[0].mass = wheelMass;
            wheelsColliderBack[1].mass = wheelMass;
            wheelsColliderForward[0].mass = wheelMass;
            wheelsColliderForward[1].mass = wheelMass;
        }

        if (currentCrush != null)
        {
            currentCrush.RevertObject();
            currentCrush.transform.parent = transform;
            currentCrush.transform.localPosition = Vector3.zero;
            currentCrush.transform.localRotation = Quaternion.identity;
            currentCrush.currentGameObject.SetActive(false);
        }

        isUnitInSphere = false;
    }

    public int CheckGround()
    {
        int summ = 0;
        if (wheelsColliderBack[0].isGrounded)
            summ++;
        if (wheelsColliderBack[1].isGrounded)
            summ++;
        if (wheelsColliderForward[0].isGrounded)
            summ++;
        if (wheelsColliderForward[1].isGrounded)
            summ++;
        return summ;
    }

    void CreateGarbage(SideTriangle side)
    {

        for (int i = 0; i < garbagePart.Length; i++)
            if (garbagePart[i].side == side && !garbagePart[i].isUsed)
            {
                // Main.instance.DrawDestroyBodytIcon(this);
                //Debug.Log("CreateGarbage " + side.ToString());
                garbagePart[i].GarbagePanel.gameObject.SetActive(true);
                garbagePart[i].GarbagePanel.parent = null;
                for (int j = 0; j < garbagePart[i].GarbageParts.Length; j++)
                    garbagePart[i].GarbageParts[j].AddForce(Random.Range(0, 32.5f) - 19.0f, Random.RandomRange(3.8f, 19.0f), Random.RandomRange(0, 37.5f) - 19.0f, ForceMode.Impulse);
                garbagePart[i].isUsed = true;
            }
    }

    public void RevertGarbage()
    {
        for (int i = 0; i < garbagePart.Length; i++)
            garbagePart[i].Revert(this);
    }


    public virtual void CreateNewWaypoint(float wayLength, bool needNewSpline)
    {

       // if (spline==null)
       // {
       //     Debug.Log(currentGameObject.GetInstanceID());
       // }

        Vector3 curveposition = transform.position;
        float addPercent = 0.0f;
        float passed = 0.0f;
        float _thisStep = currentSpline.GetStep(transform.position);
        if (!currentSpline.UniformGlobalPosAdd(_thisStep, wayLength, 0, out curveposition, out addPercent, out passed, direction))
        {
            //Debug.Log ("GGGGG");
            BlockPlane old = currentSpline.transform.GetComponent<BlockPlane>();

            BlockPlane current = old.GetNextBlock(direction, currentSpline.type);


            /*
            if (old.type == BlockPlaneType.Fork && direction)
            {
                
                RoadElement currentForkData = Main.instance.generator.activeSeasons[old.runLine].roadElements[old.localForkNumber];

                if (spline.type == SplineType.ForkLeft)
                {
                    int local = currentForkData.ForkRun(true, old.localForkNumber);
                    Debug.Log("1 " + gameObject.GetInstanceID() + " R: " + currentForkData.leftWay + " L: " + local);
                    current = Main.instance.GetPlane(local, currentForkData.leftWay);
                }
                else
                {
                    int local = currentForkData.ForkRun(false, old.localForkNumber);
                    Debug.Log("1 " + gameObject.GetInstanceID() + " R: " + currentForkData.rightWay + " L: " + local);
                    current = Main.instance.GetPlane(currentForkData.ForkRun(false, old.localForkNumber), currentForkData.rightWay);
                }
            }
            else
                if (old.localForkNumber == 0 && old.runLine != 0 && !direction && !old.started)
                {
                    
                    int index = -1;
                    for (int i = 0; i < Main.instance.BlockPlanes.Count; i++)
                        if (Main.instance.BlockPlanes[i].type == BlockPlaneType.Fork || Main.instance.BlockPlanes[i].type == BlockPlaneType.ForkReverse)
                            index = i;
                    Debug.Log("2 ");
                    current = Main.instance.BlockPlanes[index];
                }
                else
                    if (old.localForkNumber == Main.instance.generator.activeSeasons[old.runLine].roadElements.Length - 1 && Main.instance.GetPlane(old.localForkNumber + (direction ? 1 : -1), old.runLine).type!=BlockPlaneType.EndBlock)
            {
                Debug.Log("3 " + gameObject.GetInstanceID() + " R: " + Main.instance.generator.activeSeasons[old.runLine].seasonConnect + " L: " + 0);
                RoadElement currentForkData = Main.instance.generator.activeSeasons[Main.instance.generator.activeSeasons[old.runLine].seasonConnect].roadElements[Main.instance.generator.activeSeasons[old.runLine].roadConnect];
                current = Main.instance.GetPlane(Main.instance.generator.activeSeasons[old.runLine].seasonConnect, 0);
            }
            else
            {
                int local = old.localForkNumber + (direction ? 1 : -1);
                Debug.Log("4 " + gameObject.GetInstanceID() + " R: " + old.runLine + " L: " + local);
                current = Main.instance.GetPlane(local, old.runLine);
            }
             */

            if (current != null)
            {
                if (current.type == BlockPlaneType.Fork || current.type == BlockPlaneType.Connection)
					currentSpline = Math3d.GetEnemySpline(current, transform.position + ((!direction) ? currentSplineDirection*-1 : currentSplineDirection) * wayLength);
                else
                    currentSpline = Math3d.GetEnemySpline(current, transform.position);

				if (current.type == BlockPlaneType.Outpost)
					Kill();
					return;
            }
            else
            {
				if (old.type != BlockPlaneType.Fork)
                Debug.LogError("Enemy: No block waypoint " + currentGameObject.GetInstanceID() + currentGameObject.name + transform.position);
                //Main.instance.gameSpeed = 0;
                Kill();
                return;
            }


            float temp;
            if (currentSpline != null)
            {
                if (!currentSpline.UniformGlobalPosAdd(direction ? 0 : 1.0f, wayLength - passed, 0, out curveposition, out addPercent, out temp, direction))
                {
                    Debug.LogError("Spline");
                }

            }
            else
            {
				Debug.LogWarning (transform.position + " " +  transform.position + " " +  (transform.position + currentSplineDirection * wayLength).ToString() + " " + current.blockName + " " + currentSplineDirection.ToString() + " " + wayLength);
                Debug.LogError("No spline waypoint " + curveposition);
                Kill();
                return;
            }


        }


        currentSplineDirection = currentSpline.GetTangent(addPercent);
        float angle = Math3d.GetAngle(currentSplineDirection.x, currentSplineDirection.z, Vector3.forward.x, Vector3.forward.z);

        BaseTransform = curveposition;
        Waypoint = Math3d.RotateThis(currentSplineOffset, -angle, curveposition);
        currentStep = addPercent;
    }

    protected float CalculateOffsetFromHero(float factor)
    {
        float size = 0;
        if (currentSpline != null)
            size = currentSpline.GetCurrentWidth(Main.instance.hero.currentStep);
        float heroOffset = Main.instance.hero.currentSplineOffset;

        offsetRange = Mathf.Sqrt(size * 2) * factor;

        float source = Main.instance.hero.currentStep;


        if (heroOffset + offsetRange > size)
            return Mathf.Clamp(heroOffset - offsetRange, -size, size);

        if (heroOffset - offsetRange < -size)
            return Mathf.Clamp(heroOffset + offsetRange, -size, size);

        if (heroOffset > currentSplineOffset)
            return Mathf.Clamp(heroOffset - offsetRange, -size, size);
        else
            return Mathf.Clamp(heroOffset + offsetRange, -size, size);
    }

    protected float CalculateOffsetFromOffset(float factor)
    {
        float size = 0;
        if (currentSpline != null)
            size = currentSpline.GetCurrentWidth(currentStep);
        float heroOffset = Main.instance.hero.currentSplineOffset;

        offsetRange = Mathf.Sqrt(size * 2) * factor;

        float source = Main.instance.hero.currentStep;


        if (currentSplineOffset + offsetRange > size)
            return currentSplineOffset - offsetRange;
        else
            if (currentSplineOffset - offsetRange < -size)
                return currentSplineOffset + offsetRange;
            else
                return currentSplineOffset;
    }
    public void Hide(bool isHide)
    {
        isActive = !isHide;
        if (isHide)
        {
            currentRigidbody.Sleep();
            transform.position = Vector3.zero;

        }
        else
        {
            currentRigidbody.WakeUp();

        }
    }
    public override void Kill()
    {
		//Debug.Log("KILL " + nominal + transform.GetInstanceID());
        if (isAttackedOnce)
        {
            //Debug.Log("Kill by hero ");
            if (durabilityStatus == DurabilityStatus.Damaged)
            {
               // Debug.Log(" damaged");
                Main.instance.bonusElements.SetDamaged(1/*expBounty*/);
            }

            if (durabilityStatus == DurabilityStatus.Ruined)
            {
                //Debug.Log(" ruined");
                Main.instance.bonusElements.SetRuined(1/*expBounty*/);
            }
        }

        //if (!isActive)
        //    return;

		//if (Main.instance.activeUnitObjects.Contains (this))
		//{
		//	if (Main.instance.generator.enemyCounter.Find (type).current > 0) {
		//		Debug.Log ("Vehicle " + transform.GetInstanceID () + " " + Main.instance.generator.enemyCounter.Find (type).current + " > " + (Main.instance.generator.enemyCounter.Find (type).current - 1).ToString ());
		//		Main.instance.generator.enemyCounter.Find (type).current--;
		//	} else {
		//		Debug.Log ("Vehicle2 " + transform.GetInstanceID () + " " + Main.instance.generator.enemyCounter.Find (type).current + " > 0");
		//		Main.instance.generator.enemyCounter.Find (type).current = 0;
		////	}
		//}

        RevertObject();

        Main.instance.activeUnitObjects.Remove(this);
		Main.instance.EnemyPoolSystem.DestroyObject(this, nominal);

        return;
    }



    public virtual void Boost()
    {
        currentRigidbody.velocity = transform.TransformDirection(Vector3.forward * Math3d.GetSpeed(currentRpm, wheelsColliderBack[0].radius));
        boost = false;

    }


    public virtual void Moving()
    {

        if (boost) Boost();
        SwitchSkillStatus();

        if (isAttackedByHero)
        {
            if (heroAttackTimer.isEnd(0.03f))
            {
                isCollisionActive = true;
                isAttackedByHero = false;
                Main.instance.DeselectTarget(this);
            }
        }

        //UpdateWheels();
        //DropText(skillStatus.ToString() + " " + durabilityStatus.ToString());
    }

    public virtual void SwitchSkillStatus()
    {
        MovementFunctions(Waypoint, avoidance, true, true);
    }

    public virtual float SphereAvoidance()
    {
        float dist1 = 0;
        float dist2 = 0;
        float steer = 0;
        isUnitInSphere = false;

        for (int i=0; i<Main.instance.activeUnitObjects.Count; i++)
        {
            if (currentGameObject!=Main.instance.activeUnitObjects[i].currentGameObject)
            {
                dist1 = Vector2.Distance(Main.instance.activeUnitObjects[i].transform.position.ToVector2XZ(), transform.position.ToVector2XZ());
                dist2 = sphereRadius + Main.instance.activeUnitObjects[i].sphereRadius;
                //
                if (dist1 <= dist2)
                {
                    //DropText(dist1.ToString("F2") + " " + dist2.ToString("F2") + " " + Main.instance.activeUnitObjects[i].currentGameObject.name);
                    isUnitInSphere = true;
                    Vector3 dir = Vector3.Normalize(transform.position - Main.instance.activeUnitObjects[i].transform.position);
                    float dot = Vector2.Dot(dir.ToVector2XZ(), transform.forward.ToVector2XZ());
                    float dotR = Vector2.Dot(dir.ToVector2XZ(), transform.right.ToVector2XZ());

                    if (Mathf.Abs(dot) < 0.5f)
                    //    currentRigidbody.AddForce(transform.forward * 20.0f * Mathf.Sign(dot), ForceMode.Acceleration);
                    //else
                        steer += (1-Mathf.Abs(dot)) * Mathf.Sign(dotR) * 0.5f;

                }
                
                    
            }
        }

        dist1 = Vector2.Distance(Main.instance.hero.transform.position.ToVector2XZ(), transform.position.ToVector2XZ());
		dist2 = sphereRadius + Main.instance.hero.vehicle.sphereRadius;
        //
        if (dist1 <= dist2)
        {
            //DropText(dist1.ToString("F2") + " " + dist2.ToString("F2") + " Hero");
            isUnitInSphere = true;
            Vector3 dir = Vector3.Normalize(transform.position - Main.instance.hero.transform.position);

            float dot = Vector2.Dot(dir.ToVector2XZ(), transform.forward.ToVector2XZ());
            float dotR = Vector2.Dot(dir.ToVector2XZ(), transform.right.ToVector2XZ());

            if (Mathf.Abs(dot) < 0.5f)
            //    currentRigidbody.AddForce(transform.forward * 20.0f * Mathf.Sign(dot), ForceMode.Acceleration);
            //else
                steer += (1 - Mathf.Abs(dot)) * Mathf.Sign(dotR) * 0.5f;
        }

        return steer;
    }
    public virtual float Avoidance(bool isLong, float multi)
    {

        Vector3 frontDir = transform.forward;
        Vector3 rayStartPos = transform.TransformPoint(_frontPos);
        RaycastHit hit;
        int layerMask = (1 << 9) | (1 << 0) | (1 << 12);

        int[] type = new int[6];

        if (!stableZ)
            if (Math3d.RayCastGroundDown(rayStartPos + transform.forward * 4.0f + Vector3.up * 3.0f, out hit))
                frontDir = Vector3.Normalize((hit.point + Vector3.up * 0.2f) - rayStartPos);

        bool wideTurnLeft = false;
        bool wideTurnRight = false;
        bool tightTurnLeft = false;
        bool tightTurnRight = false;
        bool sideTurnRight = false;
        bool sideTurnLeft = false;

        bool wideBoardLeft = false;
        bool wideBoardRight = false;
        bool tightBoardLeft = false;
        bool tightBoardRight = false;
        bool sideBoardLeft = false;
        bool sideBoardRight = false;
        bool longBoard = false;



        float newinputSteer1 = 0.0f;
        float newinputSteer2 = 0.0f;
        float newinputSteer3 = 0.0f;
        float newinputSteer4 = 0.0f;
        float newinputSteer5 = 0.0f;
        float newinputSteer6 = 0.0f;


        Debug.DrawRay(rayStartPos, Quaternion.AngleAxis(25.0f, transform.up) * frontDir * wideRayDistance, Color.white);
        Debug.DrawRay(rayStartPos, Quaternion.AngleAxis(-25.0f, transform.up) * frontDir * wideRayDistance, Color.white);

        Debug.DrawRay(rayStartPos, Quaternion.AngleAxis(7.0f, transform.up) * frontDir * tightRayDistance, Color.white);
        Debug.DrawRay(rayStartPos, Quaternion.AngleAxis(-7.0f, transform.up) * frontDir * tightRayDistance, Color.white);

        Debug.DrawRay(rayStartPos, Quaternion.AngleAxis(0.0f, transform.up) * frontDir * longRayDistance, Color.white);

        Debug.DrawRay(rayStartPos, Quaternion.AngleAxis(90.0f, transform.up) * frontDir * sideRayDistance, Color.white);
        Debug.DrawRay(rayStartPos, Quaternion.AngleAxis(-90.0f, transform.up) * frontDir * sideRayDistance, Color.white);

        // Wide Raycasts.
        if (Physics.Raycast(rayStartPos, Quaternion.AngleAxis(25.0f, transform.up) * frontDir, out hit, wideRayDistance, layerMask))
        {
            Debug.DrawRay(rayStartPos, Quaternion.AngleAxis(25.0f, transform.up) * frontDir * wideRayDistance, Color.red);
            newinputSteer1 = Mathf.Lerp(-0.5f, 0.0f, (hit.distance / wideRayDistance));

            GameObject current = hit.transform.gameObject;
            if (current.CompareTag("Unit"))
            {
                if (Vector3.Dot(transform.forward, hit.transform.forward) > 0)
                {
                    float currentDistance = wideRayDistance / 2;
                    Debug.DrawRay(rayStartPos, Quaternion.AngleAxis(25.0f, transform.up) * frontDir * wideRayDistance/2, Color.blue);
                    newinputSteer1 = (hit.distance>wideRayDistance/2) ? 0 : Mathf.Lerp(-0.5f, 0.0f, (hit.distance / wideRayDistance/2));
                }
                
            }
            if (current.layer == LayerMask.NameToLayer("Board") || current.CompareTag("Wall"))
            {
                solidCastMisc = current;
                wideBoardLeft = true;
            }
            wideTurnLeft = true;
        }

        if (Physics.Raycast(rayStartPos, Quaternion.AngleAxis(-25.0f, transform.up) * frontDir, out hit, wideRayDistance, layerMask))
        {   
            Debug.DrawRay(rayStartPos, Quaternion.AngleAxis(-25.0f, transform.up) * frontDir * wideRayDistance, Color.red);
            newinputSteer2 = Mathf.Lerp(0.5f, 0.0f, (hit.distance / wideRayDistance));

            GameObject current = hit.transform.gameObject;

            if (current.CompareTag("Unit"))
            {
                if (Vector3.Dot(transform.forward, hit.transform.forward) > 0)
                {
                    float currentDistance = wideRayDistance / 2;
                    Debug.DrawRay(rayStartPos, Quaternion.AngleAxis(-25.0f, transform.up) * frontDir * currentDistance, Color.blue);
                    newinputSteer2 = (hit.distance > currentDistance) ? 0 : Mathf.Lerp(-0.5f, 0.0f, (hit.distance / currentDistance));
                }

            }
            if (current.layer == LayerMask.NameToLayer("Board") || current.CompareTag("Wall"))
            {
                solidCastMisc = current;
                wideBoardRight = true;

            }

            wideTurnRight = true;
        }

        // Tight Raycasts.
        if (Physics.Raycast(rayStartPos, Quaternion.AngleAxis(7.0f, transform.up) * frontDir, out hit, tightRayDistance, layerMask, QueryTriggerInteraction.UseGlobal))
        {
            Debug.DrawRay(rayStartPos, Quaternion.AngleAxis(7.0f, transform.up) * frontDir * tightRayDistance, Color.red);
            newinputSteer3 = Mathf.Lerp(-1.0f, 0.0f, (hit.distance / tightRayDistance));

            GameObject current = hit.transform.gameObject;
            if (current.CompareTag("Unit"))
            {
               if (Vector3.Dot(transform.forward, hit.transform.forward) > 0)
                {
                    float currentDistance = tightRayDistance / 2;
                    Debug.DrawRay(rayStartPos, Quaternion.AngleAxis(7.0f, transform.up) * frontDir * currentDistance, Color.blue);
                    newinputSteer3 = (hit.distance > currentDistance) ? 0 : Mathf.Lerp(-0.5f, 0.0f, (hit.distance / currentDistance));
                }
            }
            if (current.layer == LayerMask.NameToLayer("Board") || current.CompareTag("Wall"))
            {
                solidCastMisc = current;
                tightBoardLeft = true;
            }


            tightTurnLeft = true;
        }

        if (Physics.Raycast(rayStartPos, Quaternion.AngleAxis(-7.0f, transform.up) * frontDir, out hit, tightRayDistance, layerMask))
        {
            Debug.DrawRay(rayStartPos, Quaternion.AngleAxis(-7.0f, transform.up) * frontDir * tightRayDistance, Color.red);
            newinputSteer4 = Mathf.Lerp(1.0f, 0.0f, (hit.distance / tightRayDistance));

            GameObject current = hit.transform.gameObject;
            if (current.CompareTag("Unit"))
            {
                if (Vector3.Dot(transform.forward, hit.transform.forward) > 0)
                {
                    float currentDistance = tightRayDistance / 2;
                    Debug.DrawRay(rayStartPos, Quaternion.AngleAxis(-7.0f, transform.up) * frontDir * currentDistance, Color.blue);
                    newinputSteer4 = (hit.distance > currentDistance) ? 0 : Mathf.Lerp(-0.5f, 0.0f, (hit.distance / currentDistance));
                }
            }
            if (current.layer == LayerMask.NameToLayer("Board") || current.CompareTag("Wall"))
            {
                solidCastMisc = current;
                tightBoardRight = true;
            }

            tightTurnRight = true;

        }

        // Side Raycasts.

        if (Physics.Raycast(rayStartPos, Quaternion.AngleAxis(90.0f, transform.up) * frontDir, out hit, sideRayDistance, layerMask))
        {
            Debug.DrawRay(rayStartPos, Quaternion.AngleAxis(90.0f, transform.up) * frontDir * sideRayDistance, Color.red);
            newinputSteer5 = Mathf.Lerp(-0.5f, 0.0f, (hit.distance / sideRayDistance));

            GameObject current = hit.transform.gameObject;
            if (current.CompareTag("Unit"))
            {
                if (Vector3.Dot(transform.forward, hit.transform.forward) > 0)
                {
                    float currentDistance = sideRayDistance / 2;
                    Debug.DrawRay(rayStartPos, Quaternion.AngleAxis(90.0f, transform.up) * frontDir * currentDistance, Color.blue);
                    newinputSteer5 = (hit.distance > currentDistance) ? 0 : Mathf.Lerp(-0.5f, 0.0f, (hit.distance / currentDistance));
                }

            }
            if (current.layer == LayerMask.NameToLayer("Board") || current.CompareTag("Wall"))
            {
                solidCastMisc = current;
                sideBoardLeft = true;
            }
        }

        if (Physics.Raycast(rayStartPos, Quaternion.AngleAxis(-90.0f, transform.up) * frontDir, out hit, sideRayDistance, layerMask))
        {
            Debug.DrawRay(rayStartPos, Quaternion.AngleAxis(-90.0f, transform.up) * frontDir * sideRayDistance, Color.red);
            newinputSteer6 = Mathf.Lerp(0.5f, 0.0f, (hit.distance / sideRayDistance));

            GameObject current = hit.transform.gameObject;

            if (current.CompareTag("Unit"))
            {
                if (Vector3.Dot(transform.forward, hit.transform.forward) > 0)
                {
                    float currentDistance = sideRayDistance / 2;
                    Debug.DrawRay(rayStartPos, Quaternion.AngleAxis(-90.0f, transform.up) * frontDir * currentDistance, Color.blue);
                    newinputSteer6 = (hit.distance > currentDistance) ? 0 : Mathf.Lerp(-0.5f, 0.0f, (hit.distance / currentDistance));
                }
 
            }
            if (current.layer == LayerMask.NameToLayer("Board") || current.CompareTag("Wall"))
            {
                solidCastMisc = current;
                sideBoardRight = true;
            }
        }

        longRayBraking = 0;
        if (isLong)
        {
            if (Physics.Raycast(rayStartPos, Quaternion.AngleAxis(0.0f, transform.up) * frontDir, out hit, longRayDistance, layerMask))
            {
                Debug.DrawRay(rayStartPos, Quaternion.AngleAxis(0.0f, transform.up) * frontDir * longRayDistance, Color.red);
                if (currentRigidbody.velocity.magnitude > 1.0f)
                    longRayBraking = hit.distance / longRayDistance;

                GameObject current = hit.transform.gameObject;

                if (current.CompareTag("Unit"))
                {
                    if (Vector3.Dot(transform.forward, hit.transform.forward) > 0)
                    {
                        float currentDistance = longRayDistance / 2;
                        Debug.DrawRay(rayStartPos, Quaternion.AngleAxis(0.0f, transform.up) * frontDir * currentDistance, Color.blue);
                        longRayBraking = (hit.distance > currentDistance) ? 0 : Mathf.Lerp(-0.5f, 0.0f, (hit.distance / currentDistance));
                    }

                }
                if (current.layer == LayerMask.NameToLayer("Board") || current.CompareTag("Wall"))
                {
                    solidCastMisc = current;
                    longBoard = true;
                }


            }
        }



        if (wideBoardLeft || wideBoardRight || tightBoardLeft || tightBoardRight || sideBoardLeft || sideBoardRight || longBoard)
        {
            if (solidCastMisc.layer == LayerMask.NameToLayer("Board"))
            {
                Board currentBoard = solidCastMisc.transform.parent.GetComponent<Board>();

                Vector3 RelativeBoardPosition = transform.InverseTransformPoint(currentBoard.transform.position);
                if (Mathf.Abs(RelativeBoardPosition.x) < 1.2f)
                {
                    if (wideBoardLeft)
                        newinputSteer1 = 0;
                    if (wideBoardRight)
                        newinputSteer2 = 0;
                    if (tightBoardLeft)
                        newinputSteer3 = 0;
                    if (tightBoardRight)
                        newinputSteer4 = 0;
                    if (sideBoardLeft)
                        newinputSteer5 = 0;
                    if (sideBoardRight)
                        newinputSteer6 = 0;
                    if (longBoard)
                        longRayBraking = 0;
                }
            }
            else
                if (solidCastMisc.CompareTag("Wall"))
                {
					/*
                    Wall currentWall = solidCastMisc.transform.parent.GetComponent<Wall>();
                    float angle = Mathf.Abs(Math3d.GetAngle(currentWall.transform.forward.z, currentWall.transform.forward.x, transform.forward.z, transform.forward.x));

                    if (wideBoardLeft)
                        newinputSteer1 = Mathf.Abs(newinputSteer1);
                    if (wideBoardRight)
                        newinputSteer2 = Mathf.Abs(newinputSteer2);
                    if (tightBoardLeft)
                        newinputSteer3 = Mathf.Abs(newinputSteer3);
                    if (tightBoardRight)
                        newinputSteer4 = Mathf.Abs(newinputSteer4);
                    if (sideBoardLeft)
                        newinputSteer5 = Mathf.Abs(newinputSteer5);
                    if (sideBoardRight)
                        newinputSteer6 = Mathf.Abs(newinputSteer6);
                    if (longBoard)
                        longRayBraking = 0;


                    if ((angle < 90.0f && offset > 0) || (angle >= 90.0f && offset <= 0))
                    {
                        if (wideBoardLeft)
                            newinputSteer1 *= -1;
                        if (wideBoardRight)
                            newinputSteer2 *= -1;
                        if (tightBoardLeft)
                            newinputSteer3 *= -1;
                        if (tightBoardRight)
                            newinputSteer4 *= -1;
                        if (sideBoardLeft)
                            newinputSteer5 *= -1;
                        if (sideBoardRight)
                            newinputSteer6 *= -1;

                    }
                    */

                }


        }

        return (newinputSteer1 + newinputSteer2 + newinputSteer3 + newinputSteer4 + newinputSteer5 + newinputSteer6) * multi;
    }

    public void CreateTarget(Vector3 localPos)
    {
        DeleteTarget();
        
        followTarget = ((GameObject)GameObject.Instantiate(decalAI)).transform;
        followTarget.name = "Target";
        followTarget.parent = Main.instance.mainCamera.targetController.transform;
        followTarget.localPosition = localPos;
    }

    public void DeleteTarget()
    {
        if (followTarget != null)
            Destroy(followTarget.gameObject);
    }
    public virtual void MovementFunctions(Vector3 targetPosition, bool _avoidance, bool _continuance, bool _averageSpeed)
    {

        forwardSpeed = Mathf.Abs(transform.InverseTransformDirection(currentRigidbody.velocity).z);

        float newInputSteer = 0.0f;
        RelativeWaypointPosition = transform.InverseTransformPoint(targetPosition);
        float yAngle = Vector3.Angle(targetPosition - transform.position, transform.forward);
        
        if (force)
        {
            inputSteer = FollowSteerForce(0.5f);
        }
        else
        {
            if (_avoidance)
            {

                newInputSteer = Avoidance(true, 1.5f) +SphereAvoidance();
            }
            if (follow)
                FollowSteer();

            inputSteer = RelativeWaypointPosition.x / RelativeWaypointPosition.magnitude;
            inputSteer += newInputSteer;
        }

        if (isOiled)
        {
            inputSteer = oiledSide;
        }

        

        

        

        Debug.DrawLine(transform.position, Waypoint, Color.magenta);
        Debug.DrawLine(BaseTransform, Waypoint, Color.blue);
       Debug.DrawLine(transform.position, transform.TransformPoint(RelativeWaypointPosition), Color.black);
       Debug.DrawLine(transform.TransformPoint(RelativeWaypointPosition), transform.TransformPoint(RelativeWaypointPosition) + 1.5f * Vector3.up, Color.black);

        

        if (Mathf.Abs(inputSteer) < 0.5f)
            inputTorque = Mathf.Abs(RelativeWaypointPosition.z / RelativeWaypointPosition.magnitude - (!isOiled ? Mathf.Abs(inputSteer) : 0));
        else
            inputTorque = 0.0f;



        if (RelativeWaypointPosition.magnitude < WayPointDistanceLook)
        {
            if (_continuance)
                CreateNewWaypoint(WayPointDistance, false);
            else
            {
                durabilityStatus = DurabilityStatus.Ruined;
                return;
            }
        }

        float motorTorque;
        float brakeTorque;

        if (follow)
        {
            Vector2 factor = FollowAuto();
            motorTorque = currentAccel * factor.x;
                if (!force)
                motorTorque *=  inputTorque;
            brakeTorque = brake * factor.y;
        }
        else
        {
            if (_averageSpeed)
                averageRpm = currentRpm * (1 - Mathf.Abs(inputSteer)*0.9f) * (1 - longRayBraking);
            AverageAuto(50.0f, out motorTorque, out brakeTorque);
        }


        ApplyColliders(inputSteer * steerCurrent, motorTorque, brakeTorque);



    }

  
    public void FollowSteer()
    {
        float targetOffset = followTarget.localPosition.x;
        currentSplineOffset = targetOffset;
    }

    public float FollowSteerForce(float deathZone)
    {
        float relativeOffset = followTarget.localPosition.x - currentSplineOffset;
        if (relativeOffset > deathZone)
            return 1.0f;

        if (relativeOffset < -deathZone)
                return -1.0f;

       return 0.0f;

    }
    public Vector2 FollowAuto()
    {
        float controlDist = 12.0f;
        float controlSpeedMax = 10.0f;
        float controlSpeedCam = 5.0f;
        float controlSpeedInside = 1.0f;
        float controlDistMin = 0.5f;
        float controlSpeed = 0.01f;

        Vector2 factor = Vector2.zero;

        Vector3 relativeTarget = transform.InverseTransformPoint(followTarget.position);
		float speedDiff = forwardSpeed - Main.instance.hero.vehicle.forwardSpeed;

        if (Mathf.Abs(speedDiff) > controlSpeedMax)
        {
            if (speedDiff < 0)
            {
                followStatus = FollowMoving.SpeedBoost;
                factor.x = 4.0f;
            }
            else
            {
                followStatus = FollowMoving.SpeedBrake;
                factor.y = 4.0f;
            }
            return factor;
        }

        if (Mathf.Abs(relativeTarget.z) > controlDist)
        {
            if (relativeTarget.z > controlDist)
            {
                followStatus = FollowMoving.Boost;
                factor.x = 4;
            }
            else
            {
                followStatus = FollowMoving.Brake;
                factor.y = 4;
            }
            return factor;
        }

        if (Mathf.Abs(speedDiff) > controlSpeedCam)
        {
            if (speedDiff < 0)
            {
                followStatus = FollowMoving.SpeedBoost;
                factor.x = 4;
            }
            else
            {
                followStatus = FollowMoving.SpeedBrake;
                factor.y = 4.0f;
            }

            return factor;
        }

        if (Mathf.Abs(relativeTarget.z) > controlDistMin)
        {
            if (relativeTarget.z > 0)
            {
                followStatus = FollowMoving.SmoothBoost;
                factor.x = Mathf.Clamp(Mathf.Abs(relativeTarget.z / controlDist) * 2, 0.5f, 1.0f) * 4;
            }
            else
            {

                followStatus = FollowMoving.SmoothBrake;
                factor.y = Mathf.Clamp(Mathf.Abs(relativeTarget.z / controlDist) * 2, 0.5f, 1.0f) * 4;
            }
            return factor;
        }

        if (Mathf.Abs(speedDiff) > controlSpeedInside)
        {
            followStatus = FollowMoving.Move;
            currentSplineOffset = followTarget.localPosition.x;
            if (speedDiff < 0)
            {
                followStatus = FollowMoving.DistBoost;
                factor.x = Mathf.Abs(speedDiff) * 2;
            }
            else
            {
                followStatus = FollowMoving.DistBrake;
                factor.y = Mathf.Abs(speedDiff) * 2;
            }
            return factor;
        }

        return factor;
    }

    
    public virtual void AverageAuto(float clamp, out float motorTorque, out float brakeTorque)
    {
        motorTorque = 0;
        brakeTorque = 0;

        int stat;

        float diff = averageRpm - wheelsColliderBack[0].rpm;
        float absDiff = Mathf.Abs(diff);
        float factor = absDiff / clamp;

        if (absDiff < clamp)
        {
            stat = 0;
            motorTorque = currentAccel * inputTorque * factor;


        }
        else
        {
            if (diff < 0)
            {
                stat = 1;
                motorTorque = 0;
                brakeTorque = currentBrake;
            }
            else
            {
                stat = 2;
                motorTorque = currentAccel * inputTorque;
                //
            }
        }


    }

    //if (distDiff <= controlSpeed && distDiff >= -controlSpeed)
    //{
    /*
        if (distDiff > controlSpeed)
        {

            followStatus = FollowMoving.DistBrake;
            motorTorque = 0;
            brakeTorque = brake * 2;//(brake * Mathf.Clamp(distDiff / controlSpeed, 1.0f, 2.0f) * 2);// *Mathf.Abs(relTarPos.z / controlDist);
        }
        else
        if (distDiff < -controlSpeed)
        {

            followStatus = FollowMoving.DistBoost;
            motorTorque = maxAccel * 2;//(maxAccel * Mathf.Clamp(distDiff / controlSpeed, 1.0f, 2.0f) * 2); ;// *Mathf.Abs(relTarPos.z / controlDist);// *Mathf.Abs(relTarPos.z / controlDist);
            brakeTorque = 0;
        }
    //}
    else
    {
        followStatus = FollowMoving.Move;
        motorTorque = 0;
        brakeTorque = 0;
    }
     */

    //

    /*
        
    if (relativeTarget.z >= -controlDist && relativeTarget.z <= controlDist)
    {
            
        //if (Mathf.Abs(distDiff) > 0.2f)
        //{
        /*
        if (distDiff > controlSpeed)
            {
                    
                followStatus = FollowMoving.DistBrake;
                motorTorque = 0;
                brakeTorque = (Mathf.Abs(relativeTarget.z) > 7.0f) ? brake * 4 : (brake * Mathf.Clamp(distDiff / controlSpeed, 1.0f, 2.0f) * 2);// *Mathf.Abs(relTarPos.z / controlDist);
            }
        if (distDiff < -controlSpeed)
            {
                    
                followStatus = FollowMoving.DistBoost;
                motorTorque = (Mathf.Abs(relativeTarget.z) > 7.0f) ? maxAccel * 4 : (maxAccel * Mathf.Clamp(distDiff / controlSpeed, 1.0f, 2.0f) * 2); ;// *Mathf.Abs(relTarPos.z / controlDist);// *Mathf.Abs(relTarPos.z / controlDist);
                brakeTorque = 0;
            }
             
           // if (distDiff <= controlSpeed && distDiff >= -controlSpeed)
            {

                if (relativeTarget.z > 0.3f)
                {
                    followStatus = FollowMoving.SmoothBoost;
                    motorTorque = maxAccel * Mathf.Abs(relativeTarget.z / controlDist) * 4;
                    brakeTorque = 0;
                        
                }
                else
                    if (relativeTarget.z < -0.3f)
                {
                    followStatus = FollowMoving.SmoothBrake;
                    motorTorque = 0;
                    brakeTorque = brake * Mathf.Abs(relativeTarget.z / controlDist) * 4;
                }
                else
                {
                    followStatus = FollowMoving.Move;
                    motorTorque = 0;
                    brakeTorque = 0;
                }

                return;
            }
        //}
                
        else
        {
            if (relTarPos.z < 0)
            {
                followStatus = FollowMoving.SmoothBrake;
                motorTorque = 0;
                brakeTorque = brake * Mathf.Abs(relTarPos.z / controlDist);
            }
            else
            {
                followStatus = FollowMoving.SmoothBoost;
                motorTorque = maxAccel * Mathf.Abs(relTarPos.z / controlDist);
                brakeTorque = 0;
            }
        }
             */

    // }



    /*
    float motorTorqueAdd = 0;
    float brakeTorqueAdd = 0;


    if (distDiff < 0.1f)
        motorTorqueAdd += maxAccel * Mathf.Clamp01(Mathf.Abs(distDiff));
    if (distDiff > -0.1f)
        brakeTorqueAdd += brake * Mathf.Clamp01(Mathf.Abs(distDiff));

        

    motorTorque += motorTorqueAdd;
    brakeTorque += brakeTorqueAdd;
    */
    //DropText(relTarPos.z.ToString("F2") + " " + distDiff.ToString("F2") + " " + motorTorque.ToString("F2") + " " + brakeTorque.ToString("F2"));
    //DropText(followStatus.ToString() + " " + relativeTarget.z.ToString("F2") + " " + distDiff.ToString("F2") + " " + motorTorque.ToString("F2") + " " + brakeTorque.ToString("F2"));
    /*
    float diff = 23.0f - forwardSpeed;
    float absDiff = Mathf.Abs(diff);
    float factor = absDiff / 4.0f;

    if (absDiff < 1.5f)
    {
        if (diff >= 0)
            motorTorque = 1300.0f * factor;
        else
            brakeTorque = brake * factor;

    }
    else
    {
        if (diff < 0)
        {
            motorTorque = 0;
            brakeTorque = brake;
        }
        else
        {
            motorTorque = 1300.0f;
            brakeTorque = 0;
        }
    }
        
        
    motorTorque = 0;
    brakeTorque = 0;
    int stat;

    float diff = averageRpm - wheelsColliderBack[0].rpm;
    float absDiff = Mathf.Abs(diff);
    float factor = absDiff / clamp;

    if (absDiff < clamp)
    {
        stat = 0;
        motorTorque = currentAccel * inputTorque * factor;


    }
    else
    {
        if (diff < 0)
        {
            stat = 1;
            motorTorque = 0;
            brakeTorque = currentBrake;
        }
        else
        {
            stat = 2;
            motorTorque = currentAccel * inputTorque;
            //
        }
    }

    float steer = Mathf.Abs(inputSteer * 0.5f);
    float longR = longRayBraking;
        
    //if (type == EnemyType.Roadhog)
    //Debug.Log(diff + " " + averageSpeed + " " + wheelsColliderBack[0].rpm + " " + motorTorque + " " + brakeTorque);
    */


    public override void DebugGui()
    {
        base.DebugGui();
        /*
        GUI.BeginGroup(new Rect(25, 125, 240, 280));
        GUI.Label(new Rect(0, 0, 180, 30), "forwardSpeed " + forwardSpeed.ToString("F5"));
        GUI.Label(new Rect(0, 20, 180, 30), "RelativeZ " + relativeTarget.z.ToString("F5"));
        GUI.Label(new Rect(0, 40, 180, 30), "DistDiff " + distDiff.ToString("F5"));
        GUI.Label(new Rect(0, 60, 180, 30), "motorTorque " + motorTorque.ToString("F5"));
        GUI.Label(new Rect(0, 80, 180, 30), "brakeTorque " + brakeTorque.ToString("F5"));
        GUI.Label(new Rect(0, 100, 180, 30), "followStatus " + followStatus.ToString());
        GUI.Label(new Rect(0, 120, 180, 30), "SpeedDiff " + speedDiff.ToString());
        GUI.Label(new Rect(0, 140, 180, 30), "heroSpeed " + Main.instance.hero.forwardSpeed.ToString("F5"));
        GUI.Label(new Rect(0, 160, 180, 30), "RPM0 " + wheelsColliderBack[0].rpm.ToString("F5"));
        GUI.Label(new Rect(0, 180, 180, 30), "RPM1 " + wheelsColliderBack[1].rpm.ToString("F5"));
        GUI.Label(new Rect(0, 200, 180, 30), "RPM Hero " + Main.instance.hero.wheelsColliderBack[0].rpm.ToString("F5"));
        GUI.Label(new Rect(0, 220, 180, 30), "Radius " + wheelsColliderBack[0].radius.ToString("F5"));
        GUI.Label(new Rect(0, 240, 180, 30), "DistStatic " + distStatic.ToString("F5"));
        GUI.Label(new Rect(0, 260, 180, 30), "InputSteer " + inputSteer.ToString("F5"));
        GUI.EndGroup();
         */
    }

    /*
    public void AverageAuto(float clamp, out float motorTorque, out float brakeTorque, float inputSteer)
    {
        motorTorque = 0;
        brakeTorque = 0;

        float diff = 30.0f - forwardSpeed;
        float absDiff = Mathf.Abs(diff);
        float factor = absDiff / 4.0f;

        if (absDiff < 1.5f)
        {
            if (diff >= 0)
                motorTorque = 1300 * factor;
            else
                brakeTorque = brake * factor;

        }
        else
        {
            if (diff < 0)
            {
                motorTorque = 0;
                brakeTorque = brake;
            }
            else
            {
                motorTorque = 1300;
                brakeTorque = 0;
            }
        }

        //DropText(motorTorque.ToString() + " " + brakeTorque.ToString());
    }
     */

    void Update()
    {

		if (killTimer.isEnd(Time.deltaTime))
        {
				killTimer.Restore ();
				if (isTooFar (killDistance, 5.0f)) 
				{
				currentGameObject.SetActive (false);
				Kill ();
				}
        }

        if (!isOiled)
        for (int i=0; i<Main.instance.oilDots.Count; i++)
        {
            Circle2 circle = new Circle2(Main.instance.oilDots[i].transform.position.ToVector2XZ(), Main.instance.oilSize);
            if (TrafficController.IntersectBounds(this, circle, true))
            {
                OilEffect();
            }
        }

        

        //DropText(currentRigidbody.velocity.magnitude.ToString());

    }

    void OilEffect()
    {
        isOiled = true;
        oiledSide = Math3d.GetChanceOf(50) ? 1.0f : -1.0f;

    }
    void OnCollisionEnter(Collision collision)
    {
        CollisionWithUnit(collision);

    }

    

    public static SideQuad GetSideQuad(Vector3 point, Transform _transform)
    {
        Vector3 damageVector = _transform.InverseTransformPoint(point);

        if (damageVector.z > 0)
        {
            if (damageVector.x > 0)
                return SideQuad.TopRight;
            else
                return SideQuad.TopLeft;
        }
        else
        {
            if (damageVector.x > 0)
                return SideQuad.BottomRight;
            else
                return SideQuad.BottomLeft;
        }

    }

    void DamageWheels(Vector3 point)
    {
        switch (GetSideQuad(point, transform))
        {
            case SideQuad.TopRight:
                if (wheelsColliderForward[1].mass > 0)
                {
                    GameObject newWheel = GameObject.Instantiate(wheel, wheelsColliderForward[1].transform.position, wheelsColliderForward[1].transform.rotation) as GameObject;
                    newWheel.GetComponent<Rigidbody>().AddForce(-Random.Range(30, 700) * (point - transform.position), ForceMode.Impulse);
                    wheelsColliderForward[1].mass = 0;
                    wheelsColliderForward[1].radius = 0;
                    wheelsTransformForward[1].localScale = Vector3.zero;
                    //Debug.Log("Right01 Wheel " + currentGameObject.name + " " + transform.position);
                    Main.instance.DrawDestroyWheelIcon(this);
                }
                break;
            case SideQuad.TopLeft:
                if (wheelsColliderForward[0].mass > 0)
                {
                    GameObject newWheel = GameObject.Instantiate(wheel, wheelsColliderForward[0].transform.position, wheelsColliderForward[0].transform.rotation) as GameObject;
                    newWheel.GetComponent<Rigidbody>().AddForce(-Random.Range(30, 700) * (point - transform.position), ForceMode.Impulse);
                    wheelsColliderForward[0].mass = 0;
                    wheelsColliderForward[0].radius = 0;
                    wheelsTransformForward[0].localScale = Vector3.zero;
                   // Debug.Log("Left01 Wheel " + currentGameObject.name + " " + transform.position);
                    Main.instance.DrawDestroyWheelIcon(this);
                }
                break;
            case SideQuad.BottomRight:
                if (wheelsColliderBack[1].mass > 0)
                {
                    GameObject newWheel = GameObject.Instantiate(wheel, wheelsColliderBack[1].transform.position, wheelsColliderBack[1].transform.rotation) as GameObject;
                    newWheel.GetComponent<Rigidbody>().AddForce(-Random.Range(30, 700) * (point - transform.position), ForceMode.Impulse);
                    wheelsColliderBack[1].mass = 0;
                    wheelsColliderBack[1].radius = 0;
                    wheelsTransformBack[1].localScale = Vector3.zero;
                    //Debug.Log("Right02 Wheel " + currentGameObject.name + " " + transform.position);
                    Main.instance.DrawDestroyWheelIcon(this);
                }
                break;
            case SideQuad.BottomLeft:
                if (wheelsColliderBack[0].mass > 0)
                {
                    GameObject newWheel = GameObject.Instantiate(wheel, wheelsColliderBack[0].transform.position, wheelsColliderBack[0].transform.rotation) as GameObject;
                    newWheel.GetComponent<Rigidbody>().AddForce(-Random.Range(30, 700) * (point - transform.position), ForceMode.Impulse);
                    wheelsColliderBack[0].mass = 0;
                    wheelsColliderBack[0].radius = 0;
                    wheelsTransformBack[0].localScale = Vector3.zero;
                    //Debug.Log("Left02 Wheel " + currentGameObject.name + " " + transform.position);
                    Main.instance.DrawDestroyWheelIcon(this);
                }
                break;
        }
    }

    public void DamageHull(Vector3 point)
    {
        CreateGarbage(GetSideTriangle(point, transform));
    }

    void DamageHit(Vector3 force, Vector3 point, float mass, bool withHero)
    {
        float damage = force.magnitude;

        /*
        switch(GetSideTriangle(point, transform))
        {
            case SideTriangle.Top:
            damage *= SideFactors[0];
            break;
            case SideTriangle.Left:
            damage *= SideFactors[1];
            break;
            case SideTriangle.Right:
            damage *= SideFactors[2];
            break;
            case SideTriangle.Back:
            damage *= SideFactors[3];
            break;
        }
        */
        damage *= SideFactors[(int)GetSideTriangle(point, transform)];

        damage *= mass / currentRigidbody.mass;

        if (armor < damage)
            damage -= armor;
        else
            return;



        float exp = 0;

        if (healthPoints.current - damage > 0)
        {
            healthPoints.current -= damage;
            exp = damage;
        }
        else
        {
            exp = healthPoints.current + expBounty;
            healthPoints.current = 0;

            DoCrush();
        }


        if (withHero)
        {
            //DropText(exp.ToString("F1"));
            Main.instance.hero.AddExpierence(exp);
        }


        DestroyEnemyElements(point);

    }

    public void TakeBonus()
    {
            isBonusHas = false;
            Main.instance.DeselectTarget(this);
            bool isHeal = Math3d.GetChanceOf(10);
            if (isHeal)
            {
                GameObject coin = (GameObject)GameObject.Instantiate(Main.instance.healEnemy, transform.position, transform.rotation);
                coin.transform.parent = Main.instance.mainCamera.transform;
            }
            else
            {
                GameObject coin = (GameObject)GameObject.Instantiate(Main.instance.coinEnemy, transform.position, transform.rotation);
                coin.transform.parent = Main.instance.mainCamera.transform;

                coin.GetComponent<CoinUp>().coins = expBounty;
            }
    }
    public void DoCrush()
    {
        if (isAttackedByHero &&  isBonusHas)
        {
            if (Math3d.GetChanceOf(bonusChance))
            TakeBonus();   
        }


        if (Math3d.GetChanceOf(50))
        {
            if (crush != null)
            {

                currentCrush.transform.parent = null;
                currentCrush.currentGameObject.SetActive(true);
                currentCrush.PrepareObject();
                Hide(true);
            }
            else
                Kill();
        }
        else
        {
            durabilityStatus = DurabilityStatus.Ruined;
            currentRigidbody.AddExplosionForce(2000.0f, transform.position + new Vector3(Random.Range(-2.0f,2.0f),Random.Range(-2.0f,2.0f),Random.Range(-2.0f,2.0f)), 5.0f, 100.0F, ForceMode.Impulse);
        }

    }

    void DamageHit(Vector3 force, Vector3 point)
    {
        DamageHit(force.magnitude, point);
    }

    public virtual void DamageHit(float damage, Vector3 point)
    {


        switch (GetSideTriangle(point, transform))
        {
            case SideTriangle.Top:
                damage *= SideFactors[0];
                break;
            case SideTriangle.Left:
                damage *= SideFactors[1];
                break;
            case SideTriangle.Right:
                damage *= SideFactors[2];
                break;
            case SideTriangle.Back:
                damage *= SideFactors[3];
                break;
        }

        if (armor < damage)
            damage -= armor;
        else
            return;



        float exp = 0;

        if (healthPoints.current - damage > 0)
        {
            healthPoints.current -= damage;
            exp = damage;
        }
        else
        {
            exp = healthPoints.current + expBounty;
            healthPoints.current = 0;

            DoCrush();
        }

        //Main.instance.hero.AddExpierence(exp);

        DestroyEnemyElements(point);
    }

    void DestroyEnemyElements(Vector3 point)
    {
        switch (durabilityStatus)
        {
            case DurabilityStatus.Normal:
                if (healthPoints.current < healthPoints.max * 0.67f && healthPoints.current > healthPoints.max * 0.33f)
                {
                    int i = Random.Range(0, 8);
                    switch (i)
                    {
                        case 0:
                            if (garbageEnable) DamageHull(point);
                            break;
                        case 1:
                            if (wheelsEnable) DamageWheels(point);
                            break;
                    }
                    durabilityStatus = DurabilityStatus.Damaged;
                }
                else
                    if (healthPoints.current < healthPoints.max * 0.33f)
                    {
                        int i = Random.Range(0, 8);
                        switch (i)
                        {
                            case 0:
                                if (garbageEnable) DamageHull(point);
                                break;
                            case 1:
                                if (wheelsEnable) DamageWheels(point);
                                break;
                        }
                        durabilityStatus = DurabilityStatus.Ruined;
                    }
                break;
            case DurabilityStatus.Damaged:
                if (healthPoints.current < healthPoints.max * 0.30f)
                {
                    int i = Random.Range(0, 8);
                    switch (i)
                    {
                        case 0:
                            if (garbageEnable) DamageHull(point);
                            break;
                        case 1:
                            if (wheelsEnable) DamageWheels(point);
                            break;
                    }



                    durabilityStatus = DurabilityStatus.Ruined;
                }
                break;
        }

        //DropText(durabilityStatus.ToString());
    }


    public virtual void CollisionWithUnit(Collision collision)
    {

        if (collision.gameObject.CompareTag("Unit"))
        {
            Unit unit = collision.gameObject.GetComponent<Unit>();
            bool withHero = (unit.unitType == UnitType.Hero) ? true : false;

            if (collision.relativeVelocity.magnitude > armor)
            {
                if (garbageEnable) 
                DamageHull(collision.contacts[0].point);
                GetDamage(collision.contacts[0].point, collision.relativeVelocity, collision.rigidbody.mass, withHero);

                if (withHero && heroAbilityEffect && isCollisionActive)
                {
                    SideQuad heroQuad = GetSideQuad(collision.contacts[0].point, Main.instance.hero.transform);

                    isAttackedByHero = true;
                    isAttackedOnce = true;
                    isCollisionActive = false;

                    if (heroQuad == SideQuad.TopLeft || heroQuad == SideQuad.TopRight)
                    {
                        
                       
                        Main.instance.SelectTarget(this);
                        heroAttackTimer.Restore();

                        float magn = collision.relativeVelocity.magnitude;

                        Vector3 dir = -1 * Vector3.Normalize(collision.relativeVelocity);
                        Vector3 point = transform.position + dir;
                        point.y -= 1f;

                        float dist = Vector3.Distance(transform.position, point);
							float diff = Mathf.Clamp(Main.instance.hero.vehicle.currentRigidbody.mass / currentRigidbody.mass, 1.0f, 2.0f);
                        currentRigidbody.AddExplosionForce((magn + 10) * diff * (0.75f + currentRigidbody.mass / 1000) * currentRigidbody.mass, point, 25.0f, magn * diff * 200.0f, ForceMode.Force);

                        //Debug.Log(magn);
                    }          
                }
            }

        }
        else
            if (collision.gameObject.CompareTag("Solid"))
            {
                if (collision.relativeVelocity.magnitude > armor)
                    GetDamage(collision.contacts[0].point, collision.relativeVelocity, currentRigidbody.mass, false);
            }
    }

    public override void GetDamage(Vector3 point, Vector3 force, float mass, bool withHero)
    {
        if (deformEnable)
            DeformMesh(_DeformationMesh, oldVertices, transform, point, force);


        DamageHit(force, point, mass, withHero);
    }

    public override void GetDamage(Vector3 point, Vector3 force)
    {
        if (deformEnable)
            DeformMesh(_DeformationMesh, oldVertices, transform, point, force / 10.0f);
        //if (garbageEnable)
        //    DamageHull(point);

        DamageHit(force, point);
    }

    public override void GetDamageUp(Vector3 point, Vector3 force)
    {
        GetDamage(point, force);
    }
    public override void GetDamage(float damage)
    {
        DamageHit(damage, transform.position);
    }

    public override void AttackByHero()
    {
        isAttackedByHero = true;
        isAttackedOnce = true;
        heroAttackTimer.Restore();
    }
    public void CreateSpawnDot()
    {
#if UNITY_EDITOR
        GameObject _spawnDot = new GameObject();
        //string spawnName = "spawnDot_" + _name;
        //_spawnDot.name = spawnName;
        SpawnDot _spawnDotC = _spawnDot.AddComponent<SpawnDot>();
        _spawnDotC.transform = _spawnDot.transform;
        _spawnDotC.transform.position = transform.position;
        _spawnDotC.transform.rotation = transform.rotation;
        _spawnDotC.enemy = this;

        _spawnDotC.transform.parent = Main.instance.spawnFolder;
        _spawnDotC.enemyPosition = transform.position;
        _spawnDotC.planeNumber = Main.Placed(transform.position).localForkNumber;
        _spawnDotC.forkNumber = Main.Placed(transform.position).runLine;
        //_spawnDotC.CalculateIntersectRect();
#endif
    }

    public void DeformMesh(Mesh mesh, Vector3[] originalMesh, Transform localTransform, Vector3 contactPoint, Vector3 contactForce)
    {
        Vector3[] vertices = mesh.vertices;
        //Vector3[] currentVertices = vertices;
        float sqrRadius = deformRadius * deformRadius;
        float sqrMaxDeform = maxDeform * maxDeform;

        Vector3 localContactPoint = localTransform.InverseTransformPoint(contactPoint);
        Vector3 localContactForce = localTransform.InverseTransformDirection(contactForce);

        //localContactForce *= 6;

        for (int i = 0; i < vertices.Length; i++)
        {
            float dist = (localContactPoint - vertices[i]).sqrMagnitude;

            if (dist < sqrRadius)
            {
                vertices[i] += (localContactForce * (deformRadius - Mathf.Sqrt(dist)) / deformRadius) + Random.onUnitSphere * deformNoise;

                Vector3 deform = vertices[i] - originalMesh[i];

                if (deform.sqrMagnitude > sqrMaxDeform)
                    vertices[i] = originalMesh[i] + deform.normalized * maxDeform;
            }
        }

        originalMesh = vertices;
        mesh.vertices = vertices;
        //mesh.RecalculateNormals();
        mesh.RecalculateBounds();
    }

    protected void ArbUpdate(float rpm)
    {
        /*
        float arbFactor = 22.0f;
        if (rpm > 1000 && rpm <= 3000)
            arbFactor = (rpm - 1000) / 125.0f;
        
        for (int i = 0; i < 2; i++)
            Arb[i].AntiRollFactor = arbFactor;
         */
    }



    protected string RandomString(int count, string root)
    {
        int _rand;
        string result;
        _rand = Random.Range(1, count);
        if (_rand == 1)
            result = root;
        else
            if (_rand < 10)
                result = root + "0" + _rand.ToString();
            else
                result = root + _rand.ToString();
        return result;

    }

    public virtual void onHit(int damage)
    {
        if (healthPoints.current - damage > 0)
            healthPoints.current -= damage;
        else
            SelfDestroy();
    }

    void SelfDestroy()
    {
        /*
        selfDestroy = true;
        if (trafficUnit && LineCounted)
        {
            Main.instanceCamera._trafficController.trafficLines[Line].UnitLimit--;
        }
        GameObject expl = Instantiate(explosion, transform.position, transform.rotation) as GameObject;
        expl.transform.parent = transform;
         */
    }

	/*
    public override void SetupColliders()
    {
        // Vector3 upPose = transform.position;
        // upPose.y += 2.0f;
        // transform.position = upPose;

        colliderPos = new Vector3[4];
        for (int i = 0; i < 2; i++)
            colliderPos[i] = wheelsColliderForward[i].transform.position;
        for (int i = 0; i < 2; i++)
            colliderPos[i + 2] = wheelsColliderBack[i].transform.position;

        //upPose.y -= 4.0f;
        //transform.position = upPose;
    }
	*/

    protected void DeformationMeshActivate()
    {
        oldVertices = new Vector3[_DeformMesh.vertexCount];
        oldVertices = _DeformMesh.vertices;
        _DeformationMesh = new Mesh();
        _DeformationMesh.name = "DeformationMesh";
        _DeformationMesh.vertices = new Vector3[_DeformMesh.vertexCount];
        _DeformationMesh.vertices = _DeformMesh.vertices;
        int submeshCount = _DeformMesh.subMeshCount;
        _DeformationMesh.subMeshCount = submeshCount;
        //Debug.Log(submeshCount + " " + name);
        for (int i = 0; i < submeshCount; i++)
        {
            //_DeformationMesh.SetIndices(_DeformMesh.GetIndices(1), _DeformMesh.GetTopology(1), 1);
           // 
            _DeformationMesh.SetTriangles(_DeformMesh.GetTriangles(i), i);
        }

        _DeformationMesh.uv = _DeformMesh.uv;
        _DeformationMesh.uv2 = _DeformMesh.uv2;
        _DeformationMesh.normals = _DeformMesh.normals;
        _DeformationMesh.tangents = _DeformMesh.tangents;
        _DeformationMesh.RecalculateBounds();

        meshFilter.sharedMesh = _DeformationMesh;
    }

    public void DeformationRevert()
    {
        _DeformationMesh.vertices = oldVertices;
        _DeformationMesh.RecalculateBounds();
        for (int i = 0; i < 2; i++)
        {
            wheelsColliderForward[i].mass = wheelMass;
            wheelsColliderBack[i].mass = wheelMass;
        }
    }


    void FixedUpdate()
    {
        Moving();
        
        //AntiRollBars();
    }

    /*
    public override bool isTooFar(float _Dist, float _trafficDist)
    {
        Vector3 final = transform.position;
        final -= Main.instance.mainCamera.transform.position;
        Vector3.Normalize(final);

        float result = Mathf.Acos(Vector3.Dot(final, Math3d.RotateThis(Vector3.forward, -Main.instance.mainCamera.transform.eulerAngles.y, Vector3.zero)) / (final.magnitude * Vector3.forward.magnitude));
        result *= 180.0f / 3.1415926f;

        //if (!trafficUnit)
        //{

        float Dist = Vector3.Distance(transform.position, Main.instance.mainCamera.transform.position);
        //DropText(transform.position, result.ToString("F2") + " " + Dist.ToString("F2"), 1.0f);
        if ((Dist > _Dist && result > 90.0f) && isActive)
        {
            
            return true;
        }
            


        //if ((Dist > _Dist && result > 90.0f) || (Main.instance.Proto.myRigidbody.velocity.magnitude < 2.0f && !activeInCamera))
        //       return true;

        return false;
        // }
       
        
    }
    */
    public override void SetFriction(float _stiffness)
    {
        foreach (WheelCollider collider in wheelsColliderForward)
        {
            WheelFrictionCurve forwardFrictionCurve = collider.forwardFriction;
            WheelFrictionCurve sidewaysFrictionCurve = collider.sidewaysFriction;
            forwardFrictionCurve.stiffness = _stiffness;
            sidewaysFrictionCurve.stiffness = _stiffness;
            collider.forwardFriction = forwardFrictionCurve;
            collider.sidewaysFriction = sidewaysFrictionCurve;
        }

        foreach (WheelCollider collider in wheelsColliderBack)
        {
            WheelFrictionCurve forwardFrictionCurve = collider.forwardFriction;
            WheelFrictionCurve sidewaysFrictionCurve = collider.sidewaysFriction;
            forwardFrictionCurve.stiffness = _stiffness;
            sidewaysFrictionCurve.stiffness = _stiffness;
            collider.forwardFriction = forwardFrictionCurve;
            collider.sidewaysFriction = sidewaysFrictionCurve;
        }

    }

    void OnDrawGizmos()
    {
        if (isUnitInSphere)
            Gizmos.color = Color.blue;
        else
            Gizmos.color = Color.white;

        //Gizmos.DrawWireSphere(transform.position, sphereRadius);

        EnemyGun[] guns = transform.GetComponentsInChildren<EnemyGun>();

        for (int i = 0; i < guns.Length; i++)
        {

            if (!guns[i].gun.hasTrunks)
            {
                Vector3 start = guns[i].transform.TransformPoint(guns[i].gun.GetShootPosition(transform));
                Vector3 dir = guns[i].transform.TransformDirection(guns[i].transform.forward);


                Gizmos.color = Color.green;
                Gizmos.DrawLine(start, start + dir * 0.2f);

                Gizmos.color = Color.red;
                Gizmos.DrawLine(start, Math3d.RotateThis(dir * 0.2f, guns[i].azimuth, start));
                Gizmos.DrawLine(start, Math3d.RotateThis(dir * 0.2f, -guns[i].azimuth, start));

                Gizmos.color = Color.blue;
                Gizmos.DrawSphere(start, 0.025f);
            }

        }

    }


    /*
    private void UpdateWheels()
    {
        float delta = Time.fixedDeltaTime;

        foreach (WheelData wheel in wheels)
        {
            Vector3 position;
            Quaternion rotation;
            wheel.collider.GetWorldPose(out position, out rotation);
            wheel.wheelTransform.position = position;
            wheel.wheelTransform.rotation = rotation;
        }

    }
    

    
    public void AntiRollBars()
    {

        WheelHit FrontWheelHit;

        float travelFL = 1.0f;
        float travelFR = 1.0f;

        bool groundedFL = wheelsColliderForward[0].GetGroundHit(out FrontWheelHit);

        if (groundedFL)
            travelFL = (-wheelsColliderForward[0].transform.InverseTransformPoint(FrontWheelHit.point).y - wheelsColliderForward[0].radius) / wheelsColliderForward[0].suspensionDistance;

        bool groundedFR = wheelsColliderForward[1].GetGroundHit(out FrontWheelHit);

        if (groundedFR)
            travelFR = (-wheelsColliderForward[1].transform.InverseTransformPoint(FrontWheelHit.point).y - wheelsColliderForward[1].radius) / wheelsColliderForward[1].suspensionDistance;

        float antiRollForceFront = (travelFL - travelFR) * antiRollBarForce;

        if (groundedFL)
            currentRigidbody.AddForceAtPosition(wheelsColliderForward[0].transform.up * -antiRollForceFront, wheelsColliderForward[0].transform.position);
        if (groundedFR)
            currentRigidbody.AddForceAtPosition(wheelsColliderForward[1].transform.up * antiRollForceFront, wheelsColliderForward[1].transform.position);

        WheelHit RearWheelHit;

        float travelRL = 1.0f;
        float travelRR = 1.0f;

        bool groundedRL = wheelsColliderBack[0].GetGroundHit(out RearWheelHit);

        if (groundedRL)
            travelRL = (-wheelsColliderBack[0].transform.InverseTransformPoint(RearWheelHit.point).y - wheelsColliderBack[0].radius) / wheelsColliderBack[0].suspensionDistance;

        bool groundedRR = wheelsColliderBack[1].GetGroundHit(out RearWheelHit);

        if (groundedRR)
            travelRR = (-wheelsColliderBack[1].transform.InverseTransformPoint(RearWheelHit.point).y - wheelsColliderBack[1].radius) / wheelsColliderBack[1].suspensionDistance;

        float antiRollForceRear = (travelRL - travelRR) * antiRollBarForce;

        if (groundedRL)
            currentRigidbody.AddForceAtPosition(wheelsColliderBack[0].transform.up * -antiRollForceRear, wheelsColliderBack[0].transform.position);
        if (groundedRR)
            currentRigidbody.AddForceAtPosition(wheelsColliderBack[1].transform.up * antiRollForceRear, wheelsColliderBack[1].transform.position);

       

    }
    */
}

