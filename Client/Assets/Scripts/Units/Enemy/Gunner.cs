﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Dest.Math;

public class Gunner : EnemyVehicle
{

    private bool isImpact = false;
    //private float necessarySpeed;
    private Vector3 RelativeTargetPosition;
    //private FollowMoving moving;
    private float factor;
    //private bool isRangedStart;
    //public float speedSpace;
    private SkillStatus nextSkillStatus = SkillStatus.Default;
    bool isPrepare;
    //private float restoreAngle;

    //private Vector3 heroTarget;

    public CounterTimerF meleeAttackTimer;
    public CounterTimerF rangeAttackTimer;
    public CounterTimerF cooldownTimer;
    public CounterTimerF prepareTimer;
    public CounterTimerF defaultTimer;
    public CounterTimerF diversionTimer;
    public CounterTimerF followDotTimer;


    private bool isAttackTypeChoise;

    public EnemyGun[] Pistols;

    public override void PrepareObject()
    {
        base.PrepareObject();

        prepareTimer.Restore();
        meleeAttackTimer.Restore();
        rangeAttackTimer.Restore();
        cooldownTimer.Restore();
        defaultTimer.Restore();
        diversionTimer.Restore();
        followDotTimer.Restore();

        isImpact = false;
        force = false;
        isPrepare = false;
    }

    public override void CreateObject()
    {
        base.CreateObject();
        Pistols = GetComponentsInChildren<EnemyGun>();

    }

    public override void CollisionWithUnit(Collision collision)
    {
        if (skillStatus == SkillStatus.MeleeAttack)
        {
            if (collision.gameObject.CompareTag("Unit"))
            {
                Unit unit = collision.gameObject.GetComponent<Unit>();
                bool withHero = (unit.unitType == UnitType.Hero) ? true : false;
                if (!isImpact)
                {
                    if (withHero)
                        unit.currentRigidbody.AddForceAtPosition(collision.impulse * -1.0f, collision.contacts[0].point, ForceMode.Impulse);
                    isImpact = true;
                    meleeAttackTimer.Restore();
                }
            }
        }
        else
        {
            base.CollisionWithUnit(collision);
        }
        
        
       
    }

    public void CreateRoadHogTarget(float forward, float back)
    {
        follow = true;
        Vector3 localPos = Main.instance.mainCamera.targetController.transform.InverseTransformPoint(Main.instance.hero.transform.position);
		OneSpline spline = Math3d.GetEnemySpline(Main.instance.hero.transform.position);
        if (spline == null)
            Kill();
        float width = spline.GetCurrentWidth(Main.instance.hero.currentStep) - 0.5f;
        localPos.z += Random.Range(0, forward+back)-back;

        CreateTarget(localPos);
        
        
        
        
    }
    public override void Boost()
    {

        CreateRoadHogTarget(-1.0f, 8.0f);
     
        currentRigidbody.velocity = transform.TransformDirection(Vector3.forward * Math3d.GetSpeed(currentRpm, wheelsColliderBack[0].radius));
        boost = false;

    }

    void AttackUnit(Vector3 targetPosition)
    {
        float motorTorque = 0;

        forwardSpeed = Mathf.Abs(transform.InverseTransformDirection(currentRigidbody.velocity).z);

        RelativeWaypointPosition = transform.InverseTransformPoint(targetPosition);
        float yAngle = Vector3.Angle(targetPosition - transform.position, transform.forward);


        inputSteer = RelativeWaypointPosition.x / RelativeWaypointPosition.magnitude;
        
        if (Mathf.Abs(inputSteer) < 0.5f)
            inputTorque = Mathf.Abs(RelativeWaypointPosition.z / RelativeWaypointPosition.magnitude - Mathf.Abs(inputSteer));
        else
            inputTorque = 0.0f;

        if (forwardSpeed < 40.0f)
        motorTorque = currentAccel * inputTorque * factor;


        ApplyColliders(inputSteer * steerCurrent, motorTorque, 0);
    }
    public override void SwitchSkillStatus()
    {
        Vector3 directionToTarget = Vector3.Normalize(Main.instance.hero.transform.position - transform.position);
        RelativeTargetPosition = transform.InverseTransformPoint(Main.instance.hero.transform.position);
        float distance = RelativeTargetPosition.z;
        factor = Mathf.Clamp(distance, -3.0f, 3.0f)/3;

        float yAngle = 0;


        switch (skillStatus)
        {
            case SkillStatus.Default:
                {
                    float locPos = followTarget.localPosition.z;
                    Vector3 heroLocalPos = Main.instance.mainCamera.targetController.transform.InverseTransformPoint(Main.instance.hero.transform.position);

                    if (followDotTimer.isEnd(Time.deltaTime))
                        locPos = heroLocalPos.z - 7.0f;                 
                    followTarget.localPosition = new Vector3(heroLocalPos.x, followTarget.localPosition.y, locPos);

                    if (!isAttackTypeChoise)
                    {
                        if (Math3d.GetChanceOf(20))
                        {
                     
                            nextSkillStatus = SkillStatus.MeleeAttack;
                        }
                        else
                        {
                            
                            
                            nextSkillStatus = SkillStatus.RangedAttack;
                        }
                        
                        isAttackTypeChoise = true;
                    }

                    if (defaultTimer.isEnd(Time.deltaTime))
                    {
                        DoDefault();
                    }

                    if (cooldownTimer.isEnd(Time.deltaTime))
                    {
                        isPrepare = true;
                    }

                    if (isPrepare)
                    {
                        if (RelativeTargetPosition.magnitude < 14.0f)
                        {

                            RaycastHit hit;
                            if (Physics.Raycast(transform.position, directionToTarget, out hit, 30.0f))
                            {
                                if (hit.transform.CompareTag("Unit"))
                                {
                                    Unit unit = hit.transform.GetComponent<Unit>();
                                    if (unit.unitType == UnitType.Hero)
                                    {
                                        if (prepareTimer.isEnd(Time.deltaTime))
                                        {

                                            skillStatus = nextSkillStatus;
                                            isPrepare = false;
                                            isAttackTypeChoise = false;
                                            meleeAttackTimer.Restore();
                                            rangeAttackTimer.Restore();

                                            if (skillStatus == SkillStatus.RangedAttack)
                                            {
                                                CreateRoadHogTarget(-1.0f, 8.0f);
                                            }
                                        }
                                    }

                                    else
                                    {
                                        if (unit.unitType == UnitType.Enemy)
                                        {
                                            //if (Math3d.GetChanceOf(10))
                                            //    DoDiversion();
                                        }
                                    }
                                }

                            }
                        }
                        else
                        {
                            prepareTimer.Restore();
                        }
                    }
                    

                   //DropText("-1 " + factor + " 1 " + currentRigidbody.angularVelocity.magnitude.ToString("F2") + " " + forwardSpeed.ToString("F2") + " " + currentRigidbody.velocity.magnitude.ToString("F2"));
                    MovementFunctions(Waypoint, true, true, true);
                    
                }
                break;

            //ATTACK
            case SkillStatus.MeleeAttack:
                {
                    float locPos = followTarget.localPosition.z;
                    Vector3 heroLocalPos = Main.instance.mainCamera.targetController.transform.InverseTransformPoint(Main.instance.hero.transform.position);

                    if (followDotTimer.isEnd(Time.deltaTime))
                        locPos = heroLocalPos.z - 7.0f;
                    followTarget.localPosition = new Vector3(heroLocalPos.x, followTarget.localPosition.y, locPos);
                    /*
                    if (!force)
                    {
                        followTarget.localPosition = Main.instance.mainCamera.controlPointDot.transform.InverseTransformPoint(Main.instance.hero.transform.position);
                        //offset = target.localPosition.x;
                        force = true;
                    }
                    */
                    

                    /*
                    if (Mathf.Abs(distance) > 3.5f || RelativeTargetPosition.x > 0 && inputSteer < 0 || RelativeTargetPosition.x < 0 && inputSteer > 0)
                    {
                        if (Math3d.GetChanceOf(10))
                            DoDiversion();
                        else
                            DoDefault();
                    }
                    */
                     //Debug.Log("HYU");

                    AttackUnit(Main.instance.hero.transform.position);

                    if (meleeAttackTimer.isEnd(Time.deltaTime) || isImpact || Mathf.Abs(followTarget.localPosition.x) > 5.0f)
                    {
                        //if (Math3d.GetChanceOf(10))
                        //    DoDiversion();
                        //else
                        CreateNewWaypoint(10.0f, false);
                        //ApplyColliders(0, 0, 0);
                        DoDefault();
                    }
                }

                break;

            case SkillStatus.RangedAttack:
                {
                    float locPos = followTarget.localPosition.z;
                    Vector3 heroLocalPos = Main.instance.mainCamera.targetController.transform.InverseTransformPoint(Main.instance.hero.transform.position);

                    if (followDotTimer.isEnd(Time.deltaTime))
                        locPos = heroLocalPos.z - 7.0f;
                    followTarget.localPosition = new Vector3(heroLocalPos.x, followTarget.localPosition.y, locPos);
                    

                    yAngle = Vector3.Angle(transform.forward, directionToTarget);


                    for (int i = 0; i < Pistols.Length; i++)
                    {
						Pistols[i].target = Main.instance.hero.vehicle;
                        Pistols[i].Attack();
                    }

                   

                    if (rangeAttackTimer.isEnd(Time.deltaTime) || Mathf.Abs(yAngle) > 20.0f)
                    {
                        for (int i = 0; i < Pistols.Length; i++)
                        {
                            Pistols[i].gun.magazine.Restore();
                            Pistols[i].Stop();
                        }

                        //if (Math3d.GetChanceOf(30))
                        //    DoDiversion();
                        //else
                            DoDefault();
                    }

                    MovementFunctions(Waypoint, true, true, true);
                }

                break;

            case SkillStatus.Diversion:
                {
                    if (diversionTimer.isEnd(Time.deltaTime))
                        DoDefault();

                    MovementFunctions(Waypoint, true, true, true);
                }

                break;    
        }

        //DropText(skillStatus.ToString());
    }

    void DoDefault()
    {
        
        defaultTimer.Restore();
        CreateRoadHogTarget(-1.0f, 8.0f);
        force = false;
        skillStatus = SkillStatus.Default;
        isImpact = false;
        meleeAttackTimer.Restore();
        rangeAttackTimer.Restore();
        cooldownTimer.Restore();
    }

    void DoDiversion()
    {
        diversionTimer.Restore();
        
        skillStatus = SkillStatus.Diversion;
        isImpact = false;
        force = false;
        meleeAttackTimer.Restore();
        rangeAttackTimer.Restore();
        cooldownTimer.Restore();

        //if (Math3d.GetChanceOf(50))
        //    CreateRoadHogTarget(-2.0f, 6.0f);
       // else
        CreateRoadHogTarget(-1.0f, 8.0f);
    }

    

    public override float Avoidance(bool isLong, float multi)
    {

        float _multi = 1.2f;
        if (skillStatus == SkillStatus.RangedAttack || skillStatus == SkillStatus.MeleeAttack)
            _multi = 1.0f;
        return base.Avoidance(false, _multi);
    }


}
