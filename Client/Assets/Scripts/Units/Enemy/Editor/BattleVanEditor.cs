﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(BattleVan))]
public class BattleVanEditor : Editor
{

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        BattleVan myTarget = (BattleVan)target;

        if (GUILayout.Button("Create Spawn Dot"))
        {
            myTarget.CreateSpawnDot();
        }

        
        


    }
}
