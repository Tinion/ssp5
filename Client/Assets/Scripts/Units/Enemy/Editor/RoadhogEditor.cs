﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(Roadhog))]
public class RoadhogEditor : Editor
{

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        Roadhog myTarget = (Roadhog)target;

        if (GUILayout.Button("Create Spawn Dot"))
        {
            myTarget.CreateSpawnDot();
        }
        


    }
}
