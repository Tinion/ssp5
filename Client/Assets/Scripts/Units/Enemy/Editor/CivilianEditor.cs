﻿using UnityEngine;
using System.Collections;
using UnityEditor;




[CustomEditor(typeof(Civilian))]
public class CivilianEditor : VehicleEditor
{
    private Vector3 force = Vector3.zero;
    private Vector3 pos = Vector3.zero;
    private float forcePower = 0;
    private ForceMode forceMode = ForceMode.Force;
    public string[] options = new string[4] {"Force", "Impulse", "Acceleraton", "VelocityChange"};
    
   
    public override void OnInspectorGUI()
    {
        
        base.OnInspectorGUI();
        
        Civilian myTarget = (Civilian)target;

        if (GUILayout.Button("Create Spawn Dot"))
        {
            myTarget.CreateSpawnDot();
        }

		myTarget.dir = myTarget.transform;
        force = Vector3.Normalize(myTarget.transform.InverseTransformDirection(myTarget.dir.localPosition));
        forcePower = EditorGUILayout.FloatField(forcePower);
        force = EditorGUILayout.Vector3Field("Force", force);
        pos = EditorGUILayout.Vector3Field("Pos", pos);
        forceMode = (ForceMode)EditorGUILayout.Popup((int)forceMode, options);
        Debug.DrawLine(myTarget.transform.position, myTarget.dir.position, Color.red);

        if (GUILayout.Button("Add Force"))
        {
            myTarget.currentRigidbody.AddForce(force * -forcePower * myTarget.currentRigidbody.mass, forceMode);
        }

        if (GUILayout.Button("Add Force At Position"))
        {
            myTarget.currentRigidbody.AddForceAtPosition(force * -forcePower * myTarget.currentRigidbody.mass, myTarget.transform.TransformPoint(pos), forceMode); 
        }

        if (GUILayout.Button("Add Torque"))
        {
            myTarget.currentRigidbody.AddTorque(force * -forcePower * myTarget.currentRigidbody.mass, forceMode);
        }

        if (GUILayout.Button("Revert"))
        {
            myTarget.currentRigidbody.velocity = Vector3.zero;
            myTarget.currentRigidbody.angularVelocity = Vector3.zero;
            myTarget.transform.position = new Vector3(0, -0.1640997f, 10.0f);
            myTarget.transform.rotation = Quaternion.Euler(new Vector3(0, 180.0f, 0));

        }

                
    }
}
