﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(SpawnDot))]
public class SpawnDotEditor : Editor
{
 
    public override void OnInspectorGUI()
    {

        SpawnDot myTarget = (SpawnDot)target;
        myTarget.enemy = (EnemyVehicle)EditorGUILayout.ObjectField(myTarget.enemy, typeof(EnemyVehicle));
        if (myTarget.enemy != null)
        {
            if (GUILayout.Button("Setup and Hide Enemy"))
            {
                myTarget.SaveUnitSpawnData();
            }
        }
        else

        if (GUILayout.Button("Show Enemy"))
        {
            myTarget.SpawnEnemyE();
        }
        
        

                
    }
}
