﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Plasmatron : EnemyVehicle
{

    public EnemyMachineGun MachineGun;
    
    public CounterTimerF defaultTimer;
    public CounterTimerF rotateTimer;
    public CounterTimerF roleTimer;
    bool rightSide = true;
    public FollowMoving moving;
    public Roles roles = Roles.Dominate;

    public CounterTimerF machineGunTimer;

    public Transform gunTurret;

    private Vector3 RelativeHeroPosition;

    public float MaxDistance = 30.0f;
    public float AverageDistance = 25.0f;
    public float MinDistance = 20.0f;

    public float MaxDistanceDeceive = 20.0f;
    public float AverageDistanceDeceive = 15.0f;
    public float MinDistanceDeceive = 10.0f;

    public float MaxDistanceFlame = 10.0f;
    public float AverageDistanceFlame = 5.0f;
    public float MinDistanceFlame = 0.0f;

    public float AccelerationBonus = 100.0f;
    public float BrakeBonus = 200.0f;
    public float SpeedDifference = 5.0f;

    public bool isTarget;


    public override void RevertObject()
    {
        base.RevertObject();
        roles = Roles.Dominate;
        roleTimer.Restore();
        moving = FollowMoving.Move;
        MachineGun.StopShoot();

    }
  
    
    public override void PrepareObject()
    {

        base.PrepareObject();
        roleTimer.Restore();
        isTarget = false;
    }


    public override void SwitchSkillStatus()
    {

        Vector3 directionToTarget = Vector3.Normalize(Main.instance.hero.transform.position - transform.position);

        float yAngle = 0;

        RelativeHeroPosition = transform.InverseTransformPoint(Main.instance.hero.transform.position);
        if (RelativeHeroPosition.x > 0)
            yAngle = Vector3.Angle(transform.right, directionToTarget);
        else
            yAngle = Vector3.Angle(transform.right * -1, directionToTarget) * -1;

        float factor = RelativeHeroPosition.z;


            MovementFunctions(Waypoint, true, true, false);


            StateController(factor);

            //DropText(skillStatus.ToString() + " " + roles.ToString());
    }

    private void StateController(float factor)
    {
        switch (skillStatus)
        {

            case SkillStatus.Default:
                {

                    if (defaultTimer.isEnd(Time.deltaTime) && RelativeHeroPosition.magnitude < AverageDistance)
                    {
                        skillStatus = SkillStatus.RangedAttack;
                    }

                }
                break;

            case SkillStatus.RangedAttack:
                {
                    if (!isTarget)
                    {
                        MachineGun.StartShoot();
                        machineGunTimer.current = machineGunTimer.max;
                        isTarget = true;
                        
                    }
                    if (machineGunTimer.isEnd(Time.deltaTime) || CheckGround()<2)
                    {

                        skillStatus = SkillStatus.Default;
                        gunTurret.localEulerAngles = Vector3.zero;
                        MachineGun.StopShoot();
                        MachineGun.gun.magazine.current = MachineGun.gun.magazine.max;
                        isTarget = false;
                    }
                    else
                    {
                        gunTurret.LookAt(Main.instance.hero.transform.position);
                        Vector3 localEuler = gunTurret.localEulerAngles;
                        localEuler.x = 0;
                        localEuler.z = 0;
                        gunTurret.localRotation = Quaternion.Euler(localEuler);
                    }
                }

                break;
            
            
             
        }
    }

   

    public override void CreateNewWaypoint(float wayLength, bool needNewSpline)
    {
        currentSplineOffset = CalculateOffsetFromHero(Random.Range(0.8f, 1.2f));

        base.CreateNewWaypoint(wayLength, needNewSpline);
    }

    public override void Boost()
    {
        currentRigidbody.velocity = transform.TransformDirection(Vector3.forward * Math3d.GetSpeed(currentRpm, wheelsColliderBack[0].radius));
        boost = false;
    }
    
    /*
    public override bool isTooFar(float _Dist, float _trafficDist)
    {

        float Dist = Vector3.Distance(transform.position, Main.instance.mainCamera.transform.position);
        if (Dist > _Dist)
        {

            return true;
        }

        return false;
    }
    */
    
    public override float Avoidance(bool isLong, float multi)
    {
        return base.Avoidance(false, 1.5f);

    }
    
    
    public override void AverageAuto(float clamp, out float motorTorque, out float brakeTorque)
    {
        motorTorque = 0;
        brakeTorque = 0;
        
        float difference = Main.instance.hero.forwardSpeedTact - forwardSpeed;
        float distance = -RelativeHeroPosition.z;

        if (distance < AverageDistance)
        {
            if (roleTimer.isEnd(Time.deltaTime))
            {
                
                        roles = SwitchRole();                                      

                        roleTimer.current = roleTimer.max;

                        if (skillStatus == SkillStatus.RangedAttack && roles == Roles.Melee)

                        {
                            skillStatus = SkillStatus.Default;
                            gunTurret.localEulerAngles = Vector3.zero;
                            MachineGun.StopShoot();

                        }
             }
            
            }
        

        switch (roles)
        {
            case Roles.Dominate:
                Domination(ref motorTorque, ref brakeTorque, ref distance, ref difference);
            break;
            case Roles.Melee:
                Domination(ref motorTorque, ref brakeTorque, ref distance, ref difference);
            break;
            case Roles.Deceive:
                Deceive(ref motorTorque, ref brakeTorque, ref distance, ref difference);
            break;
        }

        
    }

    

    Roles SwitchRole()
    {
        /*
        if (roles != Roles.Deceive)
        {
            float r = Random.Range(0.0f, 100.0f);
            if (r < 20.0f)
                return Roles.Deceive;
        }

        if (roles == Roles.Dominate)
            return Roles.Melee;
        else
            if (roles == Roles.Melee)
                return Roles.Dominate;

        return Roles.Dominate;
        */
        if (Math3d.GetChanceOf(0))
            return Roles.Deceive;
            else
            return Roles.Dominate;


    }

    void Domination(ref float motorTorque, ref float brakeTorque, ref float distance, ref float difference)
    {
        if ((distance < MinDistance || difference > SpeedDifference))
            moving = FollowMoving.Boost;
        else
            if (distance > AverageDistance)
                moving = FollowMoving.Brake;
            else
                moving = FollowMoving.Move;

        if (forwardSpeed > Main.instance.hero.maxSpeed)
            moving = FollowMoving.Brake;

        switch (moving)
        {
            case FollowMoving.Boost:

                motorTorque = currentAccel * inputTorque;
                if (distance < MinDistance)
                    motorTorque += Mathf.Lerp(AccelerationBonus, 0, Mathf.Clamp(distance, 0, MinDistance));
                if (difference > 0)
                    motorTorque += AccelerationBonus * (difference / SpeedDifference);
                if (difference < 0)
                    brakeTorque += Mathf.Lerp(BrakeBonus, 0, Mathf.Clamp(difference, 0, SpeedDifference) / SpeedDifference);
                brakeTorque = 0;// Mathf.Abs(inputSteer * currentBrake * 0.5f);

                break;
            case FollowMoving.Move:



                motorTorque = 0;
                brakeTorque = 0;// Mathf.Abs(inputSteer * currentBrake * 0.5f);

                if (difference > 0)
                    motorTorque += inputTorque * AccelerationBonus * (difference / SpeedDifference);
                if (difference < 0)
                    brakeTorque += BrakeBonus * (-difference / SpeedDifference);


                break;
            case FollowMoving.Brake:


                motorTorque = 0;
                brakeTorque = currentBrake + Mathf.Lerp(0, BrakeBonus, Mathf.Clamp(distance, AverageDistance, MaxDistance));
                if (difference < 0)
                    brakeTorque += BrakeBonus * (-difference / SpeedDifference);
                break;
        }      
            //string text = moving.ToString() + " " + motorTorque.ToString("F1") + " " + brakeTorque.ToString("F1") + " " + RelativeTargetPosition.z.ToString("F1") + " " + (forwardSpeed - Main.instance.hero.forwardSpeed).ToString("F1");
        
    }

    

    public void Deceive(ref float motorTorque, ref float brakeTorque, ref float distance, ref float difference)
    {
        //difference -= 7.5f;

        if (distance < MinDistanceDeceive)
            moving = FollowMoving.Boost;
        else
            if (distance > AverageDistanceDeceive)
                moving = FollowMoving.Brake;
            else
                moving = FollowMoving.Move;

        if (forwardSpeed > 40.0f)
            moving = FollowMoving.Brake;

        switch (moving)
        {
            case FollowMoving.Boost:

                motorTorque = currentAccel * inputTorque + Mathf.Lerp(AccelerationBonus, 0, Mathf.Clamp(distance, 0, MinDistanceDeceive));            
                brakeTorque = 0;

                break;
            case FollowMoving.Move:

                if (difference < 0)
                    motorTorque = inputTorque * Mathf.Lerp(AccelerationBonus, 0, Mathf.Clamp(distance, 0, MinDistanceDeceive));
                if (difference > 0)
                    brakeTorque = Mathf.Lerp(0, BrakeBonus, Mathf.Clamp(distance, AverageDistanceDeceive, MaxDistanceDeceive));
                break;

            case FollowMoving.Brake:

                motorTorque = 0;
                brakeTorque = currentBrake + Mathf.Lerp(0, BrakeBonus, Mathf.Clamp(distance, AverageDistanceDeceive, MaxDistanceDeceive));
                
                break;
        }

        if (difference > 0)
            motorTorque += inputTorque * currentAccel / 1.5f * Mathf.Clamp01(Mathf.Abs(difference));
        if (difference < 0)
            brakeTorque += currentBrake / 1.5f * Mathf.Clamp01(Mathf.Abs(difference));

        //if (difference > 0)
        //    motorTorque += AccelerationBonus * (difference / SpeedDifference);
        //if (difference < 0)
        //    brakeTorque += BrakeBonus * (-difference / SpeedDifference);

    }

    public void Leave(ref float motorTorque, ref float brakeTorque, ref float distance, ref float difference)
    {
        if (forwardSpeed > 35.0f)
            brakeTorque = currentBrake;
        else
            motorTorque = currentAccel * inputTorque;

       //DropText(Vector3.Distance(transform.position, Main.instance.mainCamera.transform.position).ToString());
    }


    
    
}
