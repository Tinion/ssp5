﻿using UnityEngine;
using System.Collections;
using Dest.Math;

public class Bomber : Enemy
{
    private CounterTimerF lifeTime;
    private float splineLenght = 0.0f;
    public float speed;
    public CatmullRomSpline3 movingSpline;
    public float selectTargetTime;
    public float selectBombingTime;
    private bool isTargetSelect = false;
    private bool isBombing = false;
    public float bombsFlyingTime = 1.0f;
    public int bombsCount = 3;
    public Object bomb;
    public Object BombTarget;
    

    void Update()
    {
        if (lifeTime.isEnd(Time.deltaTime))
        {
            Kill();
        }
        
        
        PositionTangent posTan = movingSpline.EvalPositionTangent(lifeTime.current / lifeTime.max);

        transform.position = Main.instance.mainCamera.targetController.currentPlaneBeacon.TransformPoint(posTan.Position);
        //transform.position = Main.instance.mainCamera.controlPoint.position + posTan.Position;
        transform.rotation = Quaternion.LookRotation(Main.instance.mainCamera.targetController.currentPlaneBeacon.TransformDirection(posTan.Tangent));
        if ((lifeTime.current / lifeTime.max < selectTargetTime) && !isTargetSelect)
        {
            TargetSelect();
        }

        if ((lifeTime.current / lifeTime.max < selectBombingTime) && !isBombing)
        {
            Bombing();
        }
    }

    void TargetSelect()
    {
        isTargetSelect = true;
        Main.instance.mainCamera.targetController.CreateTargets(bombsCount, bombsFlyingTime, BombTarget, transform);
    }

    void Bombing()
    {
        isBombing = true;
        for (int i=0; i<bombsCount; i++)
        {
        OneTarget target = Main.instance.mainCamera.targetController.targets[i];
        GameObject newBomb = GameObject.Instantiate(bomb, transform.position, Quaternion.identity) as GameObject;
        Shell bombComponent = newBomb.GetComponent<Shell>();
        bombComponent.lifeTime.max = target.timer.current;
        bombComponent.lifeTime.current = bombComponent.lifeTime.max;
        bombComponent.master = transform;
        bombComponent.startPos = Main.instance.mainCamera.targetController.transform.InverseTransformPoint(transform.position);
        bombComponent.oneTarget = target;
        bombComponent.cameraController = Main.instance.mainCamera.targetController.transform;
        }
    }

    public override void Initialize(bool activeObject)
    {
        base.Initialize(activeObject);

        CreateObject();

        if (activeObject)
            PrepareObject();
    }

    public override void CreateObject()
    {
        
    }

    public override void Kill()
    {

        Main.instance.activeUnitObjects.Remove(this);
		Main.instance.EnemyPoolSystem.Objects[(int)type].DestroyObjectPool(this);

        return;
    }

    public override void PrepareObject()
    {
        movingSpline = Main.instance.planeSplinesController.GetRandomSpline();
        splineLenght = movingSpline.CalcTotalLength();

        lifeTime.max = splineLenght / speed;
        lifeTime.current = lifeTime.max;

        isTargetSelect = false;
        isBombing = false;
        healthPoints.Restore();
    }

    public override void RevertObject()
    {

    }


}
