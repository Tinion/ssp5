﻿using UnityEngine;
using System.Collections;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class SpawnDot : MonoBehaviour {

    public bool isUsed = false;
    
    public Transform transform;

    public Vector2[] rect;

    public float percent;
    public int forkNumber;
    public int planeNumber;
    public string enemyName;
    public Vector3 enemyPosition;
    public Quaternion enemyRotation;

    public EnemyVehicle enemy;



    public void SaveUnitSpawnData()
    {
		Main.instance = FindObjectOfType<Main> ();
        transform = transform;
        BlockPlane currentPlane = Main.Placed(transform.position);
        planeNumber = currentPlane.localForkNumber;
        forkNumber = currentPlane.runLine;
        percent = currentPlane.spline.GetStep(transform.position);

        //enemyName = enemy._name;
        enemyPosition = enemy.transform.position;
        enemyRotation = enemy.transform.rotation;

        //CalculateIntersectRect();

        DestroyImmediate(enemy.gameObject);
    }
    /*
    public void CalculateIntersectRect()
    {
        rect = new Vector2[4];

        enemy.intersectRect.CreateLocals(enemy.currentRenderer.bounds);
        enemy.intersectRect.UpdateRect(enemy.transform);

        rect[0] = enemy.intersectRect.TopLeft2D;
        rect[1] = enemy.intersectRect.TopRight2D;
        rect[2] = enemy.intersectRect.BottomRight2D;
        rect[3] = enemy.intersectRect.BottomLeft2D;
    }
    */
	//void Update () {
       // if (Main.instance.mainCamera._currentCamPlane!=null)
       // if (Main.instance.mainCamera._currentCamPlane.localForkNumber == planeNumber && Main.instance.mainCamera._currentCamPlane.runLine == forkNumber)
            //if (Main.instance.mainCamera._currentCamPersentForRotation > percent && !isUsed)
         //   {
         //       SpawnEnemy();
         //       isUsed = true;
                //gameObject.SetActive(false);
          //  }
	//}


    public void SpawnEnemy()
    {
        Unit AutoEnemy = Main.instance.EnemyPoolSystem.GetPooled(enemyName);

        AutoEnemy.currentGameObject.SetActive(true);

        EnemyVehicle AutoEnemyComponent = AutoEnemy.GetComponent<EnemyVehicle>();
        AutoEnemyComponent.transform.position = enemyPosition;
        AutoEnemyComponent.transform.rotation = enemyRotation;

        AutoEnemyComponent.PrepareObject();
        //AutoEnemyComponent.SetupColliders();

        
    }

    public void SpawnEnemyE()
    {
#if UNITY_EDITOR
        Object _enemy = Main.instance.GetVehicle(enemyName);
        GameObject AutoEnemy = PrefabUtility.InstantiatePrefab(_enemy) as GameObject;
        
        enemy = AutoEnemy.GetComponent<EnemyVehicle>();
        enemy.transform.position = enemyPosition;
        enemy.transform.rotation = enemyRotation;
#endif

    }

#if UNITY_EDITOR
    void OnDrawGizmos()
    {
        

        Gizmos.color = Color.black;

        Gizmos.DrawLine(transform.position, enemyPosition);

        Gizmos.color = Color.green;

        Gizmos.DrawSphere(transform.position, 0.4f);

        Gizmos.color = Color.red;

        if (rect.Length>0)
        {
            Gizmos.DrawLine(new Vector3(rect[0].x, enemyPosition.y, rect[0].y), new Vector3(rect[1].x, enemyPosition.y, rect[1].y));
            Gizmos.DrawLine(new Vector3(rect[1].x, enemyPosition.y, rect[1].y), new Vector3(rect[2].x, enemyPosition.y, rect[2].y));
            Gizmos.DrawLine(new Vector3(rect[2].x, enemyPosition.y, rect[2].y), new Vector3(rect[3].x, enemyPosition.y, rect[3].y));
            Gizmos.DrawLine(new Vector3(rect[3].x, enemyPosition.y, rect[3].y), new Vector3(rect[0].x, enemyPosition.y, rect[0].y));
        }
    }
#endif

}
