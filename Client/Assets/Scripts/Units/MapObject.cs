﻿using UnityEngine;
using System.Collections;

public class MapObject : MonoBehaviour 
{
    public string nominal;
    
	[HideInInspector]
	public GameObject currentGameObject;

	protected bool isInitialize = false;
    
	public virtual void Initialize(bool activeObject)
    {
		if (isInitialize)
			return;
		
		isInitialize = true;
		//Debug.Log (nominal + " Initialized");
    }

}
