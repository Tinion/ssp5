﻿using UnityEngine;
using System.Collections;

public class FrontModule : Module {


    public Mesh sourceHood;

    public override void Install()
    {
        transform.parent = Main.instance.hero.transform;
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;
        gameObject.SetActive(true);

        MeshFilter mf;
		mf = Main.instance.hero.hood.GetComponent<MeshFilter>();
        mf.mesh = sourceHood;
        Main.instance.inventory.inventorySlots[(int)slotType].renderer[0].enabled = false;
    }

    public override void Remove()
    {
        base.Remove();
        Main.instance.hero.hood.GetComponent<MeshFilter>().mesh = Main.instance.inventory.hoodDefaultSource;
    }
    

}
