﻿using UnityEngine;
using System.Collections;
using Dest.Math;

[System.Flags]
public enum UnitType
{
    Hero,
    Enemy,
    Solid,
}

[System.Flags]
public enum DurabilityStatus
{
    Normal,
    Damaged,
    Ruined,
}

public class Unit : MapObject
{
    public bool isActive = false;


    public Rigidbody currentRigidbody;
    public Collider currentCollider;
    public Renderer currentRenderer;
    public Transform centerOfMass;

    public UnitType unitType;
	public EnemyType type;
    public DurabilityStatus durabilityStatus;
    public CounterTimerF healthPoints;
    
	protected OneSpline currentSpline;
    public float currentStep;
    public float currentSplineOffset;
	public float sphereRadius;

    public float frictionResistance;
    public float forwardSpeed;
    public bool isBurned;
    private bool isControl;

    protected bool isTextDropping = false;
    protected float textDropTimer = 0.0f;
    protected Vector3 TextPos = Vector3.zero;
    protected string droppingText;

	public CounterTimerF killTimer = new CounterTimerF (0.0f, 1.0f);
	public float killDistance;

    //public Vector3[] colliderPos; 

    //protected bool isControl = true;
	void Start () 
    {
		if (!isInitialize)
        Initialize(true);
	}

    public override void Initialize(bool activeObject)
    {
		base.Initialize(activeObject);

		Main.instance = FindObjectOfType<Main>();
        currentSpline = null;
        currentStep = 0.0f;
        isBurned = false;

		BoundsCorrector[] boundsCorrectors = transform.GetComponentsInChildren<BoundsCorrector>();
		for (int i = 0; i < boundsCorrectors.Length; i++)
			boundsCorrectors [i].Resize ();
    }

    public void DropText(string text)
    {
        isTextDropping = true;
        TextPos = transform.position;
        droppingText = text;
        textDropTimer = 1.0f;
    }

    public virtual void AttackByHero()
    {

    }
    
    public virtual void DisableControl()
    {
        isControl = false;
    }

    public virtual void EnableControl()
    {
        isControl = true;
    }
    
    public virtual void Kill()
    {
        Debug.Log("KILLLLL");
    }

    public virtual void GetDamage(Vector3 point, Vector3 force, float mass, bool withHero)
    {
    }

    public virtual void GetDamage(Vector3 point, Vector3 force)
    {
    }

    public virtual void GetDamageUp(Vector3 point, Vector3 force)
    {
    }

    public virtual void GetDamage(float damage)
    {
    }

    public virtual void GetDamageSplash(float damage)
    {
        GetDamage(damage);
    }
    public virtual void SetFriction(float _stiffness)
    {

    }

    public virtual void DebugGui()
    {
        if (isTextDropping)
        {
			Vector3 Pos = MainCamera.instance.roadAffectCamera.WorldToScreenPoint(TextPos);
            GUI.Label(new Rect((int)Pos.x, Screen.height - (int)Pos.y + 10, 90, 120), droppingText);


            textDropTimer -= Time.deltaTime;
            if (textDropTimer < 0.0f)
            {
                isTextDropping = false;
            }
        }
    }
    void OnGUI()
    {
        DebugGui();
    }

    
	/*
    public virtual void SetupColliders()
    {

        Box2 current = TrafficController.CreateBox2(currentCollider.transform, currentCollider);
        TrafficController.DrawBox2(ref current, Color.yellow, 5.0f);
        Vector2[] colliderPos2D = current.CalcVertices();
        colliderPos = new Vector3[4];
        for (int i = 0; i < colliderPos.Length; i++)
        {
            colliderPos[i].x = colliderPos2D[i].x;
            colliderPos[i].z = colliderPos2D[i].y;
            colliderPos[i].y = transform.position.y;
            
        }

        
    }
*/
    void DrawQuad(Rect position, Color color)
    {
        Texture2D texture = new Texture2D(1, 1);
        texture.SetPixel(0, 0, color);
        texture.Apply();
        GUI.skin.box.normal.background = texture;
        GUI.Box(position, GUIContent.none);

    }

  //  public virtual bool Snap()
   // {
//
 //       return true;
  //  }

    public virtual void CreateObject()
    {
    }

    public virtual void PrepareObject()
    {
    }
    public virtual void RevertObject()
    {
    }

	public void SetSpline(OneSpline splineInput)
	{
		currentSpline = splineInput;
	}

	public OneSpline GetSpline()
	{
		return currentSpline;
	}

    public virtual bool isTooFar(float _Dist, float _trafficDist)
    {
        Vector3 final = transform.position;
		final -= MainCamera.instance.transform.position;
        Vector3.Normalize(final);

		float result = Mathf.Acos(Vector3.Dot(final, Math3d.RotateThis(Vector3.forward, -MainCamera.instance.transform.eulerAngles.y, Vector3.zero)) / (final.magnitude * Vector3.forward.magnitude));
        result *= 180.0f / 3.1415926f;

        //if (!trafficUnit)
        //{

		float Dist = Vector3.Distance(transform.position, MainCamera.instance.transform.position);
        //DropText(transform.position, result.ToString("F2") + " " + Dist.ToString("F2"), 1.0f);
        if ((Dist > _Dist && result > 90.0f) && isActive)
        {

            return true;
        }



        //if ((Dist > _Dist && result > 90.0f) || (Main.instance.Proto.myRigidbody.velocity.magnitude < 2.0f && !activeInCamera))
        //       return true;

        return false;
    }
}
