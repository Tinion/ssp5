﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Dest.Math;

[System.Flags]
public enum ShopMode
{
    Small = 0,
    Big = 1,
    SmallToBig = 2,
    BigToSmall = 3,
}
public class Shop : MonoBehaviour
{
    public ShopMode shopMode;
    public List<SmallShopLine> shopSmallLines;
    public List<BigShopLine> shopBigLines;
    public List<BigShopLine> shopBigLinesSource;
    public SmallShopLine smallShopLineSource;
    public BigShopLine bigShopLineSource;

    public Scrollbar scrollBar;
    public ScrollRect scrollRect;
    public RectTransform currentRectTransform;
    public RectTransform content;
    public RectTransform shopButton;

    public CounterTimerF timer;
    public Image decal;
    public Color normal;
    public Color dark;

    public int currentToolTip = -1;
    Module current;

    public AnimationClip OpenClip;
    public Animation currentAnimation;
    public RectTransform button01;
    public RectTransform button02;

    public Sprite buttonLeftSprite;
    public Sprite buttonRightSprite;

    private int shopSizeMax = 495;
    private int shopSize = 495;
    private int shopSizeBigMax = 760;
    private int shopSizeBig = 760;

    public void LoadSmall()
    {
        if (Main.instance == null)
            Main.instance = FindObjectOfType<Main>();

        for (int i=0; i<Main.instance.inventory.items.Length; i++)
        {
            if (!Main.instance.inventory.items[i].defaultModule)
            {
                SmallShopLine current = Instantiate<SmallShopLine>(smallShopLineSource);
                current.bigPicture.sprite = Main.instance.inventory.items[i].bigPicture;
                current.price.text = Main.instance.inventory.items[i].cost.ToString("F0");
                current.transform.parent = content;
                shopSmallLines.Add(current);
            }
        }

        content.sizeDelta = new Vector2(-10.0f, shopSmallLines.Count * 135.0f + 5.0f);
    }


    public Module FindOtherModule(int select)
    {
        for (int j = 0; j < Main.instance.hero.moduleSystem.otherModules.Count; j++)
        {
            if (Main.instance.hero.moduleSystem.otherModules[j].number.Equals(select))
            {

                return Main.instance.hero.moduleSystem.otherModules[j];
                
            }
        }
        return null;
    }

    public Module FindActiveModule(int select)
    {
        for (int j = 0; j < Main.instance.hero.moduleSystem.module.Length; j++)
        {
            if (Main.instance.hero.moduleSystem.module[j] != null)
            {
                if (Main.instance.hero.moduleSystem.module[j].number.Equals(select))
                {

                    return Main.instance.hero.moduleSystem.module[j];

                }
            }
        }
        return null;
    }

    public void UnLoadBig()
    {
       // int count = content.childCount;
        //GameObject[] childs = new GameObject[count];
        for (int i = 0; i < shopBigLines.Count; i++)
        {
            shopBigLines[i].currentRectTransform.SetParent(null);
        }

        shopBigLines.Clear();

        
        

    }
    public void LoadBig()
    {
        if (Main.instance == null)
            Main.instance = FindObjectOfType<Main>();

        for (int i = 0; i < Main.instance.inventory.items.Length; i++)
        {
            if (!Main.instance.inventory.items[i].defaultModule)
            {

                    BigShopLine current = Instantiate<BigShopLine>(bigShopLineSource);

                    Module module = FindOtherModule(i);
                    if (module != null)
                    {
                        module.gameObject.SetActive(true);
                        current.ShowModule(module, false);
                        //current.currentRectTransform.SetParent(content);
                        shopBigLinesSource.Add(current);
                    }
                
            }
        }

        //content.sizeDelta = new Vector2(-10.0f, shopBigLines.Count * (content.GetComponent<GridLayoutGroup>().cellSize.y + 5.0f) + 5.0f);
        //float size = content.sizeDelta.y - currentRectTransform.sizeDelta.y;
        //content.anchoredPosition = new Vector2(-5.0f, -currentRectTransform.sizeDelta.y / 2 + (size/2)* (scrollBar.value*2-1));

        //button02.SetAsLastSibling();
        //button01.SetAsLastSibling();
    }

    public void LoadRandomBig(int count, int offset)
    {
        int countRandom = count + Random.Range(0, offset);
        
        List<BigShopLine> RandomTemp = new List<BigShopLine>();

        for (int i = 0; i < shopBigLinesSource.Count; i++)
        {
            if (!Main.instance.inventory.items[shopBigLinesSource[i].number].defaultModule)
            {
                if (FindOtherModule(shopBigLinesSource[i].number) == null && FindActiveModule(shopBigLinesSource[i].number) == null)
                {
                    RandomTemp.Add(shopBigLinesSource[i]);
                }
            }
        }

        int sizeR;
        if (RandomTemp.Count > countRandom)
         sizeR = countRandom;
        else
            sizeR = RandomTemp.Count;


            for (int i = 0; i < sizeR; i++)
            {
                int currentRandom = Random.Range(0, RandomTemp.Count);
                RandomTemp[currentRandom].currentRectTransform.SetParent(content);
                shopBigLines.Add(RandomTemp[currentRandom]);
                RandomTemp.Remove(RandomTemp[currentRandom]);

           }


            UpdateContexSize();
        button02.SetAsLastSibling();
        button01.SetAsLastSibling();
    }

    public void Open()
    {
        if (shopMode == ShopMode.Small)
        {
            timer.Restore();
            shopMode = ShopMode.SmallToBig;
            decal.rectTransform.SetAsLastSibling();
            button01.SetAsLastSibling();
            button02.SetAsLastSibling();
            shopButton.SetAsLastSibling();
            
            Main.instance.inventory.paramSliders.GetComponent<RectTransform>().SetAsLastSibling();
            Main.instance.inventory.inventory.SetAsLastSibling();
            currentRectTransform.SetAsLastSibling();
            button02.GetComponent<Image>().sprite = buttonRightSprite;
     
        }
        else
        if (shopMode == ShopMode.Big)
        {
            timer.Restore();
            shopMode = ShopMode.BigToSmall;
            decal.rectTransform.SetAsFirstSibling();
            button02.GetComponent<Image>().sprite = buttonLeftSprite;
            button02.SetAsLastSibling();
            button01.SetAsLastSibling();
        }
  
    }


    void UpdateShop()
    {
        if (Input.mousePosition.x > Screen.width - currentRectTransform.sizeDelta.x / 2 + currentRectTransform.anchoredPosition.x && Input.mousePosition.x < Screen.width + currentRectTransform.sizeDelta.x / 2 + currentRectTransform.anchoredPosition.x && Input.mousePosition.y > Screen.height / 2 + currentRectTransform.anchoredPosition.y - currentRectTransform.sizeDelta.y / 2 && Input.mousePosition.y < Screen.height / 2 + currentRectTransform.anchoredPosition.y + currentRectTransform.sizeDelta.y / 2)
        {
            scrollBar.value += Input.GetAxis("Mouse ScrollWheel");

            float y = Input.mousePosition.y - (Screen.height / 2 + currentRectTransform.anchoredPosition.y - currentRectTransform.sizeDelta.y / 2);
            Vector2 normal = scrollRect.normalizedPosition;
            float yAdd = (1 - normal.y) * content.sizeDelta.y + content.anchoredPosition.y;


            yAdd = content.anchoredPosition.y;

            yAdd = content.anchoredPosition.y + content.sizeDelta.y;
            for (int i = 0; i < shopBigLines.Count; i++)
            {
                float koef = content.sizeDelta.y/2 - (content.sizeDelta.y - currentRectTransform.sizeDelta.y);
                float anchored = shopBigLines[i].currentRectTransform.anchoredPosition.y + koef + shopBigLines[0].currentRectTransform.anchoredPosition.y;

                    if (y > anchored + yAdd && y < anchored + shopBigLines[i].currentRectTransform.sizeDelta.y + yAdd)
                    {
                        shopBigLines[i].bigPicture.color = new Color(0.8f, 0.8f, 0.8f, 1.0f);
                        
                        if (currentToolTip!=i)
                        {
                            Main.instance.LoadModule(shopBigLines[i].id);
                            if (current != null)
                                Destroy(current.currentGameObject);
                            current = FindOtherModule(shopBigLines[i].number);
                            Main.instance.hero.moduleSystem.otherModules.Remove(current);
                            Main.instance.hero.moduleSystem.UpdateIcons();
                            currentToolTip = i;
                        }
                        else
                            Main.instance.inventory.moduleTooltip.ShowModuleInfo(current, currentRectTransform, new Vector2(-175.0f, 0.0f));

                        
                        if (Input.GetMouseButtonUp(1))
                        {
                            int cost = int.Parse(shopBigLines[i].price.text);
                            if (Main.instance.moneyCount >= cost)
                            {
                                Main.instance.LoadModule(shopBigLines[i].id);
                                Main.instance.moneyCount -= cost;
                                Main.instance.inventory.moduleSystem.UpdateIcons();
                                Main.instance.UpdateMoney();
                                BigShopLine killed = shopBigLines[i];
                                shopBigLines.Remove(killed);
                                Destroy(killed.gameObject);
                                UpdateContexSize();
                                if (current != null)
                                    Destroy(current.currentGameObject);
                                currentToolTip = -1;                                
                                return;
                            }
                        }
                    }
                    else
                        shopBigLines[i].bigPicture.color = Color.white;

            }



        }
        else
        {
            if (current != null)
                Destroy(current.currentGameObject);
            currentToolTip = -1;

            for (int i = 0; i < shopBigLines.Count; i++)
            {
                shopBigLines[i].bigPicture.color = Color.white;
            }
        }
    }

    private void UpdateContexSize()
    {
        shopSize = 5 + shopBigLines.Count * 70;
        if (shopSize > shopSizeMax)
            shopSize = shopSizeMax;

        shopSizeBig = 5 + shopBigLines.Count * 140;
        if (shopSizeBig > shopSizeBigMax)
            shopSizeBig = shopSizeBigMax;

        if (shopMode== ShopMode.Small)
            currentRectTransform.sizeDelta = new Vector2(currentRectTransform.sizeDelta.x, shopSize);
        else
            currentRectTransform.sizeDelta = new Vector2(currentRectTransform.sizeDelta.x, shopSizeBig);

        content.sizeDelta = new Vector2(-10.0f, shopBigLines.Count * (content.GetComponent<GridLayoutGroup>().cellSize.y + 5.0f) + 5.0f);
        float size = content.sizeDelta.y - currentRectTransform.sizeDelta.y;
        content.localPosition = new Vector3(content.localPosition.x, (size / 2) - scrollBar.value * size, 0);

        shopButton.anchoredPosition = new Vector2(shopButton.anchoredPosition.x, -currentRectTransform.sizeDelta.y / 2 - 130);


    }
    public void Update()
    {
        switch (shopMode)
        {
            case ShopMode.Small:

                UpdateShop();

                break;

            case ShopMode.Big:


                UpdateShop();

                break;
            case ShopMode.SmallToBig:

                if (timer.isEnd(Time.deltaTime))
                {
                    shopMode = ShopMode.Big;
                    decal.color = dark;
                    currentRectTransform.anchoredPosition = new Vector2(-690.0f, 0.0f);
                    currentRectTransform.sizeDelta = new Vector2(1360.0f, shopSizeBig);
                    content.GetComponent<GridLayoutGroup>().cellSize = new Vector2(270.0f, 130.0f);
                    scrollRect.GetComponent<RectTransform>().sizeDelta = new Vector2(1340.0f, 0.0f);
                    
                    content.sizeDelta = new Vector2(-10.0f, shopBigLines.Count * (content.GetComponent<GridLayoutGroup>().cellSize.y + 5.0f) + 5.0f);
 
                    float size = content.sizeDelta.y - currentRectTransform.sizeDelta.y;
                    content.localPosition = new Vector3(content.localPosition.x, (size / 2) - scrollBar.value * size, 0);
                    shopButton.anchoredPosition = new Vector2(-95.0f, -shopSizeBig / 2 - 30);
                }
                else
                {
                    content.GetComponent<GridLayoutGroup>().cellSize = Vector2.Lerp(new Vector2(135.0f, 65.0f), new Vector2(270.0f, 130.0f), Mathf.Clamp01((1.0f - timer.current / timer.max) * 5.0f));
                    currentRectTransform.anchoredPosition = Vector2.Lerp(new Vector2(-95f, -100.0f), new Vector2(-690.0f, 0.0f), Mathf.Clamp01((1.0f - timer.current / timer.max) * 5.0f));
                    currentRectTransform.sizeDelta = Vector2.Lerp(new Vector2(170.0f, shopSize), new Vector2(1360.0f, shopSizeBig), Mathf.Clamp01((1.0f - timer.current / timer.max) * 5.0f));
                    scrollRect.GetComponent<RectTransform>().sizeDelta = Vector2.Lerp(new Vector2(150.0f, 0.0f), new Vector2(1340.0f, 0.0f), Mathf.Clamp01((1.0f - timer.current / timer.max) * 5.0f));
                    decal.color = Color.Lerp(normal, dark, 1.0f-timer.current / timer.max);
                    content.sizeDelta = new Vector2(-10.0f, shopBigLines.Count * (content.GetComponent<GridLayoutGroup>().cellSize.y + 5.0f) + 5.0f);
                    float size = content.sizeDelta.y - currentRectTransform.sizeDelta.y;

                    content.localPosition = new Vector3(content.localPosition.x, (size / 2) - scrollBar.value * size, 0);
                    shopButton.anchoredPosition = Vector2.Lerp(new Vector2(-95.0f,-shopSize / 2 - 130), new Vector2(-95.0f, -shopSizeBig / 2 - 30), Mathf.Clamp01((1.0f - timer.current / timer.max) * 5.0f));

                    
                }
                
            break;

            case ShopMode.BigToSmall:

            if (timer.isEnd(Time.deltaTime))
                {
                    shopMode = ShopMode.Small;
                    decal.color = normal;
                    currentRectTransform.anchoredPosition = new Vector2(-95f, -100.0f);
                    currentRectTransform.sizeDelta = new Vector2(170.0f, shopSize);
                    content.GetComponent<GridLayoutGroup>().cellSize = new Vector2(135.0f, 65.0f);
                    scrollRect.GetComponent<RectTransform>().sizeDelta = new Vector2(150.0f, 0.0f);
                    content.sizeDelta = new Vector2(-10.0f, shopBigLines.Count * (content.GetComponent<GridLayoutGroup>().cellSize.y + 5.0f) + 5.0f);
                    float size = content.sizeDelta.y - currentRectTransform.sizeDelta.y;
                    content.localPosition = new Vector3(content.localPosition.x, (size / 2) - scrollBar.value * size, 0);
                    shopButton.anchoredPosition = new Vector2(-95.0f, -shopSize / 2 - 130);
                    //Debug.Log((size / 2) + " " + scrollBar.value * size);
                }
                else
                {
                    content.GetComponent<GridLayoutGroup>().cellSize = Vector2.Lerp(new Vector2(270.0f, 130.0f), new Vector2(135.0f, 65.0f), Mathf.Clamp01((1.0f - timer.current / timer.max) * 5.0f));
                    currentRectTransform.anchoredPosition = Vector2.Lerp(new Vector2(-690.0f, 0.0f), new Vector2(-95f, -100.0f), Mathf.Clamp01((1.0f - timer.current / timer.max) * 5.0f));
                    currentRectTransform.sizeDelta = Vector2.Lerp(new Vector2(1360.0f, shopSizeBig), new Vector2(170.0f, shopSize), Mathf.Clamp01((1.0f - timer.current / timer.max) * 5.0f));
                    scrollRect.GetComponent<RectTransform>().sizeDelta = Vector2.Lerp(new Vector2(1340.0f, 0.0f), new Vector2(150.0f, 0.0f), Mathf.Clamp01((1.0f - timer.current / timer.max) * 5.0f));
                    decal.color = Color.Lerp(dark, normal, 1.0f-timer.current / timer.max);
                    content.sizeDelta = new Vector2(-10.0f, shopBigLines.Count * (content.GetComponent<GridLayoutGroup>().cellSize.y + 5.0f) + 5.0f);
                    float size = content.sizeDelta.y - currentRectTransform.sizeDelta.y;

                    content.localPosition = new Vector3(content.localPosition.x, (size / 2) - scrollBar.value * size, 0);
                    shopButton.anchoredPosition = Vector2.Lerp(new Vector2(-95.0f, -shopSizeBig / 2 - 30), new Vector2(-95.0f, -shopSize / 2 - 130), Mathf.Clamp01((1.0f - timer.current / timer.max) * 5.0f));

                    
                }
                
            break;

        }
        
    }
    public void Close()
    {
        
    }
}
