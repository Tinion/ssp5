﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Corpse : MonoBehaviour {

	public Vector3 center;
	public float force;
	public float radius;
	public Rigidbody[] parts;


	public void Start()
	{
		for (int i = 0; i<parts.Length; i++) 
		{
			parts [i].AddExplosionForce (force, Vector3.Normalize (parts [i].centerOfMass - center), radius);
		}
	}
}
