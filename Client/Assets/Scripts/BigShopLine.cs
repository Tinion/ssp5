﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BigShopLine : ModuleTooltip
{
    public string id;
    public bool isSold = false;
    public int number;

    public override void ShowModule(Module module, bool isToolTip)
    {
        base.ShowModule(module, false);
		price.text = Main.instance.inventory.items[module.number].cost.ToString("F0");
		id = Main.instance.inventory.items[module.number].id;
        number = module.number;
    }

    public void UpdateLine()
    {

    }
   
	
}
