﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

[System.Flags]
public enum ScoreMode
{
    Begin = 0,
    Calc01Time = 1,
    Calc02Path = 2,
    Calc03Points = 3,
    Calc04Damaged = 4,
    Calc05Ruined = 5,
    Repaired = 6,
    Damaged = 7,
    Destroyed = 8,
    EndResult = 9,

}

[System.Flags]
public enum Act
{
    Wait = 0,
    Show = 1,

}
public class ScoreUI : MonoBehaviour {

    public ScoreMode mode;
    public Act act;

    public CounterTimerF levelTimer;
    public CounterTimerF TabellenTimer;

    public Text LabelStart;

    public Text Label01;
    public Text Label02;
    public Text Label03;
    public Text Label04;
    public Text Label05;
    public Text ResultLabel;
    public Text RepairLabelText;
    public Text DamagedLabelText;
    public Text DestroyedLabelText;

    public Text Label01Bonus;
    public Text Label02Bonus;
    public Text Label03Bonus;
    public Text Label04Bonus;
    public Text Label05Bonus;
    public Text ResultLabelBonus;
    public Text RepairLabelBonus;
    public Text DamagedLabelBonus;
    public Text DestroyedLabelBonus;

    public float reward = 200.0f;
    public float timeMulti;
    public float coinMulti;
    public float damageMulti;
    public float destroyMulti;
    
    public List<string> destroyedModules;
    public Image depth;

    private float diff;

    private float bonus01;
    private float bonus02;
    private float bonus03;
    private float bonus04;
    private float bonus05;
    private float result;

    public RectTransform button;

    public Slider TimeSliderScore;
    public Slider BlockSliderScore;
    public Slider TimeExSliderScore;

    public DurabilityInfo durabilityScore;
    public BonusElements bonusElements;

    public void StartAnim()
    {
        levelEnd();
    }

    public void levelEnd()
    {
        float Time = Main.instance.generator.GeneratorTime;
        float TimeEx = Time / Main.instance.generator.MaxTime;

        TimeSliderScore.value = Main.instance.generator.timeSlider.value;
        TimeExSliderScore.value = Main.instance.generator.timeExSlider.value;
        BlockSliderScore.value = Main.instance.generator.blockSlider.value;

        durabilityScore.CreateDurabilityInfo();
        bonusElements.CopyFrom(Main.instance.bonusElements);
        Main.instance.bonusElements.Restore();

        levelTimer.Restore();
        TabellenTimer.Restore();
        diff = Main.instance.generator.timeSlider.value;

        depth.color = new Color(0.0f, 0.0f, 0.0f, 0.0f);

        button.gameObject.SetActive(false);

        act = Act.Show;
        mode = ScoreMode.Begin;

		CalcPointsResult ();
    }

    public void CalcReward()
    {

        if (TabellenTimer.isEnd(Time.deltaTime))
        {
            levelTimer.Restore();
            TabellenTimer.current = TabellenTimer.max * diff;
            act = Act.Wait;

            bonus01 = reward;    
            result = bonus01;
            Label01Bonus.text = bonus01.ToString("F0");
            ResultLabelBonus.text = result.ToString("F0");

        }
        else
        {
            bonus01 = Mathf.Lerp(0, reward, 1.0f - TabellenTimer.current / TabellenTimer.max);
            result = bonus01;
            
            Label01Bonus.text = bonus01.ToString("F0");
            ResultLabelBonus.text = result.ToString("F0"); 


        }

    }
    public void Calc01Time()
    {

        if (TabellenTimer.isEnd(Time.deltaTime))
        {
            Main.instance.generator.timeSlider.value = 0;
            Main.instance.generator.blockSlider.value = 1.0f - diff;
            TabellenTimer.current = TabellenTimer.max * (1.0f - diff);
            act = Act.Wait;
        }
        else
        {
            Label01.enabled = true;
            Label01Bonus.enabled = true;

            TimeSliderScore.value = TabellenTimer.current / TabellenTimer.max;
            BlockSliderScore.value = 1.0f - diff + TimeSliderScore.value;


        }

    }

    public void Calc02Path()
    {
        if (TabellenTimer.isEnd(Time.deltaTime))
        {
            float bonusOrigin = (Main.instance.generator.MaxTime * (1.0f - diff));
            bonus02 = bonusOrigin * timeMulti;
            Label02Bonus.text = bonusOrigin.ToString("F0") + " x " + timeMulti + " = " + bonus02.ToString("F0");
            result = bonus01 + bonus02;
            ResultLabelBonus.text = result.ToString("F0");
            levelTimer.Restore();
            TabellenTimer.Restore();
            BlockSliderScore.value = 0.0f;
            diff = bonusElements.coinsCount;
            act = Act.Wait;


        }
        else
        {
            Label02.enabled = true;
            Label02Bonus.enabled = true;
           // Debug.Log(TabellenTimer.current / TabellenTimer.max);
            BlockSliderScore.value = TabellenTimer.current / TabellenTimer.max;

            float bonusOrigin = (Main.instance.generator.MaxTime * (1.0f - diff)) * (1.0f - TabellenTimer.current / TabellenTimer.max);
            bonus02 = bonusOrigin * timeMulti;

            Label02Bonus.text = bonusOrigin.ToString("F0") + " x " + timeMulti + " = " + bonus02.ToString("F0");
            result = bonus01 + bonus02;
            ResultLabelBonus.text = result.ToString("F0");

            depth.color = Color.Lerp(new Color(0.0f, 0.0f, 0.0f, 0.5f), new Color(0.0f, 0.0f, 0.0f, 0.0f), TabellenTimer.current / TabellenTimer.max);
        }


    }

    public void Calc03Points()
    {
        if (TabellenTimer.isEnd(Time.deltaTime))
        {
            bonus03 = diff * coinMulti;
            Label03Bonus.text = diff.ToString("F0") + " x " + coinMulti + " = " + bonus03.ToString("F0");

            result = bonus01 + bonus02 + bonus03;
            ResultLabelBonus.text = result.ToString("F0");
            levelTimer.Restore();
            TabellenTimer.Restore();

            bonusElements.coinsCount = 0;
            bonusElements.coins.text = bonusElements.coinsCount.ToString();

            diff = bonusElements.damagedCount;

            act = Act.Wait;
        }
        else
        {
            Label03.enabled = true;
            Label03Bonus.enabled = true;

            bonusElements.coinsCount = Mathf.CeilToInt(TabellenTimer.current / TabellenTimer.max * diff);
            bonusElements.coins.text = bonusElements.coinsCount.ToString();
            float bonusOriginal = (1.0f - TabellenTimer.current / TabellenTimer.max) * diff;
            bonus03 = bonusOriginal * coinMulti;
            Label03Bonus.text = bonusOriginal.ToString("F0") + " x " + coinMulti + " = " + bonus03.ToString("F0");

            result = bonus01 + bonus02 + bonus03;
            ResultLabelBonus.text = result.ToString("F0");
            
        }
    }

    public void Calc04Damaged()
    {
        if (TabellenTimer.isEnd(Time.deltaTime))
        {
            bonus04 = diff * damageMulti;
            Label04Bonus.text = diff.ToString("F0") + " x " + damageMulti + " = " + bonus04.ToString("F0");

           result = bonus01 + bonus02 + bonus03 + bonus04;
            ResultLabelBonus.text = result.ToString("F0");
            levelTimer.Restore();
            TabellenTimer.Restore();

            bonusElements.damagedCount = 0;
            bonusElements.damaged.text = bonusElements.damagedCount.ToString();

            Main.instance.bonusElements.damagedCount = 0;
            diff = bonusElements.ruinedCount;

            act = Act.Wait;
        }
        else
        {
            Label04.enabled = true;
            Label04Bonus.enabled = true;

            bonusElements.damagedCount = Mathf.CeilToInt(TabellenTimer.current / TabellenTimer.max * diff);
            bonusElements.damaged.text = bonusElements.damagedCount.ToString();
            float bonusOriginal = (1.0f - TabellenTimer.current / TabellenTimer.max) * diff;
            bonus04 = bonusOriginal * damageMulti;

            Label04Bonus.text = bonusOriginal.ToString("F0") + " x " + damageMulti + " = " + bonus04.ToString("F0");

            result = bonus01 + bonus02 + bonus03 + bonus04;
            ResultLabelBonus.text = result.ToString("F0");

        }
    }

    public void Calc05Ruined()
    {
        if (TabellenTimer.isEnd(Time.deltaTime))
        {
            bonus05 = diff * destroyMulti;
            Label05Bonus.text = diff.ToString("F0") + " x " + destroyMulti + " = " + bonus05.ToString("F0");

            result = bonus01 + bonus02 + bonus03 + bonus04 + bonus05;
            ResultLabelBonus.text = result.ToString("F0");
            levelTimer.Restore();
            TabellenTimer.Restore();

            bonusElements.ruinedCount = 0;
            bonusElements.ruined.text = bonusElements.ruinedCount.ToString();

            diff = Main.instance.repairCount;

            Main.instance.moneyCount += Mathf.CeilToInt(result);
            Main.instance.UpdateMoney();

            act = Act.Wait;
        }
        else
        {
            Label05.enabled = true;
            Label05Bonus.enabled = true;

            bonusElements.ruinedCount = Mathf.CeilToInt(TabellenTimer.current / TabellenTimer.max * diff);
            bonusElements.ruined.text = bonusElements.ruinedCount.ToString();

            
            float bonusOriginal = (1.0f - TabellenTimer.current / TabellenTimer.max) * diff;
            bonus05 = bonusOriginal * destroyMulti;
            Label05Bonus.text = bonusOriginal.ToString("F0") + " x " + destroyMulti + " = " + bonus05.ToString("F0");

            result = bonus01 + bonus02 + bonus03 + bonus04 + bonus05;
            ResultLabelBonus.text = result.ToString("F0");

        }
    }

    public void RepairLabel()
    {
        if (TabellenTimer.isEnd(Time.deltaTime))
        {
            RepairLabelBonus.text = diff.ToString("F0");

            levelTimer.Restore();
            TabellenTimer.Restore();

            act = Act.Wait;
        }
        else
        {
            RepairLabelText.enabled = true;
            RepairLabelBonus.enabled = true;

            float repair = Mathf.CeilToInt((1.0f-TabellenTimer.current / TabellenTimer.max) * diff);
            RepairLabelBonus.text = repair.ToString("F0");


        }
    }


    public void DestroyedLabel()
    {
        if (TabellenTimer.isEnd(Time.deltaTime))
        {

            levelTimer.Restore();
            TabellenTimer.Restore();

            act = Act.Wait;

            destroyedModules.Clear();
        }
        else
        {
            if (destroyedModules.Count > 0)
            {
                DestroyedLabelText.enabled = true;
                DestroyedLabelBonus.enabled = true;
            }
        }

        
    }

    public void CalcPointsResult()
    {

        button.gameObject.SetActive(true);
    }

    void Update()
    {
        switch (mode)
        {

            case ScoreMode.Begin:

                if (act == Act.Wait)
                {
                    if (levelTimer.isEnd(Time.deltaTime))
                    {
                        mode = ScoreMode.Calc01Time;
                        act = Act.Show;
                    }
                }
                else
                    CalcReward();

                break;
            case ScoreMode.Calc01Time:

                if (act == Act.Wait)
                {
                    if (levelTimer.isEnd(Time.deltaTime))
                    {
                        mode = ScoreMode.Calc02Path;
                        act = Act.Show;
                    }
                }
                else
                    Calc01Time();

                break;
            case ScoreMode.Calc02Path:

                if (act == Act.Wait)
                {
                    if (levelTimer.isEnd(Time.deltaTime))
                    {
                        mode = ScoreMode.Calc03Points;
                        act = Act.Show;
                    }
                }
                else
                    Calc02Path();
                
                break;
            case ScoreMode.Calc03Points:

                if (act == Act.Wait)
                {
                    if (levelTimer.isEnd(Time.deltaTime))
                    {
                        mode = ScoreMode.Calc04Damaged;
                        act = Act.Show;
                    }
                }
                else
                    Calc03Points();

                break;
            case ScoreMode.Calc04Damaged:

                if (act == Act.Wait)
                {
                    if (levelTimer.isEnd(Time.deltaTime))
                    {
                        mode = ScoreMode.Calc05Ruined;
                        act = Act.Show;
                    }
                }
                else
                    Calc04Damaged();

                break;
            case ScoreMode.Calc05Ruined:

                if (act == Act.Wait)
                {
                    if (levelTimer.isEnd(Time.deltaTime))
                    {
                        mode = ScoreMode.Repaired;
                        act = Act.Show;
                        
                    }
                }
                else
                    Calc05Ruined();

                break;
            case ScoreMode.Repaired:

                if (act == Act.Wait)
                {
                    if (levelTimer.isEnd(Time.deltaTime))
                    {
                        mode = ScoreMode.Destroyed;
                        act = Act.Show;
                        

                        DestroyedLabelBonus.text = "";
                        for (int i = 0; i < destroyedModules.Count; i++)
                        {
                            DestroyedLabelBonus.text += destroyedModules[i] + "\n";
                        }
                    }
                }
                else
                    RepairLabel();

                break;
            case ScoreMode.Destroyed:

                if (act == Act.Wait)
                {
                    if (levelTimer.isEnd(Time.deltaTime))
                    {
                        mode = ScoreMode.EndResult;
                        act = Act.Show;
                    }
                }
                else
                    DestroyedLabel();

                break;
		case ScoreMode.EndResult:
			Button button2 = button.GetComponent<Button> ();
			ColorBlock cb = button2.colors;
			cb.normalColor = Color.yellow;
			button2.colors = cb;
                break;
        }
    }

    public void Reset()
    {
        Label01Bonus.text = "0";
        Label02Bonus.text = "0";
        Label03Bonus.text = "0";
        Label04Bonus.text = "0";
        Label05Bonus.text = "0";
        ResultLabelBonus.text = "0";
        RepairLabelBonus.text = "0";
        DestroyedLabelBonus.text = "";

        Label01.enabled = false;
        Label02.enabled = false;
        Label03.enabled = false;
        Label04.enabled = false;
        Label05.enabled = false;
        Label01Bonus.enabled = false;
        Label02Bonus.enabled = false;
        Label03Bonus.enabled = false;
        Label04Bonus.enabled = false;
        Label05Bonus.enabled = false;
        RepairLabelText.enabled = false;
        RepairLabelBonus.enabled = false;
        DestroyedLabelText.enabled = false;
        DestroyedLabelBonus.enabled = false;
    }

    
}
