﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Dest.Math;

public class ParamSliders : MonoBehaviour {


    public Slider massSlider;
    public Text massValue;

    public Text minMassText;
    public Text averMassText;
    public Text maxMassText;

    public Color max;
    public Color average;
    public Color min;

    public Color maxEnergy;
    public Color minEnergy;

    public float minMass;
    public float averMass;
    public float maxMass;
    public Image fillMass;

    public Slider energySlider;
    public Text energyValue;
    public Image fillEnergy;

    public Text massDiff;
    public Text energyDiff;

    public SpeedImage speedImage;

    public void UpdateSliders()
    {
		massValue.text = "Масса " + Main.instance.hero.vehicle.currentRigidbody.mass.ToString("F0") + " кг";
		if (Main.instance.hero.vehicle.currentRigidbody.mass < averMass)
			massSlider.value = ((Main.instance.hero.vehicle.currentRigidbody.mass - minMass) / (averMass - minMass)) / 2.0f;
        else
			massSlider.value = ((Main.instance.hero.vehicle.currentRigidbody.mass - averMass) / (maxMass - averMass)) / 2.0f + 0.5f;
        if (massSlider.value > 0.5f)
            fillMass.color = Color.Lerp(average, min, (massSlider.value - 0.5f) * 2.0f);
        else
            fillMass.color = max;

		massDiff.text = "(+" + Mathf.Clamp(maxMass - Main.instance.hero.vehicle.currentRigidbody.mass, 0, 9000.0f) + ")";

        minMassText.text = minMass.ToString("0");
        averMassText.text = averMass.ToString("0");
        maxMassText.text = maxMass.ToString("0");

        energyValue.text = Main.instance.hero.energyLimit.current.ToString("0") + "/" + Main.instance.hero.energyLimit.max.ToString("0");
        energySlider.value = Main.instance.hero.energyLimit.current / Main.instance.hero.energyLimit.max;
        if (energySlider.value > 0.5f)
            fillEnergy.color = Color.Lerp(maxEnergy, minEnergy, (energySlider.value - 0.5f) * 2.0f);
        else
            fillEnergy.color = maxEnergy;

        energyDiff.text = "(+" + Mathf.Clamp(Main.instance.hero.energyLimit.max - Main.instance.hero.energyLimit.current, 0, 9000.0f) + ")";

        speedImage.Set(massSlider.value);
    }


}
