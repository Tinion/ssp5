﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class WheelData
{
    public Transform wheelTransform;
    public WheelCollider collider;
    public Vector3 colliderLocalPosition;
    public float radius;
    public WheelHit hit;
    public float downforceRatio;
    public float combinedTireSlip;

    public Vector3 velocity = Vector3.zero;
    public Vector2 localVelocity = Vector2.zero;
    public Vector2 tireSlip = Vector2.zero;
    public float angularVelocity = 0.0f;
}

[System.Serializable]
public class TireFxData
{
    // Tire marks

    public TireMarksRenderer lastRenderer;
    public int lastMarksIndex = -1;
    public float marksDelta = 0.0f;

    // Tire particles

}

public class Vehicle : Unit 
{
    public AnimationCurve curveAccel;
    public AnimationCurve curveBoost;

    public Vector3 currentSplineDirection;
	public  float worldAngle;

    public float steerCurrent;
    public float currentSteerClamp;

    public float currentBrake;
    public float currentBoost;
    public float currentAccel;

    public float averageRpm;
    public float currentRpm;

    public Vector3 size;

    public WheelCollider[] wheelsColliderForward;
    public WheelCollider[] wheelsColliderBack;

    public Transform[] wheelsTransformForward;
    public Transform[] wheelsTransformBack;

    [Range(0, 20)]
    public float naturalFrequency = 10;

    [Range(0, 3)]
    public float dampingRatio = 0.8f;

    [Range(-1, 1)]
    public float forceShift = 0.03f;

    public bool setSuspensionDistance = true;

    public WheelData[] wheels;

    public CounterTimerF impactTimer;

    public float tireWidth = 0.2f;
    public float minSlip = 1.0f;
    public float maxSlip = 5.0f;

    [Header("Tire marks")]
    [Range(0, 2)]
    public float intensity = 1.0f;
    public float updateInterval = 0.02f;
	// Use this for initialization

    public TireFxData[] fxData;

    public SideTriangle GetSideTriangle(Vector3 point, Transform _transform)
    {
        Vector3 damageVector = _transform.InverseTransformPoint(point);
        if (damageVector.z > 0)
            damageVector.z /= size.y;
        else
            damageVector.z /= size.z;

        damageVector.x /= size.x;

        if (Mathf.Abs(damageVector.z) > Mathf.Abs(damageVector.x))
        {
            if (damageVector.z > 0)
                return SideTriangle.Top;
            else
                return SideTriangle.Back;
        }
        else
        {
            if (damageVector.x > 0)
                return SideTriangle.Right;
            else
                return SideTriangle.Left;
        }


    }

    //float referenceDownforce =    computeExtendedTireData ? (m_rigidbody.mass * Physics.gravity.magnitude) / m_wheelData.Length : 1.0f;
    public void SetupWheels()
    {
        fxData = new TireFxData[2];
        fxData[0] = new TireFxData();
        fxData[1] = new TireFxData();

        wheels = new WheelData[wheelsColliderForward.Length + wheelsColliderBack.Length];
        for (int i = 0; i < wheelsColliderForward.Length; i++)
        {
            if (wheelsTransformForward.Length>0)
            wheels[i] = SetupWheel(wheelsTransformForward[i], wheelsColliderForward[i]);
            else
            wheels[i] = SetupWheel(wheelsColliderForward[i].transform, wheelsColliderForward[i]);
        }
        for (int i = 0; i < wheelsColliderBack.Length; i++)
        {
            

            if (wheelsTransformBack.Length > 0)
            wheels[i + 2] = SetupWheel(wheelsTransformBack[i], wheelsColliderBack[i]);
            else
            wheels[i + 2] = SetupWheel(wheelsColliderBack[i].transform, wheelsColliderBack[i]);
        }
    }

    private WheelData SetupWheel(Transform wheel, WheelCollider collider)
    {
        WheelData result = new WheelData();
        result.wheelTransform = wheel;
        result.collider = collider;
        result.colliderLocalPosition = collider.transform.localPosition;
        result.radius = collider.radius;
        return result;
    }

    
    public void UpdateWheels()
    {
        float delta = Time.fixedDeltaTime;

        foreach (WheelData wheel in wheels)
        {
            Vector3 position;
            Quaternion rotation;
            wheel.collider.GetWorldPose(out position, out rotation);
            wheel.wheelTransform.position = position;
            wheel.wheelTransform.rotation = rotation;
            wheel.angularVelocity = (wheel.localVelocity.y + wheel.hit.sidewaysSlip) / wheel.collider.radius;
            wheel.tireSlip.x = wheel.localVelocity.x;
            wheel.tireSlip.y = wheel.localVelocity.y - wheel.angularVelocity * wheel.collider.radius;
            ComputeExtendedTireData(wheel, 1.0f);

        }

        

    }

    static float ComputeCombinedSlip(Vector2 localVelocity, Vector2 tireSlip)
    {
        
        float h = localVelocity.magnitude;

        if (h > 0.01f)
        {
            float sx = tireSlip.x * localVelocity.x / h;
            float sy = tireSlip.y;
            return Mathf.Sqrt(sx * sx + sy * sy);
        }
        else
        {
            return tireSlip.magnitude;
        }
    }


    void ComputeExtendedTireData(WheelData wd, float referenceDownforce)
    {
        wd.combinedTireSlip = ComputeCombinedSlip(wd.localVelocity, wd.tireSlip);
        wd.downforceRatio = wd.hit.force / referenceDownforce;
    }
    public virtual void ApplyColliders(float inputSteer, float motorTorque, float brakeTorque)
    {
        //Debug.Log("FF");
        foreach (WheelCollider collider in wheelsColliderForward)
        {
            collider.steerAngle = inputSteer;
        }

        foreach (WheelCollider collider in wheelsColliderBack)
            collider.steerAngle = 0;

        for (int i = 2; i < wheels.Length; i++)
        {
            wheels[i].collider.motorTorque = motorTorque;
            wheels[i].collider.brakeTorque = brakeTorque;

        }
    }

    public override void SetFriction(float _stiffness)
    {
        foreach (WheelData wheel in wheels)
        {
            WheelFrictionCurve forwardFrictionCurve = wheel.collider.forwardFriction;
            WheelFrictionCurve sidewaysFrictionCurve = wheel.collider.sidewaysFriction; // get a copy
            forwardFrictionCurve.stiffness = _stiffness;
            sidewaysFrictionCurve.stiffness = _stiffness;
            wheel.collider.forwardFriction = forwardFrictionCurve;
            wheel.collider.sidewaysFriction = sidewaysFrictionCurve;
        }
    }

    public bool Snap()
    {

        if (wheels.Length==0)
            SetupWheels();

        Vector3 snapPosition = Vector3.zero;
        Vector3 snapRotation = Vector3.zero;

        Vector3[] ground = new Vector3[4];
        float[] diff = new float[4];

        RaycastHit hit;
        

        //Debug.DrawLine(ControllerPos[0], ControllerPos[1], Color.red, 10.0f);
        // //Debug.DrawLine(ControllerPos[2],ControllerPos[3], Color.red, 10.0f);

        for (int i = 0; i < 4; i++)
        {
            Vector3 colliderPos = wheels[i].collider.transform.position;
            //Locators.CreateLocator(colliderPos, "colliderPos " + i.ToString());

            if (!Math3d.RayCastGroundDown(colliderPos, out hit))
            {
                Debug.Log("FUCK");
                return false;
            }
            ground[i] = hit.point;
            diff[i] = colliderPos.y - ground[i].y - wheels[i].radius;
            //    Debug.Log(ControllerPos[i].y + " " + ground[i].y + " " + radius);
            //    
            ground[i] = transform.InverseTransformPoint(ground[i]);



        }

        // //Debug.DrawLine(ground[0], ground[1], Color.blue, 10.0f);
        // //Debug.DrawLine(ground[2], ground[3], Color.blue, 10.0f);

        float resultDiff = (diff[0] + diff[1] + diff[2] + diff[3]) / 4;

        //  Debug.Log("Ground 0: " + ground[0] + "Diff 0: " + diff[0]);
        //  Debug.Log("Ground 1: " + ground[1] + "Diff 1: " + diff[1]);
        //  Debug.Log("Ground 2: " + ground[2] + "Diff 2: " + diff[2]);
        //  Debug.Log("Ground 3: " + ground[3] + "Diff 3: " + diff[3]);

        Vector3 LeftDir = Vector3.Normalize(ground[0] - ground[2]);
        Vector3 RightDir = Vector3.Normalize(ground[1] - ground[3]);
        Vector3 TopDir = Vector3.Normalize(ground[1] - ground[0]);
        Vector3 BackDir = Vector3.Normalize(ground[3] - ground[2]);
        LeftDir.x = 0;
        RightDir.x = 0;
        TopDir.z = 0;
        BackDir.z = 0;

        //  Debug.Log("LeftDir: " + LeftDir);
        //  Debug.Log("RightDir: " + RightDir);
        //  Debug.Log("TopDir: " + TopDir);
        // Debug.Log("BackDir: " + BackDir);

        float LeftAngle = Math3d.GetAngle(LeftDir, Vector3.forward);
        float RightAngle = Math3d.GetAngle(RightDir, Vector3.forward);
        float TopAngle = Math3d.GetAngle(TopDir, Vector3.right);
        float BackAngle = Math3d.GetAngle(BackDir, Vector3.right);

        // Debug.Log("LeftAngle: " + LeftAngle);
        // Debug.Log("RightAngle: " + RightAngle);
        // Debug.Log("TopAngle: " + TopAngle);
        // Debug.Log("BackAngle: " + BackAngle);

        LeftAngle *= diff[0] > diff[2] ? 1 : -1;
        RightAngle *= diff[1] > diff[3] ? 1 : -1;
        TopAngle *= diff[0] > diff[1] ? 1 : -1;
        BackAngle *= diff[2] > diff[3] ? 1 : -1;

        // Debug.Log("LeftAngleD: " + LeftAngle);
        //Debug.Log("RightAngleD: " + RightAngle);
        //Debug.Log("TopAngleD: " + TopAngle);
        //Debug.Log("BackAngleD: " + BackAngle);



        snapRotation = new Vector3(Mathf.LerpAngle(LeftAngle, RightAngle, 0.5f), transform.rotation.eulerAngles.y, Mathf.LerpAngle(TopAngle, BackAngle, 0.5f));
        // Debug.Log("Rotation: " + Rotation);
        snapPosition = new Vector3(transform.position.x, transform.position.y - resultDiff, transform.position.z);

        transform.position = snapPosition;
        transform.rotation = Quaternion.Euler(snapRotation);

        return true;
    }



//#if UNITY_EDITOR
 //   [ContextMenu("Apply WheelCollector")]
    public void ApplyWheelCollector()
    {
        naturalFrequency = 9.0f;
        dampingRatio = 0.75f;
        forceShift = 0.03f;

        Vehicle current = this;

        Collider col = current.currentCollider;
        Bounds bounds = col.bounds;
        float length = bounds.extents.z;

        Transform COM = centerOfMass;
        Vector3 comLP = COM.localPosition;
        comLP.y = current.transform.InverseTransformPoint(current.wheelsColliderBack[0].transform.position).y;
        comLP.x = 0;
        comLP.z = 0.28f * length;
        COM.localPosition = comLP;

        current.currentRigidbody.centerOfMass = COM.localPosition;

        float radius = current.wheelsColliderBack[0].radius;
        //float speedCoeff = 0.17f / radius;

        //current.currentAccel = Mathf.Round(1000.0f * speedCoeff);
        //current.currentBoost = Mathf.Round(650.0f * speedCoeff);
        //current.currentBrake = Mathf.Round(650.0f * speedCoeff);
        //current.steerCurrent = 12.0f;
        //current.currentSteerClamp = 45.0f;
        //current.averageRpm = Mathf.Round(1000.0f * speedCoeff); 

        float frontStiff = 6.0f;
        float sideStiff = 5.0f;

        WheelFrictionCurve front = new WheelFrictionCurve();
        front.extremumSlip = 0.6f;
        front.extremumValue = 1.0f;
        front.asymptoteSlip = 0.8f;
        front.asymptoteValue = 0.75f;
        front.stiffness = frontStiff;

        WheelFrictionCurve back = new WheelFrictionCurve();
        front.extremumSlip = 0.4f;
        front.extremumValue = 1.0f;
        front.asymptoteSlip = 0.8f;
        front.asymptoteValue = 0.75f;
        front.stiffness = frontStiff;

        WheelFrictionCurve sideFront = new WheelFrictionCurve();
        sideFront.extremumSlip = 0.35f;
        sideFront.extremumValue = 1.0f;
        sideFront.asymptoteSlip = 0.5f;
        sideFront.asymptoteValue = 0.75f;
        sideFront.stiffness = sideStiff;

        WheelFrictionCurve sideBack = new WheelFrictionCurve();
        sideBack.extremumSlip = 0.15f;
        sideBack.extremumValue = 1.0f;
        sideBack.asymptoteSlip = 0.5f;
        sideBack.asymptoteValue = 0.75f;
        sideBack.stiffness = sideStiff;

        foreach (WheelCollider wc in current.wheelsColliderForward)
        {
            wc.mass = 0.22f * current.currentRigidbody.mass;
            wc.forwardFriction = front;
            wc.sidewaysFriction = sideFront;
        }

        foreach (WheelCollider wc in current.wheelsColliderBack)
        {
            wc.mass = 0.22f * current.currentRigidbody.mass;
            wc.forwardFriction = front;
            wc.sidewaysFriction = sideBack;
        }

        for (int i = 0; i < current.wheels.Length; i++)
        {
            JointSpring spring = current.wheels[i].collider.suspensionSpring;

            spring.spring = Mathf.Round(Mathf.Pow(Mathf.Sqrt(current.wheels[i].collider.sprungMass) * naturalFrequency, 2));
            spring.damper = Mathf.Round(2 * dampingRatio * Mathf.Sqrt(spring.spring * current.wheels[i].collider.sprungMass));

            current.wheels[i].collider.suspensionSpring = spring;

            Vector3 wheelRelativeBody = current.transform.InverseTransformPoint(current.wheels[i].collider.transform.position);
            float distance = (COM.localPosition.y - wheelRelativeBody.y + current.wheels[i].collider.radius);

            current.wheels[i].collider.forceAppPointDistance = distance - forceShift;


            if (spring.targetPosition > 0 && setSuspensionDistance)
                current.wheels[i].collider.suspensionDistance = current.wheels[i].collider.sprungMass * Physics.gravity.magnitude / (spring.targetPosition * spring.spring);
        }
    }
//#endif

	void OnDrawGizmos()
	{
		Gizmos.color = Color.blue;
		Gizmos.DrawLine (transform.position, transform.position + currentSplineDirection * 3);

		Gizmos.color = Color.red;
		Gizmos.DrawLine (transform.position, transform.position + transform.forward * 3);
	}
    

    protected void UpdateTireMarks(WheelData wheelData, TireFxData fxData)
    {
        // If we are already drawing marks to this wheel, wait before updating.

        if (fxData.lastMarksIndex != -1 && wheelData.collider.isGrounded && fxData.marksDelta < 0.02f)
        {
            fxData.marksDelta += Time.deltaTime;
            return;
        }

        wheelData.collider.GetGroundHit(out wheelData.hit);

        Vector3 wheelV = currentRigidbody.GetPointVelocity(wheelData.hit.point);

		wheelData.velocity = wheelV - Vector3.Project(wheelV, wheelData.hit.normal);
		wheelData.localVelocity.y = Vector3.Dot(wheelData.hit.forwardDir, wheelData.velocity);
        wheelData.localVelocity.x = Vector3.Dot(wheelData.hit.sidewaysDir, wheelData.velocity);
        // deltaT = time since last mark for this wheel

        float deltaT = fxData.marksDelta;
        if (deltaT == 0.0f)
            deltaT = Time.deltaTime;
        fxData.marksDelta = 0.0f;

        // Verify: Should we put marks?
        // - Grounded
        // - Contacted object has not a rigidbody (assumed to be static)

        if (!wheelData.collider.isGrounded)
        {
            fxData.lastMarksIndex = -1;
            return;
        }

       

        // Have we changed renderer? If so, start a new tread

        TireMarksRenderer marksRenderer = Main.instance.currentTireRenderer;

        if (marksRenderer != fxData.lastRenderer)
        {
            fxData.lastRenderer = marksRenderer;
            fxData.lastMarksIndex = -1;
        }

        if (marksRenderer != null)
        {
            float pressureRatio = Mathf.Clamp01(1.0f * wheelData.downforceRatio * 0.5f);
            
            float skidRatio = 0;// = Mathf.InverseLerp(minSlip, maxSlip, wheelData.combinedTireSlip);

            //Debug.Log(wheelData.hit.sidewaysSlip + " " + wheelData.hit.forwardSlip);
            if (Mathf.Abs(wheelData.hit.sidewaysSlip) > 0.02f || Mathf.Abs(wheelData.hit.forwardSlip) > 0.02f)
                skidRatio =  Mathf.Abs(wheelData.hit.sidewaysSlip) / 0.15f;// wheelData.hit.sidewaysSlip;

            //Debug.DrawRay(wheelData.wheelTransform.position, Vector3.up, Color.yellow);

            fxData.lastMarksIndex = marksRenderer.AddMark
                (
                    wheelData.hit.point - wheelData.wheelTransform.right * wheelData.collider.center.x + wheelData.velocity * Time.deltaTime,
                    wheelData.hit.normal,
                    pressureRatio,
                    skidRatio,
                    tireWidth,
                    fxData.lastMarksIndex
                );
        }
    }
}
