﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutpostDoor : MonoBehaviour {

	public Animation anim;
	public Transform halt;
	public StartController sc;
	public SplineType type;

	public void OpenInside()
	{
//		Debug.Log ("OpenInside");
		halt.gameObject.SetActive (false);
		sc.sctype = StartControllerType.BattleActivator;
		sc.isCheck = false;
		anim.PlayQueued ("Open");
	}

	public void OpenOutside()
	{
//		Debug.Log ("OpenOutside");
		halt.gameObject.SetActive (false);
		sc.sctype = StartControllerType.EndLevelActivator;
		sc.isCheck = false;
		anim.PlayQueued ("Open");
	}

	public void Close()
	{
//		Debug.Log ("Close");
		sc.sctype = StartControllerType.Inactive;
		sc.isCheck = false;
		anim.PlayQueued ("Close");

	}

	public void CloseImmediately()
	{
//		Debug.Log ("CloseImmediately");
		sc.sctype = StartControllerType.Inactive;
		sc.isCheck = false;
		anim.Stop ();
		transform.Find ("root").Find ("Bone").Find ("left").transform.localPosition = new Vector3 (0, 5, -14);
		transform.Find ("root").Find ("Bone").Find ("right").transform.localPosition = new Vector3 (0, 5, 14);
		halt.gameObject.SetActive (true);
	}
}
