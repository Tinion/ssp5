﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;
using System.Collections.Generic;
using System;
using System.Reflection;
using System.IO;


[System.Flags]
public enum CreationType
{
	Input = 0,
	Biom = 1,
	Paint = 2,
	Road = 3,
	Bake = 4,
	Create = 5,
}

[System.Flags]
public enum landscapeType
{
	rock = 0,
	desert = 1,
	snow = 2,
	tunnel = 3,
}

[System.Serializable]
public class InputDatabaseBlock
{
	public LayersNumber ln;
	public landscapeType type;
	public landscapeType typeExternal;
	public BlockPlaneType typeBlockPlane; 
	public string blockName;

	public Mesh terrain;
	public List<Mesh> road;
	public Mesh collider;

	public Texture2D[] splatMapSources;

	public UnityEngine.Object bezierData;
	public UnityEngine.Object bezierData02;
}

#if UNITY_EDITOR
[ExecuteInEditMode]
public class PrefabCollector : MonoBehaviour 
{
	public List<InputDatabaseBlock> blocks;
	public InputDatabaseBlock current;

	public GUISkin guiSkin;
	public BiomesSources biomeSources;
	public bool mergingButton = false;
	public float Progress = 0.0f;
	public string ProgressName = "";
	public UnityEngine.Object rotatationMesh;
	public UnityEngine.Object colliderOne;
	public UnityEngine.Object colliderTwo;
	public float rotationAngle = 90.0f;
	public int paintOpacity = -1;

	[HideInInspector]
	public bool isBezierData = false;
	public bool isSavePresets = true;
	public CreationType creatType;

	[HideInInspector]
	public ReliefTerrain RT;

	public GeometryVsTerrainBlend gblend;


	[HideInInspector] public int tabSwitch = 0;
	[HideInInspector]public int tabSwitchSelect = 0;

	[MenuItem("Tools/RoadsEditor")]
	public static void InstantiateCollector()
	{
		PrefabCollector PC = (PrefabCollector)AssetDatabase.LoadAssetAtPath<PrefabCollector>("Assets/Prefabs/PrefabCollector.prefab");
		PrefabCollector PCI = (PrefabCollector)PrefabUtility.InstantiatePrefab (PC);
		Selection.activeGameObject = PCI.gameObject;
		//PCI.splatMapSources = new Texture2D[2];
		//PCI.splatMapSources[0] = (Texture2D)AssetDatabase.LoadAssetAtPath<Texture2D>("Assets/Art/roads/additional/splatATemplate.tga");
		//PCI.splatMapSources[1] = (Texture2D)AssetDatabase.LoadAssetAtPath<Texture2D>("Assets/Art/roads/additional/splatBTemplate.tga");
		PCI.creatType = CreationType.Input;
		PCI.Progress = 0.05f;
		PCI.ProgressName = "Input";
		PCI.current = new InputDatabaseBlock ();
		PCI.current.blockName = "Name";
		PCI.current.splatMapSources = new Texture2D[2];
		PCI.current.splatMapSources[0] = AssetDatabase.LoadAssetAtPath ("Assets/Art/roads/additional/additionalSplatMap.tga", typeof(Texture2D)) as Texture2D;
		PCI.current.splatMapSources[1] = AssetDatabase.LoadAssetAtPath ("Assets/Art/roads/additional/additionalSplatMap.tga", typeof(Texture2D)) as Texture2D;
		if (PCI.current.road == null)
			PCI.current.road = new List<Mesh> ();
		PCI.tabSwitch = 0;
		PCI.tabSwitchSelect = 0;
		PCI.isSavePresets = true;
		PCI.biomeSources = AssetDatabase.LoadAssetAtPath("Assets/Resources/biomes.asset", typeof(BiomesSources)) as BiomesSources;
	}

	public void SavePreset()
	{
		if (isSavePresets) {
			Debug.Log ("Save");
			int select = -1;
			for (int i = 0; i < blocks.Count; i++) {
				if (blocks [i].blockName == current.blockName && blocks [i].type == current.type) {
					select = i;
					break;
				}
			}

			if (select == -1) {
				blocks.Add (new InputDatabaseBlock ());
				blocks [blocks.Count - 1] = current;
				Debug.Log ("Save " + blocks [blocks.Count - 1].blockName);
			} else {
				blocks [select] = current;
				Debug.Log ("Save " + blocks [select].blockName);
			}
			PrefabUtility.CreatePrefab ("Assets/Prefabs/PrefabCollector.prefab", this.gameObject);
			AssetDatabase.Refresh ();
			EditorUtility.SetDirty (this.gameObject);
			AssetDatabase.SaveAssets ();
		}
	}
	public void Create()
	{

		SavePreset ();
		GameObject blockBase = new GameObject();
		MeshFilter bbMF = blockBase.AddComponent<MeshFilter> ();
		bbMF.sharedMesh = current.terrain;
		MeshRenderer bbMR = blockBase.AddComponent<MeshRenderer>();

		blockBase.name = "Mesh";
		blockBase.transform.position = Vector3.zero;
		blockBase.transform.rotation = Quaternion.identity;

		for (int i = 0; i < current.road.Count; i++)
		{
			GameObject roadBase = new GameObject ();
			MeshFilter rbMF = roadBase.AddComponent<MeshFilter> ();
			rbMF.sharedMesh = current.road[i];
			MeshRenderer rbMR = roadBase.AddComponent<MeshRenderer> ();

			roadBase.name = current.road[i].name;
			roadBase.transform.position = Vector3.zero;
			roadBase.transform.rotation = Quaternion.identity;
			roadBase.transform.SetParent (blockBase.transform);
			rbMR.sharedMaterial = (Material)AssetDatabase.LoadAssetAtPath<Material> ("Assets/Art/roads/additional/roadTimelapse.mat");
			EditorUtility.SetSelectedWireframeHidden (rbMR, true);
		}

		Texture2D[] splatMap;

		if (current.ln == LayersNumber._4Layers)
			splatMap = new Texture2D[1];
		else
			splatMap = new Texture2D[2];

		for (int i = 0; i < ((current.ln == LayersNumber._4Layers) ? 1 : 2); i++)
		{
			Color32[] pix = current.splatMapSources[i].GetPixels32 ();
			splatMap[i] = new Texture2D (current.splatMapSources[i].width, current.splatMapSources[i].height);
			splatMap[i].SetPixels32 (pix);
			splatMap[i].Apply ();

			string savename;
			if (current.ln == LayersNumber._4Layers)
				savename = "splatTexA_";
			else
			{
				if ( i == 0)
					savename = "splatTexA_";	
				else
					savename = "splatTexB_";	
			}

			string sourcePath02 = "Assets/Art/roads/additional/";
			SaveTexture (ref splatMap[i], ref sourcePath02, savename, 0, 256, TextureImporterFormat.ARGB32, true, false, false);

		}

		if (current.ln == LayersNumber._4Layers)
			ReliefTerrain.ImportStandart (blockBase.transform, splatMap [0]);
		else 
			ReliefTerrain.ImportDouble (blockBase.transform, splatMap [0], splatMap [1]);
			
		ReliefTerrain RT = blockBase.GetComponent<ReliefTerrain> ();
		RT.globalSettingsHolder.submenu = ReliefTerrainMenuItems.GeneralSettings;
		RT.globalSettingsHolder.submenu_settings = ReliefTerrainSettingsItems.MainSettings;
		creatType = CreationType.Biom;
		EditorUtility.SetSelectedWireframeHidden (bbMR, true);


		RT.GetGlobalUV ();

		//Selection.activeGameObject = RT.gameObject;
		RT.Initialize();
		//RT.
		Progress = 0.2f;
		ProgressName = "Select Biom";
		//Selection.activeGameObject = this.gameObject;

		switch (current.ln) 
		{
		case LayersNumber._4Layers:
			tabSwitch = 0;
			tabSwitchSelect = 0;
			break;
		case LayersNumber._8Layers:
			tabSwitch = 1;
			tabSwitchSelect = 1;
			break;
		case LayersNumber._Transition:
			tabSwitch = 2;
			tabSwitchSelect = 2;
			break;
		}
	}

	public void Prepare()
	{
		string sourcePath = "Assets/Art/roads";

		if (!Directory.Exists (sourcePath + "/" + RT.presets[RT.currentPresetNumber01].name.ToString())) {
			AssetDatabase.CreateFolder (sourcePath, RT.presets[RT.currentPresetNumber01].name.ToString());
			AssetDatabase.Refresh ();
			AssetDatabase.SaveAssets ();
		}

		sourcePath += "/" + RT.presets[RT.currentPresetNumber01].name.ToString ();

		if (!Directory.Exists (sourcePath + "/" + current.blockName)) {
			AssetDatabase.CreateFolder (sourcePath, current.blockName);
			AssetDatabase.Refresh ();
			AssetDatabase.SaveAssets ();
		}

		string savename = "";

		sourcePath = "Assets/Art/roads/" + RT.presets[RT.currentPresetNumber01].name.ToString() + "/" + current.blockName + "/";

		for (int i = 0; i < ((current.ln == LayersNumber._4Layers) ? 1 : 2); i++)
		{
			Color32[] pix = current.splatMapSources[i].GetPixels32 ();
			current.splatMapSources[i] = new Texture2D (current.splatMapSources[i].width, current.splatMapSources[i].height);
			current.splatMapSources[i].SetPixels32 (pix);
			current.splatMapSources[i].Apply ();


			if (current.ln == LayersNumber._4Layers)
				savename = "splatTex_" + RT.presets[RT.currentPresetNumber01].name.ToString () + current.blockName;
			else
			{
				if ( i == 0)
					savename = "splatTex_" + RT.presets[RT.currentPresetNumber01].name.ToString () + current.blockName.ToString ();	
				else
					savename = "splatTex_" + RT.presets[RT.currentPresetNumber02].name.ToString () + current.blockName.ToString ();	
			}

			string sourcePath02 = sourcePath;
			SaveTexture (ref current.splatMapSources[i], ref sourcePath02, savename, 0, 256, TextureImporterFormat.ARGB32, true, false, false);

		}

		sourcePath = "Assets/Art/roads/" + RT.presets[RT.currentPresetNumber01].name.ToString() + "/";
		savename = "bumpmap_layers01";

		SaveTexture (ref RT.globalSettingsHolder.Bump01, ref sourcePath, savename, 0, 1024, TextureImporterFormat.ARGB32, true, false, false);

		sourcePath = "Assets/Art/roads/" + RT.presets[RT.currentPresetNumber01].name.ToString() + "/";
		savename = "bumpmap_layers23";

		SaveTexture (ref RT.globalSettingsHolder.Bump23, ref sourcePath, savename, 0, 1024, TextureImporterFormat.ARGB32, true, false, false);

		sourcePath = "Assets/Art/roads/" + RT.presets[RT.currentPresetNumber01].name.ToString() + "/";
		savename = "heightmap_layers_0_to_3";

		SaveTexture (ref RT.globalSettingsHolder.HeightMap, ref sourcePath, savename, 0, 128, TextureImporterFormat.AutomaticCompressed, true, false, true);

		if (current.ln == LayersNumber._8Layers)
		{
			sourcePath = "Assets/Art/roads/" + RT.presets[RT.currentPresetNumber02].name.ToString() + "/";
			savename = "bumpmap_layers01";

			SaveTexture (ref RT.globalSettingsHolder.Bump45, ref sourcePath, savename, 0, 1024, TextureImporterFormat.ARGB32, true, false, false);

			sourcePath = "Assets/Art/roads/" + RT.presets[RT.currentPresetNumber02].name.ToString() + "/";
			savename = "bumpmap_layers23";

			SaveTexture (ref RT.globalSettingsHolder.Bump67, ref sourcePath, savename, 0, 1024, TextureImporterFormat.ARGB32, true, false, false);

			sourcePath = "Assets/Art/roads/" + RT.presets[RT.currentPresetNumber02].name.ToString() + "/";
			savename = "heightmap_layers_0_to_3";

			SaveTexture (ref RT.globalSettingsHolder.HeightMap2, ref sourcePath, savename, 0, 128, TextureImporterFormat.AutomaticCompressed, true, false, true);

			for (int i = 0; i < 2; i++) 
			{
				if (i==0)
					sourcePath = "Assets/Art/roads/" + RT.presets[RT.currentPresetNumber01].name.ToString () + "/";
				else
					sourcePath = "Assets/Art/roads/" + RT.presets[RT.currentPresetNumber02].name.ToString () + "/";
				savename = "atlas_texture_layers_0_to_3";

				if (SaveTexture (ref RT.globalSettingsHolder.splat_atlases [i], ref sourcePath, savename, 100, 2048, TextureImporterFormat.AutomaticCompressed, true, false, false)) {
					string path = AssetDatabase.GetAssetPath (RT.globalSettingsHolder.splat_atlases [i]);
					TextureImporter textureImporter = AssetImporter.GetAtPath (path) as TextureImporter;
					textureImporter.wrapMode = TextureWrapMode.Clamp;
					textureImporter.filterMode = FilterMode.Trilinear;
					AssetDatabase.ImportAsset (path, ImportAssetOptions.ForceUpdate);
				}
			}

		}

		Progress = 0.5f;
		ProgressName = "Paint & Merging";
		Road ();

		creatType = CreationType.Paint;
	}

	public void Paint()
	{
		Progress = 0.7f;
		ProgressName = "Paint & Merging";

	}

	public void BakeRoad()
	{
		
	}

	public void Road()
	{
		



		for (int i = 0; i < current.road.Count; i++) 
		{
			Transform roadTr = RT.transform.GetChild (i);

			MeshFilter rbMF = roadTr.GetComponent<MeshFilter> ();
			MeshRenderer mrgblend = roadTr.GetComponent<MeshRenderer> ();
			mrgblend.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
			mrgblend.receiveShadows = false;
			EditorUtility.SetSelectedWireframeHidden (mrgblend, true);

			roadTr.gameObject.AddComponent<BoundsCorrector> ();
			//Material road = new Material(Shader.Find("Relief Pack/Road_4Layers"));
			int index = roadTr.name.IndexOf ('_');
			string currentName = roadTr.name.Substring (index, roadTr.name.Length - index);

			if (current.road.Count > 1) 
			{
				switch (currentName) {
				case "_roadDefault":
					Debug.Log ("0");
					mrgblend.sharedMaterial = (Material)AssetDatabase.LoadAssetAtPath<Material> ("Assets/Art/roads/" + RT.presets [RT.currentPresetNumber01].name.ToString () + "/roadDefault.mat");
					break;
				case "_roadCenter":
					Debug.Log ("1");
					mrgblend.sharedMaterial = (Material)AssetDatabase.LoadAssetAtPath<Material> ("Assets/Art/roads/" + RT.presets [RT.currentPresetNumber01].name.ToString () + "/roadCenter.mat");
					break;
				case "_roadCenterDecal":
					mrgblend.sharedMaterial = (Material)AssetDatabase.LoadAssetAtPath<Material> ("Assets/Art/roads/" + RT.presets [RT.currentPresetNumber01].name.ToString () + "/roadCenterDecal.mat");
					break;
				case "_roadDarken":
					mrgblend.sharedMaterial = (Material)AssetDatabase.LoadAssetAtPath<Material> ("Assets/Art/roads/" + RT.presets [RT.currentPresetNumber01].name.ToString () + "/roadDarken.mat");
					break;
				case "_roadDarkenDecal":
					mrgblend.sharedMaterial = (Material)AssetDatabase.LoadAssetAtPath<Material> ("Assets/Art/roads/" + RT.presets [RT.currentPresetNumber01].name.ToString () + "/roadDarkenDecal.mat");
					break;
				case "_roadFork":
					mrgblend.sharedMaterial = (Material)AssetDatabase.LoadAssetAtPath<Material> ("Assets/Art/roads/" + RT.presets [RT.currentPresetNumber01].name.ToString () + "/roadFork.mat");
					break;
				}
			}
			else
				mrgblend.sharedMaterial = (Material)AssetDatabase.LoadAssetAtPath<Material> ("Assets/Art/roads/" + RT.presets [RT.currentPresetNumber01].name.ToString () + "/roadDefault.mat");
			//
			//road.name = "Road " + i.ToString ();
			//intReplaced [i] = road;


		}



		switch (current.ln) {
		case LayersNumber._4Layers: 
			{
				string path = "Assets/Art/roads/" + RT.presets [RT.currentPresetNumber01].name.ToString () + "/" + current.blockName + "/";
				string name = "splatTex_" + RT.presets [RT.currentPresetNumber01].name.ToString () + current.blockName;
				SaveTexture (ref RT.controlA, ref path, name, 0, 256, TextureImporterFormat.ARGB32, true, false, true);

			
			}
			break;
		case LayersNumber._8Layers: 
			{
				string path = "Assets/Art/roads/" + RT.presets [RT.currentPresetNumber01].name.ToString () + "/" + current.blockName + "/";
				string name = "splatTex_" + RT.presets [RT.currentPresetNumber01].name.ToString () + current.blockName;
				SaveTexture (ref RT.controlA, ref path, name, 0, 256, TextureImporterFormat.ARGB32, true, false, true);

				path = "Assets/Art/roads/" + RT.presets [RT.currentPresetNumber01].name.ToString () + "/" + current.blockName + "/";
				name = "splatTex_" + RT.presets [RT.currentPresetNumber02].name.ToString () + current.blockName;

				SaveTexture (ref RT.controlB, ref path, name, 0, 256, TextureImporterFormat.ARGB32, true, false, true);
			}
			break;
		case LayersNumber._Transition:
			{
				string path = "Assets/Art/roads/" + RT.presets [RT.currentPresetNumber01].name.ToString () + "/" + current.blockName + "/";
				string name = "splatTex_" + RT.presets [RT.currentPresetNumber01].name.ToString () + current.blockName;
				SaveTexture (ref RT.controlA, ref path, name, 0, 256, TextureImporterFormat.ARGB32, true, false, true);	
			}
			break;
		}

		RT.globalSettingsHolder.bakeType = RT.presets[RT.currentPresetNumber01].name.ToString ();
		RT.globalSettingsHolder.bakeName = current.blockName.ToString ();

	}

	public void TextureControlTo256()
	{
		Texture2D newTexture = new Texture2D (256, 256, UnityEngine.TextureFormat.ARGB32, true, false);
		for (int i = 0; i < 256; i++)
			for (int j = 0; j < 256; j++) 
			{
				newTexture.SetPixel (i, j, RT.controlA.GetPixel (Mathf.CeilToInt (i / 2), Mathf.CeilToInt (j / 2)));
			}

		RT.controlA = newTexture;
		string path = "Assets/Art/roads/" + RT.presets [RT.currentPresetNumber01].name.ToString () + "/" + current.blockName + "/";
		string name = "splatTex_" + RT.presets [RT.currentPresetNumber01].name.ToString () + current.blockName;
		SaveTexture (ref RT.controlA, ref path, name, 0, 256, TextureImporterFormat.ARGB32, true, false, true);	
	}

	[ContextMenu("Rotate Mesh")]
	public void RotateMesh()
	{
		string path = AssetDatabase.GetAssetPath (rotatationMesh);
		GameObject newGO = new GameObject ();
		GameObject secondGO = new GameObject ();
		secondGO.transform.localRotation = Quaternion.Euler (new Vector3 (0, rotationAngle, 0));
		MeshFilter MF = newGO.AddComponent<MeshFilter> ();
		MeshRenderer MR = newGO.AddComponent<MeshRenderer> ();
		MF.sharedMesh = (Mesh)rotatationMesh;

		int count = MF.sharedMesh.vertexCount;
		Vector3[] vertices = MF.sharedMesh.vertices;
		for (int i = 0; i < count; i++) 
		{
			Vector3 vertex = secondGO.transform.TransformPoint (vertices [i]);
			vertices [i] = vertex;
		}


		MF.sharedMesh.vertices = vertices;
		MF.sharedMesh.RecalculateNormals ();
		MF.sharedMesh.RecalculateBounds ();

		DestroyImmediate (newGO);
		DestroyImmediate (secondGO);
		Debug.Log (rotatationMesh.name + " rotation Complete!");
	}

	[ContextMenu("Merging Mesh")]
	public void MergingMesh()
	{
		string path = AssetDatabase.GetAssetPath (colliderOne);
		string path2 = AssetDatabase.GetAssetPath (colliderTwo);

		CombineInstance[] combine = new CombineInstance[2];

		combine [0] = new CombineInstance ();
		combine [0].mesh = (Mesh)colliderOne;
		combine [1] = new CombineInstance ();
		combine [1].mesh = (Mesh)colliderTwo;

		Mesh newMesh = new Mesh ();
		newMesh.name = colliderOne.name;
		newMesh.CombineMeshes (combine, true, false);
		newMesh.RecalculateNormals ();

		CreateOrReplaceAssetMesh (newMesh, path);
		//SaveMesh (newMesh);
		//BlockPlane.AutoWeld (newMesh, 0.01f);
		//KillUselessVertex (newMesh);

		colliderOne = newMesh;
	}

	public void Bake()
	{
		/*
		Progress = 1.0f;
		ProgressName = "Bake";

		Transform tr = RT.transform;

		GameObject newBlock = BakeAll (tr);
		SaveMeshes (tr, true, true, true, true);
		string data = AssetDatabase.GetAssetPath (current.bezierData);
		BlockPlane.CreatePlaneEditor (data, newBlock, current.collider, current.type, current.blockName);

		PrefabUtility.CreatePrefab ("Assets/Resources/blocks/" + RT.presets[RT.currentPresetNumber01].name.ToString () + "/" + RT.presets[RT.currentPresetNumber01].name.ToString () + current.blockName.ToString () + ".prefab", newBlock);

		AssetDatabase.Refresh ();
		EditorUtility.SetDirty (newBlock);
		AssetDatabase.SaveAssets ();

		Selection.activeGameObject = newBlock;
		DestroyImmediate (this.gameObject);
		*/
	}

	public void Merging()
	{
		Progress = 1.0f;
		ProgressName = "Bake";

		Transform tr = RT.transform;

		GameObject newBlock = BakeAll (tr);
		newBlock.transform.position = Vector3.zero;
		newBlock.transform.rotation = Quaternion.identity;
		SaveMeshes (tr, true, true, true, true);

		string path = "Assets/Resources/blocks/" + RT.presets [RT.currentPresetNumber01].name.ToString () + "/" + RT.presets [RT.currentPresetNumber01].name.ToString () + current.blockName.ToString () + ".prefab";
		GameObject oldBlock = (GameObject)PrefabUtility.InstantiatePrefab(AssetDatabase.LoadAssetAtPath<UnityEngine.Object> (path));
		oldBlock.transform.position = Vector3.zero;
		oldBlock.transform.rotation = Quaternion.identity;

		Transform oldMesh = oldBlock.transform.Find ("mesh");
		if (oldMesh!=null)
			DestroyImmediate(oldMesh.gameObject);

		Transform oldCollider = oldBlock.transform.Find ("collider");
		if (oldCollider!=null)
			DestroyImmediate(oldCollider.gameObject);

		newBlock.transform.Find ("mesh").transform.parent = oldBlock.transform;

		GameObject colliderGO = new GameObject ();
		colliderGO.name = "collider";
		colliderGO.transform.position = Vector3.zero;
		colliderGO.transform.rotation = Quaternion.Euler(new Vector3(0, 90f, 0));
		colliderGO.transform.SetParent (oldBlock.transform);
		colliderGO.tag = "Block";
		colliderGO.layer = LayerMask.NameToLayer ("Ground");

		BlockPlane BP = oldBlock.GetComponent<BlockPlane> ();
		BP.sourceCollider = current.collider;
		BP.roadCollider = colliderGO.AddComponent<MeshCollider> (); 
		BP.roadCollider.sharedMaterial = (PhysicMaterial)AssetDatabase.LoadAssetAtPath<PhysicMaterial> ("Assets/PhysicMaterials/Asphalt.physicmaterial");
		Rigidbody RB = colliderGO.AddComponent<Rigidbody> ();
		RB.mass = 1.0f;
		RB.useGravity = false;
		RB.isKinematic = true;

		//PrefabUtility.CreatePrefab ("Assets/Resources/blocks/" + RT.presets[RT.currentPresetNumber01].name.ToString () + "/" + RT.presets[RT.currentPresetNumber01].name.ToString () + blockName.ToString () + ".prefab", newBlock);

		AssetDatabase.Refresh ();
		EditorUtility.SetDirty (oldBlock);
		AssetDatabase.SaveAssets ();

		DestroyImmediate(newBlock.gameObject);

		Selection.activeGameObject = oldBlock;
		DestroyImmediate (this.gameObject);
	}

	public GameObject BakeAll(Transform _tr)
	{
		

		ReliefTerrain NRT = _tr.GetComponent<ReliefTerrain> ();
		Renderer NR = _tr.GetComponent<Renderer> ();
		MeshFilter NF = _tr.GetComponent<MeshFilter> ();
		MeshCollider NC = _tr.GetComponent<MeshCollider> ();

		//NRT.globalSettingsHolder.Refresh (null, NRT);
		DestroyImmediate (NRT);
		DestroyImmediate (NC);

		GeometryVsTerrainBlend[] additionals = _tr.GetComponentsInChildren<GeometryVsTerrainBlend> ();

		int countAdd = additionals.Length;
		for (int i = 0; i < countAdd; i++) 
		{
			GameObject currentGO = additionals [i].gameObject;
			string tempName = additionals [i].gameObject.name;
			MeshFilter oldMF = additionals [i].GetComponent<MeshFilter> ();
			MeshRenderer oldMR = additionals [i].GetComponent<MeshRenderer> ();
			MeshCollider oldMC = additionals [i].GetComponent<MeshCollider> ();

			BoundsCorrector bc = currentGO.AddComponent<BoundsCorrector> ();
			bc.isOne = true;

			DestroyImmediate (additionals [i]);
			DestroyImmediate (currentGO.transform.GetChild (0).gameObject);
			DestroyImmediate (oldMC);

		}

		AssetDatabase.Refresh ();
		EditorUtility.SetDirty (_tr);
		AssetDatabase.SaveAssets ();

		GameObject root = new GameObject ();
		root.name = RT.presets[RT.currentPresetNumber01].name + current.blockName;
		root.transform.position = Vector3.zero;
		root.transform.rotation = Quaternion.identity;
		_tr.position = Vector3.zero;
		_tr.rotation = Quaternion.Euler (new Vector3 (0, 90f, 0));
		_tr.name = "mesh";
		BoundsCorrector bc2 = _tr.gameObject.AddComponent<BoundsCorrector> ();
		bc2.isOne = true;


		_tr.transform.SetParent (root.transform);

		return root;
	}

	public static void GetTextures(ref Material mat, PrefabCollector pc)
	{

		string sourcePath = "Assets/Art/roads/" + pc.RT.presets[pc.RT.currentPresetNumber01].name.ToString() + "/";
		string savename;

		savename = "bumpmap_layers01.png";
		Texture2D bump01 = AssetDatabase.LoadAssetAtPath (sourcePath + savename, typeof(Texture2D)) as Texture2D;

		savename = "bumpmap_layers23.png";
		Texture2D bump23 = AssetDatabase.LoadAssetAtPath (sourcePath + savename, typeof (Texture2D)) as Texture2D;

		savename = "heightmap_layers_0_to_3.png";
		Texture2D height1 = AssetDatabase.LoadAssetAtPath (sourcePath + savename, typeof (Texture2D)) as Texture2D;

		mat.SetTexture ("_BumpMap01", bump01);
		mat.SetTexture ("_BumpMap23", bump23);
		mat.SetTexture ("_TERRAIN_HeightMap", height1);

		if (pc.current.ln == LayersNumber._8Layers)
		{
			sourcePath = "Assets/Art/roads/" + pc.RT.presets[pc.RT.currentPresetNumber01].name.ToString() + "/";
			savename = "atlas_texture_layers_0_to_3.png";
			Texture2D atlas1 = AssetDatabase.LoadAssetAtPath (sourcePath + savename, typeof (Texture2D)) as Texture2D;

			sourcePath = "Assets/Art/roads/" + pc.RT.presets[pc.RT.currentPresetNumber02].name.ToString() + "/";

			savename = "bumpmap_layers01.png";
			Texture2D bump45 = AssetDatabase.LoadAssetAtPath (sourcePath + savename, typeof(Texture2D)) as Texture2D;
			savename = "bumpmap_layers23.png";
			Texture2D bump67 = AssetDatabase.LoadAssetAtPath (sourcePath + savename, typeof (Texture2D)) as Texture2D;
			savename = "heightmap_layers_0_to_3.png";
			Texture2D height2 = AssetDatabase.LoadAssetAtPath (sourcePath + savename, typeof (Texture2D)) as Texture2D;
			savename = "atlas_texture_layers_0_to_3.png";
			Texture2D atlas2 = AssetDatabase.LoadAssetAtPath (sourcePath + savename, typeof (Texture2D)) as Texture2D;

			mat.SetTexture ("_BumpMap45", bump45);
			mat.SetTexture ("_BumpMap67", bump67);
			mat.SetTexture ("_TERRAIN_HeightMap2", height2);
			mat.SetTexture ("_SplatAtlasA", atlas1);
			mat.SetTexture ("_SplatAtlasB", atlas2);
		}

		if (pc.current.ln == LayersNumber._Transition)
		{
			sourcePath = "Assets/Art/roads/" + pc.RT.presets[pc.RT.currentPresetNumber01].name.ToString() + "/";
			savename = "atlas_texture_layers_0_to_3.png";
			Texture2D atlas1 = AssetDatabase.LoadAssetAtPath (sourcePath + savename, typeof (Texture2D)) as Texture2D;

			sourcePath = "Assets/Art/roads/" + pc.RT.presets[pc.RT.currentPresetNumber02].name.ToString() + "/";

			savename = "bumpmap_layers01.png";
			Texture2D bump45 = AssetDatabase.LoadAssetAtPath (sourcePath + savename, typeof(Texture2D)) as Texture2D;
			savename = "bumpmap_layers23.png";
			Texture2D bump67 = AssetDatabase.LoadAssetAtPath (sourcePath + savename, typeof (Texture2D)) as Texture2D;
			savename = "heightmap_layers_0_to_3.png";
			Texture2D height2 = AssetDatabase.LoadAssetAtPath (sourcePath + savename, typeof (Texture2D)) as Texture2D;
			savename = "atlas_texture_layers_0_to_3.png";
			Texture2D atlas2 = AssetDatabase.LoadAssetAtPath (sourcePath + savename, typeof (Texture2D)) as Texture2D;

			mat.SetTexture ("_BumpMap45", bump45);
			mat.SetTexture ("_BumpMap67", bump67);
			mat.SetTexture ("_TERRAIN_HeightMap2", height2);
			mat.SetTexture ("_SplatAtlasA", atlas1);
			mat.SetTexture ("_SplatAtlasB", atlas2);
		}
	}

	public static void GetTexturesRoad(ref Material mat, PrefabCollector pc)
	{

		string sourcePath = "Assets/Art/roads/" + pc.current.type.ToString() + "/";
		string savename;

			
		savename = "heightmap_layers_0_to_3.png";
		Texture2D height1 = AssetDatabase.LoadAssetAtPath (sourcePath + savename, typeof (Texture2D)) as Texture2D;

		Texture2D splat01 = AssetDatabase.LoadAssetAtPath (sourcePath + pc.current.blockName + "/" + "splatTex_" + pc.current.type.ToString() + pc.current.blockName + ".png", typeof (Texture2D)) as Texture2D;
		Debug.Log (sourcePath + pc.current.blockName + "/" + "splatTex_" + pc.current.blockName + ".png");

		mat.SetTexture ("_TERRAIN_HeightMap", height1);
		mat.SetTexture ("_TERRAIN_Control", splat01); 

		if (pc.current.ln == LayersNumber._8Layers)
		{

		}
	}

	public void SaveMesh(Mesh mesh)
	{
		string pathParent = "Assets/Art/roads";
		string pathNew = pathParent;

		if (!Directory.Exists (pathNew + "/" + current.type.ToString()))
		{
			AssetDatabase.CreateFolder (pathNew, current.type.ToString());
			AssetDatabase.Refresh ();
			AssetDatabase.SaveAssets ();
		}
		pathNew += "/" + current.type.ToString();

		if (!Directory.Exists (pathNew + "/" + current.blockName))
		{
			AssetDatabase.CreateFolder (pathNew, current.blockName);
			AssetDatabase.Refresh ();
			AssetDatabase.SaveAssets ();
		}

		pathNew += "/" + current.blockName;


		if (!Directory.Exists (pathNew + "/Meshes"))
		{
			AssetDatabase.CreateFolder (pathNew, "Meshes");
			AssetDatabase.Refresh ();
			AssetDatabase.SaveAssets ();
		}

		pathNew += "/Meshes/";
		string[] files = Directory.GetFiles (pathNew);

		string meshMainPath = pathNew + "/" + current.type.ToString () + current.blockName + files.Length + "Mesh.asset";
		current.terrain = CreateOrReplaceAssetMesh (mesh, meshMainPath);

		SavePreset ();

	}

	public void SaveMeshes(Transform roadTr, bool isMeshMain, bool isMatMain, bool isMeshAdd, bool isMatAdd)
	{

		MeshFilter MF = roadTr.GetComponent<MeshFilter> ();
		Renderer MR = roadTr.GetComponent<Renderer> ();

		string pathParent = "Assets/Art/roads";
		string pathNew = pathParent;

		if (!Directory.Exists (pathNew + "/" + current.type.ToString()))
		{
			AssetDatabase.CreateFolder (pathNew, current.type.ToString());
			AssetDatabase.Refresh ();
			AssetDatabase.SaveAssets ();
		}
		pathNew += "/" + current.type.ToString();

		if (!Directory.Exists (pathNew + "/" + current.blockName))
		{
			AssetDatabase.CreateFolder (pathNew, current.blockName);
			AssetDatabase.Refresh ();
			AssetDatabase.SaveAssets ();
		}

		pathNew += "/" + current.blockName;

		if (isMeshMain)
		{
			Debug.Log ("+M");
			string meshMainPath = pathNew + "/" + current.type.ToString() + current.blockName + "Mesh.asset";
			MF.sharedMesh = CreateOrReplaceAssetMesh (MF.sharedMesh, meshMainPath);
		}

		if (isMatMain)
		{
			Debug.Log ("++M");
			string matMainPath = pathNew + "/" + current.type.ToString() + current.blockName + "Material.mat";
			MR.sharedMaterial = CreateOrReplaceAssetMaterial (MR.sharedMaterial, true, matMainPath,  this);
		}

		if (!Directory.Exists (pathNew + "/" + "road")) 
		{
			AssetDatabase.CreateFolder (pathNew, "road");
			AssetDatabase.Refresh ();
			AssetDatabase.SaveAssets ();
		}

		for (int i = 0; i < current.road.Count; i++) 
		{
			Transform additionals = roadTr.GetChild (i);

			string tempName = additionals.gameObject.name;
			MeshFilter oldMF = additionals.GetComponent<MeshFilter> ();
			MeshRenderer oldMR = additionals.GetComponent<MeshRenderer> ();

			string pathNewAdditional = pathNew + "/" + "road";

			if (isMeshAdd)
			{
				string meshPath = pathNewAdditional + "/" + tempName + "Mesh.asset";
				oldMF.sharedMesh = CreateOrReplaceAssetMesh (oldMF.sharedMesh, meshPath);
			}

			/*
			if (isMatAdd)
			{
				string matPath = pathNewAdditional + "/" + tempName + "Material" + i.ToString() + ".mat";
				oldMR.sharedMaterial = CreateOrReplaceAssetMaterial (oldMR.sharedMaterial, false, matPath, this);
			}
			*/
		}

		AssetDatabase.Refresh ();
		EditorUtility.SetDirty (MR.sharedMaterial);
		AssetDatabase.SaveAssets ();

		creatType = CreationType.Paint;
	}

	public static Mesh CreateOrReplaceAssetMesh (Mesh asset, string path)
	{
		Mesh meshAsset = new Mesh ();
		meshAsset.vertices = asset.vertices;
		meshAsset.triangles =  asset.triangles;
		meshAsset.uv = asset.uv;
		meshAsset.uv2 = asset.uv2;
		meshAsset.normals = asset.normals;
		meshAsset.colors32 = asset.colors32;
		meshAsset.tangents = asset.tangents;
		meshAsset.subMeshCount = asset.subMeshCount;
		for (int i = 0; i < asset.subMeshCount; i++)
			meshAsset.SetTriangles (asset.GetTriangles(i), i);
		meshAsset.RecalculateNormals ();

		AssetDatabase.CreateAsset (meshAsset, path);
		AssetDatabase.Refresh ();
		AssetDatabase.SaveAssets ();

		return meshAsset;
	}

	public static Material CreateOrReplaceAssetMaterial (Material asset, bool isMain, string path, PrefabCollector pc)
	{
		Material materialAsset = new Material (asset.shader);

		materialAsset.CopyPropertiesFromMaterial(asset);

		if (isMain)
			GetTextures (ref materialAsset, pc);
		else
			GetTexturesRoad (ref materialAsset, pc);

		AssetDatabase.CreateAsset (materialAsset, path);
		AssetDatabase.Refresh ();
		AssetDatabase.SaveAssets ();

		return materialAsset;
	}

	public bool SaveTexture (ref Texture2D tex, ref string save_path, string default_name, int buttonwidth, int _maxSize, TextureImporterFormat textureFormat, bool mipmapEnabled, bool button_triggered = true, bool sRGB_flag = false)
	{
		EditorGUI.BeginDisabledGroup (tex == null || AssetDatabase.GetAssetPath (tex) != "");
		bool saved = false;
		bool cond;
		if (button_triggered) {
			cond = GUILayout.Button ("Save to file", GUILayout.MaxWidth (buttonwidth));
		} else {
			cond = true;
		}
		if (cond) {
			string directory;
			string file;
			if (save_path == "") {
				directory = Application.dataPath;
				file = default_name;
			} else {
				directory = Path.GetDirectoryName (save_path);
				file = Path.GetFileNameWithoutExtension (save_path) + ".png";
			}

			string path = save_path + default_name + ".png";
			if (path != "") {

				save_path = path;

				Texture2D ntex = new Texture2D (tex.width, tex.height, TextureFormat.RGBA32, true);
				for (int mip = 0; mip < tex.mipmapCount; mip++) {
					Color32[] cols = tex.GetPixels32 (mip);
					ntex.SetPixels32 (cols, mip);
					ntex.Apply (false, false);
				}					
				byte[] bytes = ntex.EncodeToPNG ();
				ntex = null;
				System.IO.File.WriteAllBytes (path, bytes);
				AssetDatabase.Refresh (ImportAssetOptions.ForceUpdate);
				AssetDatabase.SaveAssets ();


				TextureImporter textureImporter = AssetImporter.GetAtPath (path) as TextureImporter;
				textureImporter.textureFormat = textureFormat; 
				textureImporter.mipmapEnabled = mipmapEnabled; 
				textureImporter.linearTexture = sRGB_flag;
				textureImporter.maxTextureSize = _maxSize;
				textureImporter.isReadable = true;
				textureImporter.textureCompression = TextureImporterCompression.Uncompressed;
				AssetDatabase.ImportAsset (path, ImportAssetOptions.ForceUpdate);
				tex = (Texture2D)AssetDatabase.LoadAssetAtPath (path, typeof(Texture2D));
				saved = true;
			}		
		}
		EditorGUI.EndDisabledGroup ();
		return saved;
	}

}
#endif
