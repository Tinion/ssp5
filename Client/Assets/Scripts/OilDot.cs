﻿using UnityEngine;
using System.Collections;

public class OilDot : MonoBehaviour {


    public CounterTimerF deathTimer;
    
	void Update () {
	
       if (deathTimer.isEnd(Time.deltaTime))
       {
           Main.instance.oilDots.Remove(this);
           Destroy(gameObject);
       }
	}
}
