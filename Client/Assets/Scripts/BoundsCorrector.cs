﻿using UnityEngine;
using System.Collections;

public class BoundsCorrector : MonoBehaviour {

	public bool isOne = true;
	public MeshFilter[] MF;
	public bool isResize = false;

	public void Resize()
	{
		
		if (!isResize) 
		{
			
			if (isOne) 
			{
				
				MF = new MeshFilter[1];
				MF [0] = transform.GetComponent<MeshFilter> ();

				Bounds bounds = MF [0].mesh.bounds;
				Vector3 size = bounds.size;
				Vector3 center = bounds.center;
				size.y += 60.0f;
				center.y -= 30.0f;
				bounds = new Bounds (bounds.center, size);
				MF [0].mesh.bounds = bounds;



			} 
			else 
			{
				MF = transform.GetComponentsInChildren<MeshFilter> ();
				for (int i = 0; i < MF.Length; i++) {
					Bounds bounds = MF [i].mesh.bounds;
					Vector3 size = bounds.size;
					Vector3 center = bounds.center;
					size.y += 60.0f;
					center.y -= 30.0f;
					bounds = new Bounds (bounds.center, size);
					MF [i].mesh.bounds = bounds;
				}


			}

			isResize = true;
		}
	}

	void OnDrawGizmos()
	{
        /*
		if (isOne)
			Gizmos.color = Color.green;
		else
			Gizmos.color = Color.yellow;
		
		if (MF!=null)
		for (int i = 0; i < MF.Length; i++) 
		{
				if (MF[i]!=null)	
					Gizmos.DrawWireCube (MF[i].transform.position + MF[i].mesh.bounds.center, MF[i].mesh.bounds.size);

		}
        */
	}

}
