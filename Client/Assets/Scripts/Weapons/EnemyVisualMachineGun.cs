﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Dest.Math;
using Dest.Math.Tests;

public class EnemyVisualMachineGun : MachineGun {

    public float azimuth;
    public float dispersion;

	public OneSpline spline;
    private Vector3 inversePoint;
    private OneTarget targetDecal;

    public override void StateSwitch()
    {
        if (isAttack)
        {
            UpdateGun();

            

            switch (weaponState)
            {
                case WeaponState.Start:
                    CreateAutoEffect();
                    break;

                case WeaponState.Stop:
                    DestroyAutoEffect();
                    break;

                case WeaponState.Attack:

                    break;

                case WeaponState.Recharge:

                    break;
            }
        }
    }

    public override void GunShoot()
    {
        Vector3 shootPosition = gun.GetShootPosition(transform); 
        Vector3 shootRotation = transform.forward;
        shootRotation.x += Random.Range(-dispersion, dispersion);
        shootRotation.y += Random.Range(-dispersion, dispersion);
        shootRotation.z += Random.Range(-dispersion, dispersion);

        gun.ShootVisualBurst(shootRotation, shootPosition, inversePoint, targetDecal);
    }

    public void GetInversePoint(Vector3 pos, OneTarget _targetDecal)
    {
        inversePoint = pos;
        targetDecal = _targetDecal;
    }

    public override Unit Aim(Vector3 shootDirection, Vector3 shootPosition, float aimRange)
    {
        Vector2 aimTriangleStartPos = Vector2.zero;
        Vector2 aimTriangleLeftPos = Vector2.zero;
        Vector2 aimTriangleRightPos = Vector2.zero;
  
        CalculateAimTriangle(shootDirection, shootPosition, aimRange, out aimTriangleStartPos, out aimTriangleRightPos, out aimTriangleLeftPos);

        Unit target = CalculateHero(aimTriangleLeftPos, aimTriangleRightPos, aimTriangleStartPos);
        if (target == null)
            return CalculateMainUnits(aimTriangleLeftPos, aimTriangleRightPos, aimTriangleStartPos);
        else
            return target;
        
    }

    public Unit CalculateHero(Vector2 aimTriangleLeftPos, Vector2 aimTriangleRightPos, Vector2 aimTriangleStartPos)
    {
		Box2 box2 = TrafficController.CreateBox2(Main.instance.hero.transform, Main.instance.hero.vehicle.currentCollider);
            Vector2[] points2 = box2.CalcVertices();

             if (Math3d.isTriRectIntersect(aimTriangleLeftPos, aimTriangleRightPos, aimTriangleStartPos, points2[0], points2[1], points2[2], points2[3]))
            {
			return Main.instance.hero.vehicle;
            }  
        return null;
    }

    
}
