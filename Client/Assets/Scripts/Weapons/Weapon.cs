﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Dest.Math;
using Dest.Math.Tests;


[System.Serializable]
public struct CounterTimerI
{
    public int current;
    public int max;

    public CounterTimerI(int _current, int _max)
    {
        current = _current;
        max = _max;
    }

    public bool isEnd(int _step)
    {
        current -= _step;
        if (current < 0)
        {
            current = max;
            return true;
        }
        return false;
    }

    public void Restore()
    {
        current = max;
    }
}

[System.Serializable]
public struct CounterTimerF
{
    public float current;
    public float max;
    public float maxOffset;
    public bool enableOffset;

    public CounterTimerF(float _current, float _max)
    {
        enableOffset = false;
        current = _current;
        max = _max;
        maxOffset = 0;
    }

	public CounterTimerF(float _max, float _maxOffset, bool _enableOffset)
	{
		enableOffset = _enableOffset;
		current = 0;
		max = _max;
		maxOffset = _maxOffset;
	}

    public bool isEnd(float _step)
    {
        current -= _step;
        if (current < 0)
        {
            Restore();
            return true;
        }
        return false;
    }

	public bool isStart(float _step)
	{
		current += _step;
		if (current > max)
		{
			Restore();
			return true;
		}
		return false;
	}

	public float NormalizeTime()
	{
		return current / max;
	}

    public void Restore()
    {
        current = max;
        if (enableOffset)
            current = Random.Range(max - maxOffset, max + maxOffset);
    }

    public string GetToolTipInfo(string _two, string _one)
    {
        string result = "";
        if (enableOffset)
        {
            result += (max - maxOffset).ToString(_two);
            result += " - ";
            result += (max + maxOffset).ToString(_two);
        }
        else
            result += max.ToString(_one);

        return result;
    }
}

[System.Serializable]
public enum WeaponType
{
    Fixed,
    Turret,
    MultiTurret,
}

[System.Serializable]
public enum GunState
{
    Ready,
    Recharge,
    WaitRate,
    NowRecharged,
}

[System.Serializable]
public class Gun
{
    
    private Unit target;
    private OneTarget targetDecal;
    public Transform transform;

    public Vector3 shootDirection;
    public Vector3 shootPosition;

    public Bullet BulletSource;
    public GunState gunState;

    public bool hasTrunks;
    public Vector3[] trunks;

    public float range;
    public float mass;
    public int damage;
    public Unit master;
    

    public CounterTimerI magazine;
    public CounterTimerF bulletRate;
    public CounterTimerF recharge;


    private int trunkSelect = 0;

    public Vector3 GetShootPosition(Transform master)
    {
        Vector3 currentShootPosition;

        if (hasTrunks)
        {
            currentShootPosition = master.TransformPoint(trunks[trunkSelect]);

            if (trunkSelect == trunks.Length - 1)
                trunkSelect = 0;
            else
                trunkSelect++;
        }
        else
            currentShootPosition = master.TransformPoint(shootPosition);

        return currentShootPosition;
    }

    public void ShootBurst(Unit _target, Vector3 attackDirection, Vector3 attackPosition)
    {
        shootDirection = attackDirection;
        target = _target;


        ShootSingle(attackPosition);

        if (magazine.isEnd(1))
        {
            target = null;
            trunkSelect = 0;
            gunState = GunState.Recharge;
        }                     
    }

    public void ShootBurst(Unit _target, Vector3 attackDirection, Vector3 attackPosition, Vector3 endPos)
    {
        shootDirection = attackDirection;
        target = _target;


        ShootSingle(attackPosition, endPos);

        if (magazine.isEnd(1))
        {
            target = null;
            trunkSelect = 0;
            gunState = GunState.Recharge;
        }
    }

    public void ShootVisualBurst(Vector3 attackDirection, Vector3 attackPosition, Vector3 targetPosition, OneTarget _targetDecal)
    {
        shootDirection = attackDirection;

        targetDecal = _targetDecal;

        ShootVisualSingle(attackPosition, targetPosition);

        if (magazine.isEnd(1))
        {
            target = null;
            trunkSelect = 0;
            gunState = GunState.Recharge;
        }
    }

    private void ShootSingle(Vector3 currentShootPosition)
    {
        RaycastHit Hit;
        int layerMask = (1 << 0);// | (1 << 8) | (1 << 9);

        if (target != null)
            shootDirection = Vector3.Normalize(target.centerOfMass.position - currentShootPosition);

        Bullet bullet = GameObject.Instantiate<Bullet>(BulletSource);
        bullet.transform.position = currentShootPosition;
        bullet.transform.rotation = Quaternion.LookRotation(shootDirection);


        

        bullet.Initialize(currentShootPosition, shootDirection, master.transform.forward, master.forwardSpeed, range, master, damage, mass);

        Debug.DrawRay(currentShootPosition, shootDirection * range, Color.blue, 1.0f);
        if (Physics.Raycast(currentShootPosition, shootDirection, out Hit, range))
        {
            //Debug.Log(Hit.collider.tag + " " + Hit.collider.transform.gameObject.layer.ToString() + " " + Hit.collider.transform.gameObject.name);
            Transform targetTransform = Hit.collider.transform;
            if (targetTransform.CompareTag("Unit"))
            {
                //Debug.Log("Select Target");
                bullet.SelectTarget(Hit.distance, targetTransform.position, Hit.normal, Hit.point, target, damage, mass);
                return;
            }

        }
        bullet.bulletDirection = BulletDirection.None;

    }

    private void ShootSingle(Vector3 currentShootPosition, Vector3 endPosition)
    {
        RaycastHit Hit;
        int layerMask = (1 << 0);// | (1 << 8) | (1 << 9);

        if (target != null)
            shootDirection = Vector3.Normalize(target.centerOfMass.position - currentShootPosition);

        Bullet bullet = GameObject.Instantiate<Bullet>(BulletSource);
        bullet.transform.position = currentShootPosition;
        bullet.transform.rotation = Quaternion.LookRotation(shootDirection);
        bullet.endPosition = endPosition;



        bullet.Initialize(currentShootPosition, shootDirection, master.transform.forward, master.forwardSpeed, range, master, damage, mass);

        Debug.DrawRay(currentShootPosition, shootDirection * range, Color.blue, 1.0f);
        if (Physics.Raycast(currentShootPosition, shootDirection, out Hit, range))
        {
            //Debug.Log(Hit.collider.tag + " " + Hit.collider.transform.gameObject.layer.ToString() + " " + Hit.collider.transform.gameObject.name);
            Transform targetTransform = Hit.collider.transform;
            if (targetTransform.CompareTag("Unit"))
            {
                //Debug.Log("Select Target");
                bullet.SelectTarget(Hit.distance, targetTransform.position, Hit.normal, Hit.point, target, damage, mass);
                return;
            }

        }
        bullet.bulletDirection = BulletDirection.None;

    }

    private void ShootVisualSingle(Vector3 currentShootPosition, Vector3 targetPosition)
    {
        Vector3 shootDirection = Vector3.Normalize(targetPosition - currentShootPosition);

        Bullet bullet = GameObject.Instantiate<Bullet>(BulletSource);
        bullet.transform.position = currentShootPosition;
        bullet.transform.rotation = Quaternion.LookRotation(shootDirection);

        bullet.Initialize(currentShootPosition, shootDirection, master.transform.forward, master.forwardSpeed, range, master, damage, mass);
        bullet.SelectTarget(Vector3.Distance(currentShootPosition, targetPosition), currentShootPosition, shootDirection, targetPosition, targetDecal, damage, mass);
        bullet.bulletDirection = BulletDirection.Position;

    }

    public GunState getWeaponState()
   {
       if (gunState != GunState.Recharge)
       {
           if (bulletRate.isEnd(Time.deltaTime))
           {
               gunState = GunState.Ready;
               return gunState;
           }
           gunState = GunState.WaitRate;
           return gunState;
       }
       else
       {
           
           if (recharge.isEnd(Time.deltaTime))
           {
               gunState = GunState.NowRecharged;
               return gunState;
           }
           gunState = GunState.Recharge;
           return gunState;
       }

       return gunState;
   }


}

[System.Serializable]
public enum WeaponState
{
    Start,
    Attack,
    Stop,
    Recharge,

}






[System.Serializable]
public class Weapon : MonoBehaviour
{
    public ModuleWeapon moduleWeapon;
    public Gun gun;
    public bool isAttack;
    public WeaponState weaponState;
    public Transform transform;
    public Vector3 mousePos;
    public float aimRange;

    protected float angle = 0;
    public float rotateSpeed;
    protected float currentVelocity = 0;

    void Start()
    {
        Initialize();
    }

    public virtual void Initialize()
    {
		Main.instance = FindObjectOfType<Main> ();
    }

    
    public virtual void UpdateGun()
    {
        GunState currentGunState;
        currentGunState = gun.getWeaponState();

        if (currentGunState == GunState.NowRecharged)
                weaponState = WeaponState.Start;
        else
        if (currentGunState == GunState.Ready)
        {
                weaponState = WeaponState.Attack;
                GunShoot();
        }
        else
        if (currentGunState == GunState.Recharge)
        {
            if (weaponState == WeaponState.Attack)
            {
                
                weaponState = WeaponState.Stop;
            }
            else
                weaponState = WeaponState.Recharge;
        }
    }

    public virtual void StartShoot()
    {
        if (!isAttack)
        {
            
            weaponState = WeaponState.Start;
            isAttack = true;
        }
    }

    public virtual void StopShoot()
    {
        if (isAttack)
        {
            weaponState = WeaponState.Stop;
            isAttack = false;
        }
    }

    public virtual void Reset()
    {
        StopShoot();
        gun.magazine.Restore();
        gun.recharge.current = 0;
        transform.localRotation = Quaternion.identity;
    }

    public virtual void Remove()
    {

    }

    public virtual void GunShoot()
    {
        Vector3 shootPosition = gun.GetShootPosition(transform);
        Vector3 shootRotation = transform.forward;
        Unit target = Aim(shootRotation, shootPosition, aimRange);
        gun.ShootBurst(target, shootRotation, shootPosition);
    }

    public virtual Unit Aim(Vector3 shootDirection, Vector3 shootPosition, float aimRange)
    {
        Vector2 aimTriangleStartPos = Vector2.zero;
        Vector2 aimTriangleLeftPos = Vector2.zero;
        Vector2 aimTriangleRightPos = Vector2.zero;

        CalculateAimTriangle(shootDirection, shootPosition, aimRange, out aimTriangleStartPos, out aimTriangleRightPos, out aimTriangleLeftPos);

        return CalculateMainUnits(aimTriangleLeftPos, aimTriangleRightPos, aimTriangleStartPos);
        
    }

    public virtual void CalculateAimTriangle(Vector3 shootRotation, Vector3 shootPosition, float aimRange, out Vector2 aimTriangleStartPos, out Vector2 aimTriangleRightPos, out Vector2 aimTriangleLeftPos)
    {
        Vector2 aimDirection = new Vector2(shootRotation.x, shootRotation.z);
        aimDirection.Normalize();
        Vector2 right = new Vector2(transform.right.x, transform.right.z);
        aimTriangleStartPos = new Vector2(shootPosition.x, shootPosition.z);
        Vector2 aimTriangleRangePos = aimDirection * gun.range + aimTriangleStartPos;
        aimTriangleLeftPos = aimTriangleRangePos + right * aimRange;
        aimTriangleRightPos = aimTriangleRangePos - right * aimRange;

        //Debug.DrawLine(new Vector3(aimTriangleStartPos.x, transform.position.y, aimTriangleStartPos.y), new Vector3(aimTriangleLeftPos.x, transform.position.y, aimTriangleLeftPos.y), Color.red, 1.0f);
        //Debug.DrawLine(new Vector3(aimTriangleStartPos.x, transform.position.y, aimTriangleStartPos.y), new Vector3(aimTriangleRightPos.x, transform.position.y, aimTriangleRightPos.y), Color.red, 1.0f);
        //Debug.DrawLine(new Vector3(aimTriangleLeftPos.x, transform.position.y, aimTriangleLeftPos.y), new Vector3(aimTriangleRightPos.x, transform.position.y, aimTriangleRightPos.y), Color.red, 1.0f);
    }

    public Unit CalculateMainUnits(Vector2 aimTriangleLeftPos, Vector2 aimTriangleRightPos, Vector2 aimTriangleStartPos)
    {
        List<Unit> targets = new List<Unit>();

        float minimum = gun.range * aimRange;
        int select = -1;
        int count = Main.instance.unitObjects.Count;
        for (int i=0; i<count; i++)
        {
            if (Main.instance.unitObjects[i].currentGameObject.activeInHierarchy && Main.instance.unitObjects[i].isActive)
            {
                Box2 box = TrafficController.CreateBox2(Main.instance.unitObjects[i].transform, Main.instance.unitObjects[i].currentCollider);
                Vector2[] points = box.CalcVertices();
                if (Math3d.isTriRectIntersect(aimTriangleLeftPos, aimTriangleRightPos, aimTriangleStartPos, points[0], points[1], points[2], points[3]))
                {
                    //Debug.DrawLine(Main.instance.unitObjects[i].transform.position, Main.instance.unitObjects[i].transform.position + Vector3.up * 8.0f, Color.magenta, 5.0f);
                    targets.Add(Main.instance.unitObjects[i]);
                    float min = Mathf.Abs(transform.InverseTransformPoint(Main.instance.unitObjects[i].transform.position).x);
                    if (min < minimum)
                    {
                        select = i;
                        minimum = min;
                    }
                    
                }
                
            }
                
        }

        if (select > -1)
        {
            return Main.instance.unitObjects[select];
        }
        else
            return null;
    }

    void Update()
    {
        WeaponUpdate();
    }

    public virtual void WeaponUpdate()
    {
    }
    public void GetTransform(Vector3 pos, float _angle)
    {
        angle = _angle;
        mousePos = pos;
    }

    
}
