﻿using UnityEngine;
using System.Collections;
using Dest.Math;

public class FragmentGuns : Weapon
{
    bool isInitializeDecal = false;
    private Vector3 inversePoint;
    
    public OneArcMachine targetDecalSource;
    public OneArcMachine targetDecal;
    public Animation currentAnimation;

    public float minDist;
    public float maxDist;
    public float azimut;
    void FixedUpdate()
    {
        if (Main.instance.gameMode == Mode.Battle)
        {
            if (!targetDecal)
            {
                targetDecal = Instantiate<OneArcMachine>(targetDecalSource);
                targetDecal.Create(20.0f, Main.instance.hero.transform);
                targetDecal.transform.parent = Main.instance.mainCamera.targetController.transform;
                targetDecal.targetController = Main.instance.mainCamera.targetController;
            }

            targetDecal.transform.localPosition = targetDecal.targetController.transform.InverseTransformPoint(mousePos);
            targetDecal.transform.LookAt(Main.instance.hero.transform);
            Vector3 dir = Vector3.Normalize(targetDecal.transform.position - Main.instance.hero.transform.position);
            float angle = Math3d.GetAngle(Main.instance.hero.transform.forward.ToVector2XZ(), dir.ToVector2XZ());
            float dist = Vector3.Distance(Main.instance.hero.transform.position, targetDecal.transform.position);
            if (Mathf.Abs(angle) < azimut && dist > minDist && dist < maxDist)
                targetDecal.transform.Find("decalIn").gameObject.SetActive(true);
            else
                targetDecal.transform.Find("decalIn").gameObject.SetActive(false);

            if (isAttack)
            {

                UpdateGun();

                switch (weaponState)
                {
                    case WeaponState.Start:
                        break;

                    case WeaponState.Stop:
                        currentAnimation.Stop("Shoot");
                        break;

                    case WeaponState.Attack:
                        currentAnimation.PlayQueued("Shoot", QueueMode.CompleteOthers);
                        break;

                    case WeaponState.Recharge:
                        currentAnimation.Stop("Shoot");
                        break;
                }
            }
        }

    }

    public override void Reset()
    {
        StopShoot();
        currentAnimation.Stop("Shoot");
        transform.localRotation = Quaternion.identity;
    }


    public override void Remove()
    {
        if (targetDecal!=null)
        Destroy(targetDecal.gameObject);
    }
    public override void GunShoot()
    {
        Vector3 shootPosition = gun.GetShootPosition(transform);
        Vector3 shootRotation = transform.forward;
        Unit target = Aim(shootRotation, shootPosition, aimRange);
        Vector3 dir = Vector3.Normalize(targetDecal.transform.position - Main.instance.hero.transform.position);
        float angle = Math3d.GetAngle(Main.instance.hero.transform.forward.ToVector2XZ(), dir.ToVector2XZ());
        float dist = Vector3.Distance(Main.instance.hero.transform.position, targetDecal.transform.position);
        Vector3 targetPos = targetDecal.transform.position;
        //targetPos.x += Random.Range(-1, 1);
        //targetPos.y += Random.Range(-1, 1);
        //targetPos.z += Random.Range(-1, 1);
        if (Mathf.Abs(angle) < azimut && dist > minDist && dist < maxDist)
        {
            gun.ShootVisualBurst(shootRotation, shootPosition, targetPos, targetDecal);
            Main.instance._temp++;
        }
       // else
           // gun.ShootBurst(null, shootRotation, shootPosition);
    }

}
