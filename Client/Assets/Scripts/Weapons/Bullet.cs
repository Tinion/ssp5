﻿using UnityEngine;
using System.Collections;

public enum BulletDirection
{
    None,
    Target,
    Position,
}
public abstract class Bullet : MonoBehaviour
{

    public BulletDirection bulletDirection;

    public string name;
    public string type;

    public Transform transform;
    public float attackSpeed;
    public float lenght;
    public float width;
    public Object muzzleEffect;
    public Object impactEffect;
    protected Unit master;

    protected bool onHit = false;

    protected float damage;
    protected float mass;
    protected float parentSpeed;

    protected Vector3 offsetDirection;

    protected Unit target;
    protected OneTarget targetDecal;
    protected Vector3 controlPosition;


    protected Vector3 targetStartPosition;
    protected Vector3 startPosition;
    public Vector3 endPosition;
    protected float range;
    protected float distance;

    protected GameObject muzzle;
    protected GameObject impact;

    protected Vector3 normal;
    protected Vector3 attackDir;
    protected Vector3 parentDir;

    public float radiusBullet;

    public CounterTimerF death;

    public float offset = 0.05f;
    public abstract void Initialize(Vector3 _position, Vector3 _direction, Vector3 _parentDir, float _parentSpeed, float _range, Unit _master, float _damage, float _mass);


    public abstract void SelectTarget(float _distance, Vector3 _targetStartPosition, Vector3 _normal, Vector3 _endPosition, Unit _target, float _damage, float _mass);


    public abstract void SelectTarget(float _distance, Vector3 _targetStartPosition, Vector3 _normal, Vector3 _endPosition, OneTarget _target, float _damage, float _mass);


    
}
