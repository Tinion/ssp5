﻿using UnityEngine;
using System.Collections;

public class PlasmaBullet : Bullet
{
    public SphereCollider sphereCollider;
    public CounterTimerF healthPoints;
    public Object crater;
    public Light light;
    

    public override void Initialize(Vector3 _position, Vector3 _direction, Vector3 _parentDir, float _parentSpeed, float _range, Unit _master, float _damage, float _mass)
    {
        attackDir = _direction;
        master = _master;

        attackDir.x += Random.Range(-0.05f, 0.05f);
        attackDir.y += Random.Range(-0.05f, 0.05f);
        attackDir.z += Random.Range(-0.05f, 0.05f);

        parentDir = _parentDir;
        parentSpeed = _parentSpeed;
        startPosition = _position;
        range = _range;
        if (muzzleEffect != null)
        {
            muzzle = GameObject.Instantiate(muzzleEffect, _position, transform.rotation) as GameObject;
            muzzle.transform.parent = master.transform;
        }

    }
    void CreateFire(Unit unit)
    {
        GameObject newFire = GameObject.Instantiate(impactEffect, Vector3.zero, Quaternion.identity) as GameObject;
        newFire.transform.parent = unit.transform;
        newFire.GetComponent<DigitalRuby.PyroParticles.FireBaseScript>().master = unit;
        newFire.transform.localPosition = Vector3.zero;
        newFire.transform.localRotation = Quaternion.identity;
    }
    void OnTriggerStay(Collider other)
    {
        bool isMaster = false;
        if (other.gameObject.CompareTag("Unit"))
        {
            Unit unit = other.transform.parent.GetComponent<Unit>();
            if (unit != master)
            {
                if (unit.unitType == UnitType.Hero || unit.unitType == UnitType.Enemy)
                {
                    unit.GetDamageSplash(damage);
                    if (!unit.isBurned)
                    {
                        unit.isBurned = true;
						CreateFire(Main.instance.hero.vehicle);
                    }
                    unit.currentRigidbody.AddForce(mass * 70.0f * Vector3.Normalize(unit.transform.position - startPosition), ForceMode.Force);
                    
                }
            }
            else
                isMaster = true;
            
            
        }

        if (!isMaster)
        {
            light.intensity = healthPoints.current / healthPoints.max * 2.7f;
            if (healthPoints.isEnd(1.0f))
            {
                if (other.gameObject.CompareTag("Block"))
                {
                    RaycastHit hit;
                    if (Math3d.RayCastGroundDown(transform.position + Vector3.up * 3, out hit))
                    {
                        GameObject.Instantiate(crater, hit.point, Quaternion.Euler(new Vector3(0, Random.Range(0,360.0f), 0)));
                    }
                }
                Destroy(this.gameObject);
            }
        }

        
    }

    public override void SelectTarget(float _distance, Vector3 _targetStartPosition, Vector3 _normal, Vector3 _endPosition, Unit _target, float _damage, float _mass)
    {

        distance = _distance;
        targetStartPosition = _targetStartPosition;
        normal = _normal;
        endPosition = _endPosition;
        if (_target != null)
            target = _target;
        bulletDirection = BulletDirection.Target;

        damage = _damage;
        mass = _mass;

        offsetDirection.x = -normal.x + Random.Range(-0.5f, 0.5f);
        offsetDirection.y = -normal.y + Random.Range(-0.5f, 0.5f);
        offsetDirection.z = -normal.z + Random.Range(-0.5f, 0.5f);

        offsetDirection.Normalize();

    }

    public override void SelectTarget(float _distance, Vector3 _targetStartPosition, Vector3 _normal, Vector3 _endPosition, OneTarget _target, float _damage, float _mass)
    {

        distance = _distance;
        targetStartPosition = _targetStartPosition;
        normal = _normal;
        endPosition = _endPosition;
        targetDecal = _target;
        controlPosition = targetDecal.transform.InverseTransformPoint(_endPosition);
        bulletDirection = BulletDirection.Position;
        

        damage = _damage;
        mass = _mass;

        offsetDirection.x = -normal.x + Random.Range(-0.5f, 0.5f);
        offsetDirection.y = -normal.y + Random.Range(-0.5f, 0.5f);
        offsetDirection.z = -normal.z + Random.Range(-0.5f, 0.5f);

        offsetDirection.Normalize();

    }

    void Update()
    {

        {
            transform.position += parentDir * parentSpeed * Time.smoothDeltaTime;
            transform.position += attackDir * attackSpeed;
        }




    }

  
}
