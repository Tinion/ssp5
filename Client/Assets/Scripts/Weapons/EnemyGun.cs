﻿using UnityEngine;
using System.Collections;

public class EnemyGun : Weapon {

    public float azimuth;
    public float humanFactor;
    public Vector3 barrelDirection;
    public Transform gunTransform;

    protected float currenthumanFactor;
    
    public Unit target;

    public override void Initialize()
    {
        barrelDirection = gunTransform.forward;
    }

    void FixedUpdate()
    {
        Step();
    }

    public virtual void Step()
    {
        if (!isAttack)
            return;

        if (currenthumanFactor > 0)
        {
            currenthumanFactor -= Time.deltaTime;
            return;
        }

        UpdateGun();

        //if (weaponState == WeaponState.Start)
        //    CalculateHumanFactor();

      
    }

    public override void GunShoot()
    {
        Vector3 shootPosition = gun.GetShootPosition(gun.transform);
        gunTransform.LookAt(target.transform.position);
        Vector3 shootDirection = transform.InverseTransformDirection(gunTransform.forward);
        float currentAzimuth = Mathf.Abs(Math3d.GetAngle(shootDirection.x, shootDirection.z, barrelDirection.x, barrelDirection.z));
        if (currentAzimuth > azimuth)
        {
            CalculateHumanFactor();
            return;
        }

        gun.ShootBurst(target, gunTransform.forward, shootPosition);
    }

    public virtual void Attack()
    {
        if (!isAttack)
        {
            isAttack = true;
            CalculateHumanFactor();
        }
    }

    public virtual void CalculateHumanFactor()
    {
        currenthumanFactor = Random.Range(0, humanFactor);
    }

    public virtual void Stop()
    {
        isAttack = false;
    }

    
}
