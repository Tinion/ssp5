﻿using UnityEngine;
using System.Collections;

public class MachineGun : Weapon
{
    protected GameObject[] effectFireCurrent;
    public Object effectFire;
    protected bool isEffectCreated = false;
    
    
    
    public virtual void CreateAutoEffect()
    {
        if (!isEffectCreated && effectFire!=null)
        {

            isEffectCreated = true;
            if (gun.hasTrunks)
            {
                effectFireCurrent = new GameObject[gun.trunks.Length];
                for (int i=0; i<gun.trunks.Length; i++)
                {

                    effectFireCurrent[i] = GameObject.Instantiate(effectFire, transform.TransformPoint(gun.trunks[i]), transform.rotation) as GameObject;
                    effectFireCurrent[i].transform.parent = transform;
                }
            }
            else
            {
                effectFireCurrent = new GameObject[1];
                effectFireCurrent[0] = GameObject.Instantiate(effectFire, transform.TransformPoint(gun.shootPosition), transform.rotation) as GameObject;
                effectFireCurrent[0].transform.parent = transform;
            }
        }
    }


    public virtual void DestroyAutoEffect()
    {
        if (effectFireCurrent != null && isEffectCreated)
        {
            
            if (!gun.hasTrunks)
            {
                Destroy(effectFireCurrent[0]);
            }
            else
            for (int i = 0; i < gun.trunks.Length; i++)
            {
                Destroy(effectFireCurrent[i]);
            }
            isEffectCreated = false;
        }


    }

    public override void Reset()
    {
        base.Reset(); 
        DestroyAutoEffect();
    }

    void FixedUpdate()
    {
        StateSwitch();

       
    }

    public override void StopShoot()
    {
        if (isAttack)
        {
            weaponState = WeaponState.Stop;
            isAttack = false;
            DestroyAutoEffect();
        }
    }

    public virtual void StateSwitch()
    {
        if (isAttack)
        {
            UpdateGun();

             switch (weaponState)
            {
                case WeaponState.Start:
                    CreateAutoEffect();
                    break;

                case WeaponState.Stop:
                    DestroyAutoEffect();
                    break;

                case WeaponState.Attack:

                    break;

                case WeaponState.Recharge:

                    break;
           }
        }
    }
}
