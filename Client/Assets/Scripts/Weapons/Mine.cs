﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Mine : Unit
{
    public float range;
    public Object explosive;
    public CounterTimerF timer;
    public bool isDetonation = false;
    public Unit target;
    public Unit master;

    public float healthMax;
    public float healthCurrent;

    private Vector3 parentDir;
    private float parentSpeed;
    
    void Update()
    {
        if (master == null)
            master = this;

        //transform.position += parentDir * parentSpeed * Time.deltaTime;

        if (Vector3.Distance(master.transform.transform.position, transform.transform.position) < 4.0f)
        {
            currentRigidbody.AddForce((transform.position - master.transform.position), ForceMode.Impulse);
            //Debug.DrawLine(master.transform.transform.position, transform.position, Color.green);
        }

        

        if (!isDetonation)
        {

            List<Unit> possibleTargets = new List<Unit>();

               if ((Vector3.Distance(Main.instance.hero.transform.position, transform.position)<range) && Main.instance.hero != master)
                {
                    isDetonation = true;
                    timer.current = timer.max;
				possibleTargets.Add(Main.instance.hero.vehicle);
                }
        

                for (int i = 0; i < Main.instance.unitObjects.Count; i++)
                {
                     if (Main.instance.unitObjects[i].currentGameObject.activeInHierarchy && Main.instance.unitObjects[i].isActive && (Main.instance.unitObjects[i].unitType == UnitType.Enemy))
                     {
                         if (Vector3.Distance(Main.instance.unitObjects[i].transform.position, transform.position) < range)
                        {
                            if (Main.instance.unitObjects[i] != master)
                            {
                                isDetonation = true;
                                timer.current = timer.max;
                                possibleTargets.Add(Main.instance.unitObjects[i]);
                            }
                        }
                     }
                 }

                if (possibleTargets.Count != 0)
                {
                    float min = 1000.0f;
                    for (int i = 0; i < possibleTargets.Count; i++)
                    {
                        float thisMin = Vector3.Distance(possibleTargets[i].transform.position, transform.position);
                        if (thisMin < min)
                        {
                            min = thisMin;
                            target = possibleTargets[i];
                        }
                    }

                    //currentRigidbody.AddForce(Vector3.Normalize(target.transform.position - transform.position) * 6.0f, ForceMode.VelocityChange);
                }
            else


                isDetonation = false;
        }
        else
        {
            //Debug.DrawLine(transform.position, target.transform.position, Color.red);

            float charge = 1 - (timer.current / timer.max);
            float speed = Mathf.Lerp(0.25f, 1.0f, charge);

            //float dist = 
            Vector3 dir = Vector3.Normalize(target.transform.position - transform.position);
            currentRigidbody.MovePosition(transform.position + dir * target.forwardSpeed * speed * Time.deltaTime);
            //currentRigidbody.AddForce( *  * 130.5f, ForceMode.Impulse);
            //currentRigidbody.AddForce(target.transform.forward * 40.0f, ForceMode.Acceleration);

            if (timer.isEnd(Time.deltaTime))
            {
                Explosive(transform.position);
            }

            currentRenderer.material.SetColor("_Color", new Color(1.0f * charge, 0.5f * charge, 0.5f * charge, 1.0f));
        }

        if (currentRigidbody.velocity.sqrMagnitude > 2025.0f)
            Explosive(transform.position);

        //DropText(currentRigidbody.velocity.magnitude.ToString());
    }

    public void Initialize(Unit _unit)
    {
        master = _unit;
        timer.max = Random.Range(1.1f, 2.8f);
    }

    void Explosive(Vector3 Position)
    {

        Collider[] victims = Physics.OverlapSphere(Position, 5.0f);
        for (int i = 0; i < victims.Length; i++)
        {
            if (victims[i].gameObject.CompareTag("Unit"))
            {
                if (victims[i].attachedRigidbody != null)
                    victims[i].attachedRigidbody.AddExplosionForce(2000.0f, Position, 10.0f, 100.0F, ForceMode.Impulse);
                Unit unit = victims[i].GetComponent<Unit>();
                if (unit != null)
                    unit.GetDamage(Position, 10000.0f * Vector3.Normalize(unit.transform.position - Position));
                 
            }
        }

        GameObject.Instantiate(explosive, Position, Quaternion.identity);
        GameObject.Destroy(gameObject);
    }

    



    
}
