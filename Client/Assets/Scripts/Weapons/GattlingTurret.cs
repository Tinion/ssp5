﻿using UnityEngine;
using System.Collections;

public class GattlingTurret : MachineGun
{
    Vector3 currentVelocityV = Vector3.zero;
    void Update()
    {
        WeaponUpdate();
    }

    public override void WeaponUpdate()
    {
        if (Main.instance.gameMode == Mode.Battle)
        {
            gun.transform.rotation = Quaternion.LookRotation(Vector3.Normalize(mousePos - gun.transform.position));
            /*
            Quaternion newRot = 

            Vector3 camRot = newRot.eulerAngles;
            Vector3 SmoothRot = Vector3.zero;
            SmoothRot.x = Mathf.SmoothDampAngle(transform.eulerAngles.x, camRot.x, ref currentVelocityV.x, rotateSpeed);
            SmoothRot.y = Mathf.SmoothDampAngle(transform.eulerAngles.y, camRot.y, ref currentVelocityV.y, rotateSpeed);
            SmoothRot.z = Mathf.SmoothDampAngle(transform.eulerAngles.z, camRot.z, ref currentVelocityV.z, rotateSpeed);
            Quaternion.Euler(SmoothRot);
            */
        }
    }

    public override void GunShoot()
    {
        Vector3 shootPosition = gun.GetShootPosition(transform);
        Vector3 shootRotation = transform.forward;
        gun.ShootBurst(null, shootRotation, shootPosition, mousePos);
        Main.instance._temp++;
        

    }

    
}
