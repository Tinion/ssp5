﻿using UnityEngine;
using System.Collections;

public class Bomb : Shell
{

    public override void Moving()
    {
        if (lifeTime.isEnd(Time.deltaTime))
        {
            Destroy(gameObject);
        }

        if (oneTarget != null)
        {
            float time = lifeTime.current / lifeTime.max;

            transform.position = Vector3.Lerp(oneTarget.transform.position, cameraController.TransformPoint(startPos), time);
            transform.rotation = Quaternion.LookRotation(Vector3.Normalize(oneTarget.transform.position - transform.position));
        }

      }

    void Update()
    {
        Moving();

    }

}
