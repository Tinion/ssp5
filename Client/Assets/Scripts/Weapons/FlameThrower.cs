﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Dest.Math;

public class FlameThrower : Weapon
{
    public CounterTimerF flameThrowerTimer;
    public CounterTimerF prepareFlameThrowerTimer;

    public Object fireDecal;
    public GameObject currentFireDecal;
    public SkillStatus skillStatus = SkillStatus.Default;

    public Object effectFire;
    private GameObject currentEffectFire;
    public Object effectNapalm;
    private bool isEffectCreated = false;
    public List<Unit> targets;
	// Use this for initialization
    void FixedUpdate()
    {
        if (isAttack)
        {
            
            AreaDamage(transform.forward, transform.position, aimRange);
        }

    }

    public virtual void StartShoot()
    {
        if (!isAttack)
        {
            isAttack = true;
            if (!isEffectCreated)
            {
                isEffectCreated = true;
                currentEffectFire = GameObject.Instantiate(effectFire, Vector3.zero, Quaternion.identity) as GameObject;
                currentEffectFire.transform.parent = transform;
                currentEffectFire.transform.localPosition = Vector3.zero;
                currentEffectFire.transform.localRotation = Quaternion.identity;
            }
            
        }
    }

    void CreateFire(Unit unit)
    {
        GameObject newFire = GameObject.Instantiate(effectNapalm, Vector3.zero, Quaternion.identity) as GameObject;
        newFire.transform.parent = unit.transform;
        newFire.GetComponent<DigitalRuby.PyroParticles.FireBaseScript>().master = unit;
        newFire.transform.localPosition = Vector3.zero;
        newFire.transform.localRotation = Quaternion.identity;
    }

    public virtual void StopShoot()
    {
        if (isAttack)
        {
            isEffectCreated = false;
            isAttack = false;
        }
    }

    public virtual void UpdateGun()
    {
        GunState currentGunState;
        currentGunState = gun.getWeaponState();

        if (currentGunState == GunState.NowRecharged)
            weaponState = WeaponState.Start;
        else
            if (currentGunState == GunState.Ready)
            {
                weaponState = WeaponState.Attack;
                GunShoot();
            }
            else
                if (currentGunState == GunState.Recharge)
                {
                    if (weaponState == WeaponState.Attack)
                    {

                        weaponState = WeaponState.Stop;
                    }
                    else
                        weaponState = WeaponState.Recharge;
                }
    }

    public void AreaDamage(Vector3 shootDirection, Vector3 shootPosition, float aimRange)
    {
        Vector2 aimDirection = new Vector2(shootDirection.x, shootDirection.z);
        aimDirection.Normalize();

        Vector2 aimTriangleStartPos = Vector2.zero;
        Vector2 aimTriangleLeftPos = Vector2.zero;
        Vector2 aimTriangleRightPos = Vector2.zero;
        Line2 line = new Line2(aimTriangleStartPos, aimDirection);

        CalculateAimTriangle(shootDirection, shootPosition, aimRange, out aimTriangleStartPos, out aimTriangleRightPos, out aimTriangleLeftPos);

		Box2 box2 = TrafficController.CreateBox2(Main.instance.hero.transform, Main.instance.hero.vehicle.currentCollider);
        Vector2[] points2 = box2.CalcVertices();

        if (Math3d.isTriRectIntersect(aimTriangleLeftPos, aimTriangleRightPos, aimTriangleStartPos, points2[0], points2[1], points2[2], points2[3]))
        {
            Vector2 point = Main.instance.hero.transform.position.ToVector2XZ();
            float distance = Dest.Math.Distance.Point2Line2(ref point, ref line);

			if (!Main.instance.hero.vehicle.isBurned)
            {
				Main.instance.hero.vehicle.isBurned = true;
				CreateFire(Main.instance.hero.vehicle);
            }
            Main.instance.hero.GetDamageSplash(gun.damage);

        }

        int count = Main.instance.unitObjects.Count;
        for (int i = 0; i < count; i++)
        {
            if (Main.instance.unitObjects[i].currentGameObject.activeInHierarchy && Main.instance.unitObjects[i].isActive && Main.instance.unitObjects[i] != gun.master)
            {
                Box2 box = TrafficController.CreateBox2(Main.instance.unitObjects[i].transform, Main.instance.unitObjects[i].currentCollider);
                Vector2[] points = box.CalcVertices();
                if (Math3d.isTriRectIntersect(aimTriangleLeftPos, aimTriangleRightPos, aimTriangleStartPos, points[0], points[1], points[2], points[3]))
                {
                    Vector2 point = Main.instance.unitObjects[i].transform.position.ToVector2XZ();
                    float distance = Dest.Math.Distance.Point2Line2(ref point, ref line);
  
                    if (!Main.instance.unitObjects[i].isBurned)
                    {
                        Main.instance.unitObjects[i].isBurned = true;
                        CreateFire(Main.instance.unitObjects[i]);
                    }

                    Main.instance.unitObjects[i].GetDamageSplash(gun.damage);
                }

            }

        }

    }

    void Update()
    {
        StateController();
    }

    public virtual void CalculateAimTriangle(Vector3 shootRotation, Vector3 shootPosition, float aimRange, out Vector2 aimTriangleStartPos, out Vector2 aimTriangleRightPos, out Vector2 aimTriangleLeftPos)
    {
        Vector2 aimDirection = new Vector2(shootRotation.x, shootRotation.z);
        aimDirection.Normalize();
        Vector2 right = new Vector2(transform.right.x, transform.right.z);
        aimTriangleStartPos = new Vector2(shootPosition.x, shootPosition.z);
        Vector2 aimTriangleRangePos = aimDirection * gun.range + aimTriangleStartPos;
        aimTriangleLeftPos = aimTriangleRangePos + right * aimRange;
        aimTriangleRightPos = aimTriangleRangePos - right * aimRange;

        Debug.DrawLine(new Vector3(aimTriangleStartPos.x, transform.position.y, aimTriangleStartPos.y), new Vector3(aimTriangleLeftPos.x, transform.position.y, aimTriangleLeftPos.y), Color.red, 1.0f);
        Debug.DrawLine(new Vector3(aimTriangleStartPos.x, transform.position.y, aimTriangleStartPos.y), new Vector3(aimTriangleRightPos.x, transform.position.y, aimTriangleRightPos.y), Color.red, 1.0f);
        Debug.DrawLine(new Vector3(aimTriangleLeftPos.x, transform.position.y, aimTriangleLeftPos.y), new Vector3(aimTriangleRightPos.x, transform.position.y, aimTriangleRightPos.y), Color.red, 1.0f);
    }

    public void SetTarget(Unit unit)
    {
        if (skillStatus == SkillStatus.Default)
        {
            skillStatus = SkillStatus.BattlePrepare;
            transform.LookAt(unit.transform.position);
            currentFireDecal = GameObject.Instantiate(fireDecal, transform.position, transform.rotation) as GameObject;
            currentFireDecal.transform.parent = transform;

            Vector3 loc = transform.localEulerAngles;
            loc.x = 0; loc.z = 0;
            transform.localEulerAngles = loc;
            prepareFlameThrowerTimer.current = prepareFlameThrowerTimer.max;
        }
    }

    private void StateController()
    {
        switch (skillStatus)
        {

            case SkillStatus.Default:
                {

                    

                }
                break;

            case SkillStatus.BattlePrepare:
                {

                    if (prepareFlameThrowerTimer.isEnd(Time.deltaTime))
                    {
                        Destroy(currentFireDecal);
                        StartShoot();
                        flameThrowerTimer.current = flameThrowerTimer.max;
                        skillStatus = SkillStatus.ExplosiveAttack;
                    }
                }

                break;

            case SkillStatus.ExplosiveAttack:
                {
                    if (flameThrowerTimer.isEnd(Time.deltaTime))
                    {

                        skillStatus = SkillStatus.Default;
                        StopShoot();
                    }
                }

                break;
        }
    }

    
}
