﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Pylon
{
    public Transform transform;
    public bool isUsed;
}

public class MissilePylons : Module
{
    public Animation currentAnimation;
    public bool isLaunched = false;
    public Pylon[] Pylons;
    public CounterTimerF recharge;
    public CounterTimerI magazine;
    public Missile Missile;

    public override void Install()
    {
        base.Install();
    
    }


    public void Launch()
    {
        
            for (int i = 0; i < Pylons.Length; i++)
            {
                if (Pylons[i].isUsed == false)
                {
                    LaunchFromPylon(i);
                    
                    if (i == Pylons.Length - 1)
                    {
                        currentAnimation.Play("Close");
                        recharge.Restore();
                        magazine.Restore();
                        isLaunched = false;
                        for (int j = 0; j < Pylons.Length; j++)
                        {
                            Pylons[j].isUsed = false;
                        }
                    }
                    return;
                }

            }
        
    }

    void LaunchFromPylon(int select)
    {
        Missile _missile = null;
        _missile = GameObject.Instantiate(Missile, Pylons[select].transform.position, Pylons[select].transform.rotation) as Missile;
		_missile.GetComponent<Rigidbody>().velocity = Main.instance.hero.vehicle.currentRigidbody.velocity;
		_missile.GetComponent<Rigidbody>().angularVelocity = Main.instance.hero.vehicle.currentRigidbody.angularVelocity;

        _missile.targetTimer.Restore();
        Pylons[select].isUsed = true;
    }

    public override void Reset()
    {
        magazine.Restore();
        recharge.current = 0;
        isLaunched = false;
    }

    void Update()
    {
        if (Main.instance == null)
            Main.instance = FindObjectOfType<Main>();

        if (Main.instance.gameMode != Mode.Battle)
            return;

        if (recharge.current < 0)
        {
            if (Input.GetButtonDown("Special") && !isLaunched)
            {
                isLaunched = true;
                currentAnimation.Play("Open");
                
                
            }

            ammoSource.ico.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
            ammoSource.back.fillAmount = (float)magazine.current / (float)magazine.max;
            ammoSource.data.text = magazine.current.ToString("F0") + "/" + magazine.max.ToString("F0");
            
        }
        else
        {
            ammoSource.ico.color = new Color(1.0f, 1.0f, 1.0f, 0.2f);
            ammoSource.back.fillAmount = 1 - (recharge.current / recharge.max);
            ammoSource.data.text = "Перезарядка";
        }

        recharge.current-=Time.deltaTime;

        if (isLaunched && (currentAnimation.GetComponent<Animation>()["Open"].time > currentAnimation.GetComponent<Animation>()["Open"].length))
            Launch();

    }

    public override void Shoot()
    {
        if (!isLaunched)
        {
            isLaunched = true;
            currentAnimation.Play("Open");
        }
    }
}
