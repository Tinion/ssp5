﻿using UnityEngine;
using System.Collections;

public class Crush : Unit
{
    public EnemyVehicle master;
    public GarbagePart garbagePart;
    public ParticleSystem effect;
	void Start () {
        //garbagePart.Create();
	}
	
	// Update is called once per frame
    public override void CreateObject()
    {
        transform.parent = master.transform;
        currentGameObject.SetActive(false);
        garbagePart.Create();
        isActive = true;
    }

    public override void PrepareObject()
    {
        effect.Emit(1);
        PlaceGarbage();
        isBurned = false;
    }
    public override void RevertObject()
    {
        effect.Emit(0);
        garbagePart.Revert(this);
    }

    public void PlaceGarbage()
    {
        for (int j = 0; j < garbagePart.GarbageParts.Length; j++)
        {
            garbagePart.GarbageParts[j].velocity = master.currentRigidbody.velocity/2;
            garbagePart.GarbageParts[j].angularVelocity = master.currentRigidbody.angularVelocity;
            garbagePart.GarbageParts[j].AddExplosionForce(Random.Range(600.0f, 1500.0f), garbagePart.GarbagePanel.position, 20.0f, 750.0F, ForceMode.Impulse);
        }
    }

    void Update()
    {

		if (killTimer.isEnd(Time.deltaTime))
		{
			killTimer.Restore ();
			if (isTooFar (killDistance, 5.0f)) 
			{
				Kill ();
				master.currentGameObject.SetActive (false);
				master.Kill ();
			}
		}

    }

	public override void Kill()
	{
		
	}

    /*
    public override bool isTooFar(float _Dist, float _trafficDist)
    {
        Vector3 final = transform.position;
        final -= _Main.mainCamera.transform.position;
        Vector3.Normalize(final);

        float result = Mathf.Acos(Vector3.Dot(final, Math3d.RotateThis(Vector3.forward, -_Main.mainCamera.transform.eulerAngles.y, Vector3.zero)) / (final.magnitude * Vector3.forward.magnitude));
        result *= 180.0f / 3.1415926f;

        //if (!trafficUnit)
        //{

        float Dist = Vector3.Distance(transform.position, _Main.mainCamera.transform.position);
        //DropText(result.ToString("F2") + " " + Dist.ToString("F2"));
        if ((Dist > _Dist && result > 90.0f))
            return true;


        return false;

    }
    */
}
