﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GrassBlock : MonoBehaviour {

	// Use this for initialization
	[ContextMenu("Do")]
	void Do()
	{
		Mesh mesh = new Mesh ();
		mesh = GetComponent<MeshFilter> ().sharedMesh;

     	int[] triangles = mesh.triangles;
     	Vector3[] vertices = mesh.vertices;
     	Vector2[] uv = mesh.uv;
		Color[] colors = mesh.colors;
     	Vector3[] normals = mesh.normals;
		Vector4[] tangents = mesh.tangents;

    	List<Vector3> vertList = new List<Vector3>();
     	List<Vector2> uvList = new List<Vector2>();
	 	List<Color> colList = new List<Color>();
     	List<Vector3> normalsList = new List<Vector3>();
	 	List<Vector4> tangentList = new List<Vector4>();
     	List<int> trianglesList = new List<int>();



		int QuadsCount = mesh.vertexCount / 4;
		int ch = 0;

		for (int i = 0; i < QuadsCount; i++) 
		{
			//Debug.Log (triangles [i*6] + " " + triangles [i*6+1] + " " + triangles [i*6+2] + " " + triangles [i*6+3] + " " + triangles [i*6+4] + " " + triangles [i*6+5]);
			if (i % 2 == 0) 
			{
				
				vertList.AddRange (AddSome (4, vertices, i));
				uvList.AddRange (AddSome (4, uv, i));
				colList.AddRange (AddSome (4, colors, i));
				normalsList.AddRange (AddSome (4, normals, i));
				tangentList.AddRange (AddSome (4, tangents, i));

				int[] tempTriangle = new int[6];
				tempTriangle [0] = 0 + 4*ch;
				tempTriangle [1] = 1 + 4*ch;
				tempTriangle [2] = 2 + 4*ch;
				tempTriangle [3] = 1 + 4*ch;
				tempTriangle [4] = 0 + 4*ch;
				tempTriangle [5] = 3 + 4*ch;
				trianglesList.AddRange (tempTriangle);
				ch++;
			}
		}
     
     
	 	mesh.triangles = trianglesList.ToArray ();

		mesh.vertices = vertList.ToArray ();
		mesh.uv = uvList.ToArray ();
		mesh.normals = normalsList.ToArray ();
		mesh.colors = colList.ToArray ();
		mesh.tangents = tangentList.ToArray ();

		GetComponent<MeshFilter> ().sharedMesh = mesh;

	}

	Vector3[] AddSome(int count, Vector3[] Source, int number)
	{
		Vector3[] current = new Vector3[count];
		for (int i = 0; i < count; i++) 
		{
			current [i] = Source [number * count + i];
		}
		return current;
	}

	Vector4[] AddSome(int count, Vector4[] Source, int number)
	{
		Vector4[] current = new Vector4[count];
		for (int i = 0; i < count; i++) 
		{
			current [i] = Source [number * count + i];
		}
		return current;
	}

	Color[] AddSome(int count, Color[] Source, int number)
	{
		Color[] current = new Color[count];
		for (int i = 0; i < count; i++) 
		{
			current [i] = Source [number * count + i];
		}
		return current;
	}

	Vector2[] AddSome(int count, Vector2[] Source, int number)
	{
		Vector2[] current = new Vector2[count];
		for (int i = 0; i < count; i++) 
		{
			current [i] = Source [number * count + i];
		}
		return current;
	}

	int[] AddSome(int count, int[] Source, int number)
	{
		int[] current = new int[count];
		for (int i = 0; i < count; i++) 
		{
			current [i] = Source [number * count + i];
		}
		return current;
	}
}
