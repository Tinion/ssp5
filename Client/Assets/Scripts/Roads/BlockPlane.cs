﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Dest.Math;
#if UNITY_EDITOR
using UnityEditor;
using System.IO;



[System.Serializable]
public struct BezierPoint
{
	public Vector3 back;
	public Vector3 center;
	public Vector3 front;

}
#endif
[System.Serializable]
public enum BlockPlaneType
{
    Standart,
    Fork,
    Connection,
    Outpost,
}

[System.Serializable]
public enum SplineType
{
    Front = 0,
    Left = 1,
    Right = 2,
	Partial = 3,
}

[System.Serializable]
public class BorderData
{
	public Matrix4x4 _transform;
	public int type;

	public BorderData(Matrix4x4 _m, int _type)
	{
		_transform = _m;
		type = _type;
	}
}

public class BlockPlane : MapObject
{
    public Transform transform;

	public BlockPlaneType type;
	public bool canCreateNextPlane;
	public bool isManualPosition;
	public bool nonKill = false;

    public Collider roadCollider;
    public Collider wallCollider;
	public Mesh sourceCollider;
	public bool ignoreCollider;

    public float point;
	public float size;

	public landscapeType landType;

	public OneSpline spline;
	public OneSpline[] splines;

	#if UNITY_EDITOR
	public List<NearPolygon> nearPolygons;
    #endif
	public int runLine = 0;
	public float localNumber;
	public int localForkNumber;
    
	public Vector3 ManualPosition;

	public string blockName;

    public BlockPlane nextBlock;
    public BlockPlane previousBlock;
    public BlockPlane LeftForkPlane;
    public BlockPlane RightForkPlane;

	public BakedProps mainBorder;
	public List<BakedProps> customBorders;

	public float width;

	public BordersHub bordersHub;

    
	#if UNITY_EDITOR

	public class SubdCheckSide
	{
		public int a;
		public int b;
		public int c;
		public bool isChecked = false;

		public SubdCheckSide(int _a, int _b, int _c)
		{
			a = _a;
			b = _b;
			c = _c;
			isChecked = false;
		}
	}

	[System.Serializable]
	public class NearPolygon
	{
		public List<int> triangleIndex;

		public NearPolygon()
			{
				triangleIndex = new List<int>();
			}
	}

	public class Vertex
	{
		public Vector3 pos;
		public Color color;

		public Vertex(Vector3 _pos, Vector2 _uv, Vector2 _uv2, Color _color)
		{
			pos = _pos;
			color = _color;
		}

		public Vertex(Data data, int _triangleIndex, Color _color)
		{
			pos = data.vertices[_triangleIndex];
			color = _color;
		}

		public Vertex(Data data, int _triangleIndexA, int _triangleIndexB, Color _color)
		{
			pos = Vector3.Lerp(data.vertices[_triangleIndexA],data.vertices[_triangleIndexB], 0.5f);
			color = _color;
		}
	}

	[System.Serializable]
	public class Data
	{
		public List<Vector3> vertices;
		public List<Color> color;
		public int vertexCount;
		public List<int> indexes;


		public List<Vector3i> triangles;
		public List<Vector3i> fullTriangles;
		//public int[] ind;

		public Data(Mesh _mesh, List<Vector3i> _triangles)
		{
			vertexCount = _mesh.vertexCount;

			vertices = new List<Vector3>();
			color = new List<Color>();
			indexes = new List<int>();
			triangles = new List<Vector3i>();
			fullTriangles = new List<Vector3i>();


			vertices.AddRange(_mesh.vertices);

			if (_mesh.colors.Length == 0)
			{
				//Debug.Log("Black");
				for (int i=0; i<vertexCount;i++)
					color.Add(Color.black);
			}
			else
				color.AddRange(_mesh.colors);
			//color.AddRange(color.ToArray());
			indexes.AddRange(_mesh.triangles);

			triangles = _triangles;
			int ch=0;
			for (int i=0; i<indexes.Count; i+=3)
			{
				fullTriangles.Add(new Vector3i(indexes[ch], indexes[ch+1], indexes[ch+2]));
				ch+=3;
			}
		}

		public Data(Mesh _mesh)
		{
			vertexCount = _mesh.vertexCount;

			vertices = new List<Vector3>();
			color = new List<Color>();
			indexes = new List<int>();
			fullTriangles = new List<Vector3i>();


			vertices.AddRange(_mesh.vertices);

			if (_mesh.colors.Length == 0)
			{
				//Debug.Log("Black");
				for (int i=0; i<vertexCount;i++)
					color.Add(Color.black);
			}
			else
				color.AddRange(_mesh.colors);
			//color.AddRange(color.ToArray());
			indexes.AddRange(_mesh.triangles);

				int ch=0;
			for (int i=0; i<indexes.Count; i+=3)
			{
				fullTriangles.Add(new Vector3i(indexes[ch], indexes[ch+1], indexes[ch+2]));
				ch+=3;
			}
		}


		public void AddVertex(Vertex _vertex)
		{
			vertices.Add (_vertex.pos);
			color.Add (_vertex.color);
			vertexCount = vertices.Count;
		}



		public void RecalcIndexes()
		{
			
			List<int> indexes02 = new List<int>();
			for (int i = 0; i < fullTriangles.Count; i++)
			{
				if (!fullTriangles [i].isDelete) 
				{
					indexes02.Add (fullTriangles [i].x);
					indexes02.Add (fullTriangles [i].y);
					indexes02.Add (fullTriangles [i].z);
				}
			}

			indexes = new List<int> ();
			indexes.AddRange(indexes02.ToArray());
			//indexes02.Clear ();

		}

		public Mesh CreateMesh(string _name)
		{
			Mesh _mesh = new Mesh ();
			_mesh.vertices = vertices.ToArray();
			_mesh.colors = color.ToArray();
			_mesh.SetTriangles (indexes, 0, true);
			_mesh.name = _name;
			_mesh.RecalculateNormals ();
			_mesh.RecalculateBounds ();

			return _mesh;
		}
	}

	public enum ColliderEditStep
	{
		None = 0,
		Prepare = 1,
		Tier1 = 2,
		Tier2 = 3,
		Correction = 4,
		Final = 5,
	}

		public bool isMainSettings = false;
		public bool isColliderSettings = false;
		public bool isAdditionalElements = false;
		public bool isSplineSettigns = false;
		public bool isBorderSettigns = false;	
		
		public bool isColliderView = false;
		public bool isColliderBrushMenu = false;
		public bool isColliderEdited = false;
		public bool isTesselation = true;
		public bool isSplineOpen = false;
		public bool isBoardsOpen = false;
		public int splineSelect = -1;
	 	

		public float posCam;
		public float oldPosCam;
		public CameraDot camDot01;
		public CameraDot camDot02;
		public BlockPlane previousPlane;
		public BlockPlane nextPlane;
		public float playerAffectSlider = 1.0f;	

		public Vector3 currentLocalPosition;
		public Vector3 currentLocalRotate;
		public Vector2 currentAxisBendSize;
		public Vector2 currentBias;

		public float tessPower = 2.5f;
		public Mesh editedCollider;
		public Mesh preparedCollider;
		public MeshCollider editMeshCollider;

		//public List<int>[] nearPolygons;
		public int tabSwitch = 0;
		public ColliderEditStep colliderStep;

		public bool isOneShot = false;
		public int cullingColliderDistance = 69;
		public float brushOpacity = 0.1f;
		public float brushSize = 5.0f;
		public Vector2 brushSizeMinMax = new Vector2(0.5f, 10.0f);

		public bool isCanRayCast = false;
		public int verticesCount = 0;
		public int polygonsCount = 0;

		public bool isMassive = true;
		public float dist = 0.25f;
		public float e1 = 1.25f;
		public float e2 = 3.5f;
		public float t = 1.0f;
		public float t2 = 1.0f;

		public Data data;
		
	#endif

    public virtual BlockPlane GetNextBlock(bool direction, SplineType type)
    {
        if (direction)
            return nextBlock;
        else
            return previousBlock;
    }

   // public virtual void SetNextPlane(BlockPlane next)
   // {
   //     nextBlock = next;
   // }
	void Awake()
	{
		if (customBorders == null)
			customBorders = new List<BakedProps> ();
	}
	/*
    void Start()
    {
		Debug.LogError (transform.GetInstanceID());
        Initialize(false);     
    }
	*/

    public virtual void CheckNewCreate()
    {
        float dec = spline.GetStep(Main.instance.hero.transform.position);

        if (dec > 0.15f && canCreateNextPlane)
        {
             canCreateNextPlane = false;
            Main.instance.CreateNew();
            Main.instance.hero.expiriencePoints.current += 20.0f;
        }

        
        
    }
    
	public void CreateBorder(OneSpline _spline, float currentLenght, float borderLength, bool isLeft)
	{

		int count = Mathf.FloorToInt (currentLenght / borderLength);
		float remainder = (currentLenght % borderLength) / count;
		bordersHub.borderSources = new MeshFilter[customBorders.Count + 1];
		bordersHub.borderSources [0] = mainBorder.GetComponent<MeshFilter> ();
		for (int i = 1; i < customBorders.Count + 1; i++)
			bordersHub.borderSources [i] = customBorders [i - 1].GetComponent<MeshFilter> ();

		int ch = 0;
		for (float i = borderLength/2; i <= (currentLenght); i+=(borderLength+remainder) ) 
		{
			float uT = _spline.UniformGlobalLenght (i / currentLenght);//, width);
			Vector3 pos = _spline.GlobalPos2(uT, width);
			Quaternion rot = Quaternion.LookRotation(_spline.GetTangent (uT, width)) * Quaternion.AngleAxis((isLeft) ? 270 : 90, Vector3.up);
			BakedProps newProps = Instantiate<BakedProps> (mainBorder, pos + Vector3.up, rot);
			newProps.Create (this, 0);
			if (ch == 0 || i + (borderLength + remainder) > currentLenght)
				newProps.edge = true;
			if (ch > 0) 
			{
				newProps.previous = bordersHub.borderData [bordersHub.borderData.Count-1].transform;
				bordersHub.borderData [bordersHub.borderData.Count-1].next = newProps.transform;
			}

			bordersHub.borderData.Add (newProps);

			RaycastHit Hit;
			int layerMask = 1 << 0;
			layerMask = ~layerMask;

			if (Physics.Raycast(newProps.transform.position + Vector3.up * 5.0f, Vector3.down, out Hit, Mathf.Infinity, layerMask))
			{
				newProps.transform.position = Hit.point;
				Debug.Log (Hit.point);
			}

			newProps.transform.parent = bordersHub.transform;
			ch++;
		}

	}
	public virtual Quaternion GetSplineRot(float _currentCamPercent)
	{
		return transform.rotation * Quaternion.Euler(spline.GetYAngle(_currentCamPercent));
	}
	public void CreateBorders()
	{
		CheckHub ();

		if (bordersHub.borderData != null)
			for (int i = 0; i < bordersHub.borderData.Count; i++)
			{
				DestroyImmediate (bordersHub.borderData [i].gameObject);
			}
		
		CreateBordersOnSpline (spline);
	}

	private void CheckHub()
	{
		if (bordersHub == null) 
		{
			GameObject bordersGO = new GameObject ();
			bordersGO.transform.parent = transform;
			bordersGO.name = "bordersHub";
			bordersGO.transform.localPosition = Vector3.zero;
			bordersGO.transform.localRotation = Quaternion.identity;
			MeshRenderer MR = bordersGO.AddComponent<MeshRenderer> ();
			#if UNITY_EDITOR
			MR.sharedMaterial = mainBorder.GetComponent<MeshRenderer>().sharedMaterial;
			#else
			MR.material = mainBorder.GetComponent<MeshRenderer>().sharedMaterial;
			#endif
			MeshFilter MF = bordersGO.AddComponent<MeshFilter> ();

			bordersHub = bordersGO.AddComponent<BordersHub> ();
			bordersHub.currentMeshFilter = MF;
			bordersHub.transform = bordersGO.transform;
			bordersHub.boundsCorrector = bordersGO.AddComponent<BoundsCorrector> ();
			bordersHub.master = this;
		}
	}



	public void CreateBordersOnSpline(OneSpline _spline)
	{
		CheckHub ();

		bordersHub.borderData = new List<BakedProps> ();

		Props testLengthMain = Instantiate<Props> (mainBorder);
		float borderLength = testLengthMain.GetComponent<BoxCollider> ().size.Length ();
		DestroyImmediate (testLengthMain.gameObject);

		//CreateBorder (_spline, _spline.CalculateLenghtLeft (width), borderLength, true);
		//CreateBorder (_spline, _spline.CalculateLenghtRight (width), borderLength, false);

	}

	public void CleanBorders()
	{
		CheckHub ();

		if (bordersHub.borderData != null)
			for (int i = 0; i < bordersHub.borderData.Count; i++)
			{
				DestroyImmediate (bordersHub.borderData [i].gameObject);
			}
	}

	public void BakeBorders()
	{
		List<CombineInstance> combine = new List<CombineInstance> ();

		Matrix4x4 myTransform = transform.worldToLocalMatrix;

		for (int i = 0; i < bordersHub.borderData.Count; i++) 
		{
			if (bordersHub.borderData [i].isActive) 
			{
				CombineInstance combineInstance = new CombineInstance ();
				#if UNITY_EDITOR
				combineInstance.mesh = bordersHub.borderSources [bordersHub.borderData [i].type].sharedMesh;
				#else
				combineInstance.mesh = bordersHub.borderSources [bordersHub.borderData [i].type].mesh;
				#endif

				if (Application.isPlaying)
					combineInstance.transform = bordersHub.borderData [i].myTransform;
				else
				{
					combineInstance.transform = myTransform * bordersHub.borderData [i].transform.localToWorldMatrix;
					bordersHub.borderData [i].myTransform = myTransform * bordersHub.borderData [i].transform.localToWorldMatrix;
				}

				combine.Add (combineInstance);

				bordersHub.borderData [i].Clean ();
			}
		}
		#if UNITY_EDITOR
		bordersHub.currentMeshFilter.sharedMesh = new Mesh ();
		bordersHub.currentMeshFilter.sharedMesh.CombineMeshes (combine.ToArray(), true, true);
		bordersHub.currentMeshFilter.sharedMesh.name = "CobineBorders";

		if (!Application.isPlaying)
		{
		string path = "Assets/Art/roads/" + landType.ToString() + "/" + blockName;
		//Debug.Log(path);
		if (!Directory.Exists (path + "/road"))
		{
			AssetDatabase.CreateFolder (path, "road");
			AssetDatabase.Refresh ();
			AssetDatabase.SaveAssets ();
		}


		bordersHub.currentMeshFilter.sharedMesh = PrefabCollector.CreateOrReplaceAssetMesh (bordersHub.currentMeshFilter.sharedMesh, path+"/road/borderMesh.asset");
		}

		#else
		bordersHub.currentMeshFilter.mesh = new Mesh ();
		bordersHub.currentMeshFilter.mesh.CombineMeshes (combine.ToArray(), true, true);
		bordersHub.currentMeshFilter.mesh.name = "CobineBorders";
		#endif

		bordersHub.boundsCorrector.isResize = false;

	}

	public void ReCalculateBorderData()
	{
		for (int i = 0; i < bordersHub.borderData.Count; i++) 
		{
			bordersHub.borderData [i].isActive = true;
			bordersHub.borderData [i].collider.isTrigger = true;
		}
	}








    public virtual Vector4 GetOldBlockTransforms()
    {
        Vector4 oldBlockTransforms = Vector4.zero;
		oldBlockTransforms = (Vector4)transform.TransformPoint(spline.nodes[spline.nodes.Count-1].position);
		Main.instance.blocksYposition += spline.nextY;
		oldBlockTransforms.w = spline.GetAngle(Vector3.forward, Vector3.up,1) + transform.eulerAngles.y;
        return oldBlockTransforms;
    }

    public virtual Vector4 GetOldBlockTransforms(SplineType type)
    {
        Vector4 oldBlockTransforms = Vector4.zero;
		oldBlockTransforms = (Vector4)transform.TransformPoint(spline.nodes[spline.nodes.Count-1].position);
		Main.instance.blocksYposition += spline.nextY;
		oldBlockTransforms.w = spline.GetAngle(Vector3.forward, Vector3.up,1) + transform.eulerAngles.y;
        return oldBlockTransforms;
    }
	public virtual float CheckRoadWidth(float _percent)
	{

			return spline.GetCurrentWidth (_percent);

	}

    public virtual void Create(int _localForkNumber, int _runLine, Vector4 _position)
    {
		UnityEngine.Profiling.Profiler.BeginSample ("First");

		if (!isInitialize)
        	Initialize(true);
		
		UnityEngine.Profiling.Profiler.EndSample ();

		UnityEngine.Profiling.Profiler.BeginSample ("Data");
        localNumber = Main.instance.currentPlane.localNumber + Main.instance.currentPlane.point;

        Main.instance.currentPlane.nextBlock = this;
        previousBlock = Main.instance.currentPlane;
        Main.instance.currentPlane = this;

        
        localForkNumber = Main.instance.planeRun;
        runLine = Main.instance.forkRun;
        canCreateNextPlane = true;
		UnityEngine.Profiling.Profiler.EndSample ();

		UnityEngine.Profiling.Profiler.BeginSample ("Position");
        if (!isManualPosition)
        {
			float localYangle = spline.GetAngle(Vector3.forward, Vector3.up,0);
            float newRotation = _position.w - localYangle;

            transform.position = Vector3.zero;
            transform.rotation = Quaternion.Euler(new Vector3(0.0f, newRotation, 0.0f));

			Vector3 newPosition = (Vector3)_position - transform.TransformPoint(spline.nodes[0].position);
            newPosition.y = Main.instance.blocksYposition;
            
            transform.position = newPosition;           
        }
        else
        CreateWithManualPosition();
		UnityEngine.Profiling.Profiler.EndSample ();

		UnityEngine.Profiling.Profiler.BeginSample ("Shit");

		if (type != BlockPlaneType.Fork) 
		{
			Main.instance.planeRun++;
		}

		if (type == BlockPlaneType.Standart)
        	Main.instance.AddAndDeletePlanes(this);

		if (type == BlockPlaneType.Fork)
			Main.instance.BlockPlanes.Add (this);
		UnityEngine.Profiling.Profiler.EndSample ();


//		Debug.Log ("Create BlockPlane");
		if (bordersHub != null) 
		{
			UnityEngine.Profiling.Profiler.BeginSample ("ReCalculateBorderData");
			ReCalculateBorderData ();
			UnityEngine.Profiling.Profiler.EndSample ();

			UnityEngine.Profiling.Profiler.BeginSample ("BakeBorders");
			BakeBorders ();
			UnityEngine.Profiling.Profiler.EndSample ();
		}
    }

    public virtual void CreateSubPlane(SplineType splineType, Fork forkCurrent)
    {

        if (!isInitialize)
            Initialize(true);

        canCreateNextPlane = true;
        
        currentGameObject.tag = "Untagged";
        nonKill = true;


        Vector4 oldTransforms = forkCurrent.GetOldBlockTransforms(splineType);

        if (!isManualPosition)
        {
			float localYangle = spline.GetAngle(Vector3.forward, Vector3.up,0);
            float newRotation = oldTransforms.w - localYangle;

            transform.position = Vector3.zero;
            transform.rotation = Quaternion.Euler(new Vector3(0.0f, newRotation, 0.0f));

			Vector3 newPosition = (Vector3)oldTransforms - transform.TransformPoint(spline.nodes[0].position);
            newPosition.y = Main.instance.blocksYposition;

            transform.position = newPosition;
        }
        else
           CreateWithManualPosition();
    }

    public virtual void Additive()
    {

    }
    public virtual void DestroyThis()
    {
        /*
        ArrayList Temp = new ArrayList();
        foreach (SpawnDot current in Main.instance.spawnDots)
        {
            if (localForkNumber == current.planeNumber && runLine == current.forkNumber)
                Temp.Add(current);
        }
        foreach (SpawnDot current in Temp)
        {
            Main.instance.spawnDots.Remove(current);
            GameObject.Destroy(current.gameObject);
        }
        */
       // Temp = new ArrayList();
        //foreach (Misc current in Main.instance.miscs)
        //{
        //    if (localForkNumber == current.planeNumber && runLine == current.forkNumber)
        //        Temp.Add(current);
        //}
        //foreach (Misc current in Temp)
        //{
        //    Main.instance.miscs.Remove(current);
       //     GameObject.Destroy(current.gameObject);
       // }
        
        /*
        int count = Main.instance.spawnDots.Count;
        for (int i = 0; i < Main.instance.spawnDots.Count; i++)
        {
            if (localForkNumber == Main.instance.spawnDots[i].planeNumber && runLine == Main.instance.spawnDots[i].forkNumber)
            {
                SpawnDot temp = Main.instance.spawnDots[i];
                Main.instance.spawnDots.Remove(Main.instance.spawnDots[i]);
                GameObject.Destroy(temp.gameObject);
            }
        }
         
        for (int i = 0; i < Main.instance.miscs.Count; i++)
        {
            Debug.Log(i + " " + localForkNumber + " " + Main.instance.miscs[i].planeNumber + " " + runLine + " " + Main.instance.miscs[i].forkNumber + " " + Main.instance.miscs[i]._name);
            if (localForkNumber == Main.instance.miscs[i].planeNumber && runLine == Main.instance.miscs[i].forkNumber)
            {
                
                Misc temp = Main.instance.miscs[i];
                Main.instance.miscs.Remove(Main.instance.miscs[i]);
                GameObject.Destroy(temp.gameObject);
            }
        }
        */
        

        if (Main.instance.isGenerator)
            Main.instance.BlockPoolSystem.Objects[(int)type].DestroyObjectPool(this);
        else
            Destroy(currentGameObject);
    }

    

	public virtual OneSpline GetSpline(Vector3 _pos)
    {
        return spline;
    }

	public OneSpline GetSpline(SplineType type)
	{
		OneSpline result = null;
		OneSpline[] Temp = GetComponents<OneSpline>();
		for (int i = 0; i < Temp.Length; i++)
		{
			if (Temp[i].type == type)
				result = Temp[i];
		}
		return result;
	}

	public static OneSpline GetSpline(Transform transform, SplineType type)
    {
		OneSpline result = null;
		OneSpline[] Temp = transform.GetComponents<OneSpline>();
        for (int i = 0; i < Temp.Length; i++)
        {
            if (Temp[i].type == type)
                result = Temp[i];
        }
        return result;
    }

	public OneSpline GetSpline(int type)
    {
		OneSpline result = null;
		OneSpline[] Temp = GetComponents<OneSpline>();
        for (int i = 0; i < Temp.Length; i++)
            if ((int)Temp[i].type == type)
                result = Temp[i];
        return result;
    }

	public override void Initialize(bool activeObject)
    {
		base.Initialize(activeObject);

		Main.instance = FindObjectOfType<Main> ();
        transform = transform;
        currentGameObject = gameObject;

        spline = GetSpline(SplineType.Front);

		splines = GetComponents<OneSpline>();
        //for (int i = 0; i < splines.Length; i++)
        //    splines[i].CalculateLenght();
		
		if (type != BlockPlaneType.Connection)
		{
			//startPosition = transform.TransformPoint (spline.a);
			//endPosition = transform.TransformPoint (spline.b);
		}
		BoundsCorrector[] boundsCorrectors = transform.GetComponentsInChildren<BoundsCorrector>();
		for (int i = 0; i < boundsCorrectors.Length; i++)
			boundsCorrectors [i].Resize ();



        
    }

    public void GetManualPosition()
    {
        transform.position = ManualPosition;
        transform.rotation = Quaternion.identity;
    }

    public void SetManualPosition()
    {
        ManualPosition = transform.position;  
    }

    public void CreateWithManualPosition()
    {
        transform.position = ManualPosition;
        transform.rotation = Quaternion.identity;
        
    }
	/*
#if UNITY_EDITOR
	//[MenuItem("Roads/Create/Plane")]
	public static void CreatePlaneEditor(string data, GameObject current, Mesh _collider, landscapeType _landType, string _blockName)
		{
		BlockPlane BP = current.AddComponent<BlockPlane> ();

		BP.landType = _landType;
		BP.blockName = _blockName.ToLower();
		BP.currentGameObject = current;
		BP.transform = BP.currentGameObject.transform;
		BP.nominal = current.name.ToLower();
		current.tag = "Block";
		current.layer = LayerMask.NameToLayer ("Ground");

		GameObject colliderGO = new GameObject ();
		colliderGO.name = "collider";
		colliderGO.transform.position = Vector3.zero;
		colliderGO.transform.rotation = Quaternion.Euler(new Vector3(0, 90f, 0));
		colliderGO.transform.SetParent (BP.transform);
		colliderGO.tag = "Block";
		colliderGO.layer = LayerMask.NameToLayer ("Ground");

		BP.sourceCollider = _collider;
		BP.roadCollider = colliderGO.AddComponent<MeshCollider> (); 
		BP.roadCollider.sharedMaterial = (PhysicMaterial)AssetDatabase.LoadAssetAtPath<PhysicMaterial> ("Assets/PhysicMaterials/Asphalt.physicmaterial");
		Rigidbody RB = colliderGO.AddComponent<Rigidbody> ();
		RB.mass = 1.0f;
		RB.useGravity = false;
		RB.isKinematic = true;

		//string data = EditorUtility.OpenFilePanel ("Open Bezier Source Data", "Assets/Prefabs/BlockSources/", "txt");
		BP.ImportBezier (data);
		BP.spline.nextY = BP.spline.b.y - BP.spline.a.y;
		BP.size = BP.point = BP.spline.Length / 280f;
		BP.canCreateNextPlane = true;

		BP.ColliderCorrection ();
		//string sourceColliderPath = EditorUtility.OpenFilePanel ("Open Collider Source Mesh", "Assets/Art/roads/", "fbx");
		//MeshFilter meshCollider = (MeshFilter)Instantiate (AssetDatabase.LoadAssetAtPath<MeshFilter> (sourceColliderPath));
		//BP.SourceCollider = meshCollider.sharedMesh;

		}


	public void ImportBezier(string data)
	{

		string[] filedata;
		BezierPoint start;
		BezierPoint end;

		if (data != "") 
		{
			
			filedata = File.ReadAllLines (data);

			int count = filedata.Length;

			Debug.Log (count);
			if (count == 3) {
				OneSpline currentSpline = currentGameObject.AddComponent<OneSpline> ();

				string currentString = filedata [0];

				start = LoadLine (filedata [0]);
				end = LoadLine (filedata [1]);

				currentSpline.a = start.center;
				currentSpline.aa = start.front;
				currentSpline.bb = end.back;
				currentSpline.b = end.center;

				float startAngle = Math3d.GetAngle (Vector3.forward, start.front - start.center);
				float endAngle = Math3d.GetAngle (Vector3.forward, end.center - end.back);
	
					currentSpline.startRot = new Vector3 (0, startAngle, 0);
					currentSpline.endRot = new Vector3 (0, endAngle, 0);
				currentSpline.startWidth = 7.5f;
				currentSpline.endWidth = 7.5f;
				currentSpline.startVerge = 21f;
				currentSpline.endVerge = 21f;
				currentSpline.CalculateLenght();

				spline = currentSpline;

			} else
			{
				
				ComplexSpline currentSpline = currentGameObject.AddComponent<ComplexSpline> ();

				currentSpline.splines = new Spline[count-2];

				spline = currentSpline;

				start = LoadLine (filedata [0]);

				for (int i = 0; i < count-2; i++) 
				{
					GameObject splineGO = new GameObject ();
					splineGO.transform.parent = transform;
					splineGO.name = "spline" + i.ToString ();
					splineGO.transform.localPosition = Vector3.zero;
					splineGO.transform.localRotation = Quaternion.identity;
					currentSpline.splines[i] = splineGO.AddComponent<OneSpline> ();
					end = LoadLine (filedata [i+1]);
					currentSpline.splines[i].a = start.center;
					currentSpline.splines[i].aa = start.front;
					currentSpline.splines[i].bb = end.back;
					currentSpline.splines[i].b = end.center;
					float startAngle = Math3d.GetAngle (Vector3.forward, start.front - start.center);
					float endAngle = Math3d.GetAngle (Vector3.forward, end.center - end.back);
					if (i==0)
						currentSpline.splines [i].startRot = new Vector3 (0, startAngle, 0);
					if (i==count-3)
						currentSpline.splines [i].endRot = new Vector3 (0, endAngle, 0);
					currentSpline.splines [i].startWidth = 7.5f;
					currentSpline.splines [i].endWidth = 7.5f;
					currentSpline.splines [i].startVerge = 21f;
					currentSpline.splines [i].endVerge = 21f;
					currentSpline.splines [i].CalculateLenght ();

					if (i<count-1)
						start = end;

				}

				splines = currentSpline.splines;


				currentSpline.CalculateLenght();

			}

		}
	}
	*/
/*
	BezierPoint LoadLine(string _line)
	{
		string currentString = _line;
		currentString = currentString.Substring (currentString.IndexOf (' ')+1);
		//Debug.Log (currentString);
		string[] bezier = new string[9];
		for (int i = 0; i < 9; i++)
		{
			//Debug.Log (currentString.IndexOf (' ') - 1);
			bezier[i] = currentString.Substring (0, currentString.IndexOf (' '));
			currentString = currentString.Substring (currentString.IndexOf (' ')+1);
			//Debug.Log (bezier [i]);
		}

		BezierPoint result = new BezierPoint ();

		result.back = new Vector3(-float.Parse(bezier[1]),float.Parse(bezier[2]),float.Parse(bezier[0]));
		result.center = new Vector3(-float.Parse(bezier[4]),float.Parse(bezier[5]),float.Parse(bezier[3]));
		result.front = new Vector3(-float.Parse(bezier[7]),float.Parse(bezier[8]),float.Parse(bezier[6]));

		//Debug.Log (back + " " + center + " " + front);

		return  result;
	}
*/
	#if UNITY_EDITOR
	public void PrepareToCorrection()
	{
		Transform meshTransform = transform.Find("mesh");
		meshTransform.localRotation = Quaternion.identity;

		MeshFilter MF = meshTransform.GetComponent<MeshFilter> ();
		MeshCollider MC = meshTransform.gameObject.AddComponent<MeshCollider> ();
		MeshRenderer MR = meshTransform.GetComponent<MeshRenderer> ();
		MC.sharedMesh = MF.sharedMesh;

		int verticesCount = editMeshCollider.sharedMesh.vertexCount;
		Vector3[] vertices = editMeshCollider.sharedMesh.vertices;

		Texture2D control = (Texture2D)MR.sharedMaterial.GetTexture ("_Control1");
		Texture2D height = (Texture2D)MR.sharedMaterial.GetTexture ("_TERRAIN_HeightMap");

		for (int i = 0; i < verticesCount; i++) 
		{
			RaycastHit info;
			Vector3 pos = vertices [i];
			bool result = false;
			pos.y += 10.0f;
			result = MC.Raycast(new Ray(pos, Vector3.down), out info, Mathf.Infinity);

			if (result) 
			{
				vertices [i] = info.point;
			}
		}

		Mesh correctionMesh = new Mesh ();
		correctionMesh.vertices = vertices;
		correctionMesh.triangles = editMeshCollider.sharedMesh.triangles;
		correctionMesh.uv = editMeshCollider.sharedMesh.uv;
		correctionMesh.uv2 = editMeshCollider.sharedMesh.uv2;
		correctionMesh.name = "PreparedMesh";

		correctionMesh.RecalculateNormals ();
		correctionMesh.RecalculateBounds ();

		preparedCollider = correctionMesh;
		editMeshCollider.sharedMesh = preparedCollider;
		editedCollider = editMeshCollider.sharedMesh;

		DestroyImmediate (MC);

		transform.Find ("mesh").localRotation = Quaternion.Euler(new Vector3(0,90f,0));
		colliderStep = ColliderEditStep.Prepare;
	}

	public void ColliderCorrection()
	{

		Transform meshTransform = transform.Find("mesh");
		meshTransform.localRotation = Quaternion.identity;

		MeshFilter MF = meshTransform.GetComponent<MeshFilter> ();
		MeshCollider MC = meshTransform.gameObject.AddComponent<MeshCollider> ();
		MeshRenderer MR = meshTransform.GetComponent<MeshRenderer> ();
		MC.sharedMesh = MF.sharedMesh;

		int verticesCount = editMeshCollider.sharedMesh.vertexCount;
		Vector3[] vertices = editMeshCollider.sharedMesh.vertices;
		Vector3[] normals = editMeshCollider.sharedMesh.normals;
		Color[] colors = editMeshCollider.sharedMesh.colors;
		int count = 0;
		int typeCorrection = 0;

		if (MR.sharedMaterial.shader.name == "Terrain2Geometry8Layers")
			typeCorrection = 1;
		if (MR.sharedMaterial.shader.name == "Terrain2GeometryTransition")
			typeCorrection = 2;

		Texture2D control = (Texture2D)MR.sharedMaterial.GetTexture ("_Control1");
		Texture2D height = (Texture2D)MR.sharedMaterial.GetTexture ("_TERRAIN_HeightMap");
		Texture2D height2 = (Texture2D)MR.sharedMaterial.GetTexture ("_TERRAIN_HeightMap2");
		if (typeCorrection > 0)
			height2 = (Texture2D)MR.sharedMaterial.GetTexture ("_TERRAIN_HeightMap2");

		Vector4 s = MR.sharedMaterial.GetVector ("_TessStrenght0123");
		Vector4 s2 = MR.sharedMaterial.GetVector ("_TessStrenght4567");
		//if (typeCorrection > 0)
		//	s2 = MR.sharedMaterial.GetVector ("_TessStrenght4567");
		


		float transitionSliderParam = 0;
		if (typeCorrection == 2)
			transitionSliderParam = MR.sharedMaterial.GetFloat ("_TransitionSlider");
		for (int i = 0; i < verticesCount; i++) 
		{
			RaycastHit info;
			Vector3 pos = vertices [i];
			bool result = false;
			pos.y += 10.0f;
			result = MC.Raycast(new Ray(pos, Vector3.down), out info, Mathf.Infinity);

			if (result) 
			{
				count++;
				Color c = control.GetPixelBilinear (info.textureCoord.x, info.textureCoord.y-0.004f);
				Color h = height.GetPixelBilinear (info.textureCoord2.x, info.textureCoord2.y);
				Color h2 = Color.black;

				Vector4 splatControlA = new Vector4 (c.r, c.g, c.b, c.a);
				Vector4 heightV = new Vector4 (h.r, h.g, h.b, h.a);
				float lenght = heightV.magnitude/8;
				float lenght2 = 0;
				Vector4 heightV2 = Vector4.zero;
				Vector4 tha = heightV;
				Vector4 thb = Vector4.zero;

				Vector4 splat_control1 = splatControlA;
				float transitionParam = 0;
				Vector4 s3 = Vector4.zero;
				float energy = 0;

				if (typeCorrection == 0) 
				{
					splat_control1 = Vector4.Scale (splat_control1, tha);

					energy = s.x * splat_control1.x;
					energy += s.y * splat_control1.y;
					energy += s.z * splat_control1.z;
					energy += s.w * splat_control1.w;
				}
				if (typeCorrection==2)
				{
					transitionParam = 1-colors[i].g*transitionSliderParam;
					h2 = height2.GetPixelBilinear(info.textureCoord2.x, info.textureCoord2.y);

					heightV2 = new Vector4 (h2.r, h2.g, h2.b, h2.a);
					thb = heightV2;

					lenght2 = heightV2.magnitude/8;
					splat_control1 = Vector4.Scale(splat_control1,Vector4.Lerp(tha,thb,transitionParam));
					s3 = Vector4.Lerp (s, s2, transitionParam);

					energy  = s3.x * splat_control1.x;
					energy += s3.y * splat_control1.y;
					energy += s3.z * splat_control1.z;
					energy += s3.w * splat_control1.w;
				}




				Vector4 sCMid = Vector4.Scale (splat_control1, splat_control1);
				sCMid /= Mathf.Max(0.001f,Vector4.Dot (sCMid, new Vector4(1,1,1,1)));
				Vector4 sCC = Vector4.Scale (sCMid, sCMid);
				sCC/=Mathf.Max(0.001f, Vector4.Dot(sCC,new Vector4(1,1,1,1)));
				splat_control1=Vector4.Lerp(sCMid, sCC, 1);

				float actH = 0;
				float energy2 = 0;
				float energy3 = 0;

				if (typeCorrection == 0) 
				{
					actH = Vector4.Dot (splat_control1, tha);

					energy2 = s.x * splat_control1.x;
					energy2 += s.y * splat_control1.y;
					energy2 += s.z * splat_control1.z;
					energy2 += s.w * splat_control1.w;
					energy3 = Mathf.Lerp(energy,energy2, lenght);
				}
				if (typeCorrection == 2) 
				{
					actH = Vector4.Dot (splat_control1, Vector4.Lerp(tha,thb,transitionParam));
					energy2 = s3.x * splat_control1.x;
					energy2 += s3.y * splat_control1.y;
					energy2 += s3.z * splat_control1.z;
					energy2 += s3.w * splat_control1.w;
					energy3 = Mathf.Lerp(energy,energy2, Mathf.Lerp(lenght,lenght2,transitionParam));
				}

				float current = energy3;
				vertices [i] = info.point + info.normal * current * actH * MR.sharedMaterial.GetFloat("_TessYOffset");

			}



		}

		Mesh correctionMesh = new Mesh ();
		correctionMesh.vertices = vertices;
		correctionMesh.triangles = editMeshCollider.sharedMesh.triangles;
		correctionMesh.uv = editMeshCollider.sharedMesh.uv;
		correctionMesh.uv2 = editMeshCollider.sharedMesh.uv2;
		correctionMesh.name = "CorrectionMesh";

		correctionMesh.RecalculateNormals ();
		correctionMesh.RecalculateBounds ();
		//TangentSolver.Solve (correctionMesh);
		editMeshCollider.sharedMesh = correctionMesh;
		editedCollider = editMeshCollider.sharedMesh;

		DestroyImmediate (MC);

		transform.Find ("mesh").localRotation = Quaternion.Euler(new Vector3(0,90f,0));
		colliderStep = ColliderEditStep.Final;
	}

	public void CreateGrass()
	{
		GrassSystem gsSource = AssetDatabase.LoadAssetAtPath ("Assets/GrassSystem/GrassSystem.prefab", typeof(GrassSystem)) as GrassSystem;
		GrassSystem gs = PrefabUtility.InstantiatePrefab (gsSource) as GrassSystem;

		roadCollider.gameObject.SetActive (false);

		gs.MR = transform.Find ("mesh").GetComponent<MeshRenderer> ();
		gs.MF = transform.Find ("mesh").GetComponent<MeshFilter> ();

		gs.tempMeshCollider = gs.MR.transform.gameObject.AddComponent<MeshCollider> ();
		gs.tempMeshCollider.sharedMesh = gs.MF.sharedMesh;

		gs.layersNumber = LayersNumber._4Layers;
		gs.meshColors = gs.MF.sharedMesh.colors;
		gs.meshTriangles = gs.MF.sharedMesh.triangles;
		gs.layerBright0123 = gs.MR.sharedMaterial.GetVector ("_LayerBrightness0123");
		gs.layerSatur0123 = gs.MR.sharedMaterial.GetVector ("_LayerSaturation0123");
		Debug.Log (gs.MR.sharedMaterial.shader.name);

		if (gs.MR.sharedMaterial.shader.name == "Relief Pack/Terrain2Geometry8Layers")
			gs.layersNumber = LayersNumber._8Layers;
		if (gs.MR.sharedMaterial.shader.name == "Relief Pack/Terrain2GeometryTransition")
		{
			gs.layersNumber = LayersNumber._Transition;
			gs.layerBright4567 = gs.MR.sharedMaterial.GetVector ("_LayerBrightness4567");
			gs.layerSatur4567 = gs.MR.sharedMaterial.GetVector ("_LayerSaturation4567");

		}
		Selection.activeGameObject = gs.gameObject;


	}

	public bool SubdSizeCheck(Vector3i _current)
	{
		BlockPlane.Data d = data;

		float S = Mathf.Abs (((d.vertices [_current.x].x - d.vertices [_current.z].x) * (d.vertices [_current.y].z - d.vertices [_current.z].z) - 
			(d.vertices [_current.y].x - d.vertices [_current.z].x) * (d.vertices [_current.x].z - d.vertices [_current.z].z)) / 2);

		//if ((d.color [_current.x] == Color.blue || d.color [_current.y] == Color.blue || d.color [_current.z] == Color.blue))
		//	return false;

		switch (colliderStep) 
		{
		case ColliderEditStep.Prepare:
			if (S < 8.0f)
				return false;
			break;
		case ColliderEditStep.Tier1:
			if (S < 8.0f)
				return false;
			break;
		case ColliderEditStep.Tier2:
			if ((S < 1.5f || S > 3.7f) || (d.color[_current.x] == Color.red || d.color[_current.y] == Color.red || d.color[_current.z] == Color.red))

				return false;
			break;
		default:
			return true;
			break;

		}

		return true;
	}

	public bool SubdSizeCheckWithoutBlue(Vector3i _current)
	{
		BlockPlane.Data d = data;

		float S = Mathf.Abs (((d.vertices [_current.x].x - d.vertices [_current.z].x) * (d.vertices [_current.y].z - d.vertices [_current.z].z) - 
			(d.vertices [_current.y].x - d.vertices [_current.z].x) * (d.vertices [_current.x].z - d.vertices [_current.z].z)) / 2);

		if ((d.color [_current.x] != Color.yellow && d.color [_current.y] != Color.yellow && d.color [_current.z] != Color.yellow))
			if ((d.color [_current.x] == Color.blue || d.color [_current.y] == Color.blue || d.color [_current.z] == Color.blue))
				return false;

		switch (colliderStep) 
		{
		case ColliderEditStep.Tier1:
			if (S < 8.0f)
				return false;
			break;
		case ColliderEditStep.Tier2:
			if ((S < 1.5f || S > 3.7f) || (d.color[_current.x] == Color.red || d.color[_current.y] == Color.red || d.color[_current.z] == Color.red))

				return false;
			break;
		default:
			return true;
			break;

		}

		return true;
	}

	public bool SubdSizeCheck02(Vector3i _current)
	{
		BlockPlane.Data d = data;
		Vector3 a = d.vertices [_current.x];
		Vector3 b = d.vertices [_current.y];
		Vector3 c = d.vertices [_current.z];

		float S = Mathf.Abs (((a.x - c.x) * (b.z - c.z) - 
			(b.x - 	c.x) * (a.z - c.z)) / 2);
		
		float dist1 = Vector2.Distance (a.ToVector2XZ(), b.ToVector2XZ());
		float dist2 = Vector2.Distance (b.ToVector2XZ(), c.ToVector2XZ());
		float dist3 = Vector2.Distance (c.ToVector2XZ(), a.ToVector2XZ());

		if ((S < 1.5f || S > 3.5f) || dist1 > 3.7f || dist2 > 3.7f || dist3 > 3.7f)
			return false;

		return true;
	}

	public void SelectAll(bool isCheck)
	{
		data.triangles = new List<Vector3i> ();
		data.triangles = data.fullTriangles;

		int ch = 0;
		int ch2 = 0;
		if (data.triangles!=null)
			for (int i = 0; i < data.triangles.Count; i++) 
			{
				if (isCheck)
				if (!SubdSizeCheck (data.triangles[i])) 
				{
					Vector3i triangle = data.triangles [i];
					triangle.count = 4;
					data.triangles [i] = triangle;
					continue;
				}

				NearPolygon currentNearPolygon = new NearPolygon ();
				for (int k = 0; k < data.fullTriangles.Count; k++) 
				{
					int contain = data.fullTriangles [k].isContain (data.triangles [i]);

					if (contain == 2)
					{
						currentNearPolygon.triangleIndex.Add (k);
						ch2 += 3;
					}
				}
				nearPolygons.Add (currentNearPolygon);
				ch += 3;
			}
	}

	public void SelectSecond()
	{
		data.triangles = new List<Vector3i> ();

			for (int i = 0; i < data.fullTriangles.Count; i++) 
			{

				if (!SubdSizeCheck02 (data.fullTriangles[i])) 
				{
					continue;
				}

				data.triangles.Add (data.fullTriangles [i]);
			}

			for (int i = 0; i < data.triangles.Count; i++) 
			{
				NearPolygon currentNearPolygon = new NearPolygon ();

				for (int k = 0; k < data.fullTriangles.Count; k++) 
				{
					int contain = data.fullTriangles [k].isContain (data.triangles [i]);
					if (contain == 2)
					{
						currentNearPolygon.triangleIndex.Add (k);
					}
				}

				nearPolygons.Add (currentNearPolygon);
			}
	}


	void GizmosSubdivide(bool isSubd)
	{
		Mesh newMesh = new Mesh ();
		Mesh newMesh02 = new Mesh ();
		Mesh newMesh03 = new Mesh ();
		Mesh newMesh04 = new Mesh ();

		List<Vector3> newVertices = new List<Vector3> ();
		List<Vector3> newVertices02 = new List<Vector3> ();
		List<Vector3> newVertices03 = new List<Vector3> ();
		List<Vector3> newVertices04 = new List<Vector3> ();
		List<Color> newColor = new List<Color> ();
		List<Color> newColor02 = new List<Color> ();
		List<Color> newColor03 = new List<Color> ();
		List<Color> newColor04 = new List<Color> ();
		List<int> newIndices = new List<int> ();
		List<int> newIndices02 = new List<int> ();
		List<int> newIndices03 = new List<int> ();
		List<int> newIndices04 = new List<int> ();

		int ch = 0;
		int ch2 = 0;
		int ch3 = 0;
		int ch4 = 0;
		for (int i = 0; i < data.triangles.Count; i++) 
		{
			if (isSubd) 
			{
				if (!SubdSizeCheck (data.triangles [i])) {
					continue;
				}
			}

			if ((data.color [data.triangles [i].x] == Color.yellow) || (data.color [data.triangles [i].y] == Color.yellow) || (data.color [data.triangles [i].z] == Color.yellow)) 
			{
				newVertices04.Add(data.vertices [data.triangles[i].x]);
				newVertices04.Add(data.vertices [data.triangles[i].y]);
				newVertices04.Add(data.vertices [data.triangles[i].z]);

				newColor04.Add(data.color [data.triangles [i].x]);
				newColor04.Add(data.color [data.triangles[i].y]);
				newColor04.Add(data.color [data.triangles[i].z]);

				for (int z = 0; z < 3; z++)
					newVertices04 [ch4+z] = new Vector3 (newVertices04 [ch4+z].x, newVertices04 [ch4+z].y + 0.1f, newVertices04 [ch4+z].z);

				newIndices04.Add (ch4);
				newIndices04.Add (ch4+1);
				newIndices04.Add (ch4+2);
				ch4 += 3;
			}
			else
			if ((data.color [data.triangles [i].x] == Color.blue) || (data.color [data.triangles [i].y] == Color.blue) || (data.color [data.triangles [i].z] == Color.blue)) 
			{
				newVertices03.Add(data.vertices [data.triangles[i].x]);
				newVertices03.Add(data.vertices [data.triangles[i].y]);
				newVertices03.Add(data.vertices [data.triangles[i].z]);

				newColor03.Add(data.color [data.triangles [i].x]);
				newColor03.Add(data.color [data.triangles[i].y]);
				newColor03.Add(data.color [data.triangles[i].z]);

				for (int z = 0; z < 3; z++)
					newVertices03 [ch3+z] = new Vector3 (newVertices03 [ch3+z].x, newVertices03 [ch3+z].y + 0.1f, newVertices03 [ch3+z].z);

				newIndices03.Add (ch3);
				newIndices03.Add (ch3+1);
				newIndices03.Add (ch3+2);
				ch3 += 3;
			}
			else
			if ((data.color [data.triangles [i].x] == Color.red) || (data.color [data.triangles [i].y] == Color.red) || (data.color [data.triangles [i].z] == Color.red)) 
			{
				newVertices02.Add(data.vertices [data.triangles[i].x]);
				newVertices02.Add(data.vertices [data.triangles[i].y]);
				newVertices02.Add(data.vertices [data.triangles[i].z]);

				newColor02.Add(data.color [data.triangles [i].x]);
				newColor02.Add(data.color [data.triangles[i].y]);
				newColor02.Add(data.color [data.triangles[i].z]);

				for (int z = 0; z < 3; z++)
					newVertices02 [ch2+z] = new Vector3 (newVertices02 [ch2+z].x, newVertices02 [ch2+z].y + 0.1f, newVertices02 [ch2+z].z);
				
				newIndices02.Add (ch2);
				newIndices02.Add (ch2+1);
				newIndices02.Add (ch2+2);
				ch2 += 3;
			}
			else							
			{
				newVertices.Add(data.vertices [data.triangles[i].x]);
				newVertices.Add(data.vertices [data.triangles[i].y]);
				newVertices.Add(data.vertices [data.triangles[i].z]);

				newColor.Add(data.color [data.triangles [i].x]);
				newColor.Add(data.color [data.triangles[i].y]);
				newColor.Add(data.color [data.triangles[i].z]);

				for (int z = 0; z < 3; z++)
					newVertices [ch+z] = new Vector3 (newVertices [ch+z].x, newVertices [ch+z].y + 0.1f, newVertices [ch+z].z);

				newIndices.Add (ch);
				newIndices.Add (ch+1);
				newIndices.Add (ch+2);
				ch += 3;
			}
		}

		if (newIndices.Count>0)
		{
			newMesh.vertices = newVertices.ToArray();
			newMesh.colors = newColor.ToArray();
			newMesh.SetIndices (newIndices.ToArray(), MeshTopology.Triangles, 0);
			newMesh.RecalculateNormals ();
			newMesh.RecalculateBounds ();
			Gizmos.color = Color.white;
			Gizmos.DrawMesh (newMesh);
		}

		if (newIndices02.Count>0)
		{
			newMesh02.vertices = newVertices02.ToArray();
			newMesh02.colors = newColor02.ToArray();
			newMesh02.SetIndices (newIndices02.ToArray(), MeshTopology.Triangles, 0);
			newMesh02.RecalculateNormals ();
			newMesh02.RecalculateBounds ();
			Gizmos.color = Color.red;
			Gizmos.DrawMesh (newMesh02);
		}

		if (newIndices03.Count>0)
		{
			newMesh03.vertices = newVertices03.ToArray();
			newMesh03.colors = newColor03.ToArray();
			newMesh03.SetIndices (newIndices03.ToArray(), MeshTopology.Triangles, 0);
			newMesh03.RecalculateNormals ();
			newMesh03.RecalculateBounds ();
			Gizmos.color = Color.blue;
			Gizmos.DrawMesh (newMesh03);
		}

		if (newIndices04.Count>0)
		{
			newMesh04.vertices = newVertices04.ToArray();
			newMesh04.colors = newColor04.ToArray();
			newMesh04.SetIndices (newIndices04.ToArray(), MeshTopology.Triangles, 0);
			newMesh04.RecalculateNormals ();
			newMesh04.RecalculateBounds ();
			Gizmos.color = Color.yellow;
			Gizmos.DrawMesh (newMesh04);
		}
	}

	void GizmosDelete()
	{
		Mesh TriangleMesh = new Mesh ();
		Vector3[] triangleVertices = new Vector3[3 * data.triangles.Count];
		int[] indices = new int[3 * data.triangles.Count];

		int ch = 0;
		for (int i = 0; i < data.triangles.Count; i++) 
		{
			triangleVertices [ch] = data.vertices [data.triangles[i].x];
			triangleVertices [ch+1] = data.vertices [data.triangles[i].y];
			triangleVertices [ch+2] = data.vertices [data.triangles[i].z];

			Gizmos.color = Color.white;
			for (int z = 0; z < 3; z++)
			{
				//Gizmos.DrawSphere (triangleVertices [ch+z], 0.25f);
				triangleVertices [ch+z] = new Vector3 (triangleVertices [ch+z].x, triangleVertices [ch+z].y + 0.1f, triangleVertices [ch+z].z);
			}

			indices[ch] = ch;
			indices[ch+1] = ch+1;
			indices[ch+2] = ch+2;

			ch+=3;
		}
		TriangleMesh.vertices = triangleVertices;
		TriangleMesh.SetIndices (indices, MeshTopology.Triangles, 0);
		TriangleMesh.RecalculateNormals ();
		TriangleMesh.RecalculateBounds ();

		Gizmos.color = Color.red;
		Gizmos.DrawMesh (TriangleMesh);
	}

	void GizmosSideSubdivide()
	{
		Mesh selectMesh = new Mesh ();
		Mesh borderMesh = new Mesh ();
		List<Vector3>  selectVertices = new List<Vector3> ();
		List<Vector3> borderVertices = new List<Vector3> ();
		List<int> selectIndices = new List<int>();
		List<int> borderIndices = new List<int>();



		int ch = 0;
		int ch2 = 0;
		if (data.triangles!=null)
			for (int i = 0; i < data.triangles.Count; i++) 
			{

				NearPolygon currentNearPolygon = new NearPolygon ();
				selectVertices.Add(data.vertices [data.triangles[i].x]);
				selectVertices.Add(data.vertices [data.triangles[i].y]);
				selectVertices.Add(data.vertices [data.triangles[i].z]);

				Gizmos.color = Color.white;
				for (int z = 0; z < 3; z++) {
					selectVertices [ch + z] = new Vector3 (selectVertices [ch + z].x, selectVertices [ch + z].y + 0.1f, selectVertices [ch + z].z);
				}

				selectIndices.Add(ch);
				selectIndices.Add(ch + 1);
				selectIndices.Add(ch + 2);


				for (int k = 0; k < data.fullTriangles.Count; k++) 
				{
					int contain = data.fullTriangles [k].isContain (data.triangles [i]);

					if (contain == 2)
					{
						currentNearPolygon.triangleIndex.Add (k);

						borderVertices.Add (data.vertices [data.fullTriangles[k].x]);
						borderVertices.Add (data.vertices [data.fullTriangles[k].y]);
						borderVertices.Add (data.vertices [data.fullTriangles[k].z]);

						borderIndices.Add(ch2);
						borderIndices.Add(ch2+1);
						borderIndices.Add(ch2+2);

						for (int z = 0; z < 3; z++) {
							borderVertices [ch2 + z] = new Vector3 (borderVertices [ch2 + z].x, borderVertices [ch2 + z].y + 0.1f, borderVertices [ch2 + z].z);
						}

						ch2 += 3;
					}
				}
				nearPolygons.Add (currentNearPolygon);
				ch += 3;
			}



		//Debug.Log (data.nearPolygons.Count);

		borderMesh.vertices = borderVertices.ToArray ();
		if (borderIndices.Count > 0) 
		{
			borderMesh.SetIndices (borderIndices.ToArray (), MeshTopology.Triangles, 0);
			borderMesh.RecalculateNormals ();
			borderMesh.RecalculateBounds ();
			Gizmos.color = Color.blue;
			Gizmos.DrawMesh (borderMesh);
		}

		selectMesh.vertices = selectVertices.ToArray();
		if (selectIndices.Count > 0) 
		{
			selectMesh.SetIndices (selectIndices.ToArray (), MeshTopology.Triangles, 0);
			selectMesh.RecalculateNormals ();
			selectMesh.RecalculateBounds ();
			Gizmos.color = Color.green;
			Gizmos.DrawMesh (selectMesh);
		}
	}

	void OnDrawGizmos()
	{
		
		if (isColliderEdited) 
		{
			Transform meshTransform = transform.Find ("mesh").transform;
			Gizmos.matrix = meshTransform.localToWorldMatrix;

			nearPolygons = new List<NearPolygon> ();

			if (isCanRayCast && data!=null) 
			{

				if (tabSwitch == 0)
					GizmosSubdivide (false);
				
				if (tabSwitch == 1 && (colliderStep == ColliderEditStep.Tier1 || colliderStep == ColliderEditStep.Tier2))
					GizmosSubdivide (true);

				if ((tabSwitch == 2  && (colliderStep == ColliderEditStep.Tier1 || colliderStep == ColliderEditStep.Tier2)) || (tabSwitch == 1 && (colliderStep == ColliderEditStep.Correction || colliderStep == ColliderEditStep.Prepare)))
					GizmosDelete ();
			}

			if (isColliderView) 
			{
				Gizmos.color = Color.gray;
				Gizmos.DrawMesh (editedCollider);	
			} 

			Gizmos.color = Color.yellow;
			Gizmos.DrawWireMesh (editedCollider);
		}

	}

#endif
}


