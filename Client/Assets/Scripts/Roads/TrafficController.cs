﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Dest.Math;

[System.Serializable]
public enum DetectType
{
    Front,
    Right,
    Left,
    Back
}

[System.Serializable]
public struct TrafficPose
{
    public Vector3 point;
    public Vector2 axis;
    public float angle;
    public float offset;
	public OneSpline spline;
    public float percent;
    public int localNumber;
    public bool isCheck;
}

[System.Serializable]
public class TrafficObject
{
    public string name;
    public float probability;
 

    public TrafficObject()
    {
        name = "";
    probability = 0.0f;
    }
}

[System.Serializable]
public class TrafficLine
{
    public bool direction;
    public bool engaged;
    public float timer;
    public float timerMax;
    public Vector2 probability;
    public int UnitLimit;

    public TrafficLine(float _timerMax)
    {
        direction = false;
        engaged = false;
        timerMax = _timerMax;
        timer = timerMax;
        probability = Vector2.zero;
        UnitLimit = 0;

    }

    public void TrafficLineUpdate()
    {
        if (engaged)
        {
            if (timer > 0)
            {
                timer -= Time.deltaTime;
            }
            else
            {
                timer = timerMax;
                engaged = false;
            }
            
        }
    }
}

[System.Serializable]
public class CurvePart
{
    public Transform part;
    public Transform aa;
    public Transform bb;

    public Vector3 position;
    public Vector3 aaPosition;
    public Vector3 bbPosition;
    public Vector3 prepareIntersect;
    public float scale;
    public bool editorEnable;
    
    

    public CurvePart()
    {
        position = Vector3.zero;
        aaPosition = Vector3.zero;
        bbPosition = Vector3.zero;
        prepareIntersect = Vector3.zero;
        scale = 1.0f;
        editorEnable = false;
    }

  

}

public class TrafficController : MonoBehaviour {


    public bool isReady = true;
    public bool isReadyBack = true;
    public GUIStyle TrafficStyle;
    public GUIStyle TrafficStyle02;
    public int CurveLenght;
    public int CarsLenght;
    public Vector3 startPosition;
    public float startScale;
    public List<CurvePart> curve;

    public Vector3 CamCircle;
    public Vector3 CamCircle2;
    private float full = 0.0f;
    private float average;
    public int frontLinesCount;
    public int backLinesCount;
    public int UnitLimitMax = 2;

    static float duration = 3.0f;

    public TrafficLine[] trafficLines;
    public List<TrafficObject> trafficObjects;


    public float timerMax;
    private float timer;

    public float backTimerMax;
    private float backTimer;

    TrafficPose one;
    TrafficPose two;

    public Vector3 FrontIntersectPoint = Vector3.zero;
    public Vector3 BackIntersectPoint = Vector3.zero;

    
    protected static void DrawAAB(ref AAB2 box, Color color, float duration)
    {
        Vector2 v0, v1, v2, v3;
        box.CalcVertices(out v0, out v1, out v2, out v3);
        //Debug.DrawLine(v0.ToVector3XZ(), v1.ToVector3XZ(), color, duration);
        //Debug.DrawLine(v1.ToVector3XZ(), v2.ToVector3XZ(), color, duration);
        //Debug.DrawLine(v2.ToVector3XZ(), v3.ToVector3XZ(), color, duration);
        //Debug.DrawLine(v3.ToVector3XZ(), v0.ToVector3XZ(), color, duration);
    }

 

    public static void DrawAAB(ref AAB3 box, Color color, float duration)
    {
        Vector3 v0, v1, v2, v3, v4, v5, v6, v7;
        box.CalcVertices(out v0, out v1, out v2, out v3, out v4, out v5, out v6, out v7);
        //Debug.DrawLine(v0, v1, color, duration);
        //Debug.DrawLine(v1, v2, color, duration);
        //Debug.DrawLine(v2, v3, color, duration);
        //Debug.DrawLine(v3, v0, color, duration);
        //Debug.DrawLine(v4, v5, color, duration);
        //Debug.DrawLine(v5, v6, color, duration);
        //Debug.DrawLine(v6, v7, color, duration);
        //Debug.DrawLine(v7, v4, color, duration);
        //Debug.DrawLine(v0, v4, color, duration);
        //Debug.DrawLine(v1, v5, color, duration);
        //Debug.DrawLine(v2, v6, color, duration);
        //Debug.DrawLine(v3, v7, color, duration);
    }

    public static void DrawBox2(ref Box2 box, Color color, float duration)
    {
        Vector2 v0, v1, v2, v3;
        box.CalcVertices(out v0, out v1, out v2, out v3);
        //Debug.DrawLine(v0.ToVector3XZ(), v1.ToVector3XZ(), color, duration);
        //Debug.DrawLine(v1.ToVector3XZ(), v2.ToVector3XZ(), color, duration);
        //Debug.DrawLine(v2.ToVector3XZ(), v3.ToVector3XZ(), color, duration);
        //Debug.DrawLine(v3.ToVector3XZ(), v0.ToVector3XZ(), color, duration);
    }

    public static Box2 CreateBox2(Transform box, Collider collider)
    {
        return new Box2(box.position.ToVector2XZ(), box.right.ToVector2XZ(), box.forward.ToVector2XZ(), new Vector2(collider.bounds.size.x/2, collider.bounds.size.z/2));
    }

    public static void DrawCircle(ref Circle2 circle, Color color, float duration)
    {
        Vector3 pos = circle.Center.ToVector3XZ();
        float r = circle.Radius;

        for (int i = 0; i <= 20; i++)
        {
            float theta = 2.0f * 3.1415925f * (float)i / 20.0f;
            float theta2 = 2.0f * 3.1415925f * (float)(i + 1) / 20.0f;

            float x = r * Mathf.Cos(theta);
            float y = r * Mathf.Sin(theta);
            float x2 = r * Mathf.Cos(theta2);
            float y2 = r * Mathf.Sin(theta2);

           // Debug.DrawLine(new Vector3(x + pos.x, pos.y, y + pos.z), new Vector3(x2 + pos.x, pos.y, y2 + pos.z), color, duration);//output vertex
        }

    }

	/*
    public static bool IntersectBounds(Unit misc, Box2 box, bool visual)
    {

            Circle2 current = new Circle2(misc.transform.position.ToVector2XZ(), 1.0f);

            if (Intersection.TestBox2Circle2(ref box, ref current))
            {
                //DrawBox2(ref box, Color.red, duration);
               // DrawCircle(ref current, Color.red, duration);
                return true;
            }
            else
            {
               // DrawBox2(ref box, Color.green, duration);
               // DrawCircle(ref current, Color.green, duration);
                return false;
            }
            return false;
        
    }
*/
    public static bool IntersectBounds(Unit unit, Box2 box, bool visual)
    {

            Box2 current = CreateBox2(unit.transform, unit.currentCollider);

            if (Intersection.TestBox2Box2(ref box, ref current))
            {
                DrawBox2(ref box, Color.red, duration);
                DrawBox2(ref current, Color.red, duration);
                return true;
            }
            else
            {
                DrawBox2(ref box, Color.green, duration);
                DrawBox2(ref current, Color.green, duration);
                return false;
            }
            return false;    
    }

    public static bool IntersectBounds(Unit unit, Circle2 circle, bool visual)
    {

            Box2 current = CreateBox2(unit.transform, unit.currentCollider);

            if (Intersection.TestBox2Circle2(ref current, ref circle))
            {
               DrawBox2(ref current, Color.red, duration);
               DrawCircle(ref circle, Color.red, duration);
                return true;
            }
            else
            {
               DrawBox2(ref current, Color.green, duration);
               DrawCircle(ref circle, Color.green, duration);
                return false;
            }
            return false;
    }

    public static bool IntersectBounds(BlockPlane plane, Circle2 circle, bool visual)
    {

        
        Box2 current = CreateBox2(plane.transform, plane.wallCollider);
        Debug.Log(current.Center + " " + current.Extents + " " + current.Axis0 + " " + current.Axis1);

        if (Intersection.TestBox2Circle2(ref current, ref circle))
            {
              //  DrawBox2(ref current, Color.red, duration);
              //  DrawCircle(ref circle, Color.red, duration);
                return true;
            }
            else
            {
               // DrawBox2(ref current, Color.green, duration);
               // DrawCircle(ref circle, Color.green, duration);
                return false;
            }

    }

	/*
    public static bool IntersectBounds(Unit misc, Circle2 circle, bool visual)
    {

        Circle2 current = new Circle2(misc.transform.position.ToVector2XZ(), 1.0f);

        if (Intersection.TestCircle2Circle2(ref current, ref circle))
            {
              //  DrawCircle(ref circle, Color.red, duration);
               // DrawCircle(ref current, Color.red, duration);
                return true;
            }
            else
            {
               // DrawCircle(ref circle, Color.green, duration);
               // DrawCircle(ref current, Color.green, duration);
                return false;
            }

            return false;
    }
*/
}

