﻿using UnityEngine;
using System.Collections;



public class ForkReverse : BlockPlane
{
    
    public SplineType line;
	public OneSpline LeftChoiceSpline;
	public OneSpline RightChoiceSpline;
    private Quad2dMarker[] markers;

    public bool inCamCenter = false;

    public override void Create(int _localForkNumber, int _runLine, Vector4 _position)
    {
       // if (Main.instance.isConnect)
       // {
		//	Main.instance.forkRun = Main.instance.generator.currentMap.activeSeasons[Main.instance.forkRun].seasonConnect;
		//	Main.instance.planeRun = Main.instance.generator.currentMap.activeSeasons[Main.instance.forkRun].roadConnect;
       // }

		Debug.Log (_position);
        base.Create(_localForkNumber, _runLine, _position);

        LeftChoiceSpline = GetSpline(SplineType.Left);
        RightChoiceSpline = GetSpline(SplineType.Right);

		//startPosition = transform.TransformPoint(spline.a);
		//endPosition = transform.TransformPoint(spline.b);

		//Main.instance.BlockPlanes.Add (this);
		Main.instance.AddAndDeletePlanes(this);
		//Debug.Log ("Create Fork Reverse");
    }

	public override void Initialize(bool activeObject)
    {
		base.Initialize (activeObject);

        markers = GetComponents<Quad2dMarker>();

        spline = GetSpline(line);

        type = BlockPlaneType.Connection;

        

    }

	public override float CheckRoadWidth(float _percent)
	{
		float max = 0;
		for (int i = 0; i < splines.Length; i++) 
		{
			float thisWidth = splines [i].GetCurrentWidth (_percent);
			if (max < thisWidth)
				max = thisWidth;
		}

		return max;
	}

	public override OneSpline GetSpline(Vector3 _pos)
    {
        Vector3 pos = transform.InverseTransformPoint(_pos);
        for (int i = 0; i < markers.Length; i++)
        {
			if (markers[i].Check(pos, false))
            {
                if (markers[i].type == SplineType.Left)
                    return LeftChoiceSpline;
                else
                    return RightChoiceSpline;
            }
        }
        return null;
    }
    public void SwapSplines()
    {
        if (line == SplineType.Left)
            line = SplineType.Right;          
        else
            line = SplineType.Left;
        spline = GetSpline(line);
    }

}


