﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Dest.Math;

[System.Serializable]
public class SupportPoint
{
    public Vector3 supportPosition;
    public float supportAngle;
	public OneSpline spline;
	public OneSpline splineForward;
	public SplineType type;

	public OutpostDoor outpostDoor;

    public SupportPoint(Vector3 _blockSupport, float _blockRotate)
    {
        supportPosition = _blockSupport;
        supportAngle = _blockRotate;
    }
}



public class OutpostPlane : BlockPlane
{
    public SupportPoint[] supportPoints;
	public SupportPoint currentSupportPoint;
    public int usedSupportPoints;
    public Vector3 returnPos;
    public Vector3 returnRot;
    public bool isCheckPoint;
    public Vector3 heroStartPos;
    public Quaternion heroStartRot;
    public Vector3 camStartPos;
    public Quaternion camStartRot;
	public bool isStarting;

    public void CreatePoint()
    {

        Locators.CreateLocatorLocal(Vector3.zero, Quaternion.identity, transform, "blockLocator_0");


    }

	public void CalculateSplinesToSupportPoints()
	{
		OneSpline[] _spline = transform.GetComponentsInChildren<OneSpline> ();
		for (int i = 0; i < _spline.Length; i++) 
		{
			for (int j = 0; j < supportPoints.Length; j++) 
			{
				if (_spline [i].type == supportPoints [j].type)
					supportPoints [j].spline = _spline[i];
			}
		}

		OutpostDoor[] _door = transform.GetComponentsInChildren<OutpostDoor> ();
		for (int i = 0; i < _door.Length; i++) 
		{
			for (int j = 0; j < supportPoints.Length; j++) 
			{
				if (_door [i].type == supportPoints [j].type)
					supportPoints [j].outpostDoor = _door[i];
			}
		}
	}

	public void SelectSupportPoint(int number, bool isStart)
	{
		currentSupportPoint = supportPoints [number];
		Main.instance.supportPoint = currentSupportPoint;
		if (isStart) 
		{
			
			spline = currentSupportPoint.splineForward;
			Main.instance.hero.vehicle.SetSpline (spline);
			currentSupportPoint.outpostDoor.OpenInside ();
		}
		else 
		{
			currentSupportPoint.outpostDoor.CloseImmediately ();
			spline = currentSupportPoint.spline;
			Main.instance.hero.vehicle.SetSpline (spline);
			currentSupportPoint.outpostDoor.OpenOutside ();

		}

	}

	public override Quaternion GetSplineRot(float _currentCamPercent)
	{
		if (isStarting)
			return transform.rotation * Quaternion.Euler(spline.GetYAngle(_currentCamPercent));
		else 
		{
			
			return transform.rotation * Quaternion.Euler(spline.GetYAngle(_currentCamPercent));
		}
	}

    public override void Create(int _localForkNumber, int _runLine, Vector4 _position)
    {      
		SelectSupportPoint (Random.Range (0, supportPoints.Length), false);//
		isStarting = false;

		Main.instance.currentPlane.nextBlock = this;
		localForkNumber = Main.instance.planeRun;
		runLine = Main.instance.forkRun;

		Vector3 pos = currentSupportPoint.supportPosition;
		float angle = 180.0f - currentSupportPoint.supportAngle;
		float newRotation = _position.w + angle;
		transform.rotation = Quaternion.Euler(new Vector3(0.0f, newRotation, 0.0f));

		Vector3 newPosition = (Vector3)_position - transform.TransformPoint(pos);
		newPosition.y = Main.instance.blocksYposition;
		transform.position = newPosition;

		Main.instance.currentOutpost = this;
		Main.instance.AddAndDeletePlanes(this);
		Main.instance.hangar.Summon ();
    }

    public override void DestroyThis()
    {
		if (Main.instance.gameMode == Mode.Battle)
		Main.instance.hangar.Hide ();
        for (int i = 0; i < usedSupportPoints; i++)
        {
//            if (supportPoints[i].firstPlaneTransform!= null)
    //            supportPoints[i].firstPlaneTransform.GetComponent<BlockPlane>().DestroyThis();
        }
       base.DestroyThis();
    }

    public override void CheckNewCreate()
    {
        //NOT DELETE!
    }

    public void ShowSupportPoints()
    {
        for (int i = 0; i < usedSupportPoints; i++)
        {
            Locators.CreateLocatorLocal(supportPoints[i].supportPosition, Quaternion.Euler(new Vector3(0.0f, supportPoints[i].supportAngle, 0.0f)), transform, "blockLocator_" + i);

        }
    }

	public override float CheckRoadWidth(float _percent)
	{
		return 7.5f;
	}

    public void SaveSupportPoints()
    {
        bool end = false;
        int ch = 0;
        supportPoints = new SupportPoint[4];
        while (!end)
        {
            Transform support = transform.Find("blockLocator_" + ch);
            if (support == null)
                end = true;

            supportPoints[ch] = new SupportPoint(support.localPosition, support.localEulerAngles.y);
            ch++;

        }
    }



}


