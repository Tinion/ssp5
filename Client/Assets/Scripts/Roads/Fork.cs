﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class WorldCheckBox
{
    public Vector3[] box;

    public WorldCheckBox(Transform _transform, Quad2d _data)
    {
        box = new Vector3[4];
        box[0] = _transform.TransformPoint(_data.a);
        box[1] = _transform.TransformPoint(_data.b);
        box[2] = _transform.TransformPoint(_data.c);
        box[3] = _transform.TransformPoint(_data.d);
    }
}


public class Fork : BlockPlane
{
    const float RAYCAST_ALTITUDE = 10.0f;

    private Vector3 _offsetPositionStart;
    private Vector3 _offsetPositionLeft;
    private Vector3 _offsetPositionRight;

    public bool isChoiceHeroSide = true;

	public OneSpline LeftChoiceSpline;
	public OneSpline RightChoiceSpline;

    public WorldCheckBox LeftChoiceCheckBox;
    public WorldCheckBox RightChoiceCheckBox;

    public bool inCamCenter = false;

    public int forkNumber;

    public SplineType side;
    //public Transform forkCenter;

    public OneSpline procSpline;
    public Quaternion camRotationFromChoice;
    public Vector2 leftRadiusSize;
    public Vector2 rightRadiusSize;
    private Vector2 currentRadiusSize;
    private Quad2dMarker[] markers;

	void Awake()
	{
		
	}

    public override BlockPlane GetNextBlock(bool direction, SplineType splineType)
    {
        if (direction)
        {
            if (isChoiceHeroSide)
            {
                switch(splineType)
                {
                    case SplineType.Left:
                        return LeftForkPlane;
                    case SplineType.Right:
                        return RightForkPlane;
                    default:
                        {
                            if (Math3d.GetChanceOf(50))
                                return LeftForkPlane;
                            else
                                return RightForkPlane;
                        }                 
               }
            }
            else
                return nextBlock;
        }
        else
            return previousBlock;

    }
    private Quad2d GetQuad2d(SplineType type)
    {
        Quad2d result = null;
        Quad2d[] Temp = GetComponents<Quad2d>();
        for (int i = 0; i < Temp.Length; i++)
            if (Temp[i].type == type)
                result = Temp[i];
        return result;
    }

	public override float CheckRoadWidth(float _percent)
	{
		float max = 0;
		for (int i = 0; i < splines.Length; i++) 
		{
			float thisWidth = splines [i].GetCurrentWidth (_percent);
			if (max < thisWidth)
				max = thisWidth;
		}

		return max;
	}

    public override void Create(int _localForkNumber, int _runLine, Vector4 _position)
    {
        base.Create(_localForkNumber, _runLine, _position);

        LeftChoiceSpline = GetSpline(SplineType.Left);
        RightChoiceSpline = GetSpline(SplineType.Right);

        LeftChoiceCheckBox = new WorldCheckBox(transform, GetQuad2d(SplineType.Left));
        RightChoiceCheckBox = new WorldCheckBox(transform, GetQuad2d(SplineType.Right));

        Additive();

		//Debug.Log ("Create Fork");
    }

    public override void Additive()
    {
		spline = splines [0];

        isChoiceHeroSide = true;
        canCreateNextPlane = false;
        inCamCenter = false; 

		//Debug.Log (runLine + " " + localForkNumber);
		RoadElement currentForkData = Main.instance.generator.currentMap.activeSeasons[runLine].roadElements[localForkNumber];

        RightForkPlane = Main.instance.GetLevelPlane(currentForkData.rightWay, currentForkData.ForkRun(false, localForkNumber));
        RightForkPlane.currentGameObject.SetActive(true);
        RightForkPlane.CreateSubPlane(SplineType.Right, this);

        LeftForkPlane = Main.instance.GetLevelPlane(currentForkData.leftWay, currentForkData.ForkRun(true, localForkNumber));
        LeftForkPlane.currentGameObject.SetActive(true);
        LeftForkPlane.CreateSubPlane(SplineType.Left, this);

		//startPosition = transform.TransformPoint(spline.a);
		//endPosition = transform.TransformPoint(spline.b);
    }

    private void Update()
    {
        
        if (Main.instance.hero != null && isChoiceHeroSide)
        {
            SplineType choiceSide = GetChoise(Main.instance.hero.transform.position);

			if (choiceSide != SplineType.Front)
            {
                side = choiceSide;
                ChoiceOn();
            }
        }
        
    }

    private void CamOnCenter()
    {
        camRotationFromChoice = Main.instance.mainCamera.transform.rotation;
        inCamCenter = true;
    }

    private void ChoiceOn()
    {
        //Debug.Log("Choice: " + side.ToString());

        _offsetPositionStart = Main.instance.mainCamera.transform.position;
        _offsetPositionStart.y = 0;

        camRotationFromChoice = Main.instance.mainCamera.transform.rotation;
        Vector3 charachterDirection = Main.instance.hero.transform.forward;
        float _startAngle = Math3d.GetAngle(charachterDirection.z, charachterDirection.x);
 
        if (side == SplineType.Left)
        {
            currentRadiusSize = leftRadiusSize;
            spline = LeftChoiceSpline;
            nextBlock = LeftForkPlane;
        }          
        else
        {
            currentRadiusSize = rightRadiusSize;
            spline = RightChoiceSpline;
            nextBlock = RightForkPlane;
        }
        
		/*

        procSpline = transform.gameObject.AddComponent<OneSpline>();
        procSpline.a = transform.InverseTransformPoint(Main.instance.mainCamera.transform.position);
        procSpline.a.y = 0;

        RaycastHit hit;
        int layerMask = 1 << 0;
        layerMask = ~layerMask;

        if (Physics.Raycast(procSpline.a + transform.position, Vector3.down, out hit, Mathf.Infinity, layerMask))
            procSpline.a.y = hit.point.y - transform.position.y;



        procSpline.b = spline.b;
        Vector3 radius = transform.InverseTransformDirection(spline.GetAngle(spline.GetStep(transform.position))) * currentRadiusSize.x;
        procSpline.aa = radius + procSpline.a;
        procSpline.aa.y = 0;

        if (Physics.Raycast(procSpline.aa + transform.position, Vector3.down, out hit, Mathf.Infinity, layerMask))
            procSpline.aa.y = hit.point.y - transform.position.y;


        //procSpline.bb = spline.bb;
        radius = new Vector3(0.0f, 0, -currentRadiusSize.y);
        radius = Math3d.RotateThis(radius, -spline.endRot.y, Vector3.zero);
        procSpline.bb = radius + procSpline.b;
        procSpline.bb.y = 0;

        if (Physics.Raycast(procSpline.bb + transform.position, Vector3.down, out hit, Mathf.Infinity, layerMask))
            procSpline.bb.y = hit.point.y - transform.position.y;
		*/

        isChoiceHeroSide = false;
        Main.instance.ChoiseBlock(side);
        
    }

	public override OneSpline GetSpline(Vector3 _pos)
    {
        Vector3 pos = transform.InverseTransformPoint(_pos);
        for (int i = 0; i < markers.Length; i++)
        {
            if (markers[i].Check(pos, false))
            {
                if (markers[i].type == SplineType.Left)
                    return LeftChoiceSpline;
                else
                    return RightChoiceSpline;
            }
        }
        return null;
    }

    public override void CreateSubPlane(SplineType splineType, Fork forkCurrent)
    {

        base.CreateSubPlane(splineType, forkCurrent);

		spline = splines[0];

        isChoiceHeroSide = true;
        canCreateNextPlane = false;
        inCamCenter = false;


        LeftChoiceSpline = GetSpline(SplineType.Left);
        RightChoiceSpline = GetSpline(SplineType.Right);

        LeftChoiceCheckBox = new WorldCheckBox(transform, GetQuad2d(SplineType.Left));
        RightChoiceCheckBox = new WorldCheckBox(transform, GetQuad2d(SplineType.Right));
    }


    SplineType GetChoise(Vector3 Pos)
    {
        WorldCheckBox current = LeftChoiceCheckBox;

        if (Math3d.dotInTriangle(Pos, current.box[0], current.box[1], current.box[2]) || 
            Math3d.dotInTriangle(Pos, current.box[1], current.box[2], current.box[3]))
            return SplineType.Left;

        current = RightChoiceCheckBox;

        if (Math3d.dotInTriangle(Pos, current.box[0], current.box[1], current.box[2]) ||
            Math3d.dotInTriangle(Pos, current.box[1], current.box[2], current.box[3]))
            return SplineType.Right;
        
		return SplineType.Front;
    }

    public override void CheckNewCreate()
    {
    //NOT DELETE!
    }


    
    

	public override void Initialize(bool activeObject)
    {
		base.Initialize (activeObject);

        type = BlockPlaneType.Fork;
		spline = splines [0];

        markers = GetComponents<Quad2dMarker>();


    }

    public override Vector4 GetOldBlockTransforms(SplineType type)
    {
		OneSpline currentSpline;
        if (type == SplineType.Left)
            currentSpline = LeftChoiceSpline;
        else
            currentSpline = RightChoiceSpline;
        Vector4 oldBlockTransforms = Vector4.zero;
		oldBlockTransforms = (Vector4)transform.TransformPoint(currentSpline.GlobalPos(1));
        Main.instance.blocksYposition += currentSpline.nextY;
		oldBlockTransforms.w = currentSpline.GetAngle(Vector3.forward, Vector3.up, 1) + transform.eulerAngles.y;
        return oldBlockTransforms;
    }

    public override void DestroyThis()
    {
        Main.instance.BlockPoolSystem.Objects[(int)type].DestroyObjectPool(this);
    }

}


