﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(OutpostPlane))]
public class OutpostPlaneEditor : BlockPlaneEditor
{	
		bool isOutpostSettings = false;
		public override void OnInspectorGUI()
		{
			ShowOutpostHeader ();
			ShowBlockPlaneSettings ();
			ShowOutpostSettings ();
		}

		public void ShowOutpostHeader()
		{
		if (Main.instance == null) 
		{
			Main _Main = FindObjectOfType<Main> ();
			_Main.EditorAwake ();
		}
			OutpostPlane myTarget = (OutpostPlane)target; 
			Color guiColor = GUI.color;
			if (skin == null)
				skin = AssetDatabase.LoadAssetAtPath ("Assets/Resources/MapEditorGuiSkin.guiskin", typeof(GUISkin)) as GUISkin;

			GUILayout.BeginHorizontal ();
			GUI.color = (myTarget.isMainSettings) ? Color.gray : Color.white;
			if (GUILayout.Button (Main.instance.generator.blockPlaneIcons[0], GUILayout.Width (64), GUILayout.Height (64)))
				myTarget.isMainSettings = !myTarget.isMainSettings;
			GUI.color = (myTarget.isColliderSettings) ? Color.gray : Color.white;
			if (GUILayout.Button (Main.instance.generator.blockPlaneIcons[1], GUILayout.Width (64), GUILayout.Height (64)))
				myTarget.isColliderSettings = !myTarget.isColliderSettings;
			GUI.color = (myTarget.isAdditionalElements) ? Color.gray : Color.white;
			if (GUILayout.Button (Main.instance.generator.blockPlaneIcons[3], GUILayout.Width (64), GUILayout.Height (64)))
				myTarget.isAdditionalElements = !myTarget.isAdditionalElements;
			GUI.color = (myTarget.isSplineSettigns) ? Color.gray : Color.white;
			if (GUILayout.Button (Main.instance.generator.blockPlaneIcons[2], GUILayout.Width (64), GUILayout.Height (64)))
				myTarget.isSplineSettigns = !myTarget.isSplineSettigns;
			GUI.color = (myTarget.isBorderSettigns) ? Color.gray : Color.white;
			if (GUILayout.Button (Main.instance.generator.blockPlaneIcons[4], GUILayout.Width (64), GUILayout.Height (64)))
				myTarget.isBorderSettigns = !myTarget.isBorderSettigns;
			GUI.color = (isOutpostSettings) ? Color.gray : Color.white;
			if (GUILayout.Button ("Outpost", GUILayout.Width (64), GUILayout.Height (64)))
			isOutpostSettings = !isOutpostSettings;

			GUI.color = guiColor;
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();
			GUILayout.Space (3);
		}

		public void ShowOutpostSettings()
		{
			OutpostPlane myTarget = (OutpostPlane)target; 

			if (isOutpostSettings) 
			{
				GUILayout.Label ("Outpost Settings", EditorStyles.boldLabel);
				GUILayout.Space (1);
				GUILayout.BeginVertical (GUI.skin.GetStyle ("Box"));
				GUILayout.Space (3);
				
				for (int i = 0; i < myTarget.supportPoints.Length; i++)
				{
					GUILayout.Label ("Support point " + i, EditorStyles.boldLabel);
					GUILayout.Space (1);
					myTarget.supportPoints [i].type = (SplineType)EditorGUILayout.EnumPopup ("Type", myTarget.supportPoints [i].type);
					myTarget.supportPoints [i].supportPosition = EditorGUILayout.Vector3Field ("Position", myTarget.supportPoints [i].supportPosition);
					myTarget.supportPoints [i].supportAngle = EditorGUILayout.FloatField ("Angle", myTarget.supportPoints [i].supportAngle);
					myTarget.supportPoints [i].outpostDoor = (OutpostDoor)EditorGUILayout.ObjectField ("Door", myTarget.supportPoints [i].outpostDoor, typeof(OutpostDoor));
					myTarget.supportPoints [i].spline = (OneSpline)EditorGUILayout.ObjectField ("Spline", myTarget.supportPoints [i].spline, typeof(OneSpline));
					myTarget.supportPoints [i].splineForward = (OneSpline)EditorGUILayout.ObjectField ("Spline Forward", myTarget.supportPoints [i].splineForward, typeof(OneSpline));
					EditorGUILayout.Separator ();
					GUILayout.Space (2);
				}

				GUILayout.EndVertical ();
				GUILayout.Space (3);
				
				if (GUILayout.Button ("Add new support point")) 
				{
				}

				if (GUILayout.Button ("Find splines and doors for support points"))
				{
					myTarget.CalculateSplinesToSupportPoints();
				}
				
			}
		}
      

}
