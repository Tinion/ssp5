﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(ForkReverse))]
public class ForkReverseEditor : BlockPlaneEditor
{
	bool isConnectSettings = false;
	public override void OnInspectorGUI()
	{
		ShowConnectHeader ();
		ShowBlockPlaneSettings ();
		ShowConnectSettings ();
	}

	public void ShowConnectHeader()
	{
		ForkReverse myTarget = (ForkReverse)target;

		Color guiColor = GUI.color;
		if (skin == null)
			skin = AssetDatabase.LoadAssetAtPath ("Assets/Resources/MapEditorGuiSkin.guiskin", typeof(GUISkin)) as GUISkin;

		if (Main.instance == null) 
		{
			Main _Main = FindObjectOfType<Main> ();
			_Main.EditorAwake ();
		}

		GUILayout.BeginHorizontal ();
		GUI.color = (myTarget.isMainSettings) ? Color.gray : Color.white;
		if (GUILayout.Button (Main.instance.generator.blockPlaneIcons[0], GUILayout.Width (64), GUILayout.Height (64)))
			myTarget.isMainSettings = !myTarget.isMainSettings;
		GUI.color = (myTarget.isColliderSettings) ? Color.gray : Color.white;
		if (GUILayout.Button (Main.instance.generator.blockPlaneIcons[1], GUILayout.Width (64), GUILayout.Height (64)))
			myTarget.isColliderSettings = !myTarget.isColliderSettings;
		GUI.color = (myTarget.isAdditionalElements) ? Color.gray : Color.white;
		if (GUILayout.Button (Main.instance.generator.blockPlaneIcons[3], GUILayout.Width (64), GUILayout.Height (64)))
			myTarget.isAdditionalElements = !myTarget.isAdditionalElements;
		GUI.color = (myTarget.isSplineSettigns) ? Color.gray : Color.white;
		if (GUILayout.Button (Main.instance.generator.blockPlaneIcons[2], GUILayout.Width (64), GUILayout.Height (64)))
			myTarget.isSplineSettigns = !myTarget.isSplineSettigns;
		GUI.color = (myTarget.isBorderSettigns) ? Color.gray : Color.white;
		if (GUILayout.Button (Main.instance.generator.blockPlaneIcons[4], GUILayout.Width (64), GUILayout.Height (64)))
			myTarget.isBorderSettigns = !myTarget.isBorderSettigns;
		GUI.color = (isConnectSettings) ? Color.gray : Color.white;
		if (GUILayout.Button (Main.instance.generator.blockPlaneIcons[5], GUILayout.Width (64), GUILayout.Height (64)))
			isConnectSettings = !isConnectSettings;

		GUI.color = guiColor;
		GUILayout.FlexibleSpace ();
		GUILayout.EndHorizontal ();
		GUILayout.Space (3);
	}

	public void ShowConnectSettings()
	{
		if (isConnectSettings) {
			GUILayout.Label ("Connect Settings", EditorStyles.boldLabel);
			GUILayout.Space (1);
			GUILayout.BeginVertical (GUI.skin.GetStyle ("Box"));
			GUILayout.Space (3);

			GUILayout.Label ("Connect Settings");

			GUILayout.EndVertical ();
			GUILayout.Space (3);
		}
	}
  
}
