﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ModuleTooltip : MonoBehaviour {


    public Image bigPicture;
    public GameObject currentGameObject;
    public RectTransform currentRectTransform;

    public Text moduleName;
    public Text moduleType;

    public Text DPS;
    public Text Using;
    public Text Accuracy;

    public Text weaponInfo;
    public ArmorData armorData;  
    
    public Text additionalName;
    public Text additionalType;
    public Text additionalInfo;

    public Image areaEffect;

    public Slider durability;
    public Slider mass;
    public Slider energyCost;
    public Text durabilityText;
    public Text massText;
    public Text energyCostText;

    public Text tabStats;

    public RectTransform weaponData = null;
    public RectTransform moduleData = null;
    public RectTransform additionalData = null;

    public Text price;

    public void CalculateArmorTooltip(Module module, int countLines, bool isToolTip)
    {
        armorData.first.text = "";
        armorData.two.text = "";
        armorData.three.text = "";
        Text armorText;
        if (module.additionalArmor > 0 || module.armor > 0 || module.hp > 0)
        {
            armorData.gameObject.SetActive(true);
            armorText = armorData.first;
            if (isToolTip)
            armorData.currentRectTransform.anchoredPosition = new Vector2(armorData.currentRectTransform.anchoredPosition.x, -165 - 15 * (countLines));
            armorData.labels.text = "";

            if (module.additionalArmor > 0)
            {
                

                string label01 = "";
                switch (module.additionalArmorType)
                {
                    case SideTriangle.Top:
                        label01 = "Лобовая";
                        break;
                    case SideTriangle.Left:
                        label01 = "Бортовая";
                        break;
                    case SideTriangle.Right:
                        label01 = "Бортовая";
                        break;
                    case SideTriangle.Back:
                        label01 = "Кормовая";
                        break;
                }

                label01 += " броня";
            
                armorData.labels.text += label01;
                 armorText.text = "+" + module.additionalArmor.ToString();

                armorText = armorData.two;
            }

            if (module.armor > 0)
            {
                if (armorText==armorData.two)
                armorData.labels.text += "\n";

                armorData.labels.text += "Броня корпуса";
                armorText.text = "+" + module.armor.ToString();

                if (armorText==armorData.first)
                    armorText = armorData.two;
                else
                    armorText = armorData.three;
            }

            if (module.hp > 0)
            {
                 if (armorText!=armorData.first)
                armorData.labels.text += "\n";

                armorData.labels.text += "Выживаемость";
                armorText.text = "+" + module.hp.ToString();

            }
        }
        else
            armorData.gameObject.SetActive(false);
    }
    public void ShowModuleInfo(Module module, RectTransform transformMaster, Vector2 offsets)
    {
        currentGameObject.SetActive(true);
        currentRectTransform.SetParent(transformMaster);
        currentRectTransform.anchoredPosition = offsets;
        ShowModule(module, true);            
    }

    public virtual void ShowModule(Module module, bool isToolTip)
    {
        Item currentItem = Main.instance.inventory.items[module.number];
        moduleName.text = currentItem.name;
        AmmoInfo ammoInfo = Main.instance.inventory.GetAmmoInfo(currentItem.ammoType);
        if (bigPicture != null)
            bigPicture.sprite = currentItem.bigPicture;
         switch (currentItem.moduleType)
        {
            case ModuleType.Weapon:
                {
                    weaponData.gameObject.SetActive(true);
                    armorData.gameObject.SetActive(false);
                    if (isToolTip)
                    {
                        additionalData.anchoredPosition = new Vector2(additionalData.anchoredPosition.x, -250);
                    }
                    else
                    {
                        additionalData.anchoredPosition = new Vector2(-176.0f, additionalData.anchoredPosition.y);
                    }
                    ModuleWeapon mw = module.GetComponent<ModuleWeapon>();
                    Gun thisGun = module.weapon.gun;
                    weaponInfo.text = "";

                    if (mw.weaponType == WeaponType.Turret)
                    {
                        if (isToolTip)
                            moduleType.text = "Орудийный модуль / Турель";
                        else
                            moduleType.text = "Турель";

                    }
                    else
                    {
                        if (isToolTip)
                            moduleType.text = "Орудийный модуль / Фиксированный";
                        else
                            moduleType.text = "Фиксированный";
                    }

                    string dps;
                    string attackSpeed;
                    float attackTime;
                                       
                    float speed = (1.0f / thisGun.bulletRate.max);
                    if (thisGun.bulletRate.enableOffset)
                    {
                        Vector2 speedV;
                        speedV.x = (1.0f / (thisGun.bulletRate.max + thisGun.bulletRate.maxOffset));
                        speedV.y = (1.0f / (thisGun.bulletRate.max - thisGun.bulletRate.maxOffset));
                        speed = Mathf.Lerp(speedV.x, speedV.y, 0.5f);
                        attackSpeed = speedV.x.ToString("F0") + " - " + speedV.y.ToString("F0");
                        dps = (thisGun.damage * speedV.x).ToString("F0") + " - " + (thisGun.damage * speedV.y).ToString("F0");                                               
                    }
                    else
                    {
                        attackSpeed = speed.ToString("F2");
                        dps = (thisGun.damage * speed).ToString("F1");
                    }

                    attackTime = (thisGun.magazine.max / speed);

                    float usingParam = Mathf.Clamp((attackTime / thisGun.recharge.max) * 100.0f,0,100.0f);
                    float acc = (1.0f - thisGun.BulletSource.offset / thisGun.BulletSource.radiusBullet) * (thisGun.BulletSource.attackSpeed / 0.7f) * (thisGun.BulletSource.radiusBullet / 0.5f);
                    float accuracy = Mathf.Clamp((acc * 100),0,100);

                    DPS.text = dps;
                    Using.text = usingParam.ToString("F0") + "%";
                    Accuracy.text = accuracy.ToString("F0") + "%";

                    weaponInfo.text = thisGun.damage + "\n";
                    weaponInfo.text += attackSpeed + "\n";
                    weaponInfo.text += thisGun.magazine.max.ToString("F0") + "\n";
                    weaponInfo.text += thisGun.recharge.GetToolTipInfo("F0","F0") + "\n";
                    weaponInfo.text += thisGun.BulletSource.attackSpeed.ToString("F2") + "\n";
                    weaponInfo.text += thisGun.BulletSource.radiusBullet.ToString("F1") + "\n";
                    weaponInfo.text += ((1.0f - thisGun.BulletSource.offset / thisGun.BulletSource.radiusBullet) * 100).ToString("F0");

                    additionalName.text = thisGun.BulletSource.name;
                    additionalType.text = thisGun.BulletSource.type;
                    additionalInfo.text = "+2 фронтальная броня\n";
                    additionalInfo.text += "+1 общая броня\n";
                    additionalInfo.text += "+100 к выживаемости\n";
                    additionalInfo.text += "+1 к удаче\n";

                    CalculateArmorTooltip(module, 5, isToolTip);

                    areaEffect.sprite = currentItem.areaEffect;
                }
            break;
                 
            case ModuleType.Active:
                {
                   

                switch (module.slotType)
                {
                    case ModuleSlot.Front:
                        moduleType.text = "Носовой ";
                        break;
                    case ModuleSlot.Side:
                        moduleType.text = "Прочий ";
                        break;
                    case ModuleSlot.Top:
                        moduleType.text = "Верхний ";
                        break;
                    case ModuleSlot.Back:
                        moduleType.text = "Кормовой ";
                        break;
                    case ModuleSlot.Down:
                        moduleType.text = "Бортовой ";
                        break;
                }

                weaponData.gameObject.SetActive(false);

                if (isToolTip)
                    additionalData.anchoredPosition = new Vector2(additionalData.anchoredPosition.x, -115);
                else
                    additionalData.anchoredPosition = new Vector2(-378.0f, additionalData.anchoredPosition.y);

                moduleType.text += "модуль";
                switch (currentItem.ammoType)
                {
                    
                    case AmmoType.Missiles:
                    {
                        if (isToolTip)
                        moduleType.text += " / Ракетные пилоны";
                        MissilePylons mp = module.GetComponent<MissilePylons>();

                   
                    additionalName.text = mp.Missile.name;
                    additionalType.text = mp.Missile.type;
                    additionalInfo.text = mp.Missile.damage + " eд. урона\n";
                    additionalInfo.text += mp.Missile.radius + " м радиус поражения\n";
                    additionalInfo.text += mp.Missile.upperForce + " атм. мощность ударной волны\n";
                    additionalInfo.text += mp.Missile.range.ToString() + " м дальность атаки\n";
                    additionalInfo.text += mp.magazine.max.ToString() + " шт. количество залпов";

                    CalculateArmorTooltip(module, 5, isToolTip);

                        
                    areaEffect.sprite = currentItem.areaEffect;

                    }
                    break;
                    
                    
                                        case AmmoType.Toxic:
                                        {
                                            if (isToolTip)
                                            moduleType.text += " / Разливатель";
                                            OilThrower ot = module.GetComponent<OilThrower>();

                                            additionalName.text = "Баллоны с маслом ";
                                            additionalType.text = "Выводит из строя колесную технику";

                                            additionalInfo.text = ot.magazine.max.ToString() + " сек. длительность\n";
                                            additionalInfo.text += ot.recharge.GetToolTipInfo("F0", "F0") + " сек. перезарядка\n";
                                            additionalInfo.text += ot.oilSize.ToString("F1") + " ширина разлива";

                                            CalculateArmorTooltip(module, 3, isToolTip);

                                            areaEffect.sprite = currentItem.areaEffect;

                                        }
                                        break;

                    
                    case AmmoType.Steel:
                    {
                        if (isToolTip)
                        moduleType.text += " / Оружие ближнего боя";
                        SawModule sm = module.GetComponent<SawModule>();

                        additionalName.text = "Циркулярные пилы";
                        additionalType.text = "Отбрасывают и наносят урон";

                        additionalInfo.text = sm.damage.ToString() + " ед. урона\n";
                        additionalInfo.text += sm.force.ToString("F0") + " сила отталкивания\n";
                        additionalInfo.text += (sm.wear * 100).ToString("F0") + "% износ"; 

                        CalculateArmorTooltip(module, 3, isToolTip);

                        areaEffect.sprite = currentItem.areaEffect;

                    }
                    break;
                    
                }

                areaEffect.sprite = currentItem.areaEffect;
                }
                break;
                  

                 ///

                 
            case ModuleType.Passive:

                weaponData.gameObject.SetActive(false);

                if (isToolTip)
                additionalData.anchoredPosition = new Vector2(additionalData.anchoredPosition.x, -115);
                else
                    additionalData.anchoredPosition = new Vector2(-378.0f, additionalData.anchoredPosition.y);
                additionalInfo.text = "";
                areaEffect.sprite = currentItem.areaEffect;
                switch (currentItem.slotType)
                {
                    case ModuleSlot.Wheels:
                        {
                            Wheels wh = module.GetComponent<Wheels>();
                            moduleType.text = "Шасси";

                            additionalName.text = wh.additionalName;
                            additionalType.text = wh.additionalType;

                            additionalInfo.text = "Маневренность +" + (wh.addSpeed + Main.instance.hero.steerMin).ToString("F0") + " ед.\n";
                            additionalInfo.text += "Грузоподъемность +" + wh.addMaxMass.ToString("F0") + " кг\n";                           
                            additionalInfo.text += "Сопротивление замедлению: " + ((wh.resistanceSlip) * 100.0f).ToString("F0") + "%\n";
                            additionalInfo.text += "Потеря скорости снижена на " + (wh.factorSlow * 100.0f).ToString("F0") + "%";

                            CalculateArmorTooltip(module, 4, isToolTip);
                           
                        }
                        break;
                    case ModuleSlot.Engine:
                        {
                            Engine en = module.GetComponent<Engine>();
                            moduleType.text = "Двигатель";

                            additionalName.text = "Электромотор";
                            additionalType.text = "Не нуждается в топливе";

                            additionalInfo.text = "Мощность: " + (en.power + en.addMaxMass + 1000).ToString("F0") + " ЛС\n";
                            additionalInfo.text += "Грузоподъемность +" + (en.addMaxMass + 1250).ToString("F0") + " кг\n";
                            additionalInfo.text += "Коэфф. ускорения: " + (en.accelBonus * 100.0f).ToString("F0") + "%\n";

                            
                            additionalInfo.text += "Набор скорости увеличен на " + ((en.factorSpeed) * 100.0f).ToString("F0") + "%";

                            CalculateArmorTooltip(module, 4, isToolTip);

                        }
                        break;
                    case ModuleSlot.Generator:
                        {
                            moduleType.text = "Генератор энергии";
                            //moduleInfo.text += "Не требует ресурсов для работы";

                            additionalName.text = "Магнитный генератор";
                            additionalType.text = "Не нуждается в топливе";

                            additionalInfo.text = "Выработка : " + Main.instance.inventory.items[module.number].energy.ToString("F0") + " кДж/сек";

                            CalculateArmorTooltip(module, 1, isToolTip);

                        }
                        break;
                    default:
                        {
                            moduleType.text = "Обшивка";

                            additionalName.text = "Вольфрамовый сплав";
                            additionalType.text = "Повышает выживаемость";

                            CalculateArmorTooltip(module, 0, isToolTip);
                        }
                        break;

                        
                }

                break;
                 
        }

         if (isToolTip)
         {

             durability.value = module.durability.current / module.durability.max;
             durabilityText.text = module.durability.current.ToString("F0") + "/" + module.durability.max.ToString("F0");

			mass.value = Mathf.Clamp01(Main.instance.inventory.items[module.number].mass / (Main.instance.inventory.paramSliders.maxMass - Main.instance.hero.vehicle.currentRigidbody.mass));
             massText.text = Main.instance.inventory.items[module.number].mass.ToString("F0");

             if (module.slotType != ModuleSlot.Generator)
             {
                 energyCost.gameObject.SetActive(true);
                 energyCostText.gameObject.SetActive(true);
                 energyCost.value = Mathf.Clamp01(Main.instance.inventory.items[module.number].energy / (Main.instance.hero.energyLimit.max - Main.instance.hero.energyLimit.current));
                 energyCostText.text = Main.instance.inventory.items[module.number].energy.ToString("F0");
             }
             else
             {
                 energyCost.gameObject.SetActive(false);
                 energyCostText.gameObject.SetActive(false);
             }
         }
         else
         {
             tabStats.text = "";
             tabStats.text += module.durability.max.ToString("F0") + " ед. \n";
             tabStats.text += Main.instance.inventory.items[module.number].mass.ToString("F0") + " кг";
             if (module.slotType != ModuleSlot.Generator)
                 tabStats.text += "\n" + Main.instance.inventory.items[module.number].energy.ToString() + " кДж";


            
         }
    }
	
}
