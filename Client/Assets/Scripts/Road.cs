﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[RequireComponent (typeof (MeshCollider))]
public class Road : MonoBehaviour {

	public MeshCollider collider;
	public float opacity = 0.1f;
	public float size = 5.0f;
	public Vector2 sizeMinMax = new Vector2(1.5f, 5.0f);
	public int tabSwitch = 0;
	public bool isEdit = false;



}
