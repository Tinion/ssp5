﻿using UnityEngine;
using System.Collections;

public class MarkerToGlobal : MonoBehaviour {

    public Quad2dMarker quad2d;

    private bool isIniatilize = false;

    void Initialize()
    {
        Main.instance = GameObject.FindObjectOfType<Main>();
        isIniatilize = true;
    }
    void Update()
    {
        if (!isIniatilize)
            Initialize();

		if (quad2d.Check(Main.instance.hero.transform.position, true) && Main.instance.gameMode == Mode.Free)
        {
            ToGlobal();
        }
    }

    void ToGlobal()
    {
        //Debug.Log("TOO");
        Main.instance.screenFader.fadeState = ScreenFader.FadeState.In;
        //Debug.Log("Global");
        //Main.instance.hero.currentRigidbody.velocity = Vector3.zero;
        //Main.instance.hero.currentRigidbody.angularVelocity = Vector3.zero;
       // Main.instance.gameMode = Mode.Global;
       // Main.instance.globalMap.gameObject.SetActive(true);
        //Main.instance.globalMap.Open();
    }
}
