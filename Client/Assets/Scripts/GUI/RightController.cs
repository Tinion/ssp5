﻿using UnityEngine;
using System.Collections;

public class RightController : TouchController
{
    bool isDrawMarker = false;

    public ModuleSystem moduleSystem;
    public Texture marker;
    public Texture Attack;

    private float angle;
    public float roundInDist;
    private float roundInDistSqr;

	public override void Initialize() 
    {
        base.Initialize();
        _controllerPosition.x = Screen.width * 0.9f + (controllerPos.y * size + 1);
        _controllerPosition.y = Screen.height * 0.15f - (controllerPos.y * size + 1);

        roundInDist = roundInDist * size;
        roundInDistSqr = roundInDist * roundInDist;
    }

    public override void TouchOff()
    {
        guiTex.texture = Default;
        for (int i = 0; i < moduleSystem.moduleWeapon.Count; i++)
            moduleSystem.moduleWeapon[i].weapon.StopShoot();
   
        isDrawMarker = false; 
    }

    public override void Calculate(Vector2 position)
    {
        for (int i = 0; i < moduleSystem.moduleWeapon.Count; i++)
        {
            if (moduleSystem.moduleWeapon[i] != null)
            {
                if (position.x * position.x + position.y * position.y > roundInDistSqr)
                {
                    guiTex.texture = Default;
                    if (moduleSystem.moduleWeapon[i].weaponType == WeaponType.Turret)
                    {
                        Vector3 dir = new Vector3(-position.x, -position.y, 0);
                        dir.Normalize();
                        angle = Math3d.GetAngle(dir, Vector3.up);
                        if (position.x < 0)
                            angle = 360 - angle;
                        isDrawMarker = true;
                    }

                }
                else
                {
                    guiTex.texture = Attack;
                    angle = 180.0f;
                    isDrawMarker = false;
                }

                moduleSystem.moduleWeapon[i].weapon.StartShoot();
                if (moduleSystem.moduleWeapon[i].weaponType == WeaponType.Turret)
                {
                    moduleSystem.moduleWeapon[i].transform.localRotation = Quaternion.Euler(new Vector3(0.0f, 180.0f - angle, 0.0f));
                }
            }
        }
    }
	/*
    void Update()
    {
        //if (Input.touchCount > 0)
        //for (int i=0; i<Input.touchCount; i++)
        if (true)
        {
            //Touch touch = Input.GetTouch(i);
            //float mouseX = touch.position.x - _controllerPositionX;
            //float mouseY = touch.position.y - _controllerPositionY;

            float mouseX = Input.mousePosition.x - _controllerPositionX;
            float mouseY = Input.mousePosition.y - _controllerPositionY;

            if (mouseX * mouseX + mouseY * mouseY <= roundOutDist * roundOutDist)
            {

                
                
                
                //if (touch.phase == TouchPhase.Ended)
                //{
                //    guiTex.texture = Default;
                //    moduleSystem.Weapon.ShootStop();
                //    isDrawMarker = false;   
                //}
                
            }
                
            else
            {
                guiTex.texture = Default;
                moduleSystem.Weapon.isAttack = false;
                isDrawMarker = false;    
            }
                 
        }
        
        //else
        //{
        //    guiTex.texture = Default;
        //    moduleSystem.Weapon.ShootStop();
        //    isDrawMarker = false;   
        //}
        
        
        
    }
   */
    void OnGUI()
    {
        //GUI.BeginGroup(new Rect(Screen.width - 330, 150, 320, 270));
        //GUI.Label(new Rect(15, 15, 45, 45), Input.touchCount.ToString(), guiSkin.GetStyle("label"));
        //GUI.Label(new Rect(0, 15, 200, 45), hui, guiSkin.GetStyle("label"));
        //GUI.EndGroup();

        if (isDrawMarker)
        {
            Rect TexturePos = new Rect(_controllerPosition.x - 27.0f * size, Screen.height - _controllerPosition.y + 31.5f * size, 50.0f * size, 59.0f * size);
            Matrix4x4 matrixBackup = GUI.matrix;
            GUIUtility.RotateAroundPivot(-angle, new Vector2(_controllerPosition.x, Screen.height - _controllerPosition.y));
            GUI.color = new Color(1.0f, 1.0f, 1.0f, 0.4f);
            GUI.DrawTexture(TexturePos, marker);
            GUI.matrix = matrixBackup;
        }
    }


}
