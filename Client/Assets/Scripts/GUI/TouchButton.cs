﻿using UnityEngine;
using System.Collections;

[System.Flags]
public enum ButtonType
{
    SecondWeapon,
    Skill,
    Menu,
}

public class TouchButton : TouchController
{
    public Texture Pressed;
    public ButtonType type;

    public override bool CheckHit(Vector2 position)
    {
        if (Main.instance.isTouchPad)
        {
            if (guiTex.HitTest(touch.position))
                return true;
            else
                return false;
        }
        else
        {
            if (this.GetComponent<GUITexture>().HitTest(Input.mousePosition))
                return true;
            else
                return false;
        }
    }

    public override void TouchOff()
    {
        //Debug.Log("press");
        guiTex.texture = Default;
    }

    public override void Calculate(Vector2 position)
    {
        /*
        guiTex.texture = Pressed;
        if (type == ButtonType.SecondWeapon)
        {
            if (Main.instance.hero.moduleSystem.Side!=null)
            {
                Main.instance.hero.moduleSystem.Side.Shoot();
            }
        }
        */
    }
}
