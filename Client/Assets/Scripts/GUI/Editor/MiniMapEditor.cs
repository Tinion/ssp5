﻿

using UnityEngine;
using System.Collections;
    #if UNITY_EDITOR
using UnityEditor;
#endif


[CustomEditor(typeof(MiniMap))]
public class MiniMaEditor : Editor
{
    const int UsedTypes = 8;
    public void OnSceneGUI()
    {
        MiniMap myTarget = (MiniMap)target;
        if (Event.current.type == EventType.MouseDown && Event.current.button == 0)
        {
            Ray worldRay = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
            RaycastHit hitInfo;
            if (Physics.Raycast(worldRay, out hitInfo))
            {
                Debug.Log(hitInfo.point);
                
                
                for (int horizontal = 0; horizontal < myTarget.count; horizontal++)
                    for (int vertical = 0; vertical < myTarget.count; vertical++)
                {
                    if ((hitInfo.point.x > (myTarget.startPosition.x + horizontal * myTarget.size) && hitInfo.point.x < (myTarget.startPosition.x + horizontal * myTarget.size + myTarget.size))
                    && (hitInfo.point.z > (myTarget.startPosition.z + vertical * myTarget.size) && hitInfo.point.z < (myTarget.startPosition.z + vertical * myTarget.size + myTarget.size)))
                    {
                        myTarget.isSelected = true;
                        myTarget.selectedCell = new Vector2i(horizontal, vertical);

                    }
                }
                 
            }
         }

        Selection.activeGameObject = myTarget.gameObject;  
    }

    public override void OnInspectorGUI()
    {


        MiniMap myTarget = (MiniMap)target;
        EditorGUILayout.BeginHorizontal();
        myTarget.size = EditorGUILayout.FloatField("Size", myTarget.size);
        
        myTarget.count = EditorGUILayout.IntField("X Count", myTarget.count);
        EditorGUILayout.EndHorizontal();
        myTarget.startPosition = EditorGUILayout.Vector3Field("Start Position", myTarget.startPosition);

        myTarget.isHiddenMode = EditorGUILayout.Toggle("Hidden mode", myTarget.isHiddenMode);

         if (myTarget.isSelected)
            {
                int horCount = Mathf.Clamp(Mathf.CeilToInt(EditorGUIUtility.labelWidth / 32.0f),0,UsedTypes);
                int vertCount = Mathf.CeilToInt(UsedTypes / horCount);
                if (UsedTypes % horCount > 0)
                    vertCount++;
                Rect rect;
                Vector2i ch = new Vector2i(0, 0);
                EditorGUILayout.BeginHorizontal();

                rect = GUILayoutUtility.GetRect(0, 64.0f, GUILayout.Width(64.0f));
                if (GUI.Button(rect, ""))
                {
                    myTarget.SelectType(48);
                }
                GUI.DrawTextureWithTexCoords(rect, myTarget.miniMapPlaneMat.GetTexture("_DiffTex"), new Rect(0 * 0.125f, 0.875f - 6 * 0.125f, 0.125f, 0.125f));
                

                for (int vertical = 0; vertical < vertCount; vertical++)
                {
                    if (vertical!=0)
                        EditorGUILayout.BeginHorizontal();
                    for (int horizontal = 0; horizontal < horCount; horizontal++)
                    {
                        int number = ch.y * 8 + ch.x;
                        if (number < UsedTypes)
                        {
                            rect = GUILayoutUtility.GetRect(64.0f * horizontal, 64.0f, GUILayout.Width(64.0f));
                            if (GUI.Button(rect, ""))
                            {
                                myTarget.SelectType(number);
                            }
                            GUI.DrawTextureWithTexCoords(rect, myTarget.miniMapPlaneMat.GetTexture("_DiffTex"), new Rect(ch.x * 0.125f, 1.0f - ch.y * 0.125f, 0.125f, -0.125f));

                            if (ch.x < 7)
                                ch.x++;
                            else
                            {
                                ch.x = 0;
                                ch.y++;
                            }
                        }
                    }
                    EditorGUILayout.EndHorizontal();

                }

                



            }
         if (GUILayout.Button("UpdatePlane"))
         {
             myTarget.UpdatePlane();
         }
                  

         if (GUILayout.Button("Save MiniMap Data"))
         {
              #if UNITY_EDITOR
             myTarget.SaveMinimapData();
            #endif
         }


    }
}
