﻿using UnityEngine;
using System.Collections;

public class LeftController : TouchController
{
    public Texture Drag;
    public Texture DragBoost;
    public Texture DragBrake;
    public Texture DragLeft;
    public Texture DragRight;
    public Texture Steer;
    public Texture Helm;
    public Texture Speedometer;
  
    public int accelClampFactor;
    public float steerClampFactor;
    public float steerBattleFactor;
    public Vector2 helmSize;
    public Vector2 ellipse;
    
	public float vertical;


    private float angle;

    float steer;
    float accelMax;

    public override void Initialize()
    {
        base.Initialize();
        
        
        _controllerPosition.x = Screen.width * 0.1f - (controllerPos.x * size + 1);
        _controllerPosition.y = Screen.height * 0.15f - (controllerPos.y * size + 1);

        roundOutDist = 127.0f * size;
        roundOutDistSqr = roundOutDist * roundOutDist;

        ellipse.x *= size;
        ellipse.y *= size;
        vertical *= size;
    }

    public override void TouchOff()
    {
        steer = 0;
        if (Main.instance.gameMode == Mode.Free)
        {
            accelMax = 0;
            Main.instance.hero.InputAccel(0.0f);
        }
        else
        {
            guiTex.texture = Drag;
            Main.instance.hero.InputSteer(0);
        }
    }

    public override void Calculate(Vector2 position)
    {
        //if (position.x * position.x + position.y * position.y <= roundOutDistSqr)
        //{
            if (Main.instance.gameMode == Mode.Battle)
                CalculateBattleMode(position);    
            else
                CalculateFreeMode(position);
        /*   
        }
        else
        {
            steer = 0;
            guiTex.texture = Drag;


            if (Main.instance.gameMode == Mode.Free)
            {
                
                guiTex.texture = Default;
                Main.instance.hero.InputAccel(0.0f);
                Main.instance.hero.InputSteer(steer);
            }
            
                
        }
        Main.instance.hero.InputSteer(steer);
        */
        
    }




    void CalculateBattleMode(Vector2 position)
    {
        guiTex.texture = Drag;
        
        Vector2 pos = new Vector2(position.x, position.y - vertical);
        if (Math3d.Contains(ellipse, pos) < 1.0f)
        {
            guiTex.texture = DragBoost;
            Main.instance.hero.Boost(1.0f);
        }
        else
        {
            pos = new Vector2(position.x, position.y + vertical);
            if (Math3d.Contains(ellipse, pos) < 1.0f)
            {
                guiTex.texture = DragBoost;
                Main.instance.hero.Brake(1.0f);
            }
            else
            {
                if (position.x > 0)
                {
                    steer += Time.deltaTime * 4;
                    guiTex.texture = DragRight;
                    
                }
                else
                {
                    steer -= Time.deltaTime * 4;
                    guiTex.texture = DragLeft;
                    
                }
                steer = Mathf.Clamp(steer, -1, 1);    
                Main.instance.hero.InputSteer(steer);    
                
            }
        }

    }
    

    void CalculateFreeMode(Vector2 position)
    {
        guiTex.texture = Default;

            
            if (position.y > 0)
                angle = Math3d.GetAngle(0, 1, position.x, position.y);
            else
            {
                angle = Math3d.GetAngle(0, -1, position.x, position.y);
                angle *= -1;
            }
            float angleAbs = Mathf.Abs(angle);
            float clamp = 0;
            if (angleAbs > 60)
                clamp = 1;
            else
                if (angleAbs < steerClampFactor)
                    clamp = 0;
                else
                    clamp = Mathf.Lerp(0, 1, (angleAbs - steerClampFactor) / (60 - steerClampFactor*2));

            if (angle < 0)
                steer += Time.deltaTime * 4;
            else
                steer -= Time.deltaTime * 4;
                
            steer = Mathf.Clamp(steer, -clamp, clamp);

            float dist = Vector2.Distance(Vector2.zero,position);

            float maxClamp = roundOutDist - (accelClampFactor * size + roundOutDist * 0.3f);
            if (dist > maxClamp)
                    accelMax = 1.0f;
                else
                if (dist < accelClampFactor * size)
                        accelMax = 0.0f;
                    else
                    accelMax = Mathf.Lerp(0, 1, (dist - accelClampFactor * size) / (maxClamp - accelClampFactor * size));
            if (position.y > 0)
                Main.instance.hero.Boost(accelMax);
            else
                Main.instance.hero.Brake(accelMax);
    }

    
    void OnGUI()
    {
        
       // GUI.BeginGroup(new Rect(Screen.width - 330, 50, 320, 270));
        //GUI.Label(new Rect(0, 15, 45, 45), accel.ToString("F2"), guiSkin.GetStyle("label"));
        //GUI.Label(new Rect(50, 15, 75, 45), steer.ToString("F2"), guiSkin.GetStyle("label"));
        //GUI.EndGroup();

        if (Main.instance.gameMode == Mode.Free)
        {
            Rect TexturePos = new Rect(_controllerPosition.x - (controllerSize.x * size) / 2, Screen.height - _controllerPosition.y - (controllerSize.y * size) / 2, controllerSize.x * size, controllerSize.y * size);
           // Rect TexturePos2 = new Rect(_controllerPosition.x - 6.0f * size, Screen.height - _controllerPosition.y - 33 * size, 12.0f * size, -Mathf.Lerp(0, 68 * size, accelMax));
            Matrix4x4 matrixBackup = GUI.matrix;
            GUIUtility.RotateAroundPivot(Mathf.Lerp(-60, 60, (steer + 1) * 0.5f), new Vector2(_controllerPosition.x, Screen.height - _controllerPosition.y));
            //GUI.DrawTexture(TexturePos2, Speedometer);
            GUI.color = new Color(1.0f, 1.0f, 1.0f, 0.4f);
            GUI.DrawTexture(TexturePos, Steer);
            GUI.matrix = matrixBackup;
        }
        else
        {
            /*
            Rect TexturePos = new Rect(_controllerPosition.x - (helmSize.x * size) / 2, Screen.height - _controllerPosition.y - (helmSize.y * size) / 2, helmSize.x * size, helmSize.y * size);
            Matrix4x4 matrixBackup = GUI.matrix;
            GUIUtility.RotateAroundPivot(Mathf.Lerp(-15, 15, (steer + 1) * 0.5f), new Vector2(_controllerPosition.x, Screen.height - _controllerPosition.y));
            GUI.color = new Color(1.0f, 1.0f, 1.0f, 0.4f);
            GUI.DrawTexture(TexturePos, Helm);
            GUI.matrix = matrixBackup;
            */
        }
    }
     

}
