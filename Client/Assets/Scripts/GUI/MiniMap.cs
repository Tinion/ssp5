﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Flags]
public enum MiniMapCellType
{
    
    FreeExplored = 0,
    DragExplored = 1,
    CheckPoint = 2,
    Barrier = 3,
    Engine = 4,
    Treasure = 5,
    OnDrag = 6,
    OutDrag = 7,

    

    None = 48,
}

[System.Serializable]
public struct Vector2i
{
    public int x;
    public int y;

    public Vector2i(int _x, int _y)
    {
        x = _x;
        y = _y;
    }

};

[System.Serializable]
public struct Vector3i
{
	public int x;
	public int y;
	public int z;
	public int count;
	public bool isDelete;
	#if UNITY_EDITOR
	public BlockPlane.SubdCheckSide[]  checkSides;
	#endif


	public Vector3i(int _x, int _y, int _z)
	{
		x = _x;
		y = _y;
		z = _z;
		isDelete = false;
		#if UNITY_EDITOR
		checkSides = null;
		#endif
		count = 4;
	}

	public bool isContain(int _w)
	{
		if (x == _w || y == _w || z == _w)
			return true;
		else
			return false;
		
	}

	public int isContain(Vector3i _vec3i)
	{
		int count = 0;
		if (isContain (_vec3i.x))
			count++;
		if (isContain (_vec3i.y))
			count++;
		if (isContain (_vec3i.z))
			count++;
		return count;

	}

};

[System.Serializable]
public struct Vector4i
{
    public int x;
    public int y;
    public int z;
    public int w;

    public Vector4i(int _x, int _y, int _z, int _w)
    {
        x = _x;
        y = _y;
        z = _z;
        w = _w;
    }

};

[System.Serializable]
public class MiniMapCell
{
    public MiniMapCellType type;
    public bool hidden;
    public Material currentMaterial;

    public MiniMapCell(MiniMapCellType _miniMapCelltype, Material source, bool _hidden)
    {
        type = _miniMapCelltype;
        currentMaterial = new Material(source);
        hidden = _hidden;
    }

    ~MiniMapCell()
    {
        currentMaterial = null;
    }
};


public class MiniMap : MonoBehaviour
{

    private const float srcSize = 0.125f;
    private const int srcCount = 8;

  
    public bool isHiddenMode = false;
    public List<MiniMapCell> miniMapCell;
    public Mesh miniMapPlane;
    public Material miniMapPlaneMat;
    public bool isSelected = false;
    public Vector2i selectedCell = new Vector2i(0, 0);

    public float size = 10.0f;
    public int count = 11;
    public Vector3 startPosition = Vector3.zero;

    void OnDrawGizmos()
    {


        if (miniMapCell != null)
        {

            Gizmos.color = Color.blue;

            for (int horizontal = 0; horizontal < count + 1; horizontal++)
            {
                for (int vertical = 0; vertical < count + 1; vertical++)
                {
                    if (horizontal == count && vertical != count)
                    {
                        Gizmos.DrawLine(new Vector3(horizontal * size + startPosition.x, 0, vertical * size + startPosition.z), new Vector3(horizontal * size + startPosition.x, 0, vertical * size + size + startPosition.z));

                    }
                    else
                        if (vertical == count && horizontal != count)
                        {
                            Gizmos.DrawLine(new Vector3(horizontal * size + startPosition.x, 0, vertical * size + startPosition.z), new Vector3(horizontal * size + size + startPosition.x, 0, vertical * size + startPosition.z));
                        }
                        else
                            if (vertical != count && horizontal != count)
                            {
                                Gizmos.DrawLine(new Vector3(horizontal * size + startPosition.x, 0, vertical * size + startPosition.z), new Vector3(horizontal * size + size + startPosition.x, 0, vertical * size + startPosition.z));
                                Gizmos.DrawLine(new Vector3(horizontal * size + startPosition.x, 0, vertical * size + startPosition.z), new Vector3(horizontal * size + startPosition.x, 0, vertical * size + size + startPosition.z));
                                DrawCell(horizontal, vertical);
                            }
                }
            }
        }
        else
        {
            Debug.Log("CREATE");
            miniMapCell = new List<MiniMapCell>();
            for (int i = 0; i < count * count; i++)
                miniMapCell.Add(new MiniMapCell(MiniMapCellType.None, miniMapPlaneMat, false));
        }

    }

    public void UpdatePlane()
    {
        BoxCollider boxCollider = transform.Find("plane").GetComponent<BoxCollider>();
        boxCollider.center = new Vector3(startPosition.x + (size * count) / 2, 0, startPosition.z + (size * count) / 2);
        boxCollider.size = new Vector3(size * count, 1, size * count);
        miniMapCell.Clear();
        miniMapCell = null;

    }

    void DrawCell(int horizontal, int vertical)
    {
        int number, xN, yN;
        bool selective = false;

        if (selectedCell.x.Equals(horizontal) && selectedCell.y.Equals(vertical) && isSelected)
            selective = true;

        number = (int)miniMapCell[horizontal + vertical * count].type;

            if (!selective)
            {
                    xN = Mathf.CeilToInt(number % srcCount);
                    yN = Mathf.CeilToInt(number / srcCount);
            }
            else
            {
                    xN = 7;
                    yN = 5;
            }


            int countLine = horizontal + vertical * count;
            miniMapCell[countLine].currentMaterial.SetFloat("inputX", xN * (srcSize));
            miniMapCell[countLine].currentMaterial.SetFloat("inputY", 1 - yN * (srcSize));

            if (miniMapCell[countLine].hidden)
                miniMapCell[countLine].currentMaterial.SetFloat("inputA", 0.25f);
            else
                miniMapCell[countLine].currentMaterial.SetFloat("inputA", 1.0f);

            if (miniMapCell[countLine].type != MiniMapCellType.None || selective)
            {
                Graphics.DrawMesh(miniMapPlane, new Vector3(horizontal * size + startPosition.x + size / 2, 0, vertical * size + startPosition.z + size / 2), Quaternion.Euler(0.0f, 180.0f, 0.0f), miniMapCell[horizontal + vertical * count].currentMaterial, 0);
            }

            selective = false;

    }

   

    public void SelectType(int number)
    {
        int ord = selectedCell.x + count * selectedCell.y;
        isSelected = false;
        miniMapCell[ord].type = (MiniMapCellType)number;
        if (isHiddenMode)
            miniMapCell[ord].hidden = true;
        else
            miniMapCell[ord].hidden = false;

        Debug.Log(miniMapCell[ord].type.ToString());
        

    }

    public void SaveMinimapData()
    {
        #if UNITY_EDITOR
        LevelData.SaveMiniMapData(startPosition, count, size, miniMapCell);
#endif
    }


    


}
