﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Dest.Math;

[System.Flags]
public enum GlobalMapStatus
{
    Wait,
    Rotate,
    Move,
    Fade,
	Dialog,
}

[System.Flags]
public enum TypeGlobalMarker
{
    City,
    Village,
}

[System.Flags]
public enum GlobalEventType
{
	Passed,
	Dist,
	Enter,
}

[System.Serializable]
public class Missions
{
	public bool isActivated;
	public GlobalEvent[] events;
	public Vector2 coord;
}

[System.Serializable]
public class GlobalEvent
{
	public GlobalEventType eventType;

	public bool isActivated;
	public Dialogical.DialogueTree dialog;

	public int passed;
	public int waypointIndex;
}

[System.Serializable]
public class GlobalMapWaypoint
{
    public bool isExplored;
	public int index;
    public Vector2 coord;
    public string name;
    public OutpostPlane platform;
    public float angle;
    public TypeGlobalMarker type;

}
public class GlobalMap : MonoBehaviour {

    public Camera globalCam;
    public Object mapMarkerBig;
    public Object mapMarkerSmall;
    public UnityEngine.UI.Image map;
    public Transform canvas;
    public GlobalMapWaypoint[] waypoints;
    public UnityEngine.UI.Image car;
    public UnityEngine.UI.Image target;
    public UnityEngine.UI.Image path;
	public RectTransform dialog;
    public UnityEngine.UI.Slider fuelSlider;
	public UnityEngine.UI.Slider cargoSlider;
	public Missions[] missions;
	public UnityEngine.UI.Text globalTimeText;
	public UnityEngine.UI.Text day;
	public UnityEngine.UI.Text mount;
	public UnityEngine.UI.Text year;
	public Missions currentMission;

	public int Path = 0;

    List<GameObject> markers;
    public GlobalMapStatus mode;
    public GlobalMapWaypoint current;
    public GlobalMapWaypoint targetWaypoint;

    public CounterTimerF fuel;
    public CounterTimerF movementTimer;
    private Quaternion prevCarRot;
    private Quaternion nextCarRot;

    private Vector2 pathTargetPos;
    private Vector2 pathCarPos;
    private float lenght;
    private bool isTargerIsWaypoint;
	// Use this for initialization

    public void Open()
    {
        if (!Main.instance)
        {
            current = waypoints[0];
            Main.instance = FindObjectOfType<Main>();
            fuel.Restore();
            markers = new List<GameObject>();
        }

		currentMission = missions [0];
        ReCalculateWaypoints();

        car.rectTransform.anchoredPosition = current.coord;
        car.rectTransform.rotation = Quaternion.Euler(new Vector3(0, 0, current.angle));
    }

	public void SwithTime(float time)
	{
		int minutes = Mathf.FloorToInt (time / 60.0f);
		int hours = Mathf.FloorToInt (minutes / 60.0f);
		minutes = Mathf.FloorToInt (minutes % 60.0f);
		int secundes = Mathf.FloorToInt (time % 60.0f);
		string fullTime = hours.ToString ("00") + ":" + minutes.ToString ("00") + ":" + secundes.ToString ("00"); 
		globalTimeText.text = fullTime;
	}

	public void ReCalculateWaypoints()
    {
        int count = markers.Count;
        for (int i = 0; i < markers.Count; i++)
        {
            Destroy(markers[i]);
        }

        markers.Clear();

        for (int i =0; i<waypoints.Length; i++)
        {
            if (waypoints[i].isExplored)
            {
                Object source = mapMarkerBig;
                switch (waypoints[i].type)
                {
                    case TypeGlobalMarker.City:
                        source = mapMarkerBig;
                        break;
                    case TypeGlobalMarker.Village:
                        source = mapMarkerSmall;
                        break;
                }
                GameObject marker = GameObject.Instantiate(source, Vector3.zero, Quaternion.identity) as GameObject;
                marker.name = "_" + i;
                RectTransform current = marker.GetComponent<RectTransform>();
                current.parent = map.rectTransform;
                current.anchoredPosition = waypoints[i].coord;
                current.localScale = new Vector3(1.0f, 1.0f, 1.0f);
                marker.GetComponentInChildren<UnityEngine.UI.Text>().text = waypoints[i].name;
                markers.Add(marker);
            }
        }

        path.rectTransform.SetAsLastSibling();
        target.rectTransform.SetAsLastSibling();
        car.rectTransform.SetAsLastSibling();
		dialog.SetAsLastSibling();

    }

    void SetCarToRotate(float angle)
    {
        prevCarRot = car.rectTransform.localRotation;
        nextCarRot = Quaternion.Euler(new Vector3(0,0,angle));
        movementTimer.max = 0.5f;
        movementTimer.Restore();
        mode = GlobalMapStatus.Rotate;
    }

    void CalculatePathVisual()
    {
        Vector2 carPos = car.rectTransform.anchoredPosition;
        Vector2 targetPos = target.rectTransform.anchoredPosition;
        Vector2 pathPos = Vector2.Lerp(carPos, targetPos, 0.5f);
        float dist = Vector2.Distance(carPos, targetPos);
        Vector2 dir = (targetPos - carPos);
        dir.Normalize();
        float angle =  Math3d.GetAngle(Vector2.up.x, Vector2.up.y, dir.x, dir.y);

        path.rectTransform.anchoredPosition = pathPos;
        path.rectTransform.sizeDelta = new Vector2(path.rectTransform.sizeDelta.x, dist);
        path.rectTransform.localRotation = Quaternion.Euler(new Vector3(0, 0, angle));
    }

	void ToLocalMap()
    {
        Main.instance.screenFader.fadeState = ScreenFader.FadeState.In;
   
    }

    public void ToRealLocalMap()
    {
        
    }
	// Update is called once per frame
	void Update () 
    {


        
        

		if (Input.GetMouseButton(0) && mode!=GlobalMapStatus.Fade && mode!=GlobalMapStatus.Dialog)
        {
            
            Vector3[] corners = new Vector3[4];
            Vector2 localPos = Vector2.zero;
            map.rectTransform.GetWorldCorners(corners);
            Rect newRect = new Rect(corners[0], corners[2] - corners[0]);
            if (newRect.Contains(Input.mousePosition))
            {
                localPos.x = (Input.mousePosition.x - newRect.x);
                localPos.y = (Input.mousePosition.y - newRect.y);
                target.rectTransform.anchoredPosition = localPos * (1 / canvas.localScale.x);

                target.enabled = true;
                path.enabled = true;
                pathCarPos = car.rectTransform.anchoredPosition;
                pathTargetPos = target.rectTransform.anchoredPosition;

                float dist = Vector2.Distance(pathCarPos, pathTargetPos);
                lenght = dist;
                Vector2 dir = (pathTargetPos - pathCarPos);
                dir.Normalize();
                float angle = Math3d.GetAngle(Vector2.up.x, Vector2.up.y, dir.x, dir.y);

                SetCarToRotate(angle);

				for (int i = 0; i < waypoints.Length; i++)
                {
                    if (waypoints[i].isExplored)
                    {
                        float distTarget = Vector2.Distance(waypoints[i].coord, target.rectTransform.anchoredPosition);
                        if (distTarget < 20.0f)
                        {
                            targetWaypoint = waypoints[i];
                            isTargerIsWaypoint = true;
                            return;
                        }
                    }
                }

                isTargerIsWaypoint = false;
            }
        }

        switch (mode)
        {
            case GlobalMapStatus.Rotate:
                {
                    if (movementTimer.isEnd(Time.deltaTime * 2))
                    {
                        mode = GlobalMapStatus.Move;
                        movementTimer.max = lenght / 20.0f;
                        movementTimer.Restore();
                    }
                    else
                    car.rectTransform.localRotation = Quaternion.Lerp(nextCarRot, prevCarRot, movementTimer.current / movementTimer.max);
                    CalculatePathVisual();
                }
                break;
            case GlobalMapStatus.Move:
                {
                    float comsumption = 2;
                    if (movementTimer.isEnd(Time.deltaTime * comsumption))
                    {
                        mode = GlobalMapStatus.Wait;
                        target.enabled = false;
                        path.enabled = false;

                        if (isTargerIsWaypoint)
                            ToLocalMap();

                    }
                    else
                    {
                        car.rectTransform.anchoredPosition = Vector2.Lerp(pathTargetPos, pathCarPos, movementTimer.current / movementTimer.max);
                        fuel.current -= (comsumption/25);
                        if (fuel.current < 0)
                            fuel.current = 0;
                        fuelSlider.value = fuel.current / fuel.max;
						Path++;

					for (int i = 0; i < currentMission.events.Length; i++)
						if (!currentMission.events [i].isActivated)
						{
							switch (currentMission.events [i].eventType) 
							{
							case GlobalEventType.Passed:
								if (currentMission.events [i].passed <= Path) {
									Main.instance.dialog.tree = currentMission.events [i].dialog;
									Main.instance.dialog.Activate ();
									Main.instance.gameMode = Mode.DialogGlobal;
									currentMission.events [i].isActivated = true;
									mode = GlobalMapStatus.Dialog;
								}
								break;
							case GlobalEventType.Dist:
								if (Vector2.Distance (waypoints [currentMission.events [i].waypointIndex].coord, car.rectTransform.anchoredPosition) < currentMission.events [i].passed) {
									Main.instance.dialog.tree = currentMission.events [i].dialog;
									Main.instance.dialog.Activate ();
									Main.instance.gameMode = Mode.DialogGlobal;
									currentMission.events [i].isActivated = true;
									mode = GlobalMapStatus.Dialog;
								}
								break;
							}
						}
                    }
                    car.rectTransform.localRotation = nextCarRot;
                    CalculatePathVisual();

                    for (int i=0; i<waypoints.Length; i++)
                    {
                        if (!waypoints[i].isExplored)
                        {
                            float dist = Vector2.Distance(waypoints[i].coord, car.rectTransform.anchoredPosition);
                            if (dist < 10.0f)
                            {
                                mode = GlobalMapStatus.Wait;
                                target.enabled = false;
                                path.enabled = false;

                                waypoints[i].isExplored = true;
                                ReCalculateWaypoints();
                                targetWaypoint = waypoints[i];
 
                                ToLocalMap();
                            }
                        }
                    }
                }
                break;
        }
	}
}
