﻿using UnityEngine;
using System.Collections;

public class BattleUI : MonoBehaviour {

    public GameObject currentGameObject;
    public RectTransform currentRectTransform;
    public DurabilityInfo durabilityInfo;
}
