﻿using UnityEngine;
using System.Collections;
using Dest.Math;

public class Slot : MonoBehaviour {


    public Camera cam;
    public RectTransform current;
    public Renderer renderer;
    public UnityEngine.UI.Image currentImageBackground;
    public UnityEngine.UI.Image currentImageIcon;
    public RectTransform parent;
    public int number;
    public bool isActive;

    public durabilitySlider durSlider;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    public bool UpdateSlot()
    {
        if (!Main.instance)
            Main.instance = FindObjectOfType<Main>();

        float x = parent.anchoredPosition.x - parent.sizeDelta.x / 2 + current.anchoredPosition.x;
        float y = Screen.height / 2 + (parent.anchoredPosition.y + parent.sizeDelta.y / 2) + current.anchoredPosition.y;

        if (Input.mousePosition.x > x - current.sizeDelta.x / 2 && Input.mousePosition.x < x + current.sizeDelta.x / 2 && Input.mousePosition.y > y - current.sizeDelta.y / 2 && Input.mousePosition.y < y + current.sizeDelta.y / 2)
        {

            currentImageBackground.color = Color.red;

            if (isActive)
            {
                Main.instance.inventory.inventory.SetAsLastSibling();
                Main.instance.inventory.shop.button01.SetAsLastSibling();
                Main.instance.inventory.shop.button02.SetAsLastSibling();
                current.SetAsLastSibling();
                Main.instance.inventory.moduleTooltip.ShowModuleInfo(Main.instance.hero.moduleSystem.otherModules[number], parent, new Vector2(460.0f, 0.0f));
                if (Input.GetMouseButtonUp(1))
                {
                    Main.instance.hero.moduleSystem.EquipModule(number);
                }

                return true;
            }
            else
                return false;
            //renderer.material = Main.instance.baseCamera.heroOutlined;
            
        }
        else
        {
            //Main.instance.inventory.moduleTooltip.gameObject.SetActive(false);
            currentImageBackground.color = Color.white;
            //renderer.material = Main.instance.baseCamera.hero;
            return false;
        }
    }

}
