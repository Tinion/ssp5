﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor (typeof(ForkCreator))]
public class ForkCreatorEditor : Editor {

	private ForkCreator fc;

	private void OnEnable ()
	{
		fc = (ForkCreator)target;

	}

	void OnSceneGUI ()
	{
		switch (fc.state) 
		{
		case ForkCreatorState.SphereSize:
			Handles.DrawLine (fc.center, fc.center + Vector3.up);
			Handles.DrawWireDisc (fc.center, Vector3.up, fc.radius);
			break;
		default:
			break;
		}
	}

	public override void OnInspectorGUI ()
	{
		fc.center = EditorGUILayout.Vector3Field ("Center", fc.center);
		fc.radius = EditorGUILayout.Slider ("Radius", fc.radius, 0.1f, 300f);

		fc.mainType = (SplineType)EditorGUILayout.EnumPopup ("Main Spline", fc.mainType);


		EditorGUI.BeginChangeCheck();
		fc.splineDistance = EditorGUILayout.Slider ("Center Distance", fc.splineDistance, 0.1f, 20f);
		if (EditorGUI.EndChangeCheck ()) 
		{
			fc.centerSpline.UpdateSpline (fc.splineDistance, fc.splineDistance);
		}

		EditorGUI.BeginChangeCheck();
		fc.leftSplineDistanceStart = EditorGUILayout.Slider ("Left Distance Start", fc.leftSplineDistanceStart, 0.1f, 100f);
		fc.leftSplineDistanceEnd = EditorGUILayout.Slider ("Left Distance End", fc.leftSplineDistanceEnd, 0.1f, 100f);
		fc.rightSplineDistanceStart = EditorGUILayout.Slider ("Right Distance Start", fc.rightSplineDistanceStart, 0.1f, 100f);
		fc.rightSplineDistanceEnd = EditorGUILayout.Slider ("Right Distance End", fc.rightSplineDistanceEnd, 0.1f, 100f);
		if (EditorGUI.EndChangeCheck ()) 
		{
			fc.leftSpline.UpdateSpline (-fc.leftSplineDistanceStart, fc.leftSplineDistanceEnd);
			fc.rightSpline.UpdateSpline (-fc.rightSplineDistanceStart, fc.rightSplineDistanceEnd);
		}

		if (GUILayout.Button ("Start")) 
		{
			fc.EditorEnable ();
		}

		if (GUILayout.Button ("Create Dots")) 
		{
			fc.CreateDots ();
		}

		if (GUILayout.Button ("CutSplinesStart")) 
		{
			fc.CutSplinesStart ();
		}

		if (GUILayout.Button ("CutSplinesEnd")) 
		{
			fc.CutSplinesEnd ();
		}

		if (GUILayout.Button ("CutSegments")) 
		{
			fc.CutSegments ();
		}

		if (GUILayout.Button ("CreateTriangulateSurface")) 
		{
			fc.CreateTriangulateSurface();
		}
	}
}
