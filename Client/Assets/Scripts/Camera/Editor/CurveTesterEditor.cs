﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System.IO;
using Dest.Math;

namespace SplineSystem
{

[CustomEditor(typeof(CurveTester))]
public class CurveTesterEditor : Editor
{


		public CurveTester  tester;
	




    public override void OnInspectorGUI()
    {
		tester = (CurveTester)target;
			CurveTester.isUpdate = EditorGUILayout.Toggle ("Updated", CurveTester.isUpdate);
			if (!CurveTester.isUpdate)
			if (GUILayout.Button ("Update")) 
			{
				//tester.Draw ();
				tester.SceneGUI ();
			}
		tester.type = (SplineType)EditorGUILayout.EnumPopup("Type", tester.type);
			CurveTester.t = EditorGUILayout.Slider ("Percent", CurveTester.t, 0, 1);
			CurveTester.debugSize = EditorGUILayout.Slider ("Gizmo Size", CurveTester.debugSize, 0.1f, 1f);

			for (int i = 0; i < tester.debugModule.Count; i++) 
			{
				EditorGUILayout.BeginHorizontal ();
				EditorGUILayout.LabelField (tester.debugModule [i].name, EditorStyles.boldLabel);
				tester.debugModule [i].isActive = EditorGUILayout.Toggle (tester.debugModule [i].isActive);
				EditorGUILayout.EndHorizontal ();

				GUILayout.Space (3);
				EditorGUILayout.BeginVertical ("box");

				tester.debugModule [i].type = (CurveTesterType)EditorGUILayout.EnumPopup ("Type", tester.debugModule [i].type);

				switch (tester.debugModule [i].type) {
				case CurveTesterType.SelfPercent:
					tester.debugModule [i].selfType = EditorGUILayout.Slider ("Self Percent", tester.debugModule [i].selfType, 0, 1f);
					break;
				case CurveTesterType.GameObject:
					tester.debugModule [i].selfGO = (GameObject)EditorGUILayout.ObjectField("Self GameObject", tester.debugModule [i].selfGO, typeof(GameObject), true);
					break;
				}

				for (int j = 0; j < tester.debugModule [i].floats.Count; j++) 
					tester.debugModule [i].floats [j].param = EditorGUILayout.FloatField (tester.debugModule [i].floats [j].name, tester.debugModule [i].floats [j].param);

				for (int j = 0; j < tester.debugModule [i].bools.Count; j++) 
					tester.debugModule [i].bools [j].param = EditorGUILayout.Toggle (tester.debugModule [i].bools [j].name, tester.debugModule [i].bools [j].param);
				
				for (int j = 0; j < tester.debugModule [i].colors.Count; j++) 
					tester.debugModule [i].colors [j].param = EditorGUILayout.ColorField (tester.debugModule [i].colors [j].name, tester.debugModule [i].colors [j].param);

				for (int j = 0; j < tester.debugModule [i].gameObjects.Count; j++) 
					tester.debugModule [i].gameObjects [j].param = (GameObject)EditorGUILayout.ObjectField(tester.debugModule [i].gameObjects [j].name, tester.debugModule [i].gameObjects [j].param, typeof(GameObject), true);
				EditorGUILayout.EndVertical ();
			}

		
        
    }



	public virtual void OnSceneGUI()
	{
			if (CurveTester.isUpdate) 
			{
				Handles.BeginGUI ();
				CurveTester tester = (CurveTester)target;
		
				tester.SceneGUI ();
				Handles.EndGUI ();
			}
	}

	

	

	

}
}