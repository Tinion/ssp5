﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(CarAntiRollBar))]
public class CarAntiRollBarEditor : Editor
{
  public override void OnInspectorGUI()
    {
        CarAntiRollBar myTarget = (CarAntiRollBar)target;
        myTarget.AntiRoll = EditorGUILayout.FloatField("Сила", myTarget.AntiRoll);
        myTarget.AntiRollFactor = EditorGUILayout.FloatField("Фактор", myTarget.AntiRollFactor);
        myTarget.AntiRollBias = EditorGUILayout.FloatField("Смещение", myTarget.AntiRollBias);
        myTarget.StrictMode = EditorGUILayout.IntField("Строгость", myTarget.StrictMode);
             
    }
}
