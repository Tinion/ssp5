﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System.IO;
using Dest.Math;

[CustomEditor (typeof(SplineRailing))]
public class SplineRailingEditor : Editor
{
	private const int QUAD_SIZE = 10;
	private Color CURVE_COLOR = new Color (0.8f, 0.8f, 0.8f);
	private bool mustCreateNewNode = false;

	private bool isMaterialsOpen = false;

	private SplineRailing se;
	Vertex selection = null;

	private RoadTemplateAsset m_WaitingToLoad = null;
	private int loadtingSelection = -1;

	private void OnEnable ()
	{
		se = (SplineRailing)target;

	}

	public override void OnInspectorGUI ()
	{
		//serializedObject.Update();
		se = (SplineRailing)target;

		EditorGUI.BeginChangeCheck ();
		se.splineType = (SplineType)EditorGUILayout.EnumPopup ("SplineType", se.splineType);
		se.source = (Mesh)EditorGUILayout.ObjectField ("Source", se.source, typeof(Mesh));
		se.material = (Material)EditorGUILayout.ObjectField ("Material", se.material, typeof(Material));


		se.scale = EditorGUILayout.Vector3Field ("Scale", se.scale);
		se.widthOffset = EditorGUILayout.FloatField ("Width", se.widthOffset);
		se.startTime = EditorGUILayout.Slider ("Start Time", se.startTime, 0, 1f);
		se.endTime = EditorGUILayout.Slider ("End Time", Mathf.Clamp01(se.endTime), 0, 1f);
		se.rotation = EditorGUILayout.Vector3Field ("Rotation", se.rotation);
		se.zOffset = EditorGUILayout.FloatField ("Z Offset", se.zOffset);
		se.density = EditorGUILayout.Slider ("Density", se.density, 1f, 30f);
		se.startRoll = EditorGUILayout.FloatField ("Start Roll", se.startRoll);
		se.endRoll = EditorGUILayout.FloatField ("End Roll", se.endRoll);

		//se.TextureScale = EditorGUILayout.FloatField ("Texture Scale", se.TextureScale);
		if (EditorGUI.EndChangeCheck ())
			se.CreateMeshes ();
	}
}
		
