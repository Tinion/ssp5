﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(MainCamera))]
public class MainCameraEditor : Editor
{
	  

    public override void OnInspectorGUI()
	{
		MainCamera myTarget = (MainCamera)target;
		float width = EditorGUIUtility.currentViewWidth;

		GUIStyle _labelStyle;
		_labelStyle = new GUIStyle ();
		_labelStyle.alignment = TextAnchor.MiddleCenter;
		_labelStyle.clipping = TextClipping.Overflow;
		_labelStyle.fontSize = 12;
		/*
		//GUI.Box(GUILayoutUtility.GetRect(width, 20.0f), "Cam Percent");
		//EditorGUI.ProgressBar (EditorGUILayout.GetControlRect (true, 20.0f, _labelStyle, GUILayout.Height (20.0f)), myTarget.GetCamPercent() / 1.0f, "_currentCamPercent");
		//EditorGUILayout.Separator ();
		//EditorGUI.ProgressBar (EditorGUILayout.GetControlRect (true, 20.0f, _labelStyle, GUILayout.Height (20.0f)), myTarget.GetCamPercentRotation() / 1.0f, "_currentCamPersentForRotation");
		//EditorGUILayout.Separator ();
		//EditorGUI.ProgressBar (EditorGUILayout.GetControlRect (true, 20.0f, _labelStyle, GUILayout.Height (20.0f)), myTarget.GetHeroPercent() / 1.0f, "_currentHeroPercent");
		//EditorGUILayout.Separator ();
		//EditorGUI.ProgressBar (EditorGUILayout.GetControlRect (true, 20.0f, _labelStyle, GUILayout.Height (20.0f)), myTarget.cameraDotCurrentPercent / 1.0f, myTarget.cameraDotCurrentPercent.ToString()/);
		//if(myTarget._currentCamPlane!=null)
		//GUILayout.Label("localForkNumber " + myTarget._currentCamPlane.localForkNumber.ToString());
		*/
		if (Application.isPlaying)
		{
		GUI.Box (GUILayoutUtility.GetRect (width, 20.0f), "Cam Percent");
		EditorGUI.ProgressBar (EditorGUILayout.GetControlRect (true, 20.0f, _labelStyle, GUILayout.Height (20.0f)), Hero.instance.verticleSliceNormal / 1.0f, "Hero Vertical");
		EditorGUILayout.Separator ();
		EditorGUI.ProgressBar (EditorGUILayout.GetControlRect (true, 20.0f, _labelStyle, GUILayout.Height (20.0f)), Hero.instance.boostSlice / 1.0f, "Boost Slice");
		EditorGUILayout.Separator ();
			EditorGUI.ProgressBar (EditorGUILayout.GetControlRect (true, 20.0f, _labelStyle, GUILayout.Height (20.0f)), 1f-Controller.instance.switchTime, (Controller.instance.switchTime == 0) ? "Free" : ((Controller.instance.switchTime == 1) ? "Battle" : "Battle <=> Free"));
		EditorGUILayout.Separator ();
		}
		base.OnInspectorGUI ();

    }
}
