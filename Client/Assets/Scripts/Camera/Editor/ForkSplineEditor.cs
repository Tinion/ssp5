﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System.IO;
using Dest.Math;

[CustomEditor(typeof(ForkSpline))]
public class ForkSplineEditor : OneSplineEditor
{
    public override void OnInspectorGUI()
    {
		base.OnInspectorGUI();
	}

	public override void OnSceneGUI()
	{
		base.OnSceneGUI ();
	}
}