﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System.IO;
using Dest.Math;

[CustomEditor (typeof(SplineExtrusion))]
public class SplineExtrusionEditor : Editor
{
	private const int QUAD_SIZE = 10;
	private Color CURVE_COLOR = new Color (0.8f, 0.8f, 0.8f);
	private bool mustCreateNewNode = false;

	private bool isMaterialsOpen = false;

	private SplineExtrusion se;
	Vertex selection = null;

	private RoadTemplateAsset m_WaitingToLoad = null;
	private int loadtingSelection = -1;

	private void OnEnable ()
	{
		se = (SplineExtrusion)target;

	}

	void OnSceneGUI ()
	{
		Event e = Event.current;
		if (e.type == EventType.MouseDown) {
			Undo.RegisterCompleteObjectUndo (se, "change extruded shape");
			// if control key pressed, we will have to create a new vertex if position is changed
			if (e.alt) {
				mustCreateNewNode = true;
			}
		}
		if (e.type == EventType.MouseUp) {
			mustCreateNewNode = false;
		}



		Vector3 splineStartTangent = se.spline.GetTangent (0);
		Vector3 splineStart = se.spline.GlobalPos (0);
		Quaternion q = OneCubicBezierCurve.GetRotationFromTangent (splineStartTangent);

		foreach (RoadTemplate roadTemplate in se.roadTemplates) {
			
			foreach (RoadPart rp in roadTemplate.roadParts) {
				int index2 = 0;
				Vector3 partPoint = se.transform.TransformPoint (q * (new Vector2 (rp.pivot * ((rp.pivot > 0) ? se.spline.GetSegmentScale (0).x : se.spline.GetSegmentScale (0).y), 0)) + splineStart);
				Handles.Label (partPoint - Vector3.up/2, rp.type.ToString ());
				foreach (Vertex v in rp.vertex) {
					if (roadTemplate.isOpenVertexEdit) {
						// we create point and normal relative to the spline start where the shape is drawn
						Vector3 point = se.transform.TransformPoint (q * (v.point + new Vector2 (rp.pivot * ((rp.pivot > 0) ? se.spline.GetSegmentScale (0).x : se.spline.GetSegmentScale (0).y), 0)) + splineStart);
						Vector3 normal = se.transform.TransformPoint (q * (v.point + new Vector2 (rp.pivot * ((rp.pivot > 0) ? se.spline.GetSegmentScale (0).x : se.spline.GetSegmentScale (0).y), 0) + v.normal) + splineStart);
						Handles.Label (point, index2.ToString ());
						index2++;
						if (v == selection) {
							// draw the handles for selected vertex position and normal
							float size = HandleUtility.GetHandleSize (point) * 0.3f;
							float snap = 0.1f;

							// create a handle for the vertex position
							Vector3 movedPoint = Handles.Slider2D (0, point, splineStartTangent, Vector3.right, Vector3.up, size, Handles.CircleHandleCap, new Vector2 (snap, snap));
							if (movedPoint != point) {
								// position has been moved
								Vector2 newVertexPoint = Quaternion.Inverse (q) * (se.transform.InverseTransformPoint (movedPoint) - splineStart);
								if (mustCreateNewNode) {
									// We must create a new node
									mustCreateNewNode = false;
									Vertex newVertex = new Vertex (newVertexPoint, v.normal, v.uCoord);
									int i = rp.vertex.IndexOf (v);
									if (i == rp.vertex.Count - 1) {
										rp.vertex.Add (newVertex);
									} else {
										rp.vertex.Insert (i + 1, newVertex);
									}
									selection = newVertex;
								} else {
									v.point = newVertexPoint;
									// normal must be updated if point has been moved
									normal = se.transform.TransformPoint (q * (v.point + v.normal) + splineStart);
								}
								se.GenerateMesh ();
							} else {
								// vertex position handle hasn't been moved
								// create a handle for normal
								Vector3 movedNormal = Handles.Slider2D (normal, splineStartTangent, Vector3.right, Vector3.up, size, Handles.CircleHandleCap, snap);
								if (movedNormal != normal) {
									// normal has been moved
									v.normal = (Vector2)(Quaternion.Inverse (q) * (se.transform.InverseTransformPoint (movedNormal) - splineStart)) - v.point;
									se.GenerateMesh ();
								}
							}

							Handles.BeginGUI ();
							DrawQuad (HandleUtility.WorldToGUIPoint (point), CURVE_COLOR);
							DrawQuad (HandleUtility.WorldToGUIPoint (normal), Color.red);
							Handles.EndGUI ();
						} else {
							// we draw a button to allow selection of the vertex
							Handles.BeginGUI ();
							Vector2 p = HandleUtility.WorldToGUIPoint (point);
							if (GUI.Button (new Rect (p - new Vector2 (QUAD_SIZE / 2, QUAD_SIZE / 2), new Vector2 (QUAD_SIZE, QUAD_SIZE)), GUIContent.none)) {
								selection = v;
							}
							Handles.EndGUI ();
						}

						// draw an arrow from the vertex location to the normal
						Handles.color = Color.red;
						Handles.DrawLine (point, normal);

						// draw a line between that vertex and the next one
						int index = rp.vertex.IndexOf (v);
						int nextIndex = index == rp.vertex.Count - 1 ? 0 : index + 1;
						Vertex next = rp.vertex [nextIndex];
						Handles.color = CURVE_COLOR;
						Vector3 vAtSplineEnd = se.transform.TransformPoint (q * next.point + splineStart);
						Handles.DrawLine (point, vAtSplineEnd);
					}
				}

			}
		}
	}

	void DrawQuad (Rect rect, Color color)
	{
		Texture2D texture = new Texture2D (1, 1);
		texture.SetPixel (0, 0, color);
		texture.Apply ();
		GUI.skin.box.normal.background = texture;
		GUI.Box (rect, GUIContent.none);
	}

	void DrawQuad (Vector2 position, Color color)
	{
		DrawQuad (new Rect (position - new Vector2 (QUAD_SIZE / 2, QUAD_SIZE / 2), new Vector2 (QUAD_SIZE, QUAD_SIZE)), color);
	}

	public void CreateNewHazeContextPreset (RoadTemplate template)
	{
			
		RoadTemplateAsset asset = template.GetContextAsset ();
		asset.context.sourceMaterials = new List<Material> ();
			int index = 0;
		for (int i = 0; i < asset.context.roadParts.Count; i++) 
		{
					asset.context.sourceMaterials.Add (se.sourceMaterials [asset.context.roadParts [i].matId]);
					asset.context.roadParts [i].matId = index;
					index++;
		}

		string[] paths = Directory.GetDirectories (Application.dataPath, "RoadTemplates", SearchOption.AllDirectories);
		if (paths.Length == 0 || paths.Length > 1) {
			Debug.LogError ("RoadTemplates::CreateNewHazeContextPreset: Unable to find the DeepSky Haze folder! Has it been renamed?");
			return;
		}
		int assetind = paths [0].IndexOf ("Assets", 0);
		string rootpath = paths [0].Substring (assetind);
		string contextpath = rootpath + Path.DirectorySeparatorChar + "Contexts";

		if (!AssetDatabase.IsValidFolder (contextpath)) {
			AssetDatabase.CreateFolder (rootpath, "Contexts");
		}

		string path = EditorUtility.SaveFilePanelInProject("Asset", "Untitled", "asset", "Save Road Asset", "RoadTemplates/Contexts/");
		//AssetDatabase.CreateAsset (asset, contextpath + Path.DirectorySeparatorChar + template.name + ".asset");
		AssetDatabase.CreateAsset (asset, path);
		AssetDatabase.SaveAssets ();

		EditorGUIUtility.PingObject (asset);
	}


	public override void OnInspectorGUI ()
	{
		//serializedObject.Update();
		se = (SplineExtrusion)target;

		se.spline = (OneSpline)EditorGUILayout.ObjectField ("Spline", se.spline, typeof(OneSpline));
		se.splineType = (SplineType)EditorGUILayout.EnumPopup ("SplineType", se.splineType);

		EditorGUI.BeginChangeCheck ();
		se.size = EditorGUILayout.FloatField ("Size", se.size);

		se.TextureScale = EditorGUILayout.FloatField ("Texture Scale", se.TextureScale);
		if (EditorGUI.EndChangeCheck ())
			se.GenerateMesh ();

        if (GUILayout.Button((se.isHide) ? "Show meshes" : "Hide meshes"))
        {
            se.isHide = !se.isHide;
            if (se.isHide)
            {
                foreach(Transform child in se.transform)
                {
                    child.gameObject.hideFlags = HideFlags.HideInHierarchy;
                }
            }
            else
            {
                foreach (Transform child in se.transform)
                {
                    child.gameObject.hideFlags = HideFlags.None;
                }
            }
        }

		if (se.sourceMaterials == null)
			se.sourceMaterials = new List<Material> ();


		EditorGUILayout.BeginVertical ("box");

		EditorGUILayout.BeginHorizontal ();
		if (GUILayout.Button ((!isMaterialsOpen) ? "▼" : "▲", GUILayout.Width (30)))
			isMaterialsOpen = !isMaterialsOpen;
			
		

		if (GUILayout.Button ("Add Material", GUILayout.ExpandWidth(true))) 
			se.sourceMaterials.Add (new Material (Shader.Find("Standard")));
		
		EditorGUILayout.EndHorizontal ();

		if (isMaterialsOpen) 
		{
			for (int i = 0; i < se.sourceMaterials.Count; i++) 
			{
				EditorGUILayout.BeginHorizontal ();
				
				EditorGUILayout.LabelField (i.ToString ());

				se.sourceMaterials [i] = (Material)EditorGUILayout.ObjectField (se.sourceMaterials [i], typeof(Material));
				GUI.color = Color.red;
				if (GUILayout.Button ("X", GUILayout.Width (20))) 
				{
					se.sourceMaterials.RemoveAt (i);
					se.GenerateMesh ();
				}
				GUI.color = Color.white;


				EditorGUILayout.EndHorizontal ();
			}
		}


		EditorGUILayout.EndVertical ();
		//Material mat = (Material)EditorGUILayout.ObjectField (mat, typeof(Material));

		string[] segments = new string[se.spline.segments.Count];
		for (int i = 0; i < se.spline.segments.Count; i++)
			segments [i] = i.ToString ();

		GUI.color = Color.white;
		for (int i = 0; i < se.roadTemplates.Count; i++) 
		{
			GUI.color = new Color(0.7f,0.7f,0.7f);
			GUILayout.BeginVertical ("box");
			GUI.color = Color.white;
			EditorGUILayout.BeginHorizontal ();
			EditorGUI.BeginChangeCheck ();

			float width2 = EditorGUIUtility.currentViewWidth - 130;
			if (GUILayout.Button ((se.roadTemplates [i].isOpenVertexEdit) ? "▲" : "▼", GUILayout.Width (30)))
				se.roadTemplates [i].isOpenVertexEdit = !se.roadTemplates [i].isOpenVertexEdit;

			float fullSegmentLenght = 0;
			float coeffError = 1;
			foreach (OneSplineSegment segment in se.spline.segments) 
					fullSegmentLenght += segment.lenght;

			if (fullSegmentLenght > se.spline.Length)
				coeffError = se.spline.Length/fullSegmentLenght;
			foreach (OneSplineSegment segment in se.spline.segments) 
			{
				
				GUI.color = segment.color;
				int index = se.spline.segments.IndexOf(segment);
				GUIStyle style = (index == 0) ? GUI.skin.FindStyle ("ButtonLeft") : GUI.skin.FindStyle ("ButtonMid");
				style = (index == se.spline.segments.Count-1) ? GUI.skin.FindStyle ("ButtonRight") : style;
				string name = "";
				int indexSegment = se.spline.segments.IndexOf (segment);
				if (se.roadTemplates [i].id == indexSegment) {
					GUI.backgroundColor = Color.gray;
					style.fontStyle = FontStyle.Bold;
					name = "[" + indexSegment.ToString () + "]";
				} else {
					GUI.backgroundColor = Color.white;
					style.fontStyle = FontStyle.Normal;
					name = indexSegment.ToString ();
				}

				if (GUILayout.Button (name, style, GUILayout.Width ((segment.lenght * coeffError) / se.spline.Length * width2)))
					se.roadTemplates [i].id = indexSegment;
				GUI.color = Color.white;
				GUI.backgroundColor = Color.white;
				style.fontStyle = FontStyle.Normal;
			}
					

				GUI.color = Color.white;
			if (EditorGUI.EndChangeCheck ())
				se.GenerateMesh ();

			if (GUILayout.Button ("S", GUI.skin.FindStyle ("ButtonLeft"), GUILayout.Width (30))) {
				CreateNewHazeContextPreset (se.roadTemplates [i]);
			}

			if (GUILayout.Button ("L", GUI.skin.FindStyle ("ButtonRight"), GUILayout.Width (30))) {
				int ctrlID = EditorGUIUtility.GetControlID (FocusType.Passive);
				EditorGUIUtility.ShowObjectPicker<RoadTemplateAsset> (null, false, "", ctrlID);
				loadtingSelection = i;
			}

			EditorGUILayout.EndHorizontal ();


			if (se.roadTemplates [i].isOpenVertexEdit) {

				//if (GUILayout.Button ("Delete selected vertex")) {
				//	Undo.RegisterCompleteObjectUndo (se, "delete vertex");
				//	se.roadTemplates [i].ShapeVertices.Remove (selection);
				//	selection = null;
				//se.GenerateMesh ();
				//}

				EditorGUI.BeginChangeCheck ();

				for (int z = 0; z < se.roadTemplates [i].roadParts.Count; z++) 
				{
					GUI.color = Color.gray;
					GUILayout.Space (3);
					GUILayout.BeginVertical ("box");
					GUI.color = Color.white;
					{
						se.roadTemplates [i].roadParts [z].type = (RoadPartType)EditorGUILayout.EnumPopup ("Type", se.roadTemplates [i].roadParts [z].type);
						if (se.roadTemplates [i].roadParts [z].type != RoadPartType.Road) 
						{
							se.roadTemplates [i].roadParts [z].pivot = EditorGUILayout.FloatField ("Pivot", se.roadTemplates [i].roadParts [z].pivot);
						
							for (int j = 0; j < se.roadTemplates [i].roadParts [z].vertex.Count; j++) 
							{
								GUILayout.BeginVertical ("box");
								{
									GUILayout.BeginHorizontal ();
									GUILayout.Label (j.ToString ());
									GUILayout.BeginVertical ();
									se.roadTemplates [i].roadParts [z].vertex [j].point = EditorGUILayout.Vector2Field ("Point", se.roadTemplates [i].roadParts [z].vertex [j].point);
									se.roadTemplates [i].roadParts [z].vertex [j].normal = EditorGUILayout.Vector2Field ("Normal", se.roadTemplates [i].roadParts [z].vertex [j].normal);
									se.roadTemplates [i].roadParts [z].vertex [j].uCoord = EditorGUILayout.Slider ("U Coord", se.roadTemplates [i].roadParts [z].vertex [j].uCoord, 0, 1);
									GUILayout.EndVertical ();
									GUI.color = Color.red;
									if (GUILayout.Button ("X", GUILayout.Width (20))) 
									{
										se.roadTemplates [i].roadParts [z].vertex.RemoveAt (j);
										se.GenerateMesh ();
									}
									GUI.color = Color.white;
									GUILayout.EndHorizontal ();
								}
								GUILayout.EndVertical ();

							}
						} 
						else 
						{
							if (se.roadTemplates [i].roadParts [z].vertex.Count < 2) 
							{
								if (se.roadTemplates [i].roadParts [z].vertex.Count < 1) 
								{
									int vertexLast = se.roadTemplates [i].roadParts [z-1].vertex.Count - 1;
									se.roadTemplates [i].roadParts [z].vertex.Add (new Vertex (se.roadTemplates [i].roadParts [z-1].vertex [vertexLast].point,
										se.roadTemplates [i].roadParts [z-1].vertex [vertexLast].normal,
										0));
								}

		
								se.roadTemplates [i].roadParts [z].vertex.Add (new Vertex (se.roadTemplates [i].roadParts [z+1].vertex [0].point,
									se.roadTemplates [i].roadParts [z+1].vertex [0].normal,
									1));
							}

							for (int j = 0; j < se.roadTemplates [i].roadParts [z].vertex.Count; j++) {
								GUILayout.BeginVertical ("box");
								{
									GUILayout.BeginHorizontal ();
									GUILayout.Label (j.ToString ());
									GUILayout.BeginVertical ();
									if (j == 0) {
										GUI.enabled = false;
										se.roadTemplates [i].roadParts [z].vertex [j].point = EditorGUILayout.Vector2Field ("Point", se.roadTemplates [i].roadParts [z - 1].vertex [se.roadTemplates [i].roadParts [z - 1].vertex.Count - 1].point);
										se.roadTemplates [i].roadParts [z].vertex [j].normal = EditorGUILayout.Vector2Field ("Normal", se.roadTemplates [i].roadParts [z - 1].vertex [se.roadTemplates [i].roadParts [z - 1].vertex.Count - 1].normal);
										GUI.enabled = true;
									} else if (j == (se.roadTemplates [i].roadParts [z].vertex.Count - 1)) {
										GUI.enabled = false;
										se.roadTemplates [i].roadParts [z].vertex [j].point = EditorGUILayout.Vector2Field ("Point", se.roadTemplates [i].roadParts [z + 1].vertex [0].point);
										se.roadTemplates [i].roadParts [z].vertex [j].normal = EditorGUILayout.Vector2Field ("Normal", se.roadTemplates [i].roadParts [z + 1].vertex [0].normal);
										GUI.enabled = true;
									} else {
										se.roadTemplates [i].roadParts [z].vertex [j].point = EditorGUILayout.Vector2Field ("Point", se.roadTemplates [i].roadParts [z].vertex [j].point);
										se.roadTemplates [i].roadParts [z].vertex [j].normal = EditorGUILayout.Vector2Field ("Normal", se.roadTemplates [i].roadParts [z].vertex [j].normal);
									}

									se.roadTemplates [i].roadParts [z].vertex [j].uCoord = EditorGUILayout.Slider ("U Coord", se.roadTemplates [i].roadParts [z].vertex [j].uCoord, 0, 1);
									GUILayout.EndVertical ();
									if (j != 0 || j != se.roadTemplates [i].roadParts [z].vertex.Count - 1) {
										GUI.color = Color.red;
										if (GUILayout.Button ("X", GUILayout.Width (20))) {
											se.roadTemplates [i].roadParts [z].vertex.RemoveAt (j);
											se.GenerateMesh ();
										}
										GUI.color = Color.white;
									}
									GUILayout.EndHorizontal ();
								}
								GUILayout.EndVertical ();
							}
						}
						GUILayout.BeginHorizontal ();
						if (GUILayout.Button ("Add Vertex", GUILayout.Width (200))) 
							{
							if (se.roadTemplates [i].roadParts [z].vertex == null)
								se.roadTemplates [i].roadParts [z].vertex = new System.Collections.Generic.List<Vertex> ();

							if (se.roadTemplates [i].roadParts [z].vertex.Count == 0)
								se.roadTemplates [i].roadParts [z].vertex.Add (new Vertex (Vector3.zero, Vector3.up,	0));
							else {
								int vertexLast = se.roadTemplates [i].roadParts [z].vertex.Count - 1;
								se.roadTemplates [i].roadParts [z].vertex.Add (new Vertex (se.roadTemplates [i].roadParts [z].vertex [vertexLast].point,
									se.roadTemplates [i].roadParts [z].vertex [vertexLast].normal,
									se.roadTemplates [i].roadParts [z].vertex [vertexLast].uCoord));
							}
						}

						if (z > 0)
						if (GUILayout.Button ("▲", GUILayout.Width (30))) {
							RoadPart current = se.roadTemplates [i].roadParts [z];
							RoadPart up = se.roadTemplates [i].roadParts [z - 1];
							RoadPart currentNew = new RoadPart (up.type, up.pivot, up.vertex.ToArray (), up.matId);
							RoadPart upNew = new RoadPart (current.type, current.pivot, current.vertex.ToArray (), current.matId);
							se.roadTemplates [i].roadParts [z - 1] = upNew;

							se.roadTemplates [i].roadParts [z] = currentNew;
							se.GenerateMesh ();

						}

						if (z < se.roadTemplates [i].roadParts.Count - 1)
						if (GUILayout.Button ("▼", GUILayout.Width (30))) 
						{
							RoadPart current = se.roadTemplates [i].roadParts [z];
							RoadPart down = se.roadTemplates [i].roadParts [z + 1];
							RoadPart currentNew = new RoadPart (down.type, down.pivot, down.vertex.ToArray (), down.matId);
							RoadPart downNew = new RoadPart (current.type, current.pivot, current.vertex.ToArray (), current.matId);
							se.roadTemplates [i].roadParts [z + 1] = downNew;

							se.roadTemplates [i].roadParts [z] = currentNew;
							se.GenerateMesh ();
						}

						GUI.color = Color.red;
						if (GUILayout.Button ("X", GUILayout.Width (20))) {
							se.roadTemplates [i].roadParts.Remove (se.roadTemplates [i].roadParts [z]);
							se.GenerateMesh ();
						}
						GUI.color = Color.white;
						GUILayout.EndHorizontal ();
					}
					GUILayout.EndVertical ();
				}
				if (EditorGUI.EndChangeCheck ())
					se.GenerateMesh ();


			} 
				else 
			{
				float width = EditorGUIUtility.currentViewWidth;

				string[] options = new string[se.sourceMaterials.Count];
				for (int r = 0; r < se.sourceMaterials.Count; r++)
					options [r] = se.sourceMaterials [r].name;

				GUILayout.BeginHorizontal ();
				for (int z = 0; z < se.roadTemplates [i].roadParts.Count; z++) 
				{
					GUI.color = Color.gray;
					GUILayout.BeginVertical("box");
					GUI.color = Color.white;
					TextAnchor oldAlignment = GUI.skin.label.alignment;
					GUI.skin.label.alignment = TextAnchor.MiddleCenter;

					GUILayout.Label(se.roadTemplates [i].roadParts[z].type.ToString());
					if (se.roadTemplates[i].roadParts[z].type != RoadPartType.Road)
					GUILayout.Label(se.roadTemplates [i].roadParts [z].pivot.ToString());

					EditorGUI.BeginChangeCheck ();


					se.roadTemplates[i].roadParts[z].matId = EditorGUILayout.Popup(se.roadTemplates[i].roadParts[z].matId, options);
					if (EditorGUI.EndChangeCheck ()) 
					{
						se.ChangeMaterial (se.roadTemplates [i]);
						se.GenerateMesh ();
					}
					GUI.skin.label.alignment = oldAlignment;
					//EditorGUILayout.FloatField("", se.roadTemplates [i].roadParts[z].pivot);
					GUILayout.EndVertical();
				}
				GUILayout.EndHorizontal();
			}
			
			GUILayout.BeginHorizontal ();
			if (GUILayout.Button ("Add Part", GUILayout.Width (200))) {
				
				se.roadTemplates [i].roadParts.Add (new RoadPart (RoadPartType.Center, 0));
			}

			GUI.color = Color.red;
			if (GUILayout.Button ("X", GUILayout.Width (25))) 
			{
				se.roadTemplates.RemoveAt (i);// (se.roadTemplates [i].roadParts [z]);
				Transform trChild = se.transform.Find("RoadTemplateMesh" + i.ToString());
				if (trChild != null) 
					DestroyImmediate (trChild.gameObject);
				se.GenerateMesh ();
			}
			GUI.color = Color.white;
			GUILayout.EndHorizontal();

			if (Event.current.commandName == "ObjectSelectorClosed") {
				m_WaitingToLoad = EditorGUIUtility.GetObjectPickerObject () as RoadTemplateAsset;
			}
			EditorGUILayout.Space ();

			if (Event.current.type == EventType.Repaint && m_WaitingToLoad != null) 
			{
				se.LoadRoadTemplate (m_WaitingToLoad, loadtingSelection);
				se.GenerateMesh ();
				m_WaitingToLoad = null;
				loadtingSelection = -1;
			}

			GUILayout.EndVertical ();

		}

		if (GUILayout.Button ("Add Template", GUILayout.Width (200))) 
		{

			if (se.roadTemplates == null)
				se.roadTemplates = new System.Collections.Generic.List<RoadTemplate> ();
			
			RoadTemplate roadNew = new RoadTemplate (se.transform, se.roadTemplates.Count);
			se.roadTemplates.Add (roadNew);
		}

	}
}
