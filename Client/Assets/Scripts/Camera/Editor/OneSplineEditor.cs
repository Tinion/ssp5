﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System.IO;
using Dest.Math;

[CustomEditor(typeof(OneSpline))]
public class OneSplineEditor : Editor
{
	public const int QUAD_SIZE = 12;
	public Color CURVE_COLOR = new Color(0.8f, 0.8f, 0.8f);
	public Color CURVE_BUTTON_COLOR = new Color(0.8f, 0.8f, 0.8f);
	public  Color DIRECTION_COLOR = Color.red;
	public Color DIRECTION_BUTTON_COLOR = Color.red;
	public Color FORWARD_BUTTON_COLOR = Color.blue;
	public Color BACK_BUTTON_COLOR = Color.gray;

	public OneSpline  spline;
	public SerializedProperty nodes;


	public float percent;

	public bool mustCreateNewNode = false;
	//private SplineNode selection;
	//private SelectionType selectionType;

	public GUIStyle nodeButtonStyle, directionButtonStyle, forwardButtonStyle, backButtonStyle;

	public void OnEnable()
	{
		spline = (OneSpline)target;
		nodes = serializedObject.FindProperty("nodes");

		Texture2D t = new Texture2D(1, 1);
		t.SetPixel(0, 0, CURVE_BUTTON_COLOR);
		t.Apply();
		nodeButtonStyle = new GUIStyle();
		nodeButtonStyle.normal.background = t;

		t = new Texture2D(1, 1);
		t.SetPixel(0, 0, DIRECTION_BUTTON_COLOR);
		t.Apply();
		directionButtonStyle = new GUIStyle();
		directionButtonStyle.normal.background = t;

		t = new Texture2D(1, 1);
		t.SetPixel(0, 0, FORWARD_BUTTON_COLOR);
		t.Apply();
		forwardButtonStyle = new GUIStyle();
		forwardButtonStyle.normal.background = t;

		t = new Texture2D(1, 1);
		t.SetPixel(0, 0, BACK_BUTTON_COLOR);
		t.Apply();
		backButtonStyle = new GUIStyle();
		backButtonStyle.normal.background = t;
		spline.RecalculateOpenSegments ();

	}




    public override void OnInspectorGUI()
    {
		spline = (OneSpline)target;
		spline.type = (SplineType)EditorGUILayout.EnumPopup("Type", spline.type);

		GUILayout.BeginHorizontal ();
		//GUI.backgroundColor = Color.white;//new Color(0.9f,0.9f,0.9f);


		GUI.color = (spline.isNodesOpen) ? Color.gray : Color.white;
		if (GUILayout.Button("Node", GUILayout.Height(60), GUILayout.Width(60)))
		{
			spline.isNodesOpen = !spline.isNodesOpen;
		}

		GUI.color = (spline.isSegmentsVisible) ? Color.gray : Color.white;
		if (GUILayout.Button("Segments", GUILayout.Height(60), GUILayout.Width(60)))
		{
			spline.isSegmentsVisible = !spline.isSegmentsVisible;
		}

		GUI.color = (spline.isCameraNodes) ? Color.gray : Color.white;
		if (GUILayout.Button("Camera", GUILayout.Height(60), GUILayout.Width(60)))
		{
			spline.isCameraNodes = !spline.isCameraNodes;
		}

		GUI.color = (spline.isOther) ? Color.gray : Color.white;
		if (GUILayout.Button("Other", GUILayout.Height(60), GUILayout.Width(60)))
		{
			spline.isOther = !spline.isOther;
		}
		GUILayout.EndHorizontal ();
		GUI.color = Color.white;
		if (spline.isNodesOpen) {

			GUILayout.Space (3);
			EditorGUILayout.BeginHorizontal ();
			EditorGUILayout.LabelField ("Nodes", EditorStyles.boldLabel);
			spline.isDrawSpline = EditorGUILayout.Toggle (spline.isDrawSpline);
			EditorGUILayout.EndHorizontal ();
			GUI.color = new Color (0.8f, 0.8f, 0.8f, 1f);
			GUILayout.BeginVertical ("box");
			GUILayout.Space (3);
			GUI.color = Color.white;

			if (nodes.isExpanded) {
				for (int i = 0; i < spline.nodes.Count; i++) {

					GUILayout.Space (3);
					GUILayout.BeginHorizontal ();

					EditorGUILayout.LabelField (i.ToString(), EditorStyles.boldLabel, GUILayout.Width(15));
					GUILayout.BeginVertical ("box");
					EditorGUI.BeginChangeCheck ();
					spline.nodes [i].position = EditorGUILayout.Vector3Field ("Position", spline.nodes [i].position);
					spline.nodes [i].direction = EditorGUILayout.Vector3Field ("Direction", spline.nodes [i].direction);
					Vector3 distance = spline.nodes [i].direction - spline.nodes [i].position;
					float width = distance.Length();
					float newWidth = width;
					newWidth = EditorGUILayout.FloatField ("Width", width);
					if (newWidth != width)
						spline.nodes [i].direction = spline.nodes [i].position + Vector3.Normalize (distance) * newWidth;

					spline.nodes[i].inverseWidth = EditorGUILayout.FloatField ("Inverse Width", spline.nodes[i].inverseWidth);
                    spline.nodes[i].tilt = EditorGUILayout.Slider("Tilt", spline.nodes[i].tilt, -90f, 90f);
					//spline.nodes [i].width = EditorGUILayout.FloatField ("Width", spline.nodes [i].width);
					//spline.nodes [i].verge = EditorGUILayout.FloatField ("Verge", spline.nodes [i].verge);
					if (EditorGUI.EndChangeCheck ())
						spline.EditorUpdate();
					GUILayout.EndVertical ();

					GUILayout.BeginVertical (GUILayout.Width (30));
					GUI.color = Color.red;
					if (GUILayout.Button ("X")) 
					{
						spline.nodes.Remove (spline.nodes [i]);
						spline.EditorUpdate();
					}
					GUI.color = Color.white;

					if (i > 0)
					if (GUILayout.Button ("▲")) 
					{
						OneSplineNode current = spline.nodes [i];
						OneSplineNode up = spline.nodes [i - 1];
						OneSplineNode currentNew = new OneSplineNode (up.position, up.direction);
						OneSplineNode upNew = new OneSplineNode (current.position, current.direction);
						spline.nodes [i - 1] = upNew;
						spline.nodes [i] = currentNew;
						spline.EditorUpdate();
					}

					if (i < spline.nodes.Count-1)
					if (GUILayout.Button ("▼")) 
					{
						OneSplineNode current = spline.nodes [i];
						OneSplineNode down = spline.nodes [i + 1];
						OneSplineNode currentNew = new OneSplineNode (down.position, down.direction);
						OneSplineNode downNew = new OneSplineNode (current.position, current.direction);
						spline.nodes [i - 1] = downNew;
						spline.nodes [i] = currentNew;
						spline.EditorUpdate();
					}
					GUILayout.EndVertical ();
					GUILayout.EndHorizontal ();
					//EditorGUILayout.PropertyField (nodes.GetArrayElementAtIndex (i), new GUIContent ("Node " + i), true);
				}
			}
			//EditorGUI.indentLevel -= 1;
			//serializedObject.ApplyModifiedProperties();
			GUILayout.Space (5);
			GUILayout.BeginVertical ("box");

			if (spline.selectionType == OneSplineNode.SelectionType.Node) {
				GUI.color = Color.red;
				Vector3 position = spline.transform.TransformPoint (spline.selection.position);
				spline.selection.position = EditorGUILayout.Vector3Field ("Position Node", spline.transform.InverseTransformPoint(position));
				GUI.color = Color.white;
				GUILayout.Space (3);
			}

		
			if (spline.selectionType == OneSplineNode.SelectionType.Direction) {
				GUI.color = Color.red;
				Vector3 direction = spline.transform.InverseTransformPoint (spline.transform.TransformPoint (spline.selection.direction));
				spline.selection.direction = EditorGUILayout.Vector3Field ("Direction Node", direction);
				GUI.color = Color.white;
				GUILayout.Space (3);
			}

			if (spline.selectionType == OneSplineNode.SelectionType.InverseDirection) {
				GUI.color = Color.red;
				Vector3 direction = spline.transform.InverseTransformPoint (2 * spline.transform.TransformPoint (spline.selection.position) - spline.transform.TransformPoint (spline.selection.direction));
				spline.selection.direction = 2 * spline.selection.position - EditorGUILayout.Vector3Field ("Inverse Direction Node", direction);
				GUI.color = Color.white;
				GUILayout.Space (3);
			}

			GUI.color = Color.white;
			GUILayout.Space (3);

			GUILayout.BeginHorizontal ();
			GUI.color = (spline.editType != OneSpline.EditType.Insert) ? Color.white : Color.yellow;
			if (GUILayout.Button ("Insert Node by Click", GUILayout.Width (180))) 
			{
				if (spline.editType != OneSpline.EditType.Insert)
					spline.editType = OneSpline.EditType.Insert;	
				else
					spline.editType = OneSpline.EditType.Default;
			}
			GUI.color = Color.white;

			GUI.color = (spline.editType != OneSpline.EditType.Slider) ? Color.white : Color.yellow;
			if (GUILayout.Button ("Insert Node by Slider", GUILayout.Width (180))) 
			{
				if (spline.editType != OneSpline.EditType.Slider)
					spline.editType = OneSpline.EditType.Slider;	
				else
					spline.editType = OneSpline.EditType.Default;
			}
			GUI.color = Color.white;

			if (GUILayout.Button ("Add Node", GUILayout.ExpandWidth(true))) 
			{
				OneSplineNode final = spline.nodes [spline.nodes.Count - 1];
				spline.AddNode (new OneSplineNode (final.position + Vector3.Normalize (final.direction) * 20f, final.direction));

			}
			GUILayout.EndHorizontal ();

			if (spline.editType == OneSpline.EditType.Slider) 
			{
				
				GUILayout.BeginHorizontal ();
				spline.sliderT = EditorGUILayout.Slider ("Slider " + spline.sliderT.ToString(), spline.sliderT, 0, 1f);
				if (GUILayout.Button ("Add", GUILayout.Width (100))) 
				{
					spline.InsertNode (spline.sliderT);
					spline.editType = OneSpline.EditType.Default;
				}

				GUILayout.EndHorizontal ();
			}

            spline.tiltCurve = EditorGUILayout.CurveField("Tilt Curve", spline.tiltCurve);

			GUILayout.EndVertical ();
			GUILayout.EndVertical ();
		}
		if (spline.isSegmentsVisible) {
			/*

*/
			GUILayout.Space (3);
			EditorGUILayout.LabelField ("Segments", EditorStyles.boldLabel);

			GUI.color = new Color (0.8f, 0.8f, 0.8f, 1f);
			GUILayout.BeginVertical ("box");
			GUILayout.Space (3);
			GUI.color = Color.white;

			spline.segmentsStrenght = EditorGUILayout.Slider ("Segments Strenght", spline.segmentsStrenght, 0, 1);
			spline.editView = (OneSpline.EditView)EditorGUILayout.EnumPopup ("Edit View", spline.editView);
			int ch = 0;

			float fullSegmentLenght = 0;
			float width2 = EditorGUIUtility.currentViewWidth - 70;

			foreach (OneSplineSegment segment in spline.segments) 
				fullSegmentLenght += segment.lenght;
			

			for (int i=0; i<spline.segments.Count; i++)
			{
				GUILayout.BeginHorizontal ();
				for (int j = 0; j<spline.segments.Count; j++)
				{
					if (j == ch) {
						GUIStyle style = GUI.skin.FindStyle ("Button");
						/*
						if (j > 0) {
							if (spline.segments [j].startTime > spline.segments [j - 1].endTime) {
								GUI.color = new Color (1, 1, 1, 0.5f);
								style = GUI.skin.FindStyle ("ButtonMid");
								GUILayout.Box ("", style, GUILayout.Width ((spline.GetLenght (spline.segments [j - 1].endTime, spline.segments [j].startTime) * coeffError) / spline.Length * width2));
								GUI.color = new Color (1, 1, 1, 1f);
							}
							*/

							if (spline.segments [j].startTime > 0) {
								GUI.color = new Color (1, 1, 1, 0.5f);
								style = GUI.skin.FindStyle ("ButtonLeft");
								GUILayout.Box ("", style, GUILayout.Width ((spline.GetLenght (0, spline.segments [j].startTime)) / spline.Length * width2));
								GUI.color = new Color (1, 1, 1, 1f);
							}


						GUI.color = spline.segments [j].color;
						style = (spline.segments [j].startTime == 0) ? GUI.skin.FindStyle ("ButtonLeft") : GUI.skin.FindStyle ("ButtonMid");
						if (spline.segments [j].endTime == 1)
							style = GUI.skin.FindStyle ("ButtonRight");
						else if (spline.segments [j].startTime == 0 && spline.segments [j].endTime == 1f)
							style = GUI.skin.FindStyle ("Button");

						if (GUILayout.Button (((spline.isOpenSegments [j]) ? "▲" : "▼").ToString () + " Segment " + j, style, GUILayout.Width ((spline.segments [j].lenght) / spline.Length * width2)))
							spline.isOpenSegments [j] = !spline.isOpenSegments [j];


							if (spline.segments [j].endTime < 1) {
								GUI.color = new Color (1, 1, 1, 0.5f);
								style = GUI.skin.FindStyle ("ButtonRight");
								GUILayout.Box ("", style, GUILayout.Width ((spline.GetLenght (spline.segments [j].endTime, 1f)) / spline.Length * width2));
								GUI.color = new Color (1, 1, 1, 1f);
							}

						GUI.color = Color.red;
						if (GUILayout.Button ("X", GUILayout.Width (20))) {
							spline.segments.Remove (spline.segments[i]);
							spline.RecalculateOpenSegments ();
						}
						GUI.color = Color.white;

					}
				}
				GUILayout.EndHorizontal ();
				GUI.color = Color.white;

				if (spline.isOpenSegments [i]) 
				{

					//GUILayout.Label ("Segment " + i, EditorStyles.boldLabel);




					GUILayout.BeginHorizontal ();
					GUILayout.BeginVertical ("box");

					EditorGUI.BeginChangeCheck ();
					spline.segments[i].startTime = EditorGUILayout.Slider ("Start Time", spline.segments[i].startTime, 0, 1);
					
					if (EditorGUI.EndChangeCheck ()) 
					{
						if (spline.segments[i].startTime > spline.segments[i].endTime)
							spline.segments[i].startTime = spline.segments[i].endTime;
						/*
						if (i > 0) 
						{
							if (spline.segments [i].startTime < spline.segments [i - 1].endTime) 
								spline.segments [i - 1].endTime = spline.segments [i].startTime;
							
							if (spline.segments [i].startTime < spline.segments [i - 1].startTime) 
								spline.segments [i].startTime = spline.segments [i - 1].startTime;
						}
						*/
						spline.EditorUpdate ();
					}
					
					EditorGUI.BeginChangeCheck ();
					spline.segments[i].endTime = EditorGUILayout.Slider ("End Time", spline.segments[i].endTime, 0, 1);
					
					if (EditorGUI.EndChangeCheck ()) 
					{
						if (spline.segments[i].endTime < spline.segments[i].startTime)
							spline.segments[i].endTime = spline.segments[i].startTime;
						/*
						if (i < spline.segments.Count - 1) 
						{
								if (spline.segments [i].endTime > spline.segments [i + 1].startTime) 
									spline.segments [i + 1].startTime = spline.segments [i].endTime;
								
								if (spline.segments [i].endTime > spline.segments [i + 1].endTime) 
								spline.segments [i].endTime = spline.segments [i + 1].endTime;
							}
							*/
							spline.EditorUpdate ();
					}
					
					EditorGUI.BeginChangeCheck ();
					spline.segments[i].startWidth = EditorGUILayout.Vector2Field ("Start Width", spline.segments[i].startWidth);
					spline.segments[i].endWidth = EditorGUILayout.Vector2Field ("End Width", spline.segments[i].endWidth);
					spline.segments[i].curve = EditorGUILayout.CurveField ("Curve", spline.segments[i].curve);

					spline.segments[i].color = EditorGUILayout.ColorField ("Color", spline.segments[i].color);

					GUILayout.EndVertical ();



					GUILayout.EndHorizontal ();


					if (spline.editView == OneSpline.EditView.Ways) {
						GUILayout.Label ("Ways Setting", EditorStyles.boldLabel);
						GUILayout.Space (3);
						GUILayout.BeginVertical ("box");
						spline.segments[i].offsetCenter = EditorGUILayout.Slider ("Center", spline.segments[i].offsetCenter, 0, 5f);
						spline.segments[i].offsetBetween = EditorGUILayout.Slider ("Between", spline.segments[i].offsetBetween, 0, 5f);
						spline.segments[i].offsetSide = EditorGUILayout.Slider ("Side", spline.segments[i].offsetSide, 0, 5f);
						spline.segments[i].leftWays = EditorGUILayout.IntSlider ("Left Ways", spline.segments[i].leftWays, 0, 5);
						spline.segments[i].rightWays = EditorGUILayout.IntSlider ("Right Ways", spline.segments[i].rightWays, 0, 5);
						GUILayout.EndVertical ();
					}

					if (EditorGUI.EndChangeCheck ()) {
						spline.EditorUpdate ();
					}
				} 
				ch++;
			}
			GUILayout.BeginHorizontal ();
			if (GUILayout.Button ("Add new Segment", GUILayout.Width (200))) 
			{
				spline.segments.Add (new OneSplineSegment ());
				spline.CalculateSegments ();
				spline.RecalculateOpenSegments ();
			}

			if (GUILayout.Button ("Add Extrusion", GUILayout.Width (200))) 
			{
				
				spline.AddExtrusion ();

			}
			GUILayout.EndHorizontal ();
			GUILayout.EndVertical ();
		}

		if (spline.isCameraNodes) 
		{

			GUILayout.Space (2);
			GUILayout.Label ("Camera Local Settings", EditorStyles.boldLabel);
			GUILayout.Space (1);

			GUILayout.BeginVertical ("Box");

			if (spline.cameraDots != null)
				for (int i = 0; i < spline.cameraDots.Count; i++) {
					GUILayout.Label ("CameraDot " + i.ToString ());
					spline.cameraDots [i].percent = EditorGUILayout.Slider ("percent", spline.cameraDots [i].percent, 0, 1.0f);
					spline.cameraDots [i].AxisBendSize = EditorGUILayout.Vector2Field ("AxisBendSize", spline.cameraDots [i].AxisBendSize);
					spline.cameraDots [i].Bias = EditorGUILayout.Vector2Field ("Bias", spline.cameraDots [i].Bias);
					//spline.cameraDots [i].supportPercentPrevious = EditorGUILayout.Slider("SupportPercentPrevious", spline.cameraDots [i].supportPercentPrevious, 0, 0.5f);
					spline.cameraDots [i].localPosition = EditorGUILayout.Vector3Field ("LocalPosition", spline.cameraDots [i].localPosition);
					spline.cameraDots [i].localRotate = EditorGUILayout.Vector3Field ("LocalRotation", spline.cameraDots [i].localRotate);
					EditorGUILayout.Separator ();
				}

			if (GUILayout.Button ("Add new dot", GUILayout.Height (20))) {
				if (spline.cameraDots == null)
					spline.cameraDots = new System.Collections.Generic.List<CameraDot> ();

				spline.cameraDots.Add (new CameraDot ());
			}

			GUILayout.EndVertical ();
		}

		if (spline.isOther) 
		{
			spline.previous = (OneSpline)EditorGUILayout.ObjectField ("Previous Spline", spline.previous, typeof(OneSpline));
			if (spline.previous != null)
			if (GUILayout.Button("Connect"))
				{
				spline.transform.position = spline.previous.transform.TransformPoint(spline.previous.nodes[spline.previous.nodes.Count-1].position);
				spline.nodes[0].position = Vector3.zero;
				spline.nodes[0].direction = spline.previous.nodes[spline.previous.nodes.Count-1].direction - spline.previous.nodes[spline.previous.nodes.Count-1].position;
				spline.nodes[0].inverseWidth = spline.previous.nodes[spline.previous.nodes.Count-1].inverseWidth;
				spline.nodes[0].tilt = spline.previous.nodes[spline.previous.nodes.Count-1].tilt;
				}

			spline.next = (OneSpline)EditorGUILayout.ObjectField ("Next Spline", spline.next, typeof(OneSpline));
			if (spline.next != null)
			if (GUILayout.Button("Connect"))
			{
				spline.next.transform.position = spline.transform.TransformPoint(spline.nodes[spline.previous.nodes.Count-1].position);
				spline.next.nodes[0].position = Vector3.zero;
				spline.next.nodes[0].direction = spline.nodes[spline.nodes.Count-1].direction - spline.nodes[spline.nodes.Count-1].position;
				spline.next.nodes[0].inverseWidth = spline.nodes[spline.nodes.Count-1].inverseWidth;
				spline.next.nodes[0].tilt = spline.nodes[spline.nodes.Count-1].tilt;
			}
		}
        /*
        if (GUILayout.Button("Dotes"))
        {
            spline.GetDotes();
        }
        percent = EditorGUILayout.FloatField("percent", percent);
        if (GUILayout.Button("Check"))
        {
            spline.CheckPercent(percent);
        }
        if (GUILayout.Button("CheckCorrect"))
        {
            spline.CheckCorrect(percent);
        }
        if (GUILayout.Button("5Spline"))
        {
            spline.Create5Bezier();
        }
        */
        
    }



	public virtual void OnSceneGUI()
	{
		OneSpline spline = (OneSpline)target;
		if (Selection.activeGameObject == spline.gameObject) 
		{
			Tools.current = Tool.None;
		}

		if (spline.editType == OneSpline.EditType.Slider) 
		{
			Vector3 globalP = spline.GlobalPos2(spline.sliderT, 0);
			Handles.color = Color.yellow;
			Handles.DrawLine (globalP, globalP + Vector3.up*5);
		}

		foreach (OneCubicBezierCurve curve in spline.curves)
		{
			if (spline.isDrawSpline) {
				Handles.DrawBezier (spline.transform.TransformPoint (curve.n1.position),
					spline.transform.TransformPoint (curve.n2.position),
					spline.transform.TransformPoint (curve.n1.direction),
					spline.transform.TransformPoint (curve.GetInverseDirection ()),
					Color.white,
					null,
					3);


			}
		}


		foreach (OneSplineSegment segment in spline.segments) 
		{
			switch (spline.editView) 
			{
			case OneSpline.EditView.Segments:
				DisplaySegments (segment);
				break;
			case OneSpline.EditView.Ways:
				DisplayWayes (segment);
				break;
			}






			//Handles.DrawAAPolyLine(vertex);

		}

		Event current = Event.current;
		if (current.type == EventType.MouseDown) 
		{
			if (spline.editType == OneSpline.EditType.Insert) 
			{
				
				RaycastHit hit;
				Ray ray = HandleUtility.GUIPointToWorldRay (Event.current.mousePosition);
				bool result = Physics.Raycast (ray, out hit);
					if (result)
					spline.InsertNode (hit.point);

			}
		}

		switch (spline.selectionType) {
		case OneSplineNode.SelectionType.Node:
			// place a handle on the node and manage position change
			Vector3 newPosition = spline.transform.InverseTransformPoint (Handles.PositionHandle (spline.transform.TransformPoint (spline.selection.position), Quaternion.identity));

			if (newPosition != spline.selection.position) {

				spline.selection.direction = spline.selection.direction + newPosition - spline.selection.position;
				spline.selection.position = newPosition;


			}
			break;
		case OneSplineNode.SelectionType.Direction:
			spline.selection.direction = spline.transform.InverseTransformPoint (Handles.PositionHandle (spline.transform.TransformPoint (spline.selection.direction), Quaternion.identity));

			break;
		case OneSplineNode.SelectionType.InverseDirection:
			Vector3 invDir = Vector3.Normalize (spline.transform.TransformPoint (spline.selection.position) - spline.transform.TransformPoint (spline.selection.direction)) * spline.selection.inverseWidth + spline.transform.TransformPoint (spline.selection.position);
			spline.selection.inverseWidth = Handles.ScaleValueHandle (spline.selection.inverseWidth, spline.transform.InverseTransformPoint (invDir), Quaternion.LookRotation (spline.selection.position - spline.selection.direction, Vector3.up), 100.0f, Handles.ArrowHandleCap, 0.1f);


			break;

		}


		Handles.BeginGUI();
		foreach (OneSplineNode n in spline.nodes)
		{
			Vector3 guiPos = HandleUtility.WorldToGUIPoint (spline.transform.TransformPoint (n.position));
			if (n == spline.selection) {
				Vector3 guiDir = HandleUtility.WorldToGUIPoint(spline.transform.TransformPoint(n.direction));
				Vector3 invDir = Vector3.Normalize (n.position - n.direction) * n.inverseWidth + n.position;
				Vector3 guiInvDir = HandleUtility.WorldToGUIPoint(spline.transform.TransformPoint(invDir));

				// for the selected node, we also draw a line and place two buttons for directions
				Handles.color = Color.red;
				Handles.DrawLine(guiDir, guiInvDir);

				// draw quads direction and inverse direction if they are not selected
				if (spline.selectionType != OneSplineNode.SelectionType.Node) {
					if (Button(guiPos, directionButtonStyle)) {
						spline.selectionType = OneSplineNode.SelectionType.Node;
					}
				}
				if (spline.selectionType != OneSplineNode.SelectionType.Direction) {
					if (Button(guiDir, forwardButtonStyle)) {
					//	Debug.Log ("Pipidor");
						spline.selectionType = OneSplineNode.SelectionType.Direction;
					}
				}
				if (spline.selectionType != OneSplineNode.SelectionType.InverseDirection) {
					if (Button(guiInvDir, backButtonStyle)) {
					//	Debug.Log ("Pipidor2");
						spline.selectionType = OneSplineNode.SelectionType.InverseDirection;
					}
				}
			} else 
				if (Button (guiPos, nodeButtonStyle)) 
				{
					spline.selection = n;
					spline.selectionType = OneSplineNode.SelectionType.Node;
				}

		}

		Handles.EndGUI();

		if (GUI.changed)
		{
			EditorUtility.SetDirty (target);
			spline.EditorUpdate();
		}
	}

	public bool Button(Vector2 position, GUIStyle style) {
		return GUI.Button(new Rect(position - new Vector2(QUAD_SIZE / 2, QUAD_SIZE / 2), new Vector2(QUAD_SIZE, QUAD_SIZE)), GUIContent.none, style);
	}

	public void DisplaySegments(OneSplineSegment segment)
	{
		if (spline.segmentsStrenght == 0)
			return;
		

			Handles.color = new Color (segment.color.r, segment.color.g, segment.color.b, spline.segmentsStrenght);
			Vector3 posNext = spline.GlobalPos (segment.startTime);
			Vector3 tangentNext = spline.transform.TransformDirection(spline.GetTangent (segment.startTime));
			
			float step = spline._step_n;
			Quaternion tangentOneNext = OneCubicBezierCurve.GetRotationFromTangentGlobal (tangentNext, spline.transform.up);//Quaternion.FromToRotation (Vector3.up, tangentNext);
			for (float i = segment.startTime; i < segment.endTime; i += step) 
			{
				
				Vector3 pos = posNext;
				Vector3 tangent = tangentNext;
				float startPercent = 0;
				float endPercent = 0;

				Quaternion tangentOne = tangentOneNext;
				if (i + step < segment.endTime) 
				{
					posNext = spline.GlobalPos (i + step);
					tangentNext = spline.transform.TransformDirection(spline.GetTangent (i + step));
					startPercent = (i - segment.startTime) / (segment.endTime - segment.startTime);
					endPercent = (i + step - segment.startTime) / (segment.endTime - segment.startTime);
					tangentOneNext = OneCubicBezierCurve.GetRotationFromTangentGlobal (tangentNext, spline.transform.up);
					
				} 
				else 
				{
					posNext = spline.GlobalPos (segment.endTime);
					tangentNext = spline.transform.TransformDirection(spline.GetTangent (segment.endTime));
					startPercent = (i - segment.startTime) / (segment.endTime - segment.startTime);
					endPercent = 1;
					
					tangentOneNext = OneCubicBezierCurve.GetRotationFromTangentGlobal (tangentNext, spline.transform.up);
				}

				Vector3[] vertex = new Vector3[4];
								
				Vector2 startLerpedWidth = Vector2.Lerp (segment.startWidth, segment.endWidth, segment.curve.Evaluate(startPercent));
				Vector2 endLerpedWidth = Vector2.Lerp (segment.startWidth, segment.endWidth, segment.curve.Evaluate(endPercent));

			vertex [0] = pos + tangentOne * new Vector3 (-startLerpedWidth.x, 0, 0);
			vertex [1] = pos + tangentOne * new Vector3 (startLerpedWidth.y, 0, 0);
			vertex [2] = posNext + tangentOneNext * new Vector3 (endLerpedWidth.y, 0, 0);
			vertex [3] = posNext + tangentOneNext * new Vector3 (-endLerpedWidth.x, 0, 0);

		

				Handles.DrawAAConvexPolygon (vertex);
			}

	}

	public void DisplayWayes(OneSplineSegment segment)
	{
		if (spline.segmentsStrenght == 0)
			return;


		Handles.color = new Color (segment.color.r, segment.color.g, segment.color.b, spline.segmentsStrenght);


			Vector3 posNext = spline.GlobalPos (segment.startTime);
			Vector3 tangentNext = spline.GetTangent (segment.startTime);
			float step = segment.lenght / spline.quality;

			for (float i = segment.startTime; i < segment.endTime; i += step) 
			{
				Vector3 pos = posNext;
				Vector3 tangent = tangentNext;
				float startPercent = 0;
				float endPercent = 0;
				if (i + step < segment.endTime) {
					posNext = spline.GlobalPos (i + step);
					tangentNext = spline.GetTangent (i + step);
					startPercent = (i - segment.startTime) / (segment.endTime - segment.startTime);
					endPercent = (i + step - segment.startTime) / (segment.endTime - segment.startTime);

				} else {
					posNext = spline.GlobalPos (segment.endTime);
					tangentNext = spline.GetTangent (segment.endTime);
					startPercent = (i - segment.startTime) / (segment.endTime - segment.startTime);
					endPercent = 1;
				}

				Vector3[] vertex = new Vector3[4];
				float tangentAngle = Vector3ex.SignedAngleDeg (Vector3.forward, tangent, Vector3.up);
				float tangentAngleNext = Vector3ex.SignedAngleDeg (Vector3.forward, tangentNext, Vector3.up);


				Vector2 startLerpedWidth = Vector2.Lerp (segment.startWidth, segment.endWidth, segment.curve.Evaluate(startPercent));
				Vector2 endLerpedWidth = Vector2.Lerp (segment.startWidth, segment.endWidth, segment.curve.Evaluate(endPercent));

			Handles.color = new Color(0.2f, 0.2f, 0.9f, spline.segmentsStrenght);
			float leftWidthStart = (startLerpedWidth.x - segment.offsetCenter/2 - segment.offsetBetween * (segment.leftWays-1) - segment.offsetSide) / segment.leftWays;
			float leftWidthEnd = (endLerpedWidth.x - segment.offsetCenter/2 - segment.offsetBetween * (segment.leftWays-1) - segment.offsetSide) / segment.leftWays;
			for (int w = segment.leftWays; w > 0; w--) 
			{
				vertex [0] = spline.transform.TransformPoint (pos) + Math3d.RotateThis (-startLerpedWidth.x + segment.offsetSide + segment.offsetBetween * (w-1)  + leftWidthStart * (w-1), -tangentAngle);
				vertex [1] = spline.transform.TransformPoint (pos) + Math3d.RotateThis (-startLerpedWidth.x +  segment.offsetSide + segment.offsetBetween * (w-1) + leftWidthStart * w, -tangentAngle);
				vertex [2] = spline.transform.TransformPoint (posNext) + Math3d.RotateThis (-endLerpedWidth.x + segment.offsetSide + segment.offsetBetween * (w-1) + leftWidthEnd * w, -tangentAngleNext);
				vertex [3] = spline.transform.TransformPoint (posNext) + Math3d.RotateThis (-endLerpedWidth.x +  segment.offsetSide + segment.offsetBetween * (w-1) + leftWidthEnd * (w-1), -tangentAngleNext);
				Handles.DrawAAConvexPolygon (vertex);
			}
			Handles.color = new Color(0.9f, 0.2f, 0.2f, spline.segmentsStrenght);
			float rightWidthStart = (startLerpedWidth.y - segment.offsetCenter/2 - segment.offsetBetween * (segment.rightWays-1) - segment.offsetSide) / segment.rightWays;
			float rightWidthEnd = (endLerpedWidth.y - segment.offsetCenter/2 - segment.offsetBetween * (segment.rightWays-1) - segment.offsetSide) / segment.rightWays;
			for (int w = segment.rightWays; w > 0; w--) 
			{
				vertex [0] = spline.transform.TransformPoint (pos) + Math3d.RotateThis (startLerpedWidth.y - segment.offsetSide - segment.offsetBetween * (w-1)  - rightWidthStart * (w-1), -tangentAngle);
				vertex [1] = spline.transform.TransformPoint (pos) + Math3d.RotateThis (startLerpedWidth.y -  segment.offsetSide - segment.offsetBetween * (w-1) - rightWidthStart * w, -tangentAngle);
				vertex [2] = spline.transform.TransformPoint (posNext) + Math3d.RotateThis (endLerpedWidth.y - segment.offsetSide - segment.offsetBetween * (w-1) - rightWidthEnd * w, -tangentAngleNext);
				vertex [3] = spline.transform.TransformPoint (posNext) + Math3d.RotateThis (endLerpedWidth.y -  segment.offsetSide - segment.offsetBetween * (w-1) - rightWidthEnd * (w-1), -tangentAngleNext);
				Handles.DrawAAConvexPolygon (vertex);
			}
		

		}
	}

}
