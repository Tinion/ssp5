﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System.IO;
using Dest.Math;



[CustomEditor(typeof(TerrainPreparer))]
public class TerrainPreparerEditor : Editor
{


	public TerrainPreparer  preparer;
	




    public override void OnInspectorGUI()
	{
		preparer = (TerrainPreparer)target;
			
		if (GUILayout.Button ("Load Splines")) {
			//tester.Draw ();
			preparer.LoadSplines();
		}

        if (GUILayout.Button("Slice"))
        {
            preparer.Slice();
        }

        if (GUILayout.Button("SelectBorder"))
        {
            preparer.SelectBorder();
        }

        if (GUILayout.Button("SelectBorder02"))
        {
            preparer.SelectBorder02();
        }
        if (GUILayout.Button("SelectBorder03"))
        {
            preparer.SelectBorder03();
        }
        preparer.width = EditorGUILayout.FloatField("Width", preparer.width);
        preparer.lenght = EditorGUILayout.FloatField("Lenght", preparer.lenght);
    }



}