﻿using UnityEngine;
using System.Collections;
using Dest.Math;

public class OneTarget : MonoBehaviour {

    public Animation currentAnimation;
    public Object effect;
    public Vector2 posOnControl;
    public CounterTimerF timer;
    public Transform transform;
    public TargetController targetController;
    public float radius;
    public Vector2 distance;
    public float Damage;
    public float upperForce;
    public float timeOffset;
    public bool isDistanceDemension;
    public float maxDistance;

    public virtual void Create(float lifeTime, Transform master)
    {
        timer.max = lifeTime + Random.Range(-lifeTime / timeOffset, lifeTime / timeOffset);
        if (isDistanceDemension)
        {
            float dist = Vector2.Distance(master.position.ToVector2XZ(), transform.position.ToVector2XZ());
            float factor = dist / maxDistance;
            timer.max *= factor;
        }
            timer.current = timer.max;
            currentAnimation.Play("BombTargetAnim");
    }

    public virtual void Proceed()
    {
        if (timer.isEnd(Time.deltaTime))
        {
            Collider[] victims = Physics.OverlapSphere(transform.position, radius);
            for (int i = 0; i < victims.Length; i++)
            {
                if (victims[i].gameObject.CompareTag("Unit"))
                {
                    if (victims[i].attachedRigidbody != null)
                        victims[i].attachedRigidbody.AddExplosionForce(Damage, transform.position, radius, upperForce, ForceMode.Impulse);
                    Unit unit = victims[i].transform.parent.GetComponent<Unit>();
                    if (unit != null)
                    {
                        unit.GetDamageUp(transform.position, Damage / 20 * (unit.transform.position - transform.position));
                        Debug.Log(unit.name + " " + Damage / 20 * (unit.transform.position - transform.position));
                    }

                }
            }

            //targetController.targets.Remove(this);
            GameObject explosion = GameObject.Instantiate(effect, transform.position, Quaternion.identity) as GameObject;

            //explosion.transform.parent = targetController.transform;
            Destroy(gameObject);
        }
    }
    void Update()
    {
        Proceed();
    }
}
