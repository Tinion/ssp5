﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EzySlice;
using Dest.Math;

public class TerrainPreparer : MonoBehaviour {

    public OneSpline[] oneSplines;
    public List<GameObject> planeSlices;
    public Vector3 dir;
    public Vector3 pos;
    public Vector3 posBorder;
    public Vector3 dirBorderForward;
    public Vector3 dirBorder;
    public float width;
    public float lenght;
    public List<int> selectedPoints;
    public List<int> selectedPoints02;
    public GameObject secondTerrain;
    public Box2 box;

    public Transform point;
    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    public void LoadSplines()
    {
        int count = transform.childCount;
        for (int i = 0; i < count; i++)
            DestroyImmediate(transform.GetChild(i).gameObject);
        planeSlices = new List<GameObject>();
        oneSplines = FindObjectsOfType<OneSpline>();
        for (int i = 0; i < oneSplines.Length; i++)
        {

            OneSplineNode node = oneSplines[i].nodes[oneSplines[i].nodes.Count - 1];
            //
            pos = node.position;
            dir = Vector3.Normalize(node.direction - node.position);
            dir = Quaternion.Euler(0, -90f, 0) * dir;

            GameObject planeSlice = GameObject.CreatePrimitive(PrimitiveType.Plane);
            planeSlice.transform.parent = transform;
            planeSlice.transform.position = node.position;
            planeSlice.transform.LookAt(node.position + dir);
            planeSlice.transform.Rotate(new Vector3(0, 0, 90f));
            //planeSlice.transform.rotation.SetLookRotation(dir);
            planeSlice.transform.localScale = Vector3.one * 50.0f;
            planeSlices.Add(planeSlice);

        }
    }



    public void Slice()
    {
        for (int i = 0; i < planeSlices.Count; i++)
        {
            SlicedHull hull = this.gameObject.Slice(planeSlices[i].transform.position, planeSlices[i].transform.forward);
        }

    }

    public void SelectBorder()
    {
        oneSplines = FindObjectsOfType<OneSpline>();
        for (int i = 0; i < oneSplines.Length; i++)
        {
            OneSplineNode node = oneSplines[i].nodes[oneSplines[i].nodes.Count - 1];
            posBorder = node.position;
            dirBorderForward = Vector3.Normalize(node.direction - node.position);
            dirBorder = Quaternion.Euler(0, -90f, 0) * dirBorderForward;
        }
    }

    public void SelectBorder02()
    {
        Mesh mesh = GetComponent<MeshFilter>().sharedMesh;
        List<Vector2> points = new List<Vector2>();
        points.Add(posBorder.ToVector2XZ() - dirBorder.ToVector2XZ() * width + dirBorderForward.ToVector2XZ());
        points.Add(posBorder.ToVector2XZ() + dirBorder.ToVector2XZ() * width + dirBorderForward.ToVector2XZ());
        points.Add(posBorder.ToVector2XZ() + dirBorder.ToVector2XZ() * width - dirBorderForward.ToVector2XZ() * lenght);
        points.Add(posBorder.ToVector2XZ() - dirBorder.ToVector2XZ() * width - dirBorderForward.ToVector2XZ() * lenght);
        //box = new Box2(posBorder.ToVector2XZ() - dirBorderForward.ToVector2XZ() * lenght / 2, dirBorder.ToVector2XZ(), dirBorderForward.ToVector2XZ(), new Vector2(width * 2, lenght));
        box = Box2.CreateFromPoints(points);

        if (box.Contains(point.position.ToVector2XZ()))
            Debug.Log("yes");
        else
            Debug.Log("no");

        selectedPoints = new List<int>();

        for (int i = 0; i < mesh.vertexCount; i++)
        {


            Vector3 vert = transform.TransformPoint(mesh.vertices[i]);
            if (box.Contains(vert.ToVector2XZ()))
            {
                selectedPoints.Add(i);

            }
        }
    }

    public void SelectBorder03()
    {
        Mesh mesh = secondTerrain.GetComponent<MeshFilter>().sharedMesh;
        List<Vector2> points = new List<Vector2>();
        points.Add(posBorder.ToVector2XZ() - dirBorder.ToVector2XZ() * width + dirBorderForward.ToVector2XZ());
        points.Add(posBorder.ToVector2XZ() + dirBorder.ToVector2XZ() * width + dirBorderForward.ToVector2XZ());
        points.Add(posBorder.ToVector2XZ() + dirBorder.ToVector2XZ() * width - dirBorderForward.ToVector2XZ() * lenght);
        points.Add(posBorder.ToVector2XZ() - dirBorder.ToVector2XZ() * width - dirBorderForward.ToVector2XZ() * lenght);
        
        //box = new Box2(posBorder.ToVector2XZ() - dirBorderForward.ToVector2XZ() * lenght / 2, dirBorder.ToVector2XZ(), dirBorderForward.ToVector2XZ(), new Vector2(width * 2, lenght));
        box = Box2.CreateFromPoints(points);

        selectedPoints02 = new List<int>();

        for (int i = 0; i < mesh.vertexCount; i++)
        {
            Vector3 vert = secondTerrain.transform.TransformPoint(mesh.vertices[i]);
            if (box.Contains(vert.ToVector2XZ()))
            {
                selectedPoints02.Add(i);

            }
        }

    }

    public void SelectBorder04()
    {

    }

    public void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(pos, pos + dir * 5f);

        Gizmos.color = Color.blue;
        Gizmos.DrawLine(posBorder - dirBorder * width, posBorder + dirBorder * width);
        Gizmos.DrawLine(posBorder - dirBorder * width - dirBorderForward * lenght, posBorder + dirBorder * width - dirBorderForward * lenght);
        Gizmos.DrawLine(posBorder - dirBorder * width - dirBorderForward * lenght, posBorder - dirBorder * width);
        Gizmos.DrawLine(posBorder + dirBorder * width - dirBorderForward * lenght, posBorder + dirBorder * width);

        Gizmos.color = Color.green;
        DrawBox(ref box);
        Mesh mesh = GetComponent<MeshFilter>().sharedMesh;
        Vector3[] vertices = mesh.vertices;
        for (int i = 0; i < selectedPoints.Count; i++)
        {
            Vector3 vert = transform.TransformPoint(vertices[selectedPoints[i]]);
            Gizmos.DrawLine(vert, vert + Vector3.up);
        }

        Gizmos.color = Color.red;
        Mesh mesh02 = secondTerrain.GetComponent<MeshFilter>().sharedMesh;
        Vector3[] vertices02 = mesh02.vertices;
        for (int i = 0; i < selectedPoints02.Count; i++)
        {
            Vector3 vert = secondTerrain.transform.TransformPoint(vertices02[selectedPoints02[i]]);
            Gizmos.DrawLine(vert, vert + Vector3.up);
        }
    }

    protected void DrawBox(ref Box2 box)
    {
        Vector2 v0, v1, v2, v3;
        box.CalcVertices(out v0, out v1, out v2, out v3);
        Gizmos.DrawLine(new Vector3(v0.x,0,v0.y), new Vector3(v1.x, 0, v1.y));
        Gizmos.DrawLine(new Vector3(v1.x, 0, v1.y), new Vector3(v2.x, 0, v2.y));
        Gizmos.DrawLine(new Vector3(v2.x, 0, v2.y), new Vector3(v3.x, 0, v3.y));
        Gizmos.DrawLine(new Vector3(v3.x, 0, v3.y), new Vector3(v0.x, 0, v0.y));
    }
}
        /*
        for (int i=0; i<mesh.vertexCount; i++)
        {
            

            Vector3 vert = transform.TransformPoint(mesh.vertices[i]);
            if (box.Contains(vert.ToVector2XZ()))
            {
                Debug.Log(i);
                //GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                //sphere.transform.parent = transform;
                //sphere.transform.position = mesh.vertices[i];
                //sphere.name = i.ToString();

            }
        }
        */
