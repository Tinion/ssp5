﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
[System.Serializable]
public class RoadTemplate
{
	public int id = 0;
	public List<RoadPart> roadParts = new List<RoadPart> ();
	public List<Material> sourceMaterials = new List<Material> ();
	public bool isOpenVertexEdit = false;
	public MeshFilter MF;
	public MeshRenderer MR;

	public RoadTemplate ()
	{

	}

	public RoadTemplate (Transform tr, int count)
	{
		if (MF == null) 
		{
			Transform trChild = tr.Find ("RoadTemplateMesh" + count.ToString ());
			if (trChild != null) {
				MF = trChild.GetComponent<MeshFilter> ();
				MR = trChild.GetComponent<MeshRenderer> ();
			}	
			else 
			{
				GameObject newGO = new GameObject ();
				newGO.hideFlags = HideFlags.HideInHierarchy;
				newGO.name = "RoadTemplateMesh" + count.ToString ();
				newGO.transform.parent = tr;
				newGO.transform.localPosition = Vector3.zero;

				MF = newGO.AddComponent<MeshFilter> ();
				MF.sharedMesh = new Mesh ();
				MF.sharedMesh = PrefabCollector.CreateOrReplaceAssetMesh (MF.sharedMesh, "Assets/" + newGO.name + ".asset");
				MR = newGO.AddComponent<MeshRenderer> ();
			}
		}
	}

	public RoadTemplateAsset GetContextAsset ()
	{
		RoadTemplateAsset cxt = ScriptableObject.CreateInstance<RoadTemplateAsset> ();

		cxt.context = new RoadTemplate ();

		cxt.context.roadParts = new List<RoadPart> ();
		for (int i = 0; i < roadParts.Count; i++) {
			RoadPart roadPart = new RoadPart (roadParts [i].type, roadParts [i].pivot, roadParts [i].vertex.ToArray (), roadParts [i].matId);
			cxt.context.roadParts.Add (roadPart);
			Debug.Log (i + " PPP " + roadPart.vertex.Count);
		}
		return cxt;
	}



}

[Serializable]
public class Vertex
{
	public Vector2 point;
	public Vector2 normal;
	public float uCoord;

	public Vertex (Vector2 point, Vector2 normal, float uCoord)
	{
		this.point = point;
		this.normal = normal;
		this.uCoord = uCoord;
		;
	}
}

public enum RoadPartType
{
	Center = 0,
	LeftSide = 1,
	RightSide = 2,
	BetweenPart = 3,
	Road = 4,
}

public enum DotSelectionType
{
	RoadStart = 0,
	RoadLeft = 1,
	RoadRight = 2,
	RoadEnd = 3,
}

[Serializable]
public class RoadPart
{
	public RoadPartType type;
	public List<Vertex> vertex;
	public float pivot;
	public int matId;

	public RoadPart (RoadPartType type, float pivot, Vertex[] vertex, int matId)
	{
		this.type = type;
		this.pivot = pivot;
		this.vertex = new List<Vertex> ();
		this.vertex.AddRange (vertex);
		this.matId = matId;
	}

	public RoadPart (RoadPartType type, float pivot)
	{
		this.type = type;
		this.pivot = pivot;
		this.vertex = new List<Vertex> ();
	}
}

[ExecuteInEditMode]
//[RequireComponent(typeof(MeshFilter))]
//[RequireComponent(typeof(MeshRenderer))]
[System.Serializable]
public class SplineExtrusion : MonoBehaviour
{
	[SerializeField]
	public SplineType splineType;
	public OneSpline spline;

	public List<RoadTemplate> roadTemplates;
	public List<Material> sourceMaterials;
    List<OrientedPoint> path;

    private bool toUpdate = true;
	public float TextureScale = 0.5f;
	public float size = 0.2f;
    public bool isHide = true;
	public SplineExtrusion (SplineType type)
	{
		this.splineType = type;
	}

	/// <summary>
	/// Clear shape vertices, then create three vertices with three normals for the extrusion to be visible
	/// </summary>
	private void Reset ()
	{
		if (roadTemplates != null)
			roadTemplates.Clear ();
		roadTemplates = new List<RoadTemplate> ();

		toUpdate = true;
		OnEnable ();
	}

	private void OnValidate ()
	{
		toUpdate = true;
	}



	private void OnEnable ()
	{
		
		if (!Application.isPlaying) 
		{
			if (sourceMaterials == null)
				sourceMaterials = new List<Material> ();
		
			spline = BlockPlane.GetSpline (transform.parent, splineType);
        
			if (spline != null) {
				spline.NodeCountChanged.AddListener (() => toUpdate = true);
				spline.CurveChanged.AddListener (() => toUpdate = true);
				GenerateMesh ();
			}
		}

	}

	private List<OrientedPoint> GetPath (RoadTemplate template, out float lenght)
	{
		lenght = 0;
		path = new List<OrientedPoint> ();
		foreach (OneSplineSegment segment in spline.segments) {
			//
			if (spline.segments.IndexOf(segment) == template.id) 
			{
				lenght = segment.lenght;
				float oldT = 0;
				if (size < 0.001f)
					size = 0.1f;
				for (float t = segment.startTime; t <= segment.endTime; t += spline._step_n * size)
                {
                    if (t > segment.endTime)
                        t = segment.endTime;
                    float uT =  spline.UniformGlobalLenght (t);
					//float uT = t;
					var point = spline.transform.InverseTransformPoint(spline.GlobalPos2 (uT, 0));//UniformGlobalPos (t); 

					var rotation = OneCubicBezierCurve.GetRotationFromTangent (spline.GetTangent (uT));
					Vector2 scale = spline.GetUniformSegmentScale (t);
					path.Add (new OrientedPoint (point, rotation, scale, spline.GetTilt(uT)));
					oldT = t;
				}
                
                
				float diff = segment.endTime;
				if (diff > 0) {
                    //float uT = diff;
                    float uT = spline.UniformGlobalLenght (diff);

					var point = spline.transform.InverseTransformPoint(spline.GlobalPos2 (uT, 0));
					var rotation = OneCubicBezierCurve.GetRotationFromTangent (spline.GetTangent (uT));
					Vector2 scale = spline.GetUniformSegmentScale(diff);
					path.Add (new OrientedPoint (point, rotation, scale, spline.GetTilt(uT)));
				}
                
				return path;
			}
		}
		return null;

	}

	public void LoadRoadDefault (int current)
	{
		RoadTemplateAsset asset = AssetDatabase.LoadAssetAtPath<RoadTemplateAsset> ("Assets/RoadTemplates/Contexts/RoadDefault.asset");
		LoadRoadTemplate(asset, current);
	}

	public void LoadRoadTemplate(RoadTemplateAsset asset, int currentSelection)
	{
		if (currentSelection >= 0)
			roadTemplates [currentSelection] = new RoadTemplate ();
		roadTemplates [currentSelection].roadParts = new System.Collections.Generic.List<RoadPart> ();
		for (int j = 0; j < asset.context.roadParts.Count; j++) {
			{
				RoadPart roadPart = new RoadPart (asset.context.roadParts [j].type, asset.context.roadParts [j].pivot, asset.context.roadParts [j].vertex.ToArray(), asset.context.roadParts [j].matId);
				roadTemplates [currentSelection].roadParts.Add (roadPart);

			}
		}

		if (roadTemplates [currentSelection].MF == null) 
		{
			Transform trChild = transform.Find ("RoadTemplateMesh" + currentSelection.ToString ());
			if (trChild != null) {
				roadTemplates [currentSelection].MF = trChild.GetComponent<MeshFilter> ();
				roadTemplates [currentSelection].MR = trChild.GetComponent<MeshRenderer> ();
			}	
			else 
			{
				GameObject newGO = new GameObject ();
                if (isHide)
                    newGO.hideFlags = HideFlags.HideInHierarchy;
                else
                    newGO.hideFlags = HideFlags.None;
				newGO.name = "RoadTemplateMesh" + currentSelection.ToString ();
				newGO.transform.parent = transform;
				newGO.transform.localPosition = Vector3.zero;

				roadTemplates [currentSelection].MF = newGO.AddComponent<MeshFilter> ();
				roadTemplates [currentSelection].MF.sharedMesh = new Mesh ();
				roadTemplates [currentSelection].MF.sharedMesh = PrefabCollector.CreateOrReplaceAssetMesh (roadTemplates [currentSelection].MF.sharedMesh, "Assets/" + newGO.name + ".asset");
				roadTemplates [currentSelection].MR = newGO.AddComponent<MeshRenderer> ();
			}
		}

		for (int h = 0; h < asset.context.sourceMaterials.Count; h++) 
		{
			if (sourceMaterials.Contains (asset.context.sourceMaterials [h])) 
			{
				Debug.Log(h + " Contains");
				int matIndex = sourceMaterials.IndexOf (asset.context.sourceMaterials [h]);
				Debug.Log (matIndex);
				for (int j = 0; j < roadTemplates [currentSelection].roadParts.Count; j++)
					if (asset.context.roadParts[j].matId == h) 
					{
						Debug.Log (j + " = " + matIndex);
						roadTemplates [currentSelection].roadParts [j].matId = matIndex;
					}
			} else {
				Debug.Log(h + " Not Contains");
				sourceMaterials.Add (asset.context.sourceMaterials [h]);
				Debug.Log (sourceMaterials.Count - 1);
				for (int j = 0; j < roadTemplates [currentSelection].roadParts.Count; j++)
					if (asset.context.roadParts[j].matId == h) {
						Debug.Log (j + " = " + (sourceMaterials.Count - 1).ToString());
						roadTemplates [currentSelection].roadParts [j].matId = sourceMaterials.Count - 1;

					}
			}
		}

		ChangeMaterial (roadTemplates [currentSelection]);

	}
	/*
	private List<OrientedPoint> GetPath(RoadTemplate template, out float lenght)
    {
		lenght = 0;
        var path = new List<OrientedPoint>();
			foreach (OneSplineSegment segment in spline.segments) 
			{
			
				if (segment.id == template.id) 
				{
				lenght = segment.lenght;
				float oldT = 0;
				for (float t = segment.startTime; t < segment.endTime; t += (segment.endTime - segment.startTime)/lenght) 
					{
						float uT = spline.UniformGlobalLenght (t);
						var point = spline.GlobalPos (uT);//UniformGlobalPos (t); 
						
						var rotation = OneCubicBezierCurve.GetRotationFromTangent (spline.GetTangent (uT));
						Vector2 scale = spline.GetSegmentScale (uT);
						path.Add (new OrientedPoint (point, rotation, scale));
						oldT = t;
					}
				float diff = segment.endTime;
				if (diff > 0) 
				{
					float uT = spline.UniformGlobalLenght (diff);

					var point = spline.GlobalPos (uT);
					var rotation = OneCubicBezierCurve.GetRotationFromTangent (spline.GetTangent (uT));
					Vector2 scale = spline.GetSegmentScale (uT);
					path.Add (new OrientedPoint (point, rotation, scale));
				}
				return path;
				}
			}
			return null;

    }
*/
	[ContextMenu ("SaveMesh")]
	public void SaveMesh ()
	{
		for (int i = 0; i < roadTemplates.Count; i++) {
			if (roadTemplates [i].MF.sharedMesh == null) {
				roadTemplates [i].MF.sharedMesh = new Mesh ();
			}
			roadTemplates [i].MF.sharedMesh = PrefabCollector.CreateOrReplaceAssetMesh (roadTemplates [i].MF.sharedMesh, "Assets/" + gameObject.name + ".asset");
		}
	}

	public Vector3[] GenerateDots (RoadTemplate roadTemplate, DotSelectionType selectionType)
	{

		List<Vector3> dots = new List<Vector3> ();

		float lenght = 15;
		path = GetPath (roadTemplate, out lenght);
		int fullCount = 0;

		RoadPart rpLeft = null;

		if (selectionType != DotSelectionType.RoadStart && selectionType != DotSelectionType.RoadEnd) {

			foreach (RoadPart rp in roadTemplate.roadParts) {
				if ((int)rp.type == (int)selectionType) {
					rpLeft = rp;

					fullCount += rpLeft.vertex.Count;

					int vertsInShape = fullCount;
					int segments = path.Count - 1;
					int edgeLoops = path.Count;
					int vertCount = vertsInShape * edgeLoops;

					var vertices = new Vector3[vertCount];
					var normals = new Vector3[vertCount];
					var uvs = new Vector2[vertCount];

					int index = 0;
					int roadIndex = 0;
					foreach (OrientedPoint op in path) {
						foreach (Vertex v in rpLeft.vertex) {
							vertices [index] = op.LocalToWorld (v.point, rpLeft.pivot);
							normals [index] = op.LocalToWorldDirection (v.normal);
							float sec = path.IndexOf (op) / ((float)edgeLoops) * TextureScale;
							uvs [index] = new Vector2 (v.uCoord, sec * lenght);



							if (rpLeft.vertex.IndexOf (v) == ((selectionType == DotSelectionType.RoadRight) ? rpLeft.vertex.Count - 1 : 0)) {
								dots.Add (vertices [index]);
								//Debug.Log (index);
								//GameObject newGo = GameObject.CreatePrimitive (PrimitiveType.Sphere);
								//newGo.transform.position = vertices [index];
								//newGo.name = index.ToString ();
							}
						
							index++;
						}
					}

				}
			}

		} 
		else 
		{
			foreach (RoadPart rp in roadTemplate.roadParts) 
			{
				if (rp.type == RoadPartType.LeftSide || rp.type == RoadPartType.RightSide || rp.type == RoadPartType.Center) 
				{
					rpLeft = rp;

					fullCount += rpLeft.vertex.Count;

					int vertsInShape = fullCount;
					int segments = path.Count - 1;
					int edgeLoops = path.Count;
					int vertCount = vertsInShape * edgeLoops;

					var vertices = new Vector3[vertCount];
					var normals = new Vector3[vertCount];
					var uvs = new Vector2[vertCount];

					int index = 0;
					int roadIndex = 0;
					OrientedPoint op = new OrientedPoint();
					if (selectionType == DotSelectionType.RoadStart)
						op = path [0];
					else if (selectionType == DotSelectionType.RoadEnd)
						op = path [path.Count - 1];

					foreach (Vertex v in rpLeft.vertex) {
						vertices [index] = op.LocalToWorld (v.point, rpLeft.pivot);
						normals [index] = op.LocalToWorldDirection (v.normal);
						float sec = path.IndexOf (op) / ((float)edgeLoops) * TextureScale;
						uvs [index] = new Vector2 (v.uCoord, sec * lenght);

						switch (rp.type) 
						{
						case RoadPartType.RightSide:
							{
								if (rpLeft.vertex.IndexOf (v) == (rpLeft.vertex.Count - 1)) 
								{
									dots.Add (vertices [index]);
									//Debug.Log (index);
									//GameObject newGo = GameObject.CreatePrimitive (PrimitiveType.Sphere);

									//newGo.transform.position = vertices [index];
									//newGo.name = index.ToString () + " RightSide " + index;
								}
							}
							break;
						case RoadPartType.LeftSide:
							{
								if (rpLeft.vertex.IndexOf (v) == 0) 
								{
									dots.Add (vertices [index]);
									//Debug.Log (index);
									//GameObject newGo = GameObject.CreatePrimitive (PrimitiveType.Sphere);

									//newGo.transform.position = vertices [index];
									//newGo.name = index.ToString () + " LeftSide " + index;
								}
							}
							break;
						case RoadPartType.Center:
							{
									dots.Add (vertices [index]);
									//Debug.Log (index);
									//GameObject newGo = GameObject.CreatePrimitive (PrimitiveType.Sphere);

									//newGo.transform.position = vertices [index];
									//newGo.name = index.ToString () + " Center " + index;

							}
							break;
						}
						index++;
					}
					
				}

			}

		}

		return dots.ToArray ();
	}

    private void OnDrawGizmos()
    {
        //for (int i = 0; i < path.Count; i++)
        //    Gizmos.DrawLine(path[i].position, Math3d.RotatePointAroundPivot(path[i].position + Vector3.up * 3, path[i].position, new Vector3(0, 0, path[i].tilt)));

    }

    public void GenerateMesh ()
	{
		if (roadTemplates == null)
			return;
		
		for (int z = 0; z < roadTemplates.Count; z++) {
			int matCount = 0;
			List<int> idData = new List<int> ();
			for (int i = 0; i < roadTemplates [z].roadParts.Count; i++)
				if (!idData.Contains (roadTemplates [z].roadParts [i].matId)) {
					idData.Add (roadTemplates [z].roadParts [i].matId);
					matCount++;
				}


			float lenght = 15;
			List<OrientedPoint> path = GetPath (roadTemplates [z], out lenght);
			int fullCount = 0;
			foreach (RoadPart rp in roadTemplates[z].roadParts)
				fullCount += rp.vertex.Count;


			int vertsInShape = fullCount;
			int segments = path.Count - 1;
			int edgeLoops = path.Count;
			int vertCount = vertsInShape * edgeLoops;

			//var triangleIndices = new List<int> (vertsInShape * 2 * segments * 3);
			List<List<int>> triangleIndices = new List<List<int>> ();
			for (int i = 0; i < matCount; i++) {
				triangleIndices.Add (new List<int> ());
			}

			var vertices = new Vector3[vertCount];
			var normals = new Vector3[vertCount];
			var uvs = new Vector2[vertCount];

			int index = 0;
			int roadIndex = 0;
			foreach (OrientedPoint op in path) {
				for (int i = 0; i < roadTemplates [z].roadParts.Count; i++) {
					if (roadTemplates [z].roadParts [i].type == RoadPartType.Road)
						roadIndex = 0;

					foreach (Vertex v in roadTemplates[z].roadParts[i].vertex) {
						if (roadTemplates [z].roadParts [i].type == RoadPartType.Road) {
							float pivot = Mathf.Lerp (roadTemplates [z].roadParts [i - 1].pivot, roadTemplates [z].roadParts [i + 1].pivot, 
								              ((float)roadIndex) / ((float)(roadTemplates [z].roadParts [i].vertex.Count - 1)));
                            vertices[index] = Math3d.RotatePointAroundPivot(op.LocalToWorld(v.point, pivot), op.position, new Vector3(0, 0, op.tilt));
							roadIndex++;
						} else
							vertices [index] = Math3d.RotatePointAroundPivot(op.LocalToWorld (v.point, roadTemplates [z].roadParts [i].pivot), op.position, new Vector3(0,0,op.tilt));
                        normals [index] = op.LocalToWorldDirection (v.normal);
						float sec = path.IndexOf (op) / ((float)edgeLoops) * TextureScale;
						uvs [index] = new Vector2 (v.uCoord, sec * lenght);
						index++;

					}
				}
			}


			index = 0;

				
			//fullCount = rp.vertex.Count;

			for (int i = 0; i < segments; i++) {	
					
				int roadMax = 0;
				foreach (RoadPart rp in roadTemplates[z].roadParts) {

					for (int j = 0; j < rp.vertex.Count; j++) {
						int offset = (j + roadMax) == (fullCount - 1) ? -(fullCount - 1) : 1;
						int a = index + fullCount;
						int b = index;
						int c = index + offset;
						int d = index + offset + fullCount;
						int currentTriangle = idData.FindIndex (x => x.Equals (rp.matId));
						triangleIndices [currentTriangle].Add (c);
						triangleIndices [currentTriangle].Add (b);
						triangleIndices [currentTriangle].Add (a);
						triangleIndices [currentTriangle].Add (a);
						triangleIndices [currentTriangle].Add (d);
						triangleIndices [currentTriangle].Add (c);
						//if (i == 0 || i == 1 || i == 2) 
						//{
						//	Debug.Log (i + " " + c + " " + b + " " + a + " " + a + " " + d + " " + c);
						//}
						index++;
					}

					roadMax += rp.vertex.Count;
				}
			}

			Mesh mesh = new Mesh ();
			mesh.vertices = vertices;
			mesh.normals = normals;
			mesh.uv = uvs;
			mesh.subMeshCount = matCount;
			//Debug.Log (z + " " + matCount + " MatCount");
			for (int i = 0; i < matCount; i++) {
				//Debug.Log (i + " " + triangleIndices [i].Count + " " + idData [i]);
				mesh.SetTriangles (triangleIndices [i].ToArray (), i);
			}
			mesh.RecalculateBounds ();
			//mf.sharedMesh = mesh;

			if (roadTemplates [z].MF == null)
				roadTemplates [z].MF = transform.Find ("RoadTemplateMesh" + z).GetComponent<MeshFilter> ();
			
			if (roadTemplates [z].MR == null)
				roadTemplates [z].MR = transform.Find ("RoadTemplateMesh" + z).GetComponent<MeshRenderer> ();
			
			roadTemplates [z].MF.sharedMesh = mesh;

		

		}

	}

	private int Contains (ArrayList searchList, string searchName)
	{
		for (int i = 0; i < searchList.Count; i++) {
			if (((Material)searchList [i]).name == searchName) {
				return i;
			}
		}
		return -1;
	}

	public void ChangeMaterial (RoadTemplate roadTmp)
	{
		List<Material> mat = new List<Material> ();

		for (int i = 0; i < roadTmp.roadParts.Count; i++) {
			if (!mat.Contains (sourceMaterials [roadTmp.roadParts [i].matId]))
				mat.Add (sourceMaterials [roadTmp.roadParts [i].matId]);
		}
			
		roadTmp.MR.sharedMaterials = mat.ToArray ();
	}

	public struct OrientedPoint
	{
		public Vector3 position;
		public Quaternion rotation;
		public Vector2 scale;
        public float tilt;

		public OrientedPoint (Vector3 position, Quaternion rotation, Vector2 scale, float tilt)
		{
			this.position = position;
			this.rotation = rotation;
			this.scale = scale;
            this.tilt = tilt;
		}

		public Vector3 LocalToWorld (Vector3 point, float xPivot)
		{
			if (xPivot < 0)
				return position + rotation * new Vector3 (point.x + scale.x * xPivot, point.y, point.z);
			else
				return position + rotation * new Vector3 (point.x + scale.y * xPivot, point.y, point.z);
		}

		public Vector3 LocalToWorldDirection (Vector3 dir)
		{
			return rotation * dir;
		}
	}
}
