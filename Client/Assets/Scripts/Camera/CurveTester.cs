﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Dest.Math;

namespace SplineSystem
{
	
	public enum CurveTesterType
	{
		MainPercent = 0,
		SelfPercent = 1,
		GameObject = 2,
	}

	[System.Serializable]
	public class DebugAdditionalFloat
	{
		public string name;
		public float param;

		public DebugAdditionalFloat (string _name, float _param)
		{
			name = _name;
			param = _param;
		}
	}

	[System.Serializable]
	public class DebugAdditionalBool
	{
		public string name;
		public bool param;

		public DebugAdditionalBool (string _name, bool _param)
		{
			name = _name;
			param = _param;
		}
	}

	[System.Serializable]
	public class DebugAdditionalGameObject
	{
		public string name;
		public GameObject param;

		public DebugAdditionalGameObject (string _name)
		{
			name = _name;
		}
	}

	[System.Serializable]
	public class DebugAdditionalColor
	{
		public string name;
		[ColorUsage (false)]
		public Color param;

		public DebugAdditionalColor (string _name, Color _param)
		{
			name = _name;
			param = _param;
		}
	}



	public enum DebugList
	{
		GetStep = 0,
		GlobalPos2 = 1,
		UniformGlobalPos = 2,
		UniformGlobalPosAdd = 3,
	}

	[System.Serializable]
	public class DebugModule
	{
		public delegate void Visualize (DebugModule module);

		public delegate string Message (DebugModule module);

		Visualize _vis;
		Message _mes;

		public bool isOnGui;
		public bool isDrawGizmos;
		public string debugString = "---";

		public string name;
		public bool isActive;
		public float selfType;
		public GameObject selfGO;

		public CurveTesterType type;
		public List<DebugAdditionalFloat> floats;
		public List<DebugAdditionalBool> bools;
		public List<DebugAdditionalGameObject> gameObjects;
		public List<DebugAdditionalColor> colors;

		public DebugModule (string _name, bool _isOnGui, bool _isDrawGizmos)
		{
			name = _name;
			isActive = true;
			isOnGui = _isOnGui;
			isDrawGizmos = _isDrawGizmos;
			floats = new List<DebugAdditionalFloat> ();
			bools = new List<DebugAdditionalBool> ();
			gameObjects = new List<DebugAdditionalGameObject> ();
			colors = new List<DebugAdditionalColor> ();
		}

		public void RegisterHandlerVis (Visualize vis)
		{
			_vis = vis;
		}

		public void RegisterHandlerMes (Message mes)
		{
			_mes = mes;
		}

		public void Draw ()
		{
			_vis (this);
		}

		public string GetMessage ()
		{
			return _mes (this);
		}
		/*
		public static void VisualizeGlobalPos(OneSpline spline, , float _t, float _debugSize)
		{
			switch (this.type) 
			{
			case CurveTesterType.MainPercent:
				Gizmos.DrawSphere (spline.GlobalPos2 (_t, module.floats [0].param), _debugSize);
				//Debug.Log ("4");
				break;
			case CurveTesterType.SelfPercent:
				Gizmos.DrawSphere (spline.GlobalPos2 (module.selfType, module.floats [0].param), _debugSize);
				break;
			case CurveTesterType.GameObject:
				if (module.selfGO != null)
					Gizmos.DrawSphere (spline.GlobalPos2 (spline.GetStep (module.selfGO.transform.position), module.floats [0].param), _debugSize);
				break;
			}
		}
		*/
	}



	[ExecuteInEditMode]
	public class CurveTester : MonoBehaviour
	{
		public static bool isUpdate = false;
		public static float t;
		public static float debugSize = 1f;
		public static OneSpline oneSpline;
		public static Highway highWay;
		public SplineType type;
		public List<DebugModule> debugModule = new List<DebugModule> ();

		
		//public float dist;
		
		//[Range (-1, 1)]
		//public float width;
		//public bool isForward = true;
		//public Transform obj2;

		//public string _GetStepObj2 = "";
		private float oldT;


		void Start ()
		{
			Initialize ();
		}

		[ContextMenu ("Recalc")]
		public void Initialize ()
		{
			debugModule = new List<DebugModule> ();
			DebugModule getStepModule = new DebugModule ("GlobalPos", false, true);
			getStepModule.floats.Add (new DebugAdditionalFloat ("Width", 0));
			getStepModule.colors.Add (new DebugAdditionalColor ("SphereColor", Color.blue));
			getStepModule.RegisterHandlerVis (new DebugModule.Visualize (DrawGlobalPos));

			debugModule.Add (getStepModule);

			getStepModule = new DebugModule ("GetStep", true, true);
			getStepModule.RegisterHandlerVis (new DebugModule.Visualize (DrawGetStep));
			getStepModule.RegisterHandlerMes (new DebugModule.Message (GetMessageGetStep));

			debugModule.Add (getStepModule);

			getStepModule = new DebugModule ("UniformGlobalPos", true, true);
			getStepModule.floats.Add (new DebugAdditionalFloat ("Width", 0));
			getStepModule.colors.Add (new DebugAdditionalColor ("SphereColor", Color.yellow));
			getStepModule.RegisterHandlerVis (new DebugModule.Visualize (DrawUniformGlobalPos));
            getStepModule.RegisterHandlerMes(new DebugModule.Message(GetMessageUniform));


            debugModule.Add (getStepModule);

			getStepModule = new DebugModule ("UniformGlobalPosAdd", false, true);
			getStepModule.floats.Add (new DebugAdditionalFloat ("Width", 0));
			getStepModule.floats.Add (new DebugAdditionalFloat ("Dist", 10));
			getStepModule.bools.Add (new DebugAdditionalBool ("Forward", false));
			getStepModule.colors.Add (new DebugAdditionalColor ("SphereColor", Color.green));
			getStepModule.colors.Add (new DebugAdditionalColor ("TargetColor", Color.red));
			getStepModule.colors.Add (new DebugAdditionalColor ("LinesColor", Color.cyan));
			getStepModule.RegisterHandlerVis (new DebugModule.Visualize (DrawUniformGlobalPosAdd));

			debugModule.Add (getStepModule);

			getStepModule = new DebugModule ("GetAngle", true, false);
			getStepModule.RegisterHandlerMes (new DebugModule.Message (GetMessageGetAngle));

			debugModule.Add (getStepModule);

			getStepModule = new DebugModule ("NearestPointOnCurve", true, true);
			getStepModule.floats.Add (new DebugAdditionalFloat ("Width", 0));
			getStepModule.colors.Add (new DebugAdditionalColor ("SphereColor", Color.yellow));
			getStepModule.RegisterHandlerVis (new DebugModule.Visualize (DrawNearestPointOnCurve));
			getStepModule.RegisterHandlerMes (new DebugModule.Message (GetMessageOnPointCurve));

			debugModule.Add (getStepModule);
		}

		public static void DrawGlobalPos (DebugModule module)
		{
			float time = CurveTester.t;
			switch (module.type) {
			case CurveTesterType.MainPercent:
				time = CurveTester.t;
				break;
			case CurveTesterType.SelfPercent:
				time = module.selfType;
				break;
			case CurveTesterType.GameObject:
				if (module.selfGO != null) 
				{
					if (highWay != null) 
					{
						OneSpline thisSpline = highWay.GetSpline(module.selfGO.transform.position);
						float size = thisSpline.Length / highWay.Lenght;
						time = thisSpline.GetStep (module.selfGO.transform.position);
					}
					else
						time = oneSpline.GetStep (module.selfGO.transform.position);
				}
				break;
			}
			if (highWay != null) 
			{
				float percent = 0;
				OneSpline thisSpline = highWay.GetSpline (time, out percent);
				Gizmos.DrawSphere (thisSpline.GlobalPos2 (percent, module.floats [0].param), CurveTester.debugSize);
			}
			else 
				Gizmos.DrawSphere (oneSpline.GlobalPos2 (time, module.floats [0].param), CurveTester.debugSize);

		}

		public static void DrawGetStep (DebugModule module)
		{
			Vector3 point = Vector3.zero;
			float uT = 0;
			switch (module.type) {
			case CurveTesterType.MainPercent:
				
				if (highWay != null) 
				{
					float percent = 0;
					OneSpline thisSpline = highWay.GetSpline (CurveTester.t, out percent);
					uT = thisSpline.UniformGlobalLenght (percent);
					point = thisSpline.GlobalPos2 (uT, 0);
				} else {
					uT = oneSpline.UniformGlobalLenght (CurveTester.t);
					point = CurveTester.oneSpline.GlobalPos2 (uT, 0);
				}
				break;
			case CurveTesterType.SelfPercent:
				if (highWay != null) 
				{
					float percent = 0;
					OneSpline thisSpline = highWay.GetSpline (module.selfType, out percent);
					uT = thisSpline.UniformGlobalLenght (percent);
					point = thisSpline.GlobalPos2 (uT, 0);
				} 
				else 
				{
					uT = oneSpline.UniformGlobalLenght (module.selfType);
					point = CurveTester.oneSpline.GlobalPos2 (uT, 0);
				}
				break;
			case CurveTesterType.GameObject:
				if (module.selfGO != null) 
				{
					point = module.selfGO.transform.position;
				}
				break;
			}

			Vector3 point2 = Vector3.zero;
			float currentStep = 0;
			float currentPercent = 0;
			Vector3 tempNext = Vector3.zero;

			if (highWay != null) 
			{
				OneSpline thisSpline = highWay.GetSpline(point);
				if (thisSpline.NearestPointOnCurve2 (point, out point2, out currentPercent, out currentStep)) 
					Gizmos.DrawSphere (point2, CurveTester.debugSize);
			}
			else
			if (oneSpline.NearestPointOnCurve2 (point, out point2, out currentPercent, out currentStep)) 
				Gizmos.DrawSphere (point2, CurveTester.debugSize);

		}

		public static void DrawUniformGlobalPos (DebugModule module)
		{
			float uT = 0;
			switch (module.type) {
			case CurveTesterType.MainPercent:
				if (highWay != null) 
				{
					float percent = 0;
					OneSpline thisSpline = highWay.GetSpline (CurveTester.t, out percent);
					uT = thisSpline.UniformGlobalLenght (percent);
				} 
				else 
				uT = oneSpline.UniformGlobalLenght (CurveTester.t);
				break;
			case CurveTesterType.SelfPercent:
				if (highWay != null) 
				{
					float percent = 0;
					OneSpline thisSpline = highWay.GetSpline (module.selfType, out percent);
					uT = thisSpline.UniformGlobalLenght (percent);
				} 
				else 
				uT = oneSpline.UniformGlobalLenght (module.selfType);
				break;
			case CurveTesterType.GameObject:
				if (module.selfGO != null) 
				{
					if (highWay != null) 
					{
						OneSpline thisSpline = highWay.GetSpline(module.selfGO.transform.position);
						float size = thisSpline.Length / highWay.Lenght;
						uT = thisSpline.GetStep (module.selfGO.transform.position);
					}
					else
					uT = oneSpline.UniformGlobalLenght (CurveTester.oneSpline.GetStep (module.selfGO.transform.position));
				}
				break;
			}
			if (highWay != null) 
			{
				float percent = 0;
				OneSpline thisSpline = highWay.GetSpline (uT, out percent);
				Gizmos.DrawSphere (thisSpline.GlobalPos2 (percent, module.floats [0].param), CurveTester.debugSize);
			}
			else
				Gizmos.DrawSphere (CurveTester.oneSpline.GlobalPos2 (uT, module.floats [0].param), CurveTester.debugSize);

		}

		public static void DrawNearestPointOnCurve (DebugModule module)
		{
			Vector3 point = Vector3.zero;
			Vector3 point2 = Vector3.zero; 
			float percent = 0;
			float step = 0;
			float uT = 0;


			switch (module.type) {
			case CurveTesterType.MainPercent:
				uT = oneSpline.UniformGlobalLenght (CurveTester.t);
				point = CurveTester.oneSpline.GlobalPos2 (uT, 0);
				//if (CurveTester.oneSpline.NearestPointOnCurve2 (point, out point2, out percent, out step))
					//Gizmos.DrawSphere (point2, CurveTester.debugSize);
				break;
			case CurveTesterType.SelfPercent:
				uT = oneSpline.UniformGlobalLenght (module.selfType);
				point = CurveTester.oneSpline.GlobalPos2 (uT, 0);
				//if (CurveTester.oneSpline.NearestPointOnCurve2 (point, out point2, out percent, out step))
					//Gizmos.DrawSphere (point2, CurveTester.debugSize);
				break;
			case CurveTesterType.GameObject:
				if (module.selfGO != null) {
					point = module.selfGO.transform.position;
					//if (CurveTester.oneSpline.NearestPointOnCurve2 (module.selfGO.transform.position, out point2, out percent, out step))
					//	Gizmos.DrawSphere (point2, CurveTester.debugSize);
				}
				break;
			}

			Vector3 start = Vector3.zero;
			Vector3 end = Vector3.zero;
			float min = 9999f;
			float currentStep = 0;

			for (float i = 0; (i + CurveTester.oneSpline._step_n) < 1.0f; i += CurveTester.oneSpline._step_n) {
				float next = i + CurveTester.oneSpline._step_n;
				if (next > 1.0f)
					next = 1.0f;

				float temp01 = CurveTester.oneSpline.UniformGlobalLenght (i);
				float temp02 = CurveTester.oneSpline.UniformGlobalLenght (next);
				Vector3 startTemp = CurveTester.oneSpline.GlobalPos2 (temp01, module.floats [0].param);
				Vector3 endTemp = CurveTester.oneSpline.GlobalPos2 (temp02, module.floats [0].param);
				Vector3 central = Vector3.Lerp (startTemp, endTemp, 0.5f);
				float dist = Vector3.Distance (point, central);
				if (dist < min) {
					min = dist;
					start = startTemp;
					end = endTemp;
					currentStep = i;
				}
			}

			Segment2 front = new Segment2 (start, end);

			Vector3 dir = Vector3.Normalize (start - end);
			Vector3 left = 10.0f * Vector3.Cross (dir, Vector3.up) + point;
			Vector3 right = -10.0f * Vector3.Cross (dir, Vector3.up) + point;


			Gizmos.color = Color.blue;
			Gizmos.DrawLine (start, start + Vector3.up * 2);
			Gizmos.DrawLine (end, end + Vector3.up * 2);
			Gizmos.DrawLine (start + Vector3.up * 2, end + Vector3.up * 2);



			Math3d.CrossPointLine (start, end, left, right, out point2);

			Gizmos.color = Color.yellow;
			Gizmos.DrawLine (left, left + Vector3.up * 2);
			Gizmos.DrawLine (right, right + Vector3.up * 2);
			Gizmos.DrawLine (left + Vector3.up * 2, right + Vector3.up * 2);

			float dist1 = Vector2.Distance (new Vector2 (start.z, start.x), new Vector2 (end.z, end.x));
			float dist2 = Vector2.Distance (new Vector2 (start.z, start.x), new Vector2 (point2.z, point2.x));
			step = (dist2 / dist1);
			point2.y = Mathf.Lerp (start.y, end.y, step);

			//Segment2Segment2Intr info;
			//Segment2 side = new Segment2 (left, right);
			//if (Intersection.FindSegment2Segment2 (ref front, ref side, out info)) 
			//{
			Gizmos.color = module.colors[0].param;
			Gizmos.DrawLine (point2, point2 + Vector3.up * 2);
			Gizmos.DrawLine (start + Vector3.up, point2 + Vector3.up);
			Gizmos.DrawSphere (point2, CurveTester.debugSize);
			//float dist1 = Vector2.Distance (start.ToVector2XZ (), end.ToVector2XZ ());
			//float dist2 = Vector2.Distance (start.ToVector2XZ (), info.Point0);
			//currentPercent = (dist2 / dist1);
			//point2 = info.Point0.ToVector3XZ ();
			//point2.y = Mathf.Lerp (start.y, end.y, currentPercent);
			//}
		}

		public static void DrawUniformGlobalPosAdd (DebugModule module)
		{
			Vector3 point = Vector3.zero;
			float addPercent = 0;
			float _dist = 0;
			float time = 0;
			switch (module.type) {
			case CurveTesterType.MainPercent:
				time = CurveTester.t;
				break;
			case CurveTesterType.SelfPercent:
				time = module.selfType;
				break;
			case CurveTesterType.GameObject:
				if (module.selfGO != null)
					time = oneSpline.UniformGlobalLenght (CurveTester.oneSpline.GetStep (oneSpline.GetPointOnCurve (module.selfGO.transform.position)));
				break;
			}

			float uT = oneSpline.UniformGlobalLenght (time);

			if (oneSpline.UniformGlobalPosAdd (uT, module.floats [1].param, module.floats [0].param, out point, out addPercent, out _dist, module.bools [0].param)) {
				Gizmos.color = module.colors [0].param;
				Vector3 pos = oneSpline.GlobalPos2 (uT, module.floats [0].param);
				Gizmos.DrawSphere (pos, CurveTester.debugSize);

				Gizmos.color = module.colors [1].param;
				Gizmos.DrawSphere (point, CurveTester.debugSize);

				Gizmos.color = module.colors [2].param;
				Gizmos.DrawLine (pos, pos + Vector3.up);
				Gizmos.DrawLine (pos + Vector3.up, point + Vector3.up);
				Gizmos.DrawLine (point, point + Vector3.up);
			} else {
				//Debug.Log (addPercent + " " + _dist + "!");
				Gizmos.color = module.colors [0].param;
				Vector3 pos = oneSpline.GlobalPos2 (uT, module.floats [0].param);
				Gizmos.DrawSphere (pos, CurveTester.debugSize);

				//point = oneSpline.GlobalPos ((isForward) ? 1 : 0);
				Vector3 pos2 = oneSpline.GlobalPos2 ((module.bools [0].param) ? 1 : 0, module.floats [0].param) + (oneSpline.GetTangent (module.bools [0].param ? 1 : 0, module.floats [0].param) * (module.bools [0].param ? 1 : -1)) * (module.floats [1].param - _dist);
				Gizmos.color = module.colors [1].param;
				Gizmos.DrawSphere (pos2, CurveTester.debugSize);

				Gizmos.color = module.colors [2].param;
				Gizmos.DrawLine (pos, pos + Vector3.up);
				Gizmos.DrawLine (pos + Vector3.up, pos2 + Vector3.up);
				Gizmos.DrawLine (pos2, pos2 + Vector3.up);


			}

		}

		public static string GetMessageGetAngle (DebugModule module)
		{
			float time = 0;
			switch (module.type) {
			case CurveTesterType.MainPercent:
				time = CurveTester.t;
				break;
			case CurveTesterType.SelfPercent:
				time = module.selfType;
				break;
			case CurveTesterType.GameObject:
				if (module.selfGO != null)
					time = oneSpline.GetStep (oneSpline.GetPointOnCurve (module.selfGO.transform.position));
				break;
			}

			float uT = oneSpline.UniformGlobalLenght (time);
			return oneSpline.GetAngle (oneSpline.transform.forward, oneSpline.transform.up, uT).ToString ();

		}

        public static string GetMessageUniform(DebugModule module)
        {
            float time = 0;
            switch (module.type)
            {
                case CurveTesterType.MainPercent:
				if (highWay != null) 
				{
					float percent = 0;
					OneSpline thisSpline = highWay.GetSpline (CurveTester.t, out percent);
					time = thisSpline.UniformGlobalLenght (percent);
				} 
				else 
                    time = CurveTester.t;
                    break;
                case CurveTesterType.SelfPercent:
				if (highWay != null) 
				{
					float percent = 0;
					OneSpline thisSpline = highWay.GetSpline (module.selfType, out percent);
					time = thisSpline.UniformGlobalLenght (percent);
				} 
				else 
                    time = module.selfType;
                    break;
			case CurveTesterType.GameObject:
				if (module.selfGO != null) 
				{
					if (highWay != null) 
					{
						OneSpline thisSpline = highWay.GetSpline(module.selfGO.transform.position);
						time = thisSpline.GetStep (module.selfGO.transform.position);
					}
					else
					time = oneSpline.GetStep (oneSpline.GetPointOnCurve (module.selfGO.transform.position));
				}
                    break;
            }

            float pct = 0;
			OneCubicBezierCurve curve;
			OneSpline current;
			if (highWay != null) {
				
				float percent = 0;
				OneSpline thisSpline = highWay.GetSpline (t, out percent);
				current = thisSpline;
				curve = current.GetLocalCurve (percent, out pct);
			} else {
				current = oneSpline;
				curve = oneSpline.GetLocalCurve (t, out pct);
			}
            float result = 0;
            float _length = curve.Length * pct;

            float hui = 0;
            float curStep = 0;
            float l = 0;
            for (float i = 0; i <= curve.Length; i += curve.step)
            {
                hui = i;
                float currentStep = Vector3.Distance(curve.GetLocation(Mathf.Clamp01(i / curve.Length)), curve.GetLocation(Mathf.Clamp01((i + curve.step) / curve.Length)));

                if (currentStep < 0.005f)
                    currentStep = 0.005f;
                curStep = currentStep;
                l = _length;
                if (currentStep <= _length)
                    _length -= currentStep;
                else
                {
                    float percent = _length / currentStep;
                    result = Mathf.Lerp((i / curve.Length), ((i + curve.step) / curve.Length), percent);

					if (highWay != null) 
					{
						return highWay.GetGlobalCurve(current, current.GetGlobalCurve(curve, result)).ToString();
					}
					else
                    	return oneSpline.GetGlobalCurve(curve, result).ToString();
                }
            }

            return oneSpline.GetGlobalCurve(curve, 1).ToString();


        }

        public static string GetMessageGetStep (DebugModule module)
		{
			Vector3 point = Vector3.zero;
			switch (module.type) {
			case CurveTesterType.MainPercent:
				{
					if (highWay != null) 
					{
						float percent = 0;
						OneSpline thisSpline = highWay.GetSpline (CurveTester.t, out percent);
						float uT = thisSpline.UniformGlobalLenght (percent);
						point = thisSpline.GlobalPos2 (uT, 0);
					} 
					else
					{
						float uT = oneSpline.UniformGlobalLenght (CurveTester.t);
						point = oneSpline.GlobalPos2 (uT, 0);
					}

				}
				break;
			case CurveTesterType.SelfPercent:
				{
					if (highWay != null) 
					{
						float percent = 0;
						OneSpline thisSpline = highWay.GetSpline (module.selfType, out percent);
						float uT = thisSpline.UniformGlobalLenght (percent);
						point = thisSpline.GlobalPos2 (uT, 0);
					} 
					else
					{
						float uT = oneSpline.UniformGlobalLenght (module.selfType);
						point = oneSpline.GlobalPos2 (uT, 0);
					}
				}
				break;
			case CurveTesterType.GameObject:
				{
					if (module.selfGO != null) 
					{
						if (highWay != null) 
						{
							OneSpline thisSpline = highWay.GetSpline (module.selfGO.transform.position);
							point = thisSpline.GetPointOnCurve (module.selfGO.transform.position);
						}
						else
						point = oneSpline.GetPointOnCurve (module.selfGO.transform.position);
					}
				}
				break;



			}
			string full = "";
			if (highWay != null) 
			{
				float percent = 0;
				OneSpline thisSpline = highWay.GetSpline (point);
				float currentStep = thisSpline.GetStep (point);

				int ch = highWay.roads.IndexOf (thisSpline);
				float allStep = 0;
				for (int i = 0; i < ch; i++)
					allStep += highWay.roads [i].Length;

				allStep += currentStep * highWay.roads [ch].Length;
				allStep /= highWay.Lenght;

				full = allStep.ToString () + " [" + ch + "/" + highWay.roads.Count + "]: " + currentStep;
			} 
			else 
			{
				float allStep = oneSpline.GetStep (point);
				float percent2 = 0;
				OneCubicBezierCurve curve = oneSpline.GetLocalCurve (allStep, out percent2);
				full = allStep.ToString () + " [" + oneSpline.curves.IndexOf (curve) + "/" + oneSpline.curves.Count + "]: " + percent2;
			}
			return full;

		}

		public static string GetMessageOnPointCurve (DebugModule module)
		{
			Vector3 point = Vector3.zero;
			switch (module.type) {
			case CurveTesterType.MainPercent:
				{
					float uT = oneSpline.UniformGlobalLenght (CurveTester.t);
					point = oneSpline.GlobalPos2 (uT, 0);
				}
				break;
			case CurveTesterType.SelfPercent:
				{
					float uT = oneSpline.UniformGlobalLenght (module.selfType);
					point = oneSpline.GlobalPos2 (uT, 0);
				}
				break;
			case CurveTesterType.GameObject:
				{
					if (module.selfGO != null)
						point = module.selfGO.transform.position;
				}
				break;
			}

			float currentPercent = 0;
			float currentStep = 0;
			Vector3 point2 = Vector3.zero;
			//NearestPointOnCurve(_pos, out curveIndex, out point2D, out step, out currentStep);
			if (oneSpline.NearestPointOnCurve2 (point, out point2, out currentPercent, out currentStep))
				return currentPercent.ToString ();
			else
				return "Fuck";

		}
		/*
		// Use this for initialization
		void Message ()
		{
			//Debug.Log ("0 ");
			//Debug.Log (Vector3.Normalize(oneSpline.GetTangent(t)));
			//Debug.Log (oneSpline.GetAngle(Vector3.forward, Vector3.up, t));
			int ch = 0;
			for (int i = 0; i < debugModule.Count; i++) 
			{
				//Debug.Log ("1 " + i);
				if (debugModule [i].isActive && debugModule[i].isOnGui) 
				{
					//Debug.Log ("2 " + i);

					//Debug.Log ("3 " + debugModule [i].debugString);
					if (debugModule [i].debugString != "")
						ch++;
				}

			}

			//if (obj2!=null)
			//	_GetStepObj2 = oneSpline.GetStep(obj2.transform.position).ToString();
			//Debug.Log (oneSpline.GetPointOnCurve(oneSpline.GlobalPos (t, width)));
		}
		*/
		public void SceneGUI ()
		{
			
			int ch = 0;
			for (int i = 0; i < debugModule.Count; i++) {
				if (debugModule [i].isOnGui && debugModule [i].isActive) {
					debugModule [i].debugString = debugModule [i].GetMessage ();
					GUI.Box (new Rect (10, 10 + 30 * ch, 1000, 20), debugModule [i].name + ": " + debugModule [i].debugString);
					ch++;
				}

			}

		}

		public void Draw ()
		{
			int ch = 0;
			for (int i = 0; i < debugModule.Count; i++) {
				//Debug.Log ("2");
				if (debugModule [i].isDrawGizmos && debugModule [i].isActive) {
					if (debugModule [i].colors.Count > 0)
						Gizmos.color = debugModule [i].colors [0].param;

					debugModule [i].Draw ();

					ch++;
				}
			}
		}

		void OnDrawGizmos ()
		{
			if (isUpdate)
				Draw ();
			//Gizmos.color = Color.red;
			//Gizmos.DrawSphere (oneSpline.GetLocationAlongSpline (t), 2.5f);

			//Gizmos.color = Color.red;
			//Gizmos.DrawSphere (oneSpline.GlobalPos (t, width), sphereSize);



			//Gizmos.color = Color.blue;
			//Gizmos.DrawSphere (oneSpline.GlobalPos (t, width), 2.5f);

			//Gizmos.color = Color.blue;
			//Gizmos.DrawSphere (oneSpline.UniformGlobalPos (t, width), sphereSize);
			/*
		if (isUniformGlobalPos2) 
		{
			Gizmos.color = Color.yellow;
			float uT = oneSpline.UniformGlobalLenght (t);
			float percent = 0;
			Gizmos.DrawSphere (oneSpline.GlobalPos2 (uT, width), sphereSize);
		}


		if (isUniformGlobalPosAdd) 
		{
			Vector3 point = Vector3.zero;
			float addPercent = 0;
			float _dist = 0;

			float t2 = oneSpline.GetStep (oneSpline.GetPointOnCurve (obj2.position));
			if (oneSpline.UniformGlobalPosAdd (t2, dist, out point, out addPercent, out _dist, isForward)) {
				Gizmos.color = Color.green;
				Vector3 pos = oneSpline.GlobalPos (t2);
				Gizmos.DrawSphere (pos, 2.5f);

				Gizmos.color = Color.magenta;
				Gizmos.DrawSphere (point, 2.5f);

				Gizmos.color = Color.blue;
				Gizmos.DrawLine (pos, pos + Vector3.up);
				Gizmos.DrawLine (pos + Vector3.up, point + Vector3.up);
				Gizmos.DrawLine (point, point + Vector3.up);
			} else {
				Debug.Log (addPercent + " " + _dist + "!");
				Gizmos.color = Color.green;
				Vector3 pos = oneSpline.GlobalPos (t2);
				Gizmos.DrawSphere (pos, 2.5f);

				//point = oneSpline.GlobalPos ((isForward) ? 1 : 0);
				Vector3 pos2 = oneSpline.GlobalPos ((isForward) ? 1 : 0) + (oneSpline.GetTangent (isForward ? 1 : 0) * (isForward ? 1 : -1)) * (dist - _dist);
				Gizmos.color = Color.red;
				Gizmos.DrawSphere (pos2, 2.5f);

				Gizmos.color = Color.blue;
				Gizmos.DrawLine (pos, pos + Vector3.up);
				Gizmos.DrawLine (pos + Vector3.up, pos2 + Vector3.up);
				Gizmos.DrawLine (pos2, pos2 + Vector3.up);


			}
		}


		*/
		}

		void Reset()
		{
			
			highWay = transform.GetComponent<Highway> ();
			if (highWay==null)
				oneSpline = transform.GetComponent<OneSpline> ();
		}

		// Update is called once per frame
		void Update ()
		{

			if (debugModule.Count < 1)
				Initialize ();

			/*
			if (oneSpline == null)
				

			if (oneSpline == null || oneSpline.type != type) {
				OneSpline[] splines = GetComponents<OneSpline> ();
				for (int i = 0; i < splines.Length; i++) {
					if (splines [i].type == type)
						oneSpline = splines [i];
				}
			}
*/



		}
	}
}
