﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Dest.Math;

#if UNITY_EDITOR
using UnityEditor;
#endif
public enum ForkCreatorState
{
	SphereSize = 0,
	CutSplines = 1,
	Next = 2,

}



public class ForkCreator : MonoBehaviour {

	public Vector3 center;
	public float radius = 10;
	[SerializeField]
	public OneSpline mainSpline;
	[SerializeField]
	public OneSpline secondSpline;

	public int mainStartSegment;
	public int mainEndSegment;
	public int secondEndSegment;

	public ForkCreatorState state;
	public Transform pidor1;
	public Transform pidor2;

	public GameObject Triangulator;
	[SerializeField]
	public ForkSpline centerSpline;
	[SerializeField]
	public ForkSpline leftSpline;
	[SerializeField]
	public ForkSpline rightSpline;

	public float splineDistance = 6.0f;
	public float leftSplineDistanceStart = 25.0f;
	public float leftSplineDistanceEnd = 25.0f;
	public float rightSplineDistanceStart = 25.0f;
	public float rightSplineDistanceEnd = 25.0f;


	[SerializeField]
	Vector3 mainStartPoint = Vector3.zero;
	[SerializeField]
	Vector3 secondStartPoint = Vector3.zero;
	[SerializeField]
	Vector3 mainEndPoint = Vector3.zero;
	[SerializeField]
	Vector3 secondEndPoint = Vector3.zero;
	[SerializeField]
	float mainStartStep = 0;
	[SerializeField]
	float mainEndStep = 0;
	[SerializeField]
	float secondStartStep = 0;
	[SerializeField]
	float secondEndStep = 0;

	public SplineType mainType;

	[ContextMenu("Start")]
	void OnEnable()
	{
		EditorEnable ();

	}

	public void EditorEnable()
	{
		state = ForkCreatorState.SphereSize;
		OneSpline[] splines = GetComponents<OneSpline> ();
		for (int i = 0; i < splines.Length; i++) 
		{
			if (splines [i].type == mainType)
				mainSpline = splines [i];
			else
				secondSpline = splines [i];
		}
	}

	[ContextMenu("Test Int")]
	public void TestIntersection()
	{
		Circle2 circle2 = new Circle2 (center.ToVector2XZ(), radius);
		Segment2 segment2 = new Segment2(pidor1.position.ToVector2XZ(), pidor2.position.ToVector2XZ());

		Segment2Circle2Intr sci = new Segment2Circle2Intr ();
		if (Intersection.FindSegment2Circle2 (ref segment2, ref circle2, out sci)) {
			/*
			GameObject sphere = GameObject.CreatePrimitive (PrimitiveType.Sphere);
			sphere.name = "Left Start ";
			sphere.transform.position = sci.Point0.ToVector3XZ();

			GameObject sphere2 = GameObject.CreatePrimitive (PrimitiveType.Sphere);
			sphere2.name = "Left Start 2 ";
			sphere2.transform.position = sci.Point1.ToVector3XZ();
			*/
		} else
			Debug.Log ("Not Intersection");
	}
	/*
	protected void DrawCircle(ref Circle2 circle)
	{
		int count = 40;
		float delta = 2f * Mathf.PI / count;
		Vector3 prev = circle.Eval(0);
		for (int i = 1; i <= count; ++i)
		{
			Vector3 curr = circle.Eval(i * delta);
			Gizmos.DrawLine(prev, curr);
			prev = curr;
		}
	}
*/
	void OnDrawGizmos()
	{
		/*
		Vector3 point = Vector3.zero;
		float startAddPercent = 0;
		float endAddPercent = 0;
		float dist = 0;

		Vector3 pointOnCurve = left.GetPointOnCurve (center);
		float t = left.GetStep (pointOnCurve);
		if (left.UniformGlobalPosAdd (t, radius, out point, out startAddPercent, out dist, false)) 
			left.NearestCircleOnCurveDraw(startAddPercent, center, radius, Color.blue);

		if (left.UniformGlobalPosAdd (t, radius, out point, out endAddPercent, out dist, true)) 
			left.NearestCircleOnCurveDraw(endAddPercent, center, radius, Color.red);

		if (right.UniformGlobalPosAdd (t, radius, out point, out startAddPercent, out dist, false)) 
			right.NearestCircleOnCurveDraw(startAddPercent, center, radius, Color.blue);

		if (right.UniformGlobalPosAdd (t, radius, out point, out endAddPercent, out dist, true)) 
			right.NearestCircleOnCurveDraw(endAddPercent, center, radius, Color.red);

		float leftUT = left.UniformGlobalLenght (left.segments [1].startTime);
		//Gizmos.color = Color.green;
		//Gizmos.DrawLine (left.GlobalPos2 (leftUT, 1), left.GlobalPos2 (leftUT, -1));
		//Gizmos.DrawLine (left.GlobalPos2 (leftUT, 1), left.GlobalPos2 (leftUT, 1) - left.GetTangent(leftUT) * 5f);
		//Gizmos.DrawLine (left.GlobalPos2 (leftUT, -1), left.GlobalPos2 (leftUT, -1) - left.GetTangent(leftUT) * 5f);

		Gizmos.color = Color.green;
		Gizmos.DrawLine (right.GlobalPos2 (right.segments [0].startTime, 1), right.GlobalPos2 (right.segments [0].startTime, -1));
		Gizmos.DrawLine (right.GlobalPos2 (right.segments [0].startTime, 1), right.GlobalPos2 (right.segments [0].startTime, 1) - right.GetTangent(right.segments [0].startTime) * 5f);
		Gizmos.DrawLine (right.GlobalPos2 (right.segments [0].startTime, -1), right.GlobalPos2 (right.segments [0].startTime, -1) - right.GetTangent(right.segments [0].startTime) * 5f);

		Gizmos.color = Color.yellow;
		Gizmos.DrawLine (left.GlobalPos2 (left.segments [2].startTime, 1), left.GlobalPos2 (left.segments [2].startTime, -1));
		Gizmos.DrawLine (left.GlobalPos2 (left.segments [2].startTime, 1), left.GlobalPos2 (left.segments [2].startTime, 1) + left.GetTangent (left.segments [2].startTime) * 5f);
		Gizmos.DrawLine (left.GlobalPos2 (left.segments [2].startTime, -1), left.GlobalPos2 (left.segments [2].startTime, -1) + left.GetTangent (left.segments [2].startTime) * 5f);

		Gizmos.color = Color.yellow;
		Gizmos.DrawLine (left.GlobalPos2 (left.segments [1].startTime, 1), left.GlobalPos2 (left.segments [1].startTime, -1));
		Gizmos.DrawLine (left.GlobalPos2 (left.segments [1].startTime, 1), left.GlobalPos2 (left.segments [1].startTime, 1) - left.GetTangent (left.segments [1].startTime) * 5f);
		Gizmos.DrawLine (left.GlobalPos2 (left.segments [1].startTime, -1), left.GlobalPos2 (left.segments [1].startTime, -1) - left.GetTangent (left.segments [1].startTime) * 5f);

		//Gizmos.DrawLine (right.GlobalPos (right.segments [1].startTime), right.GlobalPos (right.segments [1].startTime) + right.GetTangent (right.segments [1].startTime) * -10);
		//Gizmos.DrawLine (right.GlobalPos (right.segments [1].startTime, 1), right.GlobalPos (right.segments [1].startTime, 1) + right.GetTangent (right.segments [1].startTime, 1) * -10);
		//Gizmos.DrawLine (right.GlobalPos (right.segments [1].startTime, -1), right.GlobalPos (right.segments [1].startTime, -1) + right.GetTangent (right.segments [1].startTime, -1) * -10);
*/
	}

	void GetSplineDots(OneSpline spline, out Vector3 startPoint, out Vector3 endPoint)
	{
		Vector3 point = Vector3.zero;
		startPoint = Vector3.zero;
		endPoint = Vector3.zero;
		float startAddPercent = 0;
		float endAddPercent = 0;
		float dist = 0;

		Vector3 pointOnCurve = spline.GetPointOnCurve (center);
		float t = spline.GetStep (pointOnCurve);

		if (spline.UniformGlobalPosAdd (t, radius, 0, out point, out startAddPercent, out dist, false)) 
		{
			Dest.Math.Segment2Circle2Intr segmCircIntr;

			if (spline.NearestCircleOnCurve(startAddPercent, out segmCircIntr, center, radius, true)) 
				startPoint = segmCircIntr.Point0.ToVector3XZ ();
			else
				Debug.Log (spline.type + " Start Failure Curve");
		} 
		else
			Debug.Log (spline.type + " Start Failure");

		if (spline.UniformGlobalPosAdd (t, radius, 0, out point, out endAddPercent, out dist, true)) 
		{
			Dest.Math.Segment2Circle2Intr segmCircIntr = new Segment2Circle2Intr ();

			//GameObject sphere = GameObject.CreatePrimitive (PrimitiveType.Sphere);
			//sphere.name = "Main Start " + endAddPercent + " " + dist;
			//sphere.transform.position = point;

			//spline.NearestCircleOnCurveDraw (endAddPercent, center, radius, Color.white);
			if (spline.NearestCircleOnCurve (endAddPercent, out segmCircIntr, center, radius, false)) 
				endPoint = segmCircIntr.Point0.ToVector3XZ ();
			else
				Debug.Log (spline.type + " End Failure Curve");
		} else
			Debug.Log (spline.type + " End Failure");
	}

	public void CutSegment(OneSpline spline, float startStep, float endStep, out int endSegment)
	{
		endSegment = -1;

		bool both = false;

		int count = spline.segments.Count;
		for (int i = 0; i < count; i++) 
		{
			if (spline.segments [i].startTime < startStep && spline.segments [i].endTime >= startStep) 
			{
				if (spline.segments [i].startTime < endStep && spline.segments [i].endTime >= endStep) 
				{
					spline.segments [i].startTime = endStep;
					endSegment = i;
					spline.CalculateSegments ();
					spline.RecalculateOpenSegments ();
				} 
				else 
					if (spline.segments [i].endTime < endStep) 
						spline.segments [i].endTime = startStep;
			}
		}


		if (both == false) 
		{
			for (int i = 0; i < spline.segments.Count; i++) 
				if (spline.segments [i].startTime < endStep && spline.segments [i].endTime >= endStep) 
					spline.segments [i].startTime = endStep;
		}

		spline.EditorUpdate ();
	}

	public void CutSegment(OneSpline spline, float startStep, float endStep, out int endSegment, out int startSegment)
	{
		endSegment = -1;
		startSegment = -1;

		bool both = false;

		int count = spline.segments.Count;
		for (int i = 0; i < count; i++) 
		{
			if (spline.segments [i].startTime < startStep && spline.segments [i].endTime >= startStep) 
			{
				if (spline.segments [i].startTime < endStep && spline.segments [i].endTime >= endStep) 
				{
					float oldEndTime = spline.segments [i].endTime;
					spline.segments [i].endTime = startStep;
					startSegment = i;

					OneSplineSegment segment = new OneSplineSegment (endStep, oldEndTime, spline.segments [i]);
					spline.segments.Add (segment);
					endSegment = spline.segments.IndexOf(segment);
					spline.CalculateSegments ();
					spline.RecalculateOpenSegments ();

					RoadTemplate rt = new RoadTemplate (spline.extructions [0].transform, spline.extructions [0].roadTemplates.Count);
					spline.extructions [0].roadTemplates.Add(rt);
					spline.extructions [0].LoadRoadDefault (spline.extructions [0].roadTemplates.Count - 1);
					spline.extructions [0].roadTemplates [spline.extructions [0].roadTemplates.Count - 1].id = endSegment;
					spline.extructions [0].ChangeMaterial (spline.extructions [0].roadTemplates [spline.extructions [0].roadTemplates.Count - 1]);
					spline.extructions [0].GenerateMesh ();

				} 
				else 
					if (spline.segments [i].endTime < endStep) 
						spline.segments [i].endTime = startStep;
			}
		}


		if (both == false) 
		{
			for (int i = 0; i < spline.segments.Count; i++) 
				if (spline.segments [i].startTime < endStep && spline.segments [i].endTime >= endStep) 
					spline.segments [i].startTime = endStep;
		}
			
		spline.EditorUpdate ();

	}

	public void CreateDots()
	{

		GetSplineDots(mainSpline, out mainStartPoint, out mainEndPoint);
		GetSplineDots(secondSpline, out secondStartPoint, out secondEndPoint);


		GameObject sphere = GameObject.CreatePrimitive (PrimitiveType.Sphere);
		mainStartStep = mainSpline.GetStep (mainStartPoint);
		sphere.name = "Main Start " + mainStartStep;
		sphere.transform.position = mainStartPoint;

		sphere = GameObject.CreatePrimitive (PrimitiveType.Sphere);
		mainEndStep = mainSpline.GetStep (mainEndPoint);
		sphere.name = "Main End "+ mainEndStep;
		sphere.transform.position = mainEndPoint;

		sphere = GameObject.CreatePrimitive (PrimitiveType.Sphere);
		secondStartStep = secondSpline.GetStep (secondStartPoint);
		sphere.name = "Second Start "+ secondStartStep;
		sphere.transform.position = secondStartPoint;

		sphere = GameObject.CreatePrimitive (PrimitiveType.Sphere);
		secondEndStep = secondSpline.GetStep (secondEndPoint);
		sphere.name = "Second End "+ secondEndStep;
		sphere.transform.position = secondEndPoint;


	}

	public void CutSplinesStart()
	{

		mainStartStep = mainSpline.GetStep (mainStartPoint);
		secondStartStep = secondSpline.GetStep (secondStartPoint);
		CutSpline (mainSpline, mainStartStep);
		CutSpline (secondSpline, secondStartStep);
	}

	public void CutSplinesEnd()
	{
		mainEndStep = mainSpline.GetStep (mainEndPoint);
		secondEndStep = secondSpline.GetStep (secondEndPoint);
		CutSpline (mainSpline, mainEndStep);
		CutSpline (secondSpline, secondEndStep);
	}

	public void CutSegments()
	{
		GetSplineDots(mainSpline, out mainStartPoint, out mainEndPoint);
		GetSplineDots(secondSpline, out secondStartPoint, out secondEndPoint);

		mainStartStep = mainSpline.GetStep (mainStartPoint);
		secondStartStep = secondSpline.GetStep (secondStartPoint);
		mainEndStep = mainSpline.GetStep (mainEndPoint);
		secondEndStep = secondSpline.GetStep (secondEndPoint);
		Debug.Log (mainStartStep + " " +  mainEndStep);
		CutSegment (mainSpline, mainStartStep, mainEndStep, out mainEndSegment, out mainStartSegment);
		CutSegment (secondSpline, secondStartStep, secondEndStep, out secondEndSegment);
	}

	public void CutSpline(OneSpline spline, float step)
	{
		spline.InsertNode (step);
	}

	public void SetSpline(ref ForkSpline spline, SplineType type)
	{
		if (spline == null)
			spline = (ForkSpline)BlockPlane.GetSpline(Triangulator.transform, type);
		if (spline == null) {
			spline = Triangulator.AddComponent<ForkSpline> ();
		}
		spline.type = type;

		if (spline.segments == null || spline.segments.Count == 0)
		{
			spline.segments.Add (new OneSplineSegment ());
			spline.CalculateSegments ();
			spline.RecalculateOpenSegments ();
		}

		if (spline.extructions == null) {
			SplineExtrusion se = spline.AddExtrusion ();
			se.splineType = type;
			se.LoadRoadDefault (0);
			List<RoadPart> toRemove = new List<RoadPart> ();
			foreach (RoadPart roadPart in se.roadTemplates[0].roadParts)
			{
				if ((roadPart.type == ((type == SplineType.Right) ? RoadPartType.LeftSide : RoadPartType.RightSide))) {
					roadPart.pivot = 0;
				} else
					toRemove.Add (roadPart);
			}

			int count = toRemove.Count;
			for (int i=0; i<count; i++)
				se.roadTemplates [0].roadParts.Remove (toRemove[i]);
		} else {
			SplineExtrusion se = spline.extructions [0];

			se.splineType = type;
			se.LoadRoadDefault (0);
			List<RoadPart> toRemove = new List<RoadPart> ();
			foreach (RoadPart roadPart in se.roadTemplates[0].roadParts)
			{
				if ((roadPart.type == ((type == SplineType.Right) ? RoadPartType.LeftSide : RoadPartType.RightSide))) {
					roadPart.pivot = 0;
				} else
					toRemove.Add (roadPart);
				
			}


			int count = toRemove.Count;
			for (int i=0; i<count; i++)
				se.roadTemplates [0].roadParts.Remove (toRemove[i]);
		}

	}

	public void CreateTriangulateSurface()
	{
		if (Triangulator == null) 
		{
			Transform tr = transform.Find ("ForkTriangulator");
			if (tr != null)
				Triangulator = tr.gameObject;
		}

		if (Triangulator == null) 
		{
			Triangulator = new GameObject ();
			Triangulator.name = "ForkTriangulator";
			Triangulator.transform.parent = transform;
			Triangulator.transform.localPosition = Vector3.zero;
		}

		RoadTemplate mainTemplate = mainSpline.extructions [0].roadTemplates [mainEndSegment];
		RoadTemplate secondTemplate = secondSpline.extructions [0].roadTemplates [secondEndSegment];
		Vector3[] mainEnd = mainSpline.extructions [0].GenerateDots (mainTemplate, DotSelectionType.RoadStart);
		Vector3[] secondEnd = secondSpline.extructions [0].GenerateDots (secondTemplate, DotSelectionType.RoadStart);

		Vector3[] mainStart = mainSpline.extructions [0].GenerateDots (mainSpline.extructions [0].roadTemplates [mainStartSegment], DotSelectionType.RoadEnd);

		SetSpline (ref centerSpline, SplineType.Front);
		centerSpline.startData = new ForkSplineDotData(mainEnd [mainEnd.Length - 1], mainSpline, mainEndSegment, true);
		centerSpline.endData = new ForkSplineDotData(secondEnd [0], secondSpline, secondEndSegment, true);
		centerSpline.UpdateSpline (splineDistance, splineDistance);

		SetSpline (ref leftSpline, SplineType.Left);
		leftSpline.startData = new ForkSplineDotData(mainStart [0], mainSpline, mainStartSegment, false);
		leftSpline.endData = new ForkSplineDotData(mainEnd [0], mainSpline, mainEndSegment, true);
		leftSpline.UpdateSpline (-leftSplineDistanceStart, leftSplineDistanceEnd);

		SetSpline (ref rightSpline, SplineType.Right);
		rightSpline.startData = new ForkSplineDotData(mainStart [mainStart.Length-1], mainSpline, mainStartSegment, false);
		rightSpline.endData = new ForkSplineDotData(secondEnd [mainStart.Length-1], secondSpline, secondEndSegment, true);
		rightSpline.UpdateSpline (-rightSplineDistanceStart, rightSplineDistanceEnd);

		Vector3[] splineDots = centerSpline.extructions [0].GenerateDots (centerSpline.extructions [0].roadTemplates [0], DotSelectionType.RoadRight);
		Vector3[] leftDots = leftSpline.extructions [0].GenerateDots (leftSpline.extructions [0].roadTemplates [0], DotSelectionType.RoadRight);
		Vector3[] rightDots = rightSpline.extructions [0].GenerateDots (rightSpline.extructions [0].roadTemplates [0], DotSelectionType.RoadLeft);

		List<Vector2> vertex = new List<Vector2>();


		for (int i=mainStart.Length-2; i>=1; i--)
			vertex.Add(mainStart[i].ToVector2XZ());

		for (int i=0; i<leftDots.Length; i++)
			vertex.Add(leftDots[i].ToVector2XZ());

		for (int i=1; i<mainEnd.Length; i++)
			vertex.Add(mainEnd[i].ToVector2XZ());

		for (int i=1; i<splineDots.Length-1; i++)
			vertex.Add(splineDots[i].ToVector2XZ());

		//vertex.Add(Vector3.Lerp(leftBorderDotsCenter[0], leftBorderDotsCenter2[leftBorderDotsCenter2.Length-1], 0.5f).ToVector2XZ());

		for (int i=0; i<secondEnd.Length-1; i++)
			vertex.Add(secondEnd[i].ToVector2XZ());

		for (int i=rightDots.Length-1; i>=0; i--)
			vertex.Add(rightDots[i].ToVector2XZ());

		/*
		for (int i = 0; i < vertex.Count; i++) 
		{
			GameObject newGo = GameObject.CreatePrimitive (PrimitiveType.Sphere);

			newGo.transform.position = vertex[i].ToVector3XZ();
			newGo.name = i.ToString ();
		}
*/

		TriangulatorNew triangulator = new TriangulatorNew (vertex.ToArray());
		int[] indices = triangulator.Triangulate();

		Vector3[] vertices = new Vector3[vertex.Count];
		Vector3[] normals = new Vector3[vertex.Count];
		Vector2[] uv = new Vector2[vertex.Count];
		for (int i=0; i<vertices.Length; i++) {
			vertices [i] = vertex [i].ToVector3XZ ();
			normals[i] = Vector3.up;
			uv[i] = vertices[i].ToVector2XZ() / 7;
		}

		Mesh mesh = new Mesh ();
		mesh.vertices = vertices;
		mesh.normals = normals;
		mesh.uv = uv;
		mesh.triangles = indices;

		mesh.RecalculateNormals();
		mesh.RecalculateBounds ();
		mesh.UploadMeshData (true);


		MeshFilter MF = Triangulator.GetComponent<MeshFilter> ();
		if (MF == null)
		MF = Triangulator.AddComponent<MeshFilter> ();
		MF.sharedMesh = mesh;
		MF.sharedMesh = PrefabCollector.CreateOrReplaceAssetMesh (MF.sharedMesh, "Assets/" + Triangulator.name + ".asset");

		MeshRenderer MR = Triangulator.GetComponent<MeshRenderer> ();
		if (MR == null)
			MR = Triangulator.AddComponent<MeshRenderer> ();
		#if UNITY_EDITOR
		RoadTemplateAsset asset = AssetDatabase.LoadAssetAtPath<RoadTemplateAsset> ("Assets/RoadTemplates/Contexts/RoadDefault.asset");
		for (int i=0; i<asset.context.roadParts.Count; i++)
			if (asset.context.roadParts[i].type == RoadPartType.Road)
			{
				MR.sharedMaterial = asset.context.sourceMaterials[asset.context.roadParts[i].matId];
				break;
			}
		#endif

	}




}
