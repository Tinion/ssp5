﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[SelectionBase]
public class SplineRailing : MonoBehaviour {

	private struct Vertex {
		public Vector3 v;
		public Vector3 n;

	}

	private readonly List<Vertex> vertices = new List<Vertex>();

	public GameObject meshObject;

	public Mesh source;
    public Material material;
    public Vector3 rotation;
	public float widthOffset = 0;
	public Vector3 scale = Vector3.one;
    public float startTime = 0;
    public float endTime = 1;
	private float startScale = 1, endScale = 1;
	private Quaternion sourceRotation;
	private Vector3 sourceTranslation;
	public float zOffset = 0;
	public float density = 1;
	public MeshFilter MF;
	public MeshRenderer MR;

	public float startRoll = 0;
	public float endRoll = 1;

	public List<Vector3> debug = new List<Vector3>();
	public List<Vector3> debug2 = new List<Vector3>();
	float minX;
	float maxX;
	float length;
	float distanceLenght;
	int count;

    public SplineType splineType;
    [SerializeField]
    public OneSpline spline;

    public List<GameObject> meshes = new List<GameObject>();
    private bool toUpdate = true;

    private void OnEnable() {
        spline = BlockPlane.GetSpline(transform.parent, splineType);
    }

    private void OnValidate() {
        toUpdate = true;
    }

    private void Update() {
        if (toUpdate) {
            CreateMeshes();
            toUpdate = false;
        }
    }

	[ContextMenu("Set")]
	public void SetMesh()
	{
		//result = new Mesh();

		MeshFilter MF = GetComponent<MeshFilter> ();
		if (MF == null)
			MF = gameObject.AddComponent<MeshFilter> ();
		//MF.sharedMesh = result;
		SetSourceMesh (source);
	}

	public void SetSourceMesh(Mesh mesh, bool update = true) 
	{

			this.source = mesh;
			vertices.Clear();
			int i = 0;
			foreach (Vector3 vert in source.vertices) 
			{
				Vertex v = new Vertex();
				v.v = vert;
				v.n = source.normals[i++];
				vertices.Add(v);
			}


		Debug.Log (vertices.Count);
		if (update) Compute();

	}

	private void Compute()
	{
		vertices.Clear();
		int i = 0;
		foreach (Vector3 vert in source.vertices) 
		{
			Vertex v = new Vertex();
			v.v = vert;
			v.n = source.normals[i++];
			vertices.Add(v);
		}

		if (source == null)
			return;
		int nbVert = source.vertices.Length;

		minX = float.MaxValue;
		maxX = float.MinValue;
		foreach (Vertex vert in vertices) 
		{
			
			Vector3 p = vert.v;
			if (sourceRotation != Quaternion.identity) {
				p = sourceRotation * p;
			}
			if(sourceTranslation != Vector3.zero) {
				p += sourceTranslation;
			}
			maxX = Math.Max(maxX, p.x);
			minX = Math.Min(minX, p.x);
		}
		length = Math.Abs(maxX - minX);
		distanceLenght = spline.GetLenght(startTime, endTime);

		count = Mathf.CeilToInt (distanceLenght / length);

		debug = new List<Vector3>();
		debug2 = new List<Vector3>();
	}

	/*
	void OnDrawGizmos()
	{
		for (int i = 0; i < debug.Count; i++) {
			Gizmos.color = Color.green;
			Gizmos.DrawSphere (debug [i] + Vector3.up * 2, 0.3f);
			Gizmos.DrawLine (debug [i], debug [i] + Vector3.up * 3);
		}
		for (int i = 0; i < debug2.Count; i++) 
		{
			Gizmos.color = Color.red;
			Gizmos.DrawSphere (debug2 [i] + Vector3.up * 3, 0.2f);
			Gizmos.DrawLine (debug2 [i], debug2 [i] + Vector3.up * 3);
		}
	}
*/
	private Mesh BakeOne(float _startPosition, float _endPosition, int index)
	{
		List<Vector3> deformedVerts = new List<Vector3>();
		List<Vector3> deformedNormals = new List<Vector3>();
		//List<Color> deformedColors = new List<Color>();
		// for each mesh vertex, we found its projection on the curve

		//float lenghtFull = spline.GetLenght (startTime, endTime);
		//float step = length / lenghtFull;

		float uT = spline.UniformGlobalLenght (_startPosition);
		float uA = spline.UniformGlobalLenght (_endPosition);

		Vector3 start = spline.GlobalPos2 (uT, 0);
		Vector3 end = spline.GlobalPos2 (uA, 0);
		debug.Add (start);
		debug2.Add (end);

		foreach (Vertex vert in vertices) 
		{
			Vector3 p = vert.v;
			Vector3 n = vert.n;
			//Color c = Color.black;

			Quaternion sourceRot = Quaternion.Euler (rotation);
			if (sourceRot != Quaternion.identity) 
			{
				
				p = sourceRot * p;
				n = sourceRot * n;
			}

			if (zOffset != 0)
				p.y += zOffset;;


			Vector3 startTangent = spline.GetTangent(uT, 0);

			float lenghtCurr = (end-start).magnitude;

			float distanceRate = ((p.x * startTangent).magnitude)/lenghtCurr; //Math.Abs((p.x - start.x)/((end-start).magnitude));

			//c = Color.Lerp (Color.black, Color.red, distanceRate);

			float uZ = spline.UniformGlobalLenght (Mathf.Lerp(_startPosition, _endPosition, distanceRate));

			Vector3 curvePoint = spline.GlobalPos2(uZ, widthOffset);
			Vector3 curveTangent = spline.GetTangent(uZ, widthOffset);


			Quaternion q = OneCubicBezierCurve.GetRotationFromTangent(curveTangent) * Quaternion.Euler(0, 90, 0);

			p.x *= 0;
			p.y *= scale.y;
			p.z *= scale.z;

			// application of roll
			float rollAtDistance = startRoll + (endRoll - startRoll) * distanceRate;
			p = Quaternion.AngleAxis(rollAtDistance, Vector3.right) * p;
			n = Quaternion.AngleAxis(rollAtDistance, Vector3.right) * n;

			//p = Math3d.RotatePointAroundPivot(p, curvePoint, new Vector3(0,0,spline.GetTilt(uZ)));
			// reset X value of p
			//p = new Vector3(0, p.y, p.z);


			//Debug.Log ("Add");
			//deformedVerts.Add(p);
			//deformedNormals.Add(n);
			deformedVerts.Add(q * p + curvePoint);
			deformedNormals.Add(q * n);
			//deformedColors.Add (c);
		}

		Mesh result = new Mesh ();
		//Debug.Log (deformedVerts.Count + " " + source.uv.Length);
		result.vertices = deformedVerts.ToArray();
		result.normals = deformedNormals.ToArray();
		result.uv = source.uv;
		//result.colors = deformedColors.ToArray ();
		result.triangles = source.triangles;
		return result;
	}

    public void CreateMeshes()
    {
        foreach (GameObject go in meshes)
        {
            if (gameObject != null)
            {
                if (Application.isPlaying)
                {
                    Destroy(go);
                } else {
                    DestroyImmediate(go);
                }
            }
        }
        meshes.Clear();
        /*
        int i = 0;
        foreach (OneCubicBezierCurve curve in spline.curves) {
            GameObject go = new GameObject("SplineMesh" + i++, typeof(MeshFilter), typeof(MeshRenderer), typeof(MeshBender), typeof(MeshCollider));
            go.transform.parent = transform;
            go.transform.localRotation = Quaternion.identity;
            go.transform.localPosition = Vector3.zero;
            go.transform.localScale = Vector3.one;
            //go.hideFlags = HideFlags.NotEditable;

            go.GetComponent<MeshRenderer>().material = material;

            MeshBender mb = go.GetComponent<MeshBender>();
            mb.SetSourceMesh(mesh, false);
            mb.SetRotation(Quaternion.Euler(rotation), false);
            mb.SetTranslation(new Vector3(0, YOffset, ZOffset), false);
            mb.SetCurve(curve, false);
            mb.SetStartScale(scale, false);
            mb.SetEndScale(scale);
            //mb.SetStartRoll(15, false);
            //mb.SetEndRoll(-15);
            meshes.Add(go);
        }
        */

		float lenghtFull = spline.GetLenght (startTime, endTime);
		Compute ();
		List<CombineInstance> combine = new List<CombineInstance> ();
		float step = (length / lenghtFull) * (endTime-startTime) * density;
		//Debug.Log (length + " " + lenghtFull + " " + step + " " + startTime + " " + endTime);
        int ch = 0;


		float next = 0;
		for (float t = startTime; t <= endTime; t += step)
        {
			
			next = t + step;
			if (next > endTime)
				next = endTime;
				

			CombineInstance combined = new CombineInstance();
			//float uO = spline.UniformGlobalLenght (t);
			//Debug.Log (ch + " " + t + " " + next.ToString());
			combined.mesh = BakeOne(t, next, ch);
			ch++;
			//Vector3 curveTangent = spline.GetTangent(t);
			//Quaternion q = OneCubicBezierCurve.GetRotationFromTangent(curveTangent) * rotation;
			//combined.transform = Matrix4x4.TRS(spline.GlobalPos2(t, widthOffset), q, scale);
			combine.Add(combined);
			/*
			Mesh mesh = new Mesh();
            ch++;
            GameObject go = new GameObject("SplineMesh" + ch, typeof(MeshFilter), typeof(MeshRenderer), typeof(MeshBender), typeof(MeshCollider));
            go.transform.parent = transform;
            go.transform.localRotation = Quaternion.identity;
            go.transform.localPosition = Vector3.zero;
            go.transform.localScale = Vector3.one;
            //go.hideFlags = HideFlags.NotEditable;

            go.GetComponent<MeshRenderer>().material = material;

            MeshBender mb = go.GetComponent<MeshBender>();
            mb.SetSourceMesh(mesh, false);
            mb.SetRotation(Quaternion.Euler(rotation), false);
           //mb.SetTranslation(new Vector3(0, YOffset, ZOffset), false);
            mb.SetSpline(spline, false);
            mb.SetStartScale(scale, false);
            mb.SetEndScale(scale);
            mb.SetTime(t, t + spline._step_n);
            //mb.SetStartRoll(15, false);
            //mb.SetEndRoll(-15);
            meshes.Add(go);
            */
        }

		/*
		if (next < 1) 
		{
			CombineInstance combinedFinal = new CombineInstance ();
			Debug.Log ("Final" + " " + last + " " + endTime);
			combinedFinal.mesh = BakeOne (last, endTime, 0);
			combine.Add (combinedFinal);
		}
*/
		if (meshObject == null) 
		{
			meshObject = new GameObject ();
			meshObject.name = "RailingMesh";
			meshObject.transform.parent = transform;
			MF = meshObject.AddComponent<MeshFilter> ();
			MR = meshObject.AddComponent<MeshRenderer> ();


		}

		Mesh mesh = new Mesh ();
		mesh.name = "Baked";
		mesh.CombineMeshes (combine.ToArray(), true, false);
		if (MF == null)
			MF = meshObject.GetComponent<MeshFilter> ();
		MF.sharedMesh = mesh;
		if (MR == null)
			MR = meshObject.GetComponent<MeshRenderer> ();
		MR.sharedMaterial = material;
    }
}
