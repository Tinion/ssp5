﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SerializeField, AddComponentMenu("")]
public class RoadTemplateAsset : ScriptableObject {

	[SerializeField]
	public RoadTemplate context;
}
