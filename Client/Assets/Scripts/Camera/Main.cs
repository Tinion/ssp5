﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Dest.Math;

#if UNITY_EDITOR
using UnityEditor;
#endif


[System.Flags]
public enum Mode
{
    Free = 0,
    Battle  = 1,
    Boss = 2,
    Global = 3,
	Dialog = 4,
	DialogGlobal = 5,
    Base = 6,
    BaseToBattle = 7,
    Score = 8,
	EnterToHangar = 9,
}


[System.Flags]
public enum HeroReturnState
{
    Deactive = 0,
    Active = 1,
    FindPlace = 2,
    Return = 3,

}

[System.Serializable]
public class ObjectPoolSystem<T> where T : MapObject
{
    public int types;
    private int ch = 0;
    public Transform pool;
    public ObjectPool<T>[] Objects;

    public void Initialize(T _obj, string name, int count)
    {
        Objects[ch] = new ObjectPool<T>();
        Objects[ch].CreatePool(count, _obj, pool);
        ch++;
    }

    public void DestroyObject(T obj, string objectName)
    {
        for (int i = 0; i < types; i++)
        {
            if (objectName == Objects[i].poolName)
                Objects[i].DestroyObjectPool(obj);
        }
    }

    public T GetPooled(string objectName)
    {
        for (int i = 0; i < types; i++)
        {
			if (objectName.ToLower() == Objects[i].poolName.ToLower())
                return Objects[i].GetObject();
        }
        return null;
    }
}

[System.Serializable]
public class ObjectPool<T> where T : MapObject
{
    public List<T> PooledObjects;
    public string poolName = "";

    public void CreatePool(int size, T prefab, Transform _parent)
    {
		poolName = prefab.nominal.ToLower();
        PooledObjects = new List<T>();

        for (int i = 0; i < size; i++)
        {
            T obj = (T)Object.Instantiate(prefab);
            obj.Initialize(false);
            obj.transform.parent = _parent;
            obj.currentGameObject.SetActive(false);
            obj.name = prefab.name;
            PooledObjects.Add(obj);
        }
    }


    public T GetObject()
    {
        for (int i = 0; i < PooledObjects.Count; i++)
            if (!PooledObjects[i].currentGameObject.activeInHierarchy)
                return PooledObjects[i];
        return null;
    }

    public void DestroyObjectPool(T obj)
    {
        PooledObjects.Add(obj);
        obj.currentGameObject.SetActive(false);
    }
    
}

[System.Serializable]
public class HeroReturn
{
    //private CounterTimerF timer;
    public HeroReturnState state;

    private Mode mode;
    private OneSpline splineReturn;
    private float percentReturn;
    private Vector3 roadDirection;

    private Vector3 returnPosition = Vector3.zero;
    private Vector3 returnRotation = Vector3.zero;

    public HeroReturn()
    {
        state = HeroReturnState.Deactive;
        //timer.max = 0.0f;
    }

    public void SetReturnData(Vector3 retPos, Vector3 retRot)
    {
        returnPosition = retPos;
        returnRotation = retRot;
    }


    public void Activate()
    {
        if (state == HeroReturnState.Deactive)
        {
            state = HeroReturnState.Active;
			splineReturn = Main.instance.mainCamera.GetSpline(Main.instance.hero.transform.position);

			percentReturn = splineReturn.GetStep(Main.instance.hero.transform.position);// -0.3f;
			Debug.Log(splineReturn.transform.name + " return " + percentReturn.ToString() + " percent");

        } 
    }


    private bool CheckCurrentPlace(OneSpline _splineReturn, float _percentReturn)
    {
        returnPosition = _splineReturn.GlobalPos(_percentReturn);

		roadDirection = _splineReturn.GetTangent(_percentReturn);
        float angle = Math3d.GetAngle(roadDirection.z, roadDirection.x, Vector3.forward.z, Vector3.forward.x);
        returnRotation = new Vector3(0.0f, -angle, 0.0f);

		SetReturnData(returnPosition, returnRotation);
		return false;

        Vector2 data = new Vector2(3.0f, 29.0f);
        Vector2 rX = Math3d.RotateThis2d(new Vector3(1.5f, 0.0f, 14.5f), -angle, returnPosition);
        Vector2 lX = Math3d.RotateThis2d(new Vector3(-1.5f, 0.0f, 14.5f), -angle, returnPosition);
        Vector2 rY = Math3d.RotateThis2d(new Vector3(1.5f, 0.0f, -14.5f), -angle, returnPosition);
        Vector2 lY = Math3d.RotateThis2d(new Vector3(-1.5f, 0.0f, -14.5f), -angle, returnPosition);
        
        Box2 current = Box2.CreateFromPoints(new Vector2[] { rX, lX, rY, lY });

		for (int i = 0; i < Main.instance.unitObjects.Count; i++)
        {
			if (TrafficController.IntersectBounds (Main.instance.unitObjects [i], current, true)) 
			{
				
				return true;
			}
        }

		for (int i = 0; i < Main.instance.miscObjects.Count; i++)
        {
			if (TrafficController.IntersectBounds (Main.instance.miscObjects [i], current, true)) 
			{
				
				return true;
			}
            
       }
        
		SetReturnData(returnPosition, returnRotation);
        return false;
    }

    private void CheckPlace()
    {
		//state = HeroReturnState.FindPlace;
		//return;

        if (!CheckCurrentPlace(splineReturn, percentReturn))
        {
            Debug.Log("CheckNow");
            state = HeroReturnState.FindPlace;
        }
        else
        {
            Debug.Log("SplineMove " + percentReturn);
            if (percentReturn - splineReturn._step_n > 0.3f)
            {
                percentReturn -= splineReturn._step_n + 0.3f;
            }
            else
            {
                Debug.Log("ForkMove");
                int localFork = splineReturn.transform.GetComponent<BlockPlane>().localForkNumber;
				BlockPlane[] planes = GameObject.FindObjectsOfType<BlockPlane> ();
                for (int i = 0; i < planes.Length; i++)
                {
                    if (planes[i].localForkNumber == (localFork - 1))
                    {
                        splineReturn = planes[i].spline;
                        percentReturn = 1.0f;
                    }
                }
            }
        }
    }

   

    private void SetNewHeroPose()
    {
        returnPosition.y += 2.00f;

		Main.instance.hero.vehicle.currentRigidbody.velocity = Vector3.zero;
		Main.instance.hero.vehicle.currentRigidbody.angularVelocity = Vector3.zero;
		Debug.Log (returnPosition);
		Main.instance.hero.vehicle.transform.position = returnPosition;
		Main.instance.hero.vehicle.transform.rotation = Quaternion.Euler(returnRotation);

		if (Main.instance.hero.vehicle.Snap())
        {
			Main.instance.hero.vehicle.currentRigidbody.velocity = Main.instance.hero.transform.forward;
        }
        else
            Debug.LogWarning("Snap ERROR " + returnPosition);

		//Main.instance.mainCamera.SetHeroPos();
        state = HeroReturnState.Deactive;
      
    }

    public void UpdateThis()
    {
		
        switch (state)
        {
            case HeroReturnState.Deactive:
                    return;
                break;

            case HeroReturnState.Active:
                {
                    CheckPlace();
                }
                break;
            case HeroReturnState.FindPlace:
                {
                    SetNewHeroPose();
                }
                break;
       }


    }
            
    
}

[System.Serializable]
public class EnemyUI
{
    public UnityEngine.UI.Slider EnemyHealthBar;
    public UnityEngine.UI.Image EnemyHealthBarBackground; 
}


public class Main : MonoBehaviour
{
	public static Main instance = null;

    public Transform transform;
    public ResourcesDatabase resourcesDB;
    public LevelData levelDB;
	public OutpostPlane currentOutpost;

    public Hero hero;
    public MainCamera mainCamera;

    public BlockPlane currentPlane;
	public Hangar hangar;
    public Generator generator;
    public GlobalMap globalMap;
    public Transform Pool;
    public VacuumShaders.CurvedWorld.CurvedWorld_Controller curveController;
	public Light editorLight;

    public BaseUI baseRect;
    public BattleUI battleRect;
    public ScoreUI scoreUI;

    public PlaneSplinesController planeSplinesController;

    public Inventory inventory;
    public ScreenFader screenFader; //внедрить
    public MiniMapRenderer miniMapCam;

    public ObjectPoolSystem<Unit> EnemyPoolSystem;
    public ObjectPoolSystem<Misc> MiscPoolSystem;
    public ObjectPoolSystem<BlockPlane> BlockPoolSystem;

    public List<BlockPlane> BlockPlanes;
    public bool isAll;

    public float passedPath;
    public int passedLocalNumber;

	public SupportPoint supportPoint;
	public float supportStep;

    public float ResultTimeF;
    public float ResultPathF;
    public float ExpF;
    public float ResultF;
    public float ResultFex;

    public GameObject dirLight;
    public int currentMap;

    public HeroReturn heroReturn;

    public Vector3 heroStartPos;
    public Vector3 heroStartRot;

    public float blocksYposition = 0;
    
    public Mode gameMode;
    public float gameSpeed = 1.0f;
    public float guiSize = 1.0f;

    public Unit guiTarget;
    public Unit destroyWheelTarget;
    public Unit destroyBodyTarget;

	public float globalTime;
	public int day;
	public int mount;
	public int year;

    public bool isTouchPad = true;
    public bool freeIn = false;
    public bool isMenu = false;
    public bool isGenerator;
    public bool speedFactorActive;

    public Object spawnDotSource;

    public int usedMarkGate = -1; //проверить

    private string levelName;
    public bool isConnect = false;

    public int blockCountLevel = 5;
    public float currentBlock;

    public int forkRun = 0; //объединить
    public int planeRun;

    //public List<SpawnDot> spawnDots;
    //public List<Misc> miscs;//к хуям

    public EnemyUI[] enemyUI;
    
    public SplineType reverseLine;
    
    public List<Unit> unitObjects;
	public List<Misc> miscObjects;

    public List<Unit> activeUnitObjects;
    public List<Unit> activeTargetUnits;
	public List<Misc> activeMiscObjects;
	public Dialogue dialog;
	public GameObject UIFree;
    public bool isHeroFreeBoost;

    public CounterTimerF bonusTimer;

    public Object coinEnemy;
    public Object healEnemy;

    public Transform door;

    public Transform spawnFolder; //к хуям

    public TireMarksRenderer currentTireRenderer;
    public TireMarksRenderer oilRenderer;
    
    public Image destroyWheelIcon;
    public Image destroyBodyIcon;
    public Text ResultPath;
    public Text ResultTime;
    public Text Exp;
    public Text Result;
    public Text Coins;
    public Slider weaponSlider;

    public OilDot sourceOilDot;
    public List<OilDot> oilDots;
    public float oilSize;

    public StartController startController;

    public int moneyCount = 15000;
    public int repairCount;

    public bool isLevelEndActivate = false;

    public BonusElements bonusElements;

    public int _temp = 0;
    public int _temp2 = 0;
    public void AddOilDot(Vector3 pos)
    {
        if (sourceOilDot==null)
        {
            OilThrower ot = hero.moduleSystem.module[(int)ModuleSlot.Back].GetComponent<OilThrower>();
            sourceOilDot = ot.sourceOilDot;
            oilSize = ot.oilSize;
        }
        OilDot newOilDot = Instantiate<OilDot>(sourceOilDot);
        newOilDot.transform.position = pos;
        oilDots.Add(newOilDot);
    }
    void Start()
    {
		instance = this;

		curveController.enabled = true;
		editorLight.gameObject.SetActive (false);
        hero = FindObjectOfType<Hero>();
        mainCamera = FindObjectOfType<MainCamera>();

        levelName = Application.loadedLevelName;

        planeRun = 0;

        ResultTimeF = 0;
        ResultPathF = 0;

        currentBlock = 0.0f;
        resourcesDB = Resources.Load(ResourcesDatabase.assetName, typeof(Object)) as ResourcesDatabase;
        levelDB = Resources.Load(levelName, typeof(Object)) as LevelData;

        EnemyPoolSystem = new ObjectPoolSystem<Unit>();
        EnemyPoolSystem.pool = Pool.Find("Units");

        EnemyPoolSystem.types = 12;
        EnemyPoolSystem.Objects = new ObjectPool<Unit>[EnemyPoolSystem.types];
		EnemyPoolSystem.Initialize((Unit)GetVehicle("Civilian"), "Civilian".ToLower(), 8);
		EnemyPoolSystem.Initialize((Unit)GetVehicle("Roadhog"), "Roadhog".ToLower(), 8);
		EnemyPoolSystem.Initialize((Unit)GetVehicle("Scorcher"), "Scorcher".ToLower(), 8);
		EnemyPoolSystem.Initialize((Unit)GetVehicle("BattleVan"), "BattleVan".ToLower(), 8);
		EnemyPoolSystem.Initialize((Unit)GetVehicle("Baggy"), "Baggy".ToLower(), 8);
		EnemyPoolSystem.Initialize((Unit)GetVehicle("Bomber"), "Bomber".ToLower(), 8);
		EnemyPoolSystem.Initialize((Unit)GetVehicle("FlameTruck"), "FlameTruck".ToLower(), 8);
		EnemyPoolSystem.Initialize((Unit)GetVehicle("Civilian01"), "Civilian01".ToLower(), 8);
		EnemyPoolSystem.Initialize((Unit)GetVehicle("Civilian02"), "Civilian02".ToLower(), 8);
		EnemyPoolSystem.Initialize((Unit)GetVehicle("Civilian03"), "Civilian03".ToLower(), 8);
		EnemyPoolSystem.Initialize((Unit)GetVehicle("Plasmatron"), "Plasmatron".ToLower(), 8);
		EnemyPoolSystem.Initialize((Unit)GetVehicle("Gunner"), "Gunner".ToLower(), 8);

		//for (int i=0; i<EnemyPoolSystem.Objects.Length; i++)
		//	Debug.Log(EnemyPoolSystem.Objects[i].poolName);


		MiscPoolSystem = new ObjectPoolSystem<Misc>();
        MiscPoolSystem.pool = Pool.Find("Props");

        MiscPoolSystem.types = 1;
		MiscPoolSystem.Objects = new ObjectPool<Misc>[MiscPoolSystem.types];
		//MiscPoolSystem.Initialize((Misc)GetMisc("Barrels"), "Barrels".ToLower(), 8);
		//MiscPoolSystem.Initialize((Misc)GetMisc("CoinsLine"), "CoinsLine".ToLower(), 2);
		//MiscPoolSystem.Initialize((Misc)GetMisc("Column"), "Column".ToLower(), 8);
		//MiscPoolSystem.Initialize((Misc)GetMisc("JazzParts"), "JazzParts".ToLower(), 2);
		//MiscPoolSystem.Initialize((Misc)GetMisc("JazzParts02"), "JazzParts02".ToLower(), 2);
		//MiscPoolSystem.Initialize((Misc)GetMisc("Puddle"), "Puddle".ToLower(), 2);
		//MiscPoolSystem.Initialize((Misc)GetMisc("Puddle02"), "Puddle02".ToLower(), 2);
		MiscPoolSystem.Initialize((Misc)GetMisc("Board"), "Board".ToLower(), 8);
		//MiscPoolSystem.Initialize((Misc)GetMisc("Wall"), "Wall".ToLower(), 8);
		//MiscPoolSystem.Initialize((Misc)GetMisc("Wall02"), "Wall02".ToLower(), 8);

        BlockPoolSystem = new ObjectPoolSystem<BlockPlane>();
        BlockPoolSystem.pool = Pool.Find("Blocks");

		BlockPoolSystem.types = resourcesDB.blocks.Count;
        BlockPoolSystem.Objects = new ObjectPool<BlockPlane>[BlockPoolSystem.types];

		for (int i = 0; i < resourcesDB.blocks.Count; i++) 
		{
			BlockPoolSystem.Initialize ((BlockPlane)GetBlock (resourcesDB.blocks [i].name), resourcesDB.blocks [i].name, 4);
		}

        PrepareAvoidanceOcclusion();

        heroReturn = new HeroReturn();
        heroReturn.SetReturnData(new Vector3(-90, 0, -90), Vector3.zero);

        BlockPlanes.AddRange(FindObjectsOfType<BlockPlane>());

        UpdateMoney();
        baseRect.durabilityInfo.CreateDurabilityInfo();

		mainCamera.SetBasePos ();
    }

    public void GetExp(int exp, Vector3 _transform)
    {
        bonusElements.SetCoins(exp);
        Vector3 pos = mainCamera.roadAffectCamera.WorldToScreenPoint(_transform);
        Coins.rectTransform.position = pos;
        Coins.text = exp.ToString("F0");
        bonusTimer.Restore();
    }

	public void EditorAwake()
	{
		instance = this;
	}

    public void PrepareStart()
    {
		for (int i = 0; i < hero.vehicle.wheels.Length; i++)
			hero.vehicle.wheels[i].collider.brakeTorque = 0;
		
		generator.CreateMap ();

        StartPlacement();

		blockCountLevel = generator.currentMap.blockCount;

		PreparingRotationInHangar ();

        baseRect.currentGameObject.SetActive(false);
        battleRect.currentGameObject.SetActive(true);
        

        AmmoSource[] ammo = FindObjectsOfType<AmmoSource>();
        int count = ammo.Length;
        for (int i = 0; i<count; i++)
        {
            Destroy(ammo[i].gameObject);
        }

        battleRect.durabilityInfo.CreateDurabilityInfo();

       // mainCamera.mode = CamMode.Battle;

        for (int i = 0; i < 5; i++)
        {
            if (hero.moduleSystem.module[i] != null)
                hero.moduleSystem.module[i].Reset();
        }
        
    }

    public void OpenGlobal()
    {
        gameMode = Mode.Global;
        baseRect.currentGameObject.SetActive(false);
        globalMap.gameObject.SetActive(true);
        globalMap.Open();
        globalMap.mode = GlobalMapStatus.Wait;
    }
    public void PrepareBattle()
    {
		
        gameMode = Mode.Battle;
		mainCamera.OnBattleCam (CamMode.Battle);

		//mainCamera.mode = CamMode.Battle;
        hero.moduleSystem.CalculateCooldownIcons();
        
    }
    public void Lose()
    {
       // GameObject.Find("Finish").GetComponent<GUIText>().text = "YOU DIE!";
    }
    public void GetHeal(Vector3 _transform)
    {
        hero.Heal();
        Vector3 pos = mainCamera.roadAffectCamera.WorldToScreenPoint(_transform);
        Coins.rectTransform.position = pos;
        Coins.text = "Repaired";
        bonusTimer.Restore();
        //Debug.Log("Heal");
    }
    
    void PrepareAvoidanceOcclusion()
    {
        unitObjects = new List<Unit>();
		miscObjects = new List<Misc>();

        for (int i = 0; i < EnemyPoolSystem.types; i++)
            for (int j = 0; j < EnemyPoolSystem.Objects[i].PooledObjects.Count; j++)
            {
                unitObjects.Add(EnemyPoolSystem.Objects[i].PooledObjects[j].GetComponent<Unit>());
                if (EnemyPoolSystem.Objects[i].poolName == "Trailer")
                    unitObjects.Add(unitObjects[unitObjects.Count - 1].GetComponent<Trailer>().wagon);
            }

       //for (int i = 0; i < MiscPoolSystem.types; i++)
       //     for (int j = 0; j < MiscPoolSystem.Objects[i].PooledObjects.Count; j++)
       //     {
       //         miscObjects.Add(MiscPoolSystem.Objects[i].PooledObjects[j].GetComponent<Misc>());
       //     }
    }

    public void PreparingRotationInHangar()
    {
        
		//mainCamera.PreparingRotationInHangar ();
	}

	public void ActivateFreeMode()
	{
		gameMode = Mode.Free;
		mainCamera.transform.position = hero.transform.position;
		mainCamera.transform.LookAt (hero.transform.position);
		mainCamera.playerAffectCamera.transform.localPosition = Vector3.zero;
		mainCamera.playerAffectCamera.transform.localRotation = Quaternion.identity;
		mainCamera.roadAffectCamera.transform.localRotation = Quaternion.identity;
		mainCamera.roadAffectCamera.transform.localPosition = Vector3.zero;
		mainCamera.mode = CamMode.Free;
		scoreUI.Reset ();
		battleRect.currentGameObject.SetActive(false);
		scoreUI.gameObject.SetActive(false);
		battleRect.currentGameObject.SetActive(true);

	}


	public void EndMission()
	{
        if (gameMode == Mode.Battle)
        {
            gameMode = Mode.Score;
            mainCamera.ToEndLevel();
			hero.vehicle.SetSpline (supportPoint.spline);
            battleRect.currentGameObject.SetActive(false);

            for (int i = 0; i < 5; i++)
            {
                if (hero.moduleSystem.module[i] != null)
                    hero.moduleSystem.module[i].Reset();
            }

            scoreUI.gameObject.SetActive(true);
            scoreUI.StartAnim();
            scoreUI.transform.Find("Button").GetComponent<RectTransform>().SetAsLastSibling();
            UpdateMoney();
            
        }
        
	}

    public void UpdateMoney()
    {
        inventory.moneyCapacity.text = "Баланс: " + moneyCount.ToString() + " руб.";
        repairCount = hero.moduleSystem.GetRepairCost();
        inventory.repairButton.text = repairCount.ToString();
    }
   
	public void EnterToHanger()
	{
		gameMode = Mode.EnterToHangar;

	}

    public void ReturnToBase()
    {
        gameMode = Mode.Base;
        currentTireRenderer.Reset();
        oilRenderer.Reset();

        scoreUI.Reset();
		hero.vehicle.currentRigidbody.velocity = Vector3.zero;
		hero.vehicle.currentRigidbody.angularVelocity = Vector3.zero;

		hero.vehicle.ApplyColliders(0.0f, 0.0f, 0.0f);

		Transform parentOfhero = hero.vehicle.transform.parent;
		hero.vehicle.transform.parent = hangar.transform;
		hero.vehicle.transform.localPosition = heroStartPos;

		Debug.Log (hero.transform.position);

		hero.transform.localRotation = Quaternion.Euler (heroStartRot);
		hero.transform.parent =  parentOfhero;

        for (int i = 0; i < hero.vehicle.wheels.Length; i++)
			hero.vehicle.wheels[i].collider.brakeTorque = Mathf.Infinity;

		door.localPosition = new Vector3(15.31896f, 2.77f, 0.12f);
		mainCamera.SetBasePos ();
        //baseCamera.ReturnToStartPos();

        battleRect.currentGameObject.SetActive(false);
        scoreUI.gameObject.SetActive(false);
        baseRect.currentGameObject.SetActive(true);

        int count = BlockPlanes.Count;
        for (int i = 0; i < count; i++)
        {
            BlockPlanes[i].DestroyThis();
        }

        mainCamera.mode = CamMode.Base;
        mainCamera.transform.position = new Vector3(0.0f, 17.0f, -0.56f);
        mainCamera.transform.rotation = Quaternion.identity;

        blockCountLevel = 5;
        currentBlock = 0;
        forkRun = 0; //объединить
        planeRun = 0;
        blocksYposition = 0;

		//if (generator.map<generator.mapData.maps.Count-1)
        Destroy(dirLight);

        generator.GeneratorTime = 0;
        generator.currentEnemyTime[0] = 0;
        generator.currentEnemyTime[1] = 0;
        generator.currentEnemyTime[2] = 0;
        generator.currentMiscTime = 0;
        generator.currentBonusTime = 0;

        hero.moduleSystem.UpdateIcons();

        count = BlockPlanes.Count;
        for (int i = 0; i < count; i++)
            BlockPlanes[i].DestroyThis();

        BlockPlanes.Clear();
        isLevelEndActivate = false;

        

        passedLocalNumber = 0;
        passedPath = 0;
        currentBlock = 0;

        inventory.shop.UnLoadBig();

        if (!isAll)
        inventory.shop.LoadRandomBig(7, 3);
        else
            inventory.shop.LoadRandomBig(inventory.items.Length, 2);

        

        Unit[] killedList = activeUnitObjects.ToArray();
        count = killedList.Length;
        for (int i = 0; i < count; i++)
            killedList[i].Kill();

        activeUnitObjects.Clear();

		hangar.sc.isCheck = false;
		hangar.sc.sctype = StartControllerType.FreeActivator;

        baseRect.durabilityInfo.CreateDurabilityInfo();
        
    }

    void StartPlacement()
    {
//		Debug.Log (generator.currentMap.idStartBlock);
		string startOutpostName = "";
		for (int i = 0; i < generator.biomeSources.biomesSource[generator.currentMap.activeSeasons [0].biomeNumber].roadSource.Count; i++)
			if (generator.biomeSources.biomesSource [generator.currentMap.activeSeasons [0].biomeNumber].roadSource[i].roadSourceStyle == RoadSourceStyle.Outpost) 
			{
				startOutpostName = generator.biomeSources.biomesSource [generator.currentMap.activeSeasons [0].biomeNumber].roadSource [i].name;
			}

		Debug.Log (generator.biomeSources.biomesSource[generator.currentMap.activeSeasons [0].biomeNumber].roadSource[0].name);
		currentOutpost = (OutpostPlane)BlockPoolSystem.GetPooled(startOutpostName);
		currentOutpost.currentGameObject.SetActive(true);
		currentOutpost.transform.position = new Vector3(0.0f, 0.0f, 0.0f);
		currentOutpost.transform.rotation = Quaternion.identity;
        
		currentOutpost.isStarting = true;
		currentOutpost.localNumber = 0;
		currentOutpost.localForkNumber = -2;
		currentOutpost.canCreateNextPlane = false;
		currentOutpost.SelectSupportPoint (Random.Range (0, currentOutpost.supportPoints.Length),true);//, true);


		BlockPlane secondBlock = BlockPoolSystem.GetPooled(generator.biomeSources.biomesSource[generator.currentMap.activeSeasons [0].biomeNumber].roadSource[0].name);
        secondBlock.currentGameObject.SetActive(true);
		secondBlock.transform.position = currentOutpost.currentSupportPoint.supportPosition;
		secondBlock.transform.rotation = Quaternion.Euler (new Vector3 (0, currentOutpost.currentSupportPoint.supportAngle, 0));

        secondBlock.localNumber = 0.5f;
        secondBlock.localForkNumber = -1;
        secondBlock.canCreateNextPlane = true;

		currentOutpost.nextBlock = secondBlock;
		secondBlock.previousBlock = currentOutpost;

        currentPlane = secondBlock;
		BlockPlanes.Add(currentOutpost);
        BlockPlanes.Add(secondBlock);

		if (secondBlock.bordersHub != null) 
		{
			secondBlock.ReCalculateBorderData ();
			secondBlock.BakeBorders ();
		}
    }
    
    void Update()
	{
		Time.timeScale = gameSpeed;
		/*
		List<Vector4> InteractPosition = new List<Vector4> ();
		InteractPosition.Add (new Vector4(hero.transform.position.x, hero.transform.position.y, hero.transform.position.z, 0));
		int count = 1;
		for (int i = 0; i < 9; i++) 
		{
			if (activeUnitObjects.Count > i) 
			{
				InteractPosition.Add (new Vector4(activeUnitObjects [i].transform.position.x, activeUnitObjects [i].transform.position.y, activeUnitObjects [i].transform.position.z, 0));
				count++;
			}
			else
				InteractPosition.Add (new Vector4 (0, 0, 0, 0));
			
		}

		Shader.SetGlobalVectorArray("_InteractPosition", InteractPosition);
		Shader.SetGlobalInt ("_InteractCount", count);
		*/
		Shader.SetGlobalVector("_InteractPosition", new Vector4(hero.transform.position.x, hero.transform.position.y, hero.transform.position.z, 2.0f));

		globalTime += Time.deltaTime;
		if (globalTime >= 86400.0f) 
		{
			globalTime = 0;
			day++;

		}

        if (gameMode == Mode.Battle)
        {

            RunModeUpdate();

        }

        if (gameMode == Mode.Score)
        {

            //BlockPlane _plane = Placed(hero.transform.position);
            //if (_plane != null)
            //    _plane.CheckNewCreate();

        }

        if (gameMode == Mode.BaseToBattle)
        {
            door.position = new Vector3(door.position.x, door.position.y+Time.deltaTime*0.75f, door.position.z);
        }
        if (gameMode == Mode.Free)
        {
			RunModeUpdate();
			//gameSpeed = 1;
            if (screenFader.fadeState == ScreenFader.FadeState.InEnd)
            {
				hero.vehicle.currentRigidbody.velocity = Vector3.zero;
				hero.vehicle.currentRigidbody.angularVelocity = Vector3.zero;
                gameMode = Mode.Global;
				UIFree.SetActive (false);
                globalMap.gameObject.SetActive(true);
                globalMap.Open();
                globalMap.mode = GlobalMapStatus.Wait;
                screenFader.fadeState = ScreenFader.FadeState.Out;


            }
        }

		if (gameMode == Mode.Dialog || gameMode == Mode.DialogGlobal) 
		{
			hero.vehicle.currentRigidbody.velocity = Vector3.zero;
			hero.vehicle.currentRigidbody.angularVelocity = Vector3.zero;
		}

        if (gameMode == Mode.Global)
        {
			//gameSpeed = 1;
			globalMap.SwithTime (globalTime);

            if (screenFader.fadeState == ScreenFader.FadeState.InEnd)
            {
                globalMap.current = globalMap.targetWaypoint;
                for (int j = 0; j < BlockPlanes.Count; j++)
                    BlockPlanes[j].DestroyThis();

                BlockPlanes.Clear();
                globalMap.mode = GlobalMapStatus.Wait;

                CreateManual(globalMap.targetWaypoint.platform.name);
                globalMap.gameObject.SetActive(false);
				UIFree.SetActive (true);

                gameMode = Mode.Free;
                screenFader.fadeState = ScreenFader.FadeState.Out;
                isHeroFreeBoost = true;
				hero.vehicle.currentRigidbody.velocity = hero.transform.forward * 10.0f;

				for (int i = 0; i < globalMap.currentMission.events.Length; i++)
					if (!globalMap.currentMission.events [i].isActivated) 
					{
						if (globalMap.currentMission.events [i].eventType == GlobalEventType.Enter) 
						{
							if (globalMap.currentMission.events [i].waypointIndex == globalMap.targetWaypoint.index) 
							{
								gameMode = Mode.Dialog;
								dialog.tree = globalMap.currentMission.events [i].dialog;
								dialog.Activate ();
								globalMap.currentMission.events [i].isActivated = true;

							}
						}
					}
                //gameObject.SetActive(false);
            }
        }
    }

    void RunModeUpdate()
    {
        if (!isMenu)
        {
			UnityEngine.Profiling.Profiler.BeginSample ("RunStart");
            

            BlockPlane _plane = Placed(hero.transform.position);
            if (_plane != null)
                _plane.CheckNewCreate();

			UnityEngine.Profiling.Profiler.EndSample ();

			UnityEngine.Profiling.Profiler.BeginSample ("Buttons");
            if (Input.GetButtonDown("Menu"))
            {
                isMenu = true;
                inventory.Activate();
            }

            if (Input.GetKey(KeyCode.W))
                hero.Boost(1.0f);

            if (Input.GetKey(KeyCode.P))
                gameSpeed = 0;

            if (Input.GetKey(KeyCode.R))
                gameSpeed = 1.0f;

            if (Input.GetKey(KeyCode.S))
                hero.Brake(1.0f);

            if (Input.GetKey(KeyCode.A))
                hero.InputSteer(-1.0f);

            if (Input.GetKey(KeyCode.D))
                hero.InputSteer(1.0f);

            if (((Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.D)) && (!Input.GetKey(KeyCode.A) && !Input.GetKey(KeyCode.D))))
                hero.InputSteer(0.0f);

            if ((Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.S)) && (!Input.GetKey(KeyCode.W) && !Input.GetKey(KeyCode.S)))
                hero.InputAccel(0.0f);

            if (Input.GetButtonDown("HeroReturn"))
            {
                heroReturn.Activate();
            }


            if (hero.startBoostTimerFree.isEnd(Time.deltaTime) && isHeroFreeBoost)
            {
                hero.InputAccel(0.0f);
                isHeroFreeBoost = false;
            }

            if (isHeroFreeBoost)
                hero.InputAccel(1 - hero.startBoostTimerFree.current / hero.startBoostTimerFree.max);
            
            if (bonusTimer.current>0)
                if (bonusTimer.isEnd(Time.deltaTime))
                {
                    Coins.transform.position = new Vector3(-1000.0f, -1000.0f, 0);
                }

            for (int i = 0; i < 5; i++)
                enemyUI[i].EnemyHealthBar.transform.position = new Vector3(-1000.0f, -1000.0f, 0);
			UnityEngine.Profiling.Profiler.EndSample ();
			UnityEngine.Profiling.Profiler.BeginSample ("UI_Shit");
            for (int i = 0; i < activeTargetUnits.Count; i++)
                //if (guiTarget != null)
                {
                    Vector3 Pos = mainCamera.roadAffectCamera.WorldToScreenPoint(activeTargetUnits[i].currentRigidbody.position);
                    Pos.x -= 40.0f;
                    enemyUI[i].EnemyHealthBar.transform.position = Pos;
                    //EnemyHealthBar.transform.position = Pos;
                    enemyUI[i].EnemyHealthBar.value = activeTargetUnits[i].healthPoints.current / activeTargetUnits[i].healthPoints.max;
                    switch (activeTargetUnits[i].durabilityStatus)
                    {
                        case DurabilityStatus.Normal:
                            enemyUI[i].EnemyHealthBarBackground.color = new Color(0.36f, 0.62f, 0.42f, 1.0f);
                            break;
                        case DurabilityStatus.Damaged:
                            enemyUI[i].EnemyHealthBarBackground.color = new Color(0.9f, 0.76f, 0.21f, 1.09f);
                            break;
                        case DurabilityStatus.Ruined:
                            enemyUI[i].EnemyHealthBarBackground.color = new Color(0.71f, 0.05f, 0.05f, 1.00f);
                            break;
                    }
                    //guiTargetOut.pixelInset = new Rect(Pos.x - 45, Pos.y - 46, 7, 92);
                    //guiTargetIn.pixelInset = new Rect(Pos.x - 43, Pos.y - 46, 3, 92 * (guiTarget.healthPoints.current / guiTarget.hpMax));
                }
			UnityEngine.Profiling.Profiler.EndSample ();
        }
        else
        {
            Time.timeScale = 0;
            if (Input.GetButtonDown("Menu"))
            {
                inventory.Deactivate();
                isMenu = false;
            }
        }
        //if (hero.transform.position.y < -5)
        //    heroReturn.Activate();
		UnityEngine.Profiling.Profiler.BeginSample ("Turret");
        for (int i = 0; i < hero.moduleSystem.moduleWeapon.Count; i++)
        {
            if (!isTouchPad && hero.moduleSystem.moduleWeapon[i] != null)
            {
                //if (hero.moduleSystem.moduleWeapon[i].weaponType == WeaponType.Turret)
                //{


                    
                   // Vector3 globalPos = mainCamera._cam.ScreenToWorldPoint(Input.mousePosition);
                    //Vector3 globalCurvedPos = curveController.TransformPoint(globalPos);
                    //Debug.Log(globalPos + " " + globalCurvedPos);
                   // Vector3 newMousePos = Input.mousePosition;
                    Ray ray = mainCamera.roadAffectCamera.ScreenPointToRay(Input.mousePosition);
                   // Debug.Log(ray.origin + " " + ray.direction.x + " " + ray.direction.y + " " + ray.direction.z + "1");
                    //ray.direction = curveController.TransformPoint(ray.direction + ray.origin) - ray.origin;
                    //Debug.Log(ray.origin + " " + ray.direction.x + " " + ray.direction.y + " " + ray.direction.z + "2");
                    //ray.direction.Normalize();
                    //Debug.Log(ray.direction + " " + curveController.TransformPoint(ray.direction + ray.origin) - curveController.TransformPoint(ray.origin));
                    RaycastHit hit;
                    Vector3 MousePos;
                    int layerMask = 1 << 10;
                    layerMask = ~layerMask;
                    if (Physics.Raycast(ray, out hit, 500, layerMask, QueryTriggerInteraction.Ignore))
                    {
                        //Debug.DrawRay(ray.origin, ray.direction, Color.red, 2.0f);
                        MousePos = hit.point;
                        Vector3 heroPos = hero.transform.position;
                        Vector3 Dir = MousePos - heroPos;
                        Dir.Normalize();
                        float angle = Math3d.GetAngle(hero.transform.forward.x, hero.transform.forward.z, Dir.x, Dir.z);
                        //hero.moduleSystem.moduleWeapon.weapon.transform.localRotation = Quaternion.Euler(0.0f, -angle, 0.0f);
                        hero.moduleSystem.moduleWeapon[i].weapon.GetTransform(MousePos, -angle);
                    }
                //}
                if (Input.GetMouseButton(0))
                {
                    if (hero.moduleSystem.moduleWeapon[i].weapon.weaponState != WeaponState.Recharge)
                        hero.moduleSystem.moduleWeapon[i].weapon.StartShoot();
                }
                else
                {
                    if (hero.moduleSystem.moduleWeapon[i].weapon.weaponState != WeaponState.Recharge)
                        hero.moduleSystem.moduleWeapon[i].weapon.StopShoot();
                }


            }
        }
		UnityEngine.Profiling.Profiler.EndSample ();
    }

    

    public void SelectTarget(Unit unit)
    {
        if (!activeTargetUnits.Contains(unit))
        activeTargetUnits.Add(unit);
    }

    public void DeselectTarget(Unit unit)
    {
        activeTargetUnits.Remove(unit);
    }

    void FixedUpdate()
    {
        if (heroReturn!=null)
        heroReturn.UpdateThis();

        

        if (destroyWheelTarget!=null)
        {
            
            Vector3 Pos = mainCamera.roadAffectCamera.WorldToScreenPoint(destroyWheelTarget.transform.position);
            Pos.x -= 50.0f;
            destroyWheelIcon.transform.position = Pos;
            
                if (!destroyWheelIcon.GetComponent<Animation>().IsPlaying("damageIcon"))
                {
                 destroyWheelIcon.transform.position = new Vector3(-50.0f, -50.0f, 1.0f);
                    destroyWheelTarget = null;
                }
            }
            

        if (destroyBodyTarget!=null)
        {
            Vector3 Pos = mainCamera.roadAffectCamera.WorldToScreenPoint(destroyBodyTarget.transform.position);
            Pos.x -= 50.0f;
            destroyBodyIcon.transform.position = Pos;

                if (!destroyBodyIcon.GetComponent<Animation>().IsPlaying("damageIcon"))
                {
                 destroyBodyIcon.transform.position = new Vector3(-50.0f, -50.0f, 1.0f);
                    destroyBodyTarget = null;
                }
            
        }
    }

    public void DrawDestroyWheelIcon(Unit target)
    {
        //destroyWheelTarget = target;
        //destroyWheelIcon.GetComponent<Animation>().PlayQueued("damageIcon");
    }

    public void DrawDestroyBodytIcon(Unit target)
    {
        //destroyBodyTarget = target;
        //destroyBodyIcon.GetComponent<Animation>().PlayQueued("damageIcon");
    }

    public BlockPlane GetBlock(string name)
    {
        int index = 0;
        for (int i = 0; i < resourcesDB.blocks.Count; i++)
            if (resourcesDB.blocks[i].name == name.ToLower())
                index = i;
        string Path = resourcesDB.blocks[index].prefabPath.Substring(17, resourcesDB.blocks[index].prefabPath.Length - 24);
        return Resources.Load(Path, typeof(BlockPlane)) as BlockPlane;
    }

    public BlockPlane GetPlane(int _number, int _line)
    {
		BlockPlane[] AllPlanes = BlockPlanes.ToArray ();
        foreach (BlockPlane _plane in AllPlanes)
        {
            if (_plane.localForkNumber == _number && _plane.runLine == _line && !_plane.nonKill)
                return _plane;
        }
        return null;
    }

    public Unit GetVehicle(string name)
    {
        int index = 0;
        for (int i = 0; i < resourcesDB.vehicles.Count; i++)
			if (resourcesDB.vehicles[i].name.ToLower() == name.ToLower())
                index = i;
        string Path = resourcesDB.vehicles[index].prefabPath.Substring(17, resourcesDB.vehicles[index].prefabPath.Length - 24);
        return Resources.Load(Path, typeof(Unit)) as Unit;
    }

	public Misc GetMisc(string name)
    {
        int index = 0;
        for (int i = 0; i < resourcesDB.misc.Count; i++)
			if (resourcesDB.misc[i].name.ToLower() == name.ToLower())
                index = i;
        string Path = resourcesDB.misc[index].prefabPath.Substring(17, resourcesDB.misc[index].prefabPath.Length - 24);
		return Resources.Load(Path, typeof(Unit)) as Misc;
    }

    public Object GetModule(string name)
    {
        int index = 0;
        for (int i = 0; i < resourcesDB.modules.Count; i++)
            if (resourcesDB.modules[i].name == name.ToLower())
                index = i;
        string Path = resourcesDB.modules[index].prefabPath.Substring(17, resourcesDB.modules[index].prefabPath.Length - 24);
        return Resources.Load(Path, typeof(Object));
    }

    public void EndLevel()
    {
        //screenFader.fadeState = ScreenFader.FadeState.In;
    }

    public void CreateNew()
    {
        if (isGenerator)
            CreateNewAuto();
       // else
        //    CreateNewManual();
    }


    public void CreateManual(string blockName)
    {
        OutpostPlane lastPlane = BlockPoolSystem.GetPooled(blockName) as OutpostPlane;
        lastPlane.currentGameObject.SetActive(true);

        lastPlane.Create(0, 0, Vector4.zero);
        hero.transform.position = lastPlane.heroStartPos;
        hero.transform.localRotation = lastPlane.heroStartRot;
        mainCamera.transform.position = lastPlane.camStartPos;
        mainCamera.transform.rotation = lastPlane.camStartRot;

       BlockPlanes.Add(lastPlane);
    }
    private void CreateNewAuto()
    {
		UnityEngine.Profiling.Profiler.BeginSample ("All");
        BlockPlane newBlockPlane;

		if (generator.isBlockEnabled) 
		{
			newBlockPlane = GetLevelPlane (forkRun, planeRun);
			Debug.Log (newBlockPlane.name);
		}
		else {
			newBlockPlane = BlockPoolSystem.GetPooled (generator.biomeSources.biomesSource [generator.activeSeason.biomeNumber].roadSource [0].name);
			Debug.Log ("Standart");
		}
		
		if (BlockPlanes.Contains (newBlockPlane))
			return;
		
        Vector4 oldBlockPosition = currentPlane.GetOldBlockTransforms();

        newBlockPlane.currentGameObject.SetActive(true);
		UnityEngine.Profiling.Profiler.EndSample ();
		UnityEngine.Profiling.Profiler.BeginSample ("Create");
        newBlockPlane.Create(planeRun, forkRun, oldBlockPosition);
		UnityEngine.Profiling.Profiler.EndSample ();
    }

	//[ContextMenu("TestBlock")]
	//public void TestPlane()
	//{
	//	Vector3 pos = Selection.activeGameObject.transform.position;
	//	Debug.Log (GetBlockPlane (pos).nominal);
	//}

	public BlockPlane GetBlockPlane(Vector3 _pos, out float step)
	{
		step = 0;
		Vector3 Point = transform.InverseTransformPoint(_pos);

		for (int i = 0; i < BlockPlanes.Count; i++) 
		{
			float rev = BlockPlanes [i].spline.GetStep (Point);
			if (rev >= 0 && rev <= 1f) 
			{
				step = rev;
				return BlockPlanes [i];
			}
		}

		return null;
	}

    public void AddAndDeletePlanes(BlockPlane lastPlane)
    {
			/*
            int min = 1000;
            BlockPlane currentForDelete = null;

            if (BlockPlanes.Count >= 3)
            {
                bool isMoreLines = false;
                for (int i = 0; i < BlockPlanes.Count; i++)
                {
                    if (lastPlane.runLine != BlockPlanes[i].runLine)
                    isMoreLines = true;
                }

                for (int i = 0; i < BlockPlanes.Count; i++)
                {
                if (BlockPlanes[i].localForkNumber < min && ((lastPlane.runLine != BlockPlanes[i].runLine && isMoreLines) || !isMoreLines))
                    {
                        min = BlockPlanes[i].localForkNumber;
                        currentForDelete = BlockPlanes[i];
                    }
                }

                if (currentForDelete != null)
                {
					Debug.Log ("Remove " + currentForDelete.nominal);
                    BlockPlanes.Remove(currentForDelete);
                    currentForDelete.DestroyThis();
                }
            }
			*/

			BlockPlane currentForDelete = BlockPlanes [0];
			BlockPlanes.Remove (currentForDelete);
			currentForDelete.DestroyThis();

            BlockPlanes.Add(lastPlane);
    }

    private BlockPlane GetRandomPlane()
    {
        BlockPlane randomPlane = BlockPoolSystem.GetPooled("rockForward01");

        if (randomPlane == null)
            return GetRandomPlane();
        else
            return randomPlane;
    }

    public BlockPlane GetLevelPlane(int _runLine, int _localForkNumber)
    {
        string data = generator.getLevelPart(_runLine, _localForkNumber);
//		Debug.Log (_runLine + " " + _localForkNumber + " " + data);
		BlockPlane specialPlane = BlockPoolSystem.GetPooled(data.ToLower());
        if (specialPlane == null)
        {
            Debug.LogError("Wrong block. No acces to " + data);
            return GetRandomPlane();
       }
       else
         return specialPlane;
    }

    /*
    private void CreateNewManual()
    {

    GameObject lastPlaneObj;
    int choice = 0;
      
    if (planeRun >= levelDB.forkData[forkRun].blocks.Count)
    {
        Debug.LogWarning("Cannot create " + planeRun + " object, by " + forkRun + " fork");
        return;
    }

    if (levelDB.forkData[forkRun].blocks[planeRun].fork>0)
        choice=1;
    else
    if (levelDB.forkData[forkRun].blocks[planeRun].reverseCount>0)
        choice=2;


    Vector4 oldBlockPosition = currentPlane.GetOldBlockTransforms(forkRun, false);
    CreateSpawnDots(forkRun,planeRun);
    CreateMiscs(forkRun, planeRun);
    switch (choice)
    {
        case 0:
            {

                lastPlaneObj = GameObject.Instantiate(GetBlock(levelDB.forkData[forkRun].blocks[planeRun].name), Vector3.zero, Quaternion.identity) as GameObject;
                currentPlane = lastPlaneObj.GetComponent<BlockPlane>();
                currentPlane.Create(planeRun, forkRun, oldBlockPosition, false);

                if (!isReverse)
                planeRun++;
                else
                planeRun--;
            }
            break;

        case 2:
            {

                lastPlaneObj = GameObject.Instantiate(GetBlock(levelDB.forkData[forkRun].blocks[planeRun].name), Vector3.zero, Quaternion.identity) as GameObject;
                currentPlane = lastPlaneObj.GetComponent<ForkReverse>();
                currentPlane.Create(planeRun, forkRun, oldBlockPosition, false);
                BlockData forkReverse = levelDB.forkData[forkRun].blocks[planeRun];
                if (forkReverse.fork != forkRun)
                {
                    forkRun = forkReverse.fork;
                    planeRun = forkReverse.reverseCount;
                }

                if (!isReverse)
                planeRun++;
                else
                planeRun--;
            }

            break;

        case 1:
            {
                lastPlaneObj = GameObject.Instantiate(GetBlock(levelDB.forkData[forkRun].blocks[planeRun].name), Vector3.zero, Quaternion.identity) as GameObject;
                currentPlane = lastPlaneObj.GetComponent<Fork>();
                currentPlane.Create(planeRun, forkRun, oldBlockPosition, false);

                string adress;

                if (!isReverse)
                    planeRun++;
                else
                    planeRun--;

                adress = levelDB.forkData[currentPlane.runLine].blocks[planeRun].name;
                    Debug.Log(adress);
                GameObject RightForkPlaneObj = GameObject.Instantiate(GetBlock(adress), Vector3.zero, Quaternion.identity) as GameObject;
                RightForkPlane = RightForkPlaneObj.GetComponent<BlockPlane>();
                RightForkPlane.CreateSubPlane(SplineType.ForkRight);

                adress = levelDB.forkData[levelDB.forkData[currentPlane.runLine].blocks[planeRun - 1].fork].blocks[0].name;
                GameObject LeftForkPlaneObj = GameObject.Instantiate(GetBlock(adress), Vector3.zero, Quaternion.identity) as GameObject;
                LeftForkPlane = LeftForkPlaneObj.GetComponent<BlockPlane>();
                LeftForkPlane.CreateSubPlane(SplineType.ForkLeft);
            }
            break;
        }

    

    if (currentPlane.ignoreCollider)
        currentPlane.currentGameObject.layer = LayerMask.NameToLayer("Default");
    }
    */


    public void CreateSpawnDots(int _forkNum, int _planeNum)
    {
        
        for (int i = 0; i < levelDB.enemies.Count; i++)
        {
            if (levelDB.enemies[i].planeNumber == _planeNum && levelDB.enemies[i].forkNumber == _forkNum)
            {
                GameObject _spawnDotObj = GameObject.Instantiate(spawnDotSource, levelDB.enemies[i].currentPosition, Quaternion.identity) as GameObject;
                Debug.Log(_spawnDotObj.name);
                SpawnDot _spawnDot = _spawnDotObj.GetComponent<SpawnDot>();
                _spawnDot.enemyName = levelDB.enemies[i].name;
                _spawnDot.enemyPosition = levelDB.enemies[i].EnemyPos;
                _spawnDot.enemyRotation = levelDB.enemies[i].EnemyRot;
                _spawnDot.percent = levelDB.enemies[i].percent;
                _spawnDot.rect = levelDB.enemies[i].rect;
                _spawnDot.planeNumber = levelDB.enemies[i].planeNumber;
                _spawnDot.forkNumber = levelDB.enemies[i].forkNumber;
                _spawnDot.transform.parent = spawnFolder;
                //spawnDots.Add(_spawnDot);
            }

        }

    }
    

        
    public void CreateMiscs(int _forkNum, int _planeNum)
    {

        for (int i = 0; i < levelDB.miscs.Count; i++)
        {
            //Debug.Log(levelDB.miscs[i].planeNumber + " " + _planeNum + " " + levelDB.miscs[i].forkNumber + " " + _forkNum);
            if (levelDB.miscs[i].planeNumber == _planeNum && levelDB.miscs[i].forkNumber == _forkNum)
            {
				//GameObject _miscObj =  as GameObject;
				Misc _misc = Instantiate(GetMisc(levelDB.miscs[i].name), levelDB.miscs[i].Pos, levelDB.miscs[i].Rot) as Misc;//_miscObj.GetComponent<Misc>();
				_misc.nominal = levelDB.miscs[i].name;
                _misc.miscType = levelDB.miscs[i].type;
                _misc.transform.localScale = levelDB.miscs[i].Scale;
               // _misc.forkNumber = levelDB.miscs[i].forkNumber;
               // _misc.planeNumber = levelDB.miscs[i].planeNumber;
                //miscs.Add(_misc);
            }

        }

    }


    public Transform CreateBlock(int line, int num, Vector4 oldBlockPosition)
    {
		BlockPlane lastPlane = GameObject.Instantiate(GetBlock(levelDB.forkData[line].blocks[num].name), Vector3.zero, Quaternion.identity) as BlockPlane;
        lastPlane.Create(num, line, oldBlockPosition);
        return lastPlane.transform;
    }

    public void DeleteAllLine(int _line, BlockPlane firstPlane)
    {
        BlockPlane[] planes = FindObjectsOfType<BlockPlane>();
        List<BlockPlane> DeleteList = new List<BlockPlane>();
        foreach (BlockPlane plane in planes)
        {
            if (plane.runLine == _line && plane.type != BlockPlaneType.Outpost && plane != firstPlane)
                    DeleteList.Add(plane);
        }

        for (int i = 0; i < DeleteList.Count; i++)
            DeleteList[i].DestroyThis();
        
    }

    public bool CanDestroyBlock(BlockPlane block)
    {
        int numb = 1;
        if (block.runLine < 0)
            return false;

       numb = levelDB.forkData[block.runLine].blocks.Count;

        if (block.localForkNumber == numb)
            return false;
        else
            return true;
    }

    public Vector3 GetHeroWaypoint()
    {
		OneSpline spline = hero.vehicle.GetSpline();
        PosRot posRot = GetNextDot(Vector3.zero, true, 5.0f, hero.transform, ref spline);
        return posRot.pos;
    }
	public PosRot GetNextDot(Vector3 _pos, bool direction, float wayLength, Transform master, ref OneSpline currentSpline)
    {
        Vector3 pos = master.TransformPoint(_pos);
        //pos.y += 5.0f;
          Vector3 curveposition = Vector3.zero;

        float addPercent = 0.0f;
        float passed = 0.0f;

        //Debug.Log(currentSpline.GetStep(pos) + " " + pos + " " + _pos);
        if (currentSpline.UniformGlobalPosAdd(currentSpline.GetStep(pos), wayLength, 0, out curveposition, out addPercent, out passed, direction))
        {
            PosRot posRot = new PosRot(master.InverseTransformPoint(curveposition), master.InverseTransformDirection(Vector3.Normalize(currentSpline.GetTangent(addPercent))) * ((direction) ? -1 : 1));
            //PosRot posRot = new PosRot(master.InverseTransformPoint(curveposition), master.InverseTransformDirection(Vector3.Normalize(currentSpline.GetAngle(addPercent))));
            return posRot;
        }
        else
        {
            BlockPlane old = currentSpline.transform.GetComponent<BlockPlane>();
            BlockPlane current = old.GetNextBlock(direction, currentSpline.type);

            if (current != null)
            {
                currentSpline = Math3d.GetEnemySpline(current, transform.position);
                //Debug.Log(currentSpline.gameObject.name);
            }
            else
                Debug.LogError("Main: No block ");

            float temp;
            if (currentSpline != null)
            {
                if (currentSpline.UniformGlobalPosAdd(direction ? 0 : 1.0f, wayLength - passed, 0, out curveposition, out addPercent, out temp, direction))
                {
                    PosRot posRot = new PosRot(master.InverseTransformPoint(curveposition), master.InverseTransformDirection(Vector3.Normalize(currentSpline.GetTangent(addPercent))) * ((direction) ? -1 : 1));
                    return posRot;
                }
                else
                    Debug.LogError("Main: Spline Error " + current.currentGameObject.GetInstanceID());
            }
            else
                Debug.LogError("Main: No spline ");
        }

        Debug.LogError("Lol, End");
        return new PosRot(Vector3.zero, Vector3.zero);

       
    }


    public void ChoiseBlock(SplineType side)
    {
        BlockPlane falsePlane;
        RoadElement currentForkData = generator.currentMap.activeSeasons[forkRun].roadElements[planeRun];
        float _localNumber = currentPlane.localNumber;
        float _localPoint = currentPlane.point;

        if (side == SplineType.Right)
        {
            currentPlane.RightForkPlane.previousBlock = currentPlane;

            falsePlane = currentPlane.LeftForkPlane;

            currentPlane = currentPlane.RightForkPlane;
            
            forkRun = currentForkData.rightWay;
            planeRun = currentForkData.ForkRun(false, planeRun);
        }
        else
        {
            currentPlane.LeftForkPlane.previousBlock = currentPlane;
            falsePlane = currentPlane.RightForkPlane;

            currentPlane = currentPlane.LeftForkPlane;
            
            
            forkRun = currentForkData.leftWay;
            planeRun = currentForkData.ForkRun(true, planeRun);
        }

        currentPlane.localNumber = _localNumber + _localPoint;
        currentPlane.localForkNumber = planeRun;
        currentPlane.runLine = forkRun;
        currentPlane.nonKill = false;
        falsePlane.nonKill = false;
        currentPlane.currentGameObject.tag = "Block";
        currentPlane.canCreateNextPlane = true;
        currentPlane.transform.parent = transform;

        reverseLine = side;
        falsePlane.DestroyThis();

        currentPlane.transform.parent = transform;
        planeRun++;

        AddAndDeletePlanes(currentPlane);

        currentPlane.Additive();

        mainCamera.ForkShitOperation();
    }

    public void CreateNewEditor(bool add, int fcount)
    {
        /*
        Transform StartTransform;

        if (!add)
        {
            if (lastPlane.type!=PlaneType.Fork)
                StartTransform = lastPlane.GetComponent<Plane>().gameObject.transform.Find("startPos").transform;
            else
            {

                lastPlane.GetComponent<Fork>().forkNumber = levelDB.forkData[fcount].blocks[ch - 1].fork;
                StartTransform = lastPlane.GetComponent<Plane>().gameObject.transform.Find("startPos02").transform;
            }
        }
        else
        {
            StartTransform = lastPlane.GetComponent<Plane>().gameObject.transform.Find("startPos01").transform;
        }
        Vector3 Pos = Math3d.RotateThis(Vector3.forward * 25.0f, -(StartTransform.localRotation.eulerAngles.y + mainAngle), StartTransform.position);
        mainAngle += StartTransform.localRotation.eulerAngles.y;
        */
        
#if UNITY_EDITOR
        
        if (levelDB.forkData[fcount].blocks[planeRun].name.Substring(0, 1) != "_")
        {          
            Vector4 oldBlockPosition = Vector4.zero;
            if (planeRun>0)
            oldBlockPosition = currentPlane.GetOldBlockTransforms();        

            GameObject lastPlaneObj = PrefabUtility.InstantiatePrefab(GetBlock(levelDB.forkData[fcount].blocks[planeRun].name)) as GameObject;
            currentPlane = lastPlaneObj.GetComponent<BlockPlane>();
            forkRun = fcount;
            currentPlane.Create(planeRun, fcount, oldBlockPosition);
          
        }
#endif
        /*
        if (lastPlane.type == PlaneType.ForkReverse)
        {
            lastPlane.GetComponent<ForkReverse>().line = SplineType.ForkRight;
            Pos = Math3d.RotateThis(new Vector3(-15.0f, 0.0f, 25.0f), -(StartTransform.localRotation.eulerAngles.y + mainAngle), StartTransform.position);
            lastPlane.transform.position = Pos;
            lastPlane.transform.rotation = StartTransform.rotation;
            //Center.GetComponent<Plane>().number = countPlanes;
        }
        */
        currentPlane.transform.parent = transform;
        planeRun++;

    }

    public void BuildLevel(string _name)
    {
        planeRun = 0;

        resourcesDB = Resources.Load(ResourcesDatabase.assetName, typeof(Object)) as ResourcesDatabase;
        levelDB = Resources.Load(_name, typeof(Object)) as LevelData;
        
        for (int i = 0; i < transform.childCount; i++)
			transform.GetChild(i).GetComponent<BlockPlane>().Initialize(true);
       
        for (int i = 0; i < levelDB.forkData.Count; i++)
        {
            for (int j = 0; j < levelDB.forkData[i].blocks.Count; j++)
            {
                bool isFined = false;
                foreach (Transform selectPlane in transform)
                {
                    BlockPlane current = selectPlane.GetComponent<BlockPlane>();
                    if ((current.runLine == i && current.localForkNumber == j) || (current.type == BlockPlaneType.Outpost && current.name == levelDB.forkData[i].blocks[j].name))
                    {
                        currentPlane = current;
                        isFined = true;
                        planeRun++;
                        break;
                    }
                }

                if (!isFined)
                {
                    CreateNewEditor(false, i);
                    currentPlane.Initialize(true);
                }
            }

            planeRun = 0;

        }


    }

    /*
    void CreateEnemy(int index, bool editor)
    {
        
        GameObject AutoEnemy = GameObject.Instantiate(GetVehicle(levelDB.enemies[index].name), levelDB.enemies[index].Pos, levelDB.enemies[index].Rot) as GameObject;
        Enemy AutoEnemyComponent = AutoEnemy.GetComponent<Enemy>();
        AutoEnemyComponent.direction = levelDB.enemies[index].direction;
        AutoEnemyComponent.steerSpeed = levelDB.enemies[index].startPower;
        AutoEnemyComponent.startVelocityZ = levelDB.enemies[index].startPower;
        AutoEnemy.name = AutoEnemyComponent._name;
        if (editor)
            AutoEnemy.transform.parent = GameObject.Find("Vehicles").transform;
                
    }

    void CreateMisc(int index, bool editor)
    {
        GameObject AutoMisc = GameObject.Instantiate(GetMisc(levelDB.miscs[index].name), levelDB.miscs[index].Pos, levelDB.miscs[index].Rot) as GameObject;
        AutoMisc.transform.localScale = levelDB.miscs[index].Scale;
        Misc AutoMiscComponent = AutoMisc.GetComponent<Misc>();
        AutoMisc.name = AutoMiscComponent._name;
        if (editor)
            AutoMisc.transform.parent = GameObject.Find("Misc").transform;
    }
    */

    public void LoadModule(string name)
    {
        GameObject moduleObject = Instantiate(GetModule(name), Vector3.zero, Quaternion.identity) as GameObject;
        Module module = moduleObject.GetComponent<Module>();
        module.durability.max = inventory.items[module.number].durability;
        module.durability.Restore();
        hero.moduleSystem.otherModules.Add(module);
        module.gameObject.SetActive(false);

        hero.moduleSystem.UpdateIcons();
    }

    public void LoadModuleInStock(string name)
    {
        GameObject moduleObject = Instantiate(GetModule(name), Vector3.zero, Quaternion.identity) as GameObject;
        Module module = moduleObject.GetComponent<Module>();
        module.transform.parent = hero.transform;
        module.transform.position = Vector3.zero;
        module.transform.rotation = Quaternion.identity;
        module.gameObject.SetActive(true);
        module.durability.max = inventory.items[module.number].durability;
        module.durability.Restore();
        hero.moduleSystem.module[(int)module.slotType] = module;
        hero.moduleSystem.UpdateIcons();
    }

    public static BlockPlane Placed(Vector3 _pos)
    {

        RaycastHit[] hit;
        BlockPlane result = null;
		int layerMask = 1 << 0 | 1 << 9;// 1 << 12;
        layerMask = ~layerMask;
        

        hit = Physics.RaycastAll(_pos, Vector3.down, Mathf.Infinity, layerMask);
        for (int i = 0; i < hit.Length; i++)
            if (hit[i].transform.CompareTag("Block"))
                result = hit[i].collider.transform.parent.GetComponent<BlockPlane>();

        

        
        return result;
    }

    public static BlockPlane PlacedCam(Vector3 _pos, out float height)
    {
        height = 0;
        RaycastHit hit;
        BlockPlane result = null;
        int layerMask = 1 << 0 | 1 << 9 | 1 << 12;
        layerMask = ~layerMask;

        if (Physics.Raycast(_pos, Vector3.down, out hit, Mathf.Infinity, layerMask))
        {
            result = hit.collider.transform.parent.GetComponent<BlockPlane>();
            height = hit.point.y;
        }

        
        return result;
    }

    /*
    public static void GUIDrawRect(Rect position, Color color)
    {
        if (_staticRectTexture == null)
        {
            _staticRectTexture = new Texture2D(1, 1);
        }

        if (_staticRectStyle == null)
        {
            _staticRectStyle = new GUIStyle();
        }

        _staticRectTexture.SetPixel(0, 0, color);
        _staticRectTexture.Apply();

        _staticRectStyle.normal.background = _staticRectTexture;

        GUI.Box(position, GUIContent.none, _staticRectStyle);
    }
    */

    
	/*

    public void SaveLevel()
    {
#if UNITY_EDITOR

        planeRun = 0;



        SpawnDot[] _spawnDots = FindObjectsOfType<SpawnDot>() as SpawnDot[];
        levelDB.enemies = new List<EnemyCreation>();
        for (int i = 0; i < _spawnDots.Length; i++)
        {
            Vector2i planeParams = Math3d.ReturnForkPlaneNumbers(_spawnDots[i].transform.position);
            levelDB.enemies.Add(new EnemyCreation(_spawnDots[i].enemyName,
                                                  _spawnDots[i].enemyPosition,
                                                  _spawnDots[i].enemyRotation,
                                                  _spawnDots[i].transform.position,
                                                  planeParams.y,
                                                  planeParams.x,
                                                  _spawnDots[i].percent,
                                                  _spawnDots[i].rect));
        }

		Unit[] _miscs = FindObjectsOfType<Misc>() as Misc[];
        levelDB.miscs = new List<MiscCreation>();
        for (int i = 0; i < _miscs.Length; i++)
        {
            Vector2i planeParams = Math3d.ReturnForkPlaneNumbers(_miscs[i].transform.position);
            levelDB.miscs.Add(new MiscCreation(_miscs[i]._name,
                                                  _miscs[i].transform.position,
                                                  _miscs[i].transform.rotation,
                                                  _miscs[i].transform.localScale,
                                                  planeParams.y,
                                                  planeParams.x,
                                                  _miscs[i].type)); 
        }



#endif
    }
	*/
}

