﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForkSplineDotData
{
	public Vector3 pos;
	public OneSpline spline;
	public int segment;
	public bool segmentTailStart;

	public ForkSplineDotData(Vector3 pos, OneSpline spline, int segment, bool segmentTailStart)
	{
		this.pos = pos;
		this.spline = spline;
		this.segment = segment;
		this.segmentTailStart = segmentTailStart;
	}

	public Vector3 CalculateNormal()
	{
		return spline.GetTangent ((segmentTailStart == true) ? spline.segments[segment].startTime : spline.segments[segment].endTime);
	}
}

public class ForkSpline : OneSpline
{
	
	public ForkSplineDotData startData;
	public ForkSplineDotData endData;

	public void UpdateSpline(float splineDistanceStart, float splineDistanceEnd)
	{
		if (this != null) 
		{
			nodes.Clear ();
			nodes = new List<OneSplineNode> ();
			//Vector3 startNormal = left.GetTangent (left.segments [1].startTime);
			//Vector3 endNormal = right.GetTangent (right.segments [0].startTime);
			Vector3 startNormal = startData.CalculateNormal();
			Vector3 endNormal = endData.CalculateNormal ();

			AddNode (new OneSplineNode (startData.pos, startData.pos - startNormal * splineDistanceStart)); 
			AddNode (new OneSplineNode (endData.pos, endData.pos + endNormal * splineDistanceEnd)); 
			//centerSpline.EditorUpdate ();
			EditorUpdate ();
		}


	}
}
