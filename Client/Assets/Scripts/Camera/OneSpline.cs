﻿using UnityEngine;
using UnityEngine.Events;
using System;
using System.Collections;
using System.Collections.Generic;
using Dest.Math;
#if UNITY_EDITOR
using UnityEditor;
#endif


[System.Serializable]
public class CameraDot
{
	public float percent;
	public float lenght;
	public int number;
	public Vector2 AxisBendSize;
	public Vector2 Bias;
	public Vector3 localPosition;
	public Vector3 localRotate;
}

[System.Serializable]
public class OneSplineSegment
{
	public float startTime;
	public float endTime;
	public float lenght;
	public Vector2 startWidth;
	public Vector2 endWidth;
	[ColorUsage(false)]
	public Color color;
	public AnimationCurve curve;
	public float offsetCenter;
	public float offsetBetween;
	public float offsetSide;
	public int leftWays;
	public int rightWays;

	public OneSplineSegment()
	{
		startTime = 0;
		endTime = 1;
		startWidth = new Vector2(1,1);
		endWidth = new Vector2(1,1);
		color = Color.white;
		List<Keyframe> keys = new List<Keyframe> ();
		keys.Add(new Keyframe(0,0));
		keys.Add(new Keyframe(1,1));
		curve = new AnimationCurve (keys.ToArray ());
	}

	public OneSplineSegment(float startTime, float endTime, OneSplineSegment other)
	{
		this.startTime = startTime;
		this.endTime = endTime;
		this.startWidth = other.startWidth;
		this.endWidth = other.endWidth;
		this.color = other.color;
		this.curve = other.curve;
		this.leftWays = other.leftWays;
		this.rightWays = other.rightWays;
		this.offsetCenter = other.offsetCenter;
		this.offsetSide = other.offsetSide;
		this.offsetBetween = other.offsetBetween;
	}
}

[System.Serializable]
public class OneSplineNode
{
	public Vector3 position;
	public Vector3 direction;
    public float tilt;
	public float inverseWidth;
	public float width;
	public float verge;


	public OneSplineNode(Vector3 position, Vector3 direction) {
		SetPosition(position);
		SetDirection(direction);
		inverseWidth = (direction - position).Length();
	}
		
	public void SetPosition(Vector3 p) {
		if (!position.Equals(p)) {
			position.x = p.x;
			position.y = p.y;
			position.z = p.z;
			if (Changed != null)
				Changed.Invoke();
		}
	}

	public void SetDirection(Vector3 d) {
		if (!direction.Equals(d)) {
			direction.x = d.x;
			direction.y = d.y;
			direction.z = d.z;
			if (Changed != null)
				Changed.Invoke();
		}
	}

	public Vector3 GetInverseDir()
	{
		return (position + Vector3.Normalize(position - direction) * inverseWidth);
	}

	public enum SelectionType {
		Node,
		Direction,
		InverseDirection
	}



	[HideInInspector]
	public UnityEvent Changed = new UnityEvent();
}

[ExecuteInEditMode]
public class OneSpline : MonoBehaviour
{
	public enum EditType {
		Default,
		Insert,
		Slider,
	}

	public enum EditView {
		Segments,
		Ways,
	}

	public int _count;
	public int quality = 2000;
	public float segmentsStrenght = 0.5f;
	public float _step;
	public float _step_n;
	public bool isCalculated = false;
	public bool isDrawSpline = true;
	public float nextY;
	public float sliderT;
    public AnimationCurve tiltCurve = new AnimationCurve(
        new Keyframe[2] { new Keyframe(0, 0, 0, 0, 0, 0), new Keyframe(1, 1, 0, 0, 0, 0) });

	public SplineType type;
	public float Length;
	public EditType editType;
	public EditView editView;
	public List<SplineExtrusion> extructions;
	public List<SplineRailing> railings;

	public List<OneSplineNode> nodes;
	public List<OneCubicBezierCurve> curves = new List<OneCubicBezierCurve>();
	public List<OneSplineSegment> segments = new List<OneSplineSegment>();

	public float EditorPercent;
	public List<CameraDot> cameraDots;

	#if UNITY_EDITOR
	public List<bool> isOpenSegments;

	public OneSplineNode selection;
	public OneSplineNode.SelectionType selectionType;
	public bool isNodesOpen = false;
	public bool isSegmentsVisible = false;
	public bool isCameraNodes = false;
	public bool isOther = false;
	#endif

	public OneSpline previous;
	public OneSpline next;

	[HideInInspector]
	public UnityEvent NodeCountChanged = new UnityEvent();

	/// <summary>
	/// Event raised when one of the curve changes.
	/// </summary>
	[HideInInspector]
	public UnityEvent CurveChanged = new UnityEvent();

	void OnEnable()
	{
		EditorUpdate ();
	}

	#if UNITY_EDITOR
	[ContextMenu("Mouse t")]
	public void MouseT()
	{
		Debug.Log(curves[0].GetInverseDirection());

		Vector3 inverseDir = curves [0].n2.position - curves [0].n2.direction;
		inverseDir.Normalize ();
		Vector3 inverseDirNorm = inverseDir * curves [0].n2.inverseWidth;

		Debug.Log (curves [0].n2.position + inverseDirNorm);

	}

	public void RecalculateOpenSegments()
	{
		isOpenSegments = new List<bool> ();
		for (int i = 0; i < segments.Count; i++)
			isOpenSegments.Add (false);
	}

	#endif
	[ContextMenu("Calculate")]
	public void EditorUpdate()
	{
		curves.Clear();
		for (int i = 0; i < nodes.Count - 1; i++) {
			OneSplineNode n = nodes[i];
			OneSplineNode next = nodes[i + 1];

			OneCubicBezierCurve curve = new OneCubicBezierCurve(n, next);
			curve.Changed.AddListener(() => UpdateAfterCurveChanged());
			
			curves.Add(curve);
		}

		if (nodes.Count > 1)
			nextY = nodes[nodes.Count-1].position.y - nodes[0].position.y;

		RaiseNodeCountChanged();
		UpdateAfterCurveChanged();

		_count = Mathf.CeilToInt(Length / 4);
		_step = Length / _count;
		_step_n = _step / Length; 

		CalculateSegments ();
		GenerateExtructions ();
	}

	public void GenerateExtructions()
	{
		if (extructions!=null)
		foreach (SplineExtrusion extrusion in extructions)
			extrusion.GenerateMesh ();

		if (railings!=null)
			foreach (SplineRailing railing in railings)
				railing.CreateMeshes ();
	}

	public void CalculateSegments()
	{
		foreach (OneSplineSegment segment in segments)
			segment.lenght = GetLenght (segment.startTime, segment.endTime);

		SplineExtrusion se = GetComponent<SplineExtrusion> ();
		if (se!=null)
		se.GenerateMesh ();
	}

	private void RaiseNodeCountChanged() {
		if (NodeCountChanged != null)
			NodeCountChanged.Invoke();
	}

	public Vector2 GetSegmentScale(float t)
	{
		
		foreach (OneSplineSegment segment in segments) 
		{
			if (segment.startTime <= t && segment.endTime >= t) 
			{
				float percent = (t - segment.startTime) / (segment.endTime - segment.startTime);
				return Vector2.Lerp (segment.startWidth, segment.endWidth, segment.curve.Evaluate(percent));
			}
		}
		return Vector2.one;

	}

	public Vector2 GetUniformSegmentScale(float t)
	{
		

		foreach (OneSplineSegment segment in segments) 
		{
			if (segment.startTime <= t && segment.endTime >= t) 
			{
				
				float percent = (t - segment.startTime) / (segment.endTime - segment.startTime);
				float uT = UniformGlobalLenght (percent);
				return Vector2.Lerp (segment.startWidth, segment.endWidth, segment.curve.Evaluate(uT));
			}
		}
		return Vector2.one;

	}

	private void UpdateAfterCurveChanged() {
		Length = 0;
		foreach (var curve in curves) 
		{
			Length += curve.Length;
		}
		if (CurveChanged != null) {
			CurveChanged.Invoke();
		}
	}

	private void Reset() {
		nodes.Clear();
		curves.Clear();
		AddNode(new OneSplineNode(new Vector3(5, 0, 0), new Vector3(5, 0, -3)));
		AddNode(new OneSplineNode(new Vector3(10, 0, 0), new Vector3(10, 0, 3)));
		EditorUpdate ();
	}

	public void AddNode(OneSplineNode node)
	{
		nodes.Add(node);
		if (nodes.Count != 1) {
			OneSplineNode previousNode = nodes[nodes.IndexOf(node)-1];
			OneCubicBezierCurve curve = new OneCubicBezierCurve(previousNode, node);
			curve.Changed.AddListener(() => UpdateAfterCurveChanged());
			curves.Add(curve);
		}
	}

	public SplineExtrusion AddExtrusion()
	{
		GameObject roadMesh = new GameObject ();
		roadMesh.name = "Extruction";
		roadMesh.transform.parent = transform;
		roadMesh.transform.localPosition = Vector3.zero;
		roadMesh.transform.localRotation = Quaternion.identity;
		roadMesh.transform.localScale = Vector3.one;
		SplineExtrusion se = roadMesh.AddComponent<SplineExtrusion> ();
		se.spline = this;
		se.splineType = this.type;
		se.size = 0.2f;
		se.TextureScale = 0.2f;
		if (extructions == null)
			extructions = new System.Collections.Generic.List<SplineExtrusion> ();
		extructions.Add (se);

		se.roadTemplates.Add (new RoadTemplate (se.transform, 0));

		return se;
	}

	public void InsertNode(float t)
	{
		Vector3 pos1 = GlobalPos2 (t, 0);
		float percent = 0;
		int node = GetLocalNode (t, out percent);
		Debug.Log (node + " " + percent);
		float currentWidth = GetCurrentWidth (t);
		float currentVerge = GetCurrentVerge (t);
		float lenght = 0;
		Vector3 pos2 = Vector3.zero;
		float addPercent = 0;
		float dist = 0;

		lenght = curves [node].Length;
		OneSplineNode newNode = new OneSplineNode (pos1, pos2);
		newNode.verge = currentVerge;
		newNode.width = currentWidth;
		nodes.Insert (node+1, newNode);

		Vector3 upFirst = Vector3.Lerp (nodes [node].direction, nodes [node + 2].GetInverseDir(), percent);

		//GameObject go3 = GameObject.CreatePrimitive (PrimitiveType.Sphere);
		//go3.transform.name = "Up Dir";
		//go3.transform.position =upFirst;

		nodes [node].direction = Vector3.Lerp (nodes [node].position, nodes [node].direction, percent);
		nodes [node + 2].inverseWidth = nodes [node + 2].inverseWidth * (1-percent);
		//GameObject go = GameObject.CreatePrimitive (PrimitiveType.Sphere);
		//go.transform.name = "A Dir";
		//go.transform.position = nodes [node].direction;

		//GameObject go2 = GameObject.CreatePrimitive (PrimitiveType.Sphere);
		//go2.transform.name = "B Dir";
		//go2.transform.position = nodes [node + 2].GetInverseDir();





		Vector3 secondA = Vector3.Lerp (nodes [node].direction, upFirst, percent);
		Vector3 secondB = Vector3.Lerp (upFirst, nodes [node + 2].GetInverseDir(), percent);

		//GameObject go4 = GameObject.CreatePrimitive (PrimitiveType.Sphere);
		//go4.transform.name = "A Second";
		//go4.transform.position =secondA;

		//GameObject go5 = GameObject.CreatePrimitive (PrimitiveType.Sphere);
		//go5.transform.name = "B Second";
		//go5.transform.position =secondB;

		Vector3 center = Vector3.Lerp (secondA, secondB, percent);

		//GameObject go6 = GameObject.CreatePrimitive (PrimitiveType.Sphere);
		//go6.transform.name = "Center";
		//go6.transform.position =center;

		nodes [node + 1].position = center;
		nodes [node + 1].direction = secondB;
		nodes [node + 1].inverseWidth = (center - secondA).Length();

		editType = OneSpline.EditType.Default;

		EditorUpdate();
	}

	public void InsertNode(Vector3 pos)
	{
		Vector3 pos1 = GetPointOnCurve (pos);
		InsertNode (GetStep (pos1));
	}

	public int GetNodeIndexForTime(float t) {
		if (t < 0 || t > nodes.Count - 1) {
			throw new ArgumentException(string.Format("Time must be between 0 and last node index ({0}). Given time was {1}.", nodes.Count-1, t));
		}
		int res = Mathf.FloorToInt(t);
		if (res == nodes.Count - 1)
			res--;
		return res;
	}

	public Vector3 GetLocationAlongSplineAtDistance(float d) {
		if(d < 0 || d > Length)
			throw new ArgumentException(string.Format("Distance must be between 0 and spline length ({0}). Given distance was {1}.", Length, d));
		foreach (OneCubicBezierCurve curve in curves) {
			if (d > curve.Length) {
				d -= curve.Length;
			} else {
				return curve.GetLocationAtDistance(d);
			}
		}
		throw new Exception("Something went wrong with GetLocationAlongSplineAtDistance");
	}

	public float GetLenght(float start, float end)
	{
		float result = 0;
		float percentStart = 0;
		float percentEnd = 0;
		int startNode = GetLocalNode (start, out percentStart);
		int endNode = GetLocalNode (end, out percentEnd);

		//Debug.Log ("StartNode: " + startNode + ", Percent Start: " + percentStart + ", EndNode: " + endNode + ", PercentEnd: " + percentEnd);

		for (int i = startNode+1; i < endNode; i++) 
		{
			//Debug.Log(result + " += " + curves[i].Length + " : " + i + " Curve");
			result += curves [i].Length;

		}

		if (startNode == endNode) 
		{
			float diff = percentEnd - percentStart;
			//Debug.Log ("Diff: " + diff + " = End: " + percentEnd + " - Start: " + percentStart + " | Res: " + diff * curves [startNode].Length + " : One Node");
			result = diff * curves [startNode].Length;

		} 
		else 
		{
			//Debug.Log (result + " += " + curves [startNode].Length * percentStart + " : Start");
			result += curves [startNode].Length * (1 - percentStart);

			//Debug.Log (result + " += " + curves [endNode].Length * percentEnd + " : End");
			result += curves [endNode].Length * percentEnd;
		}
		//Debug.Log(result + " Final | Full: " + Length);
		return result;
	}

	public int GetLocalNode(float t, out float percent)
	{
		float _length = 0;
		float t_current = t;

		for (int i=0; i<curves.Count; i++)
		{
			_length += curves[i].Length;
			if (_length / Length >= t)
			{
				percent = t_current / (curves[i].Length / Length);
				return i;
			}
			else
				t_current -= curves[i].Length / Length;

		}
		percent = t_current / (curves[curves.Count-1].Length / Length);
		return curves.Count-1;
	}

	public OneCubicBezierCurve GetLocalCurve(float t, out float percent)
	{
		float _length = 0;

		for (int i=0; i<curves.Count; i++) 
		{
			float currentSize = curves [i].Length / Length;
			if (t <= _length + currentSize)
			{
				percent = (t - _length) / currentSize;
				return curves [i];
			}
			else
				_length += currentSize;

		}
		percent = 1f;
		return curves[curves.Count-1];

	}

	public float GetGlobalCurve(OneCubicBezierCurve currentCurve, float t)
	{
		float currentLenght = currentCurve.Length * t;

		foreach (OneCubicBezierCurve curve in curves) 
		{
			if (curve.Equals (currentCurve))
			{
				break;
			} else
				currentLenght += curve.Length;

		}
		return currentLenght / Length;

	}

	public Vector3 GetLocationAlongSpline(float t)
	{
		float percent = 0;
		return GetLocalCurve(t, out percent).GetLocation(percent);
	}

	public CameraDot[] GetCameraDots()
	{
		return cameraDots.ToArray ();
	}

    public float CheckCamProcess(float t)
    {
		float percent = 0;
		GetLocalCurve (t, out percent);
		return percent;
    }


    public OneSpline()
    {

		nodes = new List<OneSplineNode> ();
		curves = new List<OneCubicBezierCurve> ();
    }

    public Vector3 GlobalPos(float t)
    {
		float pct = 0;
		OneCubicBezierCurve curve = GetLocalCurve (t, out pct);

		return transform.TransformPoint (curve.GetLocation (Mathf.Clamp01(pct)));
    }

	public float GlobalStep(float t)
	{
		
		float pct = 0;
		OneCubicBezierCurve curve = GetLocalCurve (t, out pct);

		return ((curve.Length * pct) / curve.count);
	}

	public Vector3 GlobalPos(float t, float width)
	{
		float pct = 0;
		OneCubicBezierCurve curve = GetLocalCurve (t, out pct);

		float currentLenght = 0;

		float startTangent = Vector3ex.SignedAngleDeg (Vector3.forward, curve.GetTangent(0), Vector3.up);
		float endTangent = Vector3ex.SignedAngleDeg (Vector3.forward, curve.GetTangent(1), Vector3.up);	

		Vector3 offsetStart = Math3d.RotateThis(curve.n1.width * width, -startTangent);
		Vector3 offsetEnd = Math3d.RotateThis(curve.n2.width * width, -endTangent);

		float percent = 0;

		return transform.TransformPoint (GetLocalCurve (t, out percent).GetLocation (percent, offsetStart, offsetEnd));

	}

	public Vector3 GlobalPos2(float t, float width)
	{
		float pct = 0;
		OneCubicBezierCurve curve = GetLocalCurve (t, out pct);

		Vector3 dir = curve.GetTangent (pct);
		float y = Vector3ex.SignedAngleDeg (Vector3.forward, dir, Vector3.up);
		Vector2 scale = GetSegmentScale (t);
		Vector3 offset = Math3d.RotateThis(((width > 0) ? scale.y : scale.x) * width, -y);
		float percent = 0;

		return transform.TransformPoint (curve.GetLocation (Mathf.Clamp01(pct)) + offset);
	}

    public float GetTilt(float t)
    {
        float pct = 0;
        OneCubicBezierCurve curve = GetLocalCurve(t, out pct);

        return Mathf.Lerp(curve.n1.tilt, curve.n2.tilt, tiltCurve.Evaluate(pct));
    }

    public Vector3 GetTangent(float t)
	{
		float percent = 0;
		return GetLocalCurve (t, out percent).GetTangent (Mathf.Clamp01(percent));
	}

	public Vector3 GetTangent(float t, float width)
	{
		
		float pct = 0;
		OneCubicBezierCurve curve = GetLocalCurve (t, out pct);

		float currentLenght = 0;

		float startTangent = Vector3ex.SignedAngleDeg (Vector3.forward, curve.GetTangent(0), Vector3.up);
		float endTangent = Vector3ex.SignedAngleDeg (Vector3.forward, curve.GetTangent(1), Vector3.up);	

		Vector3 offsetStart = Math3d.RotateThis(curve.n1.width * width, -startTangent);
		Vector3 offsetEnd = Math3d.RotateThis(curve.n2.width * width, -endTangent);

		float percent = 0;

		return GetLocalCurve (t, out percent).GetTangent (percent, offsetStart, offsetEnd);
	}

	public float GetAngleDirection(Vector3 _forward, Vector3 _up, float t)
	{
		return Vector3ex.SignedAngleDeg (_forward, GetTangent(t) * Controller.highwayDirection, _up);
	}

	public float GetAngle(Vector3 _forward, Vector3 _up, float t)
	{
		return Vector3ex.SignedAngleDeg (_forward, GetTangent(t), _up);
	}

	public float GetAngle(Vector3 _forward, Vector3 _up, float t, float width)
	{
		return Vector3ex.SignedAngleDeg (_forward, GetTangent(t, width), _up);
	}

	public Vector3 GetYAngle(float t)
	{
		return new Vector3(0, GetAngleDirection(Vector3.forward, Vector3.up, t), 0);
	}

    public float GetCurrentWidth(float t)
    {
		float pct = 0;
		OneCubicBezierCurve curve = GetLocalCurve (t, out pct);

		return Mathf.Lerp(curve.n1.width, curve.n2.width, pct);
    }

    public float GetCurrentVerge(float t)
    {
		float pct = 0;
		OneCubicBezierCurve curve = GetLocalCurve (t, out pct);

		return Mathf.Lerp(curve.n1.verge, curve.n2.verge, pct);
    }




	public Vector3 GetPointOnCurve(Vector3 _pos)
	{
		float currentPercent = 0;
		float currentStep = 0;
		Vector3 point2 = Vector3.zero;
		//NearestPointOnCurve(_pos, out curveIndex, out point2D, out step, out currentStep);
		NearestPointOnCurve2(_pos, out point2, out currentPercent, out currentStep);
		return point2;
	}
	/*
	public float GetStep(Vector3 _pos)
	{
		int curveIndex = 0;
		float step = 0;
		float currentStep = 0;
		Vector3 point2D = Vector3.zero;
		NearestPointOnCurve2(_pos, out curveIndex, out point2D, out step, out currentStep);

		//GameObject sphere = GameObject.CreatePrimitive (PrimitiveType.Sphere);
		//sphere.name = "Step ";
		//sphere.transform.position = point2D;

		float allStep = 0;
		for (int j = 0; j < curveIndex; j++)
			allStep += curves [j].Length / Length;

		float splineFactor = curves [curveIndex].Length / Length;
		float last = ((currentStep) / curves [curveIndex].Length + curves [curveIndex].step_n * step) * splineFactor;

		return (allStep + last) ;
	}

	public float GetWidth(float t)
	{
		float pct = 0;
		OneCubicBezierCurve curve = GetLocalCurve (t, out pct);
	}
*/
	public float GetStep(Vector3 _pos)
	{
		float currentPercent = 0;
		float currentStep = 0;
		Vector3 point2 = Vector3.zero;

		if (NearestPointOnCurve2 (_pos, out point2, out currentPercent, out currentStep)) 
		{
			float allStep = 0;
			for (float i = 0; i < currentStep; i += _step_n) 
			{
				
				float dist = Vector3.Distance (GlobalPos2 (i, 0), GlobalPos2 (i + _step_n, 0));
				allStep += dist/Length;
				/*
			GameObject sphere = GameObject.CreatePrimitive (PrimitiveType.Sphere);
			sphere.name = "Step " + i + dist;
			sphere.transform.position = GlobalPos2 (i, 0);

			*/
			}

			float next = currentStep + _step_n;
			if (next > 1)
				next = 1;
			float distCalc = Vector3.Distance (GlobalPos2 (currentStep, 0), GlobalPos2 (next, 0));

			//float splineFactor = curves [curveIndex].Length / Length;
			//float last = ((currentStep) / curves [curveIndex].Length + curves [curveIndex].step_n * step) * splineFactor;

			return (allStep + currentPercent * distCalc/Length);
		} else
			return 0;
	}

	public float GetStep(Vector3 _pos, out float allStep, out float localStep)
	{
		allStep = 0;
		localStep = 0;
		float currentPercent = 0;
		float currentStep = 0;
		Vector3 point2 = Vector3.zero;

		if (NearestPointOnCurve2 (_pos, out point2, out currentPercent, out currentStep)) 
		{
			allStep = 0;
			for (float i = 0; i < currentStep; i += _step_n) 
			{

				float dist = Vector3.Distance (GlobalPos2 (i, 0), GlobalPos2 (i + _step_n, 0));
				allStep += dist/Length;
				/*
			GameObject sphere = GameObject.CreatePrimitive (PrimitiveType.Sphere);
			sphere.name = "Step " + i + dist;
			sphere.transform.position = GlobalPos2 (i, 0);

			*/
			}

			float next = currentStep + _step_n;
			if (next > 1)
				next = 1;
			float distCalc = Vector3.Distance (GlobalPos2 (currentStep, 0), GlobalPos2 (next, 0));

			localStep = currentPercent;
			//float splineFactor = curves [curveIndex].Length / Length;
			//float last = ((currentStep) / curves [curveIndex].Length + curves [curveIndex].step_n * step) * splineFactor;

			return (allStep + localStep * distCalc/Length);
		} else
			return 0;
	}

	public void NearestPointOnCurve(Vector3 _pos, out int curveIndex, out Vector3 point2D, out float step, out float currentStep)
	{
		Vector3 point = transform.InverseTransformPoint(_pos);
		point2D = Vector3.zero;
		curveIndex = -1;
		currentStep = 0;
		float min = 1000.0f;

		for (int j = 0; j < curves.Count; j++) 
		{
			for (float i = 0; i < curves [j].Length; i += curves [j].step) 
			{
				Vector3 central = curves[j].GetLocation (Mathf.Clamp01 (Mathf.Lerp (i, i + curves [j].step, 0.5f) / curves [j].Length));
				float dist = Vector3.Distance (point, central);
				if (dist < min) {
					min = dist;
					currentStep = i;
					curveIndex = j;
				}
			}
		}

		Vector3 start = curves[curveIndex].GetLocation(Mathf.Clamp01(currentStep / curves[curveIndex].Length));
		Vector3 end = curves[curveIndex].GetLocation(Mathf.Clamp01((currentStep + curves[curveIndex].step) / curves[curveIndex].Length));
		Vector3 dir = Vector3.Normalize(start - end);

		Vector3 left = 10.0f * Vector3.Cross(dir, Vector3.up) + point;
		Vector3 right = -10.0f * Vector3.Cross(dir, Vector3.up) + point;
		/*
		GameObject sphere = GameObject.CreatePrimitive (PrimitiveType.Sphere);
		sphere.name = "Start " + start.ToString();
		sphere.transform.position = start;

		GameObject sphere2 = GameObject.CreatePrimitive (PrimitiveType.Sphere);
		sphere2.name = "End " + end.ToString();
		sphere2.transform.position = end;

		GameObject sphere3 = GameObject.CreatePrimitive (PrimitiveType.Sphere);
		sphere3.name = "Left "  + left.ToString();
		sphere3.transform.position = left;

		GameObject sphere4 = GameObject.CreatePrimitive (PrimitiveType.Sphere);
		sphere4.name = "Right " + right.ToString();
		sphere4.transform.position = right;
*/
		Math3d.CrossPointLine(start, end, left, right, out point2D);



		float dist1 = Vector2.Distance(new Vector2(start.z, start.x), new Vector2(end.z, end.x));
		float dist2 = Vector2.Distance(new Vector2(start.z, start.x), new Vector2(point2D.z, point2D.x));
		step = (dist2 / dist1);
		point2D.y = Mathf.Lerp(start.y, end.y, step);

		//Intersection.FindLine2Line2
	}

	public bool NearestPointOnCurve2(Vector3 _pos, out Vector3 point2, out float currentPercent, out float currentStep)
	{
		bool final = false;
		Segment2Segment2Intr info = new Segment2Segment2Intr ();
		point2 = Vector3.zero;
		currentStep = 0;
		currentPercent = 0;
		Vector3 start = Vector3.zero;
		Vector3 end = Vector3.zero;
		float min = 1000.0f;
		float lenghtUp = 0;
		Vector3 point = _pos;//transform.InverseTransformPoint(_pos);

		for (float i = 0; i+_step_n < 1.0f; i += _step_n) 
		{
			float next = i + _step_n;
			if (next > 1.0f)
				next = 1.0f;

			Vector3 startTemp = GlobalPos2(i,0);
			Vector3 endTemp = GlobalPos2 (next, 0);
			Vector3 central = Vector3.Lerp(startTemp,endTemp,0.5f);
			float dist = Vector3.Distance (point, central);
			if (dist < min) 
			{
				min = dist;
				start = startTemp;
				end = endTemp;
				currentStep = i;
			}
		}

		Segment2 front = new Segment2 (start, end);

		Vector3 dir = Vector3.Normalize(start - end);
		Vector3 left = 10.0f * Vector3.Cross(dir, Vector3.up) + point;
		Vector3 right = -10.0f * Vector3.Cross(dir, Vector3.up) + point;
		/*
		GameObject sphere = GameObject.CreatePrimitive (PrimitiveType.Sphere);
		sphere.name = "Start " + start.ToString();
		sphere.transform.position = start;

		GameObject sphere2 = GameObject.CreatePrimitive (PrimitiveType.Sphere);
		sphere2.name = "End " + end.ToString();
		sphere2.transform.position = end;

		GameObject sphere3 = GameObject.CreatePrimitive (PrimitiveType.Sphere);
		sphere3.name = "Left "  + left.ToString();
		sphere3.transform.position = left;

		GameObject sphere4 = GameObject.CreatePrimitive (PrimitiveType.Sphere);
		sphere4.name = "Right " + right.ToString();
		sphere4.transform.position = right;
*/

		Math3d.CrossPointLine(start, end, left, right, out point2);

		float dist1 = Vector2.Distance(new Vector2(start.z, start.x), new Vector2(end.z, end.x));
		float dist2 = Vector2.Distance(new Vector2(start.z, start.x), new Vector2(point2.z, point2.x));
		currentPercent = (dist2 / dist1);
		point2.y = Mathf.Lerp(start.y, end.y, currentPercent);
		return true;
		/*
		Segment2 side = new Segment2 (left, right);
		if (Intersection.FindSegment2Segment2 (ref front, ref side, out info)) {
			final = true;
			float dist1 = Vector2.Distance (start.ToVector2XZ (), end.ToVector2XZ ());
			float dist2 = Vector2.Distance (start.ToVector2XZ (), info.Point0);
			currentPercent = (dist2 / dist1);
			point2 = info.Point0.ToVector3XZ ();
			point2.y = Mathf.Lerp (start.y, end.y, currentPercent);
		} else {
			final = false;
		}
		return final;
		*/
	}
	/*
		Math3d.CrossPointLine(start, end, left, right, out point2);

		float dist1 = Vector2.Distance(new Vector2(start.z, start.x), new Vector2(end.z, end.x));
		float dist2 = Vector2.Distance(new Vector2(start.z, start.x), new Vector2(point2.z, point2.x));
		step = (dist2 / dist1);
		point2D.y = Mathf.Lerp(start.y, end.y, step);
		}
*/



	public bool NearestCircleOnCurve(float t, out Segment2Circle2Intr segment2Circle2intr, Vector3 center, float radius, bool isForward)
	{
		segment2Circle2intr = new Segment2Circle2Intr ();
		//float pct = 0;
		//OneCubicBezierCurve curve = GetLocalCurve (t, out pct);

		Circle2 circle2 = new Circle2 (center.ToVector2XZ(), radius);

		if (isForward) {
			for (float i = 0; i < 1.0f; i += _step_n) {

				Vector3 end;
				if (i + _step_n >= 1.0f)
					end = GlobalPos2(1f,0);
				else
					end = GlobalPos2(i + _step_n,0);
			
				Segment2 segment2 = new Segment2 (GlobalPos2(i,0).ToVector2XZ (), end.ToVector2XZ ());
				/*
				GameObject sphere = GameObject.CreatePrimitive (PrimitiveType.Sphere);
				sphere.name = "Segm0F ";
				sphere.transform.position = segment2.P0.ToVector3XZ ();

				sphere = GameObject.CreatePrimitive (PrimitiveType.Sphere);
				sphere.name = "Segm1F ";
				sphere.transform.position = segment2.P1.ToVector3XZ ();
*/
				if (Intersection.FindSegment2Circle2 (ref segment2, ref circle2, out segment2Circle2intr)) {
					return true;
				} 

			}
		} else 
		{
			for (float i = 1.0f; i > 0.0f; i -= _step_n) {

				Vector3 end;
				if (i - _step_n <= 0.0f)
					end = GlobalPos2(0, 0);
				else
					end = GlobalPos2(i - _step_n,0);

				Segment2 segment2 = new Segment2 (GlobalPos2(i,0).ToVector2XZ (), end.ToVector2XZ ());
				/*
				GameObject sphere = GameObject.CreatePrimitive (PrimitiveType.Sphere);
				sphere.name = "Segm0B ";
				sphere.transform.position = segment2.P0.ToVector3XZ ();

				sphere = GameObject.CreatePrimitive (PrimitiveType.Sphere);
				sphere.name = "Segm1B ";
				sphere.transform.position = segment2.P1.ToVector3XZ ();
*/
				if (Intersection.FindSegment2Circle2 (ref segment2, ref circle2, out segment2Circle2intr)) {
					return true;
				} 

			}
		}
		return false;


	}

	void OnDrawGizmos()
	{
		Gizmos.color = Color.white;
		Gizmos.DrawWireSphere (GlobalPos2(0.5f, 0), Length/2f);

		Gizmos.color = Color.blue;
		for (int i = 0; i < curves.Count; i++) 
		{
			Gizmos.DrawWireSphere (transform.TransformPoint(Vector3.Lerp(curves [i].n1.position, curves [i].n2.position, 0.5f)), curves [i].Length / 2);
		}
	}

	public void NearestCircleOnCurveDraw(float t, Vector3 center, float radius, Color color)
	{
		float pct = 0;
		OneCubicBezierCurve curve = GetLocalCurve (t, out pct);

		Circle2 circle2 = new Circle2 (center.ToVector2XZ(), radius);

		for (float i = 0; i < 1.0f; i += curve.step_n) 
		{

			Vector3 end;
			if (i + curve.step_n >= 1.0f)
				end = curve.GetLocation (1);
			else
				end = curve.GetLocation (i + curve.step_n);

			Segment2 segment2 = new Segment2 (curve.GetLocation (i).ToVector2XZ(),  end.ToVector2XZ());

			Segment2Circle2Intr sci = new Segment2Circle2Intr ();
			if (Intersection.FindSegment2Circle2 (ref segment2, ref circle2, out sci)) 
			{
				Gizmos.color = color;
				if (sci.IntersectionType == IntersectionTypes.Point)
					Gizmos.DrawSphere (sci.Point0.ToVector3XZ (), .11f);
				if (sci.IntersectionType == IntersectionTypes.Segment)
				{
					Gizmos.DrawSphere (sci.Point0.ToVector3XZ (), .11f);
					Gizmos.DrawSphere (sci.Point1.ToVector3XZ (), .11f);
				}
				Gizmos.DrawLine (segment2.P0.ToVector3XZ (), segment2.P1.ToVector3XZ ());
			} else {
				Gizmos.color = Color.white;
				Gizmos.DrawLine (segment2.P0.ToVector3XZ (), segment2.P1.ToVector3XZ ());
			}

		}




	}


	public Vector3 UniformGlobalPos(float t, float width)
	{
		float pct = 0;
		OneCubicBezierCurve curve = GetLocalCurve (t, out pct);

		float startTangent = Vector3ex.SignedAngleDeg (Vector3.forward, curve.GetTangent(0), Vector3.up);
		float endTangent = Vector3ex.SignedAngleDeg (Vector3.forward, curve.GetTangent(1), Vector3.up);	

		Vector3 offsetStart = Math3d.RotateThis(curve.n1.width * width, -startTangent);
		Vector3 offsetEnd = Math3d.RotateThis(curve.n2.width * width, -endTangent);

		float currentLenght = 0;
		for (int i = 0; i < 20; i++)
			currentLenght += Vector3.Distance(curve.GetLocation(Mathf.Clamp01((float)i / 20.0f), offsetStart, offsetEnd), curve.GetLocation(Mathf.Clamp01((float)(i + 1) / 20.0f), offsetStart, offsetEnd));   

		Vector3 result = Vector3.zero;
		float _length = currentLenght * pct;

		for (float i = 0; i < currentLenght; i += curve.step)
		{
			float currentStep = Vector3.Distance(curve.GetLocation(Mathf.Clamp01(i / currentLenght), offsetStart, offsetEnd), curve.GetLocation(Mathf.Clamp01((i + curve.step) / currentLenght), offsetStart, offsetEnd));
			if (currentStep < _length)
				_length -= currentStep;
			else
			{
				float percent = _length / currentStep;
				result = Vector3.Lerp(curve.GetLocation(Mathf.Clamp01(i / currentLenght), offsetStart, offsetEnd), curve.GetLocation(Mathf.Clamp01((i + curve.step) / currentLenght), offsetStart, offsetEnd), percent);
				return result;
			}
		}

		return curve.GetLocation(1, offsetStart, offsetEnd);

	}


	public float UniformGlobalLenght(float t)
	{
		float pct = 0;
		OneCubicBezierCurve curve = GetLocalCurve (t, out pct);

		float result = 0;
		float _length = curve.Length * pct;

		for (float i = 0; i <= curve.Length; i += curve.step)
		{
			float currentStep = Vector3.Distance(curve.GetLocation(Mathf.Clamp01(i / curve.Length)), curve.GetLocation(Mathf.Clamp01((i + curve.step) / curve.Length)));
            if (currentStep < _length)
				_length -= currentStep;
			else
			{
				float percent = _length / currentStep;
				result = Mathf.Lerp((i / curve.Length), ((i + curve.step) / curve.Length), percent);
				return GetGlobalCurve(curve, result);
			}
		}

        return GetGlobalCurve(curve, 1);

	}



	/*
	public bool UniformGlobalPosAdd(float t, float dist, out Vector3 point, out float addPercent, out float sumDistance, bool direction)
	{

		float pct = 0;
		OneCubicBezierCurve curve = GetLocalCurve (t, out pct);
		point = Vector3.zero;
		addPercent = 0;
		sumDistance = 0;

		float sourceDistance = dist;
		float inc = curve.step_n;

		if (direction)
		{
			for (float i = pct; i <= 1.0f; i += inc)
			{
				float nextStep = Vector3.Distance(curve.GetLocation(i), curve.GetLocation(Mathf.Clamp01(i + inc)));
				if (sumDistance + nextStep > dist)
				{
					float factor = sourceDistance / nextStep;
					point = curve.GetLocation(i + inc * factor);//Math3d.RotateThis(Math3d.CubicBezierLerp(a, aa, bb, b, i + inc * factor), -transform.rotation.eulerAngles.y, transform.position);
					addPercent = i + inc * factor;

					if (i + inc * factor > 1.0f)
						return false;
					return true;
				}
				else
				{
					sumDistance += nextStep;
					sourceDistance -= nextStep;
				}
			}
		}
		else
		{
			for (float i = pct; i >= 0.0f; i -= inc)
			{
				float nextStep = Vector3.Distance(curve.GetLocation(i), curve.GetLocation(Mathf.Clamp01(i-inc)));

				if (sumDistance + nextStep > dist)
				{
					float factor = sourceDistance / nextStep;
					point = curve.GetLocation(i - inc * factor);
					addPercent = i - inc * factor;

					if (i - inc * factor < 0.0f)
						return false;
					return true;
				}
				else
				{
					sumDistance += nextStep;
					sourceDistance -= nextStep;
				}

			}
		}

		return false;
	}
*/
	public bool UniformGlobalPosAdd(float t, float dist, float width, out Vector3 point, out float addPercent, out float sumDistance, bool direction)
	{
		
		//float pct = 0;
		//OneCubicBezierCurve curve = GetLocalCurve (t, out pct);
		point = Vector3.zero;
		addPercent = 0;
		sumDistance = 0;

		float sourceDistance = dist;
		float inc = 0.01f;

		if (direction)
		{
			for (float i = t; i <= 1.0f; i += inc)
			{
				float nextStep = Vector3.Distance(GlobalPos2(i, width), GlobalPos2(Mathf.Clamp01(i + inc), width));
				if (sumDistance + nextStep > dist)
				{
					float factor = sourceDistance / nextStep;
					point = GlobalPos2(i + inc * factor, width);//Math3d.RotateThis(Math3d.CubicBezierLerp(a, aa, bb, b, i + inc * factor), -transform.rotation.eulerAngles.y, transform.position);
					addPercent = i + inc * factor;

					if (i + inc * factor > 1.0f)
						return false;
					return true;
				}
				else
				{
					sumDistance += nextStep;
					sourceDistance -= nextStep;
				}
			}
		}
		else
		{
			for (float i = t; i >= 0.0f; i -= inc)
			{
				float nextStep = Vector3.Distance(GlobalPos2(i, width), GlobalPos2(Mathf.Clamp01(i-inc), width));

				if (sumDistance + nextStep > dist)
				{
					float factor = sourceDistance / nextStep;
					point = GlobalPos2(i - inc * factor, width);
					addPercent = i - inc * factor;

					if (i - inc * factor < 0.0f)
						return false;
					return true;
				}
				else
				{
					sumDistance += nextStep;
					sourceDistance -= nextStep;
				}

			}
		}

		return false;
	}



	public CameraDot GetNext(float currentPercent)
	{
		if (cameraDots != null)
			for (int i = 0; i < cameraDots.Count; i++) 
			{
				if (cameraDots [i].percent > currentPercent)
					return cameraDots [i];
			}
		return null;
	}

	public CameraDot GetPrevious(float currentPercent)
	{
		if (cameraDots != null) {
			for (int i = cameraDots.Count-1; i >= 0; i--) {
				Debug.Log ("F " + i.ToString() + " " + cameraDots [i].percent + " " + currentPercent);
				if (cameraDots [i].percent < currentPercent)
					return cameraDots [i];
			}

		}
		return null;
	}

}