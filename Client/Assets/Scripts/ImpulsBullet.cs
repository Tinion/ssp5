﻿using UnityEngine;
using System.Collections;

public class ImpulsBullet : RealBullet {

    public LineRenderer trailRenderer;
    int min = 0;
    int max = 4;

	
	// Update is called once per frame
	void Update () {
        RealBulletUpdate();
        trailRenderer.material.SetTextureOffset("_MainTex", new Vector2(Random.Range(min, max) * 0.25f, Random.Range(min, max) * 0.25f));
	}
}
