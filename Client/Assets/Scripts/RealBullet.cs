﻿using UnityEngine;
using System.Collections;

public class RealBullet : Bullet
{
    public LineRenderer lineRenderer;
    public bool isRange = false;

    float currentDistance;
    float currentTargetDistance;
    public override void Initialize(Vector3 _position, Vector3 _direction, Vector3 _parentDir, float _parentSpeed, float _range, Unit _master, float _damage, float _mass)
    {
        attackDir = _direction;
        master = _master;

		attackDir.x += Random.Range(-offset, offset);
        attackDir.y += Random.Range(-offset, offset);
        attackDir.z += Random.Range(-offset, offset);

        parentDir = _parentDir;
        parentSpeed = _parentSpeed;
        startPosition = _position;
        range = _range;
        damage = _damage;
        mass = _mass;

        if (muzzleEffect != null)
        {
            muzzle = GameObject.Instantiate(muzzleEffect, _position, transform.rotation) as GameObject;
            muzzle.transform.parent = master.transform;
        }

        range = Vector3.Distance(startPosition, endPosition);
    }

    public override void SelectTarget(float _distance, Vector3 _targetStartPosition, Vector3 _normal, Vector3 _endPosition, Unit _target, float _damage, float _mass)
    {



    }
	// Use this for initialization
    public override void SelectTarget(float _distance, Vector3 _targetStartPosition, Vector3 _normal, Vector3 _endPosition, OneTarget _target, float _damage, float _mass)
    {



    }

    void Update()
    {

        RealBulletUpdate();




    }

    protected void RealBulletUpdate()
    {
        currentDistance = Vector3.Distance(startPosition, transform.position);
        currentTargetDistance = range;

        if (currentDistance < (currentTargetDistance/2))
        {
            lineRenderer.SetPosition(0, new Vector3(0, 0, 1.5f));
            lineRenderer.SetPosition(1, new Vector3(0, 0, Mathf.Lerp(0, -1.0f, currentDistance / (currentTargetDistance / 2))));
        }
        else
        {
            lineRenderer.SetPosition(0, new Vector3(0, 0, Mathf.Lerp(1.5f, 0, currentDistance / currentTargetDistance)));
            lineRenderer.SetPosition(1, new Vector3(0, 0, -1.0f));
            
        }

       
            transform.position += parentDir * parentSpeed * Time.deltaTime;
            transform.position += attackDir * attackSpeed * Time.deltaTime * 100;
            transform.rotation = Quaternion.LookRotation(attackDir);
        
    }


    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Unit"))
        {

            Unit other = collision.transform.GetComponent<Unit>();
            if (other == master)
                return;


            if (other.unitType == UnitType.Enemy)
            {
                target = other;
                    
                    if (master == Main.instance.hero)
                    {
                        target.AttackByHero();
                        Main.instance.SelectTarget(other);
                    }
                    target.GetDamage(collision.contacts[0].point, damage * transform.forward);
                    target.currentRigidbody.AddForce(mass * 10.0f * Vector3.Normalize(target.transform.position - collision.contacts[0].point), ForceMode.Force);
                    Main.instance._temp2++;
                

                if (impactEffect != null/* && Random.Range(0, 2) == 0*/)
                {
                    impact = GameObject.Instantiate(impactEffect, collision.contacts[0].point, Quaternion.LookRotation(collision.contacts[0].normal)) as GameObject;
                    impact.transform.parent = target.transform;
                }
                //Debug.Log("Unit");
                Destroy(this.gameObject);

            }


        }
        if (collision.gameObject.CompareTag("Solid"))
        {
            if (impactEffect != null/* && Random.Range(0, 2) == 0*/)
            {
                impact = GameObject.Instantiate(impactEffect, collision.contacts[0].point, Quaternion.LookRotation(collision.contacts[0].normal)) as GameObject;

            }
            //Debug.Log("Solid");
            Destroy(this.gameObject);

        }
        

        if ((collision.gameObject.CompareTag("Block")) && (currentDistance > currentTargetDistance * 0.75f))
        {
            if (impactEffect != null)
            {
                impact = GameObject.Instantiate(impactEffect, collision.contacts[0].point, Quaternion.identity) as GameObject;
    
            }
            //Debug.Log("Block");
            Destroy(this.gameObject);
        }
        

        
    }
}
