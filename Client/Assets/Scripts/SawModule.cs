﻿using UnityEngine;
using System.Collections;

[System.Flags]
public enum SawState
{
    Closed,
    ToOpen,
    Attack,
    ToClose,
}
public class SawModule : Module
{

    public Transform leftHand;
    public Transform rightHand;
    public Transform leftSaw;
    public Transform rightSaw;
    public ParticleSystem leftEffect;
	public ParticleSystem rightEffect;

    public float maxSpeed = 400.0f;

    public CounterTimerF openTimer;
    public SawState sawState;

    public float force = 5.0f;
    public float damage = 10.0f;
    public float wear = 0.05f;

   

    private Vector3 startArm = new Vector3(-4.0f, 0.0f, -180.0f);
    private Vector3 leftArm = new Vector3(-4.0f, 0.0f, -230.0f);
    private Vector3 rightArm = new Vector3(-4.0f, 0.0f, -130.0f);
	void ToOpen()
    {
        if (openTimer.isEnd(Time.deltaTime))
        {
            leftHand.localRotation = Quaternion.Euler(leftArm);
            rightHand.localRotation = Quaternion.Euler(rightArm);

            leftSaw.localRotation = Quaternion.Euler(leftSaw.localEulerAngles.x, leftSaw.localEulerAngles.y, leftSaw.localEulerAngles.z + maxSpeed * Time.deltaTime);
            rightSaw.localRotation = Quaternion.Euler(rightSaw.localEulerAngles.x, rightSaw.localEulerAngles.y, rightSaw.localEulerAngles.z - maxSpeed * Time.deltaTime);

            Debug.Log("Open");
            sawState = SawState.Attack;
        }
        else
        {
            leftHand.localRotation = Quaternion.Lerp(Quaternion.Euler(leftArm), Quaternion.Euler(startArm), openTimer.current / openTimer.max);
            rightHand.localRotation = Quaternion.Lerp(Quaternion.Euler(rightArm), Quaternion.Euler(startArm), openTimer.current / openTimer.max);

            float speed = Mathf.Lerp(maxSpeed, 0, openTimer.current / openTimer.max);

            leftSaw.localRotation = Quaternion.Euler(leftSaw.localEulerAngles.x, leftSaw.localEulerAngles.y, leftSaw.localEulerAngles.z + maxSpeed * Time.deltaTime);
            rightSaw.localRotation = Quaternion.Euler(rightSaw.localEulerAngles.x, rightSaw.localEulerAngles.y, rightSaw.localEulerAngles.z - maxSpeed * Time.deltaTime);
        }
    }

    void ToClose()
    {
        if (openTimer.isEnd(Time.deltaTime))
        {
            leftHand.localRotation = Quaternion.Euler(startArm);
            rightHand.localRotation = Quaternion.Euler(startArm);

            Debug.Log("CLOSED");
            sawState = SawState.Closed;
        }
        else
        {
            leftHand.localRotation = Quaternion.Lerp(Quaternion.Euler(startArm), Quaternion.Euler(leftArm), openTimer.current / openTimer.max);
            rightHand.localRotation = Quaternion.Lerp(Quaternion.Euler(startArm), Quaternion.Euler(rightArm), openTimer.current / openTimer.max);

            float speed = Mathf.Lerp(0, maxSpeed, openTimer.current / openTimer.max);

            leftSaw.localRotation = Quaternion.Euler(leftSaw.localEulerAngles.x, leftSaw.localEulerAngles.y, leftSaw.localEulerAngles.z + maxSpeed * Time.deltaTime);
            rightSaw.localRotation = Quaternion.Euler(rightSaw.localEulerAngles.x, rightSaw.localEulerAngles.y, rightSaw.localEulerAngles.z - maxSpeed * Time.deltaTime);
        }
    }

    void Update()
    {
        switch (sawState)
        {
            case SawState.ToOpen:
                ToOpen();
                break;
            case SawState.ToClose:
                ToClose();
                break;
            case SawState.Attack:
                leftSaw.localRotation = Quaternion.Euler(leftSaw.localEulerAngles.x, leftSaw.localEulerAngles.y, leftSaw.localEulerAngles.z + 400 * Time.deltaTime);
                rightSaw.localRotation = Quaternion.Euler(rightSaw.localEulerAngles.x, rightSaw.localEulerAngles.y, rightSaw.localEulerAngles.z - 400 * Time.deltaTime);
                if (Input.GetButtonDown("Special03"))
                {
                    Close();
                }
                break;
            case SawState.Closed:
                if (Input.GetButtonDown("Special03"))
                {
                    Open();
                }
                break;
        }
    }

    public void Open()
    {
        sawState = SawState.ToOpen;
        openTimer.Restore();
    }

    public void Close()
    {
        sawState = SawState.ToClose;
        openTimer.Restore();
    }

    void Start()
    {
        Open();
    }

    void FixedUpdate()
    {
		leftEffect.Stop ();
		rightEffect.Stop ();
    }
    void OnTriggerStay(Collider other)
    {
        if (sawState == SawState.Attack)
        {
            if (other.CompareTag("Unit"))
            {
                Unit unit = other.transform.parent.GetComponent<Unit>();
                if (unit.unitType == UnitType.Enemy)
                {
                    Vector3 pos = unit.transform.position - transform.position / 2;
                    unit.currentRigidbody.AddForceAtPosition(Vector3.Normalize(unit.transform.position - transform.position) * force, pos, ForceMode.Acceleration);
                    unit.GetDamage(damage * unit.healthPoints.max);

                    Vector3 relative = transform.InverseTransformPoint(unit.transform.position);
					if (relative.x > 0)
						leftEffect.Play ();
					else
						rightEffect.Play ();

                    float selfDamage = 0;
                    if (Math3d.GetChanceOf((int)(wear * 100)))
                        selfDamage = Random.Range(0.0f, damage*50.0f);
                    if (durability.current > selfDamage)
                        durability.current -= selfDamage;
                    else
                    {
                        Main.instance.hero.moduleSystem.DamageModule(ModuleSlot.Down, !Math3d.GetChanceOf(Main.instance.inventory.items[number].destructionChance));
                    }

                }
            }
        }
    }

   
   
}
