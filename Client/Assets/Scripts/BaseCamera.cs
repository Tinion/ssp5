﻿using UnityEngine;
using System.Collections;

[System.Flags]
public enum BaseCamMode
{
    Base = 0,
    ToPrepare = 1,
    Prepare = 2,
    ToBattle = 3,
    Battle = 4,
}
public class BaseCamera : MonoBehaviour {

    public BaseCamMode mode;
    public Transform transform;
    private Vector3 startPos;
    private Vector3 startDir;
    private Quaternion startRot;
    private Vector3 leftPos = new Vector3(22.1f, 2.207f, -2.5f);//25.0f);
    private Vector3 rightPos = new Vector3(25.28f, 2.207f, -1.8f);// -50.0f);
    private Vector3 insidePos = new Vector3(21.51f, 2.154f, -0.833f);// -50.0f);
    private Vector3 leftRot = new Vector3(20.8f, 25.0f, 0.0f);//25.0f);
    private Vector3 rightRot = new Vector3(20.8f, -50.0f, 0.0f);// -50.0f);
    private Vector3 insideRot = new Vector3(34.8f, 58.80005f, 0.0f);// -50.0f);
    private Vector3 _currentSmoothVelocityRotation = Vector3.zero;
    private Vector3 _currentSmoothVelocityPosition = Vector3.zero;
    public float side = 0;
    private float currentSide;
    private float nextSide;
    private Vector3 localPos = new Vector3(0, 3.4f, -4.4f);
    private Vector3 localRot = new Vector3(0, -0.5f, 1.0f);
    public Material hero;
    public Material heroOutlined;
    public CounterTimerF baseToPrepareTimer;
    public CounterTimerF baseRotateTimer;
    public CounterTimerF insideRotateTimer;
    public bool isRotation = false;
    public bool isOpen = false;
    public bool isHoodOpen = false;
    public Transform hood;
	// Use this for initialization
	public void Prepare()
    {
        startPos = transform.position;
        startDir = transform.forward;
        mode = BaseCamMode.ToPrepare;
        baseToPrepareTimer.Restore();
    }

    public void ToBattle()
    {
        baseToPrepareTimer.max = 2.0f;
        startRot = transform.rotation;
        startPos = transform.position;
        mode = BaseCamMode.ToBattle;
        baseToPrepareTimer.Restore();
    }

    public void ReturnToStartPos()
    {
        gameObject.SetActive(true);
        mode = BaseCamMode.Base;

        _currentSmoothVelocityRotation = Vector3.zero;
        _currentSmoothVelocityPosition = Vector3.zero;

        side = 0;
        transform.position = leftPos;
        transform.rotation = Quaternion.Euler(leftRot);

        baseToPrepareTimer.max = 6.0f;
        baseToPrepareTimer.Restore();
    }
    public void BaseRotate(float _next)
    {
        nextSide = _next;
        currentSide = side;
        baseRotateTimer.Restore();
        isRotation = true;
    }

	/*
    public void ToInside()
    {
        if (!isOpen && !Main.instance.baseCamera.isRotation)
        {
            startRot = transform.rotation;
            startPos = transform.position;
            isOpen = true;
            isHoodOpen = false;

            Main.instance.baseCamera.BaseRotate(currentSide);

        }
    }
	*/

    bool isToolTipExist()
    {
        bool deleteTooltip = false;

        for (int i = 0; i < Main.instance.inventory.inventorySlots.Length; i++)
            if (Main.instance.inventory.inventorySlots[i].UpdateSlot())
                deleteTooltip = true;

        for (int i = 0; i < Main.instance.inventory.inventoryObjects.Length; i++)
            if (Main.instance.inventory.inventoryObjects[i].slot.UpdateSlot())
                deleteTooltip = true;



        return deleteTooltip;
    }
	// Update is called once per frame
	void Update () {
	    
        switch (mode)
        {
            case BaseCamMode.Base:
                {

                    if (!isToolTipExist())
                        Main.instance.inventory.moduleTooltip.gameObject.SetActive(false);
                    else
                        Main.instance.inventory.moduleTooltip.gameObject.SetActive(true);

                        
                    Vector3 bPos;
                    Vector3 bRot;

                    if (isRotation)
                    {
                        if (!isOpen)
                        {
                            bPos = Vector3.Lerp(leftPos, rightPos, Mathf.Lerp(nextSide, currentSide, baseRotateTimer.current / baseRotateTimer.max));
                            bRot = Vector3.Lerp(leftRot, rightRot, Mathf.Lerp(nextSide, currentSide, baseRotateTimer.current / baseRotateTimer.max));

                            if (baseRotateTimer.isEnd(Time.deltaTime))
                            {
                                side = nextSide;
                                isRotation = false;
                            }
                        }
                        else
                        {
                            //
                            
                            //else
                            //hood.localRotation = Quaternion.Lerp(Quaternion.identity, new Quaternion(0.5f, 0.0f, 0.0f, -0.866f), Mathf.Lerp(nextSide, currentSide, baseRotateTimer.current / baseRotateTimer.max));
                            if (!isHoodOpen)
                            {
                                hood.localRotation = Quaternion.Lerp(new Quaternion(0.5f, 0.0f, 0.0f, -0.866f), Quaternion.identity, Mathf.Lerp(0, 1, baseRotateTimer.current / baseRotateTimer.max));
                                bPos = Vector3.Lerp(insidePos, startPos, Mathf.Lerp(0, 1, baseRotateTimer.current / baseRotateTimer.max));
                                bRot = Quaternion.Slerp(Quaternion.Euler(insideRot), startRot, Mathf.Lerp(0, 1, baseRotateTimer.current / baseRotateTimer.max)).eulerAngles;
                            }
                            else
                            {
                                hood.localRotation = Quaternion.Lerp(new Quaternion(0.5f, 0.0f, 0.0f, -0.866f), Quaternion.identity, Mathf.Lerp(1, 0, baseRotateTimer.current / baseRotateTimer.max));
                                bPos = Vector3.Lerp(insidePos, Vector3.Lerp(leftPos, rightPos, nextSide), Mathf.Lerp(1, 0, baseRotateTimer.current / baseRotateTimer.max));
                                bRot = Quaternion.Slerp(Quaternion.Euler(insideRot), Quaternion.Euler(Vector3.Lerp(leftRot, rightRot, nextSide)), Mathf.Lerp(1, 0, baseRotateTimer.current / baseRotateTimer.max)).eulerAngles;
                            }

                            if (baseRotateTimer.isEnd(Time.deltaTime))
                            {
                                if (isOpen && isHoodOpen)
                                {
                                    isOpen = false;

                                }
                                side = nextSide;
                                isRotation = false;
                            }
                        }

                        
                    }
                    else
                    {
                        if (!isOpen)
                        {
                            //if (!isHoodOpen)
                            //{
                            //    //hood.localRotation = Quaternion.identity;
                           //     bPos = Vector3.Lerp(leftPos, rightPos, side);
                            //    bRot = Vector3.Lerp(leftRot, rightRot, side);
                            //}
                            //else
                            {
                                bPos = Vector3.Lerp(leftPos, rightPos, side);
                                bRot = Vector3.Lerp(leftRot, rightRot, side);

                            }
                        }
                        else
                        {

                            //hood.localRotation = new Quaternion(0.5f, 0.0f, 0.0f, -0.866f);
                            bPos = insidePos;
                            bRot = insideRot;
                        }
                    }

                    transform.position = Vector3.SmoothDamp(transform.position, bPos, ref _currentSmoothVelocityPosition, 0.2f);

                    Vector3 SmoothRot = Vector3.zero;
                    SmoothRot.x = Mathf.SmoothDampAngle(transform.eulerAngles.x, bRot.x, ref _currentSmoothVelocityRotation.x, 0.25f);
                    SmoothRot.y = Mathf.SmoothDampAngle(transform.eulerAngles.y, bRot.y, ref _currentSmoothVelocityRotation.y, 0.25f);
                    SmoothRot.z = Mathf.SmoothDampAngle(transform.eulerAngles.z, bRot.z, ref _currentSmoothVelocityRotation.z, 0.25f);
                    transform.rotation = Quaternion.Euler(SmoothRot);
                }
                break;
            case BaseCamMode.ToPrepare:
                {
                 if (isOpen)
                     hood.localRotation = Quaternion.Lerp(new Quaternion(0.5f, 0.0f, 0.0f, -0.866f), Quaternion.identity, Mathf.Lerp(1, 0, baseToPrepareTimer.current / baseToPrepareTimer.max));

				Vector3 bPos = Vector3.Lerp(Main.instance.hero.transform.TransformPoint(localPos), startPos, baseToPrepareTimer.current / baseToPrepareTimer.max);
                 transform.position = Vector3.SmoothDamp(transform.position, bPos, ref _currentSmoothVelocityPosition, 0.2f);

				Quaternion bRot = Quaternion.LookRotation(Vector3.Lerp(Main.instance.hero.transform.TransformDirection(localRot), startDir, baseToPrepareTimer.current / baseToPrepareTimer.max));
                 transform.rotation = Quaternion.Slerp(transform.rotation, bRot, Time.deltaTime * 12.0f);

                 if (baseToPrepareTimer.isEnd(Time.deltaTime))
                 {
                  _currentSmoothVelocityPosition = Vector3.zero;
                  _currentSmoothVelocityRotation = Vector3.zero;
                  ToBattle();
                 }
                }
                break;
              case BaseCamMode.ToBattle:
                {
				Vector3 bPos = Vector3.Slerp(Main.instance.mainCamera.playerAffectCamera.position, Main.instance.hero.transform.TransformPoint(localPos),baseToPrepareTimer.current/baseToPrepareTimer.max);// Mathf.Clamp(baseToPrepareTimer.current * 1.0f, 0, baseToPrepareTimer.max) / baseToPrepareTimer.max);
                    transform.position = Vector3.SmoothDamp(transform.position, bPos, ref _currentSmoothVelocityPosition, 0.05f);

                    //Vector3 bPos = Vector3.Lerp(startPos, Main.instance.mainCamera.localCam.position, baseToPrepareTimer.current / baseToPrepareTimer.max);
                    //transform.position = Vector3.Slerp(transform.position, bPos, Time.smoothDeltaTime);

                    Quaternion bRot = Quaternion.Slerp(Main.instance.mainCamera.playerAffectCamera.rotation, startRot, baseToPrepareTimer.current / baseToPrepareTimer.max);
                    transform.rotation = Quaternion.Slerp(transform.rotation, bRot, Time.smoothDeltaTime * 18.0f);
                    /*
                    Vector3 SmoothRot = Vector3.zero;
                    SmoothRot.x = Mathf.SmoothDampAngle(transform.eulerAngles.x, bRot.x, ref _currentSmoothVelocityRotation.x, 0.15f);
                    SmoothRot.y = Mathf.SmoothDampAngle(transform.eulerAngles.y, bRot.y, ref _currentSmoothVelocityRotation.y, 0.15f);
                    SmoothRot.z = Mathf.SmoothDampAngle(transform.eulerAngles.z, bRot.z, ref _currentSmoothVelocityRotation.z, 0.15f);
                    transform.rotation = Quaternion.Euler(SmoothRot);
                    */
                    if (baseToPrepareTimer.isEnd(Time.deltaTime))
                    {
                        mode = BaseCamMode.Battle;
                    }
                }
                break;
            case BaseCamMode.Battle:
                {
                    gameObject.SetActive(false);
                }
                break;
        }

	}
}
