﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Dest.Math;

public class BakedProps : Props {

	public BlockPlane parentBlockPlane;
	public int selectedType = 0;
	public int type = 0;
	public bool isActive = true;
	public Transform transform;
	public Transform previous;
	public Transform next;
	public bool edge = false;
	public Matrix4x4 myTransform;

	public void Create(BlockPlane _parent, int _type)
	{
		parentBlockPlane = _parent;
		type = _type;
		selectedType = type;
		transform = transform;

	}

	public void Awake()
	{
		transform = transform;
	}

	public virtual void OnTriggerEnter(Collider other)
	{
		if (isActive)
		if (other.gameObject.CompareTag ("Unit")) 
		{
			isActive = false;
			collider.isTrigger = true;
			//collision.rigidbody.AddForceAtPosition (collision.relativeVelocity * 0.25f, collision.contacts [0].point, ForceMode.Impulse);
			Corpse InstaCorpse = Instantiate<Corpse> (corpse);
			Vector3 pos = new Vector3(myTransform.m03,myTransform.m13,myTransform.m23);
			Quaternion rotation = Quaternion.identity;
			Matrix4x4ex.RotationMatrixToQuaternion(ref myTransform, out rotation);
			InstaCorpse.transform.rotation = rotation * parentBlockPlane.transform.rotation;
			InstaCorpse.transform.position = parentBlockPlane.transform.TransformPoint (pos);
			InstaCorpse.center = transform.GetComponent<BoxCollider>().ClosestPointOnBounds(other.transform.position);

			other.attachedRigidbody.AddForceAtPosition (pos, other.attachedRigidbody.GetRelativePointVelocity (pos), ForceMode.Impulse);
			Debug.DrawLine (pos, pos + other.attachedRigidbody.GetRelativePointVelocity (pos), Color.white, 5.0f);
			
			parentBlockPlane.BakeBorders ();
		}
	}

	public void Clean()
	{
		MeshFilter MF = GetComponent<MeshFilter> ();
		if (MF != null)
			GameObject.DestroyImmediate(MF);

		MeshRenderer MR = GetComponent<MeshRenderer> ();
		if (MR != null)
			GameObject.DestroyImmediate(MR);
	}
}
