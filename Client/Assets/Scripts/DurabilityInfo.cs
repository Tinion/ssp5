﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class DurabilityInfo : MonoBehaviour {

    public DurabilityInfoModule[] durabilityInfoModules;
    public Color max;
    public Color average;
    public Color min;

    public RectTransform currentRectTransform;
    public Slider healthBarSlider;
    public Text healthBarSliderText;
    public Image healthBarSliderFill;

    public Text sideArmors;
    public Text armor;

    public void UpdateDurabilityInfo()
    {
        for (int i = 0; i < durabilityInfoModules.Length; i++)
        {
            Module current = Main.instance.hero.moduleSystem.module[i];
            if (current != null)
            {
                if (!current.isDamaged)
                {
                    durabilityInfoModules[i].durabilityText.text = current.durability.current.ToString("F0") + "/" + current.durability.max.ToString("F0");
                    durabilityInfoModules[i].durability.value = current.durability.current / current.durability.max;
                    if (durabilityInfoModules[i].durability.value > 0.5f)
                        durabilityInfoModules[i].durabilityFill.color = Color.Lerp(average, max, (durabilityInfoModules[i].durability.value - 0.5f) * 2.0f);
                    else
                        durabilityInfoModules[i].durabilityFill.color = Color.Lerp(min, average, durabilityInfoModules[i].durability.value * 2.0f);
                }
                else
                {
                    durabilityInfoModules[i].durabilityText.text = "Поврежден";
                    durabilityInfoModules[i].durability.value = 1.0f;
                    durabilityInfoModules[i].durabilityFill.color = min;
                    durabilityInfoModules[i].durabilityImage.color = min;
                }

            }
        }

		healthBarSlider.value = Main.instance.hero.vehicle.healthPoints.current / Main.instance.hero.vehicle.healthPoints.max;
		healthBarSliderText.text = Main.instance.hero.vehicle.healthPoints.current.ToString("F0") + "/" + Main.instance.hero.vehicle.healthPoints.max.ToString("F0");
        //healthBarSliderFill.color =  Color.Lerp(min, max, healthBarSlider.value);
        if (healthBarSlider.value>0.5f)
            healthBarSliderFill.color = Color.Lerp(average, max, (healthBarSlider.value - 0.5f) * 2.0f);
        else
            healthBarSliderFill.color = Color.Lerp(min, average, healthBarSlider.value * 2.0f);

        sideArmors.text = Main.instance.hero.sideArmor[0].ToString("F0") + "\n";
        sideArmors.text += Main.instance.hero.sideArmor[1].ToString("F0") + "\n";
        sideArmors.text += Main.instance.hero.sideArmor[2].ToString("F0");

        armor.text = Main.instance.hero.currentArmor.ToString("F0");
    }

    public void CreateDurabilityInfo()
    {
        
        for (int i=0; i<durabilityInfoModules.Length; i++)
        {
            Module current = Main.instance.hero.moduleSystem.module[i];

            if (current!=null)
            {
                if (!current.isDamaged)
                {
                    durabilityInfoModules[i].transform.gameObject.SetActive(true);
                    durabilityInfoModules[i].durabilityText.text = current.durability.current.ToString("F0") + "/" + current.durability.max.ToString("F0");
                    durabilityInfoModules[i].durabilityImage.color = Color.white;
                    durabilityInfoModules[i].durabilityImage.sprite = Main.instance.inventory.items[current.number].durabilityImage;
                    durabilityInfoModules[i].durability.value = current.durability.current / current.durability.max;
                    if (durabilityInfoModules[i].durability.value > 0.5f)
                        durabilityInfoModules[i].durabilityFill.color = Color.Lerp(average, max, (durabilityInfoModules[i].durability.value - 0.5f) * 2.0f);
                    else
                        durabilityInfoModules[i].durabilityFill.color = Color.Lerp(min, average, durabilityInfoModules[i].durability.value * 2.0f);
                }
                else
                {
                    durabilityInfoModules[i].transform.gameObject.SetActive(true);
                    durabilityInfoModules[i].durabilityText.text = "Поврежден";
                    durabilityInfoModules[i].durability.value = 1.0f;
                    durabilityInfoModules[i].durabilityFill.color = min;
                    durabilityInfoModules[i].durabilityImage.color = min;
                }
            }
            else
            {
                durabilityInfoModules[i].transform.gameObject.SetActive(false);
            }
        }

		healthBarSlider.value = Main.instance.hero.vehicle.healthPoints.current / Main.instance.hero.vehicle.healthPoints.max;
		healthBarSliderText.text = Main.instance.hero.vehicle.healthPoints.current.ToString("F0") + "/" + Main.instance.hero.vehicle.healthPoints.max.ToString("F0");
        if (healthBarSlider.value > 0.5f)
            healthBarSliderFill.color = Color.Lerp(average, max, (healthBarSlider.value - 0.5f) * 2.0f);
        else
            healthBarSliderFill.color = Color.Lerp(min, average, healthBarSlider.value * 2.0f);

        sideArmors.text = Main.instance.hero.sideArmor[0].ToString("F0") + "\n";
        sideArmors.text += Main.instance.hero.sideArmor[1].ToString("F0") + "\n";
        sideArmors.text += Main.instance.hero.sideArmor[2].ToString("F0");

        armor.text = Main.instance.hero.currentArmor.ToString("F0");
    }
}
