﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Dest.Math;

public class CarPathButton : MonoBehaviour {

    public Transform carPath;
    public RectTransform current;
    public UnityEngine.Renderer[] renderer;
    public ModuleSlot slotType;
    public UnityEngine.UI.Image currentImage;
    public UnityEngine.UI.Image currentIcon;
    public float currentSide;
    public bool isActive = false;
    public float lenght = 200.0f;
	public float angle;
	public Vector3 dist;
	public Vector3 targetRotation;

    public durabilitySlider durSlider;
    
    public bool UpdateSlot()
    {
        bool result = false;

        if (!Main.instance)
            Main.instance = FindObjectOfType<Main>();


		Vector2 CenterPos = Main.instance.mainCamera.roadAffectCamera.WorldToScreenPoint(Main.instance.hero.centerUIpos.position).ToVector2XY();
		Vector2 pos = Main.instance.mainCamera.roadAffectCamera.WorldToScreenPoint(carPath.position).ToVector2XY();
        Vector2 dir = pos - CenterPos;
        dir.Normalize();
        Vector2 currentPos = pos + dir * lenght;

       // current.anchoredPosition = currentPos;
	
		if (Input.mousePosition.x > current.transform.position.x - current.sizeDelta.x / 2 && Input.mousePosition.x < current.transform.position.x + current.sizeDelta.x / 2 && Input.mousePosition.y > current.transform.position.y - current.sizeDelta.y / 2 && Input.mousePosition.y < current.transform.position.y + current.sizeDelta.y / 2)
        {
			

            if (Input.GetMouseButtonUp(0))
            {
                ClickThis();
            }

            if (isActive)
            {

                float dY = Main.instance.inventory.moduleTooltip.currentRectTransform.sizeDelta.y / 2;
                float dX = Main.instance.inventory.moduleTooltip.currentRectTransform.sizeDelta.x / 2;

                float add_Y = 0;
                if (current.anchoredPosition.y < dY)
                    add_Y = dY - current.anchoredPosition.y;
                else
                    if (Screen.height - 100.0f - current.anchoredPosition.y < dY)
                        add_Y = (Screen.height - 100.0f - current.anchoredPosition.y) - dY;

                float add_X = 0;

                if (Screen.width - 530.0f - current.anchoredPosition.x < dX)// + 270.0f)
                    add_X = (Screen.width - 530.0f - current.anchoredPosition.x) - dX;//(dX + 270.0f);

                current.SetAsLastSibling();
                Main.instance.inventory.shop.button01.SetAsLastSibling();
                Main.instance.inventory.shop.button02.SetAsLastSibling();
                Main.instance.inventory.moduleTooltip.ShowModuleInfo(Main.instance.hero.moduleSystem.module[(int)slotType], current, new Vector2(add_X + 250.0f, add_Y));

                if (slotType != ModuleSlot.Wheels)
                {
                    Material[] mat = Main.instance.hero.moduleSystem.module[(int)slotType].currentRenderer.materials;
                    if (Input.GetMouseButton(1))
                    {

                        for (int i = 0; i < mat.Length; i++)
                            mat[i].shader = Main.instance.inventory.standard;
                            Main.instance.hero.moduleSystem.UnEquipModule(slotType);
                    }
                    else
                    {
                        for (int i = 0; i < mat.Length; i++)
                            mat[i].shader = Main.instance.inventory.outline;
                    }
                }
                else
                {
                    if (Input.GetMouseButton(1))
                    {
                        for (int i = 0; i < renderer.Length; i++)
                        {
                            Material[] mat = renderer[i].materials;
                            for (int j = 0; j < mat.Length; j++)

                                mat[j].shader = Main.instance.inventory.standard;
                        }
                        Main.instance.hero.moduleSystem.UnEquipModule(slotType);
                    }
                    else
                    {
                        for (int i = 0; i < renderer.Length; i++)
                        {
                            Material[] mat = renderer[i].materials;
                            for (int j = 0; j < mat.Length; j++)
                                mat[j].shader = Main.instance.inventory.outline;
                        }

                    }


                }

                result = true;

            }
            else
                result = false;

            currentImage.color = Color.red;
            for (int i = 0; i < renderer.Length; i++)
				renderer[i].material = Main.instance.inventory.heroOutlined;



        }
        else
        {
            result = false;
            if (isActive)
            {
                if (slotType != ModuleSlot.Wheels)
                {
                    Material[] mat = Main.instance.hero.moduleSystem.module[(int)slotType].currentRenderer.materials;
                    for (int i = 0; i < mat.Length; i++)
                        mat[i].shader = Main.instance.inventory.standard;
                }
                else
                {
                    for (int i = 0; i < renderer.Length; i++)
                    {
                        Material[] mat = renderer[i].materials;
                        for (int j = 0; j < mat.Length; j++)
                            mat[j].shader = Main.instance.inventory.standard;
                    }
                }

            }

            currentImage.color = Color.white;
            for (int i = 0; i < renderer.Length; i++)
				renderer[i].material = Main.instance.inventory.hero;

        }

        return result;
    }

    public virtual void ClickThis()
    {


		//Main.instance.mainCamera.BaseRotate(slotType);
        Main.instance.inventory.shop.button01.SetAsLastSibling();
        Main.instance.inventory.shop.button02.SetAsLastSibling();
    }
}
