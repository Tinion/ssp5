﻿using UnityEngine;
using System.Collections;

public class EnemyTarget : MonoBehaviour {

	// Use this for initialization

    public Transform transform;
    Vector3 oldPosition;
    protected bool isTextDropping = false;
    protected float textDropTimer = 0.0f;
    protected Vector3 TextPos = Vector3.zero;
    protected string droppingText;

    public float dist = 0.0f;

    void FixedUpdate()
    {
        dist = Vector3.Distance(oldPosition, transform.position);
        oldPosition = transform.position;
        
    }



    protected void DropText(string text)
    {
        isTextDropping = true;
        TextPos = transform.position;
        droppingText = text;
        textDropTimer = 1.0f;
    }

    void OnGUI()
    {
        if (isTextDropping)
        {
            Vector3 Pos = Main.instance.mainCamera.roadAffectCamera.WorldToScreenPoint(TextPos);
            GUI.Label(new Rect((int)Pos.x, Screen.height - (int)Pos.y + 10, 90, 120), droppingText);


            textDropTimer -= Time.deltaTime;
            if (textDropTimer < 0.0f)
            {
                isTextDropping = false;
            }
        }
    }
}
