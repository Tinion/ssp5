﻿using UnityEngine;
using System.Collections;

public class GrassObject : MonoBehaviour 
{

	public bool isCalculate;
	public MeshFilter mf;
	public int vertexCount;
	public Material material;

	[ContextMenu("Calculate")]
	public void Calculate()
	{
		mf = GetComponent<MeshFilter> ();
		material = GetComponent<Renderer> ().sharedMaterial;

		vertexCount = mf.sharedMesh.vertexCount;

		isCalculate = true;
	}
	
}
