﻿using UnityEngine;
using System.Collections;

public class ImpulseTurret : MachineGun
{
    float currentVelocitySecond = 0.0f;
    public Gun gunSecond;
    GameObject effectFireLeft;
    GameObject effectFireRight;
	// Use this for initialization
    void Update()
    {
        WeaponUpdate();
    }

    public override void WeaponUpdate()
    {
        if (gunSecond.master == null)
			gunSecond.master = Main.instance.hero.vehicle;
        if (Main.instance.gameMode == Mode.Battle)
        {
            //gun.transform.localRotation = Quaternion.identity;
            //gun.transform.localRotation = Quaternion.Euler(new Vector3(0.0f, 0.0f/*Mathf.Clamp(Mathf.Abs(angle), 0, 180.0f)*/, 0.0f));
            //gunSecond.transform.localRotation = Quaternion.Euler(new Vector3(0.0f, -Mathf.Clamp(Mathf.Abs(angle), 0, 180.0f), 0.0f));
            //gun.transform.rotation = Quaternion.LookRotation(Vector3.Normalize(mousePos - gun.transform.position));
            //gunSecond.transform.rotation = Quaternion.LookRotation(Vector3.Normalize(mousePos - gunSecond.transform.position));

            
            Vector3 relative = transform.InverseTransformPoint(mousePos);
            if (relative.x > 0)
            {
                Quaternion rot = Quaternion.LookRotation(Vector3.Normalize(mousePos - gun.transform.position));
                Vector3 rotV = rot.eulerAngles;
                gun.transform.rotation = rot;
                gunSecond.transform.rotation = rot;

                gun.transform.localRotation = Quaternion.Euler(gun.transform.localEulerAngles.x, Mathf.Clamp(Mathf.Abs(angle), 0, 180.0f), gun.transform.localEulerAngles.z);
                gunSecond.transform.localRotation = Quaternion.Euler(gunSecond.transform.localEulerAngles.x, -Mathf.Clamp(Mathf.Abs(angle), 0, 180.0f), gunSecond.transform.localEulerAngles.z);

                
            }
            else
            {
                Quaternion rot = Quaternion.LookRotation(Vector3.Normalize(mousePos - gunSecond.transform.position));
                Vector3 rotV = rot.eulerAngles;
                gun.transform.rotation = rot;
                gunSecond.transform.rotation = rot;

                gun.transform.localRotation = Quaternion.Euler(gun.transform.localEulerAngles.x, Mathf.Clamp(Mathf.Abs(angle), 0, 180.0f), gun.transform.localEulerAngles.z);
                gunSecond.transform.localRotation = Quaternion.Euler(gunSecond.transform.localEulerAngles.x, -Mathf.Clamp(Mathf.Abs(angle), 0, 180.0f), gunSecond.transform.localEulerAngles.z);

            }
            

            ///gun.transform.localRotation = Quaternion.Euler(new Vector3(0.0f, Mathf.SmoothDampAngle(gun.transform.localEulerAngles.y, Mathf.Clamp(Mathf.Abs(angle), 0, 180.0f), ref currentVelocity, rotateSpeed)));
           
          // gunSecond.transform.localRotation = Quaternion.Euler(new Vector3(0.0f, Mathf.SmoothDampAngle(gunSecond.transform.localEulerAngles.y, -Mathf.Clamp(Mathf.Abs(angle), 0, 180.0f), ref currentVelocitySecond, rotateSpeed)));

        }
    }

    public override void CreateAutoEffect()
    {
        if (!isEffectCreated && effectFire != null)
        {
            isEffectCreated = true;
            effectFireLeft = GameObject.Instantiate(effectFire, transform.TransformPoint(gun.shootPosition), transform.rotation) as GameObject;
            effectFireLeft.transform.parent = transform;
            effectFireRight = GameObject.Instantiate(effectFire, transform.TransformPoint(gunSecond.shootPosition), transform.rotation) as GameObject;
            effectFireRight.transform.parent = transform;
        }

    }
    public override void StopShoot()
    {
        if (isAttack)
        {
            weaponState = WeaponState.Stop;
            isAttack = false;
            DestroyAutoEffect();
        }
    }

    public override void Reset()
    {
        base.Reset();
        gun.transform.localRotation = Quaternion.identity;
        gunSecond.transform.localRotation = Quaternion.identity;
        gunSecond.magazine.Restore();
        gunSecond.recharge.Restore();
        DestroyAutoEffect();
    }

    public override void DestroyAutoEffect()
    {
        if (isEffectCreated)
        {

            Destroy(effectFireLeft);
            Destroy(effectFireRight);
            isEffectCreated = false;
        }


    }

    public override void GunShoot()
    {
        Vector3 shootPosition = gun.GetShootPosition(gun.transform);
        Vector3 shootRotation = gun.transform.forward;
        //Unit target = Aim(shootRotation, shootPosition, aimRange);
        gun.ShootBurst(null, shootRotation, shootPosition);
        
        Vector3 shootPosition2 = gunSecond.GetShootPosition(gunSecond.transform);
        Vector3 shootRotation2 = gunSecond.transform.forward;
        //Unit target2 = Aim(shootRotation2, shootPosition2, aimRange);
        gunSecond.ShootBurst(null, shootRotation2, shootPosition2);

        Main.instance._temp++;

    }
}
