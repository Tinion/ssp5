﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hangar : MonoBehaviour {

	public Vector3 heroPosition;
	public Quaternion heroRotaion;
	public BoundsCorrector[] boundCorrectors;
	public StartController sc;
	// Use this for initialization
	public void Summon()
	{

		gameObject.SetActive (true);
		sc.isCheck = false;
		sc.sctype = StartControllerType.ReturnToBaseActivator;
		transform.position = Main.instance.currentOutpost.transform.position;
		transform.rotation = Main.instance.currentOutpost.transform.rotation;

		for (int i = 0; i < boundCorrectors.Length; i++) 
		{
			boundCorrectors[i].Resize ();
		}
	}

	public void Hide()
	{
		gameObject.SetActive (false);
	}
}
