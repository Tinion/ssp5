﻿using UnityEngine;
using System.Collections;

public class ModuleWeapon : Module
{


    
    
    public override void Install()
    {
        base.Install();
		weapon.gun.master = Main.instance.hero.vehicle;
        //weapons = GameObject.Find("WeaponSlider").GetComponent<UnityEngine.UI.Slider>();
        
    }

    public override void Remove()
    {
        base.Remove();
        Main.instance.hero.moduleSystem.moduleWeapon.Remove(this);
        weapon.Remove();
    }
    public override void Reset()
    {
        base.Reset();
        weapon.Reset();
    }
    void Update()
    {
        if (Main.instance==null)
            Main.instance = FindObjectOfType<Main>();

        if (Main.instance.gameMode != Mode.Battle)
            return;

            if (weapon.weaponState == WeaponState.Recharge)
            {
                ammoSource.ico.color = new Color(1.0f, 1.0f, 1.0f, 0.2f);
                ammoSource.back.fillAmount = 1.0f - ((float)weapon.gun.recharge.current / (float)weapon.gun.recharge.max);
                ammoSource.data.text = "Перезарядка";
                
            }
            else
            {
                ammoSource.ico.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
                ammoSource.back.fillAmount = ((float)weapon.gun.magazine.current / (float)weapon.gun.magazine.max);
                ammoSource.data.text = weapon.gun.magazine.current.ToString("F0") + "/" + weapon.gun.magazine.max.ToString("F0");
            }
        
    }   
  

    
}
