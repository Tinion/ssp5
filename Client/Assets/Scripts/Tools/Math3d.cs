﻿using UnityEngine;
using System.Collections;
using Dest.Math;
#if UNITY_EDITOR
using UnityEditor;
#endif

[System.Serializable]
public class Twin
{

    public float param;
    public Transform tr;

    public Twin()
    {
        param = 0.0f;
        tr = null;
    }
}

[System.Serializable]
public class Math3d
{
    public static Vector2 Perpendicular(Vector2 original)
    {

        Vector2 Temp;//To create a perpendicular vector switch X and Y, then make Y negative
        Temp.y = original.x;
        Temp.x = original.y;

        Temp.x = -Temp.x;

        return Temp;
    }

    public static bool WherePoint(Vector2 _start, Vector2 _end, Vector3 _Pos)
    {
        bool result = false;
        float S = (_end.x - _start.x) * (_Pos.z - _start.y) - (_end.y - _start.y) * (_Pos.x - _start.x);
        if (S > 0)
            result = true;
        return result;

    }

    public static float TriangleHigh(Vector2 A, Vector2 B, Vector2 C)
    {
        float a = Vector2.Distance(A, C);
        float b = Vector2.Distance(B, C);
        float c = Vector2.Distance(A, B);
        float p = (a + b + c) / 2;
        float S = Mathf.Sqrt(p * (p - a) * (p - b) * (p - c));
        return 2 * S / c;
    }

	public static Vector3 RotateThis(Vector3 Pos, float angle)
	{
		return RotateThis(Pos, angle, Vector3.zero);

	}

    public static Vector3 RotatePointAroundPivot(Vector3 point, Vector3 pivot, Vector3 angles)
    {
        Vector3 dir = point - pivot; // get point direction relative to pivot
        dir = Quaternion.Euler(angles) * dir; // rotate it
        point = dir + pivot; // calculate rotated point
        return point; // return it
    }

    public static Vector3 RotateThis(Vector3 Pos, float angle, Vector3 global)
    {
        float pX, pY;
        angle *= Mathf.Deg2Rad;
        pX = Pos.z * Mathf.Cos(angle) + Pos.x * Mathf.Sin(angle);
        pY = Pos.x * Mathf.Cos(angle) - Pos.z * Mathf.Sin(angle);
        Vector3 Temp = new Vector3(pY, Pos.y, pX);
        Temp += global;
        return Temp;
        
    }

    public static bool GetChanceOf(int percent)
    {
        int r = Random.Range(0, 100);
        if (r <= percent)
            return true;
        else
            return false;

    }

	public static Vector3 RotateThis(float Scale, float angle)
	{
		return RotateThis (Scale, angle, Vector3.zero);
	}

    public static Vector3 RotateThis(float Scale, float angle, Vector3 global)
    {
        float pX, pY;
        angle *= Mathf.Deg2Rad;
        pX = Scale * Mathf.Sin(angle);
        pY = Scale * Mathf.Cos(angle);
        Vector3 Temp = new Vector3(pY, 0.0f, pX);
        Temp += global;
        return Temp;

    }

    public static Vector2 RotateThis2d(Vector3 Pos, float angle, Vector3 global)
    {
        Vector3 Temp;
        Temp = RotateThis(Pos, angle, global);
        Vector2 result = new Vector2(Temp.x, Temp.z);
        return result;

    }

    public static Vector2 RotateThis2d(float Scale, float angle, Vector3 global)
    {
        Vector3 Temp;
        Temp = RotateThis(Scale, angle, global);
        Vector2 result = new Vector2(Temp.x, Temp.z);
        return result;

    }

    public static float GetAngle(Vector3 Dir, Vector3 Dir2)
    {
        float result;
        result = Mathf.Acos(Vector3.Dot(Dir, Dir2) / (Dir.magnitude * Dir2.magnitude));
        result *= Mathf.Rad2Deg;
        if (float.IsNaN(result))
        result = 0;
        return result;
    }

    public static float GetAngle(float Ax, float Ay, float Bx, float By)
    {
        float result;
        result = Mathf.Atan2(Ax * By - Bx * Ay, Ax * Bx + Ay * By);
        result *= Mathf.Rad2Deg;
        return result;
    }

    public static float GetAngle(float Ax, float Ay)
    {
        float result;
        result = Mathf.Atan2(Ax * Vector3.forward.x - Vector3.forward.z * Ay, Ax * Vector3.forward.z + Ay * Vector3.forward.x);
        result *= Mathf.Rad2Deg;
        return result;
    }

    public static bool GetInterceptPoint(out Vector3 O, Vector3 A, Vector3 B, Vector3 C, Vector3 P1, Vector3 P2)
    {
        O = Vector3.zero;
        Vector3 V = P1 - P2;     // Направление прямой P1P2
        Vector3 AB = (B - A);
        Vector3 AC = (C - A);
        Vector3 BC = (C - B);
        Vector3 BA = (A - B);
        Vector3 N = Vector3.Cross(AB, AC);     // Нормаль к пов-ти

        float vn = Vector3.Dot(V, N);          // проекция V на N
        if (vn == 0.0f) return false;      // нет пересечения (прямая паралельна плоскости)

        float t = -(Vector3.Dot(P1 - A, N) / vn);
        O = P1 + V * t;

        Vector3 NAB = Vector3.Cross(N, AB);  // нормаль к плоскости NAB
        Vector3 NAC = Vector3.Cross(N, AC);  // нормаль к плоскости NAC
        Vector3 NBC = Vector3.Cross(N, BC);  // нормаль к плоскости NBC

        Vector3 AO = (O - A);
        Vector3 BO = (O - B);

        float acnab = Vector3.Dot(AC, NAB);  // проекция AC на NAB
        float aonab = Vector3.Dot(AO, NAB);  // проекция AO на NAB

        float acnac = Vector3.Dot(AB, NAC);  // проекция AB на NAC
        float aonac = Vector3.Dot(AO, NAC);  // проекция AO на NAC

        float acnbc = Vector3.Dot(BA, NBC);  // проекция BA на NBC
        float bonbc = Vector3.Dot(BO, NBC);  // проекция BO на NBC

        float dist1 = Vector3.Distance(P1, P2);
        float dist2 = Vector3.Distance(P1, O);
        float dist3 = Vector3.Distance(P2, O);
        if (dist2 + dist3 < dist1 - 0.01f || dist2 + dist3 > dist1 + 0.01f)
            return false;

        if (Mathf.Sign(acnab) == Mathf.Sign(aonab) && Mathf.Sign(acnac) == Mathf.Sign(aonac) && Mathf.Sign(acnbc) == Mathf.Sign(bonbc)) return true;


        // принадлежность сторонам треугольника
        if ((Mathf.Sign(aonab) == 0.0f && AO.magnitude <= AB.magnitude) &&
        (Mathf.Sign(aonac) == 0.0f && AO.magnitude <= AC.magnitude) &&
        (Mathf.Sign(bonbc) == 0.0f && BO.magnitude <= BC.magnitude))
        return true;

        return false;
    }


    public static bool RayCastGroundDown(Vector3 pos, out RaycastHit Hit)
    {
        int layerMask = 1 << 0 | 1 << 9 | 1 << 12;
        layerMask = ~layerMask;
        pos.y += 10.0f;

        if (Physics.Raycast(pos, Vector3.down, out Hit, Mathf.Infinity, layerMask))
        {
            ////Debug.DrawLine(pos, Hit.point, Color.green, 100.0f);
            return true;
        }
        return false;
    }

    public static Vector2i ReturnForkPlaneNumbers(Vector3 position)
    {
        Vector2i result = new Vector2i(0,0);

        RaycastHit Hit;

        if (RayCastGroundDown(position, out Hit))
        {
            BlockPlane plane = Hit.collider.gameObject.transform.parent.GetComponent<BlockPlane>();
            result.x = plane.runLine;
            result.y = plane.localForkNumber;
        }

        return result;

    }

	public static OneSpline GetEnemySpline(BlockPlane _plane, Vector3 _pos)
    {

        if (_plane != null)
            return _plane.GetSpline(_pos);
        else
            return null;
    }

    


	public static OneSpline GetEnemySpline(Vector3 _pos)
    {
        BlockPlane _plane = Main.Placed(_pos);
        if (_plane == null)
            return null;
        return _plane.GetSpline(_pos);
    }
    public static bool dotInQuad(Vector3 Pos, Vector3 A, Vector3 B, Vector3 C, Vector3 D)
    {
        if ((((Pos.z - A.z) * (B.x - A.x) - (Pos.x - A.x) * (B.z - A.z)) * ((C.z - A.z) * (B.x - A.x) - (C.x - A.x) * (B.z - A.z)) >= 0) &&
            (((Pos.z - B.z) * (C.x - B.x) - (Pos.x - B.x) * (C.z - B.z)) * ((A.z - B.z) * (C.x - B.x) - (A.x - B.x) * (C.z - B.z)) >= 0) &&
            (((Pos.z - C.z) * (A.x - C.x) - (Pos.x - C.x) * (A.z - C.z)) * ((B.z - C.z) * (A.x - C.x) - (B.x - C.x) * (A.z - C.z)) >= 0) ||
            (((Pos.z - B.z) * (C.x - B.x) - (Pos.x - B.x) * (C.z - B.z)) * ((D.z - B.z) * (C.x - B.x) - (D.x - B.x) * (C.z - B.z)) >= 0) &&
            (((Pos.z - C.z) * (D.x - C.x) - (Pos.x - C.x) * (D.z - C.z)) * ((B.z - C.z) * (D.x - C.x) - (B.x - C.x) * (D.z - C.z)) >= 0) &&
            (((Pos.z - D.z) * (B.x - D.x) - (Pos.x - D.x) * (B.z - D.z)) * ((C.z - D.z) * (B.x - D.x) - (C.x - D.x) * (B.z - D.z)) >= 0))
            return true;
        else
            return false;
    }

    public static bool dotInTriangle(Vector3 Pos, Vector3 A, Vector3 B, Vector3 C)
    {
        if ((((Pos.z - A.z) * (B.x - A.x) - (Pos.x - A.x) * (B.z - A.z)) * ((C.z - A.z) * (B.x - A.x) - (C.x - A.x) * (B.z - A.z)) >= 0) &&
            (((Pos.z - B.z) * (C.x - B.x) - (Pos.x - B.x) * (C.z - B.z)) * ((A.z - B.z) * (C.x - B.x) - (A.x - B.x) * (C.z - B.z)) >= 0) &&
            (((Pos.z - C.z) * (A.x - C.x) - (Pos.x - C.x) * (A.z - C.z)) * ((B.z - C.z) * (A.x - C.x) - (B.x - C.x) * (A.z - C.z)) >= 0))
            return true;
        else
            return false;
    }

    public static bool dotInTriangle2d(Vector2 Pos, Vector2 A, Vector2 B, Vector2 C)
    {
        if ((((Pos.x - A.x) * (B.y - A.y) - (Pos.y - A.y) * (B.x - A.x)) * ((C.x - A.x) * (B.y - A.y) - (C.y - A.y) * (B.x - A.x)) >= 0) &&
            (((Pos.x - B.x) * (C.y - B.y) - (Pos.y - B.y) * (C.x - B.x)) * ((A.x - B.x) * (C.y - B.y) - (A.y - B.y) * (C.x - B.x)) >= 0) &&
            (((Pos.x - C.x) * (A.y - C.y) - (Pos.y - C.y) * (A.x - C.x)) * ((B.x - C.x) * (A.y - C.y) - (B.y - C.y) * (A.x - C.x)) >= 0))
            return true;
        else
            return false;
    }

    public static bool isRectIntersect(Vector2 A, Vector2 B, Vector2 C, Vector2 D, Vector2 A1, Vector2 B1, Vector2 C1, Vector2 D1)
    {
        if (dotInTriangle2d(A1, A, B, C) || dotInTriangle2d(A1, B, C, D)) return true;
        if (dotInTriangle2d(B1, A, B, C) || dotInTriangle2d(B1, B, C, D)) return true;
        if (dotInTriangle2d(C1, A, B, C) || dotInTriangle2d(C1, B, C, D)) return true;
        if (dotInTriangle2d(D1, A, B, C) || dotInTriangle2d(D1, B, C, D)) return true;
        return false;
    }

    public static bool isTriRectIntersect(Vector2 A, Vector2 B, Vector2 C, Vector2 A1, Vector2 B1, Vector2 C1, Vector2 D1)
    {
        if (dotInTriangle2d(A1, A, B, C)) return true;
        if (dotInTriangle2d(B1, A, B, C)) return true;
        if (dotInTriangle2d(C1, A, B, C)) return true;
        if (dotInTriangle2d(D1, A, B, C)) return true;
        return false;
    }

    public static bool isTriangl2Box2Intersect(Triangle2 triangle, Box2 box)
    {
        
        Vector2[] dotes = box.CalcVertices();
        Triangle2 one = new Triangle2(ref dotes[0], ref dotes[1], ref dotes[2]);
        Triangle2 two = new Triangle2(ref dotes[1], ref dotes[2], ref dotes[3]);

        if (Intersection.TestTriangle2Triangle2(ref triangle, ref one)) return true;
        if (Intersection.TestTriangle2Triangle2(ref triangle, ref two)) return true;
        return false;
    }

    public static Vector3 BezierLerp(Vector3 start, Vector3 end, float t)
    {
        Vector3 p = end * t;
        p += start * (1 - t);
        //Debug.DrawLine(start, end, Color.blue);
        return p;
    }

    public static Vector3 GetPointOnLine(Vector3 _pos, Vector3 _start, Vector3 _end)
    {
        Vector3 result = Vector3.zero;

        Vector3 _dir = _start - _end;
        _dir.Normalize();

        float angle = Math3d.GetAngle(_dir.z, _dir.x, Vector3.forward.z, Vector3.forward.x);

        Vector3 _right = Math3d.RotateThis(new Vector3(10.0f, 0.0f, 0.0f), angle, _pos);
        Vector3 _left = Math3d.RotateThis(new Vector3(-10.0f, 0.0f, 0.0f), angle, _pos);
        //Debug.DrawLine(_right, _left, Color.gray);
        //Debug.DrawLine(_pos, _dir * 2.0f + _pos, Color.black);

        if (Math3d.CrossPoint(_right, _left, _start, _end, out result))
            return result;

        else

            Debug.Log("LOL");
        //Debug.DrawLine(_right, _left, Color.magenta);
        //Debug.DrawLine(_start, _end, Color.magenta);
        //GameObject.Find("Main").GetComponent<Main>()._timescale = 0;

        return _start;
        


        

       

    }

    public static Vector3 CubicBezierLerp(Vector3 start, Vector3 p1, Vector3 p2, Vector3 end, float t)
    {

        float u = 1.0f - t;
        float tt = t * t;
        float uu = u * u;
        float uuu = uu * u;
        float ttt = tt * t;

        Vector3 p = start * uuu;    //first term
        p += (p1 * (3 * uu * t));    //second term
        p += (p2 * (3 * u * tt));    //third term
        p += (end * ttt);           //fourth term

        return p;
    }

    public static bool CrossPoint(Vector3 _sA, Vector3 _eA, Vector3 _sB, Vector3 _eB, out Vector3 result)
    {
    result = Vector3.zero;
    
    float Ua = ((_eB.x - _sB.x) * (_sA.z - _sB.z) - (_eB.z - _sB.z) * (_sA.x - _sB.x)) / ((_eB.z - _sB.z) * (_eA.x - _sA.x) - (_eB.x - _sB.x) * (_eA.z - _sA.z));
    float Ub = ((_eA.x - _sA.x) * (_sA.z - _sB.z) - (_eA.z - _sA.z) * (_sA.x - _sB.x)) / ((_eB.z - _sB.z) * (_eA.x - _sA.x) - (_eB.x - _sB.x) * (_eA.z - _sA.z));

    



    if (Ua > 0.0f && Ua < 1.0f && Ub > 0.0f && Ub < 1.0f)
    {
        result.x = _sA.x + Ua * (_eA.x - _sA.x);
        result.z = _sA.z + Ua * (_eA.z - _sA.z);
        return true;
    }

    

    return false;
    }

    public static bool CrossPointLine(Vector3 _sA, Vector3 _eA, Vector3 _sB, Vector3 _eB, out Vector3 result)
    {
        result = Vector3.zero;

        float Ua = ((_eB.x - _sB.x) * (_sA.z - _sB.z) - (_eB.z - _sB.z) * (_sA.x - _sB.x)) / ((_eB.z - _sB.z) * (_eA.x - _sA.x) - (_eB.x - _sB.x) * (_eA.z - _sA.z));
        //float Ub = ((_eA.x - _sA.x) * (_sA.z - _sB.z) - (_eA.z - _sA.z) * (_sA.x - _sB.x)) / ((_eB.z - _sB.z) * (_eA.x - _sA.x) - (_eB.x - _sB.x) * (_eA.z - _sA.z));

        result.x = _sA.x + Ua * (_eA.x - _sA.x);
        result.z = _sA.z + Ua * (_eA.z - _sA.z);

        return true;
    }

    public static float Contains(Vector2 ellipse, Vector2 point)
    {

        float _xRadius = ellipse.x / 2;
        float _yRadius = ellipse.y / 2;


        if (_xRadius <= 0.0f || _yRadius <= 0.0f)
            return 0;


        float result = ((point.x * point.x) / (_xRadius * _xRadius) + (point.y * point.y) / (_yRadius * _yRadius));
        return result;
    }
    
    public static Vector3 IntersectTri(Vector3 A1, Vector3 A2, Vector3 A3)
    {
        float ASx = (A1.z + A2.z + A3.z) / 3;
        float ASy = (A1.x + A2.x + A3.x) / 3;

        A1.z -= ASx;
        A2.z -= ASx;
        A3.z -= ASx;
        A1.x -= ASy;
        A2.x -= ASy;
        A3.x -= ASy;

        Vector3 result;

        float a=Vector2.Distance(new Vector2(A1.z,A1.x),new Vector2(A2.z,A2.x));
        float b=Vector2.Distance(new Vector2(A2.z,A2.x),new Vector2(A3.z,A3.x));
        float c=Vector2.Distance(new Vector2(A3.z,A3.x),new Vector2(A1.z,A1.x));
        float r = ((a * b * c) / Mathf.Sqrt((a + b + c) * (-a + b + c) * (a - b + c) * (a + b - c)));
        
        float x12 = A1.z-A2.z;
        float x23 = A2.z-A3.z;
        float x31 = A3.z-A1.z;
        float y12 = A1.x-A2.x;
        float y23 = A2.x-A3.x;
        float y31 = A3.x-A1.x;
        float z1 = A1.x*A1.x+A1.z*A1.z;
        float z2 = A2.x*A2.x+A2.z*A2.z;
        float z3 = A3.x*A3.x+A3.z*A3.z;
        
        float z = x12*y31 - y12*x31;
        float zX = y12 * z3 + y23 * z1 + y31 * z2;
        float zY = x12 * z3 + x23 * z1 + x31 * z2;

        result.x = ASx + -zX / (2 * z);
        result.y = ASy + zY / (2 * z);
        result.z = r;


        return result;
    }


    public static Vector3 PrepareIntersectQuad(Vector3 A1, Vector3 A2, Vector3 A3, Vector3 A4)
    {
        float ASx = (A1.z + A2.z + A3.z + A4.z) / 4;
        float ASy = (A1.x + A2.x + A3.x + A4.x) / 4;

        Vector3 result;

        float a = Vector2.Distance(new Vector2(A1.z, A1.x), new Vector2(ASx, ASy));
        float b = Vector2.Distance(new Vector2(A2.z, A2.x), new Vector2(ASx, ASy));
        float c = Vector2.Distance(new Vector2(A3.z, A3.x), new Vector2(ASx, ASy));
        float d = Vector2.Distance(new Vector2(A4.z, A4.x), new Vector2(ASx, ASy));
        float r = Mathf.Max(a, b, c, d);
        
        result.x = ASx;
        result.y = ASy;
        result.z = r;


        return result;
    }

    public static bool CircleCompare(Vector3 Circle, Vector3 Circle02)
    {
        float dist = Vector2.Distance(new Vector2(Circle.x, Circle.y), new Vector2(Circle02.x, Circle02.y));
        float dist2 = Circle.z + Circle02.z;
        if (dist >= dist2)
            return false;
        else
            return true;
    }

    public static bool QuadInCircle(Vector2 A1, Vector2 A2, Vector2 A3, Vector2 A4, Vector2 Center, float radius)
    {
        if (Vector2.Distance(A1, Center) < radius) return true;
        if (Vector2.Distance(A2, Center) < radius) return true;
        if (Vector2.Distance(A3, Center) < radius) return true;
        if (Vector2.Distance(A4, Center) < radius) return true;
        return false;
    }

    public static bool QuadIntersectCircle(Vector2 A1, Vector2 A2, Vector2 A3, Vector2 A4, Vector2 Center, float radius)
    {

        if (Vector2.Distance(A1, Center) < radius) return true;
        if (Vector2.Distance(A2, Center) < radius) return true;
        if (Vector2.Distance(A3, Center) < radius) return true;
        if (Vector2.Distance(A4, Center) < radius) return true;
        if (Vector2.Distance(Vector2.Lerp(Vector2.Lerp(A2,A3,0.5f),Vector2.Lerp(A1,A4,0.5f),0.5f), Center) < radius) return true;
        return false;
    }


    public static void DrawCircle(Vector3 Pos, float r, Color color, float duration)
    {
        for (int i = 0; i <= 20; i++)
        {
            float theta = 2.0f * 3.1415925f * (float)i / 20.0f;
            float theta2 = 2.0f * 3.1415925f * (float)(i + 1) / 20.0f;

            float x = r * Mathf.Cos(theta);
            float y = r * Mathf.Sin(theta);
            float x2 = r * Mathf.Cos(theta2);
            float y2 = r * Mathf.Sin(theta2);

            //Debug.DrawLine(new Vector3(x + Pos.x, Pos.y, y + Pos.z), new Vector3(x2 + Pos.x, Pos.y, y2 + Pos.z), color, duration);//output vertex
        }

    }

    /*
    public static Vector3[] GetSpriteCorners(MapObject obj)
    {
        // sprite.bounds is in local space
        Bounds b = obj
        Vector2[] res = new Vector3
     {
         new Vector3(b.min.x,b.min.y),
         new Vector3(b.min.x,b.max.y),
         new Vector3(b.max.x,b.max.y),
         new Vector3(b.max.x,b.min.y)
     };
        var t = obj.transform;
        // transform the corners into worldspace
        for (int i = 0; i < res.Length; i++)
        {
            res[i] = obj.transform.TransformPoint(res[i]);
        }
        return res;
    }
   */

    /*
    public static bool SnapCar(Transform transformCurrent, Vector3[] ControllerPos, float radius, out Vector3 Position, out Vector3 Rotation)
    {
        Position = Vector3.zero;
        Rotation = Vector3.zero;

        Vector3[] ground = new Vector3[4];

        RaycastHit hit;
        float[] diff = new float[4];

       // //Debug.DrawLine(ControllerPos[0], ControllerPos[1], Color.red, 10.0f);
       // //Debug.DrawLine(ControllerPos[2],ControllerPos[3], Color.red, 10.0f);

        for (int i = 0; i < 4; i++)
        {
              if (!RayCastGroundDown(ControllerPos[i], out hit))
                return false;
            ground[i] = hit.point;
            diff[i] = ControllerPos[i].y - ground[i].y - radius;
        //    Debug.Log(ControllerPos[i].y + " " + ground[i].y + " " + radius);
        //    Locators.CreateLocator(ground[i], "Ground " + i.ToString());
            ground[i] = transformCurrent.InverseTransformPoint(ground[i]);
            
            

        }

       // //Debug.DrawLine(ground[0], ground[1], Color.blue, 10.0f);
       // //Debug.DrawLine(ground[2], ground[3], Color.blue, 10.0f);

        float resultDiff = (diff[0] + diff[1] + diff[2] + diff[3]) / 4;

      //  Debug.Log("Ground 0: " + ground[0] + "Diff 0: " + diff[0]);
      //  Debug.Log("Ground 1: " + ground[1] + "Diff 1: " + diff[1]);
      //  Debug.Log("Ground 2: " + ground[2] + "Diff 2: " + diff[2]);
      //  Debug.Log("Ground 3: " + ground[3] + "Diff 3: " + diff[3]);

        Vector3 LeftDir = Vector3.Normalize(ground[0] - ground[2]);
        Vector3 RightDir = Vector3.Normalize(ground[1] - ground[3]);
        Vector3 TopDir = Vector3.Normalize(ground[1] - ground[0]);
        Vector3 BackDir = Vector3.Normalize(ground[3] - ground[2]);
        LeftDir.x = 0;
        RightDir.x = 0;
        TopDir.z = 0;
        BackDir.z = 0;

      //  Debug.Log("LeftDir: " + LeftDir);
      //  Debug.Log("RightDir: " + RightDir);
      //  Debug.Log("TopDir: " + TopDir);
       // Debug.Log("BackDir: " + BackDir);

        float LeftAngle = GetAngle(LeftDir, Vector3.forward);
        float RightAngle = GetAngle(RightDir, Vector3.forward);
        float TopAngle = GetAngle(TopDir, Vector3.right);
        float BackAngle = GetAngle(BackDir, Vector3.right);

       // Debug.Log("LeftAngle: " + LeftAngle);
       // Debug.Log("RightAngle: " + RightAngle);
       // Debug.Log("TopAngle: " + TopAngle);
       // Debug.Log("BackAngle: " + BackAngle);

        LeftAngle *= diff[0] > diff[2] ? 1 : -1;
        RightAngle *= diff[1] > diff[3] ? 1 : -1;
        TopAngle *= diff[0] > diff[1] ? 1 : -1;
        BackAngle *= diff[2] > diff[3] ? 1 : -1;

       // Debug.Log("LeftAngleD: " + LeftAngle);
        //Debug.Log("RightAngleD: " + RightAngle);
        //Debug.Log("TopAngleD: " + TopAngle);
        //Debug.Log("BackAngleD: " + BackAngle);

        

        Rotation = new Vector3(Mathf.LerpAngle(LeftAngle,RightAngle,0.5f),transformCurrent.rotation.eulerAngles.y,Mathf.LerpAngle(TopAngle,BackAngle,0.5f));

       // Debug.Log("Rotation: " + Rotation);
        Position = new Vector3(transformCurrent.position.x, transformCurrent.position.y - resultDiff, transformCurrent.position.z);

        return true; 

    }
    */

    public static bool SnapObject(Transform transformCurrent, Vector3[] ControllerPos, out Vector3 Position, out Vector3 Rotation)
    {
        Position = Vector3.zero;
        Rotation = Vector3.zero;

        Vector3[] ground = new Vector3[4];
        RaycastHit hit;
        float[] diff = new float[4];

        for (int i = 0; i < 4; i++)
        {
            if (!RayCastGroundDown(ControllerPos[i], out hit))
                return false;
            ground[i] = hit.point;

            diff[i] = ControllerPos[i].y - ground[i].y;

        }

        

        float resultDiff = (diff[0] + diff[1] + diff[2] + diff[3]) / 4;

        Vector3 LeftDir = Vector3.Normalize(ground[0] - ground[3]);
        Vector3 RightDir = Vector3.Normalize(ground[1] - ground[2]);
        Vector3 TopDir = Vector3.Normalize(ground[2] - ground[3]);
        Vector3 BackDir = Vector3.Normalize(ground[1] - ground[0]);

        float LeftAngle = GetAngle(LeftDir, transformCurrent.forward * -1);
        float RightAngle = GetAngle(RightDir, transformCurrent.forward * -1);
        float TopAngle = GetAngle(TopDir, transformCurrent.right);
        float BackAngle = GetAngle(BackDir, transformCurrent.right);


        LeftAngle *= diff[0] > diff[3] ? -1 : 1;
        RightAngle *= diff[1] > diff[2] ? -1 : 1;
        TopAngle *= diff[2] > diff[3] ? -1 : 1;
        BackAngle *= diff[1] > diff[0] ? -1 : 1;

        Rotation = new Vector3(Mathf.LerpAngle(LeftAngle, RightAngle, 0.5f), transformCurrent.rotation.eulerAngles.y, Mathf.LerpAngle(TopAngle, BackAngle, 0.5f));
        Position = new Vector3(transformCurrent.position.x, transformCurrent.position.y - resultDiff, transformCurrent.position.z);

        return true;

    }
    /*
    #if UNITY_EDITOR
        [MenuItem("Tools/SnapVehicle")]
    #endif
        public static void SnapVehicle()
        {
    #if UNITY_EDITOR
        
            GameObject _selection = Selection.activeGameObject;
            Vector3 Position = Vector3.zero;
            Vector3 Rotation = Vector3.zero;
            Vehicle current = _selection.GetComponent<Vehicle>();
            if (!current.Snap());
                Debug.LogWarning("Snap ERROR " + _selection.name);
    #endif
        }


    #if UNITY_EDITOR
        [MenuItem("Tools/SnapEnemy")]
    #endif
        public static void SnapEnemy()
        {
    #if UNITY_EDITOR
            GameObject _selection = Selection.activeGameObject;
            Vector3 Position = Vector3.zero;
            Vector3 Rotation = Vector3.zero;
            EnemyVehicle Unit = _selection.GetComponent<EnemyVehicle>();
            //Vector3 clearRotate = Unit.transform.eulerAngles;
            //clearRotate.x = 0;
            //clearRotate.z = 0;
            //Unit.transform.rotation = Quaternion.EulerAngles(clearRotate);
            Unit.SetupColliders();

            //_selection.transform.rotation = Quaternion.Euler(0, _selection.transform.eulerAngles.y, 0);
            if (SnapCar(_selection.transform, Unit.colliderPos, Unit.wheelsColliderForward[0].radius, out Position, out Rotation))
            {
                _selection.transform.position = Position;
                Debug.Log(Rotation);
                _selection.transform.rotation = Quaternion.Euler(Rotation);
            }
            else
                Debug.LogWarning("Snap ERROR " + _selection.name);

    #endif
        }
    */
 

	/*
#if UNITY_EDITOR
    [MenuItem("Tools/GenerateLocals")]
#endif
    public static void GenerateLocals()
    {
#if UNITY_EDITOR
        GameObject _selection = Selection.activeGameObject;
        Vector3 Position = Vector3.zero;
        Vector3 Rotation = Vector3.zero;
        Unit unit = _selection.GetComponent<Unit>();

        unit.SetupColliders();

        Misc misc = _selection.GetComponent<Misc>();

        misc.colliderPosLocal = new Vector3[4];
        for (int i=0; i<4; i++)
        misc.colliderPosLocal[i] = unit.colliderPos[i];


#endif
    }
*/

    public static bool PtInTetrahedron(Vector3 A, Vector3 B, Vector3 C, Vector3 D, Vector3 P)
    {
  float Det, b, c, d;
  float cdx, cdy, cdz;

  bool result = false;
  
  B.x= B.x- A.x;
  B.y = B.y - A.y;
  B.z = B.z - A.z;
  C.x = C.x - A.x;
  C.y = C.y - A.y;
  C.z = C.z - A.z;
  D.x = D.x - A.x;
  D.y = D.y - A.y;
  D.z = D.z - A.z;
  P.x = P.x - A.x;
  P.y = P.y - A.y;
  P.z = P.z - A.z;
  cdx = C.y * D.z - C.z * D.y;
  cdy = C.z * D.x - C.x * D.z;
  cdz = C.x * D.y - C.y * D.x;
  Det = B.x* cdx + B.y * cdy + B.z * cdz;
  if (Det != 0)
  {
    b = P.x * cdx + P.y * cdy + P.z * cdz;
    c = P.x * (D.y * B.z - D.z * B.y) + P.y * (D.z * B.x- D.x * B.z) + P.z * (D.x * B.y - D.y * B.x);
    d = P.x * (B.y * C.z - B.z * C.y) + P.y * (B.z * C.x - B.x* C.z) + P.z * (B.x* C.y - B.y * C.x);
   
    if (Det > 0)
      result = (b >= 0) && (c >= 0) && (d >= 0) && (b + c + d <= Det);
    else
      result = (b <= 0) && (c <= 0) && (d <= 0) && (b + c + d >= Det);
  }
return result;
    }



    public static float GetSpeed(float rpm, float radius)
    {
        return (Mathf.PI * 2 * radius) * rpm / 60; 
    }

    public static float GetRpm(float speed, float radius)
    {
        return (speed * 60) / (Mathf.PI * 2 * radius);
    }

  

 

    
   
}
