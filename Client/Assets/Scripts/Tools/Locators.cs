﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections.Generic;

public static class Locators
{
    public static Transform CreateLocator(Vector3 _pos, Quaternion _rot, Transform _parent, string _name)
    {
        GameObject _locator = null;
#if UNITY_EDITOR
        _locator = GameObject.Instantiate(AssetDatabase.LoadAssetAtPath("Assets/Tools/Locator.prefab", typeof(GameObject)), _pos, _rot) as GameObject;
        _locator.transform.parent = _parent;
        _locator.name = _name;
#endif
        return _locator.transform;

    }

    public static Transform CreateLocator(Vector3 _pos, string _name)
    {
        GameObject _locator = null;
#if UNITY_EDITOR
        _locator = GameObject.Instantiate(AssetDatabase.LoadAssetAtPath("Assets/Tools/Locator.prefab", typeof(GameObject)), _pos, Quaternion.identity) as GameObject;
        _locator.name = _name;
#endif
        return _locator.transform;

    }

    public static Transform CreateLocatorLocal(Vector3 _pos, Quaternion _rot, Transform _parent, string _name)
    {
        GameObject _locator = null;
#if UNITY_EDITOR
        _locator = GameObject.Instantiate(AssetDatabase.LoadAssetAtPath("Assets/Tools/Locator.prefab", typeof(GameObject)), Vector3.zero, Quaternion.identity) as GameObject;
        _locator.transform.parent = _parent;
        _locator.transform.localPosition = _pos;
        _locator.transform.localRotation = _rot;
		_locator.transform.localScale = new Vector3(5.0f, 5.0f, 5.0f);
        _locator.name = _name;
#endif
        return _locator.transform;

    }
#if UNITY_EDITOR
    [MenuItem("Tools/Snap %h")]
#endif
    public static void SnapToRoad()
    {
#if UNITY_EDITOR
        GameObject _selection = Selection.activeGameObject;

        RaycastHit Hit;
        int layerMask = 1 << 0;
        layerMask = ~layerMask;

        if (Physics.Raycast(_selection.transform.position, Vector3.down, out Hit, Mathf.Infinity, layerMask))
        {
            _selection.transform.position = Hit.point;
            return;
        }



#endif
    }
    /*
    public Vector3 SnapToRoad(Vector3 sourcePosition)
    {
        RaycastHit Hit;
        int layerMask = 1 << 0;
        layerMask = ~layerMask;

        if (Physics.Raycast(sourcePosition, Vector3.down, out Hit, Mathf.Infinity, layerMask))
        {
            return Hit.point;
        }

    }
    */
#if UNITY_EDITOR
    [MenuItem("Tools/Plane %g")]
#endif
    public static void CreatePlane()
    {
#if UNITY_EDITOR
        GameObject _selection = Selection.activeGameObject;

        GameObject newObject = GameObject.CreatePrimitive(PrimitiveType.Plane);
        newObject.transform.parent = _selection.transform;
        newObject.transform.localPosition = Vector3.zero;
        newObject.transform.localRotation = Quaternion.identity;

#endif
    }


}
