﻿using UnityEngine;
using System.Collections;

public class Quad2d : MonoBehaviour
{
#if UNITY_EDITOR
    protected Transform a_sphere;
    protected Transform b_sphere;
    protected Transform c_sphere;
    protected Transform d_sphere;
#endif

    public SplineType type;
    public Vector3 a;
    public Vector3 b;
    public Vector3 c;
    public Vector3 d;

#if UNITY_EDITOR
    public void Show()
    {
        if (a_sphere)
            DestroyImmediate(a_sphere.gameObject);
        a_sphere = Locators.CreateLocatorLocal(a, Quaternion.identity, transform, "a").transform;

        if (b_sphere)
            DestroyImmediate(b_sphere.gameObject);
        b_sphere = Locators.CreateLocatorLocal(b, Quaternion.identity, transform, "b").transform;

        if (c_sphere)
            DestroyImmediate(c_sphere.gameObject);
        c_sphere = Locators.CreateLocatorLocal(c, Quaternion.identity, transform, "c").transform;

        if (d_sphere)
            DestroyImmediate(d_sphere.gameObject);
        d_sphere = Locators.CreateLocatorLocal(d, Quaternion.identity, transform, "d").transform;

    }


    public virtual void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;
        Draw();
        
    }

    protected void Draw()
    {
        Gizmos.DrawLine(Math3d.RotateThis(a, -transform.eulerAngles.y, transform.position), Math3d.RotateThis(b, -transform.eulerAngles.y, transform.position));
        Gizmos.DrawLine(Math3d.RotateThis(b, -transform.eulerAngles.y, transform.position), Math3d.RotateThis(c, -transform.eulerAngles.y, transform.position));
        Gizmos.DrawLine(Math3d.RotateThis(c, -transform.eulerAngles.y, transform.position), Math3d.RotateThis(d, -transform.eulerAngles.y, transform.position));
        Gizmos.DrawLine(Math3d.RotateThis(d, -transform.eulerAngles.y, transform.position), Math3d.RotateThis(a, -transform.eulerAngles.y, transform.position));


        if (a_sphere)
            a = a_sphere.localPosition;

        if (b_sphere)
            b = b_sphere.localPosition;

        if (c_sphere)
            c = c_sphere.localPosition;

        if (d_sphere)
            d = d_sphere.localPosition;
    }
#endif

    	
}
