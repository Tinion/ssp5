﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;


public enum IntersectionType
{
    None,
    Intersects,
    Contains
}

public class IntersectionTests
{

    public static bool Intersects(ref Bounds box, ref Vector3 center, float radius)
    {

        var closestPointInAabb = Vector3.Min(Vector3.Max(center, box.min), box.max);
        var distanceSquared = (closestPointInAabb - center).sqrMagnitude;

        return distanceSquared <= radius * radius;

    }

    public static IntersectionType GetIntersectionType(ref Bounds box, ref Vector3 center, float radius)
    {

        var closestPointInBox = Vector3.Min(Vector3.Max(center, box.min), box.max);
        var distanceSquared = (closestPointInBox - center).sqrMagnitude;

        if (distanceSquared <= (radius * radius))
        {

            var c = closestPointInBox - box.center;
            var r = box.extents;
            var x = Mathf.Abs(c.x) <= (r.x - radius);
            var y = Mathf.Abs(c.y) <= (r.y - radius);
            var z = Mathf.Abs(c.z) <= (r.z - radius);

            if (x & y & z)
            {
                return IntersectionType.Contains;
            }

            return IntersectionType.Intersects;

        }

        return IntersectionType.None;

    }

    public static IntersectionType GetIntersectionType(ref Vector3 center, float radius, ref Bounds box)
    {

        var closestPointInBox = Vector3.Min(Vector3.Max(center, box.min), box.max);
        var distanceSquared = (closestPointInBox - center).sqrMagnitude;

        if (distanceSquared <= (radius * radius))
        {

            var c = (box.center - center).magnitude;
            var r = (box.max - box.center).magnitude;

            if (c <= radius - r)
            {
                return IntersectionType.Contains;
            }

            return IntersectionType.Intersects;

        }

        return IntersectionType.None;

    }

    public static bool Intersects(ref Bounds lhs, ref Bounds rhs)
    {

        var c = lhs.center - rhs.center;
        var r = lhs.extents + rhs.extents;

        var x = Math.Abs(c.x) <= r.x;
        var y = Math.Abs(c.y) <= r.y;
        var z = Math.Abs(c.z) <= r.z;

        return x & y & z;

    }

    public static IntersectionType GetIntersectionType(ref Bounds lhs, ref Bounds rhs)
    {

        var c = lhs.center - rhs.center;
        var r = lhs.extents + rhs.extents;

        var x = Math.Abs(c.x) <= r.x;
        var y = Math.Abs(c.y) <= r.y;
        var z = Math.Abs(c.z) <= r.z;

        if (x & y & z)
        {

            r = lhs.extents - rhs.extents;
            x = Math.Abs(c.x) <= r.x;
            y = Math.Abs(c.y) <= r.y;
            z = Math.Abs(c.z) <= r.z;

            if (x & y & z)
            {
                return IntersectionType.Contains;
            }

            return IntersectionType.Intersects;

        }

        return IntersectionType.None;

    }



}
 
