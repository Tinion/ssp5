﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(Quad2d))]
public class Quad2dEditor : Editor
{

    public override void OnInspectorGUI()
    {
        Quad2d myTarget = (Quad2d)target;
		myTarget.type = (SplineType)EditorGUILayout.EnumPopup("Type", myTarget.type);
        myTarget.a = EditorGUILayout.Vector3Field("a", myTarget.a);
        myTarget.b = EditorGUILayout.Vector3Field("b", myTarget.b);
        myTarget.c = EditorGUILayout.Vector3Field("aa", myTarget.c);
        myTarget.d = EditorGUILayout.Vector3Field("bb", myTarget.d);
        
        if (GUILayout.Button("Show"))
        {
            myTarget.Show();
        }
    }
}
