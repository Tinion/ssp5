﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(Quad2dMarker))]
public class Quad2dMarkerEditor : Editor
{
	bool isOpen = false;

    public override void OnInspectorGUI()
    {
        Quad2dMarker myTarget = (Quad2dMarker)target;
        myTarget.type = (SplineType)EditorGUILayout.EnumPopup("Type", myTarget.type);

		GUILayout.BeginHorizontal ();
		myTarget.isVisible = EditorGUILayout.Toggle("Visible in Editor", myTarget.isVisible);
		if (GUILayout.Button((isOpen) ? "Hide data" : "Show data", GUILayout.Height(20)))
		{
			isOpen = !isOpen;
		}
		GUILayout.EndHorizontal ();

		if (isOpen) 
		{
			for (int i = 0; i < myTarget.count; i++)
				myTarget.data [i] = EditorGUILayout.Vector2Field (i.ToString (), myTarget.data [i]);

			if (GUILayout.Button ("Add")) {
				myTarget.Add ();
			}
			if (GUILayout.Button (myTarget.isShow ? "Apply" : "Show")) {
				if (!myTarget.isShow)
					myTarget.Show ();
				else
					myTarget.Apply ();
			}
		}
    }
}
