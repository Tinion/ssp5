﻿using UnityEngine;
using System.Collections;

public class endLevelBlock : MonoBehaviour {

	public Quad2d quad2d;
	void Start()
	{
		quad2d = GetComponent<Quad2d> ();
	}

	void Update () 
	{
		if (GetChoise (Main.instance.hero.transform.position))
			Main.instance.EndMission();
	}

	bool GetChoise(Vector3 _Pos)
	{
		Vector3 Pos = transform.InverseTransformPoint(_Pos);

		if (Math3d.dotInTriangle(Pos, quad2d.a, quad2d.b, quad2d.c) ||
			Math3d.dotInTriangle(Pos, quad2d.b, quad2d.c, quad2d.d))
			return true;
		return false;

	}
}
