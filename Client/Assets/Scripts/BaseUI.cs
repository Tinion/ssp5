﻿using UnityEngine;
using System.Collections;

public class BaseUI : MonoBehaviour {

    public GameObject currentGameObject;
    public RectTransform currentRectTransform;
    public DurabilityInfo durabilityInfo;
}
