﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class CrossingPower
{
	public bool toForward;
	public bool toLeft;
	public bool toRight;
	public bool toLeftReverse;
	public bool toRightReverse;

	public CrossingPower()
	{
		toForward = false;
		toLeft = false;
		toRight = false;
		toLeftReverse = false;
		toRightReverse = false;
	}
}

[System.Serializable]
public class BiomesSource
{
    public string name;
    public Texture image;
    public List<RoadSourceData> roadSource;
	public CrossingPower[] crossingPower;

	public RoadSourceData Get(string _name)
	{
		for (int i = 0; i < roadSource.Count; i++)
			if (roadSource [i].name == _name)
				return roadSource [i];
		return roadSource [0];
	}

}



[System.Serializable]
public class RoadSourceData
{
    public string name;
    public bool isActive;
    public float coefficient;
    public float points;
	public bool isTransition;
	public int transitionToBiom;
	public bool isCheck;
    public RoadSourceType roadSourceType;
	public RoadSourceStyle roadSourceStyle;
}

[System.Serializable]
public class BiomesSources : ScriptableObject
{
    public List<BiomesSource> biomesSource;

    
}


