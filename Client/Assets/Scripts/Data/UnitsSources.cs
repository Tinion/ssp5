using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class UnitSource
{
	public string name;
	public List<string> resourcesName;
	public int image;
	public EnemyClass type;
	public EnemyType enemyType;
	public EnemyDirection direction;
	public int cost;
	public int tier;

	public UnitSource()
	{
		name = "New enemy";
		image = 0;
		type = EnemyClass.Fighter;
		direction = EnemyDirection.Traffic;
		enemyType = EnemyType.Civilian;
		cost = 0;
		tier = 0;
	}

}

[System.Serializable]
public class UnitsSources : ScriptableObject
{
	public List<UnitSource> unit;

	public int GetImage(EnemyType _type)
	{
		for (int i = 0; i < unit.Count; i++) 
		{
			if (unit [i].enemyType == _type)
				return unit [i].image;
		}

		return 0;
	}


}

