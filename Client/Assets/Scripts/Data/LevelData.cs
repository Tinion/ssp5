﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;
using System.Collections.Generic;

[System.Flags]
public enum EnemyType
	{
		Civilian = 0,
        Roadhog = 1,
        Scorcher = 2,
        BattleVan = 3,
        Baggy = 4,
        Bomber = 5,
        Flametruck = 6, 
        Plasmatron = 7, 
        Gunner = 8,
	}

[System.Flags]
public enum EnemyClass
{
    Fighter = 0,
    Attacker = 1,
    Neutral = 2,
}

[System.Flags]
public enum MiscType
	{
		Board,
		Barrels,
		Puddle,
        Wall,
        Column,
        Garbage,
        Radiation,
        Complex,
        BonusContainer,
	}

[System.Serializable]
public class EnemyCreation
{
    public string name;
    public Vector3 EnemyPos;
    public Quaternion EnemyRot;
    public Vector3 currentPosition;
    public int planeNumber;
    public int forkNumber;
    public float percent;
    public Vector2[] rect;

    public EnemyCreation(string _name, Vector3 _EnemyPos, Quaternion _EnemyRot, Vector3 _currentPosition, int _planeNumber, int _forkNumber, float _percent, Vector2[] _rect)
    {
        name = _name;
        EnemyPos = _EnemyPos;
        EnemyRot = _EnemyRot;
        currentPosition = _currentPosition;
        planeNumber = _planeNumber;
        forkNumber = _forkNumber;
        percent = _percent;
        rect = _rect;
    }
}

[System.Serializable]
public class MiscCreation
{
    public string name;
    public Vector3 Pos;
    public Quaternion Rot;
    public Vector3 Scale;
	public MiscType type;
    public int planeNumber;
    public int forkNumber;

    public MiscCreation(string _name, Vector3 _Pos, Quaternion _Rot, Vector3 _Scale, int _planeNumber, int _forkNumber, MiscType _type)
    {
        name = _name;
        Pos = _Pos;
        Rot = _Rot;
        Scale = _Scale;
        planeNumber = _planeNumber;
        forkNumber = _forkNumber;
        type = _type;

    }
}

[System.Serializable]
public class BlockData
{
    public string name;
    public int fork;
    public int reverseCount;

    public BlockData()
    {
        name = "";
        fork = -1;
        reverseCount = -1;
    }

    public BlockData(string _name)
    {
        name = _name;
        fork = -1;
        reverseCount = -1;
    }

    public BlockData(string _name, int _fork)
    {
        name = _name;
        fork = _fork;
        reverseCount = -1;
    }

    public BlockData(string _name, int _fork, int _reverseCount)
    {
        name = _name;
        fork = _fork;
        reverseCount = _reverseCount;
    }

}

[System.Serializable]
public class ForkData
{
    public List<BlockData> blocks;
   
    public ForkData()
    {
        blocks = new List<BlockData>();     
    }
}

[System.Serializable]
public class MiniMapSource
{
    public MiniMapCellType type;
    public bool hidden;

    public MiniMapSource(MiniMapCellType _type, bool _hidden)
    {
        type = _type;
        hidden = _hidden;
    }
}





[System.Serializable]
public class LevelData : ScriptableObject
{
    public static string assetName;
    public static bool isEdited = false;
    private static LevelData _instance;
	// Use this for initialization
    //public List<BlockData> blocks;
    public List<ForkData> forkData;
    public List<EnemyCreation> enemies;
    public List<MiscCreation> miscs;

    public List<MiniMapSource> miniMapSource;

    public float miniMapSize;
    public int miniMapCount;
    public Vector3 miniMapPosition;

    #if UNITY_EDITOR
    [MenuItem("Level/Edit")]
    private static bool Create()
    {
        isEdited = true;
        Main _Main;

        LoadInstance();

        if (_instance == null)
        {
            Debug.LogError("Cannot load " + assetName + " data");
            _instance = ScriptableObject.CreateInstance(typeof(LevelData)) as LevelData;
            //_instance.blocks = new List<BlockData>();
            _instance.forkData = new List<ForkData>();
            _instance.enemies = new List<EnemyCreation>();
            _instance.miscs = new List<MiscCreation>();
        }
        

        if (AssetDatabase.LoadAssetAtPath("Assets/Resources/" + assetName + ".asset", typeof(Object)) == null)
        {
            AssetDatabase.CreateAsset(_instance, "Assets/Resources/" + assetName + ".asset");
            AssetDatabase.SaveAssets();
        }
		_Main = FindObjectOfType<Main> ();
        _Main.BuildLevel(assetName);
        return isEdited;
        
    }

    
    private static void Add(GameObject target)
    {
        //EnemyCreation ETemp = null;
        //MiscCreation MTemp = null;
        if (target != null)
        {
            /*
            if (target.GetComponent<Enemy>() != null && _instance)
            {
                Vector2 PlanePos = target.GetComponent<Enemy>().GetNumberPlaneE();
                ETemp = new EnemyCreation(target.GetComponent<Enemy>().name,
                    target.transform.position, target.transform.rotation,
                    target.GetComponent<Enemy>().steerSpeed, target.GetComponent<Enemy>().startPower,
                    target.GetComponent<Enemy>().direction, (int)PlanePos.x, (int)PlanePos.y);
                _instance.enemies.Add(ETemp);
            }
            else
                if (target.GetComponent<Misc>() != null && _instance)
                {
                    Vector2 PlanePos = target.GetComponent<Misc>().GetNumberPlaneE();
                    MTemp = new MiscCreation(target.GetComponent<Misc>().name,
                        target.transform.position, target.transform.rotation, target.transform.localScale,
                        (int)PlanePos.x, (int)PlanePos.y);
                    _instance.miscs.Add(MTemp);
                }
             */
        }      
    }





    [MenuItem("Level/Save")]
    private static bool Save()
    {
        isEdited = false;
        //GameObject vehiclesFolder = GameObject.Find("Vehicles");
        //GameObject miscFolder = GameObject.Find("Misc");

        LoadInstance();

            if (_instance == null)
            {
                Debug.LogError("Cannot load " + assetName + " data");
                _instance = ScriptableObject.CreateInstance(typeof(LevelData)) as LevelData;
                //_instance.blocks = new List<BlockData>();
                _instance.forkData = new List<ForkData>();
                //_instance.enemies = new List<EnemyCreation>();
                //_instance.miscs = new List<MiscCreation>();
            }

            //if (_instance.enemies != null)
            //    _instance.enemies.Clear();
            //if (_instance.miscs != null)
            //    _instance.miscs.Clear();
        
        /*
        for (int i = 0; i<vehiclesFolder.transform.childCount; i++)
        {
            Add(vehiclesFolder.transform.GetChild(i).gameObject);
        }

        for (int i = 0; i < miscFolder.transform.childCount; i++)
        {
            Add(miscFolder.transform.GetChild(i).gameObject);
        }
        */

		//_Main = FindObjectOfType<Main> ();.SaveLevel();

        AssetDatabase.Refresh();
        EditorUtility.SetDirty(_instance);
        AssetDatabase.SaveAssets();

        return isEdited;


    }

    public static bool SaveMiniMapData(Vector3 _pos, int _count, float _size, List<MiniMapCell> data)
    {
        isEdited = false;
        LoadInstance();

        if (_instance == null)
        {
            Debug.LogError("Cannot load " + assetName + " data");
            _instance = ScriptableObject.CreateInstance(typeof(LevelData)) as LevelData;
            _instance.forkData = new List<ForkData>();
        }

        _instance.miniMapPosition = _pos;
        _instance.miniMapCount = _count;
        _instance.miniMapSize = _size;
        _instance.miniMapSource = new List<MiniMapSource>();
        for (int horizontal = 0; horizontal < _count; horizontal++)
        for (int vertical = 0; vertical < _count; vertical++)
            _instance.miniMapSource.Add(new MiniMapSource(data[horizontal + _count * vertical].type, data[horizontal + _count * vertical].hidden));


        AssetDatabase.Refresh();
        EditorUtility.SetDirty(_instance);
        AssetDatabase.SaveAssets();

        return isEdited;


    }

    static void LoadInstance()
    {
        assetName = EditorApplication.currentScene;
        int startIndex = assetName.LastIndexOf('/');
        assetName = assetName.Substring(startIndex + 1, 7);
        _instance = Resources.Load(assetName, typeof(Object)) as LevelData;
    }

    [MenuItem("Level/ChangeBlocks")]
    private static void Change()
    {
        //_instance.blocks = new List<BlockData>();
        /*
    _instance.blocks.Add("blockPlane");
    _instance.blocks.Add("blockPlane");
    _instance.blocks.Add("blockRoundRight");
    _instance.blocks.Add("blockRoundRight");
    _instance.blocks.Add("blockPlane");
    _instance.blocks.Add("blockFork");
    _instance.blocks.Add("blockPlane");
    _instance.blocks.Add("blockUp");
    _instance.blocks.Add("blockPlane02");
    _instance.blocks.Add("blockRoundLeft");
    _instance.blocks.Add("blockPlane02");
    _instance.blocks.Add("blockPlane02");
    _instance.blocks.Add("blockPlane02");
    _instance.blocks.Add("blockPlane02");
    _instance.blocks.Add("blockPlane02");
    _instance.blocks.Add("blockPlane02");
    _instance.blocks.Add("blockPlane02");
    _instance.blocks.Add("blockPlane02");
    _instance.blocks.Add("blockPlane02");
    _instance.blocks.Add("blockPlane02");
    _instance.blocks.Add("blockPlane02");
    _instance.blocks.Add("blockPlane02");
    _instance.blocks.Add("blockPlane02");
    _instance.blocks.Add("blockPlane02");
         */
    }
    
#endif
}


/*
                         
         */