﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using Dest.Math;

#if UNITY_EDITOR
using UnityEditor;
#endif

[System.Serializable]
public enum BonusSeasonType
{
    None,
    HealthUp,
    ShieldUp,
    Coins,
    Module,
}

[System.Flags]
public enum Disposable
{
    Repeat = 0,
    Once = 1,
    Immediately = 2,
}



[System.Serializable]
public enum ComplexType
{
    Line,
    Full,
}

[System.Serializable]
public class MiscSlot
{
    public MiscType miscType;
    public string miscName;
    public int max;
    public float cooficient;
    public float radius;
    public float offset = 0;
}

[System.Serializable]
public class ComplexMisc
{
    public string miscName;
    public ComplexType type;
    public MiscSlot[] Miscs;
}

[System.Serializable]
public class Season
{
    public string name;
    public Vector2 Difficult;
    public float TimeDuration;
    public float Coefficient;
    public Disposable isDisposable;

    public float timerMax;
    public float timerOffset;
    public int max;
}

[System.Serializable]
public class EnemySeason : Season
{
    public EnemyType enemyType;
    public EnemyDirection enemyDirection;
	public EnemySeason(string _name, EnemyType _type, EnemyDirection _dir, float _coeff, Disposable _isDisp)
	{
		name = _name;
		enemyType = _type;
		enemyDirection = _dir;
		Difficult = new Vector2(0.0f,1.0f);
		Coefficient = _coeff;
		isDisposable = _isDisp;
		TimeDuration = 5.0f;
		timerMax = 2.0f;
		timerOffset = 0.0f;
		max = 1;
	}
}

[System.Serializable]
public class BlockSeason
{
    public string name;
    public float Coefficient;


}

[System.Serializable]
public class MiscSeason : Season
{
	public MiscType miscType;

	public MiscSeason(string _name, MiscType _type, float _coeff, Disposable _isDisp)
	{
		name = _name;
		miscType = _type;
		Difficult = new Vector2(0.0f,1.0f);
		Coefficient = _coeff;
		isDisposable = _isDisp;
		TimeDuration = 5.0f;
		timerMax = 2.0f;
		timerOffset = 0.0f;
		max = 1;
	}
    
}

[System.Serializable]
public class BonusSeason : Season
{
    public BonusSeasonType type;
    public string current;

}

[System.Serializable]
public class EnemyCounter
{
    [System.Serializable]
    public class EnemyData
    {
        public EnemyType enemyType;
        public int current = 0;
        public int max = 0;

        public EnemyData(EnemyType _type)
        {
            enemyType = _type;
        }
    }

    public EnemyData[] enemyData;

    public EnemyData Find(EnemyType _type)
    {
        for (int i = 0; i < enemyData.Length; i++)
        {
            if (enemyData[i].enemyType == _type)
            {
                return enemyData[i];
            }
        }

        for (int i = 0; i < enemyData.Length; i++)
        {
            Debug.Log(enemyData[i].enemyType.ToString());
        }
        return null;
    }

    public void RecalculateMax(EnemySeason _current)
    {
        EnemyData data = Find(_current.enemyType);
        data.max = _current.max;
    }
}

[System.Serializable]
public class MiscCounter
{
    [System.Serializable]
    public class MiscData
    {
        public MiscType miscType;
        public int current = 0;
        public int max = 0;

        public MiscData(MiscType _type)
        {
            miscType = _type;
        }
    }

    public MiscData[] miscData;

    public MiscData Find(MiscType _type)
    {
        for (int i = 0; i < miscData.Length; i++)
        {
            if (miscData[i].miscType == _type)
            {
                return miscData[i];
            }
        }
        return null;
    }

    public void RecalculateMax(MiscSeason _current)
    {
		MiscData data = Find(_current.miscType);
		data.max = _current.max;
    }
}

[System.Serializable]
public class Ways
{
	public Vector2 pos;
	public int wayNumber;
	public int seasonMaster;
	public int roadMaster;
	public bool isUsed;
	public int side;


	public Ways(Vector2 _pos, int _wayNumber, int _seasonMaster, int _roadMaster, int _side)
	{
		pos = _pos;
		wayNumber = _wayNumber;
		seasonMaster = _seasonMaster;
		roadMaster = _roadMaster;
		isUsed = false;
		side = _side;
	}
}

[System.Serializable]
public class MapDataSource
{
	public int number;
	public string id;
	public int blockCount;
	public float time;
	public int tier;



	public List<SeasonActive> activeSeasons;
	public List<Ways> ways;

	public List<EnemySeason> fighterSeasons;
	public List<EnemySeason> attackerSeasons;
	public List<EnemySeason> neutralSeasons;

	public List<MiscSeason> miscSeasons;
	//public List<BonusSeason> bonusSeasons;
	//public List<BlockSeason> blockSeasons;
	//public List<ComplexMisc> complexMiscs;

	public Object dirLight;

	public MapDataSource(int _number, string _id)
	{
		number = _number;
		id = _id + _number.ToString();

		activeSeasons = new List<SeasonActive> ();
		ways = new List<Ways> ();
		ways.Add(new Ways(new Vector2(50.0f,200.0f), 0, 0, 0, 0));

		/*
		fighterSeasons = new List<EnemySeason> ();
		attackerSeasons = new List<EnemySeason> ();
		neutralSeasons = new List<EnemySeason> ();

		miscSeasons = new List<MiscSeason> ();
		bonusSeasons = new List<BonusSeason> ();
		blockSeasons = new List<BlockSeason> ();
		complexMiscs = new List<ComplexMisc> ();
		*/
	}
}

[System.Flags]
public enum RoadSourceType
{
    None = 0,
    ForkLeft = 1,
    ForkRight = 2,
    ForkBoth = 3,
    ForkLeftReverse = 4,
    ForkRightReverse = 5,
    ForkBothReverse = 6,

}

[System.Flags]
public enum RoadSourceStyle
{
	Forward = 0,
	RotateLeft = 1,
	RotateRight = 2,
	Shave = 3,
	ForkLeft = 4,
	ForkRight = 5,
	ReverseLeft = 6,
	ReverseRight = 7,
	ForkForward = 8,
	Event = 9,
	Outpost = 10,
}

[System.Flags]
public enum RoadElementType
{
    Default = 0,
    Custom = 1,
    Special = 2,
    Fork = 3,
    Connection = 4,
	Outpost = 5,
}

[System.Flags]
public enum ActiveEndType
{
    final = 0,
    next = 1,
    connect = 2,
}


[System.Serializable]
public class RoadElement
{
    public RoadElementType roadSourceType;
    public List<RoadSourceData> magazine;
    public int special;
    public RoadSourceType fork;
    public RoadSourceType connect;
    public int leftWay;
    public int rightWay;

    public int ForkRun(bool left, int planeRun)
    {
        if (left)
            return ((fork == RoadSourceType.ForkLeft) ? 0 : planeRun + 1);
        else
            return ((fork == RoadSourceType.ForkLeft) ? planeRun + 1 : 0);
    }

	public RoadElement(RoadElementType _type)
	{
		roadSourceType = _type;
		special = 0;
		leftWay = 0;
		rightWay = 0;
		fork = RoadSourceType.None;
		connect = RoadSourceType.None;
	}
};

[System.Serializable]
public class SeasonActive
{
    public string name;
    public int wayNumber;
    public List<RoadSourceData> roadMagazine;
    public List<RoadElement> roadElements;
    //public Vector2 pos;
    public int offset;
    public ActiveEndType endType;
    public int seasonConnect;
    public int roadConnect;
    public int biomeNumber;
	public int seasonPrev;
	public int seasonPrevRoad;
	public int height;

	public List<EnemySeason> fighterSeasons;
	public List<EnemySeason> attackerSeasons;
	public List<EnemySeason> neutralSeasons;
	public List<MiscSeason> miscSeasons;

	public SeasonActive(string _name, int _wayNumber, int _biomeNumber, int _prevSeason, int _prevSeasonRoad)
	{
		name = _name;
		wayNumber = _wayNumber;

		fighterSeasons = new List<EnemySeason> ();
		attackerSeasons = new List<EnemySeason> ();
		neutralSeasons = new List<EnemySeason> ();
		miscSeasons = new List<MiscSeason> ();
		endType = ActiveEndType.final;
		offset = 0;
		seasonConnect = 0;
		roadConnect = 0;
		biomeNumber = _biomeNumber;
		seasonPrev = _prevSeason;
		seasonPrevRoad = _prevSeasonRoad;

		roadElements = new List<RoadElement> ();
		for (int i = 0; i < 5; i++) 
		{
			roadElements.Add (new RoadElement (RoadElementType.Default));
		}
	}

	public List<EnemySeason> GetEnemySeason(int select)
	{
		switch (select) 
		{
		case 0:
			return fighterSeasons;
			break;
		case 1:
			return attackerSeasons;
			break;
		case 2:
			return neutralSeasons;
			break;
		}
		return  neutralSeasons;
	}
};

[System.Flags]
public enum EnemyDirection
{
    Forward,
    Back,
    Traffic,
    ForwardBack,
    Plane,
    Spline,
}

public class Generator : MonoBehaviour
{

    Quaternion forward;
    Quaternion back;
	public Font polaris;

	public SeasonActive activeSeason;
	public RoadElement activeRoad;

    public Texture roadSpecial;
    public Texture roadCustom;
    public Texture roadFork;
    public Texture roadConnect;
    public Texture bigConnect;
	public Texture[] editorIcons;
	public Texture[] biomeIcons;
	public Texture[] enemyIcons;
	public Texture[] miscIcons;
	public Texture[] buttonIcons;
	public Texture[] directionIcons;
	public Texture[] blockPlaneIcons;

	public Texture redBack;
	public Texture blueBack;
	public Texture greenBack;

	public Texture green;
	public Texture red;

	public MapsData mapData;
	public MapDataSource currentMap;
	public UnitsSources unitsSources;
	public BiomesSources biomeSources;
	public MiscSources miscSources;

    public EnemyCounter enemyCounter;
    public MiscCounter miscCounter;

    public Color fogDay = new Color(0.79f, 0.79f, 0.79f, 1.0f);
    public float fogDensityDay = 0.01f;
    public Color fogNight = new Color(0.0f, 0.0f, 0.0f, 1.0f);
    public float fogDensityNight = 0.015f;

    public Color skyDay = new Color(0.682f, 0.921f, 1.0f, 1.0f);
    public Color skyNight = new Color(0.0f, 0.0f, 0.0f, 1.0f);

    public Color bricksDay = new Color(1.0f, 1.0f, 1.0f, 1.0f);
    public Color bricksNight = new Color(0.435f, 0.435f, 0.435f, 1.0f);

    public float forwardSpawnDistance = 100;
    public float backSpawnDistance = 10;

    public bool isEnemyEnabled;
    public bool isMiscEnabled;
    public bool isBonusEnabled;
    public bool isBlockEnabled;
    public bool isBossLevel = false;

    //public bool isComplexActivate;
    //public int currentComplex;
    //public int currentComplexPart;
    //public float currentComplexOffset;

    public CounterTimerF bossDarken;
    public bool isNight = false;

    public EnemySeason[] currentEnemySeason;
    public MiscSeason currentMiscSeason;
    //public BonusSeason currentBonusSeason;
    //public BlockSeason currentBlockSeason;

    public Vector3 CamCircle;
    public TrafficPose one;
    public TrafficPose two;
    CounterTimerF[] enemySpawnTimer;
    CounterTimerF miscSpawnTimer;

    Vector3 miscSpawnPosition = Vector3.zero;
    float miscCurrentDist = 0.0f;
    float miscSpawnDist = 30.0f;
    float neutralSpawnDist = 15.0f;

    Vector3 bonusSpawnPosition = Vector3.zero;
    float bonusCurrentDist = 0.0f;
    float bonusSpawnDist = 30.0f;

    public float GeneratorTime = 0;
    public float MaxTime;
    public float[] currentEnemyTime;
    public float currentMiscTime = 0;
    public float currentBonusTime = 0;

    public Object HealthUp;
    public Object ShieldUp;
    public Object Coin;
    public Object moduleBonus;

    public int plus = 0;
    public int minus = 0;

    private bool isCoinsLine;
    private float coinsOffset;
    private float coinsCurrentOffset;
    private float coinsOffsetDir;
    private float coinsOffsetSum;

    private Vector3 coinsSpawnPoint;

    public UnityEngine.UI.Slider timeSlider;
    public UnityEngine.UI.Slider timeExSlider;
    public UnityEngine.UI.Slider blockSlider;


	public void CreateMap()
	{
		forward = Quaternion.Euler(new Vector3(0, 180, 0));
		back = Quaternion.identity;
		enemySpawnTimer = new CounterTimerF[3];
		currentEnemyTime = new float[3];
		for (int i = 0; i < 3; i++)
		{
			enemySpawnTimer[i] = new CounterTimerF(3.0f, 3.0f);
			currentEnemyTime[i] = 0;
		}

		enemyCounter = new EnemyCounter();
		enemyCounter.enemyData = new EnemyCounter.EnemyData[9];

		#if UNITY_EDITOR
		biomeSources = AssetDatabase.LoadAssetAtPath("Assets/Resources/biomes.asset", typeof(BiomesSources)) as BiomesSources;
		#endif

		enemyCounter.enemyData[0] = new EnemyCounter.EnemyData(EnemyType.Civilian);
		enemyCounter.enemyData[1] = new EnemyCounter.EnemyData(EnemyType.Roadhog);
		enemyCounter.enemyData[2] = new EnemyCounter.EnemyData(EnemyType.Scorcher);
		enemyCounter.enemyData[3] = new EnemyCounter.EnemyData(EnemyType.BattleVan);
		enemyCounter.enemyData[4] = new EnemyCounter.EnemyData(EnemyType.Baggy);
		enemyCounter.enemyData[5] = new EnemyCounter.EnemyData(EnemyType.Bomber);
		enemyCounter.enemyData[6] = new EnemyCounter.EnemyData(EnemyType.Flametruck);
		enemyCounter.enemyData[7] = new EnemyCounter.EnemyData(EnemyType.Plasmatron);
		enemyCounter.enemyData[8] = new EnemyCounter.EnemyData(EnemyType.Gunner);

		miscCounter = new MiscCounter();
		miscCounter.miscData = new MiscCounter.MiscData[4];

		miscCounter.miscData [0] = new MiscCounter.MiscData (MiscType.Barrels);
		miscCounter.miscData [1] = new MiscCounter.MiscData (MiscType.Column);
		miscCounter.miscData [2] = new MiscCounter.MiscData (MiscType.Board);
		miscCounter.miscData [3] = new MiscCounter.MiscData (MiscType.Wall);

		currentMap = mapData.maps [0];

		GeneratorTime = 0;
		MaxTime = currentMap.time;
		currentEnemyTime[0] = 0;
		currentEnemyTime[1] = 0;
		currentEnemyTime[2] = 0;
		currentMiscTime = 0;
		currentBonusTime = 0;

		currentEnemySeason = new EnemySeason[3];
		if (currentMap.activeSeasons [0].fighterSeasons!=null)
		if (currentMap.activeSeasons [0].fighterSeasons.Count>0)
		currentEnemySeason [0] = currentMap.activeSeasons [0].fighterSeasons[0];
		if (currentMap.activeSeasons [0].attackerSeasons!=null)
		if (currentMap.activeSeasons [0].attackerSeasons.Count>0)
		currentEnemySeason [1] = currentMap.activeSeasons [0].attackerSeasons[0];
		if (currentMap.activeSeasons [0].neutralSeasons!=null)
		if (currentMap.activeSeasons [0].neutralSeasons.Count>0)
		currentEnemySeason [2] = currentMap.activeSeasons [0].neutralSeasons[0];

		NextEnemySeason(0);
		NextEnemySeason(1);
		NextEnemySeason(2);

		if (currentMap.activeSeasons[0].miscSeasons!=null)
		if (currentMap.activeSeasons[0].miscSeasons.Count>0)
		currentMiscSeason = currentMap.activeSeasons[0].miscSeasons[0];
		NextMiscSeason ();

		for (int i = 0; i < 3; i++)
			CalculateEnemyMaxCounter(i);

		CalculateMiscMaxCounter();

	}

    void CalculateEnemyMaxCounter(int enemyClass)
    {
        if (currentEnemySeason[enemyClass] != null)
            enemyCounter.Find(currentEnemySeason[enemyClass].enemyType).max = currentEnemySeason[enemyClass].max;
    }

    void CalculateMiscMaxCounter()
    {
		if (currentMiscSeason != null)
			miscCounter.Find(currentMiscSeason.miscType).max = currentMiscSeason.max;
    }

    void NextEnemySeason(int enemyClass)
    {
		
        currentEnemySeason[enemyClass] = null;

        currentEnemyTime[enemyClass] = GeneratorTime;
        float currentDifficult = GeneratorTime / MaxTime;

        List<EnemySeason> currentSeasons = new List<EnemySeason>();

        switch ((EnemyClass)enemyClass)
        {
            case EnemyClass.Fighter:
			for (int i = 0; i < activeSeason.fighterSeasons.Count; i++)
				if (activeSeason.fighterSeasons[i].Difficult.x < currentDifficult && activeSeason.fighterSeasons[i].Difficult.y > currentDifficult)
					currentSeasons.Add(activeSeason.fighterSeasons[i]);
                break;
            case EnemyClass.Attacker:
			for (int i = 0; i < activeSeason.attackerSeasons.Count; i++)
				if (activeSeason.attackerSeasons[i].Difficult.x < currentDifficult && activeSeason.attackerSeasons[i].Difficult.y > currentDifficult)
					currentSeasons.Add(activeSeason.attackerSeasons[i]);
                break;
            case EnemyClass.Neutral:
			for (int i = 0; i < activeSeason.neutralSeasons.Count; i++)
				if (activeSeason.neutralSeasons[i].Difficult.x < currentDifficult && activeSeason.neutralSeasons[i].Difficult.y > currentDifficult)
					currentSeasons.Add(activeSeason.neutralSeasons[i]);
                break;
        }

        float maxcoof = 0;

        if (currentSeasons.Count == 0)
            return;

        for (int i = 0; i < currentSeasons.Count; i++)
            maxcoof += currentSeasons[i].Coefficient;

        float currentCoof = Random.Range(0, maxcoof);

        float coof = 0;
        int select = -1;

        for (int i = 0; i < currentSeasons.Count; i++)
        {
            coof += currentSeasons[i].Coefficient;
            if (coof >= currentCoof)
            {
                select = i;
                break;
            }
        }

        currentEnemySeason[enemyClass] = currentSeasons[select];
        enemySpawnTimer[enemyClass].max = currentEnemySeason[enemyClass].timerMax + Random.Range(0, currentEnemySeason[enemyClass].timerOffset*2)-currentEnemySeason[enemyClass].timerOffset;
        enemySpawnTimer[enemyClass].current = enemySpawnTimer[enemyClass].max;

        CalculateEnemyMaxCounter(enemyClass);



    }

    void NextMiscSeason()
    {
		currentMiscSeason = null;
       // MiscSeason old = currentMiscSeason;
       // currentMiscTime = GeneratorTime;
       // float currentDifficult = GeneratorTime / MaxTime;

		float currentDifficult = GeneratorTime / MaxTime;
        List<MiscSeason> currentSeasons = new List<MiscSeason>();

		for (int i = 0; i < activeSeason.miscSeasons.Count; i++)
        {
			//if (activeSeason.miscSeasons[i].Difficult.x < currentDifficult && activeSeason.miscSeasons[i].Difficult.y > currentDifficult)
					currentSeasons.Add(activeSeason.miscSeasons[i]);
        }

        float maxcoof = 0;

		if (currentSeasons.Count == 0)
			return;
		
        for (int i = 0; i < currentSeasons.Count; i++)
        {
            maxcoof += currentSeasons[i].Coefficient;
        }

        float currentCoof = Random.Range(0, maxcoof);

        float coof = 0;
        int select = -1;

        for (int i = 0; i < currentSeasons.Count; i++)
        {
            coof += currentSeasons[i].Coefficient;
            if (coof >= currentCoof)
            {
                select = i;
                break;
            }

        }


        currentMiscSeason = currentSeasons[select];

        miscSpawnDist = currentMiscSeason.timerMax + Random.Range(-currentMiscSeason.timerOffset, currentMiscSeason.timerOffset);

        CalculateMiscMaxCounter();
        
    }

    void NextBonusSeason()
    {
		/*
        bool isNone = false;
        isCoinsLine = false;

        if (currentBonusSeason.type != BonusSeasonType.None)
            isNone = true;




        currentBonusTime = GeneratorTime;
        float currentDifficult = GeneratorTime / MaxTime;

        List<BonusSeason> currentSeasons = new List<BonusSeason>();

		for (int i = 0; i < currentMap.bonusSeasons.Count; i++)
        {
			if (currentMap.bonusSeasons[i].Difficult.x < currentDifficult && currentMap.bonusSeasons[i].Difficult.y > currentDifficult)
			if (currentMap.bonusSeasons[i].type == BonusSeasonType.None && isNone || !isNone)
				currentSeasons.Add(currentMap.bonusSeasons[i]);
        }

        float maxcoof = 0;

        for (int i = 0; i < currentSeasons.Count; i++)
        {
            maxcoof += currentSeasons[i].Coefficient;
        }

        float currentCoof = Random.Range(0, maxcoof);

        float coof = 0;
        int select = -1;

        //if (maxcoof == 0)
        //    select = 0;

        for (int i = 0; i < currentSeasons.Count; i++)
        {
            coof += currentSeasons[i].Coefficient;
            if (coof >= currentCoof)
            {
                select = i;
                break;
            }

        }

        if (select == -1)
        {
            Debug.Log("-1 " + currentCoof + " " + maxcoof + " " + coof);
            for (int i = 0; i < currentSeasons.Count; i++)
            {
                Debug.Log(currentSeasons[i].Coefficient);
            }
        }

        currentBonusSeason = currentSeasons[select];

        bonusSpawnDist = currentBonusSeason.timerMax + Random.Range(0, currentBonusSeason.timerOffset*2)-currentBonusSeason.timerOffset;
		*/
    }

    public string getLevelPart(int runLine, int localForkNumber)
    {
        int forkRun = runLine;
        int planeRun = localForkNumber;
		activeSeason = currentMap.activeSeasons [forkRun];

		if (activeSeason.roadElements.Count <= planeRun)
        {
			switch (activeSeason.endType) 
			{
			case ActiveEndType.next:
				{
					int nextBiomeNumber = currentMap.activeSeasons [currentMap.activeSeasons [forkRun].seasonConnect].biomeNumber;

					for (int i = 0; i < biomeSources.biomesSource[activeSeason.biomeNumber].roadSource.Count; i++) 
					{
						RoadSourceData rsd = biomeSources.biomesSource [activeSeason.biomeNumber].roadSource[i];
						if (rsd.transitionToBiom == nextBiomeNumber && rsd.roadSourceType == RoadSourceType.None) 
						{
							Main.instance.forkRun = currentMap.activeSeasons [forkRun].seasonConnect;
							Main.instance.planeRun = -1;
							return rsd.name;
						}
					}
				}
				break;
			
			case ActiveEndType.connect:
           		{
				int nextBiomeNumber = currentMap.activeSeasons [activeSeason.seasonConnect].biomeNumber;
				Main.instance.isConnect = true;

					for (int i = 0; i < biomeSources.biomesSource[activeSeason.biomeNumber].roadSource.Count; i++)
                	{
						RoadSourceData rsd = biomeSources.biomesSource [activeSeason.biomeNumber].roadSource[i];
						if ((rsd.roadSourceType == RoadSourceType.ForkLeftReverse || rsd.roadSourceType == RoadSourceType.ForkRightReverse) && rsd.transitionToBiom == nextBiomeNumber)
                    	{
							Main.instance.forkRun = activeSeason.seasonConnect;
							Main.instance.planeRun = activeSeason.roadConnect;
							return  rsd.name;
                    	}
               		}
            	}
				break;

			case ActiveEndType.final:
				{
					for (int i = 0; i < biomeSources.biomesSource[activeSeason.biomeNumber].roadSource.Count; i++)
						if (biomeSources.biomesSource [activeSeason.biomeNumber].roadSource[i].roadSourceStyle == RoadSourceStyle.Outpost) 
						{
							Main.instance.forkRun = currentMap.activeSeasons [forkRun].seasonConnect;
							Main.instance.planeRun = -1;
							return biomeSources.biomesSource [activeSeason.biomeNumber].roadSource [i].name;
						}
				}
				break;
			}
        }


		activeRoad = activeSeason.roadElements [planeRun];

		switch (activeRoad.roadSourceType)
        {
            case RoadElementType.Default:
                {
                    float maxcoof = 0;

				for (int i = 0; i < activeSeason.roadMagazine.Count; i++)
					if (activeSeason.roadMagazine[i].isActive)
						maxcoof += activeSeason.roadMagazine[i].coefficient;

                    float currentCoof = Random.Range(0, maxcoof);
                    float coof = 0;
                    int select = -1;

				for (int i = 0; i < activeSeason.roadMagazine.Count; i++)
					if (activeSeason.roadMagazine[i].isActive)
                    {
						coof += activeSeason.roadMagazine[i].coefficient;
                        if (coof >= currentCoof)
                        {
                            select = i;
                            break;
                        }
                    }
					//Debug.Log (seasonActive.roadMagazine[select].name + " 2");
				return activeSeason.roadMagazine[select].name;
                }
            break;

            case RoadElementType.Custom:
                {
                    float maxcoof = 0;

				for (int i = 0; i < activeRoad.magazine.Count; i++)
					maxcoof += activeRoad.magazine[i].coefficient;

                    float currentCoof = Random.Range(0, maxcoof);
                    float coof = 0;
                    int select = -1;

				for (int i = 0; i < activeRoad.magazine.Count; i++)
                    {
					coof += activeRoad.magazine[i].coefficient;
                        if (coof >= currentCoof)
                        {
                            select = i;
                            break;
                        }
                    }
				return activeRoad.magazine[select].name;
                }
                break;

            case RoadElementType.Special:
				return  biomeSources.biomesSource[activeSeason.biomeNumber].roadSource[activeRoad.special].name;
                
                break;
            case RoadElementType.Fork:
                {

				int nextBiomeNumber = -1;
				switch (activeRoad.fork) 
				{
				case RoadSourceType.ForkLeft:
					nextBiomeNumber = currentMap.activeSeasons [activeRoad.leftWay].biomeNumber;
					break;
				case RoadSourceType.ForkRight:
					nextBiomeNumber = currentMap.activeSeasons [activeRoad.rightWay].biomeNumber;
					break;
				}

				for (int i = 0; i <  biomeSources.biomesSource[activeSeason.biomeNumber].roadSource.Count; i++)
                    {
					RoadSourceData rst = biomeSources.biomesSource [activeSeason.biomeNumber].roadSource [i];
					if (activeRoad.fork == rst.roadSourceType && nextBiomeNumber == rst.transitionToBiom)
						return  rst.name;
                        
                    }
                }
                break;

            case RoadElementType.Connection:
                {
				int nextBiomeNumber = -1;
				switch (activeRoad.connect) 
				{
				case RoadSourceType.ForkLeftReverse:
					nextBiomeNumber = currentMap.activeSeasons [activeRoad.leftWay].biomeNumber;
					break;
				case RoadSourceType.ForkRightReverse:
					nextBiomeNumber = currentMap.activeSeasons [activeRoad.rightWay].biomeNumber;
					break;
				}

				for (int i = 0; i <  biomeSources.biomesSource[activeSeason.biomeNumber].roadSource.Count; i++)
				{
					RoadSourceData rst = biomeSources.biomesSource [activeSeason.biomeNumber].roadSource [i];
					if (activeRoad.connect == rst.roadSourceType && nextBiomeNumber == rst.transitionToBiom)
						return  rst.name;
				}
			}
                break;
        }
        return "";
    }

   
    void CreateEnemy(string enemyName, float offset, float angle, Vector3 pos, int dir)
    {
        pos.y += 2.0f;
        Vector3 direction = new Vector3(0.0f, 180 + angle + 180 * dir, 0.0f);

		Unit newEnemy = Main.instance.EnemyPoolSystem.GetPooled(enemyName.ToLower());


		if (newEnemy != null) {
			newEnemy.currentGameObject.SetActive (true);

			//Debug.Log("+ " + AutoEnemy.GetInstanceID());

			EnemyVehicle enemyVehicle = newEnemy.GetComponent<EnemyVehicle> ();
			enemyVehicle.direction = dir == 0 ? false : true;
			Main.instance.activeUnitObjects.Add (enemyVehicle);

			enemyVehicle.transform.position = Math3d.RotateThis (new Vector3 (-offset, 0.0f, 0.0f), -angle, pos);

			enemyVehicle.currentSplineOffset = offset;

			enemyVehicle.transform.rotation = Quaternion.Euler (direction);
			enemyVehicle.currentSplineOffset = offset;

			enemyVehicle.PrepareObject ();


			if (!enemyVehicle.Snap ())
				Debug.LogError ("Traffic Controller: Snap Car Error" + enemyVehicle.currentGameObject.GetInstanceID () + " " + enemyVehicle.currentGameObject.name + " " + enemyVehicle.transform.position + " " + pos);
		} else
			Debug.LogError ("Enemy is null: " + enemyName.ToLower());
    }

    void CreateEnemySpline(string enemyName, float offset, float angle, Vector3 pos, int dir)
    {
        pos.y += 2.0f;
        Vector3 direction = new Vector3(0.0f, 180 + angle + 180 * dir, 0.0f);

        Unit AutoEnemy = Main.instance.EnemyPoolSystem.GetPooled(enemyName);


        if (AutoEnemy != null)
        {
            AutoEnemy.currentGameObject.SetActive(true);

            //Debug.Log("+ " + AutoEnemy.GetInstanceID());

            Unit AutoEnemyComponent = AutoEnemy.GetComponent<Scorcher>();
            Main.instance.activeUnitObjects.Add(AutoEnemyComponent);

            Main.instance.activeUnitObjects.Add(AutoEnemyComponent);

            AutoEnemyComponent.transform.position = Math3d.RotateThis(new Vector3(-offset, 0.0f, 0.0f), -angle, pos);

            AutoEnemyComponent.currentSplineOffset = offset;

            AutoEnemyComponent.transform.rotation = Quaternion.Euler(direction);


            AutoEnemyComponent.PrepareObject();


          
        }
        else
			Debug.Log("not find in pool system" + enemyName);
    }

    void CreateEnemyAir(string enemyName)
    {

        Unit AutoEnemy = Main.instance.EnemyPoolSystem.GetPooled(enemyName);
        if (AutoEnemy != null)
        {


            AutoEnemy.currentGameObject.SetActive(true);

            Unit AutoEnemyComponent = AutoEnemy.GetComponent<Unit>();
            Main.instance.activeUnitObjects.Add(AutoEnemyComponent);

            AutoEnemyComponent.transform.parent = Main.instance.mainCamera.targetController.transform;
            AutoEnemyComponent.transform.position = Vector3.zero;
            AutoEnemyComponent.PrepareObject();
        }

    }

    void CreateMisc(string miscName, float offset, float angle, Vector3 pos, int dir)
    {

        Vector3 direction = new Vector3(0.0f, Random.Range(0, 360), 0.0f);

		Misc  newMisc = Main.instance.MiscPoolSystem.GetPooled(miscName.ToLower());
        if (newMisc != null)
        {
			
            Vector3 Position = Vector3.zero;
            Vector3 Rotation = Vector3.zero;

            newMisc.currentGameObject.SetActive(true);
            Main.instance.activeMiscObjects.Add(newMisc);
            Vector3 posSnap = Math3d.RotateThis(new Vector3(-offset, 0.0f, 0.0f), -angle, pos);

			newMisc.isActive = true;

			//if (newMisc.miscType != MiscType.Board && newMisc.miscType != MiscType.Wall)
            //    newMisc.transform.rotation = Quaternion.Euler(direction);
            //else
            //    newMisc.transform.rotation = Quaternion.Euler(new Vector3(0.0f, angle, 0.0f));

			RaycastHit info;
			if (Math3d.RayCastGroundDown (posSnap, out info)) 
			{
				Vector3 normal = info.normal;
				Quaternion rotation = Quaternion.FromToRotation (Vector3.up, info.normal);
				Vector3 euler = rotation.eulerAngles;
				euler.y = angle;
				newMisc.transform.rotation = Quaternion.Euler (euler);
				newMisc.transform.position = info.point;
			}
           // newMisc.SetupColliders();

			/*
            if (Math3d.SnapObject(newMisc.transform, newMisc.colliderPos, out Position, out Rotation))
            {
                newMisc.transform.position = Position;
                newMisc.transform.rotation = Quaternion.Euler(Rotation);
            }
            else
            {
                Debug.LogError("Traffic Controller: Snap Misc Error" + newMisc.currentGameObject.GetInstanceID() + " " + newMisc.currentGameObject.name + " " + newMisc.transform.position);

            }
*/



        }
		else
			Debug.Log ("MISC IS NULL " + miscName.ToLower());

     }

    /*
    void SpawnEnemy(string enemyName, Vector3 position, Quaternion rotation)
    {
        GameObject AutoEnemy = Main.instance.EnemyPoolSystem.GetPooledVehicle(enemyName);

        AutoEnemy.SetActive(true);

        EnemyVehicle AutoEnemyComponent = AutoEnemy.GetComponent<EnemyVehicle>();
        AutoEnemyComponent.transform.position = position;
        AutoEnemyComponent.transform.rotation = rotation;
 
        AutoEnemyComponent.PrepareObject();
        AutoEnemyComponent.SetupColliders();
    
    }
    */

    void SetWeather(float fogDensity, Color fog, Color bricks, Color sky)
    {
        RenderSettings.fogDensity = fogDensity;
        RenderSettings.fogColor = fog;
        Main.instance.mainCamera.roadAffectCamera.backgroundColor = sky;
        BlockPlane random = FindObjectOfType<BlockPlane>();
        random.transform.Find("mesh").GetComponent<Renderer>().material.SetColor("_Color", bricks);
    }




    void Update()
	{
		Vector3 Center = Vector3.zero;
		Vector3 Right = Vector3.zero;
		Vector3 Left = Vector3.zero;
		Vector3 BackRight = Vector3.zero;
		Vector3 BackLeft = Vector3.zero;

		if (Main.instance.gameMode == Mode.Battle) {
			if (isEnemyEnabled) {
				for (int i = 0; i < 3; i++) 
				{
					if (enemySpawnTimer [i].isEnd (Time.deltaTime) || (i == 2 && Main.instance.mainCamera.pathDistNeutral >= neutralSpawnDist))
					{
						if (currentEnemySeason [i] == null)
							break;
						
						EnemyCounter.EnemyData currentData = enemyCounter.Find(currentEnemySeason[i].enemyType);
						int count = 0;
						for (int j=0; j<Main.instance.activeUnitObjects.Count; j++)
							if (Main.instance.activeUnitObjects[j].type == currentEnemySeason[i].enemyType)
								count++;
						if (count < currentData.max)
						{
							Main.instance.mainCamera.TrafficParams (out Center, out Right, out Left, out BackRight, out BackLeft);
							if (CalculateFirstLine (Center, Right, Left, BackRight, BackLeft)) 
							{
								
								enemySpawnTimer [i].Restore ();
								Main.instance.mainCamera.pathDistNeutral = 0;
								SwitchEnemy (i);
								break;
							}	
						} 
						else
						{
							enemySpawnTimer[i].max = currentEnemySeason[i].timerMax + Random.Range(0, currentEnemySeason[i].timerOffset*2)-currentEnemySeason[i].timerOffset;
						}
						

					}
				}
			}

			if (isMiscEnabled) {
				if (Main.instance.mainCamera.pathDistMisc >= miscSpawnDist) 
				{
					if (currentMiscSeason != null) 
					{
						Main.instance.mainCamera.TrafficParams (out Center, out Right, out Left, out BackRight, out BackLeft);
						if (CalculateFirstLine (Center, Right, Left, BackRight, BackLeft)) {

							Main.instance.mainCamera.pathDistMisc = 0;
							SwitchMisc ();

						}
					}
				}
			}
			GeneratorTime += Time.deltaTime;
			for (int i = 0; i < 3; i++) {
				if (currentEnemySeason [i] == null)
					NextEnemySeason (i);
				else if (GeneratorTime >= currentEnemyTime [i] + currentEnemySeason [i].TimeDuration)
					NextEnemySeason (i);
			}

			if (currentMiscSeason == null)
				NextMiscSeason ();
			else if (GeneratorTime >= currentMiscTime + currentMiscSeason.TimeDuration)
				NextMiscSeason ();

			timeSlider.value = GeneratorTime / MaxTime;
			if (GeneratorTime > MaxTime)
				timeExSlider.value = (GeneratorTime - MaxTime) / MaxTime;
			blockSlider.value = Main.instance.currentBlock / (float)Main.instance.blockCountLevel;
		}
	}
          


    

	void SwitchEnemy(int enemyClass)
    {
//		Debug.Log (one.point + " " + two.point);
		if (currentEnemySeason[enemyClass] == null)
            return;

		//EnemyCounter.EnemyData currentData = enemyCounter.Find(currentEnemySeason[enemyClass].enemyType);
		//Debug.Log ("Generator " + Main.instance.generator.enemyCounter.Find (currentEnemySeason[enemyClass].enemyType).current + " < " + (Main.instance.generator.enemyCounter.Find(currentEnemySeason[enemyClass].enemyType).current + 1).ToString ());
		//currentData.current++;
      
        int dir;
        TrafficPose currentPose;

        switch (currentEnemySeason[enemyClass].enemyDirection)
        {
            case EnemyDirection.Forward:
                currentPose = one;
                dir = 0;
                break;
            case EnemyDirection.ForwardBack:
                currentPose = one;
                dir = 1;
                break;
            case EnemyDirection.Back:
                {
                    currentPose = two;
                    dir = 1;
                    if (Main.instance.hero.forwardSpeedTact < 10.0f)
                    {
                        enemySpawnTimer[enemyClass].max = currentEnemySeason[enemyClass].timerMax + Random.Range(0, currentEnemySeason[enemyClass].timerOffset *2 )-currentEnemySeason[enemyClass].timerOffset;
                        //enemySpawnTimer[enemyClass].max += 1 - (Mathf.Clamp(Main.instance.hero.forwardSpeedTact, 0, 20.0f) / 20);

                        if (currentEnemySeason[enemyClass].isDisposable == Disposable.Immediately)
                            NextEnemySeason(enemyClass);

                        return;
                    }
                    break;
                }
            case EnemyDirection.Traffic:
                currentPose = one;
                dir = 0;
                break;
            case EnemyDirection.Plane:
                {
                    CreateEnemyAir(currentEnemySeason[enemyClass].enemyType.ToString());
                    //currentData.current++;

                    enemySpawnTimer[enemyClass].max = currentEnemySeason[enemyClass].timerMax + Random.RandomRange(0, currentEnemySeason[enemyClass].timerOffset * 2) - currentEnemySeason[enemyClass].timerOffset;
                    //enemySpawnTimer[enemyClass].max += 1 - (Mathf.Clamp(Main.instance.hero.forwardSpeedTact, 0, 20.0f) / 20);

                    if (currentEnemySeason[enemyClass].isDisposable == Disposable.Immediately)
                        NextEnemySeason(enemyClass);

                    return;
                }
                break;
            case EnemyDirection.Spline:
                {
                    currentPose = one;
                    
                    //enemySpawnTimer[enemyClass].max += 1 - (Mathf.Clamp(Main.instance.hero.forwardSpeedTact, 0, 20.0f) / 20);
                    float currentOffsetS = 0;
                    for (int i = 0; i < 5; i++)
                    {
                        currentOffsetS = Random.Range(0, currentPose.offset * 2) - currentPose.offset;
                        currentPose.offset = currentOffsetS;
                        if (GetEmptyLocation(currentPose))
                        {
                           // currentData.current++;
                            string enemyName = currentEnemySeason[enemyClass].enemyType.ToString();

                            CreateEnemySpline(enemyName, currentOffsetS, currentPose.angle, currentPose.point, 0);

                            enemySpawnTimer[enemyClass].max = currentEnemySeason[enemyClass].timerMax + Random.RandomRange(0, currentEnemySeason[enemyClass].timerOffset * 2) - currentEnemySeason[enemyClass].timerOffset;

                            if (currentEnemySeason[enemyClass].isDisposable == Disposable.Immediately)
                                NextEnemySeason(enemyClass);

                            return;
                        }
                    }

                    return;
                }
                break;
            default:
                currentPose = one;
                dir = 0;
                break;
        }



        float currentOffset = 0;
        for (int i = 0; i < 5; i++)
        {
           // if (Math3d.GetChanceOf(15))
              //  currentOffset = Main.instance.hero.offset;
            //else
             currentOffset = Random.Range(0, currentPose.offset * 2) - currentPose.offset;
			currentPose.offset = currentOffset;
            if (currentEnemySeason[enemyClass].enemyDirection == EnemyDirection.Traffic)
                dir = currentOffset > 0 ? 1 : 0;
            currentPose.offset = currentOffset;
            if (GetEmptyLocation(currentPose))
            {
 //               currentData.current++;
                string enemyName = currentEnemySeason[enemyClass].enemyType.ToString();
                if (currentEnemySeason[enemyClass].enemyType == EnemyType.Civilian)
                    enemyName = ((Math3d.GetChanceOf(67) == true) ? ((Math3d.GetChanceOf(50) == true) ? "Civilian02" : "Civilian01") : ((Math3d.GetChanceOf(50) == true) ? "Civilian03" : "Civilian"));

                CreateEnemy(enemyName, currentOffset, currentPose.angle, currentPose.point, dir);

				enemySpawnTimer[enemyClass].max = currentEnemySeason[enemyClass].timerMax + Random.RandomRange(0, currentEnemySeason[enemyClass].timerOffset * 2) - currentEnemySeason[enemyClass].timerOffset;

                if (currentEnemySeason[enemyClass].isDisposable == Disposable.Immediately)
                    NextEnemySeason(enemyClass);

                return;
            }
        }

        enemySpawnTimer[enemyClass].max = currentEnemySeason[enemyClass].timerMax + Random.RandomRange(0, currentEnemySeason[enemyClass].timerOffset*2)-currentEnemySeason[enemyClass].timerOffset;
        //enemySpawnTimer[enemyClass].max += 1 - (Mathf.Clamp(Main.instance.hero.forwardSpeedTact, 0, 20.0f) / 20);

       if (currentEnemySeason[enemyClass].isDisposable == Disposable.Immediately)
            NextEnemySeason(enemyClass);

    }


    void SwitchMisc()
    {
        TrafficPose currentPose;
        currentPose = one;

        if (currentPose.localNumber < Main.instance.passedLocalNumber)
        {
            miscSpawnPosition = currentPose.point;
            Debug.Log("miscSpawn: " + currentPose.point + "2");
            Debug.Log(currentPose.localNumber + " " + currentPose.percent + " " + Main.instance.passedLocalNumber + " " + Main.instance.passedPath);
            return;
        }
        else
        {
            if (currentPose.localNumber == Main.instance.passedLocalNumber && currentPose.percent < Main.instance.passedPath)
            {
                miscSpawnPosition = currentPose.point;
                Debug.Log("miscSpawn: " + currentPose.point + "3");
                Debug.Log(currentPose.localNumber + " " + currentPose.percent + " " + Main.instance.passedLocalNumber + " " + Main.instance.passedPath);
                return;
            }
        }
		/*
        int countTypes = currentMiscSeason.miscSlot.Length;
        
		float maxcoof = 0;


        List<MiscSlot> available = new List<MiscSlot>();

        for (int i = 0; i < countTypes; i++)
        {
            MiscCounter.MiscData data = miscCounter.Find(currentMiscSeason.miscSlot[i].miscType);
            if (data.current < data.max || data.miscType == MiscType.Complex)
            {
                available.Add(currentMiscSeason.miscSlot[i]);
            }
        }



        if (available.Count == 0)
        {
            //Debug.Log("miscSpawn: " + currentPose.point + "4");
            miscSpawnPosition = currentPose.point;
            return;
        }

        for (int i = 0; i < available.Count; i++)
        {
            maxcoof += available[i].cooficient;
        }

        float currentCoof = Random.Range(0, maxcoof);

        float coof = 0;
        int select = -1;

        for (int i = 0; i < countTypes; i++)
        {
            coof += currentMiscSeason.miscSlot[i].cooficient;
            if (coof >= currentCoof)
            {
                select = i;
                break;
            }

        }
        MiscSlot currentMiscSlot = currentMiscSeason.miscSlot[select];
		*/
		MiscCounter.MiscData currentData = miscCounter.Find(currentMiscSeason.miscType);

        int dir;

        float currentOffset = 0;
        for (int i = 0; i < 5; i++)
        {
            currentOffset = Random.Range(2.0f - currentPose.offset, currentPose.offset - 2.0f);

			if (currentMiscSeason.miscType == MiscType.Wall)
            {
                float r = Random.RandomRange(0, 100);
				if (currentMiscSeason.name == "Wall")
                    currentOffset = currentPose.offset - 0.38f;
                else
                    currentOffset = currentPose.offset - 0.93f;
                if (Math3d.GetChanceOf(50))
                    currentOffset *= -1;
            }
            else
                currentPose.offset = currentOffset;
            if (GetEmptyLocation(currentPose, 2.0f, false))
            {
				Debug.Log (currentPose.angle);
				CreateMisc(currentMiscSeason.miscType.ToString().ToLower(), currentOffset, currentPose.angle, currentPose.point, 0);
                    miscSpawnPosition = currentPose.point;

                    if (currentMiscSeason.isDisposable == Disposable.Immediately)
                        NextMiscSeason();

                //else
                //    CreateComplexMisc(currentMiscSlot.miscName, currentOffset, currentPose, one, 0);
                break;
            }
        }



    }
	/*
    void SwitchBonus()
    {

        if (currentBonusSeason.type == BonusSeasonType.None)
            return;

        TrafficPose currentPose = one;

        if (currentPose.localNumber < Main.instance.passedLocalNumber)
        {
            bonusSpawnPosition = currentPose.point;
            return;
        }
        else
        {
            if (currentPose.localNumber == Main.instance.passedLocalNumber && currentPose.percent < Main.instance.passedPath)
            {
                bonusSpawnPosition = currentPose.point;
                return;
            }
        }

        if (currentBonusSeason.type == BonusSeasonType.Coins)
        {
            if (!isCoinsLine)
            {
                for (int i = 0; i < 5; i++)
                {

                    float currentOffset = Random.Range(0, currentPose.offset*2)-currentPose.offset;
                    currentPose.offset = currentOffset;

                    if (GetEmptyLocation(currentPose, 1.0f, false) && currentPose.spline.GetCurrentWidth(currentPose.percent) > Mathf.Abs(currentPose.offset + 1.0f))
                    {
                        isCoinsLine = true;
                        coinsOffset = currentOffset;
                        coinsOffsetSum = 0;
                        coinsCurrentOffset = 0;
                        float r = Random.Range(0, 100.0f);
                        if (r < 33.0f)
                            coinsOffsetDir = -0.25f;
                        else
                            if (r < 66.0f)
                            coinsOffsetDir = 0.25f;
                        else
                            coinsOffsetDir = 0;
                        break;
                    }
                }

                if (!isCoinsLine)
                {

                    return;
                }

            }

            if (Vector3.Distance(coinsSpawnPoint, currentPose.point) > 4.5f)
            {
                coinsSpawnPoint = currentPose.point;
                coinsSpawnPoint.y += 2.0f;



                currentPose.offset = coinsOffset + coinsCurrentOffset;
                if (GetEmptyLocation(currentPose, 1.5f, false) && currentPose.spline.GetCurrentWidth(currentPose.percent) > Mathf.Abs(currentPose.offset + 1.5f))
                {
                    Vector3 coinPosition = Math3d.RotateThis(new Vector3(-currentPose.offset, 0.0f, 0.0f), -currentPose.angle, currentPose.point);

                    RaycastHit Hit;
                    int layerMask = 1 << 0;
                    layerMask = ~layerMask;

                    if (Physics.Raycast(coinPosition, Vector3.down, out Hit, Mathf.Infinity, layerMask))
                    {
                        coinPosition = Hit.point;
                    }

                    GameObject coin = GameObject.Instantiate(Coin, coinPosition, Quaternion.identity) as GameObject;
                    coinsOffsetSum += coinsOffsetDir;
                    coinsCurrentOffset = Mathf.Sin(coinsOffsetSum) * 5.0f;


                    bonusSpawnPosition = currentPose.point;
                }
                else
                    NextBonusSeason();
            }
            else
                return;

            return;
        }
        else
        {
            for (int i = 0; i < 5; i++)
            {

                currentPose.offset = Random.Range(0, currentPose.offset*2)-currentPose.offset;
                if (GetEmptyLocation(currentPose, 1.0f, false))
                {
                    Vector3 finalPosition = Math3d.RotateThis(new Vector3(-currentPose.offset, 0.0f, 0.0f), -currentPose.angle, currentPose.point);

                    RaycastHit Hit;
                    int layerMask = 1 << 0;
                    layerMask = ~layerMask;

                    if (Physics.Raycast(finalPosition, Vector3.down, out Hit, Mathf.Infinity, layerMask))
                    {
                        finalPosition = Hit.point;
                    }

                    switch (currentBonusSeason.type)
                    {
                        case BonusSeasonType.HealthUp:
                            {
                                GameObject healthUp = GameObject.Instantiate(HealthUp, finalPosition, Quaternion.identity) as GameObject;
                            }
                            break;
                        case BonusSeasonType.ShieldUp:
                            {
                                GameObject shieldUp = GameObject.Instantiate(ShieldUp, finalPosition, Quaternion.identity) as GameObject;
                            }
                            break;
                        case BonusSeasonType.Module:
                            {
                                GameObject module = GameObject.Instantiate(moduleBonus, finalPosition, Quaternion.identity) as GameObject;
                                Bonus currentBonus = module.GetComponent<Bonus>();
                                currentBonus.moduleName = currentBonusSeason.current;

                            }
                            break;
                    }

                    bonusSpawnPosition = currentPose.point;
                    break;
                }
            }
        }
        if (currentBonusSeason.isDisposable == Disposable.Immediately)
            NextBonusSeason();
    }
*/
    void SetCoinsDirection()
    {
    }

	/*
    int GetComplexIndex(string name)
    {
		for (int i = 0; i < currentMap.complexMiscs.Count; i++)
        {
			if (currentMap.complexMiscs[i].miscName == name)
            {
                return i;
            }
        }

        return -1;

    }
*/
	/*
    void CreateComplexMisc(string miscName, float offset, TrafficPose trafficPose, TrafficPose old, int dir)
    {

        isComplexActivate = true;
        Vector3 direction = new Vector3(0.0f, Random.Range(0, 360), 0.0f);
        currentComplex = GetComplexIndex(miscName);

		if (currentMap.complexMiscs[currentComplex].type == ComplexType.Line)
        {
            currentComplexOffset = offset;
            //Debug.Log(complexMiscs[currentComplex].Miscs[currentComplexPart].miscName + " " + currentComplexPart);
			CreateMisc(currentMap.complexMiscs[currentComplex].Miscs[currentComplexPart].miscName, currentComplexOffset, trafficPose.angle, trafficPose.point, dir);
            currentComplexPart++;
            //Debug.Log("miscSpawn: " + trafficPose.point + "6");
            miscSpawnPosition = trafficPose.point;
            
        }
        else
        {
			float coof = currentMap.complexMiscs[currentComplex].Miscs[currentComplexPart].cooficient;
			CreateMisc(currentMap.complexMiscs[currentComplex].Miscs[currentComplexPart].miscName, currentMap.complexMiscs[currentComplex].Miscs[currentComplexPart].offset * old.offset, old.angle, old.point, dir);
            currentComplexPart++;

			for (int i = currentComplexPart; i < currentMap.complexMiscs[currentComplex].Miscs.Length; i++)
				if (currentMap.complexMiscs[currentComplex].Miscs[currentComplexPart].cooficient == coof)
                {
					CreateMisc(currentMap.complexMiscs[currentComplex].Miscs[currentComplexPart].miscName, currentMap.complexMiscs[currentComplex].Miscs[currentComplexPart].offset * old.offset, old.angle, old.point, dir);
                    currentComplexPart++;
                }
            Debug.Log("miscSpawn: " + trafficPose.point + "1");
            miscSpawnPosition = trafficPose.point;

        }

		if (currentComplexPart >= currentMap.complexMiscs[currentComplex].Miscs.Length)
        {
            isComplexActivate = false;
            currentComplexPart = 0;
            //currentMiscSeason.timerMax = complexMiscs[currentComplex].Miscs[currentComplexPart].cooficient;

            NextMiscSeason();
        }

    }
	*/

    bool GetEmptyLocation(TrafficPose trafficPose)
    {

        Vector2 axis1 = trafficPose.axis;
        Vector2 axis0 = new Vector2(trafficPose.axis.y, -trafficPose.axis.x);
        Vector2 center = trafficPose.point.ToVector2XZ() + axis0 * -trafficPose.offset;
        Vector2 extens = new Vector2(1.2f, 6.5f);

        Box2 current = new Box2(center, axis0, axis1, extens);



        for (int i = 0; i < Main.instance.activeUnitObjects.Count; i++)
        {
            if (TrafficController.IntersectBounds(Main.instance.activeUnitObjects[i], current, false))
                return false;
        }

        for (int i = 0; i < Main.instance.activeMiscObjects.Count; i++)
        {
            if (TrafficController.IntersectBounds(Main.instance.activeMiscObjects[i], current, false))

                return false;
        }

		if (TrafficController.IntersectBounds(Main.instance.hero.vehicle, current, false))
            return false;


        return true;
    }

    bool GetEmptyLocation(TrafficPose trafficPose, float radius, bool walls)
    {
        Vector2 point = Math3d.RotateThis2d(new Vector3(-trafficPose.offset, 0.0f, 0.0f), -trafficPose.angle, trafficPose.point);

        Circle2 current = new Circle2(point, radius);

        for (int i = 0; i < Main.instance.unitObjects.Count; i++)
        {
            if (TrafficController.IntersectBounds(Main.instance.unitObjects[i], current, false))
                return false;
        }

        for (int i = 0; i < Main.instance.miscObjects.Count; i++)
        {
            if (TrafficController.IntersectBounds(Main.instance.miscObjects[i], current, false))
                return false;
        }

		if (TrafficController.IntersectBounds(Main.instance.hero.vehicle, current, false))
            return false;

        if (walls)
        {
			BlockPlane[] planes = Main.instance.BlockPlanes.ToArray ();
            for (int i = 0; i < planes.Length; i++)
            {
                //if (TrafficController.IntersectBounds(planes[i], current, false))
                //    return false;
            }
        }


        return true;
    }

	/*
    public bool CalculateOnlyFirstLine(Vector3 Center, Vector3 Right, Vector3 Left, Vector3 BackRight, Vector3 BackLeft)
    {
        //Profiler.BeginSample("DetectLine");
        if (DetectLine(Center, Right, Left, out one, true))
        {
            return true;
        }
        if (DetectLine(Center, Right, BackRight, out one, true))
        {
            return true;
        }
        if (DetectLine(Center, Left, BackLeft, out one, true))
        {
            return true;
        }
        if (DetectLine(Center, BackLeft, BackRight, out one, true))
        {
            return true;
        }

        return false;
    }

     public TrafficPose GetPositionOnSplineAdd(BlockPlane blockPlane, float percent, float dist, bool direction)
    {
        TrafficPose source
        float addPercent;
        float _dist;
        Vector3 result = Vector3.zero;
        if (blockPlane.spline.UniformGlobalPosAdd(percent, dist, out result, out addPercent, out _dist, direction))
        {
            source.point = result;
            source.localNumber = blockPlane.localForkNumber;
            source.offset = blockPlane.spline.GetCurrentWidth(percent) - 1.0f;
            Vector3 dir = blockPlane.spline.GetAngle(percent);
            source.angle = Math3d.GetAngle(dir.x, dir.z, Vector3.forward.x, Vector3.forward.z);
            source.spline = blockPlane.spline;
            source.axis = dir.ToVector2XZ();
            source.axis.Normalize();
            return source;
        }
        else
        {
            if (blockPlane.nextBlock.spline.UniformGlobalPosAdd(direction ? 0 : 1.0f, dist - _dist, out result, out addPercent, out _dist, direction))
            {
                source.point = result;
                source.localNumber = blockPlane.nextBlock.localForkNumber;
                source.offset = blockPlane.nextBlock.spline.GetCurrentWidth(direction ? 0 : 1.0f) - 1.0f;
                Vector3 dir = blockPlane.nextBlock.spline.GetAngle(percent);
                source.angle = Math3d.GetAngle(dir.x, dir.z, Vector3.forward.x, Vector3.forward.z);
                source.spline = blockPlane.nextBlock.spline;
                source.axis = dir.ToVector2XZ();
                source.axis.Normalize();
                return source;
            }
            else
            {
                
                return source;
            }
        }
        
    }
    */
    TrafficPose CheckCurrentBlock(BlockPlane blockPlane, float percent, float dist, bool direction)
    {
        TrafficPose trafficPose;
        Vector3 result;
        float _dist;
        float addPercent;
		if (blockPlane.type == BlockPlaneType.Outpost)
		{
			return new TrafficPose ();
		}

        if (blockPlane.spline != null)
        {
            if (blockPlane.spline.UniformGlobalPosAdd(percent, dist, 0, out result, out addPercent, out _dist, direction))
            {
                trafficPose = new TrafficPose();
                trafficPose.point = result;
                trafficPose.localNumber = blockPlane.localForkNumber;
				trafficPose.offset = blockPlane.spline.GetCurrentWidth(addPercent) - 1.0f;

				Vector3 dir = blockPlane.spline.GetTangent(addPercent);
//				Debug.Log (dir);
                trafficPose.angle = Math3d.GetAngle(dir.x, dir.z, Vector3.forward.x, Vector3.forward.z);
			//	Debug.Log (trafficPose.angle);
                trafficPose.spline = blockPlane.spline;
                trafficPose.axis = dir.ToVector2XZ();
                trafficPose.axis.Normalize();
                return trafficPose;
            }
            else
            {
                BlockPlane next = blockPlane.GetNextBlock(direction, SplineType.Front);
                if (next == null)
                    Debug.LogError("Next block is not exist " + blockPlane.GetInstanceID());
                else
                    return CheckCurrentBlock(next, direction ? 0 : 1.0f, dist - _dist, direction);

            }
        }
        else
            Debug.LogError("Generator: Error spline in check " + blockPlane.GetInstanceID());
        trafficPose = new TrafficPose();
        return trafficPose;
    }


    public bool CalculateFirstLine(Vector3 Center, Vector3 Right, Vector3 Left, Vector3 BackRight, Vector3 BackLeft)
    {
        //one.point = camPlane.spline.GlobalPos(Main.instance.mainCamera.GetCamPercent());
        //one = GetPositionOnSplineAdd(Main.instance.mainCamera.GetCurrentCamPlane(), Main.instance.mainCamera.GetCamPercent(), forwardSpawnDistance, true);
        
        one = CheckCurrentBlock(Main.instance.mainCamera.GetCurrentCamPlane(), Main.instance.mainCamera.GetCamPercent(), forwardSpawnDistance, true);
        
        Vector3 _right = Math3d.RotateThis(one.offset, -one.angle, one.point);
        Vector3 _left = Math3d.RotateThis(-one.offset, -one.angle, one.point);
       // Debug.DrawLine(_right, one.point, Color.yellow);
       // Debug.DrawLine(_left, one.point, Color.magenta);
       // Debug.DrawLine(one.point, one.point + Vector3.up * 5.0f, Color.red);
        
        
		two = CheckCurrentBlock(Main.instance.mainCamera.GetCurrentCamPlane(), Main.instance.mainCamera.GetCamPercent(), backSpawnDistance, false);

        _right = Math3d.RotateThis(two.offset, -two.angle, two.point);
        _left = Math3d.RotateThis(-two.offset, -two.angle, two.point);
       // Debug.DrawLine(_right, two.point, Color.yellow);
       // Debug.DrawLine(_left, two.point, Color.magenta);
      //  Debug.DrawLine(two.point, two.point + Vector3.up * 5.0f, Color.red);
        
        return true;
        /*
        if (camPlane.spline.UniformGlobalPosAdd(_Main.maMain.instanceera.GetCamPercent(), forwardSpawnDistance, out one.point, out addPercent, out passed, true))
        {
            one.localNumber = camPlane.localForkNumber;
            one.offset = 7.0f;// spline.GetCurrentWidth(trafficPose.percent) - 1.0f;
            one.angle = 0;// Math3d.GetAngle(dir.x, dir.z, Vector3.forward.x, Vector3.forward.z);// -BlockPlanes[i].transform.eulerAngles.y;
            one.spline = _Main.heMain.instanceline;
            one.axis = dir.ToVector2XZ();
            one.axis.Normalize();

            Vector3 _right = Math3d.RotateThis(one.offset, -one.angle, one.point);
            Vector3 _left = Math3d.RotateThis(-one.offset, -one.angle, one.point);
            Debug.DrawLine(_right, one.point, Color.yellow);
            Debug.DrawLine(_left, one.point, Color.magenta);
            Debug.DrawLine(one.point, one.point + Vector3.up * 5.0f, Color.red);
            return true;
        }
        else
            return false;
        */
        /*

        //Profiler.BeginSample("DetectLine");
        if (DetectLine(Center, Right, Left, out one, true))
        {
            if (CalculateSecondLine(DetectType.Front, Center, Right, Left, BackRight, BackLeft))
                return true;

        }
        if (DetectLine(Center, Right, BackRight, out one, true))
        {
            if (CalculateSecondLine(DetectType.Right, Center, Right, Left, BackRight, BackLeft))
                return true;

        }
        if (DetectLine(Center, Left, BackLeft, out one, true))
        {
            if (CalculateSecondLine(DetectType.Left, Center, Right, Left, BackRight, BackLeft))
                return true;

        }
        if (DetectLine(Center, BackLeft, BackRight, out one, true))
        {
            if (CalculateSecondLine(DetectType.Back, Center, Right, Left, BackRight, BackLeft))
                return true;
        }
        //Debug.Log("NONALL");
        return false;
         */
    }


	/*
    public bool DetectLine(Vector3 A1, Vector3 A2, Vector3 A3, out TrafficPose trafficPose, bool front)
    {

        trafficPose = new TrafficPose();


        CamCircle = Math3d.IntersectTri(A1, A2, A3);



        for (int i = 0; i < _Main.BlMain.instanceanes.Count; i++)
        {

            if (_Main.BlMain.instanceanes[i].type == BlockPlaneType.BlockPlane)
            {

                if (DetectSpline(_Main.BlMain.instanceanes[i], _Main.BlMain.instanceanes[i].spline, A1, A2, A3, out trafficPose, front))
                    return true;
            }
            
            
            
            else
            {
                Spline[] splines = _Main.BlMain.instanceanes[i].splines;
                TrafficPose[] trafficPoses = new TrafficPose[splines.Length];
                List<TrafficPose> trafficPosesResult = new List<TrafficPose>();

                for (int splineNum = 0; splineNum < splines.Length; splineNum++)
                {
                    trafficPoses[splineNum] = new TrafficPose();
                    if (DetectSpline(_Main.BlMain.instanceanes[i], splines[splineNum], A1, A2, A3, out trafficPoses[splineNum], front))
                        trafficPosesResult.Add(trafficPoses[splineNum]);
                }

                if (trafficPosesResult.Count > 0)
                {
                    int rand = Random.Range(0, trafficPosesResult.Count);
                    trafficPose = trafficPoses[rand];
                    return true;
                }

            }



        }
        return false;
    }

    public bool DetectSpline(BlockPlane block, Spline spline, Vector3 A1, Vector3 A2, Vector3 A3, out TrafficPose trafficPose, bool front)
    {
        trafficPose = new TrafficPose();
        Vector3 dir = Vector3.zero;
        float addPercent;
        float passed;

        
            if (spline.UniformGlobalPosAdd(_Main.maMain.instanceera.GetCamPercent(), 100.0f, out trafficPose.point, out addPercent, out passed, !front))
            {
                trafficPose.localNumber = block.localForkNumber;
                trafficPose.offset = spline.GetCurrentWidth(trafficPose.percent) - 1.0f;
				Debug.Log (trafficPose.angle + " angle");
				trafficPose.angle = spline.GetAngle (trafficPose.percent).y;//Math3d.GetAngle(dir.x, dir.z, Vector3.forward.x, Vector3.forward.z);// -BlockPlanes[i].transform.eulerAngles.y;
                trafficPose.spline = spline;
                trafficPose.axis = dir.ToVector2XZ();
                trafficPose.axis.Normalize();

                Vector3 _right = Math3d.RotateThis(trafficPose.offset, -trafficPose.angle, trafficPose.point);
                Vector3 _left = Math3d.RotateThis(-trafficPose.offset, -trafficPose.angle, trafficPose.point);
                Debug.DrawLine(_right, trafficPose.point, Color.yellow);
                Debug.DrawLine(_left, trafficPose.point, Color.magenta);
                Debug.DrawLine(trafficPose.point, trafficPose.point + Vector3.up * 5.0f, Color.red);
                return true;
            }
            else
            return false;
        
       
    }
	*/

	public bool CrossTriangle(OneSpline spline, Vector3 A1, Vector3 A2, Vector3 A3, out Vector3 Point, out Vector3 Dir, out float percent)
    {
        Point = Vector3.zero;
        Dir = Vector3.zero;
        percent = 0.0f;

        for (float i = 0; i < 1.0f; i += 0.05f)
        {
            if (Math3d.GetInterceptPoint(out Point, A1, A2, A3, spline.GlobalPos(i), spline.GlobalPos(i + 0.05f)))
            {
                Dir = spline.GetTangent(i);
                percent = i;
                return true;
            }

        }
        return false;

    }



	/*
    bool CalculateSecondLine(DetectType detectType, Vector3 Center, Vector3 Right, Vector3 Left, Vector3 BackRight, Vector3 BackLeft)
    {
        switch (detectType)
        {
            case DetectType.Front:
                {
                    if (DetectLine(Center, BackRight, BackLeft, out two, false))
                    {
                        return true;
                    }
                    if (DetectLine(Center, Left, BackLeft, out two, false))
                    {
                        return true;
                    }

                    if (DetectLine(Center, Right, BackRight, out two, false))
                    {
                        return true;
                    }
                }
                break;

            case DetectType.Right:
                {
                    if (DetectLine(Center, Left, BackLeft, out two, false))
                    {
                        return true;
                    }
                    if (DetectLine(Center, Right, Left, out two, false))
                    {
                        return true;
                    }

                    if (DetectLine(Center, BackRight, BackLeft, out two, false))
                    {
                        return true;
                    }
                }
                break;

            case DetectType.Left:
                {
                    if (DetectLine(Center, Right, BackRight, out two, false))
                    {
                        return true;
                    }
                    if (DetectLine(Center, Right, Left, out two, false))
                    {
                        return true;
                    }

                    if (DetectLine(Center, BackRight, BackLeft, out two, false))
                    {
                        return true;
                    }
                }
                break;

            case DetectType.Back:
                {
                    if (DetectLine(Center, Right, Left, out two, false))
                    {
                        return true;
                    }
                    if (DetectLine(Center, Right, BackRight, out two, false))
                    {
                        return true;
                    }

                    if (DetectLine(Center, Left, BackLeft, out two, false))
                    {
                        return true;
                    }
                }
                break;
        }

        return false;

    }
*/
    void OnDrawGizmos()
    {

        //if (CamCircle.z > 0)
        //   TrafficController.DrawCircle(new Vector3(CamCircle.x, 0.0f, CamCircle.y), CamCircle.z);

        Vector3 myPosition = new Vector3(transform.position.z, transform.position.y, transform.position.x);
        float angle = 0.0f;
        float angle2 = 0.0f;

        /*
        for (int i = 0; i < CurveLenght; i++)
        {
            Vector3 partPos;
            float partScale;
            Vector3 dir = GetAngle(i, false);
            Vector3 dir2 = GetAngle(i, true);
            angle = Math3d.GetAngle(dir.z, dir.x, Vector3.forward.z, Vector3.forward.x);
            angle2 = Math3d.GetAngle(dir2.z, dir2.x, Vector3.forward.z, Vector3.forward.x);


            if (i == 0)
            {
                startPosition = transform.position;
                partPos = startPosition;
                partScale = startScale;

                if (curve[0].aa != null)
                    curve[0].aaPosition = curve[0].aa.position;

                //Gizmos.DrawLine(partPos + (partScale * dir), partPos + (-partScale * dir));
                Gizmos.DrawLine(Math3d.RotateThis(partScale, angle, partPos), Math3d.RotateThis(-partScale, angle, partPos));
            }
            else
            {
                partPos = curve[i - 1].position;
                partScale = curve[i - 1].scale;

                if (curve[i].aa != null)
                {
                    float dist = Vector3.Distance(curve[i - 1].position, curve[i - 1].bbPosition);
                    Vector3 direction = curve[i - 1].position - curve[i - 1].bbPosition;
                    direction.Normalize();
                    curve[i].aaPosition = dist * direction + curve[i - 1].position;
                    curve[i].aa.position = curve[i].aaPosition;
                }
            }

            Vector3 offsetLeftStart = Math3d.RotateThis(partScale, angle, Vector3.zero);
            Vector3 offsetRightStart = Math3d.RotateThis(-partScale, angle, Vector3.zero);
            Vector3 offsetLeftEnd = Math3d.RotateThis(curve[i].scale, angle2, Vector3.zero);
            Vector3 offsetRightEnd = Math3d.RotateThis(-curve[i].scale, angle2, Vector3.zero);

            if (curve[i].bb != null)
                curve[i].bbPosition = curve[i].bb.position;
            if (curve[i].part != null)
                curve[i].position = curve[i].part.position;

            curve[i].prepareIntersect = Math3d.PrepareIntersectQuad(partPos + offsetLeftStart, partPos + offsetRightStart, curve[i].position + offsetRightEnd, curve[i].position + offsetLeftEnd);

            for (float j = 0; j < 20.0f; j += 1.0f)
            {

                Gizmos.color = Color.blue;

                Gizmos.DrawLine(Math3d.CubicBezierLerp(partPos, curve[i].aaPosition, curve[i].bbPosition, curve[i].position, j / 20.0f),
                    Math3d.CubicBezierLerp(partPos, curve[i].aaPosition, curve[i].bbPosition, curve[i].position, (j + 1.0f) / 20.0f)); ;

                Gizmos.DrawLine(Math3d.CubicBezierLerp(partPos + offsetLeftStart, curve[i].aaPosition + offsetLeftStart, curve[i].bbPosition + offsetLeftEnd, curve[i].position + offsetLeftEnd, j / 20.0f), Math3d.CubicBezierLerp(partPos + offsetLeftStart, curve[i].aaPosition + offsetLeftStart, curve[i].bbPosition + offsetLeftEnd, curve[i].position + offsetLeftEnd, (j + 1.0f) / 20.0f));
                Gizmos.DrawLine(Math3d.CubicBezierLerp(partPos + offsetRightStart, curve[i].aaPosition + offsetRightStart, curve[i].bbPosition + offsetRightEnd, curve[i].position + offsetRightEnd, j / 20.0f), Math3d.CubicBezierLerp(partPos + offsetRightStart, curve[i].aaPosition + offsetRightStart, curve[i].bbPosition + offsetRightEnd, curve[i].position + offsetRightEnd, (j + 1.0f) / 20.0f));

            }

            Gizmos.DrawLine(Math3d.RotateThis(-partScale, angle2, curve[i].position), Math3d.RotateThis(partScale, angle2, curve[i].position));

            Gizmos.color = Color.green;
            DrawCircle(new Vector3(curve[i].prepareIntersect.x, myPosition.y, curve[i].prepareIntersect.y), curve[i].prepareIntersect.z);



        }
        Gizmos.color = Color.blue;
        if (CurveLenght > 0)
        {
            //Gizmos.DrawLine( + ( * GetAngle(CurveLenght - 1, true)), curve[CurveLenght - 1].position + (curve[CurveLenght - 1].scale * GetAngle(CurveLenght - 1, true)));
            Gizmos.DrawLine(Math3d.RotateThis(-curve[CurveLenght - 1].scale, angle2, curve[CurveLenght - 1].position), Math3d.RotateThis(curve[CurveLenght - 1].scale, angle2, curve[CurveLenght - 1].position));
        }
        */
    }
#if UNITY_EDITOR

    EnemySeason[] LoadEnemyClass(XmlNode enemyClass, int count)
    {
		
        int seasonCount = 0;
        XmlNodeList enemies = enemyClass.ChildNodes;
        EnemySeason[] data = new EnemySeason[enemies.Count];
        foreach (XmlNode enemy in enemies)
        {
			data [seasonCount] = new EnemySeason (enemy.Attributes.GetNamedItem ("name").Value, (EnemyType)System.Enum.Parse (typeof(EnemyType), enemy.Attributes.GetNamedItem ("type").Value), (EnemyDirection)System.Enum.Parse (typeof(EnemyDirection), enemy.Attributes.GetNamedItem ("direction").Value),
				float.Parse (enemy.Attributes.GetNamedItem ("cofficient").Value), (Disposable)System.Enum.Parse (typeof(Disposable), enemy.Attributes.GetNamedItem ("disposable").Value));
			
            data[seasonCount].Difficult = new Vector2();
            data[seasonCount].Difficult.x = float.Parse(enemy.Attributes.GetNamedItem("diffmin").Value);
            data[seasonCount].Difficult.y = float.Parse(enemy.Attributes.GetNamedItem("diffmax").Value);
            data[seasonCount].TimeDuration = float.Parse(enemy.Attributes.GetNamedItem("duration").Value);
            data[seasonCount].timerMax = float.Parse(enemy.Attributes.GetNamedItem("timer").Value);
            data[seasonCount].timerOffset = float.Parse(enemy.Attributes.GetNamedItem("timerOffset").Value);
            data[seasonCount].max = int.Parse(enemy.Attributes.GetNamedItem("max").Value);
            seasonCount++;

        }
        

        return data;
    }

    public void SaveBiomes()
    {
		/*
        BiomesSources currentSource = ScriptableObject.CreateInstance<BiomesSources>();
        MapData current = maps[0];

        BiomesSource biomSource = new BiomesSource();
        biomSource.name = "Doroga";
        biomSource.roadSource = new List<RoadSourceData>();
        

		for (int i = 0; i < biomeSources.biomesSource [current.activeSeasons[0].biomeNumber].roadSource.Count; i++)
        {
            RoadSourceData currentRoadSourceData = new RoadSourceData();
			RoadSource roadSource = biomeSources.biomesSource [current.activeSeasons [0].biomeNumber].roadSource[i];
			currentRoadSourceData.name = roadSource.name;
			currentRoadSourceData.isActive = roadSource.isActive;
			currentRoadSourceData.roadSourceType = roadSource.roadSourceType;
			currentRoadSourceData.coefficient = roadSource.coefficient;
            biomSource.roadSource.Add(currentRoadSourceData);
        }

        currentSource.biomesSource = new List<BiomesSource>();
        currentSource.biomesSource.Add(biomSource);

        if (AssetDatabase.LoadAssetAtPath("Assets/Resources/biomes.asset", typeof(Object)) == null)
        {
            AssetDatabase.CreateAsset(currentSource, "Assets/Resources/biomes.asset");
            AssetDatabase.SaveAssets();
        }
        else
        {

            AssetDatabase.Refresh();
            EditorUtility.SetDirty(currentSource);
            AssetDatabase.SaveAssets();
        }
        */
    }

	/*
    public void SaveMap(string map, int number)
    {
        MapDataSource currentSource = ScriptableObject.CreateInstance<MapDataSource>();
		MapDataSource current = maps[number];

        currentSource.number = maps[number].number;
        currentSource.id = maps[number].id;
        currentSource.blockCount = maps[number].blockCount;
        currentSource.time = maps[number].time;

        currentSource.idStartBlock = maps[number].idStartBlock;
        currentSource.idSimpleBlock = maps[number].idSimpleBlock;
        currentSource.idEndBlock = maps[number].idEndBlock;
        currentSource.dirLight = maps[number].dirLight;

        currentSource.activeSeasons = new List<SeasonActive>();
        currentSource.activeSeasons.AddRange(maps[number].activeSeasons);

        currentSource.fighterSeasons = new List<EnemySeason>();
        currentSource.fighterSeasons.AddRange(maps[number].fighterSeasons);

        currentSource.attackerSeasons = new List<EnemySeason>();
        currentSource.attackerSeasons.AddRange(maps[number].attackerSeasons);

        currentSource.neutralSeasons = new List<EnemySeason>();
        currentSource.neutralSeasons.AddRange(maps[number].neutralSeasons);

        //currentSource.miscSeasons = maps[number].miscSeasons;
        //currentSource.bonusSeasons = maps[number].bonusSeasons;
        //currentSource.complexMiscs = maps[number].complexMiscs;

        if (AssetDatabase.LoadAssetAtPath("Assets/Resources/maps/" + currentSource.id + ".asset", typeof(Object)) == null)
        {
            AssetDatabase.CreateAsset(currentSource, "Assets/Resources/maps/" + currentSource.id + ".asset");
            AssetDatabase.SaveAssets();
        }
        else
        {

            AssetDatabase.Refresh();
            EditorUtility.SetDirty(currentSource);
            AssetDatabase.SaveAssets();
        }
    }
	*/

    public void LoadParams(string map, int number)
    {
		mapData = AssetDatabase.LoadAssetAtPath ("Assets/Resources/mapsData.asset", typeof(MapsData)) as MapsData;
		/*
        XmlDocument document = new XmlDocument();
       // if (auto)
       // document.Load(Application.dataPath + "/GeneratorParams.xml");
       //     else
        document.Load(Application.dataPath + "/../../Data/" + map + "/GeneratorParams.xml");

        XmlNodeList fighterElements = document.GetElementsByTagName("Fighters");
		maps [number].fighterSeasons.AddRange (LoadEnemyClass (fighterElements.Item (0), 0));

        XmlNodeList attackerElements = document.GetElementsByTagName("Attackers");
		maps[number].attackerSeasons.AddRange (LoadEnemyClass(attackerElements.Item(0), 1));

        XmlNodeList neutralElements = document.GetElementsByTagName("Neutrals");
		maps[number].neutralSeasons.AddRange (LoadEnemyClass(neutralElements.Item(0), 2));

        XmlNodeList activeBiomes = document.GetElementsByTagName("Biomes");

		maps[number].activeSeasons.AddRange (new SeasonActive[activeBiomes.Item(0).ChildNodes.Count]);
        for (int i = 0; i < activeBiomes.Item(0).ChildNodes.Count; i++)
        {
            XmlNode biom = activeBiomes.Item(0).ChildNodes.Item(i);
            maps[number].activeSeasons[i] = new SeasonActive();
            maps[number].activeSeasons[i].name = biom.Name;
            maps[number].activeSeasons[i].wayNumber = int.Parse(biom.Attributes.GetNamedItem("way").Value);
            maps[number].activeSeasons[i].offset = int.Parse(biom.Attributes.GetNamedItem("way").Value);
            maps[number].activeSeasons[i].endType = (ActiveEndType)int.Parse(biom.Attributes.GetNamedItem("connection").Value);

            if (maps[number].activeSeasons[i].endType == ActiveEndType.next)
                maps[number].activeSeasons[i].seasonConnect = int.Parse(biom.Attributes.GetNamedItem("connectionBiome").Value);

            if (maps[number].activeSeasons[i].endType == ActiveEndType.connect)
            {
                maps[number].activeSeasons[i].seasonConnect = int.Parse(biom.Attributes.GetNamedItem("connectionBiome").Value);
                maps[number].activeSeasons[i].roadConnect = int.Parse(biom.Attributes.GetNamedItem("connectionRoad").Value);
            }

            XmlNode includeRoads = biom.ChildNodes.Item(0);
			maps[number].activeSeasons[i].roadMagazine = new List<RoadSourceData>();
			biomeSources.biomesSource [maps[number].activeSeasons[i].biomeNumber].roadSource = new List<RoadSourceData>();
            int checker = 0;
            foreach (XmlNode roadSource in includeRoads.ChildNodes)
            {
				RoadSourceData roadSrc = new RoadSourceData();
                roadSrc.name = roadSource.Name;
                roadSrc.coefficient = float.Parse(roadSource.Attributes.GetNamedItem("coefficient").Value);
                roadSrc.isActive = (roadSource.Attributes.GetNamedItem("isActive").Value == "1") ? true : false;
                roadSrc.roadSourceType = (RoadSourceType)int.Parse(roadSource.Attributes.GetNamedItem("forkType").Value);
				biomeSources.biomesSource [maps[number].activeSeasons[i].biomeNumber].roadSource.Add(roadSrc);
                if (roadSrc.isActive)
                    maps[number].activeSeasons[i].roadMagazine.Add(roadSrc);

            }

            XmlNode activeRoads = biom.ChildNodes.Item(1);
            maps[number].activeSeasons[i].roadElements = new List<RoadElement>();
            for (int j = 0; j < activeRoads.ChildNodes.Count; j++)
            {
                RoadElement roadElem = new RoadElement();
                XmlNode roadElement = activeRoads.ChildNodes.Item(j);
                roadElem = new RoadElement();
                roadElem.roadSourceType = (RoadElementType)(int.Parse(roadElement.Attributes.GetNamedItem("roadType").Value));
                switch (roadElem.roadSourceType)
                {
                    case RoadElementType.Default:
                        break;
                    case RoadElementType.Custom:
						roadElem.magazine = new List<RoadSourceData>();
                        foreach (XmlNode customRoad in roadElement.FirstChild.ChildNodes)
                        {
                            if (customRoad.Attributes.GetNamedItem("isActive").Value == "1")
                            {
								RoadSourceData customSrc = new RoadSourceData();
                                customSrc.name = customRoad.Name;
                                customSrc.coefficient = float.Parse(customRoad.Attributes.GetNamedItem("coefficient").Value);
                                roadElem.magazine.Add(customSrc);
                            }
                        }
                        break;
                    case RoadElementType.Special:
                        roadElem.special = int.Parse(roadElement.Attributes.GetNamedItem("special").Value);
                        break;
                    case RoadElementType.Fork:
                        roadElem.fork = (RoadSourceType)(int.Parse(roadElement.Attributes.GetNamedItem("forkType").Value));
                        //switch (activeSeasons[i].roadElements[checker].fork)
                        //{
                            //case RoadSourceType.ForkLeft:
                        roadElem.leftWay = int.Parse(roadElement.Attributes.GetNamedItem("leftWay").Value);
                        roadElem.rightWay = int.Parse(roadElement.Attributes.GetNamedItem("rightWay").Value);
                       
                       // }
                        break;
                    case RoadElementType.Connection:
                        roadElem.fork = (RoadSourceType)(int.Parse(roadElement.Attributes.GetNamedItem("forkType").Value) + 3);
                        roadElem.leftWay = int.Parse(roadElement.Attributes.GetNamedItem("leftWay").Value);
                        roadElem.rightWay = int.Parse(roadElement.Attributes.GetNamedItem("rightWay").Value);
                        break;
                }
                maps[number].activeSeasons[i].roadElements.Add(roadElem);
            }
       

            document.Load(Application.dataPath + "/../../Data/" + map + "/GeneratorMiscParams.xml");

            XmlNodeList generatorElements = document.GetElementsByTagName("season");

			maps[number].miscSeasons.AddRange (new MiscSeason[generatorElements.Count]);
            int seasonCount = 0;
            foreach (XmlNode generatorElement in generatorElements)
            {
                maps[number].miscSeasons[seasonCount] = new MiscSeason();


                XmlNodeList elementParams = generatorElement.ChildNodes;
                foreach (XmlNode elementParam in elementParams)
                {
                    switch (elementParam.Name)
                    {
                        case "name":
                            maps[number].miscSeasons[seasonCount].name = elementParam.InnerText;
                            break;
                        case "diffMin":
                            maps[number].miscSeasons[seasonCount].Difficult.x = float.Parse(elementParam.InnerText);
                            break;
                        case "diffMax":
                            maps[number].miscSeasons[seasonCount].Difficult.y = float.Parse(elementParam.InnerText);
                            break;
                        case "dur":
                            maps[number].miscSeasons[seasonCount].TimeDuration = float.Parse(elementParam.InnerText);
                            break;
                        case "coef":
                            maps[number].miscSeasons[seasonCount].Coefficient = float.Parse(elementParam.InnerText);
                            break;
                        case "timerMax":
                            maps[number].miscSeasons[seasonCount].timerMax = float.Parse(elementParam.InnerText);
                            break;
                        case "timerOff":
                            maps[number].miscSeasons[seasonCount].timerOffset = float.Parse(elementParam.InnerText);
                            break;
                        case "dispos":
                            maps[number].miscSeasons[seasonCount].isDisposable = (Disposable)System.Enum.Parse(typeof(Disposable), elementParam.InnerText);
                            break;
                    }
                }

                maps[number].miscSeasons[seasonCount].miscSlot = GetMiscSlotsFromXml(seasonCount + 1);

                seasonCount++;
            }

            document.Load(Application.dataPath + "/../../Data/" + map + "/GeneratorComplexParams.xml");

            generatorElements = document.GetElementsByTagName("complex");

			maps[number].complexMiscs.AddRange (new ComplexMisc[generatorElements.Count]);
            int complexCount = 0;
            foreach (XmlNode generatorElement in generatorElements)
            {
                maps[number].complexMiscs[complexCount] = new ComplexMisc();


                XmlNodeList elementParams = generatorElement.ChildNodes;
                foreach (XmlNode elementParam in elementParams)
                {
                    switch (elementParam.Name)
                    {
                        case "name":
                            maps[number].complexMiscs[complexCount].miscName = elementParam.InnerText;
                            break;
                        case "type":
                            maps[number].complexMiscs[complexCount].type = (ComplexType)System.Enum.Parse(typeof(ComplexType), elementParam.InnerText);
                            break;

                    }
                }

                maps[number].complexMiscs[complexCount].Miscs = GetMiscComplexFromXml(complexCount + 1);

                complexCount++;
            }

            document.Load(Application.dataPath + "/../../Data/" + map + "/GeneratorBonusParams.xml");

            generatorElements = document.GetElementsByTagName("season");

			maps[number].bonusSeasons.AddRange (new BonusSeason[generatorElements.Count]);
            seasonCount = 0;
            foreach (XmlNode generatorElement in generatorElements)
            {
                maps[number].bonusSeasons[seasonCount] = new BonusSeason();


                XmlNodeList elementParams = generatorElement.ChildNodes;
                foreach (XmlNode elementParam in elementParams)
                {
                    switch (elementParam.Name)
                    {
                        case "name":
                            maps[number].bonusSeasons[seasonCount].name = elementParam.InnerText;
                            break;
                        case "type":
                            maps[number].bonusSeasons[seasonCount].type = (BonusSeasonType)System.Enum.Parse(typeof(BonusSeasonType), elementParam.InnerText);
                            break;
                        case "diffMin":
                            maps[number].bonusSeasons[seasonCount].Difficult.x = float.Parse(elementParam.InnerText);
                            break;
                        case "diffMax":
                            maps[number].bonusSeasons[seasonCount].Difficult.y = float.Parse(elementParam.InnerText);
                            break;
                        case "dur":
                            maps[number].bonusSeasons[seasonCount].TimeDuration = float.Parse(elementParam.InnerText);
                            break;
                        case "coef":
                            maps[number].bonusSeasons[seasonCount].Coefficient = float.Parse(elementParam.InnerText);
                            break;
                        case "timerMax":
                            maps[number].bonusSeasons[seasonCount].timerMax = float.Parse(elementParam.InnerText);
                            break;
                        case "timerOff":
                            maps[number].bonusSeasons[seasonCount].timerOffset = float.Parse(elementParam.InnerText);
                            break;
                        case "dispos":
                            maps[number].bonusSeasons[seasonCount].isDisposable = (Disposable)System.Enum.Parse(typeof(Disposable), elementParam.InnerText);
                            break;
                        case "current":
                            maps[number].bonusSeasons[seasonCount].current = elementParam.InnerText;
                            break;
                    }
                }

                seasonCount++;
            }


        }
		*/
    }






    /*
    public EnemySlot[] GetEnemySlotsFromXml(int seasonNumber)
    {
        
        XmlDocument document = new XmlDocument();
        document.Load(Application.dataPath + "/../../Data/SeasonEnemyDataParams" + seasonNumber.ToString() + ".xml");
        XmlNodeList seasonElements = document.GetElementsByTagName("element");

        EnemySlot[] slots = new EnemySlot[seasonElements.Count];

        int elementCount = 0;
        foreach (XmlNode seasonElement in seasonElements)
        {
            slots[elementCount] = new EnemySlot();

            XmlNodeList elementParams = seasonElement.ChildNodes;
            foreach (XmlNode elementParam in elementParams)
            {
                switch (elementParam.Name)
                {
                    case "type":
                        slots[elementCount].enemyType = (EnemyType)System.Enum.Parse(typeof(EnemyType), elementParam.InnerText);
                        break;
                    case "coef":
                        slots[elementCount].cooficient = float.Parse(elementParam.InnerText);
                        break;
                    case "max":
                        slots[elementCount].max = int.Parse(elementParam.InnerText);
                        break;
                    case "dir":
                        slots[elementCount].direction = (EnemyDirection)System.Enum.Parse(typeof(EnemyDirection), elementParam.InnerText);
                        break;
                    
                }
            }
            elementCount++;
        }

        return slots;

    }
    */
    public MiscSlot[] GetMiscSlotsFromXml(int seasonNumber)
    {

        XmlDocument document = new XmlDocument();
        document.Load(Application.dataPath + "/../../Data/SeasonMiscDataParams" + seasonNumber.ToString() + ".xml");
        XmlNodeList seasonElements = document.GetElementsByTagName("element");

        MiscSlot[] slots = new MiscSlot[seasonElements.Count];

        int elementCount = 0;
        foreach (XmlNode seasonElement in seasonElements)
        {
            slots[elementCount] = new MiscSlot();

            XmlNodeList elementParams = seasonElement.ChildNodes;
            foreach (XmlNode elementParam in elementParams)
            {
                switch (elementParam.Name)
                {
                    case "type":
                        slots[elementCount].miscType = (MiscType)System.Enum.Parse(typeof(MiscType), elementParam.InnerText);
                        break;
                    case "name":
                        slots[elementCount].miscName = elementParam.InnerText;
                        break;
                    case "coef":
                        slots[elementCount].cooficient = float.Parse(elementParam.InnerText);
                        break;
                    case "max":
                        slots[elementCount].max = int.Parse(elementParam.InnerText);
                        break;
                    case "radius":
                        slots[elementCount].radius = float.Parse(elementParam.InnerText);
                        break;
                    case "offset":
                        slots[elementCount].offset = float.Parse(elementParam.InnerText);
                        break;

                }
            }
            elementCount++;
        }

        return slots;

    }

    public MiscSlot[] GetMiscComplexFromXml(int seasonNumber)
    {

        XmlDocument document = new XmlDocument();
        document.Load(Application.dataPath + "/../../Data/ComplexDataParams" + seasonNumber.ToString() + ".xml");
        XmlNodeList seasonElements = document.GetElementsByTagName("element");

        MiscSlot[] slots = new MiscSlot[seasonElements.Count];

        int elementCount = 0;
        foreach (XmlNode seasonElement in seasonElements)
        {
            slots[elementCount] = new MiscSlot();

            XmlNodeList elementParams = seasonElement.ChildNodes;
            foreach (XmlNode elementParam in elementParams)
            {
                switch (elementParam.Name)
                {
                    case "type":
                        slots[elementCount].miscType = (MiscType)System.Enum.Parse(typeof(MiscType), elementParam.InnerText);
                        break;
                    case "name":
                        slots[elementCount].miscName = elementParam.InnerText;
                        break;
                    case "delay":
                        slots[elementCount].cooficient = float.Parse(elementParam.InnerText);
                        break;
                    case "radius":
                        slots[elementCount].radius = float.Parse(elementParam.InnerText);
                        break;
                    case "offset":
                        slots[elementCount].offset = float.Parse(elementParam.InnerText);
                        break;

                }
            }
            elementCount++;
        }

        return slots;

    }

#endif



}


