﻿using System.Collections;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Generator))]
public class GeneratorEditor : Editor
{
    string[] maps = { "City01", "Desert01", "Grass01", "Standart"};
    public override void OnInspectorGUI()
    {
        Generator myTarget = (Generator)target;

		if (Application.isPlaying)
		{
			GUIStyle _labelStyle;
			_labelStyle = new GUIStyle();
			_labelStyle.alignment = TextAnchor.MiddleCenter;
			_labelStyle.clipping = TextClipping.Overflow;
			_labelStyle.fontSize = 12;

			if (myTarget.currentEnemySeason [0] != null) 
			{
				EditorGUI.ProgressBar (EditorGUILayout.GetControlRect (true, 20.0f, _labelStyle, GUILayout.Height (20.0f)), (myTarget.currentEnemyTime [0] + myTarget.currentEnemySeason [0].TimeDuration) / myTarget.GeneratorTime, "fighterSeason: " + myTarget.currentEnemySeason [0].name);
				GUILayout.Label (myTarget.enemyCounter.Find (myTarget.currentEnemySeason [0].enemyType).current.ToString () + "/" + myTarget.enemyCounter.Find (myTarget.currentEnemySeason [0].enemyType).max.ToString ());
				EditorGUILayout.Separator ();
			}
			if (myTarget.currentEnemySeason [1] != null) {
				EditorGUI.ProgressBar (EditorGUILayout.GetControlRect (true, 20.0f, _labelStyle, GUILayout.Height (20.0f)), (myTarget.currentEnemyTime [1] + myTarget.currentEnemySeason [1].TimeDuration) / myTarget.GeneratorTime, "attackerSeason: " + myTarget.currentEnemySeason [1].name);
				GUILayout.Label (myTarget.enemyCounter.Find (myTarget.currentEnemySeason [1].enemyType).current.ToString () + "/" + myTarget.enemyCounter.Find (myTarget.currentEnemySeason [1].enemyType).max.ToString ());
				EditorGUILayout.Separator ();
			}
			if (myTarget.currentEnemySeason [2] != null) 
			{
				EditorGUI.ProgressBar (EditorGUILayout.GetControlRect (true, 20.0f, _labelStyle, GUILayout.Height (20.0f)), (myTarget.currentEnemyTime [2] + myTarget.currentEnemySeason [2].TimeDuration) / myTarget.GeneratorTime, "neutralSeason: " + myTarget.currentEnemySeason [2].name);
				GUILayout.Label (myTarget.enemyCounter.Find (myTarget.currentEnemySeason [2].enemyType).current.ToString () + "/" + myTarget.enemyCounter.Find (myTarget.currentEnemySeason [2].enemyType).max.ToString ());
				EditorGUILayout.Separator ();
			}
		}

        myTarget.isEnemyEnabled = GUILayout.Toggle(myTarget.isEnemyEnabled, "Spawn Enemy");
        myTarget.isMiscEnabled = GUILayout.Toggle(myTarget.isMiscEnabled, "Spawn Misc");
        myTarget.isBonusEnabled = GUILayout.Toggle(myTarget.isBonusEnabled, "Spawn Bonus");
        myTarget.isBlockEnabled = GUILayout.Toggle(myTarget.isBlockEnabled, "Random Blocks");
        
        myTarget.forwardSpawnDistance = EditorGUILayout.FloatField("Forward Spawn Distance", myTarget.forwardSpawnDistance);
        myTarget.backSpawnDistance = EditorGUILayout.FloatField("Back Spawn Distance", myTarget.backSpawnDistance);
        
		/*
		for (int i=0; i<myTarget.mapData.maps.Count; i++)
        {
            GUILayout.BeginHorizontal();
			myTarget.mapData.maps[i].id = GUILayout.TextField(myTarget.mapData.maps[i].id);
			if (GUILayout.Button("LoadParams " + myTarget.mapData.maps[i].id))
            {
				myTarget.LoadParams(myTarget.mapData.maps[i].id, i);

            }
			if (GUILayout.Button("Save" + myTarget.mapData.maps[i].id))
            {
				//myTarget.SaveMap(myTarget.mapData.maps[i].id, i);

            }
            GUILayout.EndHorizontal();
            
        }

        myTarget._Main.currentMap = EditorGUILayout.Popup("Map", myTarget._Main.currentMap, maps);
        
        if (GUILayout.Button("SaveBiomes"))
        {
            myTarget.SaveBiomes();
        }
        */
        
    }
}