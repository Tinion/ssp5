﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;
using System.Collections.Generic;
using System.Xml;

[System.Serializable]
public class ResourceInstance
{
    public string name;
    public string prefabPath;

    public ResourceInstance(string _name, string _prefabPath)
    {
        name = _name;
        prefabPath = _prefabPath;
    }
}

[System.Serializable]
public class ResourcesDatabase : ScriptableObject {

    public static readonly string assetName = "resourcesDB";
    private static ResourcesDatabase _instance;

    public List<ResourceInstance> blocks;
    public List<ResourceInstance> misc;
    public List<ResourceInstance> vehicles;
    public List<ResourceInstance> modules;

	   

  
    private static void Add(string Path, int number)
    {
         int startIndex = Path.LastIndexOf('/');
        int length = Path.Length - startIndex - 8;
        string name = Path.Substring(startIndex+1, length);
        name = name.ToLower();
        switch (number)
        {
        case 0:
			_instance.blocks.Add(new ResourceInstance(name.ToLower(), Path));
        break;
        case 1:
			_instance.vehicles.Add(new ResourceInstance(name.ToLower(), Path));
        break;
        case 2:
			_instance.misc.Add(new ResourceInstance(name.ToLower(), Path));
        break;
        case 3:
			_instance.modules.Add(new ResourceInstance(name.ToLower(), Path));
        break;
        }
        Debug.Log(name + " " + Path);
        


    }
    #if UNITY_EDITOR
    //[MenuItem("ResourceDatabase/Output")]
    private static void Output()
    {
        //if (_instance == null)
        //{
            _instance = Resources.Load(assetName, typeof(Object)) as ResourcesDatabase;
        //}
            if (_instance.blocks == null && _instance.vehicles == null && _instance.misc == null && _instance.modules == null)
            Debug.Log("Database is null");

        else
        {
            for (int i=0; i < _instance.blocks.Count; i++)
                Debug.Log(_instance.blocks[i].name + " " + _instance.blocks[i].prefabPath);
            for (int i = 0; i < _instance.misc.Count; i++)
                Debug.Log(_instance.misc[i].name + " " + _instance.misc[i].prefabPath);
            for (int i = 0; i < _instance.vehicles.Count; i++)
                Debug.Log(_instance.vehicles[i].name + " " + _instance.vehicles[i].prefabPath);
            for (int i = 0; i < _instance.modules.Count; i++)
                Debug.Log(_instance.modules[i].name + " " + _instance.modules[i].prefabPath);
        }
    }

    [MenuItem("ResourceDatabase/Rebuild")]
    private static void Create()
    {
        _instance = Resources.Load(assetName, typeof(Object)) as ResourcesDatabase;
        if (_instance == null)
        {
            Debug.LogError("Cannot load ResourcesDatabase");
            _instance = ScriptableObject.CreateInstance(typeof(ResourcesDatabase)) as ResourcesDatabase;
        }


        _instance.blocks = new List<ResourceInstance>();
        _instance.misc = new List<ResourceInstance>();
        _instance.vehicles = new List<ResourceInstance>();
        _instance.modules = new List<ResourceInstance>();

        //string blocks = "Assets/Art/environment/blocks/";
        //string misc = "Assets/Art/misc/";
        //string vehicles = "Assets/Art/vehicles/";

        string blocks = "Assets/Resources/blocks/";
        string misc = "Assets/Resources/misc/";
        string vehicles = "Assets/Resources/vehicles/";
        string modules = "Assets/Resources/modules/";

        string[] Assets = AssetDatabase.GetAllAssetPaths();
        //for (int i=0; i<Assets.
        for (int i = 0; i < Assets.Length; i++)
        {
            if (Assets[i].Substring(Assets[i].Length - 5, 5) == "refab")
                if (Assets[i].Length > blocks.Length && Assets[i].Substring(0, blocks.Length) == blocks)
                {
                    Add(Assets[i], 0);
                }
                else
                    if (Assets[i].Length > vehicles.Length && Assets[i].Substring(0, vehicles.Length) == vehicles)
                    {
                        Add(Assets[i], 1);
                    }
                    else
                        if (Assets[i].Length > misc.Length && Assets[i].Substring(0, misc.Length) == misc)
                    {
                        Add(Assets[i], 2);
                    }
                        else
                            if (Assets[i].Length > modules.Length && Assets[i].Substring(0, modules.Length) == modules)
                            {
                                Add(Assets[i], 3);
                            }
                    

        }

        if (AssetDatabase.LoadAssetAtPath("Assets/Resources/ResourcesDB.asset", typeof(Object)) == null)
        AssetDatabase.CreateAsset(_instance, "Assets/Resources/ResourcesDB.asset");
        AssetDatabase.Refresh();
        EditorUtility.SetDirty(_instance);
        AssetDatabase.SaveAssets();

    }
#endif


}
