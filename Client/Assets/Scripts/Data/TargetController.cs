﻿using UnityEngine;
using Dest.Math;
using System.Collections;
using System.Collections.Generic;

public class TargetController : MonoBehaviour {

    [HideInInspector]
    public Transform transform;
    
    [HideInInspector]
    public List<OneTarget> targets;

    public Transform currentPlaneBeacon;

	/*
    private void Start()
    {
        Main.instance = FindObjectOfType<Main>();
    }
*/
    private void Update()
    {
		currentPlaneBeacon.localPosition = new Vector3(0, MainCamera.instance.altitude, 0);
    }

    public OneTarget CreateTarget(bool first, float lifeTime, Object targetSource, Transform master)
    {
        GameObject target = GameObject.Instantiate(targetSource) as GameObject;
        OneTarget current = target.GetComponent<OneTarget>();
        current.Create(lifeTime, master);

        current.transform.parent = transform;
        current.transform.position = Vector3.zero;
        current.transform.rotation = Quaternion.identity;
        Vector3 localPos = Vector3.zero;

        if (first == true)
        {
			Vector3 heroPos = transform.InverseTransformPoint(Hero.instance.transform.position);
            localPos = heroPos;
            current.posOnControl = heroPos.ToVector2XZ();
        }
        else
        {
            Vector2 newPos;

			float roadWidth = Hero.instance.vehicle.GetSpline().GetCurrentWidth(Hero.instance.currentStep);
            newPos.x = Random.Range(-0.75f, 0.75f);
            newPos.y = Random.Range(-0.75f, 0.75f);
            newPos.x = Mathf.Lerp(-roadWidth, roadWidth, newPos.x);
            newPos.y = Mathf.Lerp(-current.distance.x, current.distance.y, newPos.y);

            while (CrossOther(newPos, current))
            {
                newPos.x = Random.Range(-0.75f, 0.75f);
                newPos.y = Random.Range(-0.75f, 0.75f);
                newPos.x = Mathf.Lerp(-roadWidth, roadWidth, newPos.x);
                newPos.y = Mathf.Lerp(-current.distance.x, current.distance.y, newPos.y);
            }

            current.posOnControl = newPos;
            localPos = newPos.ToVector3XZ();
        }
        current.transform.localPosition = localPos;
        current.targetController = this;

        return current;
    }

    public OneArcMachine CreateTargetArc(bool first, float lifeTime, Object targetSource, Transform master)
    {
        GameObject target = GameObject.Instantiate(targetSource) as GameObject;
        OneArcMachine current = target.GetComponent<OneArcMachine>();
        current.Create(lifeTime, master);

        current.transform.parent = transform;
        current.transform.position = Vector3.zero;
        current.transform.rotation = Quaternion.identity;
        Vector3 localPos = Vector3.zero;

        if (first == true)
        {
			Vector3 heroPos = transform.InverseTransformPoint(Hero.instance.transform.position);
            localPos = heroPos;
            current.posOnControl = heroPos.ToVector2XZ();
        }
        else
        {
            Vector2 newPos;

			float roadWidth = Hero.instance.vehicle.GetSpline().GetCurrentWidth(Hero.instance.currentStep);
            newPos.x = Random.Range(0.0f, 2.0f) - 1.0f;
            newPos.y = Random.Range(0.0f, 2.0f) - 1.0f;
            newPos.x = Mathf.Lerp(-roadWidth, roadWidth, newPos.x);
            newPos.y = Mathf.Lerp(-current.distance.x, current.distance.y, newPos.y);

            while (CrossOther(newPos, current))
            {
                newPos.x = Random.Range(0.0f, 2.0f) - 1.0f;
                newPos.y = Random.Range(0.0f, 2.0f) - 1.0f;
                newPos.x = Mathf.Lerp(-roadWidth, roadWidth, newPos.x);
                newPos.y = Mathf.Lerp(-current.distance.x, current.distance.y, newPos.y);
            }

            current.posOnControl = newPos;
            localPos = newPos.ToVector3XZ();
        }
        current.transform.localPosition = localPos;
        current.transform.LookAt(master);
        current.targetController = this;

        return current;
    }

    public OneTarget[] CreateTargets(int count, float lifeTime, Object targetSource, Transform master)
    {
        targets = new List<OneTarget>();
        for (int i = 0; i < count; i++)
        {
            targets.Add(CreateTarget((i == 0) ? true : false, lifeTime, targetSource, master));
      
        }
        return targets.ToArray();

    }

    public bool CrossOther(Vector2 _pos, OneTarget current)
    {
        bool cross = false;
        for (int i = 0; i < targets.Count; i++)
        {
            if (Vector2.Distance(targets[i].posOnControl, _pos) < current.radius)
            cross = true;
        }

        return cross;
    }
}
