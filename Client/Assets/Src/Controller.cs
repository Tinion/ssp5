﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Dest.Math;
public class Controller : MonoBehaviour {

	public static Controller instance;
	public Mode gameMode;
	public bool isSwitch = false;
	public bool isFromBattle = false;
	public float switchTime;
	public CounterTimerF switchTimer = new CounterTimerF(0.5f, 0.5f, false);
	public float fromBattleTime = 1f;
	public float fromFreeTime = 1f;

	public float outsideOffset = 1.2f;
	public float insideOffset = 1f;

	public static float highwayDirection = 1; 
	// Use this for initialization
	void Start () 
	{
		instance = this;	
	}

	public void Update()
	{
		switch (gameMode) 
		{
		case Mode.Battle:

			if (Mathf.Abs (Hero.instance.currentSplineOffset) > outsideOffset) {
				if (!isSwitch) {
					isSwitch = true;
					isFromBattle = true;
					switchTimer.max = fromBattleTime;
					switchTimer.Restore ();
				}

				if (switchTimer.isEnd (Time.deltaTime)) {
					gameMode = Mode.Free;
					isSwitch = false;
					return;
				} 


			} else if (isSwitch) {
				if (switchTimer.isStart (Time.deltaTime)) {
					isSwitch = false;
					return;
				}
			}
			else
				switchTimer.Restore ();

			switchTime = switchTimer.NormalizeTime ();
			break;
		
		case Mode.Free:

			if (Mathf.Abs (Hero.instance.currentSplineOffset) < insideOffset) {
				if (!isSwitch) {
					isSwitch = true;
					isFromBattle = false;
					switchTimer.max = fromFreeTime;
					switchTimer.current = 0;
				}

				if (switchTimer.isStart (Time.deltaTime)) {
					gameMode = Mode.Battle;
					isSwitch = false;
					return;
				} 


			} else if (isSwitch) {
				if (switchTimer.isEnd (Time.deltaTime)) {
					isSwitch = false;
					return;
				}
			} else
				switchTimer.current = 0;

			float hDir = 1;
			if (Hero.instance!=null)
				hDir  = Hero.instance.GetSplineDirection ();
			highwayDirection = (hDir < 0) ? -1 : 1;
			switchTime = switchTimer.NormalizeTime ();
			break;

		
		}



	}



}