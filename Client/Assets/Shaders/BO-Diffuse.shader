Shader "MarkRoad" 
{
	Properties 
	{
		_DiffTex("Diffuse (RGBA)", 2D) = "white" {}
		_DetailTex("Detail (RGBA)", 2D) = "white" {}
		
		_Diffuse("Diffuse color", Color) = (1,1,1,1)

	}
	SubShader 
	{
		Tags 
		{ 
			"RenderType"="Opaque" 
		}
		
		CGPROGRAM
		#pragma debug
		#pragma surface surf Lambert
		
		struct Input 
		{
			float2 uv_DiffTex;
			float2 uv2_Markup;
			float3 worldPos; 
		};
		
		sampler2D _DiffTex;
		sampler2D _DetailTex;
		
		float4 _Diffuse;
		
		void surf(Input IN, inout SurfaceOutput o) 
		{
			half3 diffTex = tex2D(_DiffTex, IN.uv_DiffTex).rgb;
			half markUp = tex2D(_DiffTex, IN.uv2_Markup).a;
			
			//o.Albedo = diffTex * _Diffuse.rgb; + markUp;
			o.Albedo = markUp;
		}
		
		ENDCG
	} 
	FallBack "Diffuse"
}
