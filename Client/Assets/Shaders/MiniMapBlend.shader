Shader "MiniMapBlend" 
{
	Properties
	{
	    _Diffuse("Diffuse color", Color) = (1,1,1,1)
		_DiffTex("Diffuse (RGBA)", 2D) = "white" {}
	}
	SubShader 
	{
		Tags 
		{ 
			"Queue"="Transparent-100"
			"RenderType"="Opaque" 
		}
		Blend SrcAlpha OneMinusSrcAlpha
		ZWrite Off
		
		CGPROGRAM
		#pragma debug
		#pragma surface surf Custom
		
		sampler2D _DiffTex;
		
		struct Input 
		{
			float2 uv_DiffTex;

		};
		
		float4 _Diffuse;
		float inputX = 0.0f;
		float inputY = 0.0f;
		float inputA = 1.0f;

		void surf(Input IN, inout SurfaceOutput o) 
		{
			float2 set_uv = IN.uv_DiffTex;
			float alpha = inputA;
			set_uv.x += inputX;
			set_uv.y += inputY;
			half4 diffTex = tex2D(_DiffTex, set_uv);
			o.Albedo = diffTex.rgb * _Diffuse.rgb;
			o.Alpha = 0.5f;
		}
		
		half4 LightingCustom(SurfaceOutput s, half3 lightDir, half atten)
		{
			half4 c;
			c.rgb = s.Albedo.rgb;
			c.a = s.Alpha;

			return c;
		}
		
		ENDCG
	} 
	
}
