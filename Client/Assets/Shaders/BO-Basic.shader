Shader "BO/Basic" 
{
	Properties 
	{
		_DiffTex("Diffuse (RGBA)", 2D) = "white" {}
		_NormalTex("Normal (S_Nx_G_Ny)", 2D) = "white" {}
		
		_Diffuse("Diffuse color", Color) = (1,1,1,1)
		_SpecularC("Specular color", Color) = (0,0,0,1)

		_Specular("Specular intencity", Range(0,1)) = 0
		_Shininess("Shininess", Range(0,10)) = 0.1

		_Cutoff("Cutoff", Range(0,1)) = 0.5
	}
	SubShader 
	{
		Tags 
		{ 
			"Queue"="Overlay"
			"RenderType"="Opaque" 
			"Shadow"="On"
		}
		LOD 200
		ColorMask RGB
		
		CGPROGRAM
		#pragma debug
		#pragma surface surf Custom addshadow vertex:vert  alphatest:_Cutoff
		

		sampler2D _DiffTex;
		sampler2D _NormalTex;
		
		float4 _Diffuse;
		float4 _SpecularC;
		float  _Specular;
		float  _Shininess;

		struct Input 
		{
			float2 uv_DiffTex;
			float3 worldRefl; 
			float3 worldPos; 
			INTERNAL_DATA
		};

		struct SurfaceOutputCustom {
			half3 Albedo;
			half3 Normal;
			half3 Emission;
			half3 Reflection;			
			half  Specular;
			half  Gloss;
			half  Alpha;
			half  Reflectivity;
			
		};

		#include "Assets/VacuumShaders/Curved World/Shaders/cginc/CurvedWorld_Base.cginc"

	void vert (inout appdata_full v, out Input o) {
	    #if defined(SHADER_API_D3D11) || defined(SHADER_API_D3D11_9X) || defined(UNITY_PI)
			UNITY_INITIALIZE_OUTPUT(Input, o);
		#endif

		V_CW_TransformPointAndNormal(v.vertex, v.normal, v.tangent);
		v.texcoord = mul(UNITY_MATRIX_TEXTURE0, v.texcoord);


		float3 Wpos=mul(unity_ObjectToWorld, v.vertex);
	}

		void surf(Input IN, inout SurfaceOutputCustom o) 
		{
			half4 diffTex = tex2D(_DiffTex, IN.uv_DiffTex);
			half4 normTex = tex2D(_NormalTex, IN.uv_DiffTex);
			
			// Decode normal
			o.Normal.xy = normTex.ag * 2 - 1;
			o.Normal.z  = sqrt( 1 - dot(o.Normal.xy, o.Normal.xy) );
			
			
			// Make albedo
			o.Albedo = diffTex.rgb * _Diffuse.rgb;
			
			// Write scalar values
			o.Specular = _Shininess;
			o.Gloss = normTex.r * _Specular;
			o.Alpha = diffTex.a;
		}
		
		half4 LightingCustom(SurfaceOutputCustom s, half3 lightDir, half3 viewDir, half atten)
		{
			half3 h = normalize(lightDir + viewDir);
			
			half diff = max(0, dot(s.Normal, lightDir));
			
			float nh = max(0, dot(s.Normal, h));
			float spec = pow(nh, s.Specular*128.0f) * s.Gloss;
			
			half4 c;
			c.rgb = _LightColor0.rgb * (s.Albedo.rgb * diff + _SpecularC.rgb * spec) * (atten * 2);
			c.a = s.Alpha;
			
			
			
			return c;
		}
		
		ENDCG
	} 
	FallBack "BO/Diffuse"
}
