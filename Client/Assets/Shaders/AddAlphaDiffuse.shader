Shader "AddAlphaDiffuse" 
{
	Properties 
	{
		_MainTex ("Diffuse (RGBA)", 2D) = "white" {}
		_NormalTex ("Normal (RGBA)", 2D) = "bump" {}
		
		_SpecularC("Specular color", Color) = (0,0,0,1)
		_Specular("Specular intencity", Range(0,1)) = 0
		_Shininess("Shininess", Range(0,1)) = 0.1
	}
	SubShader 
	{
		Tags {  "RenderType"="Opaque" 
				"Shadow"="On"}

		
				CGPROGRAM
				#pragma debug
				#pragma surface surf Custom addshadow 
				#pragma target 3.0              
				
				struct Input 
				{
					float2 uv_MainTex;
					float2 uv2_NormalTex;
					float3 worldRefl; 
					float3 worldPos; 
					INTERNAL_DATA
				};
				
				sampler2D _MainTex;
				sampler2D _NormalTex;
				float4 _SpecularC;
				float  _Specular;
				float  _Shininess;
				
				struct SurfaceOutputCustom
				 {
					half3 Albedo;
					half3 Normal;
					half3 Emission;
					half  Specular;
					half  Gloss;
					half  Alpha;
				};
	                        
				void surf(Input IN, inout SurfaceOutputCustom o) 
				{
					half3 diffuse = tex2D(_MainTex, IN.uv_MainTex).rgb;
					float detail = tex2D(_MainTex, IN.uv2_NormalTex).a;
					half4 normal = tex2D(_NormalTex, IN.uv_MainTex).rgba;
												
					o.Normal.xy = normal.rg * 2 - 1;
					o.Normal.z  = sqrt( 1 - dot(o.Normal.xy, o.Normal.xy) );

					o.Albedo = diffuse + detail;
			
					o.Specular = _Shininess;
					o.Gloss = normal.a * _Specular;

				}
				
				half4 LightingCustom(SurfaceOutputCustom s, half3 lightDir, half3 viewDir, half atten)
				{
					half3 h = normalize(lightDir + viewDir);
			
					half diff = max(0, dot(s.Normal, lightDir));
			
					float nh = max(0, dot(s.Normal, h));
					float spec = pow(nh, s.Specular*128.0f) * s.Gloss;
			
					half4 c;
					c.rgb = _LightColor0.rgb * (s.Albedo.rgb * diff + _SpecularC.rgb * spec) * (atten * 2);
			
					return c;
				}
		
				ENDCG
			} 
			FallBack "Diffuse"
	

}
