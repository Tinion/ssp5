﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Mirror" 
{
	Properties
	{
		_MainTex ("Base (RGB)", 2D) = "white" {}
		[HideInInspector] _ReflectionTex ("", 2D) = "white" {}
		_SpecularColor("Specular color", Color) = (0,0,0,0)
		_Specular("Specular intencity", Range(0,1)) = 0
		_Shininess("Shininess", Range(0,1)) = 0.1
		_FresnelBias("Fresnel Bias", Range(1,10)) = 2
		_FresnelCoeff("Fresnel Power", Range(1,8)) = 3
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100
 
		Pass {
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "AutoLight.cginc"
			struct v2f
			{
				float2 uv : TEXCOORD0;		
				float4 refl : TEXCOORD1;
				SHADOW_COORDS(1)
				float4 pos : SV_POSITION;
				fixed3 diff : COLOR0;
                fixed3 ambient : COLOR1;
				
			};
			float4 _MainTex_ST;
			float4 _SpecularColor;
			float  _Specular;
			float  _Shininess;
			float  _FresnelBias;
			float  _FresnelCoeff;
			float fresnel;
			float3 viewDir;
			float3 _normal;
			v2f vert(float4 pos : POSITION, float2 uv : TEXCOORD0, float3 normal : NORMAL)
			{
				v2f o;
				o.pos = UnityObjectToClipPos (pos);
				o.uv = TRANSFORM_TEX(uv, _MainTex);
				o.refl = ComputeScreenPos (o.pos);
				float3 worldPos = mul(unity_ObjectToWorld, pos).xyz;
				viewDir = normalize(UnityWorldSpaceViewDir(pos));
				fresnel = 1.0f / pow(1.0f + max(0, dot(normal, normalize(viewDir))), _FresnelCoeff);
				fresnel = saturate(lerp(0.0f, _FresnelBias, fresnel));

				half3 worldNormal = UnityObjectToWorldNormal(normal);
				_normal = normal;
                half nl = max(0, dot(worldNormal, _WorldSpaceLightPos0.xyz));
                o.diff = nl * _LightColor0.rgb;
                o.ambient = ShadeSH9(half4(worldNormal,1));
				TRANSFER_SHADOW(o);
				return o;
			}
			sampler2D _MainTex;
			sampler2D _ReflectionTex;

			

			fixed4 frag(v2f i) : SV_Target
			{
				half3 lightdir = normalize(_WorldSpaceLightPos0.xyz);
				half3 h = normalize(lightdir + viewDir);
			
				half diff = max(0, dot(_normal, lightdir));
			
				float nh = max(0, dot(_normal, h));
				float spec = pow(nh, _Shininess*256.0f);

				fixed4 tex = tex2D(_MainTex, i.uv);
				fixed4 refl = tex2Dproj(_ReflectionTex, UNITY_PROJ_COORD(i.refl));
				fixed shadow = SHADOW_ATTENUATION(i);

				return  lerp((tex*refl + _SpecularColor * spec) * shadow, _SpecularColor, 0.6);

			}
			ENDCG
	    }

		UsePass "Legacy Shaders/VertexLit/SHADOWCASTER"
	}
}
