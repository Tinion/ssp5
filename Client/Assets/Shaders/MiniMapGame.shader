Shader "MiniMapGame" 
{
	Properties
	{
	    _Diffuse("Diffuse color", Color) = (1,1,1,1)
		_DiffTex("Diffuse (RGBA)", 2D) = "white" {}

	}
	SubShader 
	{
		Tags 
		{ 
			"Queue"="Geometry-100"
			"RenderType"="Opaque" 
		}
		Blend SrcAlpha OneMinusSrcAlpha
		ZWrite Off
		
		CGPROGRAM
		#pragma debug
		#pragma surface surf Custom
		
		sampler2D _DiffTex;

		
		
		
		struct Input 
		{
			float2 uv_DiffTex;


		};
		
		float4 _Diffuse;
		void surf(Input IN, inout SurfaceOutput o) 
		{
			half4 diffTex = tex2D(_DiffTex, IN.uv_DiffTex);
			o.Albedo = diffTex.rgb * _Diffuse.rgb;
			o.Alpha = diffTex.a;

		}
		
		half4 LightingCustom(SurfaceOutput s, half3 lightDir, half atten)
		{
			half4 c;
			c.rgb = s.Albedo.rgb;
			c.a = s.Alpha;

			return c;
		}
		
		ENDCG
	} 
	
}
