Shader "BO/Glass"
{
	Properties 
	{
		_DiffTex("Diffuse (RGBA)", 2D) = "white" {}
		_CubemapTex("Cubemap (RGB)", Cube) = "" { TexGen CubeReflect }
		
		_Diffuse("Diffuse color", Color) = (1,1,1,1)
		
		_SpecularColor("Specular color", Color) = (0,0,0,0)
		_Specular("Specular intencity", Range(0,1)) = 0
		_Shininess("Shininess", Range(0,1)) = 0.1
		_FresnelBias("Fresnel Bias", Range(1,10)) = 2
		_FresnelCoeff("Fresnel Power", Range(1,8)) = 3
	}
	SubShader 
	{
		Tags 
	    { 
			"Queue"="Geometry-35"
			"RenderType"="Transparent" 
	    }
		ColorMask RGB
		LOD 200
	  	Blend SrcAlpha OneMinusSrcAlpha
	
		CGPROGRAM
		#pragma surface surf Custom nolightmap
		#pragma target 2.0

		sampler2D _DiffTex;
		samplerCUBE _CubemapTex;
		
		float4 _Diffuse;
		float4 _SpecularColor;
		float  _Specular;
		float  _Shininess;
		float  _FresnelBias;
		float  _FresnelCoeff;

		struct Input 
		{
			float2 uv_DiffTex;
			float3 worldRefl; 
			float3 viewDir; 
			INTERNAL_DATA
		};

	    struct SurfaceOutputCustom
	    {
			half3 Albedo;
			half3 Normal;
			half3 Emission;
			half3 viewDir;
			half  Specular;
			half  Alpha;
			half  Gloss;   
			half  WarFog;
		};
		
	    void surf(Input IN, inout SurfaceOutputCustom o) 
	    {
			half4 diffTex = tex2D(_DiffTex, IN.uv_DiffTex);
			
			// Calculate reflection
			half4 cubeTex = texCUBE(_CubemapTex, IN.worldRefl);
			
			// Fresnel
			half fresnel = 1.0f / pow(1.0f + max(0, dot(o.Normal, normalize(IN.viewDir))), _FresnelCoeff);
			fresnel = saturate(lerp(0.0f, _FresnelBias, fresnel));
			
			// Make albedo
			o.Albedo = lerp(diffTex.rgb, cubeTex.rgb, fresnel);
			o.Albedo *= _Diffuse.rgb;
			
			// Write scalar values
			o.Specular = _Shininess;
			o.Alpha = fresnel * diffTex.a * _Diffuse.a;
			o.Gloss = _Specular;
	    }
		

	    half4 LightingCustom(SurfaceOutputCustom s, half3 lightDir, half3 viewDir, half atten)
		{
			half3 h = normalize(lightDir + viewDir);
			
			float nh = max(0, dot(s.Normal, h));
			float spec = pow(nh, s.Specular*256.0f) * s.Gloss;
			
			half4 c;
			c.rgb = _LightColor0.rgb * (s.Albedo + _SpecularColor.rgb * spec) * (atten * 2);
			c.a = s.Alpha;
			return c;
		}
		
		ENDCG
	} 
	FallBack off
}
