Shader "BO/Main" 
{
	Properties 
	{
		_DiffTex("Diffuse (RGBA)", 2D) = "white" {}
		_NormalTex("Normal (S_Nx_G_Ny)", 2D) = "white" {}
		_AOTex("Ambient Occlusion (Grey)", 2D) = "white" {}
		_DecalTex("Decal (RGBA)", 2D) = "white" {}
		
		_Diffuse("Diffuse color", Color) = (1,1,1,1)
		_SpecularC("Specular color", Color) = (0,0,0,1)

		_Specular("Specular intencity", Range(0,1)) = 0
		_Shininess("Shininess", Range(0,1)) = 0.1
		_DecalOpacity("Decal Opacity", Range(0,1)) = 0

		_Cutoff("Cutoff", Range(0,1)) = 0.5
	}
	SubShader 
	{
		Tags 
		{ 
			"Queue"="Geometry-40"
			"RenderType"="Opaque" 
			"Shadow"="On"
		}
		ColorMask RGB
		LOD 200
		
		CGPROGRAM
		#pragma debug
		#pragma surface surf Custom addshadow
		#pragma target 3.0
	

		sampler2D _DiffTex;
		sampler2D _NormalTex;
		sampler2D _AOTex;
		sampler2D _DecalTex;
		
		float4 _Diffuse;
		float4 _SpecularC;
		float  _DecalOpacity;
		float  _Specular;
		float  _Shininess;

		struct Input 
		{
			float2 uv_DiffTex;
			float2 uv2_AOTex;
			float3 worldRefl; 
			float3 worldPos; 
			INTERNAL_DATA
		};

		struct SurfaceOutputCustom {
			half3 Albedo;
			half3 Normal;
			half3 Emission;
			half  Specular;
			half  Gloss;
			half  Alpha;
			half  WarFog;
		};
		
		void surf(Input IN, inout SurfaceOutputCustom o) 
		{
			half4 diffTex = tex2D(_DiffTex, IN.uv_DiffTex);
			half4 normTex = tex2D(_NormalTex, IN.uv_DiffTex);
			//half4 decalTex = tex2D(_DecalTex, IN.uv_DiffTex);
			half4 decalTex = tex2D(_DecalTex, IN.uv2_AOTex);
			
			half  aoValue = tex2D(_AOTex, IN.uv_DiffTex).r;
			
			// Decode normal
			o.Normal.xy = normTex.ag * 2 - 1;
			o.Normal.z  = sqrt( 1 - dot(o.Normal.xy, o.Normal.xy) );
			
			// Calculate our main coeffs
			half coeffDecal = decalTex.a * _DecalOpacity;

		

			// Make albedo
			o.Albedo = lerp(diffTex.rgb, decalTex.rgb, coeffDecal) * _Diffuse.rgb * aoValue;
			
			// Write scalar values
			o.Specular = _Shininess;
			o.Gloss = normTex.r * _Specular * (1-coeffDecal);
			o.Alpha = decalTex.a;
		}
		
		half4 LightingCustom(SurfaceOutputCustom s, half3 lightDir, half3 viewDir, half atten)
		{
			half3 h = normalize(lightDir + viewDir);
			
			half diff = max(0, dot(s.Normal, lightDir));
			
			float nh = max(0, dot(s.Normal, h));
			float spec = pow(nh, s.Specular*128.0f) * s.Gloss;
			
			half4 c;
			c.rgb = _LightColor0.rgb * (s.Albedo.rgb * diff + _SpecularC.rgb * spec) * (atten * 2);
			c.a = s.Alpha;
			
			
			
			return c;
		}
		
		ENDCG
	} 
	FallBack "BO/Basic"
}
