﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'PositionFog()' with multiply of UNITY_MATRIX_MVP by position
// Upgrade NOTE: replaced 'V2F_POS_FOG' with 'float4 pos : SV_POSITION'

Shader "MiniMap"
 {
   Properties {
      _MainTex ("Base (RGB)", 2D) = "white" {}
      _MaskTex ("Mask (RGB)", 2D) = "white" {}
      _Rotation ("Rotation", Range (-9.42, 9.42)) = 0.0
      _MoveX ("Move X", Range (0,1)) = 0.5
      _MoveY ("Move Y", Range (0,1)) = 0.5
      _Scale ("Scale", Range (1,10)) = 1.0
   }
   SubShader {
      
      Tags {Queue=Transparent}
      Lighting Off
      ColorMask RGB
      
      
      Pass {  //+1 draw call можно закоментить, немного повлияет на текстуру
         blend Zero OneMinusSrcColor
         SetTexture [_MaskTex] {
            Combine texture
         }
      }
      
      
      Pass {
      
         blend One OneMinusSrcColor
         
            
CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#include "UnityCG.cginc"

struct v2f {
    float4 pos : SV_POSITION;
    float2  uv : TEXCOORD0;
    float2  uv2 : TEXCOORD1;
};

uniform float4 _MainTex_ST;
uniform float4 _MaskTex_ST;
uniform float _Rotation;
uniform float _MoveX;
uniform float _MoveY;
uniform float _Scale;

v2f vert (appdata_base v)
{
    v2f o;
    o.pos = UnityObjectToClipPos (v.vertex);
    float2 uv1 = TRANSFORM_TEX(v.texcoord, _MainTex);
    o.uv2 = TRANSFORM_TEX(v.texcoord, _MaskTex);

   uv1 -= float2( 0.5, 0.5 );
   o.uv.x =  cos( _Rotation )*uv1.x + sin(_Rotation)*uv1.y;
   o.uv.y = -sin( _Rotation )*uv1.x + cos(_Rotation)*uv1.y;
   o.uv += float2(_MoveY*_Scale, _MoveX*_Scale);
   o.uv /= _Scale;

    return o;
}

uniform sampler2D _MainTex;
uniform sampler2D _MaskTex;

half4 frag (v2f i) : COLOR
{
   half4 mainCol = tex2D( _MainTex, i.uv );
   half4 maskCol = tex2D( _MaskTex, i.uv2 );
   
   return mainCol * maskCol;
}
ENDCG

      }

      
   }
}
